package idsoft.inspectiondepot.sinkholeinspection;

import idsoft.inspectiondepot.sinkholeinspection.Observation1.Touch_Listener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ObjectOutputStream.PutField;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.R.integer;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Path.FillType;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class photos extends Activity implements Runnable {

	CommonFunctions cf;
	String paht, fullname, InspectorId, strdesc, picname, homeid, updstr,
		selectedImagePath = "empty", capturedImageFilePath, sgnpath,
		pathdesc, InspectionType, status, ImgDesc, imgtitle,phtodesc;
ImageView signimg, frntimg, bacimg, rgtimg, lftimg, atcimg, updimg1,
		updimg2, updimg3, fstimg, additionalimg, sgntick, frnttick,
		rgttick, lfttick, bactick, atctick, addittick;
ListAdapter adapter;
CheckBox cb1;String[] photocaption,fldelevationtype,fldelevationid;
private int id1;EditText upd_Ed;
String name = "FE ",photocaptiondesc,getidofselbtn;
String[] items = { "gallery", "camera" };
Dialog dialog;
int mychk=0;
Uri CapturedImageURI;
int t, b, quesid, rws, chk, selid, elev, f = 0, io = 0, iof = 0, ior = 0,
		id, delimagepos;
EditText eddesc1, eddesc2, edbrowse, eddesc3, ed1, firsttxt;
private static final int SELECT_PICTURE = 0;
private static final int CAPTURE_PICTURE_INTENT = 0;
String imgnum, imgord, j;
private static final int visibility = 0;
private static final String TAG = null;
public String[] str;
LinearLayout lnrlayout;
Button btnbrwse,btncamera ,btnupd, updat, del, changeimageorder;
View v1;
TextView txthdr;
int value, Count, isexist;
int ImgOrder = 0;
ListView lv1;
ProgressDialog pd,pd1;
View vv;
android.text.format.DateFormat df;
CharSequence cd, md;
String elevtype,elevid;
private Cursor cursor;
String[] SH_IM_Elevation;
private int columnIndex;
private int count;
private Bitmap[] thumbnails;
private boolean[] thumbnailsselection;
private String[] arrPath;
private ImageAdapter imageAdapter;
private ImageAdapter1 imageAdapter1;
public LinearLayout sdcardImages;
public String patharr[] = new String[8];
int maxclick, maxlevel = 0;
private int count1;
private Bitmap[] thumbnails1;
private String[] arrPath1, elevationdescription, arrimgord, arrpath,
		arrpathdesc;
private boolean[] thumbnailsselection1;
public int[] imageid;
public int[] SH_IM_ImageOrder;
private EditText[] et;
TextView policyholderinfo;
TextView tvstatus[];
ImageView elevationimage[];
Spinner spnelevation[];
ProgressDialog pd2;


Map<String, String[]> elevationcaption_map = new LinkedHashMap<String, String[]>();
Dialog dialog1;

/**used for the image folder wise  concept **/
Map<String, TextView> map1 = new LinkedHashMap<String, TextView>();
private List<String[]> fileList = new ArrayList<String[]>(); static File files2; static  
String[] pieces1=null,pieces2=null,pieces3=null;static ListView lv;File file;TextView temp_st,textview1,titlehead,chaild_titleheade;
private List<String> item = null;int backclick_FI=0;TextView pathofimage;
int ischk=0;
ImageView imgview2;Button backbtn_FI;CheckBox chkbox[];String path="";
LinearLayout lnrlayout1_FI,lnrlayout2_FI;TextView tvstatus_FI[],tvstatusparent[];ImageView imgview[],imgview1[];
File[] matchingFiles;String caps,finaltext="";
String norecords="0";int cvrtstr1;String repidofselbtn1;
String selectedtestcaption[]=null;
int maximumindb=0,selectedcount=0;
File listFile[];String testing = "";
static String[] mFiles=null;File[] listOfFiles;
File images;
//private ImageAdapter imageAdapter;
Button selectBtn;


/**used for the image folder wise  concept **/

	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        // get the type of the photo 
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				try {
					cf.ph_type=extras.getInt("type");
					elev=cf.ph_type-50;
					cf.selectedhomeid=extras.getString("homeid");
					cf.onlinspectionid = extras.getString("InspectionType");
				    cf.onlstatus = extras.getString("status");
				}
				catch (Exception e) {
					// TODO: handle exception
					
				}
			}
			setContentView(R.layout.photos);
			cf.getDeviceDimensions();
			LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
			 mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(),5, 0,cf));
	       
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.generalsubmenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(),cf.ph_type, 1,cf));
		 	ScrollView scr = (ScrollView)findViewById(R.id.scr);
		    scr.setMinimumHeight(cf.ht);
			cf.getInspectorId();
	 		cf.getinspectiondate();
	 		cf.Create_Table(16);
	 		cf.Create_Table(15);
	 		
	 		edbrowse = (EditText) findViewById(R.id.pathtxt);
	 		edbrowse.setVisibility(vv.GONE);
	 		btnbrwse = (Button) findViewById(R.id.browsetxt);
	 		btncamera = (Button) findViewById(R.id.browsecamera);
	 		
	 		btnupd = (Button) findViewById(R.id.updtxt);
	 		changeimageorder = (Button) findViewById(R.id.changeimageorder);
	 		lnrlayout = (LinearLayout) this.findViewById(R.id.linscrollview);
	 		txthdr = (TextView) findViewById(R.id.titlehdr);
	 		changeimageorder.setOnClickListener(new OnClickListener() {

	 			public void onClick(View v) {
	 				subview();
	 			}
	 		});
	 		showimages();
	 }
	 
     	private void commonelevation(String imgor,int spin) {
		// TODO Auto-generated method stub
     		int imgorder=0;
     		try
     		{
     			imgorder=Integer.parseInt(imgor);
     		}
     		catch(Exception e)
     		{
     			
     		}
     		Cursor c1;
     		if(imgorder>=9 && elev==1)
     		{
     			c1=cf.SelectTablefunction(cf.Photo_caption, " WHERE SH_IMP_Elevation='"+elev+"' and SH_IMP_ImageOrder='"+9+"'"); // To set the caption for front elevation 
     		}
     		else
     		{
     			c1=cf.SelectTablefunction(cf.Photo_caption, " WHERE SH_IMP_Elevation='"+elev+"' and SH_IMP_ImageOrder='"+imgor+"'");
     		}String[] temp;
		if(c1.getCount()>0)
		{
			temp=new String[c1.getCount()+2];
			temp[0]="Select";
			temp[1]="ADD PHOTO CAPTION";
			int i=2;
			c1.moveToFirst();
			do
			{
				temp[i]=cf.decode(c1.getString(c1.getColumnIndex("SH_IMP_Caption")));	
				i++;
			}while(c1.moveToNext());
		}
		else
		{
			temp=new String[2];
			temp[0]="Select";
			temp[1]="ADD PHOTO CAPTION";
		}
		elevationcaption_map.put("spiner"+spin, temp);
	}

		public void showimages() {
			
			if(elev==1)
			{
				name="EP";
			}
			else if(elev==2)
			{
				name="R&A";
			}
			else if(elev==3)
			{
				name="G&A";
			}
			else if(elev==4)
			{
				name="I&P";
			}
			else if(elev==5)
			{
				name="A&P";
			}
	 		sgnpath = "";
	 		pathdesc = "";
	 		try {
	 			Cursor c11 = cf.sh_db.rawQuery("SELECT * FROM " + cf.ImageTable
	 					+ " WHERE SH_IM_SRID='" + cf.selectedhomeid + "' and SH_IM_Elevation='" + elev
	 					+ "' and SH_IM_Delflag=0 order by SH_IM_ImageOrder", null);
	 			maxlevel = rws = c11.getCount();
	 			
	 			int rem = 4 - rws;
	 			arrpath = new String[rws];
	 			arrpathdesc = new String[rws];
	 			arrimgord = new String[rws];
	 			String source = "<b><font color=#000000> Number of uploaded images : "
	 					+ "</font><font color=#DAA520>"
	 					+ rws
	 					+ "</font><font color=#000000> , Remaining : </font><font color=#DAA520>"
						+ rem + "</font></b>";
	 			txthdr.setText(Html.fromHtml(source));
	 			if (rws == 0) {

	 				lnrlayout.removeAllViews();
	 				firsttxt.setVisibility(v1.GONE);
	 				fstimg.setVisibility(v1.GONE);
	 				updat.setVisibility(v1.GONE);
	 				del.setVisibility(v1.GONE);
	 			} else {
	 				int Column1 = c11.getColumnIndex("SH_IM_ImageName");
	 				int Column2 = c11.getColumnIndex("SH_IM_Description");
	 				int Column3 = c11.getColumnIndex("SH_IM_ImageOrder");
	 				c11.moveToFirst();
	 				if (c11 != null) {
	 					int i = 0;
	 					do {
	 							commonelevation(cf.decode(c11.getString(Column3)),i); // set the cation form the database 
	 							arrpath[i] = cf.decode(c11.getString(Column1));
	 							arrpathdesc[i] = cf.decode(c11.getString(Column2));
	 							arrimgord[i] = cf.decode(c11.getString(Column3));
	 							
	 							ColorDrawable sage = new ColorDrawable(this.getResources().getColor(R.color.sage));								
	 						i++;
	 					} while (c11.moveToNext());
	 					dynamicview();
	 					
	 				}
	 			}
	 		} catch (Exception e) {
	 			Log.i(TAG, "error correct= " + e.getMessage());
	 		}
	 	}	
	 	private void dynamicview() throws FileNotFoundException {
	 		// TODO Auto-generated method stub
	 		lnrlayout.removeAllViews();
	 		ScrollView sv = new ScrollView(this);
	 		lnrlayout.addView(sv);

	 		LinearLayout l1 = new LinearLayout(this);
	 		l1.setOrientation(LinearLayout.VERTICAL);
	 		sv.addView(l1);
	 		//l1.setMinimumWidth(925);

	 		tvstatus = new TextView[arrpath.length];
	 		elevationimage = new ImageView[arrpath.length];
	 		spnelevation = new Spinner[arrpath.length];

	 		for (int i = 0; i < arrpath.length; i++) {

	 			LinearLayout l2 = new LinearLayout(this);
	 			l2.setOrientation(LinearLayout.HORIZONTAL);
	 			l1.addView(l2);

	 			LinearLayout lchkbox = new LinearLayout(this);
	 			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(175, ViewGroup.LayoutParams.WRAP_CONTENT);
	 			paramschk.topMargin = 0;
	 			paramschk.leftMargin = 10;
	 			l2.addView(lchkbox);

	 			tvstatus[i] = new TextView(this);
	 			tvstatus[i].setMinimumWidth(175);
	 			tvstatus[i].setMaxWidth(175);
	 			tvstatus[i].setText(arrpathdesc[i]);
	 			tvstatus[i].setTextColor(Color.BLACK);
	 			tvstatus[i].setTag("imagechange" + i);
	 			tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
	 			lchkbox.addView(tvstatus[i], paramschk);

	 			
	 			BitmapFactory.Options o = new BitmapFactory.Options();
	 			o.inJustDecodeBounds = true;
	 			try {
	 				BitmapFactory.decodeStream(new FileInputStream(arrpath[i]),
	 						null, o);
	 				j = "1";
	 			} catch (FileNotFoundException e) {
	 				j = "2";
	 			}
	 			if (j.equals("1")) {
	 				final int REQUIRED_SIZE = 100;
	 				int width_tmp = o.outWidth, height_tmp = o.outHeight;
	 				int scale = 1;
	 				while (true) {
	 					if (width_tmp / 2 < REQUIRED_SIZE
	 							|| height_tmp / 2 < REQUIRED_SIZE)
	 						break;
	 					width_tmp /= 2;
	 					height_tmp /= 2;
	 					scale *= 2;
	 				}
	 				
	 				// Decode with inSampleSize
	 				BitmapFactory.Options o2 = new BitmapFactory.Options();
	 				o2.inSampleSize = scale;
	 				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
	 						arrpath[i]), null, o2);
	 				BitmapDrawable bmd = new BitmapDrawable(bitmap);

	 				LinearLayout lelevimage = new LinearLayout(this);
	 				LinearLayout.LayoutParams paramselevimg = new LinearLayout.LayoutParams(
	 						100, 50);
	 				paramselevimg.topMargin = 5;
	 				paramselevimg.leftMargin = 20;
	 				l2.addView(lelevimage);
	 				
	 				elevationimage[i] = new ImageView(this);
	 				elevationimage[i].setMinimumWidth(75);
	 				elevationimage[i].setMaxWidth(75);
	 				elevationimage[i].setMinimumHeight(75);
	 				elevationimage[i].setMaxHeight(75);
	 				elevationimage[i].setImageDrawable(bmd);
	 				elevationimage[i].setTag("imagechange" + i);
	 				lelevimage.addView(elevationimage[i], paramselevimg);
	 			}

	 			LinearLayout lspinner = new LinearLayout(this);
	 			LinearLayout.LayoutParams paramsspinn = new LinearLayout.LayoutParams(200, LayoutParams.WRAP_CONTENT);
	 			paramsspinn.topMargin = 5;
	 			paramsspinn.leftMargin = 20;
	 			l2.addView(lspinner);
	 			tvstatus[i].setOnClickListener(new View.OnClickListener() {

	 				public void onClick(final View v) {
	 					
	 					String getidofselbtn = v.getTag().toString();
	 					final String repidofselbtn = getidofselbtn.replace(
	 							"imagechange", "");
	 					final int cvrtstr = Integer.parseInt(repidofselbtn);

	 					String selpath = arrpath[cvrtstr];
	 					String seltxt = arrpathdesc[cvrtstr];

	 					Cursor c11 = cf.sh_db.rawQuery("SELECT SH_IM_ImageOrder FROM "
	 							+ cf.ImageTable + " WHERE SH_IM_SRID='" + cf.selectedhomeid
	 							+ "' and SH_IM_Elevation='" + elev + "' and SH_IM_ImageName='"
	 							+ cf.encode(arrpath[cvrtstr]) + "'",
	 							null);
	 					c11.moveToFirst();
	 					imgnum = c11.getString(c11.getColumnIndex("SH_IM_ImageOrder"));
	 					String a;
	 					chk = 1;
	 					//dispfirstimg(cvrtstr);
	 					dispfirstimg(cvrtstr,seltxt);

	 				}
	 			});
	 			elevationimage[i].setOnClickListener(new View.OnClickListener() {
	 				public void onClick(final View v) {
	 					
	 					String getidofselbtn = v.getTag().toString();
	 					final String repidofselbtn = getidofselbtn.replace(
	 							"imagechange", "");
	 					final int cvrtstr = Integer.parseInt(repidofselbtn);

	 					String selpath = arrpath[cvrtstr];
	 					String seltxt = arrpathdesc[cvrtstr];

	 					Cursor c11 = cf.sh_db.rawQuery("SELECT SH_IM_ImageOrder FROM "
	 							+ cf.ImageTable + " WHERE SH_IM_SRID='" + cf.selectedhomeid
	 							+ "' and SH_IM_Elevation='" + elev + "' and SH_IM_ImageName='"
	 							+ cf.encode(arrpath[cvrtstr]) + "'",
	 							null);
	 					c11.moveToFirst();
	 					imgnum = c11.getString(c11.getColumnIndex("SH_IM_ImageOrder"));
	 					String a;
	 					chk = 1;
	 					//dispfirstimg(cvrtstr);
	 					dispfirstimg(cvrtstr,seltxt);

	 				}
	 			});
	 			int n=i+1;
	 			
	 			SH_IM_Elevation = elevationcaption_map.get("spiner"+i);
	 			spnelevation[i] = new Spinner(this);
	 			spnelevation[i].setId(i);
	 			ArrayAdapter adapter2 = new ArrayAdapter(photos.this,android.R.layout.simple_spinner_item, SH_IM_Elevation);
	 			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 			spnelevation[i].setAdapter(adapter2);
	 			lspinner.addView(spnelevation[i], paramsspinn);
	 			spnelevation[i].setOnItemSelectedListener(new MyOnItemSelectedListener1(i));
	 			

	 		}
	 	}
		private Handler finishedHandler = new Handler() {
	 	    @Override 
	 	    public void handleMessage(Message msg) {
	 	    	pd1.dismiss();
	 	    }
	 	};
		protected Button backbtn;
		protected Button cancelBtn;
		protected Button selectImgBtn;




	 

	 	public void clicker(View v) {
	 		switch (v.getId()) {
	 		
	 		case R.id.browsetxt:
	 			Cursor c11 = cf.sh_db.rawQuery("SELECT * FROM " + cf.ImageTable
	 					+ " WHERE SH_IM_SRID='" + cf.selectedhomeid + "' and SH_IM_Elevation='" + elev
	 					+ "' and SH_IM_Delflag=0  order by SH_IM_ImageOrder", null);
	 			int chkrws = c11.getCount();
	 			maximumindb=chkrws;
	 			if(maximumindb>=4)
	 			{
	 				cf.ShowToast("You can upload only 4 images", 1);
	 			}else
	 			{
	 				t=0;
	 				 String source = "<b><font color=#00FF33>Loading Images. Please wait..."+ "</font></b>";
	 				 pd = ProgressDialog.show(photos.this, "",
	 					Html.fromHtml(source), true);
	 				 Thread thread = new Thread(photos.this);
	 				 thread.start();
	 				
	 			}
	 			break;
	 		 		     	
	 		case R.id.browsecamera:
	 			Cursor c12 = cf.sh_db.rawQuery("SELECT * FROM " + cf.ImageTable
	 					+ " WHERE SH_IM_SRID='" + cf.selectedhomeid + "' and SH_IM_Elevation='" + elev
	 					+ "' and SH_IM_Delflag=0  order by SH_IM_ImageOrder", null);
	 			int chkrws1 = c12.getCount();
	 			if(chkrws1>=4)
	 			{
	 				cf.ShowToast("You can upload only 4 images", 1);
	 			}else
	 			{
	 				t = 1;
	 				startCameraActivity();
	 			}
	 			break;


	 		case R.id.update:
	 			updstr = firsttxt.getText().toString();
	 			
	 			try {
	 				if (f == 1) {
	 					
	 					cf.sh_db.execSQL("UPDATE " + cf.ImageTable
	 							+ " SET SH_IM_ImageName='"
	 							+ cf.encode(selectedImagePath)
	 							+ "',SH_IM_Description='"
	 							+ cf.encode(updstr) + "',SH_IM_Nameext='"
	 							+ cf.encode(picname)
	 							+ "',SH_IM_ModifiedOn='" + cf.encode(md.toString()) + "' WHERE SH_IM_SRID ='"
	 							+ cf.selectedhomeid + "' and SH_IM_Elevation='" + elev
	 							+ "' and SH_IM_ImageOrder='" + imgnum + "'");	
	 					
	 				} else {
	 					
	 					cf.sh_db.execSQL("UPDATE " + cf.ImageTable
	 							+ " SET SH_IM_Description='"
	 							+ cf.encode(updstr) + "',SH_IM_Nameext='"
	 							+ cf.encode(picname)
	 							+ "',SH_IM_ModifiedOn='" + md + "' WHERE SH_IM_SRID ='"
	 							+ cf.selectedhomeid + "' and SH_IM_Elevation='" + elev
	 							+ "' and SH_IM_ImageOrder='" + imgnum + "'");
	 					
	 					
	 					
	 				}
	 				cf.ShowToast("Updated sucessfully.",1);

	 				f = 0;
	 				
	 				showimages();
	 			} catch (Exception e) {
	 				System.out.println("erre " + e.getMessage());
	 			}
	 			break;

	 		case R.id.delete:
	 			AlertDialog.Builder builder = new AlertDialog.Builder(this);
	 			builder.setMessage("Are you sure, Do you want to delete the image?")
	 					.setCancelable(false)
	 					.setPositiveButton("Yes",
	 							new DialogInterface.OnClickListener() {
	 								public void onClick(DialogInterface dialog,
	 										int id) {
	 									try {
	 										Cursor or = cf.sh_db.rawQuery(
	 												"select SH_IM_ImageOrder from "
	 														+ cf.ImageTable
	 														+ " WHERE  SH_IM_SRID ='"
	 														+ cf.selectedhomeid
	 														+ "' and SH_IM_Elevation='"
	 														+ elev
	 														+ "' and SH_IM_ImageName='"
	 														+ cf.encode(arrpath[delimagepos])
	 														+ "'", null);
	 										or.moveToFirst();
	 										int image_order = or.getInt(or
	 												.getColumnIndex("SH_IM_ImageOrder"));

	 										cf.sh_db.execSQL("UPDATE "
	 												+ cf.ImageTable
	 												+ " SET SH_IM_Delflag=1,SH_IM_ImageOrder=10 WHERE SH_IM_SRID ='"
	 												+ cf.selectedhomeid
	 												+ "' and SH_IM_Elevation='"
	 												+ elev
	 												+ "' and SH_IM_ImageName='"
	 												+ cf.encode(arrpath[delimagepos])
	 												+ "'");
	 										if (image_order < 8 && image_order > 0) {
	 											for (int m = image_order; m <= 8; m++) {
	 												int k = m + 1;
	 												cf.sh_db.execSQL("UPDATE "
	 														+ cf.ImageTable
	 														+ " SET SH_IM_ImageOrder='"
	 														+ m + "' WHERE SH_IM_SRID ='"
	 														+ cf.selectedhomeid
	 														+ "' and SH_IM_Elevation='"
	 														+ elev
	 														+ "' and SH_IM_ImageOrder='"
	 														+ k + "'");

	 											}
	 										}
	 									} catch (Exception e) {
	 										System.out.println("exception e  " + e);
	 									}
	 									cf.ShowToast("Image has been deleted sucessfully.",1);
	 									
	 									chk = 0;
	 									try {
	 										Cursor c11 = cf.sh_db.rawQuery(
	 												"SELECT * FROM "
	 														+ cf.ImageTable
	 														+ " WHERE SH_IM_SRID='"
	 														+ cf.selectedhomeid
	 														+ "' and SH_IM_Elevation='"
	 														+ elev
	 														+ "' and SH_IM_Delflag=0 ",
	 												null);
	 										int delchkrws = c11.getCount();
	 										
	 										
	 									} catch (Exception e) {

	 									}
	 									showimages();
	 								}
	 							})
	 					.setNegativeButton("No",
	 							new DialogInterface.OnClickListener() {
	 								public void onClick(DialogInterface dialog,
	 										int id) {
	 									dialog.cancel();
	 								}
	 							});
	 			builder.show();
	 			break;
	 		case R.id.home:
	 			cf.gohome();
	 			break;
	 		case R.id.save:
	 			if (elev == 1) {
	 				fullname = "External photos";
	 				cf.ShowToast(fullname+ " has been saved sucessfully.",1);
	 				nxtintent();
	 				
	 			} else if (elev == 2) {
	 				fullname = "Roof and attic photos";
	 				if (rws != 0) {cf.ShowToast(fullname+ " has been saved sucessfully.",1);}
	 				nxtintent();
	 			} else if (elev == 3) {
	 				fullname = "Ground and adjoining images";
	 				if (rws != 0) {cf.ShowToast(fullname+ " has been saved sucessfully.",1);}
	 				nxtintent();
	 			} else if (elev == 4) {
	 				fullname = "Internal photos";
	 				if (rws != 0) {cf.ShowToast(fullname+ " has been saved sucessfully.",1);}
	 				nxtintent();
	 			} else if (elev == 5) {
	 				fullname = "Addional Images";
	 				if (rws != 0) {cf.ShowToast( fullname+ " has been saved sucessfully.",1);}
	 				nxtintent();
	 			}
	 			

	 			break;
	 		case R.id.changeimageorder:
	 			subview();
	 			break;
	 		
	 		}
	 	}

	 	private void nxtintent() {
	 		// TODO Auto-generated method stub
	 	
	 		if (elev == 1) {
	 		  Intent myintent =new Intent(photos.this,photos.class);
	 		  cf.putExtras(myintent);
	 		  myintent.putExtra("type", 52);
	 		 startActivity(myintent);
	 		} else if (elev == 2) {
	 			Intent myintent =new Intent(photos.this,photos.class);
		 		  cf.putExtras(myintent);
		 		 myintent.putExtra("type", 53);
		 		 startActivity(myintent);
	 		} else if (elev == 3) {
	 			Intent myintent =new Intent(photos.this,photos.class);
		 		  cf.putExtras(myintent);
		 		 myintent.putExtra("type", 54);
		 		startActivity(myintent);
	 		} else if (elev == 4) {
	 			Intent myintent =new Intent(photos.this,photos.class);
		 		  cf.putExtras(myintent);
		 		 myintent.putExtra("type", 55);
		 		startActivity(myintent);
	 		} else if (elev == 5) {
	 			Intent myintent =new Intent(photos.this,FeedbackDocuments.class);
		 		  cf.putExtras(myintent);
		 		 startActivity(myintent);
		 		 
	 		}
	 	}

	 	private String getImageDesc(int elev2, String homeid2) {
	 		// TODO Auto-generated method stub
	 		if (elev2 == 1) {
	 			imgtitle ="External photos";
	 		} else if (elev2 == 2) {
	 			imgtitle = "Roof and attic photos";
	 		} else if (elev2 == 3) {
	 			imgtitle = "Ground and adjoining images";
	 		} else if (elev2 == 4) {
	 			imgtitle = "Internal photos";
	 		} else if (elev2 == 5) {
	 			imgtitle = "Additional Photo";
	 		}
	 		Cursor c11 = cf.sh_db.rawQuery("SELECT * FROM " + cf.ImageTable
	 				+ " WHERE SH_IM_SRID='" + homeid2 + "' and SH_IM_Elevation='" + elev2
	 				+ "' and SH_IM_Delflag=0 order by SH_IM_CreatedOn DESC", null);
	 		int imgrws = c11.getCount();
	 		if (imgrws == 0) {
	 			ImgOrder = imgrws + 1;
	 		} else {
	 			ImgOrder = imgrws + 1;

	 		}
	 		ImgDesc = imgtitle + " SH_IM_Elevation " + ImgOrder;
	 		return ImgDesc;
	 	}

	 	public int getImageOrder(int Elevationtype, String SH_IM_SRID) {
	 		Cursor c11 = cf.sh_db.rawQuery("SELECT * FROM " + cf.ImageTable
	 				+ " WHERE SH_IM_SRID='" + SH_IM_SRID + "' and SH_IM_Elevation='" + Elevationtype
	 				+ "' and SH_IM_Delflag=0 order by SH_IM_CreatedOn DESC", null);
	 		int imgrws = c11.getCount();
	 		if (imgrws == 0) {
	 			ImgOrder = imgrws + 1;
	 		} else {
	 			ImgOrder = imgrws + 1;

	 		}
	 		return ImgOrder;
	 	}

	 	public void next() {
	 		if (elev == 1) {
		 		  Intent myintent =new Intent(photos.this,photos.class);
		 		  cf.putExtras(myintent);
		 		 myintent.putExtra("type", 52);
		 		startActivity(myintent);
		 		} else if (elev == 2) {
		 			Intent myintent =new Intent(photos.this,photos.class);
			 		  cf.putExtras(myintent);
			 		 myintent.putExtra("type", 53);
			 		startActivity(myintent);
		 		} else if (elev == 3) {
		 			Intent myintent =new Intent(photos.this,photos.class);
			 		  cf.putExtras(myintent);
			 		 myintent.putExtra("type", 54);
			 		startActivity(myintent);
		 		} else if (elev == 4) {
		 			Intent myintent =new Intent(photos.this,photos.class);
			 		  cf.putExtras(myintent);
			 		 myintent.putExtra("type", 55);
			 		startActivity(myintent);
		 		} else if (elev == 5) {
		 			Intent myintent =new Intent(photos.this,FeedbackDocuments.class);
			 		  cf.putExtras(myintent);
			 		 startActivity(myintent);
			 		 
		 		}
	 	}

	 	



	 	public void dispfirstimg(int selid,String photocaptions) {

	 		delimagepos = selid;
	 		phtodesc = photocaptions;
	 		
	 		String k;
	 		BitmapFactory.Options o = new BitmapFactory.Options();
	 		o.inJustDecodeBounds = true;

	 		final Dialog dialog1 = new Dialog(photos.this,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alert);
			
			/** get the help and update relative layou and set the visbilitys for the respective relative layout */
			LinearLayout Re=(LinearLayout) dialog1.findViewById(R.id.maintable);
			Re.setVisibility(View.GONE);
			LinearLayout Reup=(LinearLayout) dialog1.findViewById(R.id.updateimage);
			Reup.setVisibility(View.VISIBLE);
			/** get the help and update relative layou and set the visbilitys for the respective relative layout */
			
			ImageView upd_img = (ImageView) dialog1.findViewById(R.id.firstimg);
			 upd_Ed = (EditText) dialog1.findViewById(R.id.firsttxt);
			upd_Ed.setFocusable(false);
			Button btn_helpclose = (Button) dialog1.findViewById(R.id.imagehelpclose);
			Button btn_up = (Button) dialog1.findViewById(R.id.update);
			Button btn_del = (Button) dialog1.findViewById(R.id.delete);
			upd_Ed.setOnTouchListener(new Touch_Listener(1));
			// update and delete button function 
		    btn_up.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
					updstr = upd_Ed.getText().toString();
		 			
		 			try {
		 				if(!updstr.trim().equals("")){
		 					dialog1.setCancelable(true);
							dialog1.dismiss();
		 				if (f == 1) {
		 					
		 					cf.sh_db.execSQL("UPDATE " + cf.ImageTable
		 							+ " SET SH_IM_ImageName='"
		 							+ cf.encode(selectedImagePath)
		 							+ "',SH_IM_Description='"
		 							+ cf.encode(updstr) + "',SH_IM_Nameext='"
		 							+ cf.encode(picname)
		 							+ "',SH_IM_ModifiedOn='" + cf.encode(md.toString()) + "' WHERE SH_IM_SRID ='"
		 							+ cf.selectedhomeid + "' and SH_IM_Elevation='" + elev
		 							+ "' and SH_IM_ImageOrder='" + imgnum + "'");	
		 					
		 				} else {
		 					
		 					cf.sh_db.execSQL("UPDATE " + cf.ImageTable
		 							+ " SET SH_IM_Description='"
		 							+ cf.encode(updstr) + "',SH_IM_Nameext='"
		 							+ cf.encode(picname)
		 							+ "',SH_IM_ModifiedOn='" + md + "' WHERE SH_IM_SRID ='"
		 							+ cf.selectedhomeid + "' and SH_IM_Elevation='" + elev
		 							+ "' and SH_IM_ImageOrder='" + imgnum + "'");
		 					
		 					
		 					
		 				}
		 				cf.ShowToast("Updated sucessfully.",1);

		 				f = 0;
		 				
		 				showimages();}
		 				else
		 				{
		 					cf.ShowToast("Please enter the caption.",1);
		 				}
		 			} catch (Exception e) {
		 				System.out.println("erre " + e.getMessage());
		 			}
					
				}
				
			});
		    btn_del.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					AlertDialog.Builder builder = new AlertDialog.Builder(photos.this);
		 			builder.setMessage("Are you sure, Do you want to delete the image?")
		 					.setCancelable(false)
		 					.setPositiveButton("Yes",
		 							new DialogInterface.OnClickListener() {
		 								public void onClick(DialogInterface dialog,
		 										int id) {
		 									try {
		 										Cursor or = cf.sh_db.rawQuery(
		 												"select SH_IM_ImageOrder from "
		 														+ cf.ImageTable
		 														+ " WHERE  SH_IM_SRID ='"
		 														+ cf.selectedhomeid
		 														+ "' and SH_IM_Elevation='"
		 														+ elev
		 														+ "' and SH_IM_ImageName='"
		 														+ cf.encode(arrpath[delimagepos])
		 														+ "'", null);
		 										or.moveToFirst();
		 										int image_order = or.getInt(or
		 												.getColumnIndex("SH_IM_ImageOrder"));

		 										cf.sh_db.execSQL("DELETE FROM "
		 												+ cf.ImageTable
		 												+ " WHERE SH_IM_SRID ='"
		 												+ cf.selectedhomeid
		 												+ "' and SH_IM_Elevation='"
		 												+ elev
		 												+ "' and SH_IM_ImageName='"
		 												+ cf.encode(arrpath[delimagepos])
		 												+ "'");
		 										
		 										// Get the total count of images in the table  
		 									       Cursor c3=cf.SelectTablefunction(cf.ImageTable," WHERE SH_IM_SRID ='"
		 												+ cf.selectedhomeid
		 												+ "' and SH_IM_Elevation='"
		 												+ elev
		 												+ "'" );
		 											for (int m = image_order; m <= c3.getCount(); m++) {
		 												int k = m + 1;
		 												cf.sh_db.execSQL("UPDATE "
		 														+ cf.ImageTable
		 														+ " SET SH_IM_ImageOrder='"
		 														+ m + "' WHERE SH_IM_SRID ='"
		 														+ cf.selectedhomeid
		 														+ "' and SH_IM_Elevation='"
		 														+ elev
		 														+ "' and SH_IM_ImageOrder='"
		 														+ k + "'");

		 											}
		 										
		 									} catch (Exception e) {
		 										System.out.println("exception e  " + e);
		 									}
		 									cf.ShowToast("Image has been deleted sucessfully.",1);
		 									
		 									chk = 0;
		 									try {
		 										Cursor c11 = cf.sh_db.rawQuery(
		 												"SELECT * FROM "
		 														+ cf.ImageTable
		 														+ " WHERE SH_IM_SRID='"
		 														+ cf.selectedhomeid
		 														+ "' and SH_IM_Elevation='"
		 														+ elev
		 														+ "' and SH_IM_Delflag=0 ",
		 												null);
		 										int delchkrws = c11.getCount();
		 										
		 										
		 									} catch (Exception e) {

		 									}
		 									showimages();
		 								}
		 							})
		 					.setNegativeButton("No",
		 							new DialogInterface.OnClickListener() {
		 								public void onClick(DialogInterface dialog,
		 										int id) {
		 									
		 									dialog.cancel();
		 								}
		 							});
		 			builder.show();
				}
			});
            btn_helpclose.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					 dialog1.setCancelable(true);
					 dialog1.dismiss();
				}
			});
			
            // update and delete  button function  ends
		    
		    
	 		try {
	 			if (chk == 1) {
	 				BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]),
	 						null, o);
	 			} else {
	 				BitmapFactory.decodeStream(new FileInputStream(arrpath[0]),
	 						null, o);
	 			}
	 			k = "1";

	 		} catch (FileNotFoundException e) {
	 			k = "2";

	 		}
	 		if (k.equals("1")) {
	 			final int REQUIRED_SIZE = 400;
	 			int width_tmp = o.outWidth, height_tmp = o.outHeight;
	 			int scale = 1;
	 			while (true) {
	 				if (width_tmp / 2 < REQUIRED_SIZE
	 						|| height_tmp / 2 < REQUIRED_SIZE)
	 					break;
	 				width_tmp /= 2;
	 				height_tmp /= 2;
	 				scale *= 2;
	 			}

	 			// Decode with inSampleSize
	 			BitmapFactory.Options o2 = new BitmapFactory.Options();
	 			o2.inSampleSize = scale;
	 			Bitmap bitmap = null;
	 			try {
	 				Cursor c11 = cf.sh_db.rawQuery(
	 						"SELECT * FROM " + cf.ImageTable + " WHERE SH_IM_SRID='"
	 								+ cf.selectedhomeid + "' and SH_IM_Elevation='" + elev
	 								+ "' and SH_IM_ImageName='"
	 								+ cf.encode(arrpath[selid])
	 								+ "' and SH_IM_Delflag=0", null);
	 				c11.moveToFirst();
	 				String SH_IM_Description = c11.getString(c11
	 						.getColumnIndex("SH_IM_Description"));

	 				if (chk == 1) {
	 					bitmap = BitmapFactory.decodeStream(new FileInputStream(
	 							arrpath[selid]), null, o2);
	 					upd_Ed.setText(cf.decode(SH_IM_Description));

	 				} else {
	 					bitmap = BitmapFactory.decodeStream(new FileInputStream(
	 							arrpath[0]), null, o2);
	 					upd_Ed.setText(cf.decode(arrpathdesc[0]));
	 				}
	 			} catch (FileNotFoundException e) {
	 				e.printStackTrace();
	 			}
	 			BitmapDrawable bmd = new BitmapDrawable(bitmap);
	 			upd_img.setImageDrawable(bmd);
	 			
	 			
	 			dialog1.setCancelable(false);
				dialog1.show();
	 		}
	 		else
	 		{
	 			dialog1.setCancelable(false);
				dialog1.show();
	 		}

	 	}

	
	 	class Touch_Listener implements OnTouchListener
		{
			   public int type;
			   Touch_Listener(int type)
				{
					this.type=type;
					
				}
			    @Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
			    	if(this.type==1)
					{
			    		cf.setFocus(upd_Ed);
					}
			    	
					return false;
				}
			 
		}
	 	private class watcher implements TextWatcher {

	 		int inde;

	 		public watcher(int i) {
	 			// TODO Auto-generated constructor stub
	 			this.inde = i;
	 		}

	 		public void onTextChanged(CharSequence s, int start, int before,
	 				int count) {
	 			// TODO Auto-generated method stub

	 		}

	 		public void beforeTextChanged(CharSequence s, int start, int count,
	 				int after) {
	 			// TODO Auto-generated method stub

	 		}

	 		public void afterTextChanged(Editable s) {
	 			// TODO Auto-generated method stub
	 			str[this.inde] = s.toString();

	 		}
	 	};

	 	public class ImageAdapter1 extends BaseAdapter {
	 		private LayoutInflater mInflater;

	 		public ImageAdapter1() {
	 			mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	 		}

	 		public int getCount() {
	 			return count1;
	 		}

	 		public Object getItem(int position) {
	 			return position;
	 		}

	 		public long getItemId(int position) {
	 			return position;
	 		}

	 		public View getView(int position, View convertView, ViewGroup parent) {
	 			ViewHolder1 holder;
	 			if (convertView == null) {
	 				holder = new ViewHolder1();
	 				convertView = mInflater.inflate(R.layout.updateorder, null);
	 				holder.imageview = (ImageView) convertView
	 						.findViewById(R.id.thumbImage1);
	 				holder.edit = (EditText) convertView
	 						.findViewById(R.id.itemedit);
	 				holder.r = (RelativeLayout) convertView.findViewById(R.id.Rid);

	 				convertView.setTag(holder);
	 				holder.edit.setId(position);
	 				et[position] = holder.edit;

	 				et[position].addTextChangedListener(new watcher(position));
	 				et[position].setText(String.valueOf(SH_IM_ImageOrder[position]));
	 				holder.imageview.setId(position);
	 				holder.r.setId(position);
	 				Bitmap b = thumbnails1[position];
	 				holder.imageview.setImageBitmap(b);
	 				holder.id = position;
	 			} else {

	 				holder = (ViewHolder1) convertView.getTag();

	 			}

	 			return convertView;

	 		}

	 	}

	 	class ViewHolder1 {
	 		ImageView imageview;
	 		EditText edit;
	 		RelativeLayout r;
	 		int id;
	 		int k = 0;

	 	}

	 	public class ImageAdapter extends BaseAdapter {
	 		GridView gi;
	 		ImageView immain;
	 		Button s, c;
	 		LayoutInflater mInflater;
	 		TextView edt;

	 		public ImageAdapter(GridView imagegrid, ImageView immain,
	 				Button selectBtn, Button cancelBtn, TextView tv1) {
	 			c = cancelBtn;
	 			s = selectBtn;
	 			gi = imagegrid;
	 			this.immain = immain;
	 			edt = tv1;
	 			mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	 		}

	 		public int getCount() {
	 			return count;
	 		}

	 		public Object getItem(int position) {
	 			return position;
	 		}

	 		public long getItemId(int position) {
	 			return position;
	 		}

	 		public View getView(int position, View convertView, ViewGroup parent) {
	 			final ViewHolder holder;
	 			if (convertView == null) {
	 				holder = new ViewHolder();
	 				convertView = mInflater.inflate(R.layout.galleryitem, null);
	 				holder.imageview = (ImageView) convertView
	 						.findViewById(R.id.thumbImage);
	 				holder.checkbox = (CheckBox) convertView
	 						.findViewById(R.id.itemCheckBox);
	 				holder.r = (RelativeLayout) convertView.findViewById(R.id.Rid);

	 				convertView.setTag(holder);
	 			} else {
	 				holder = (ViewHolder) convertView.getTag();
	 			}
	 			holder.checkbox.setId(position);
	 			holder.imageview.setId(position);
	 			holder.r.setId(position);

	 			holder.checkbox.setOnClickListener(new OnClickListener() {

	 				public void onClick(View v) {
	 					// TODO Auto-generated method stub
	 					CheckBox cb = (CheckBox) v;
	 					int id = cb.getId();

	 					if (thumbnailsselection[id]) {
	 						cb.setChecked(false);
	 						try {
	 							thumbnailsselection[id] = false;

	 							String temp[] = new String[patharr.length];

	 							int j = 0;
	 							for (int i = 0; i < patharr.length; i++) {
	 								if (patharr[i] != null) {
	 									if (arrPath[id] != patharr[i]) {

	 										temp[i] = patharr[j];

	 										j++;
	 									}
	 								}
	 							}
	 							
	 							patharr = null;
	 							
	 							patharr = temp.clone();
	 							maxclick--;
	 							maxlevel--;

	 						} catch (Exception e) {
	 							System.out.println("this shows " + e);
	 						}
	 					} else {
	 						

	 							try {

	 								boolean bu = cf.common(arrPath[id]);
	 								if (bu) {
	 									cb.setChecked(true);
	 									String[] bits = arrPath[id].split("/");
	 									picname = bits[bits.length - 1];
	 									Cursor c11 = cf.sh_db
	 											.rawQuery(
	 													"SELECT * FROM "
	 															+ cf.ImageTable
	 															+ " WHERE SH_IM_SRID='"
	 															+ cf.selectedhomeid
	 															+ "' and SH_IM_Elevation='"
	 															+ elev
	 															+ "' and SH_IM_Delflag=0 and SH_IM_Nameext='"
	 															+ cf.encode(picname)
	 															+ "' order by SH_IM_ImageOrder",
	 													null);
	 									isexist = c11.getCount();
	 									if (isexist == 0) {
	 										thumbnailsselection[id] = true;
	 										// We just increase the length of the array
	 										String[] tmp= new String[patharr.length];
	 										tmp=patharr;
	 										patharr=null;
	 										patharr=new String[tmp.length+1];
	 										for(int i=0;i<=tmp.length;i++)
	 										{
	 											if(i==tmp.length)
	 											{
	 												patharr[i]= arrPath[id];
	 											}
	 											else
	 											{
	 											patharr[i]=tmp[i];
	 											}
	 										}
	 											
	 										maxclick++;
	 										maxlevel++;

	 									} else {
	 										cb.setChecked(false);
	 										cf.ShowToast("Selected Image has been uploaded already. Please select another Image.",1);

	 									}
	 								} else {
	 									cb.setChecked(false);
	 									cf.ShowToast("Please select image less than 2MB",1);

	 								}

	 							} catch (Exception e) {
	 								System.out.println("rror in else " + e);
	 							}
	 						
	 					}

	 				}
	 			});
	 			holder.imageview.setOnClickListener(new OnClickListener() {
	 				public void onClick(View v) {
	 					// TODO Auto-generated method stub
	 					try {
	 						ImageView im = (ImageView) v;
	 						int id = im.getId();
	 						id1 = im.getId();
	 						paht = arrPath[id];
	 						cb1 = (CheckBox) holder.checkbox.findViewById(id);
	 						Uri uri = Uri.parse(paht);
	 						immain.setImageURI(uri);
	 						String[] bits = paht.split("/");
	 						picname = bits[bits.length - 1];
	 						dialog.setTitle(picname);
	 						edt.setTextColor(Color.WHITE);
	 						s.setText("Select");
	 						c.setText("Cancel");
	 						edt.setVisibility(View.VISIBLE);
	 						edt.setText(paht);
	 						immain.setVisibility(View.VISIBLE);
	 						gi.setVisibility(View.GONE);

	 					} catch (Exception e) {
	 						System.out.println("proiblem" + e);
	 					}

	 				}

	 			});
	 			Bitmap b = thumbnails[position];
	 			holder.imageview.setImageBitmap(b);
	 			holder.checkbox.setChecked(thumbnailsselection[position]);
	 			holder.id = position;
	 			if (b == null) {

	 				holder.r.setVisibility(View.GONE);

	 			} else {
	 				holder.r.setVisibility(View.VISIBLE);
	 			}
	 			return convertView;

	 		}

	 	}

	 	class ViewHolder {

	 		ImageView imageview;
	 		CheckBox checkbox;
	 		RelativeLayout r;
	 		int id;
	 		GridView gi;
	 		ImageView immain;
	 		Button s, c;
	 		TextView edt;
	 	}

	 	protected void startCameraActivity() {
	 		String fileName = "temp.jpg";
	 		ContentValues values = new ContentValues();
	 		values.put(MediaStore.Images.Media.TITLE, fileName);
	 		CapturedImageURI = getContentResolver().insert(
	 				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
	 		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	 		intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
	 		startActivityForResult(intent, CAPTURE_PICTURE_INTENT);

	 	}

	 	protected void pickfromgallery() {

	 		Intent intent = new Intent();
	 		intent.setType("image/*");
	 		intent.setAction(Intent.ACTION_GET_CONTENT);
	 		startActivityForResult(Intent.createChooser(intent, "Select Picture"),
	 				SELECT_PICTURE);
	 	}

	 	@Override
	 	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		if (t == 0) {
	 			if (resultCode == RESULT_OK) {
	 				if (requestCode == SELECT_PICTURE) {
	 					Uri selectedImageUri = data.getData();
	 					selectedImagePath = getPath(selectedImageUri);

	 					String[] bits = selectedImagePath.split("/");
	 					picname = bits[bits.length - 1];

	 				}
	 			} else {
	 				selectedImagePath = "";
	 				edbrowse.setText("");
	 			}
	 		} else if (t == 1) {
	 			switch (resultCode) {
	 			case 0:
	 				selectedImagePath = "";

	 				break;
	 			case -1:
	 				try {
	 					String[] projection = { MediaStore.Images.Media.DATA };
	 					Cursor cursor = managedQuery(CapturedImageURI, projection,
	 							null, null, null);
	 					int column_index_data = cursor
	 							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	 					cursor.moveToFirst();
	 					capturedImageFilePath = cursor.getString(column_index_data);
	 					selectedImagePath = capturedImageFilePath;
	 					String[] bits = selectedImagePath.split("/");
	 					picname = bits[bits.length - 1];
	 					Cursor c15 = cf.sh_db.rawQuery("SELECT * FROM "
	 							+ cf.ImageTable + " WHERE SH_IM_SRID='" + cf.selectedhomeid
	 							+ "' and SH_IM_Elevation='" + elev + "'", null);
	 					int count_tot = c15.getCount();
	 					ImgOrder = getImageOrder(elev, cf.selectedhomeid);
	 					try{cf.sh_db.execSQL("INSERT INTO "
	 							+ cf.ImageTable
	 							+ " (SH_IM_SRID,SH_IM_Ques_Id,SH_IM_Elevation,SH_IM_ImageName,SH_IM_Description,SH_IM_Nameext,SH_IM_CreatedOn,SH_IM_ModifiedOn,SH_IM_ImageOrder,SH_IM_Delflag)"
	 							+ " VALUES ('" + cf.selectedhomeid + "','" + quesid + "','"
	 							+ elev + "','"
	 							+ cf.encode(selectedImagePath) + "','"
	 							+ cf.encode(name + (count_tot + 1))
	 							+ "','" + cf.encode(picname) + "','"
	 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + ImgOrder + "','" + 0
	 							+ "')");
	 				}
	 					catch(Exception e)
	 					{
	 						System.out.println("e= "+e.getMessage());
	 					}
	 					showimages();
	 				} catch (Exception e) {
	 					System.out.println("my exception " + e);
	 				}
	 				
	 				break;

	 			}

	 		}

	 	}

	 	private void displayphoto() {

	 		try {
	 			BitmapFactory.Options o = new BitmapFactory.Options();
	 			o.inJustDecodeBounds = true;
	 			BitmapFactory.decodeStream(new FileInputStream(selectedImagePath),
	 					null, o);

	 			final int REQUIRED_SIZE = 100;
	 			int width_tmp = o.outWidth, height_tmp = o.outHeight;
	 			int scale = 1;
	 			while (true) {
	 				if (width_tmp / 2 < REQUIRED_SIZE
	 						|| height_tmp / 2 < REQUIRED_SIZE)
	 					break;
	 				width_tmp /= 2;
	 				height_tmp /= 2;
	 				scale *= 2;
	 			}
	 			BitmapFactory.Options o2 = new BitmapFactory.Options();
	 			o2.inSampleSize = scale;
	 			Bitmap bitmap1 = BitmapFactory.decodeStream(new FileInputStream(
	 					selectedImagePath), null, o2);
	 			BitmapDrawable bmd2 = new BitmapDrawable(bitmap1);
	 			fstimg.setImageDrawable(bmd2);

	 		} catch (FileNotFoundException e) {
	 		}

	 	}

	 	public String getPath(Uri uri) {
	 		String[] projection = { MediaStore.Images.Media.DATA };
	 		Cursor cursor = managedQuery(uri, projection, null, null, null);
	 		int column_index = cursor
	 				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	 		cursor.moveToFirst();
	 		return cursor.getString(column_index);
	 	}

	 

	 	public boolean onKeyDown(int keyCode, KeyEvent event) {
	 		// replaces the default 'Back' button action
	 		if (keyCode == KeyEvent.KEYCODE_BACK) {
	 			if(cf.strschdate.equals("")){
	 				cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
	 			}else{
	 			if (elev == 1) {
			 		  Intent myintent =new Intent(photos.this,photos_signature.class);
			 		  cf.putExtras(myintent);
			 		 
			 		startActivity(myintent);
			 		} else if (elev == 2) {
			 			Intent myintent =new Intent(photos.this,photos.class);
				 		  cf.putExtras(myintent);
				 		 myintent.putExtra("type", 51);
				 		startActivity(myintent);
			 		} else if (elev == 3) {
			 			Intent myintent =new Intent(photos.this,photos.class);
				 		  cf.putExtras(myintent);
				 		 myintent.putExtra("type", 52);
				 		startActivity(myintent);
			 		} else if (elev == 4) {
			 			Intent myintent =new Intent(photos.this,photos.class);
				 		  cf.putExtras(myintent);
				 		 myintent.putExtra("type", 53);
				 		startActivity(myintent);
			 		} else if (elev == 5) {
			 			Intent myintent =new Intent(photos.this,photos.class);
				 		  cf.putExtras(myintent);
				 		 myintent.putExtra("type", 54);
				 		 startActivity(myintent);
				 		 
			 		}
	 			}
	 			return true;
	 		}
	 		return super.onKeyDown(keyCode, event);
	 	}

	 
	 	public void run() {
	 		Looper.prepare();
	 		
	 		try{		// TODO Auto-generated method stub
	 			if(mychk==0){
	 			images = Environment.getExternalStorageDirectory(); 
	 		     walkdir(images);
	 			}
	 			else if(mychk==1)
	 			{
	 				
	 			}
	 			 handler.sendEmptyMessage(0);

	 		}
	 		catch(Exception e)
	 		{
	 			System.out.println("problem occure " + e.getMessage());
	 		}
	 			 
	 		
	 	}
	 	private Handler handler = new Handler() {
	         @Override
	         public void handleMessage(Message msg) {

		 			if(mychk==0){dialog = new Dialog(photos.this);
		 			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		 			dialog.setContentView(R.layout.selectimage);
		 	 		
		 	 		dialog.setCancelable(true);
		 	 		
		 	 		lnrlayout1_FI = (LinearLayout) dialog.findViewById(R.id.linscrollview2);
		 	 		lnrlayout2_FI = (LinearLayout) dialog.findViewById(R.id.linscrollview4);
		 	 		titlehead=(TextView) dialog.findViewById(R.id.titleheade);
		 	 		chaild_titleheade=(TextView) dialog.findViewById(R.id.chaild_titleheade);
		 	 		backbtn = (Button) dialog.findViewById(R.id.back);
		 	 		cancelBtn = (Button) dialog.findViewById(R.id.cancel);
		 	 		selectImgBtn = (Button) dialog.findViewById(R.id.savenext);
		 	 		pd.dismiss();
		 	 		if(dialog.isShowing())
		 	 		{
		 	 			dialog.setCancelable(true);
		 	 			dialog.cancel();
		 	 		}
		 	 			dialog.setCancelable(false);
		 	 			dialog.show();
		 		         dynamicimagesparent();
		 		    	 dynamicimageschild();
		 	 		
		 	 		cancelBtn.setOnClickListener(new OnClickListener() {
		 				public void onClick(View v) {				
		 					
		 					if(!"".equals(finaltext)){
		 						finaltext="";
		 					}
		 					selectedcount=0;
		 	 				selectedtestcaption=null;		
		 	 				maximumindb=0;
		 	 				pieces1=null;
		 	 				pieces2=null;
		 	 				pieces3=null;
		 	 				dialog.setCancelable(true);
		 	 				cf.ShowToast("Images has not been selected.", 1);
		 					dialog.cancel();
		 				}
		 	 		});
		 	 		backbtn.setOnClickListener(new OnClickListener() {
		 	 			public void onClick(View v) { 
		 	 				if(!titlehead.getText().toString().equals("/mnt/sdcard/")){
		 	 				backclick_FI=1;
		 				pieces1=null;pieces2=null;pieces3=null;
		 				
		 	 				String value = titlehead.getText().toString();
		 					String[] bits = value.split("/");
		 					String picname = bits[bits.length - 1];
		 					
		 					  
		 					
		 					if(value.equals("mnt/sdcard/"))
		 					{
		 						finaltext="";
		 						path = value;
		 						
		 					}
		 					else
		 					{
		 						String tempstr = value.substring(0,value.length()-1);
 			 					int bits1 = tempstr.lastIndexOf("/");
 			 					String pathofimage1=tempstr.substring(0,bits1+1);
 			 					path  = pathofimage1;
		 						 //path = value.replace(picname+"/", "");
		 					}
		 					
		 					titlehead.setText(path);
		 					File f = new File(path);
		 					String[] bits1 = path.split("/");
							String picname1 = bits1[bits1.length - 1];
							chaild_titleheade.setText(picname1); // set the chaild title
		 					walkdir(f);
		 					dynamicimagesparent();
		 					dynamicimageschild();
		 				}
		 	 				else
		 	 				{
		 	 					cf.ShowToast("You can not go back ", 1);
		 	 					
		 	 				}
		 	 			}
		 			});

		 	 		selectImgBtn.setOnClickListener(new OnClickListener() {
		 	 			public void onClick(View v) {
		 	 				if(selectedcount!=0)
		 	 				{
			 	 				for(int i=0;i<selectedcount;i++)
			 	 				{
			 	 					cf.sh_db.execSQL("INSERT INTO "
				 							+ cf.ImageTable
				 							+ " (SH_IM_SRID,SH_IM_Ques_Id,SH_IM_Elevation,SH_IM_ImageName,SH_IM_Description,SH_IM_Nameext,SH_IM_CreatedOn,SH_IM_ModifiedOn,SH_IM_ImageOrder,SH_IM_Delflag)"
				 							+ " VALUES ('" + cf.selectedhomeid + "','" + quesid + "','"
				 							+ elev + "','"
				 							+ cf.encode(selectedtestcaption[i]) + "','"
				 							+ cf.encode(name + (maximumindb+i+1))
				 							+ "','" + cf.encode(picname) + "','"
				 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + (maximumindb+1+i) + "','" + 0
				 							+ "')");
			 	 					
			 	 					
			 	 				}
			 	 				cf.ShowToast("Image saved successfully", 1);
			 	 				selectedcount=0;
			 	 				selectedtestcaption=null;		
			 	 				maximumindb=0;
			 	 				pieces1=null;
			 	 				pieces2=null;
			 	 				pieces3=null;
			 					if(!"".equals(finaltext)){
			 						finaltext="";
			 					}
			 					dialog.setCancelable(true);
			 					dialog.cancel();
			 					showimages();
		 	 				}else
		 	 				{
		 	 					cf.ShowToast("Please select atleast one image to upload .", 1);
		 	 				}
			 	 			}
			 	 		}); /**Select ends**/
		 			}
		 			else if(mychk==1)
		 			{
		 				    
							
		 			}
	         }
	 };
	
	private class MyOnItemSelectedListener1 implements OnItemSelectedListener {

		public final int spinnerid;

		MyOnItemSelectedListener1(int id) {
			this.spinnerid = id;
		}

		public void onItemSelected(AdapterView<?> parent, View view,final int pos,

				long id) {
			if (pos == 0) {
			} else {
				
				int maxLength=99;
				try {
					
					final int j = spinnerid + 1;
									
					photocaption =elevationcaption_map.get("spiner"+spinnerid);
					


					String[] bits = arrpath[spinnerid].split("/");
					picname = bits[bits.length - 1];
					
					
					if(photocaption[pos].equals("ADD PHOTO CAPTION"))
					{
						final AlertDialog.Builder alert = new AlertDialog.Builder(photos.this);
						alert.setTitle("ADD PHOTO CAPTION");
						final EditText input = new EditText(photos.this);
						alert.setView(input);
						InputFilter[] FilterArray = new InputFilter[1];  
						FilterArray[0] = new InputFilter.LengthFilter(maxLength);  
						input.setFilters(FilterArray);  
						input.setTextSize(14);
						input.setWidth(800);

						alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
							
						
							public void onClick(DialogInterface dialog, int whichButton) {
								String commentsadd = input.getText().toString();
								if (input.length() > 0 && ! commentsadd.trim().equals("")) {
									cf.sh_db.execSQL("INSERT INTO "
											+ cf.Photo_caption
											+ " (SH_IMP_INSP_ID,SH_IMP_Caption,SH_IMP_Elevation,SH_IMP_ImageOrder)"
											+ " VALUES ('"+cf.Insp_id+"','" + cf.encode(commentsadd) + "','" + elev + "','" + j + "')");
									
								//	commonfunction();
									showimages();
									cf.ShowToast("Photo Caption added successfully.",1);
									cf.hidekeyboard();
								           
									
								} else {
									cf.ShowToast("Please enter the caption.",1);
									spnelevation[spinnerid].setSelection(0);
								}
							}
						});

						alert.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									
									public void onClick(DialogInterface dialog, int whichButton) {
										dialog.cancel();
										spnelevation[spinnerid].setSelection(0);
										cf.hidekeyboard();
									}
								});

						alert.show();

					}
					else
					{
						final Dialog dialog = new Dialog(photos.this);
			            dialog.setContentView(R.layout.alertfront);
			           
			            dialog.setTitle("Choose Option");
			            dialog.setCancelable(true);
			            final EditText edit_desc = (EditText) dialog.findViewById(R.id.edittxtdesc);
			          
			            Button button_close = (Button) dialog.findViewById(R.id.Button01);
			    		final Button button_sel = (Button) dialog.findViewById(R.id.Button02);
			    		Button button_edit = (Button) dialog.findViewById(R.id.Button03);
			    		Button button_del = (Button) dialog.findViewById(R.id.Button04);
			    		final Button button_upd = (Button) dialog.findViewById(R.id.Button05);
			    		
			    		
			    		
			    		button_close.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            	spnelevation[spinnerid].setSelection(0);
				            	dialog.cancel();
				            }

					
						public void onNothingSelected(AdapterView<?> arg0) {
							// TODO Auto-generated method stub
							
						}
			    		});
			    		button_sel.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            	try{
				            		
				            						            	cf.sh_db.execSQL("UPDATE " + cf.ImageTable
										+ " SET SH_IM_Description='"
										+ cf.encode(photocaption[pos])
										+ "',SH_IM_Nameext='" + cf.encode(picname)
										+ "',SH_IM_ModifiedOn='" + cf.encode(cf.datewithtime.toString()) + "' WHERE SH_IM_SRID ='"
										+ cf.selectedhomeid + "' and SH_IM_Elevation='" + elev
										+ "' and SH_IM_ImageOrder='" + arrimgord[spinnerid]
										+ "'");	
				            		 	spnelevation[spinnerid].setSelection(0);
				            	}
				            	catch(Exception e)
				            	{
				            		cf.Error_LogFile_Creation("Problem in select the caption in the photos at the elevation "+elev+" error  ="+e.getMessage());
				            	}
				            
				            	tvstatus[spinnerid].setText(photocaption[pos]);
				            	dialog.cancel();
				            }});
				            
			    
			   			button_edit.setOnClickListener(new OnClickListener() {

			   				
				            public void onClick(View v) {

				            	button_sel.setVisibility(v1.GONE);	
				            	
				            	edit_desc.setVisibility(v1.VISIBLE);
				            	edit_desc.setText(photocaption[pos]);
				            	
				            	
				            	button_upd.setVisibility(v1.VISIBLE);

				            	button_upd.setOnClickListener(new OnClickListener() {

				            	//
									public void onClick(View v) {

										if(!edit_desc.getText().toString().trim().equals(""))
										{	
										cf.sh_db.execSQL("UPDATE " +cf.Photo_caption + " set SH_IMP_Caption = '"+cf.encode(edit_desc.getText().toString())+"' WHERE SH_IMP_Elevation ='" + elev + "' and SH_IMP_ImageOrder='"+arrimgord[j-1]+"' and SH_IMP_Caption='"+cf.encode(photocaption[pos])+"'");
						            	cf.hidekeyboard();
											showimages();
					   			        	cf.ShowToast("Photo Caption has been updated sucessfully.",1);
					   			        	 dialog.cancel();
										}
										else
										{
											cf.ShowToast("Please enter the caption.",1);
										}
						            }
					    		});
				            }
			    		});
			    		
			    		button_del.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            
				            	AlertDialog.Builder builder = new AlertDialog.Builder(photos.this);
				   			builder.setMessage("Are you sure, Do you want to delete?")
			   			       .setCancelable(false)
			   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			   			           public void onClick(DialogInterface dialog, int id) {
			   			        //	SH_IMP_INSP_ID,SH_IMP_Caption,SH_IMP_Elevation,SH_IMP_ImageOrder
			   			        	 cf.sh_db.execSQL("Delete From " +cf.Photo_caption+ "  WHERE  SH_IMP_Elevation ='" + elev + "' and SH_IMP_ImageOrder='"+arrimgord[j-1]+"' and SH_IMP_Caption='"+cf.encode(photocaption[pos])+"'");
			   			        	showimages();
			   			        	cf.ShowToast("Photo Caption has been deleted sucessfully.",1);
			   			        	
			   			           }
			   			       })
			   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
			   			           public void onClick(DialogInterface dialog, int id) {
			   			        	 spnelevation[spinnerid].setSelection(0);
			   			                dialog.cancel();
			   			           }
			   			       }); 
				            
				            builder.show();
				            dialog.cancel();
				            }
			    		});
			    		
				           
			   			 
			    		dialog.show();
					}
					
					
				} catch (Exception e) {
					System.out.println("e + " + e.getMessage());
				}

			}
		}
         
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
			
		}
		
	}

		private void walkdir(File images2) {
		 listFile = images2.listFiles();
		   if (listFile != null)
		   {
			   
			   
			   
			   pieces1=null;
			   pieces2= new String[listFile.length];
			   for (int i = 0; i < listFile.length; i++) {
				   
				   if (listFile[i].isDirectory()) {
	               
	               pieces2[i]="folder";
	               pieces1=dynamicarraysetting(listFile[i].getName(),pieces1);
	            } 
	    		 else  
	    		 {
	    			 	 if ((listFile[i].getName().endsWith(".jpg"))||(listFile[i].getName().endsWith(".jpeg"))||(listFile[i].getName().endsWith(".png"))||(listFile[i].getName().endsWith(".gif")||(listFile[i].getName().endsWith(".JPG"))
	            				 ||(listFile[i].getName().endsWith(".JPEG"))||(listFile[i].getName().endsWith(".PNG")))||(listFile[i].getName().endsWith(".gif"))||(listFile[i].getName().endsWith(".bmp"))){
	    	            	 
	    	            	 pieces2[i]="images";
	    	            pieces3=dynamicarraysetting(listFile[i].getName(),pieces3);
	            		 }
	    		 }
		   }
		 }
	    	 
	}

		private String[] dynamicarraysetting(String ErrorMsg,String[] pieces3) {
			// TODO Auto-generated method stub
			 try
			 {
			if(pieces3==null)
			{
				pieces3=new String[1];
				
				pieces3[0]=ErrorMsg;
			}
			else
			{
				
				String tmp[]=new String[pieces3.length+1];
				int i;
				for(i =0;i<pieces3.length;i++)
				{
					
					tmp[i]=pieces3[i];
				}
			
				tmp[tmp.length-1]=ErrorMsg;
				pieces3=null;
				pieces3=tmp.clone();
			}
			 }
			 catch(Exception e)
			 {
				 
			 }
			return pieces3;
		}

		private void dynamicimagesparent() {
			// TODO Auto-generated method stub
			lnrlayout1_FI.removeAllViews();
			
			if(finaltext.equals("")||finaltext.equals("null"))
			{			
				titlehead.setText("/mnt/sdcard/");
				chaild_titleheade.setText("sdcard");
			}
			else
			{
				
				if(backclick_FI==1)
				{
					titlehead.setText(path);backclick_FI=0;
				}
				else
				{
					titlehead.setText(finaltext);
				}

			}
			
			
			if(pieces1!=null)
			{
				tvstatusparent = new TextView[pieces1.length];
				imgview = new ImageView[pieces1.length];		
				
			
			for (int i = 0; i < pieces1.length; i++) {
				LinearLayout.LayoutParams paramschk5 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);	
				LinearLayout l2 = new LinearLayout(this);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				lnrlayout1_FI.addView(l2,paramschk5);

				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
						50, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.leftMargin = 10;
				paramschk.topMargin = 5;
				paramschk.bottomMargin = 15;
				l2.addView(lchkbox);

				imgview[i] = new ImageView(this);//imgview[i].setPadding(30, 0, 0, 0);
				imgview[i].setBackgroundResource(R.drawable.allfilesicon);
				
				lchkbox.addView(imgview[i], paramschk);

				LinearLayout ltextbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramstext = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramstext.topMargin = 5;
				paramstext.leftMargin = 15;
				l2.addView(ltextbox);
				
				tvstatusparent[i] = new TextView(this);
				tvstatusparent[i].setText(pieces1[i]);
				tvstatusparent[i].setTextColor(Color.BLACK);
				tvstatusparent[i].setMinimumWidth(100);
				map1.put("tvstatusparent" + i, tvstatusparent[i]);
				tvstatusparent[i].setTag("tvstatusparent" + i);
				imgview[i].setTag("tvstatusparent" + i);
				ltextbox.addView(tvstatusparent[i], paramstext);
				
				tvstatusparent[i].setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						pieces1=null;pieces3=null;pieces2=null;
						String source = "<b><font color=#00FF33>Loading Images. Please wait..."+ "</font></b>";
						pd2 = ProgressDialog.show(photos.this, "",
		 					Html.fromHtml(source), true);
						pd2.setCancelable(false);
						getidofselbtn = v.getTag().toString();
						String repidofselbtn = getidofselbtn.replace("tvstatusparent","");
						final int cvrtstr = Integer.parseInt(repidofselbtn) + 1;
						
						temp_st = map1.get(getidofselbtn);
						
						
						imagessub(cvrtstr, temp_st);
						pd2.setCancelable(true);
						pd2.dismiss();
		 				/* mychk=1;
		 				
		 				 Thread thread = new Thread(photos.this);
						thread.start();*/
		 				
					
					}
					
				});
				imgview[i].setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						pieces1=null;pieces3=null;pieces2=null;
						//pieces3=null;
						String source = "<b><font color=#00FF33>Loading Images. Please wait..."+ "</font></b>";
		 				 pd = ProgressDialog.show(photos.this, "",
		 					Html.fromHtml(source), true);
		 				 pd.setCancelable(false);
						String getidofselbtn = v.getTag().toString();
						String repidofselbtn = getidofselbtn.replace("tvstatusparent","");
						final int cvrtstr = Integer.parseInt(repidofselbtn) + 1;
						temp_st = map1.get(getidofselbtn);
						
						if(backclick_FI==1)
						{
						String[] bits = finaltext.split("/");
						String picname1 = bits[bits.length - 1];
						chaild_titleheade.setText(picname1); // set the chaild title 
						}
						else
						{
							String[] bits = path.split("/");
							String picname1 = bits[bits.length - 1];
							chaild_titleheade.setText(picname1); // set the chaild title 
						}
						imagessub(cvrtstr, temp_st);
						pd.setCancelable(true);
						 pd.dismiss();
					}
					
				});
			}
			}	
			else
			{
				LinearLayout l2 = new LinearLayout(this);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				lnrlayout1_FI.addView(l2);

				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.leftMargin = 15;
				l2.addView(lchkbox);
				
				
				TextView tvstatus1 = new TextView(this);
				tvstatus1.setText("No Sub Folders");
				tvstatus1.setTextColor(Color.BLACK);
				tvstatus1.setMinimumWidth(200);
				lchkbox.addView(tvstatus1,paramschk);
			}
		}
		private void dynamicimageschild() {
			
			lnrlayout2_FI.removeAllViews();
			if((pieces3==null))
			{
				LinearLayout l2 = new LinearLayout(this);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				lnrlayout2_FI.addView(l2);

				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.leftMargin = 15;
				l2.addView(lchkbox);
				
				
				TextView tvstatus1 = new TextView(this);
				tvstatus1.setText("No Records Found");
				tvstatus1.setTextColor(Color.BLACK);
				tvstatus1.setMinimumWidth(200);
				lchkbox.addView(tvstatus1,paramschk);
			}
			
			else
			{	
				tvstatus = new TextView[pieces3.length];
				imgview = new ImageView[pieces3.length];
				chkbox = new CheckBox[pieces3.length];
				imgview1 = new ImageView[pieces3.length];
				thumbnails1 = new Bitmap[pieces3.length];
			
				for (int i = 0; i < pieces3.length; i++) {
				
					LinearLayout.LayoutParams paramschk5 = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);	
					LinearLayout l2 = new LinearLayout(this);
					l2.setOrientation(LinearLayout.HORIZONTAL);
					lnrlayout2_FI.addView(l2,paramschk5);
					LinearLayout lchkbox = new LinearLayout(this);
					LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
							50, ViewGroup.LayoutParams.WRAP_CONTENT);
					paramschk.leftMargin = 5;
					paramschk.topMargin = 5;
					paramschk.bottomMargin = 15;
					l2.addView(lchkbox);
					LinearLayout ltextbox = new LinearLayout(this);
					LinearLayout.LayoutParams paramstext = new LinearLayout.LayoutParams(
							300, ViewGroup.LayoutParams.WRAP_CONTENT);
					imgview1[i] = new ImageView(this);
					chkbox[i] = new CheckBox(this);
				
						chkbox[i].setPadding(30, 0, 0, 0);
						lchkbox.addView(chkbox[i], paramschk);
						paramstext.topMargin = 5;
						paramstext.leftMargin = 15;
						l2.addView(ltextbox);
						
						
						LinearLayout ltextbox6= new LinearLayout(this);
						LinearLayout.LayoutParams paramstext6 = new LinearLayout.LayoutParams(
								75, 75);
						//paramstext6.topMargin = 5;
						paramstext6.leftMargin = 10;
						ltextbox.addView(ltextbox6);
						
						BitmapFactory.Options o = new BitmapFactory.Options();
						o.inJustDecodeBounds = true;
						try {
							BitmapFactory.decodeStream(new FileInputStream(titlehead.getText().toString()+pieces3[i]),
									null, o);
							j = "1";
						} catch (FileNotFoundException e) {
							j = "2";
						}
						if (j.equals("1")) {
							final int REQUIRED_SIZE = 100;
							int width_tmp = o.outWidth, height_tmp = o.outHeight;
							int scale = 1;
							while (true) {
								if (width_tmp / 2 < REQUIRED_SIZE
										|| height_tmp / 2 < REQUIRED_SIZE)
									break;
								width_tmp /= 2;
								height_tmp /= 2;
								scale *= 2;
							}
							
							// Decode with inSampleSize
							BitmapFactory.Options o2 = new BitmapFactory.Options();
							o2.inSampleSize = scale;
							Bitmap bitmap = null;
							try {
								
								bitmap = BitmapFactory.decodeStream(new FileInputStream(
										titlehead.getText().toString()+pieces3[i]), null, o2);
								
							
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							BitmapDrawable bmd = new BitmapDrawable(bitmap);
							imgview1[i].setImageDrawable(bmd);
						ltextbox6.addView(imgview1[i], paramstext6);
						
						}
						LinearLayout ltextbox1= new LinearLayout(this);
						LinearLayout.LayoutParams paramstext1 = new LinearLayout.LayoutParams(
								ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
						paramstext1.topMargin = 5;
						paramstext1.leftMargin = 15;
						ltextbox6.addView(ltextbox1);
						tvstatus[i] = new TextView(this);
						tvstatus[i].setText(pieces3[i]);
						tvstatus[i].setTextColor(Color.BLACK);
						tvstatus[i].setMinimumWidth(100);
						tvstatus[i].setTag("tvstatus" + i);
						imgview1[i].setTag("tvstatus" + i);
						chkbox[i].setTag("tvstatus" + i);
						for(int j=0;j<selectedcount;j++)
						{
						if(selectedtestcaption[j].equals(titlehead.getText().toString()+pieces3[i]))
							{
								
								chkbox[i].setChecked(true);
							}
						}
						map1.put("tvstatus" + i, tvstatus[i]);
						ltextbox1.addView(tvstatus[i], paramstext1);
						
						
						
					chkbox[i].setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							String getidofselbtn = v.getTag().toString();
							String repidofselbtn = getidofselbtn.replace("tvstatus","");
							int chk = Integer.parseInt(repidofselbtn);
							final int cvrtstr = Integer.parseInt(repidofselbtn) + 1;
							temp_st = map1.get(getidofselbtn);
							
							if(chkbox[chk].isChecked()==false)
							{
								selectedtestcaption=Delete_from_array(selectedtestcaption,titlehead.getText().toString()+temp_st.getText().toString());
								selectedcount=selectedcount-1;
								
							}
							else
							{
								Bitmap b=ShrinkBitmap(titlehead.getText().toString()+temp_st.getText().toString(), 400, 400);
								if(b!=null){		
								Cursor c11=null;
								int isexist=0;
									try{
										c11 =cf.sh_db.rawQuery("SELECT * FROM "+ cf.ImageTable + " WHERE SH_IM_SRID='"+cf.selectedhomeid+"' and " +
												"SH_IM_ImageName='"+cf.encode(titlehead.getText().toString()+temp_st.getText().toString())+ "'  and SH_IM_Elevation='"+elev+"' order by SH_IM_ImageOrder",null);
										
													
													isexist = c11.getCount();
									}catch(Exception e)
									{
										cf.Error_LogFile_Creation("Problem in checking for the image is exisit or not in image selection in galary filee multi selection page photos Error="+e.getMessage());
									}
							
											if (isexist == 0) {
												if(cf.common(titlehead.getText().toString()+pieces3[chk]))
												{
													if(maximumindb+selectedcount>=4)
													{
														chkbox[chk].setChecked(false);
														cf.ShowToast("You can upload only 4 images.", 1);
													}
													else
													{
																									
													//maximumindb
													selectedtestcaption=dynamicarraysetting(titlehead.getText().toString()+pieces3[chk],selectedtestcaption);
													selectedcount++;
													}
												}
												else
												{
													chkbox[chk].setChecked(false);
													cf.ShowToast("Please select image less than 2MB.",1);
												}
											
										 
											} else {
												
												chkbox[chk].setChecked(false);
												cf.ShowToast("Selected Image has been uploaded already. Please select another Image.",1);
											}
							
							}
								
							else
							{
								chkbox[chk].setChecked(false);
								cf.ShowToast("This image is not a supported format. You can'table to upload.", 1);
							}
							}
						}
						});
					imgview1[i].setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							String getidofselbtn = v.getTag().toString();
							repidofselbtn1= getidofselbtn.replace("tvstatus","");
							cvrtstr1 = Integer.parseInt(repidofselbtn1) + 1;
							temp_st = map1.get(getidofselbtn);
							if ((titlehead.getText().toString().endsWith(".jpg"))||(titlehead.getText().toString().endsWith(".jpeg"))||(titlehead.getText().toString().endsWith(".png"))||(titlehead.getText().toString().endsWith(".gif")||(titlehead.getText().toString().endsWith(".JPG"))
					   				 ||(titlehead.getText().toString().endsWith(".JPEG"))||(titlehead.getText().toString().endsWith(".PNG")))||(titlehead.getText().toString().endsWith(".gif"))||(titlehead.getText().toString().endsWith(".bmp"))){
								
								
								String[] bits = titlehead.getText().toString().split("/");
								
								
								String picname = bits[bits.length - 1];
								String val = titlehead.getText().toString().replace(picname, temp_st.getText().toString());
								
								titlehead.setText(val);
							
							}
							else
							{
								if(titlehead.getText().toString()=="mnt/sdcard/")
								{
									titlehead.setText(titlehead.getText().toString()+temp_st.getText().toString());
								}
								else
								{
									titlehead.setText(titlehead.getText().toString()+temp_st.getText().toString());
								}
									
							}
							dialog1 = new Dialog(photos.this);
			    	 		dialog1.setContentView(R.layout.listview1);
			    	 		dialog1.setTitle("Select Images");
			    	 		dialog1.setCancelable(true);
			    	 		imgview2 = (ImageView) dialog1.findViewById(R.id.full_image);
			    	 		selectBtn = (Button) dialog1.findViewById(R.id.selectBtn);
			    	 		cancelBtn = (Button) dialog1.findViewById(R.id.cancelBtn);
			    	 		pathofimage = (TextView) dialog1.findViewById(R.id.imagepath);
			    	 		pathofimage.setText(titlehead.getText().toString());
			    	 		
			    			Bitmap b = cf.ShrinkBitmap(titlehead.getText().toString(), 130, 130);
			    			imgview2.setImageBitmap(b);
			    			
			    			cancelBtn.setOnClickListener(new OnClickListener() {
			    				public void onClick(View v) {
			    					//pieces1=null;pieces2=null;pieces3=null;
			    					String value = titlehead.getText().toString();
			    					String[] bits = value.split("/");
			    					String picname = bits[bits.length - 1];
			    					
			    					titlehead.setText(value.replace(picname, ""));
			    					dialog1.setCancelable(true);
			    					dialog1.cancel();
			    				}
			    			});
			    			selectBtn.setOnClickListener(new OnClickListener() {
			    				public void onClick(View v) { 
			    					
			    						Cursor c11=null;
										int isexist=0;
									try{
										c11 =cf.sh_db.rawQuery("SELECT * FROM "+ cf.ImageTable + " WHERE SH_IM_SRID='"+cf.selectedhomeid+"' and " +
												"SH_IM_ImageName='"+cf.encode(titlehead.getText().toString())+ "' and SH_IM_Elevation='"+elev+"' order by SH_IM_ImageOrder",null);
													
													isexist = c11.getCount();
									}catch(Exception e)
									{
										cf.Error_LogFile_Creation("Problem in checking for the image is exisit or not in image selection in galary filee multi selection page photos Error="+e.getMessage());
									}
			    						 isexist = c11.getCount();
										
										if (isexist == 0) {
											
											if(chkbox[Integer.parseInt(repidofselbtn1)].isChecked())
											{
												cf.ShowToast("You have already slected this ", 1);
											}
											else
											{
												if(cf.common(pathofimage.getText().toString()))
												{
													if(maximumindb+selectedcount>=4)
													{
														cf.ShowToast("You can upload only 4 images.", 1);
													}
													else
													{
													selectedtestcaption=dynamicarraysetting(pathofimage.getText().toString(),selectedtestcaption );
													selectedcount++;
													chkbox[Integer.parseInt(repidofselbtn1)].setChecked(true);
													}
												}
												else
												{
													cf.ShowToast("Please select image less than 2MB",1);
													chkbox[Integer.parseInt(repidofselbtn1)].setChecked(false);
												}
											}
									 
										} else {
											
											chkbox[Integer.parseInt(repidofselbtn1)].setChecked(false);
											cf.ShowToast("Selected Image has been uploaded already. Please select another Image.",1);
										}
			    					String value = titlehead.getText().toString();
			    					String[] bits = value.split("/");
			    					String picname = bits[bits.length - 1];
			    					
			    					titlehead.setText(value.replace(picname, ""));
			    					dialog1.setCancelable(true);
			    					dialog1.cancel();
			    				}
			    			});
			    			dialog1.setCancelable(false);
			    			dialog1.show();
						}
					});
				}
					
				}
			
		
		}
		
		protected String[] Delete_from_array(String[] selectedtestcaption2,
				String txt) {
			
			// TODO Auto-generated method stub
			String tmp[]=new String[selectedtestcaption2.length-1];
			int j=0;
			if(selectedtestcaption2!=null)
			{
				for(int i=0;i<selectedtestcaption2.length;i++)
				{
					if(!selectedtestcaption2[i].equals(txt))
					{
						tmp[j]=selectedtestcaption2[i];
						j++;
					}
					else
					{
						
					}
				}
				
			}
			return tmp;
		}

		protected void imagessub(int cvrtstr, TextView temp_st2) {
			String value = titlehead.getText().toString();
			
			if(value=="/mnt/sdcard/")
			{
				
			titlehead.setText(value+temp_st2.getText().toString()+"/");
			}
			else
			{
				if ((value.endsWith(".jpg"))||(value.endsWith(".jpeg"))||(value.endsWith(".png"))||(value.endsWith(".gif")||(value.endsWith(".JPG"))
	   				 ||(value.endsWith(".JPEG"))||(value.endsWith(".PNG")))||(value.endsWith(".gif"))||(value.endsWith(".bmp")))
				{
					String[] bits = value.split("/");
					String picname = bits[bits.length - 1];


						String val = value.replace(picname, temp_st2.getText().toString());
						titlehead.setText(val);
					}
					else
					{
						
						titlehead.setText(value+temp_st2.getText().toString()+"/");
					}
			} 
				if ((value.endsWith(".jpg"))||(value.endsWith(".jpeg"))||(value.endsWith(".png"))||(value.endsWith(".gif")||(value.endsWith(".JPG"))
	   				 ||(value.endsWith(".JPEG"))||(value.endsWith(".PNG")))||(value.endsWith(".gif"))||(value.endsWith(".bmp"))){
					dialog = new Dialog(this);
	    	 		dialog.setContentView(R.layout.listview1);
	    	 		dialog.setTitle("Select Images");
	    	 		dialog.setCancelable(true);
	    	 		imgview2 = (ImageView) dialog.findViewById(R.id.full_image);
	    	 		selectBtn = (Button) dialog.findViewById(R.id.selectBtn);
	    	 		cancelBtn = (Button) dialog.findViewById(R.id.cancelBtn);
	    	 	
	    	 		BitmapFactory.Options o2 = new BitmapFactory.Options();
	    			int scale = 0;
	    			o2.inSampleSize = scale;
	    			Bitmap bitmap = null;
	    			try {
	    				bitmap = BitmapFactory.decodeStream(new FileInputStream(titlehead.getText().toString()), null, o2);
	    				BitmapDrawable bmd = new BitmapDrawable(bitmap);
	    				imgview2.setImageDrawable(bmd);
	    			} catch (FileNotFoundException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}
	    			cancelBtn.setOnClickListener(new OnClickListener() {
	    				public void onClick(View v) {
	    					dialog.cancel();
	    				}
	    			});
	    			dialog.show();
	    		}
	    		else
	    		{
	    			finaltext = titlehead.getText().toString();
	    			File f = new File(titlehead.getText().toString());
	    			
					String[] bits = titlehead.getText().toString().split("/");
					String picname1 = bits[bits.length - 1];
					chaild_titleheade.setText(picname1); // set the chaild title 
					walkdir(f);   	
	    			dynamicimagesparent();
	    		}
					 dynamicimageschild();
		}
		
		protected void subview() {
	 		// TODO Auto-generated method stub
     
	 		Cursor d11 = cf.sh_db.rawQuery("SELECT * FROM " + cf.ImageTable
	 				+ " WHERE SH_IM_SRID='" + cf.selectedhomeid + "' and SH_IM_Elevation='" + elev
	 				+ "' and SH_IM_Delflag=0 order by SH_IM_ImageOrder", null);
	 		rws = d11.getCount();
	 		if (rws == 0) {
	 			cf.ShowToast("There is no Image, So please upload the Image.",1);
	 		} else {
	 			String Imagepath[] = new String[rws];

	 			this.et = new EditText[rws];

	 			dialog = new Dialog(photos.this);
	 			dialog.setContentView(R.layout.changeimageorder);
	 			dialog.setTitle("Change Image Order");
	 			dialog.setCancelable(false);
	 			Button save=(Button) dialog.findViewById(R.id.Save);
	 			Button close=(Button) dialog.findViewById(R.id.close);
	 			
                TableLayout ln_main=(TableLayout) dialog.findViewById(R.id.changeorder);
	 			try {

	 				this.count1 = d11.getCount();
	 				this.thumbnails1 = new Bitmap[this.count1];
	 				this.arrPath1 = new String[this.count1];
	 				this.imageid = new int[this.count1];
	 				this.SH_IM_ImageOrder = new int[this.count1];
	 				this.str = new String[this.count1];
	 				ImageView chag_img[] = new ImageView[this.count1];
	 				final EditText chan_ed[]=new EditText[this.count1]; 
	 				TableRow ln_sub=null;
	 				d11.moveToFirst();

	 				for (int i = 0; i < this.count1; i++) {
	 					/**We create the new check box and edit box for the change image order **/
	 					chag_img[i]=new ImageView(photos.this); 
	 					TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
	 					TableRow.LayoutParams lp2 = new TableRow.LayoutParams(100, 100);
	 					lp.setMargins(5, 10, 5, 10);
	 					chag_img[i].setLayoutParams(lp2);
	 					chan_ed[i]=new EditText(photos.this);
	 					//chan_ed[i].setMa))(2);
	 					chan_ed[i].setInputType(InputType.TYPE_CLASS_NUMBER);
	 					chan_ed[i].setLayoutParams(lp);
	 					LinearLayout li_tmp=new LinearLayout(photos.this);
	 					li_tmp.setLayoutParams(lp);
	 					
	 					/**We create the new check box and edit box for the change image order ends  **/
	 					this.imageid[i] = Integer.parseInt(d11.getString(0)
	 							.toString());
	 					this.SH_IM_ImageOrder[i] = d11.getInt(d11
	 							.getColumnIndex("SH_IM_ImageOrder"));
	 					arrPath1[i] = cf.decode(d11.getString(d11
	 							.getColumnIndex("SH_IM_ImageName")));
	 					Bitmap b = cf.ShrinkBitmap(arrPath1[i], 130, 130);
	 					thumbnails1[i] = b;
	 					if(i==0)
	 					{
	 						
	 						LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
 							ln_sub = new TableRow(photos.this);
 							ln_sub.setLayoutParams(lp1);  
	 					}
	 					li_tmp.addView(chag_img[i]);
	 					li_tmp.addView(chan_ed[i]);
	 					ln_sub.addView(li_tmp);
	 					chag_img[i].setImageBitmap(b);
	 					chan_ed[i].setText(String.valueOf(this.SH_IM_ImageOrder[i]));
	 					d11.moveToNext();
	 					if((((i+1)%3)==0 || (i+1)==this.count1)&& ln_sub!=null )
 						{
 							ln_main.addView(ln_sub);
 							LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
 							ln_sub = new TableRow(photos.this);
 							ln_sub.setLayoutParams(lp1);  
 						}
	 					
	 				}
	 				close.setOnClickListener(new OnClickListener() {

	 					public void onClick(View v) {
	 						// TODO Auto-generated method stub
	 						dialog.setCancelable(true);
	 						dialog.cancel();
	 					}
	 				});

	 				save.setOnClickListener(new OnClickListener() {

	 					public void onClick(View v) {
	 						// TODO Auto-generated method stub
	 						// dialog.cancel();
	 						Boolean boo = true;
	 						String temp[] = new String[count1];
	 						try {
	 							for (int i = 0; i < imageid.length; i++) {
	 								temp[i] = chan_ed[i].getText().toString();
	 								int myorder = 0;
	 								if (temp[i] != null && !temp[i].equals("")) {
	 									myorder = Integer.parseInt(temp[i]);
	 									if (myorder <= imageid.length
	 											&& myorder != 0) {

	 									} else {
	 										boo = false;
	 									}

	 								} else {
	 									boo = false;
	 								}

	 							}
	 							
	 							int length = temp.length;
	 							for (int i = 0; i < length; i++) {
	 								for (int j = 0; j < length; j++) {
	 									if (temp[i].equals(temp[j]) && i != j) {
	 										boo = false;
	 									}

	 								}

	 							}
	 							if (!boo) {
	 								cf.ShowToast("Please select different Image Order.",1);
	 							} else {
	 								for (int i = 0; i < imageid.length; i++) {
	 									cf.sh_db.execSQL("UPDATE " + cf.ImageTable
	 											+ " SET SH_IM_ImageOrder='" + temp[i]
	 											+ "' WHERE SH_IM_ID='" + imageid[i]
	 											+ "'");
	 								}
	 								dialog.setCancelable(true);
	 								dialog.cancel();
	 								cf.ShowToast("Image Order saved successfully.",1);
	 								showimages();
	 							}
	 						}

	 						catch (Exception e) {

	 						}
	 					}
	 				});
                    
	 				dialog.show();

	 				
	 			} catch (Exception m) {
	 				
	 			}
	 		}

	 	}
	public 	Bitmap ShrinkBitmap(String file, int width, int height) { try {
			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inJustDecodeBounds = true;
			Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

			int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
					/ (float) height);
			int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
					/ (float) width);

			if (heightRatio > 1 || widthRatio > 1) {
				if (heightRatio > widthRatio) {
					bmpFactoryOptions.inSampleSize = heightRatio;
				} else {
					bmpFactoryOptions.inSampleSize = widthRatio;
				}
			}

			bmpFactoryOptions.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
			return bitmap;
		} catch (Exception e) {
			return null;
		}

	}
	
}


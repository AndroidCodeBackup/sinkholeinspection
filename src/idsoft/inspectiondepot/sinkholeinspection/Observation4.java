package idsoft.inspectiondepot.sinkholeinspection;

import idsoft.inspectiondepot.sinkholeinspection.Observation1.Touch_Listener;
import idsoft.inspectiondepot.sinkholeinspection.Observation3.Obs_clicker;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Observation4 extends Activity{
	CommonFunctions cf;
	LinearLayout SH_OBS23P,SH_OBS23AP,SH_OBS23BP,SH_OBS23CP,SH_OBS23DP,SH_OBS24P,SH_OBS25P,
	             SH_OBS26P,SH_OBS26AP,SH_OBS26BP,SH_OBS26CP,SH_OBS27P,SH_OBS23,SH_OBS23A,
	             SH_OBS23B,SH_OBS23C,SH_OBS23D,SH_OBS24,SH_OBS25,SH_OBS26,SH_OBS26A,SH_OBS26B,
	             SH_OBS26C,SH_OBS27,SH_OBSADD,SH_OBSADDP;
	TextView SH_OBS23T,SH_OBS23AT,SH_OBS23BT,SH_OBS23CT,SH_OBS23DT,SH_OBS24T,SH_OBS25T,SH_OBS26T,
	         SH_OBS26AT,SH_OBS26BT,SH_OBS26CT,SH_OBS27T,SH_OBSADDT;
	String strhomeid,rd_CrawlSpacePresent="",rd_StandingWaterNoted="",rd_LeakingPlumbNoted="",rd_VentilationNoted="",
	       rd_CrawlSpaceAccessible="",rd_RoofSagNoted="",rd_RoofLeakageEvidence="",rd_DeferredMaintanenceIssuesNoted="",
	       rd_DeterioratedNoted="",rd_DeterioratedExteriorFinishesNoted="",rd_DeterioratedExteriorSidingNoted="",rd_SafetyNoted="";
	 @Override
	    public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
	    	if (extras != null) {
	    		cf.selectedhomeid = strhomeid= extras.getString("homeid");
	    		cf.onlinspectionid = extras.getString("InspectionType");
	    	    cf.onlstatus = extras.getString("status");
	     	}
	        setContentView(R.layout.observation4);
	        cf.Create_Table(12);
	        cf.getDeviceDimensions();
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,cf));
			LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.observationsubmenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 44, 1,cf));
			
			 /*DECLARATION AND CLCIK EVENT OF OBSERVATION23,24,25,26,27 ROW*/ 
			 cf.tbl_row_obs23 = (TableRow)findViewById(R.id.firstobsrow);
			 cf.tbl_row_obs23.setOnClickListener(new Obs_clicker());
			 cf.tbl_row_obs24 = (TableRow)findViewById(R.id.secondobsrow);
			 cf.tbl_row_obs24.setOnClickListener(new Obs_clicker());
			 cf.tbl_row_obs25 = (TableRow)findViewById(R.id.thirdobsrow);
			 cf.tbl_row_obs25.setOnClickListener(new Obs_clicker());
			 cf.tbl_row_obs26 = (TableRow)findViewById(R.id.fourthobsrow);
			 cf.tbl_row_obs26.setOnClickListener(new Obs_clicker());
			 cf.tbl_row_obs27 = (TableRow)findViewById(R.id.fifthobsrow);
			 cf.tbl_row_obs27.setOnClickListener(new Obs_clicker());
			 
			 /*DECLARATION OF TABLE HEADERS OF OBSERVATION23,24,25,26,27 and ADDENDUM TEXT*/
			 cf.obs23_tbl_row_txt = (TextView)findViewById(R.id.txtobservation23);
			 cf.obs24_tbl_row_txt = (TextView)findViewById(R.id.txtobservation24);
			 cf.obs25_tbl_row_txt = (TextView)findViewById(R.id.txtobservation25);
			 cf.obs26_tbl_row_txt = (TextView)findViewById(R.id.txtobservation26);
			 cf.obs27_tbl_row_txt = (TextView)findViewById(R.id.txtobservation27);
			 cf.addenum_tbl_row_txt = (TextView)findViewById(R.id.txtaddednum);
			 cf.addenum_tbl_row_txt.setText(Html.fromHtml("<b> <font color=#ff0000>*</font> <font color=#000000> ADDENDUM PAGE: ANY OTHER INFORMATION / COMMENTS </b>"));
			
			 /*DECLARATION OF YES LAYOUT FOR OBSERVATION 23,24,25,26,27*/
			 cf.tbl_layout_obs23 = (TableLayout)findViewById(R.id.obs23_table);
			 cf.tbl_layout_obs24 = (TableLayout)findViewById(R.id.obs24_table);
			 cf.tbl_layout_obs25 = (TableLayout)findViewById(R.id.obs25_table);
			 cf.tbl_layout_obs26 = (TableLayout)findViewById(R.id.obs26_table);
			 cf.tbl_layout_obs27 = (TableLayout)findViewById(R.id.obs27_table);
			 
			 /*DECLARATION AND CLICK EVENT OF OBSERVATION23,24,25,26,27 - RADIOBUTTON YES */
			 cf.rd_obs23_yes = (RadioButton)findViewById(R.id.obs23_rd_y);
			 cf.rd_obs23_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs23_opta_yes = (RadioButton)findViewById(R.id.obs23_opta_rd_yes);
			 cf.rd_obs23_opta_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs23_optb_yes = (RadioButton)findViewById(R.id.obs23_optb_rd_yes);
			 cf.rd_obs23_optb_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs23_optc_yes = (RadioButton)findViewById(R.id.obs23_optc_rd_yes);
			 cf.rd_obs23_optc_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs23_optd_yes = (RadioButton)findViewById(R.id.obs23_optd_rd_yes);
			 cf.rd_obs23_optd_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs24_yes = (RadioButton)findViewById(R.id.obs24_rd_y);
			 cf.rd_obs24_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs25_yes = (RadioButton)findViewById(R.id.obs25_rd_y);
			 cf.rd_obs25_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs26_yes = (RadioButton)findViewById(R.id.obs26_rd_y);
			 cf.rd_obs26_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs26_opta_yes = (RadioButton)findViewById(R.id.obs26_opta_rd_yes);
			 cf.rd_obs26_opta_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs26_optb_yes = (RadioButton)findViewById(R.id.obs26_optb_rd_yes);
			 cf.rd_obs26_optb_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs26_optc_yes = (RadioButton)findViewById(R.id.obs26_optc_rd_yes);
			 cf.rd_obs26_optc_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs27_yes = (RadioButton)findViewById(R.id.obs27_rd_y);
			 cf.rd_obs27_yes.setOnClickListener(new Obs_clicker());
			 
			 /*DECLARATION AND CLICK EVENT OF OBSERVATION23,24,25,26,27 - RADIOBUTTON NO */
			 cf.rd_obs23_no = (RadioButton)findViewById(R.id.obs23_rd_n);
			 cf.rd_obs23_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs23_opta_no = (RadioButton)findViewById(R.id.obs23_opta_rd_no);
			 cf.rd_obs23_opta_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs23_optb_no = (RadioButton)findViewById(R.id.obs23_optb_rd_no);
			 cf.rd_obs23_optb_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs23_optc_no = (RadioButton)findViewById(R.id.obs23_optc_rd_no);
			 cf.rd_obs23_optc_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs23_optd_no = (RadioButton)findViewById(R.id.obs23_optd_rd_no);
			 cf.rd_obs23_optd_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs24_no = (RadioButton)findViewById(R.id.obs24_rd_n);
			 cf.rd_obs24_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs25_no = (RadioButton)findViewById(R.id.obs25_rd_n);
			 cf.rd_obs25_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs26_no = (RadioButton)findViewById(R.id.obs26_rd_n);
			 cf.rd_obs26_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs26_opta_no = (RadioButton)findViewById(R.id.obs26_opta_rd_no);
			 cf.rd_obs26_opta_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs26_optb_no = (RadioButton)findViewById(R.id.obs26_optb_rd_no);
			 cf.rd_obs26_optb_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs26_optc_no = (RadioButton)findViewById(R.id.obs26_optc_rd_no);
			 cf.rd_obs26_optc_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs27_no = (RadioButton)findViewById(R.id.obs27_rd_n);
			 cf.rd_obs27_no.setOnClickListener(new Obs_clicker());
			 
			 /*DECLARATION AND CLICK EVENT OF OBSERVATION24,25,26,27 - RADIOBUTTON NOTDETERMINED */
			 cf.rd_obs23_opta_na = (RadioButton)findViewById(R.id.obs23_opta_rd_nd);
			 cf.rd_obs23_opta_na.setOnClickListener(new Obs_clicker());
			 cf.rd_obs23_optb_na = (RadioButton)findViewById(R.id.obs23_optb_rd_nd);
			 cf.rd_obs23_optb_na.setOnClickListener(new Obs_clicker());
			 cf.rd_obs23_optc_na = (RadioButton)findViewById(R.id.obs23_optc_rd_nd);
			 cf.rd_obs23_optc_na.setOnClickListener(new Obs_clicker());
			 cf.rd_obs23_optd_na = (RadioButton)findViewById(R.id.obs23_optd_rd_nd);
			 cf.rd_obs23_optd_na.setOnClickListener(new Obs_clicker());
			 cf.rd_obs24_nd = (RadioButton)findViewById(R.id.obs24_rd_nd);
			 cf.rd_obs24_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs25_nd = (RadioButton)findViewById(R.id.obs25_rd_nd);
			 cf.rd_obs25_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs26_nd = (RadioButton)findViewById(R.id.obs26_rd_nd);
			 cf.rd_obs26_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs26_opta_nd = (RadioButton)findViewById(R.id.obs26_opta_rd_nd);
			 cf.rd_obs26_opta_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs26_optb_nd = (RadioButton)findViewById(R.id.obs26_optb_rd_nd);
			 cf.rd_obs26_optb_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs26_optc_nd = (RadioButton)findViewById(R.id.obs26_optc_rd_nd);
			 cf.rd_obs26_optc_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs27_nd = (RadioButton)findViewById(R.id.obs27_rd_nd);
			 cf.rd_obs27_nd.setOnClickListener(new Obs_clicker());
			 
			 /*DECLARATION OF COMMENTS  - OBSERVATION23,24,25,26,27*/
			  cf.etcomments_obs23 = (EditText)findViewById(R.id.obs23_etcomments);
			  cf.etcomments_obs23_opta = (EditText)findViewById(R.id.obs23_opta_etcomments);
			  cf.etcomments_obs23_optb = (EditText)findViewById(R.id.obs23_optb_etcomments);
			  cf.etcomments_obs23_optc = (EditText)findViewById(R.id.obs23_optc_etcomments);
			  cf.etcomments_obs23_optd = (EditText)findViewById(R.id.obs23_optd_etcomments);
			  cf.etcomments_obs24 = (EditText)findViewById(R.id.obs24_etcomments);
			  cf.etcomments_obs25 = (EditText)findViewById(R.id.obs25_etcomments);
			  cf.etcomments_obs26 = (EditText)findViewById(R.id.obs26_etcomments);
			  cf.etcomments_obs26_opta = (EditText)findViewById(R.id.obs26_opta_etcomments);
			  cf.etcomments_obs26_optb = (EditText)findViewById(R.id.obs26_optb_etcomments);
			  cf.etcomments_obs26_optc = (EditText)findViewById(R.id.obs26_optc_etcomments);
			  cf.etcomments_obs27 = (EditText)findViewById(R.id.obs27_etcomments);
			  cf.addednumcomments = (EditText)findViewById(R.id.addednum_etcomments);
			  
			  /*DECLARATION OF EDITTEXT LIMIT EXCEEDS */
			  SH_OBS23P = (LinearLayout) findViewById(R.id.SH_OBS23_ED_parrent);
			  SH_OBS23 = (LinearLayout) findViewById(R.id.SH_OB23);
			  SH_OBS23T = (TextView) findViewById(R.id.SH_OB23_TV);
			  
			  SH_OBS23AP = (LinearLayout) findViewById(R.id.SH_OBS23a_ED_parrent);
			  SH_OBS23A = (LinearLayout) findViewById(R.id.SH_OB23a);
			  SH_OBS23AT = (TextView) findViewById(R.id.SH_OB23a_TV);
			  
			  SH_OBS23BP = (LinearLayout) findViewById(R.id.SH_OBS23b_ED_parrent);
			  SH_OBS23B = (LinearLayout) findViewById(R.id.SH_OB23b);
			  SH_OBS23BT = (TextView) findViewById(R.id.SH_OB23b_TV);
			  
			  SH_OBS23CP = (LinearLayout) findViewById(R.id.SH_OBS23c_ED_parrent);
			  SH_OBS23C = (LinearLayout) findViewById(R.id.SH_OB23c);
			  SH_OBS23CT = (TextView) findViewById(R.id.SH_OB23c_TV);
			  
			  SH_OBS23DP = (LinearLayout) findViewById(R.id.SH_OBS23d_ED_parrent);
			  SH_OBS23D = (LinearLayout) findViewById(R.id.SH_OB23d);
			  SH_OBS23DT = (TextView) findViewById(R.id.SH_OB23d_TV);
			  
			  SH_OBS24P = (LinearLayout) findViewById(R.id.SH_OBS24_ED_parrent);
			  SH_OBS24 = (LinearLayout) findViewById(R.id.SH_OB24);
			  SH_OBS24T = (TextView) findViewById(R.id.SH_OB24_TV);
			  
			  SH_OBS25P = (LinearLayout) findViewById(R.id.SH_OBS25_ED_parrent);
			  SH_OBS25 = (LinearLayout) findViewById(R.id.SH_OB25);
			  SH_OBS25T = (TextView) findViewById(R.id.SH_OB25_TV);
			  
			  SH_OBS26P = (LinearLayout) findViewById(R.id.SH_OBS26_ED_parrent);
			  SH_OBS26 = (LinearLayout) findViewById(R.id.SH_OB26);
			  SH_OBS26T = (TextView) findViewById(R.id.SH_OB26_TV);
			  
			  SH_OBS26AP = (LinearLayout) findViewById(R.id.SH_OBS26a_ED_parrent);
			  SH_OBS26A = (LinearLayout) findViewById(R.id.SH_OB26a);
			  SH_OBS26AT = (TextView) findViewById(R.id.SH_OB26a_TV);
			  
			  SH_OBS26BP = (LinearLayout) findViewById(R.id.SH_OBS26b_ED_parrent);
			  SH_OBS26B = (LinearLayout) findViewById(R.id.SH_OB26b);
			  SH_OBS26BT = (TextView) findViewById(R.id.SH_OB26b_TV);
			  			  
			  SH_OBS26CP = (LinearLayout) findViewById(R.id.SH_OBS26c_ED_parrent);
			  SH_OBS26C = (LinearLayout) findViewById(R.id.SH_OB26c);
			  SH_OBS26CT = (TextView) findViewById(R.id.SH_OB26c_TV);
			  
			  SH_OBS27P = (LinearLayout) findViewById(R.id.SH_OBS27_ED_parrent);
			  SH_OBS27 = (LinearLayout) findViewById(R.id.SH_OB27);
			  SH_OBS27T = (TextView) findViewById(R.id.SH_OB27_TV);
			  
			  SH_OBSADDP = (LinearLayout) findViewById(R.id.SH_OBADD_ED_parrent);
			  SH_OBSADD = (LinearLayout) findViewById(R.id.SH_OBADD);
			  SH_OBSADDT = (TextView) findViewById(R.id.SH_OBADD_TV);
			  
			  cf.etcomments_obs23.setOnTouchListener(new Touch_Listener(1));
			  cf.etcomments_obs23_opta.setOnTouchListener(new Touch_Listener(2));
			  cf.etcomments_obs23_optb.setOnTouchListener(new Touch_Listener(3));
			  cf.etcomments_obs23_optc.setOnTouchListener(new Touch_Listener(4));
			  cf.etcomments_obs23_optd.setOnTouchListener(new Touch_Listener(5));
			  cf.etcomments_obs24.setOnTouchListener(new Touch_Listener(6));
			  cf.etcomments_obs25.setOnTouchListener(new Touch_Listener(7));
			  cf.etcomments_obs26.setOnTouchListener(new Touch_Listener(8));
			  cf.etcomments_obs26_opta.setOnTouchListener(new Touch_Listener(9));
			  cf.etcomments_obs26_optb.setOnTouchListener(new Touch_Listener(10));
			  cf.etcomments_obs26_optc.setOnTouchListener(new Touch_Listener(11));
			  cf.etcomments_obs27.setOnTouchListener(new Touch_Listener(12));
			  cf.addednumcomments.setOnTouchListener(new Touch_Listener(13));
			  
			  cf.etcomments_obs23.addTextChangedListener(new SH_textwatcher(1));
			  cf.etcomments_obs23_opta.addTextChangedListener(new SH_textwatcher(2));
			  cf.etcomments_obs23_optb.addTextChangedListener(new SH_textwatcher(3));
			  cf.etcomments_obs23_optc.addTextChangedListener(new SH_textwatcher(4));
			  cf.etcomments_obs23_optd.addTextChangedListener(new SH_textwatcher(5));
			  cf.etcomments_obs24.addTextChangedListener(new SH_textwatcher(6));
			  cf.etcomments_obs25.addTextChangedListener(new SH_textwatcher(7));
			  cf.etcomments_obs26.addTextChangedListener(new SH_textwatcher(8));
			  cf.etcomments_obs26_opta.addTextChangedListener(new SH_textwatcher(9));
			  cf.etcomments_obs26_optb.addTextChangedListener(new SH_textwatcher(10));
			  cf.etcomments_obs26_optc.addTextChangedListener(new SH_textwatcher(11));
			  cf.etcomments_obs27.addTextChangedListener(new SH_textwatcher(12));
			  cf.addednumcomments.addTextChangedListener(new SH_textwatcher(13));
			  
			  /* DISPLAY THE VALUES FROM DATABASE*/
			  OBS4_SetValues();
			  
	 }
	 class Touch_Listener implements OnTouchListener
		{
			   public int type;
			   Touch_Listener(int type)
				{
					this.type=type;
					
				}
			    @Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
			    	if(this.type==1)
					{
			    		cf.setFocus(cf.etcomments_obs23);
					}
			    	else if(this.type==2)
					{
				    		cf.setFocus(cf.etcomments_obs23_opta);
					}
			    	else if(this.type==3)
					{
				    		cf.setFocus(cf.etcomments_obs23_optb);
					}
			    	else if(this.type==4)
					{
				    		cf.setFocus(cf.etcomments_obs23_optc);
					}
			    	else if(this.type==5)
					{
				    		cf.setFocus(cf.etcomments_obs23_optd);
					}
			    	else if(this.type==6)
					{
				    		cf.setFocus(cf.etcomments_obs24);
					}
			    	else if(this.type==7)
					{
				    		cf.setFocus(cf.etcomments_obs25);
					}
			    	else if(this.type==8)
					{
				    		cf.setFocus(cf.etcomments_obs26);
					}
			    	else if(this.type==9)
					{
				    		cf.setFocus(cf.etcomments_obs26_opta);
					}
			    	else if(this.type==10)
					{
				    		cf.setFocus(cf.etcomments_obs26_optb);
					}
			    	else if(this.type==11)
					{
				    		cf.setFocus(cf.etcomments_obs26_optc);
					}
			    	else if(this.type==12)
					{
				    		cf.setFocus(cf.etcomments_obs27);
					}
			    	else if(this.type==13)
					{
				    		cf.setFocus(cf.addednumcomments);
					}
			    	
					return false;
				}
			 
		}
	 private void OBS4_SetValues() {
		// TODO Auto-generated method stub
		try
		{
			Cursor OBS4_Retrive=cf.SelectTablefunction(cf.Observation4, " where SH_OBS4_SRID ='"+cf.encode(cf.selectedhomeid)+"'");
			if(OBS4_Retrive.getCount()>0)
			{
				OBS4_Retrive.moveToFirst();
				/* SETTING QUESTION nO.23 */
				rd_CrawlSpacePresent=cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("CrawlSpacePresent")));
				setvalueToRadio(cf.rd_obs23_yes,cf.rd_obs23_no,rd_CrawlSpacePresent); 
				cf.etcomments_obs23.setText(cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("CrawlSpaceComments"))));
			
				/* SETTING QUESTION NO.23(a) */
				rd_StandingWaterNoted=cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("StandingWaterNoted")));
				setValueToRadio(cf.rd_obs23_opta_yes,cf.rd_obs23_opta_no,cf.rd_obs23_opta_na,rd_StandingWaterNoted); 
				cf.etcomments_obs23_opta.setText(cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("StandingWaterComments"))));
			
				/* SETTING QUESTION NO.23(b) */
				rd_LeakingPlumbNoted=cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("LeakingPlumbNoted")));
				setValueToRadio(cf.rd_obs23_optb_yes,cf.rd_obs23_optb_no,cf.rd_obs23_optb_na,rd_LeakingPlumbNoted); 
				cf.etcomments_obs23_optb.setText(cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("LeakingPlumbComments"))));
			
				/* SETTING QUESTION NO.23(c) */
				rd_VentilationNoted=cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("VentilationNoted")));
				setValueToRadio(cf.rd_obs23_optc_yes,cf.rd_obs23_optc_no,cf.rd_obs23_optc_na,rd_VentilationNoted); 
				cf.etcomments_obs23_optc.setText(cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("VentilationComments"))));
			
				/* SETTING QUESTION NO.23(d) */
				rd_CrawlSpaceAccessible=cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("CrawlSpaceAccessible")));
				setValueToRadio(cf.rd_obs23_optd_yes,cf.rd_obs23_optd_no,cf.rd_obs23_optd_na,rd_CrawlSpaceAccessible); 
				cf.etcomments_obs23_optd.setText(cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("CrawlSpaceAccessibleComments"))));
			
				/* SETTING QUESTION NO.24 */
				rd_RoofSagNoted=cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("RoofSagNoted")));
				setValueToRadio(cf.rd_obs24_yes,cf.rd_obs24_no,cf.rd_obs24_nd,rd_RoofSagNoted); 
				cf.etcomments_obs24.setText(cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("RoofSagComments"))));
		
				/* SETTING QUESTION NO.25 */
				rd_RoofLeakageEvidence=cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("RoofLeakageEvidence")));
				setValueToRadio(cf.rd_obs25_yes,cf.rd_obs25_no,cf.rd_obs25_nd,rd_RoofLeakageEvidence); 
				cf.etcomments_obs25.setText(cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("RoofLeakageComments"))));
		
				/* SETTING QUESTION NO.26 */
				rd_DeferredMaintanenceIssuesNoted=cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("DeferredMaintanenceIssuesNoted")));
				setValueToRadio(cf.rd_obs26_yes,cf.rd_obs26_no,cf.rd_obs26_nd,rd_DeferredMaintanenceIssuesNoted); 
				cf.etcomments_obs26.setText(cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("DeferredMaintanenceComments"))));
		
				/* SETTING QUESTION NO.26(A) */
				rd_DeterioratedNoted=cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("DeterioratedNoted")));
				setValueToRadio(cf.rd_obs26_opta_yes,cf.rd_obs26_opta_no,cf.rd_obs26_opta_nd,rd_DeterioratedNoted); 
				cf.etcomments_obs26_opta.setText(cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("DeterioratedComments"))));
		
				/* SETTING QUESTION NO.26(B)*/
				rd_DeterioratedExteriorFinishesNoted=cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("DeterioratedExteriorFinishesNoted")));
				setValueToRadio(cf.rd_obs26_optb_yes,cf.rd_obs26_optb_no,cf.rd_obs26_optb_nd,rd_DeterioratedExteriorFinishesNoted); 
				cf.etcomments_obs26_optb.setText(cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("DeterioratedExteriorFinishesComments"))));
		
				/* SETTING QUESTION NO.26(C) */
				rd_DeterioratedExteriorSidingNoted=cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("DeterioratedExteriorSidingNoted")));
				setValueToRadio(cf.rd_obs26_optc_yes,cf.rd_obs26_optc_no,cf.rd_obs26_optc_nd,rd_DeterioratedExteriorSidingNoted); 
				cf.etcomments_obs26_optc.setText(cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("DeterioratedExteriorSidingComments"))));
		
				/* SETTING QUESTION NO.27 */
				rd_SafetyNoted=cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("SafetyNoted")));
				setValueToRadio(cf.rd_obs27_yes,cf.rd_obs27_no,cf.rd_obs27_nd,rd_SafetyNoted); 
				cf.etcomments_obs27.setText(cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("SafetyComments"))));
		
				/* SETTING ADDEDNUM COMMENTS */
				cf.addednumcomments.setText(cf.decode(OBS4_Retrive.getString(OBS4_Retrive.getColumnIndex("OverallComments"))));
		
				
			}
		}
		catch (Exception E)
		{
			String strerrorlog="Selection of the Observation4 table not working ";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void setValueToRadio(RadioButton rd_obs23_opta_yes,
			RadioButton rd_obs23_opta_no, RadioButton rd_obs23_opta_na,
			String q23a2) {
		// TODO Auto-generated method stub
		if(rd_obs23_opta_yes.getText().toString().equals(q23a2))
		{
			rd_obs23_opta_yes.setChecked(true);
		}
		else if(rd_obs23_opta_no.getText().toString().equals(q23a2))
		{
			rd_obs23_opta_no.setChecked(true);
		}
		else if(rd_obs23_opta_na.getText().toString().equals(q23a2))
		{
			rd_obs23_opta_na.setChecked(true);
		}
	}
	private void setvalueToRadio(RadioButton rd_obs23_yes,
			RadioButton rd_obs23_no, String q232) {
		// TODO Auto-generated method stub
		if(rd_obs23_yes.getText().toString().equals(q232))
		{
			rd_obs23_yes.setChecked(true);
		}
		else if(rd_obs23_no.getText().toString().equals(q232))
		{
			rd_obs23_no.setChecked(true);
		}
		
	}
	class SH_textwatcher implements TextWatcher
		{
	         public int type;
			SH_textwatcher(int type)
			{
				this.type=type;
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(this.type==1)
				{
					cf.showing_limit(s.toString(),SH_OBS23P,SH_OBS23,SH_OBS23T,"499");
				}
				else if(this.type==2)
				{
					cf.showing_limit(s.toString(),SH_OBS23AP,SH_OBS23A,SH_OBS23AT,"499");
				}
				else if(this.type==3)
				{
					cf.showing_limit(s.toString(),SH_OBS23BP,SH_OBS23B,SH_OBS23BT,"499");
				}
				else if(this.type==4)
				{
					cf.showing_limit(s.toString(),SH_OBS23CP,SH_OBS23C,SH_OBS23CT,"499");
				}
				else if(this.type==5)
				{
					cf.showing_limit(s.toString(),SH_OBS23DP,SH_OBS23D,SH_OBS23DT,"499");
				}
				else if(this.type==6)
				{
					cf.showing_limit(s.toString(),SH_OBS24P,SH_OBS24,SH_OBS24T,"499");
				}
				else if(this.type==7)
				{
					cf.showing_limit(s.toString(),SH_OBS25P,SH_OBS25,SH_OBS25T,"499");
				}
				else if(this.type==8)
				{
					cf.showing_limit(s.toString(),SH_OBS26P,SH_OBS26,SH_OBS26T,"499");
				}
				else if(this.type==9)
				{
					cf.showing_limit(s.toString(),SH_OBS26AP,SH_OBS26A,SH_OBS26AT,"499");
				}
				else if(this.type==10)
				{
					cf.showing_limit(s.toString(),SH_OBS26BP,SH_OBS26B,SH_OBS26BT,"499");
				}
				else if(this.type==11)
				{
					cf.showing_limit(s.toString(),SH_OBS26CP,SH_OBS26C,SH_OBS26CT,"499");
				}
				else if(this.type==12)
				{
					cf.showing_limit(s.toString(),SH_OBS27P,SH_OBS27,SH_OBS27T,"499");
				}
				else if(this.type==13)
				{
					cf.showing_limit(s.toString(),SH_OBSADDP,SH_OBSADD,SH_OBSADDT,"999");
				}
				
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}
			
		}
	 class Obs_clicker implements OnClickListener	  {

		  private static final int visibility = 0;

		@Override
		  public void onClick(View v) {

		   // TODO Auto-generated method stub
			  switch(v.getId()){
			  case R.id.firstobsrow: /*SELECTING OBSERVATION23 QUESTION ROW*/ 
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs23.setVisibility(visibility);
				  cf.tbl_row_obs23.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs23_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.secondobsrow: /*SELECTING OBSERVATION24 QUESTION ROW*/ 
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs24.setVisibility(visibility);
				  cf.tbl_row_obs24.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs24_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.thirdobsrow: /*SELECTING OBSERVATION25 QUESTION ROW*/ 
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs25.setVisibility(visibility);
				  cf.tbl_row_obs25.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs25_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.fourthobsrow: /*SELECTING OBSERVATION26 QUESTION ROW*/ 
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs26.setVisibility(visibility);
				  cf.tbl_row_obs26.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs26_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.fifthobsrow: /*SELECTING OBSERVATION27 QUESTION ROW*/ 
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs27.setVisibility(visibility);
				  cf.tbl_row_obs27.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs27_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.obs23_rd_y:  /*SELECTING OBSERVATION23 RADIOBUTTON YES */
				  cf.etcomments_obs23.setText("");
				  rd_CrawlSpacePresent=retrieve_radioname(cf.rd_obs23_yes);
				 break;
			  case R.id.obs23_opta_rd_yes:  /*SELECTING OBSERVATION23-OPTA RADIOBUTTON YES */
				  cf.etcomments_obs23_opta.setText("");
				  rd_StandingWaterNoted=retrieve_radioname(cf.rd_obs23_opta_yes);
				 break;
			  case R.id.obs23_optb_rd_yes:  /*SELECTING OBSERVATION23-OPTB RADIOBUTTON YES */
				  cf.etcomments_obs23_optb.setText("");
				  rd_LeakingPlumbNoted=retrieve_radioname(cf.rd_obs23_optb_yes);
				 break;
			  case R.id.obs23_optc_rd_yes:  /*SELECTING OBSERVATION23-OPTC RADIOBUTTON YES */
				  cf.etcomments_obs23_optc.setText("");
				  rd_VentilationNoted=retrieve_radioname(cf.rd_obs23_optc_yes);
				 break;
			  case R.id.obs23_optd_rd_yes:  /*SELECTING OBSERVATION23-OPTD RADIOBUTTON YES */
				  cf.etcomments_obs23_optd.setText("");
				  rd_CrawlSpaceAccessible=retrieve_radioname(cf.rd_obs23_optd_yes);
				 break;
			  case R.id.obs24_rd_y:  /*SELECTING OBSERVATION24 RADIOBUTTON YES */
				  cf.etcomments_obs24.setText("");
				  rd_RoofSagNoted=retrieve_radioname(cf.rd_obs24_yes);
				 break;
			  case R.id.obs25_rd_y:  /*SELECTING OBSERVATION25 RADIOBUTTON YES */
				  cf.etcomments_obs25.setText("");
				  rd_RoofLeakageEvidence=retrieve_radioname(cf.rd_obs25_yes);
				 break;
			  case R.id.obs26_rd_y:  /*SELECTING OBSERVATION26 RADIOBUTTON YES */
				  cf.etcomments_obs26.setText("");
				  rd_DeferredMaintanenceIssuesNoted=retrieve_radioname(cf.rd_obs26_yes);
				 break;
			  case R.id.obs26_opta_rd_yes:  /*SELECTING OBSERVATION26-OPTA RADIOBUTTON YES */
				  cf.etcomments_obs26_opta.setText("");
				  rd_DeterioratedNoted=retrieve_radioname(cf.rd_obs26_opta_yes);
				 break;
			  case R.id.obs26_optb_rd_yes:  /*SELECTING OBSERVATION26-OPTB RADIOBUTTON YES */
				  cf.etcomments_obs26_optb.setText("");
				  rd_DeterioratedExteriorFinishesNoted=retrieve_radioname(cf.rd_obs26_optb_yes);
				 break;
			  case R.id.obs26_optc_rd_yes:  /*SELECTING OBSERVATION26-OPTC RADIOBUTTON YES */
				  cf.etcomments_obs26_optc.setText("");
				  rd_DeterioratedExteriorSidingNoted=retrieve_radioname(cf.rd_obs26_optc_yes);
				 break;
			 
			  case R.id.obs27_rd_y:  /*SELECTING OBSERVATION27 RADIOBUTTON YES */
				  cf.etcomments_obs27.setText("");
				  rd_SafetyNoted=retrieve_radioname(cf.rd_obs27_yes);
				 break;
			  case R.id.obs23_rd_n:  /*SELECTING OBSERVATION23 RADIOBUTTON NO*/ 
				  cf.etcomments_obs23.setText(cf.obs4nostr);
				  rd_CrawlSpacePresent=retrieve_radioname(cf.rd_obs23_no);
				  rd_StandingWaterNoted=retrieve_radioname(cf.rd_obs23_opta_na);
				  rd_LeakingPlumbNoted=retrieve_radioname(cf.rd_obs23_optb_na);
				  rd_VentilationNoted=retrieve_radioname(cf.rd_obs23_optc_na);
				  rd_CrawlSpaceAccessible=retrieve_radioname(cf.rd_obs23_optd_na);
				  cf.rd_obs23_opta_na.setChecked(true);
                  cf.rd_obs23_optb_na.setChecked(true);
                  cf.rd_obs23_optc_na.setChecked(true);
                  cf.rd_obs23_optd_na.setChecked(true);
                  cf.etcomments_obs23_opta.setText(cf.obs4nostr);
                  cf.etcomments_obs23_optb.setText(cf.obs4nostr);
                  cf.etcomments_obs23_optc.setText(cf.obs4nostr);
                  cf.etcomments_obs23_optd.setText(cf.obs4nostr);
				 break;
			  case R.id.obs23_opta_rd_no:  /*SELECTING OBSERVATION23-OPTA RADIOBUTTON NO*/ 
				  cf.etcomments_obs23_opta.setText(cf.nostr);
				  rd_StandingWaterNoted=retrieve_radioname(cf.rd_obs23_opta_no);
				 break;
			  case R.id.obs23_optb_rd_no:  /*SELECTING OBSERVATION23-OPTB RADIOBUTTON NO*/ 
				  cf.etcomments_obs23_optb.setText(cf.nostr);
				  rd_LeakingPlumbNoted=retrieve_radioname(cf.rd_obs23_optb_no);
				 break;
			  case R.id.obs23_optc_rd_no:  /*SELECTING OBSERVATION23-OPTC RADIOBUTTON NO*/ 
				  cf.etcomments_obs23_optc.setText(cf.nostr);
				  rd_VentilationNoted=retrieve_radioname(cf.rd_obs23_optc_no);
				 break;
			  case R.id.obs23_optd_rd_no:  /*SELECTING OBSERVATION23-OPTD RADIOBUTTON NO*/ 
				  cf.etcomments_obs23_optd.setText(cf.nostr);
				  rd_CrawlSpaceAccessible=retrieve_radioname(cf.rd_obs23_optd_no);
				 break;
			  case R.id.obs24_rd_n:  /*SELECTING OBSERVATION24 RADIOBUTTON NO*/ 
				  cf.etcomments_obs24.setText(cf.nostr);
				  rd_RoofSagNoted=retrieve_radioname(cf.rd_obs24_no);
				 break;
			  case R.id.obs25_rd_n:  /*SELECTING OBSERVATION25 RADIOBUTTON NO*/ 
				  cf.etcomments_obs25.setText(cf.nostr);
				  rd_RoofLeakageEvidence=retrieve_radioname(cf.rd_obs25_no);
				 break;
			  case R.id.obs26_rd_n:  /*SELECTING OBSERVATION26 RADIOBUTTON NO*/ 
				  cf.etcomments_obs26.setText(cf.nostr);
				  rd_DeferredMaintanenceIssuesNoted=retrieve_radioname(cf.rd_obs26_no);
				 break;
			  case R.id.obs26_opta_rd_no:  /*SELECTING OBSERVATION26-OPTA RADIOBUTTON NO*/ 
				  cf.etcomments_obs26_opta.setText(cf.nostr);
				  rd_DeterioratedNoted=retrieve_radioname(cf.rd_obs26_opta_no);
				 break;
			  case R.id.obs26_optb_rd_no:  /*SELECTING OBSERVATION26-OPTB RADIOBUTTON NO*/ 
				  cf.etcomments_obs26_optb.setText(cf.nostr);
				  rd_DeterioratedExteriorFinishesNoted=retrieve_radioname(cf.rd_obs23_optb_no);
				 break;
			  case R.id.obs26_optc_rd_no:  /*SELECTING OBSERVATION26-OPTC RADIOBUTTON NO*/ 
				  cf.etcomments_obs26_optc.setText(cf.nostr);
				  rd_DeterioratedExteriorSidingNoted=retrieve_radioname(cf.rd_obs23_optc_no);
				 break;
			 
			  case R.id.obs27_rd_n:  /*SELECTING OBSERVATION27 RADIOBUTTON NO*/ 
				  cf.etcomments_obs27.setText(cf.nostr);
				  rd_SafetyNoted=retrieve_radioname(cf.rd_obs27_no);
				 break;
			  case R.id.obs23_opta_rd_nd:  /*SELECTING OBSERVATION23-OPTA RADIOBUTTON NOTDETERMINED*/ 
				  cf.etcomments_obs23_opta.setText(cf.obs4nostr);
				  rd_StandingWaterNoted=retrieve_radioname(cf.rd_obs23_opta_na);
				 break;
			  case R.id.obs23_optb_rd_nd:  /*SELECTING OBSERVATION23-OPTB RADIOBUTTON NOTDETERMINED*/ 
				  cf.etcomments_obs23_optb.setText(cf.obs4nostr);
				  rd_LeakingPlumbNoted=retrieve_radioname(cf.rd_obs23_optb_na);
				 break;
			  case R.id.obs23_optc_rd_nd:  /*SELECTING OBSERVATION23-OPTC RADIOBUTTON NOTDETERMINED*/ 
				  cf.etcomments_obs23_optc.setText(cf.obs4nostr);
				  rd_VentilationNoted=retrieve_radioname(cf.rd_obs23_optc_na);
				 break;
			  case R.id.obs23_optd_rd_nd:  /*SELECTING OBSERVATION23-OPTD RADIOBUTTON NOTDETERMINED*/ 
				  cf.etcomments_obs23_optd.setText(cf.obs4nostr);
				  rd_CrawlSpaceAccessible=retrieve_radioname(cf.rd_obs23_optd_na);
				 break;
			  case R.id.obs24_rd_nd:  /*SELECTING OBSERVATION24 RADIOBUTTON NOTDETERMINED*/ 
				  cf.etcomments_obs24.setText("");
				  rd_RoofSagNoted=retrieve_radioname(cf.rd_obs24_nd);
				 break;
			  case R.id.obs25_rd_nd:  /*SELECTING OBSERVATION25 RADIOBUTTON NOTDETERMINED*/ 
				  cf.etcomments_obs25.setText("");
				  rd_RoofLeakageEvidence=retrieve_radioname(cf.rd_obs25_nd);
				  cf.etcomments_obs25.setText(cf.obs4nastr);
				 break;
			  case R.id.obs26_rd_nd:  /*SELECTING OBSERVATION26 RADIOBUTTON NOTDETERMINED*/ 
				  cf.etcomments_obs26.setText(cf.obs4nastr);
				  rd_DeferredMaintanenceIssuesNoted=retrieve_radioname(cf.rd_obs26_nd);
				  rd_DeterioratedNoted=retrieve_radioname(cf.rd_obs26_opta_nd);
				  rd_DeterioratedExteriorFinishesNoted=retrieve_radioname(cf.rd_obs26_optb_nd);
				  rd_DeterioratedExteriorSidingNoted=retrieve_radioname(cf.rd_obs26_optc_nd);
				  cf.rd_obs26_opta_nd.setChecked(true);
                  cf.rd_obs26_optb_nd.setChecked(true);
                  cf.rd_obs26_optc_nd.setChecked(true);
                  cf.etcomments_obs26_opta.setText(cf.obs4nastr);
                  cf.etcomments_obs26_optb.setText(cf.obs4nastr);
                  cf.etcomments_obs26_optc.setText(cf.obs4nastr);
                  break;
			  case R.id.obs26_opta_rd_nd:  /*SELECTING OBSERVATION26-OPTA RADIOBUTTON NOTDETERMINED*/ 
				  cf.etcomments_obs26_opta.setText("");
				  rd_DeterioratedNoted=retrieve_radioname(cf.rd_obs26_opta_nd);
				 break;
			  case R.id.obs26_optb_rd_nd:  /*SELECTING OBSERVATION26-OPTB RADIOBUTTON NOTDETERMINED*/ 
				  cf.etcomments_obs26_optb.setText("");
				  rd_DeterioratedExteriorFinishesNoted=retrieve_radioname(cf.rd_obs26_optb_nd);
				 break;
			  case R.id.obs26_optc_rd_nd:  /*SELECTING OBSERVATION26-OPTC RADIOBUTTON NOTDETERMINED*/ 
				  cf.etcomments_obs26_optc.setText("");
				  rd_DeterioratedExteriorSidingNoted=retrieve_radioname(cf.rd_obs26_optc_nd);
				 break;
			 
			  case R.id.obs27_rd_nd:  /*SELECTING OBSERVATION27 RADIOBUTTON NOTDETERMINED*/ 
				  cf.etcomments_obs27.setText(cf.obs4nastr);
				  rd_SafetyNoted=retrieve_radioname(cf.rd_obs27_nd);
				 break;
			  
			  }
		}
	 }
	 
	 public void clicker(View v) {
         switch(v.getId()){
			  case R.id.obs23_help: /*HELP FOR OBSERVATION23 */
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Crawl space areas can be " +
				  		"host to many issues including leaking drainage systems, termite and " +
				  		"moisture damage to the sub-floor structure and settlement or washout to " +
				  		"the ground area underneath.  Many times homeowners conduct unauthorized " +
				  		"repairs or amateur repairs to the building structures and support, all " +
				  		"of which could cause localized settlement/movement.</p><p>Not properly " +
				  		"ventilated, the structure itself may have suffered as a result of " +
				  		"moisture - vapor build up/decay underneath.  Inspectors are " +
				  		"required to record the general condition of the underside of the surface " +
				  		"of the crawlspace if one is present as it relates to potential " +
				  		"settlement or sinkholes.</p><p>On older homes it can almost be assumed " +
				  		"that decay and rot of some nature from previous or ongoing leakage can " +
				  		"be found around the water fixtures from bathrooms, kitchens, water " +
				  		"heaters, etc.  within the crawlspace</p><p>The internal inspection " +
				  		"should determine what areas of uneven flooring -  localized settlement.  " +
				  		"If areas of abnormal settlement are noted initially, these need to be " +
				  		"accessed and inspected.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs23_opta_help: /*HELP FOR OBSERVATION23-OPTA */
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Standing water within " +
				  		"crawlspaces under homes can cause a multitude of problems including but " +
				  		"not limited to rot, decay, mold, health issues, to mention a " +
				  		"view.</p><p>Standing water can, in addition cause washout or settlement " +
				  		"- subsidence to the main structure, and this is the core purpose " +
				  		"of our inspection.</p><p>If standing water is noted, inspectors must " +
				  		"identify the area(s) concerned and recommend additional evaluation of " +
				  		"the structure.  Any limitations of the inspection of the crawlspace area " +
				  		"should be recorded.</p><p>Within the inspection report, inspectors must " +
				  		"recommend that the standing water be reviewed and evaluated and the area " +
				  		"remediated to prevent standing water and potential water issues.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs23_optb_help: /*HELP FOR OBSERVATION23-OPTB */
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>During the course of the " +
				  		"inspection, Inspectors should attempt to identify any plumbing leakages " +
				  		"visible from the exterior perimeter for areas inspected, and that a full " +
				  		"detailed plumbing inspection is beyond the scope of this " +
				  		"inspection.</p><p>Any issues noted should be photographed and recorded " +
				  		"as part of the inspection service.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs23_optc_help: /*HELP FOR OBSERVATION23-OPTC */
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>An inadequately " +
				  		"ventilated crawlspace can cause potential rot and decay to the main " +
				  		"floor structure and unevenness to the floor as a result of floor " +
				  		"structure failure.  This is particularly prevalent to areas where the " +
				  		"undercarriage is exposed with limited ventilation and the water table is " +
				  		"high or standing water was present within the crawlspace " +
				  		"area.</p><p>Proper ventilation is crucial for the life cycle of the " +
				  		"building in question.</p><p>Any issues associated with ventilation or " +
				  		"lack thereof should be recorded and photographed accordingly.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs23_optd_help: /*HELP FOR OBSERVATION23-OPTD */
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors are required " +
				  		"to examine any home where a crawlspace is present from the underside of " +
				  		"the home if accessible.  If inaccessible, this should be outlined within " +
				  		"the report and photographed in detail accordingly.</p><p>Deferred " +
				  		"Maintenance should be marked \"Not Applicable\" unless " +
				  		"otherwise instructed by client</p><p>If maintenance or hazardous issues " +
				  		"are required to be collected at the time of the inspection, then " +
				  		"inspector should clearly outline and comment with ample photography any " +
				  		"issues found associated with these sections of the inspection " +
				  		"report</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs24_help: /*HELP FOR OBSERVATION24 */
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Every attic area must be " +
				  		"inspected as part of this inspection process.</p><p>Attic inspections " +
				  		"are even more important where settlement issues are noted and inspectors " +
				  		"are trying to determine the load bearing walls versus non-load-bearing " +
				  		"walls for the purposes of those settlement issues being " +
				  		"observed.</p><p>The exterior inspection will determine abnormal " +
				  		"settlement issues with regard to the roof lines insofar as settlement or " +
				  		"roof sags, however, issues with roof lines from a perspective of roof " +
				  		"settlement or sagging is more often associated with poor construction " +
				  		"techniques or alterations conducted by the home owner or an unlicensed " +
				  		"contractor.</p><p>Where roof sags or settlement is noted, these should " +
				  		"be properly identified in the comment area and photographs in addition " +
				  		"to the attic area.</p><p>Roof sagging or deflection as a result of poor " +
				  		"construction practices is not associated with sinkhole or structural " +
				  		"activity, but if noted, should be outlined on the report in addition to " +
				  		"the probable cause.</p><p>Deferred Maintenance should be marked \"Not Applicable\" unless otherwise instructed by " +
		  				"client.</p><p>If maintenance or hazardous issues are required to be " +
		  				"collected at the time of the inspection, then inspector should clearly " +
		  				"outline and comment with ample photography any issues found associated " +
		  				"with these sections of the inspection report.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs25_help: /*HELP FOR OBSERVATION25 */
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Roof leakage damage as it " +
				  		"relates to this inspection is associated with any roof sagging or " +
				  		"surface unevenness that may be associated with settlement or sinkhole " +
				  		"issues.  Any sagging or surface issues - unevenness must be " +
				  		"reported and analyzed during the attic and exterior inspections and " +
				  		"reported and recorded accordingly to eliminate any connection with " +
				  		"sinkhole or settlement issues.</p><p>Deferred Maintenance should be " +
				  		"marked \"Not Applicable\" unless otherwise instructed by " +
				  		"client.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs26_help: /*HELP FOR OBSERVATION26 */
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Deferred Maintenance " +
				  		"should be marked \"Not Applicable\" unless otherwise " +
				  		"instructed by client.</p><p>If maintenance or hazardous issues are " +
				  		"required to be collected at the time of the inspection, then inspector " +
				  		"should clearly outline and comment with ample photography any issues " +
				  		"found associated with these sections of the inspection report.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.addendum_help: /*HELP FOR ADDEDNUM */
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors should use the " +
				  		"Comment Box to clearly outline any other conditions or issues relating " +
				  		"to the inspection at hand.</p><p>If inspectors are unable to properly " +
				  		"document the findings of any of the questions listed below and the " +
				  		"associated comment boxes, then inspectors must use the comment box below " +
				  		"to conclude the finding noted above.</p><p>If the comment box below is " +
				  		"used to amplify conditions noted above, inspectors should clearly start " +
				  		"the additional comments with the question number and question and state " +
				  		"if this comment refers to the relevant question above and refer to " +
				  		"relevant pictures and comments/disclosure for the relevant " +
				  		"question.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.home: /*SELECTING HOME */
				  cf.gohome();
				  break;
			  case R.id.save: /*SELECTING SAVE*/
				  fn_save();
				  break;
         }
	 }
	 public String retrieve_radioname(RadioButton rd_obs23_yes) {
		return rd_obs23_yes.getText().toString();
		// TODO Auto-generated method stub
		
	}
	public void fn_save() {
		// TODO Auto-generated method stub
		if(rd_CrawlSpacePresent.equals(""))/* CHECK QUESTION NO.23 RADIO BUTON*/
		{
			
			cf.ShowToast("Please select any answer for Question No.23", 0);
			cf.rd_obs23_yes.requestFocus();
		}
		else
		{
			if(cf.etcomments_obs23.getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.23 */
			{
				cf.ShowToast("Please enter Question No.23 Comments", 0);
				cf.etcomments_obs23.requestFocus();
			}
			else
			{
				if(rd_StandingWaterNoted.equals(""))/* CHECK QUESTION NO.23 a RADIO BUTON*/
				{
					cf.ShowToast("Please select any answer for Question No.23(a)", 0);
					cf.rd_obs23_opta_yes.requestFocus();
				}
				else
				{
					if(cf.etcomments_obs23_opta.getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.23 (a)*/
					{
						cf.ShowToast("Please enter Question No.23(a) Comments", 0);
						cf.etcomments_obs23_opta.requestFocus();
					}
					else
					{
						if(rd_LeakingPlumbNoted.equals(""))/* CHECK QUESTION NO.23(b) RADIO BUTON*/
						{
							cf.ShowToast("Please select any answer for Question No.23(b)", 0);
							cf.rd_obs23_optb_yes.requestFocus();
						}
						else
						{
							if(cf.etcomments_obs23_optb.getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.23 (b)*/
							{
								cf.ShowToast("Please enter Question No.23(b) Comments", 0);
								cf.etcomments_obs23_optb.requestFocus();
							}
							else
							{
								if(rd_VentilationNoted.equals(""))/* CHECK QUESTION NO.23 (c) RADIO BUTON*/
								{
									cf.ShowToast("Please select any answer for Question No.23(c)", 0);
									cf.rd_obs23_optc_yes.requestFocus();
								}
								else
								{
									if(cf.etcomments_obs23_optc.getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.23 (c)*/
									{
										cf.ShowToast("Please enter Question No.23(c) Comments", 0);
										cf.etcomments_obs23_optc.requestFocus();
									}
									else
									{
										if(rd_CrawlSpaceAccessible.equals(""))/* CHECK QUESTION NO.23(d) RADIO BUTON*/
										{
											cf.ShowToast("Please select any answer for Question No.23(d)", 0);
											cf.rd_obs23_optd_yes.requestFocus();
										}
										else
										{
											if(cf.etcomments_obs23_optd.getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.23 (d)*/
											{
												cf.ShowToast("Please enter Question No.23(d) Comments", 0);
												cf.etcomments_obs23_optd.requestFocus();
											}
											else
											{
												if(rd_RoofSagNoted.equals(""))/* CHECK QUESTION NO.24 RADIO BUTON*/
												{
													cf.ShowToast("Please select any answer for Question No.24", 0);
													cf.rd_obs24_yes.requestFocus();
												}
												else
												{
													if(cf.etcomments_obs24.getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.24*/
													{
														cf.ShowToast("Please enter Question No.24 Comments", 0);
														cf.etcomments_obs24.requestFocus();
													}
													else
													{
														if(rd_RoofLeakageEvidence.equals(""))/* CHECK QUESTION NO.25 RADIO BUTON*/
														{
															cf.ShowToast("Please select any answer for Question No.25", 0);
															cf.rd_obs25_yes.requestFocus();
														}
														else
														{
															if(cf.etcomments_obs25.getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.25*/
															{
																cf.ShowToast("Please enter Question No.25 Comments", 0);
																cf.etcomments_obs25.requestFocus();
															}
															else
															{
																if(rd_DeferredMaintanenceIssuesNoted.equals(""))/* CHECK QUESTION NO.26 RADIO BUTON*/
																{
																	cf.ShowToast("Please select any answer for Question No.26", 0);
																	cf.rd_obs26_yes.requestFocus();
																}
																else
																{
																	if(cf.etcomments_obs26.getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.26*/
																	{
																		cf.ShowToast("Please enter Question No.26 Comments", 0);
																		cf.etcomments_obs26.requestFocus();
																	}
																	else
																	{
																		if(rd_DeterioratedNoted.equals(""))/* CHECK QUESTION NO.26(a) RADIO BUTON*/
																		{
																			cf.ShowToast("Please select any answer for Question No.26(a)", 0);
																			cf.rd_obs26_opta_yes.requestFocus();
																		}
																		else
																		{
																			if(cf.etcomments_obs26_opta.getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.26(a)*/
																			{
																				cf.ShowToast("Please enter Question No.26(a) Comments", 0);
																				cf.etcomments_obs26_opta.requestFocus();
																			}
																			else
																			{
																				if(rd_DeterioratedExteriorFinishesNoted.equals(""))/* CHECK QUESTION NO.26(b) RADIO BUTON*/
																				{
																					cf.ShowToast("Please select any answer for Question No.26(b)", 0);
																					cf.rd_obs26_optb_yes.requestFocus();
																				}
																				else
																				{
																					if(cf.etcomments_obs26_optb.getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.26(b)*/
																					{
																						cf.ShowToast("Please enter Question No.26(b) Comments", 0);
																						cf.etcomments_obs26_optb.requestFocus();
																					}
																					else
																					{
																						if(rd_DeterioratedExteriorSidingNoted.equals(""))/* CHECK QUESTION NO.26(c) RADIO BUTON*/
																						{
																							cf.ShowToast("Please select any answer for Question No.26(c)", 0);
																							cf.rd_obs26_optc_yes.requestFocus();
																						}
																						else
																						{
																							if(cf.etcomments_obs26_optc.getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.26(c)*/
																							{
																								cf.ShowToast("Please enter Question No.26(c) Comments", 0);
																								cf.etcomments_obs26_optc.requestFocus();
																							}
																							else
																							{
																								if(rd_SafetyNoted.equals(""))/* CHECK QUESTION NO.27 RADIO BUTON*/
																								{
																									cf.ShowToast("Please select any answer for Question No.27", 0);
																									cf.rd_obs27_yes.requestFocus();
																								}
																								else
																								{
																									if(cf.etcomments_obs27.getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.27*/
																									{
																										cf.ShowToast("Please enter Question No.27 Comments", 0);
																										cf.etcomments_obs27.requestFocus();
																									}
																									else
																									{
																										if(cf.addednumcomments.getText().toString().trim().equals(""))/* CHECK ADDEDNUM COMMENTS*/
																										{
																											cf.ShowToast("Please enter Addendum's Comments", 0);
																											cf.addednumcomments.requestFocus();
																										}
																										else
																										{
																											OBS4_InsertOrUpdate();
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}

											}
										}

									}
								}
							}
						}
					}
				}
			}
		}
	}
	private void OBS4_InsertOrUpdate() {
		// TODO Auto-generated method stub
		try
		{
			Cursor OBS_save=cf.SelectTablefunction(cf.Observation4, " where SH_OBS4_SRID='"+cf.encode(cf.selectedhomeid)+"'");
			if(OBS_save.getCount()>0)
			{
				/* UPDATE THE OBSERVATION4 TABLE*/
				cf.sh_db.execSQL("UPDATE "+cf.Observation4+ " SET CrawlSpacePresent='"+cf.encode(rd_CrawlSpacePresent)+"',CrawlSpaceComments='"+cf.encode(cf.etcomments_obs23.getText().toString())+"'," 
						+"StandingWaterNoted='"+cf.encode(rd_StandingWaterNoted)+"',StandingWaterComments='"+cf.encode(cf.etcomments_obs23_opta.getText().toString())+"',"
						+"LeakingPlumbNoted='"+cf.encode(rd_LeakingPlumbNoted)+"',LeakingPlumbComments='"+cf.encode(cf.etcomments_obs23_optb.getText().toString())+"',"
						+"VentilationNoted='"+cf.encode(rd_VentilationNoted)+"',VentilationComments='"+cf.encode(cf.etcomments_obs23_optc.getText().toString())+"',"
						+"CrawlSpaceAccessible='"+cf.encode(rd_CrawlSpaceAccessible)+"',CrawlSpaceAccessibleComments='"+cf.encode(cf.etcomments_obs23_optd.getText().toString())+"',"
						+"RoofSagNoted='"+cf.encode(rd_RoofSagNoted)+"',RoofSagComments='"+cf.encode(cf.etcomments_obs24.getText().toString())+"',"
						+"RoofLeakageEvidence='"+cf.encode(rd_RoofLeakageEvidence)+"',RoofLeakageComments='"+cf.encode(cf.etcomments_obs25.getText().toString())+"',"
						+"DeferredMaintanenceIssuesNoted='"+cf.encode(rd_DeferredMaintanenceIssuesNoted)+"',DeferredMaintanenceComments='"+cf.encode(cf.etcomments_obs26.getText().toString())+"',"
						+"DeterioratedNoted='"+cf.encode(rd_DeterioratedNoted)+"',DeterioratedComments='"+cf.encode(cf.etcomments_obs26_opta.getText().toString())+"',"
						+"DeterioratedExteriorFinishesNoted='"+cf.encode(rd_DeterioratedExteriorFinishesNoted)+"',DeterioratedExteriorFinishesComments='"+cf.encode(cf.etcomments_obs26_optb.getText().toString())+"',"
						+"DeterioratedExteriorSidingNoted='"+cf.encode(rd_DeterioratedExteriorSidingNoted)+"',DeterioratedExteriorSidingComments='"+cf.encode(cf.etcomments_obs26_optc.getText().toString())+"',"
						+"SafetyNoted='"+cf.encode(rd_SafetyNoted)+"',SafetyComments='"+cf.encode(cf.etcomments_obs27.getText().toString())+"',"
						+"OverallComments='"+cf.encode(cf.addednumcomments.getText().toString())+"' Where SH_OBS4_SRID='"+cf.encode(cf.selectedhomeid)+"'");
				cf.ShowToast("Observation 23-27 has been updated sucessfully.", 1);
				nextlayout();
			}
			else
			{
				/* INSERT INTO OBSERVATION4 TABLE*/
				try
				{
					cf.sh_db.execSQL("INSERT INTO "+cf.Observation4+ "(SH_OBS4_SRID,CrawlSpacePresent,CrawlSpaceComments,StandingWaterNoted,StandingWaterComments,LeakingPlumbNoted,LeakingPlumbComments,VentilationNoted,VentilationComments,CrawlSpaceAccessible,CrawlSpaceAccessibleComments,RoofSagNoted,RoofSagComments,RoofLeakageEvidence,RoofLeakageComments,DeferredMaintanenceIssuesNoted,DeferredMaintanenceComments,DeterioratedNoted,DeterioratedComments,DeterioratedExteriorFinishesNoted,DeterioratedExteriorFinishesComments,DeterioratedExteriorSidingNoted,DeterioratedExteriorSidingComments,SafetyNoted,SafetyComments,OverallComments)" +
							"VALUES ('"+cf.encode(cf.selectedhomeid)+"','"+cf.encode(rd_CrawlSpacePresent)+"','"+cf.encode(cf.etcomments_obs23.getText().toString())+"','"+cf.encode(rd_StandingWaterNoted)+"','"+cf.encode(cf.etcomments_obs23_opta.getText().toString())+"','"+cf.encode(rd_LeakingPlumbNoted)+"','"+cf.encode(cf.etcomments_obs23_optb.getText().toString())+"','"+cf.encode(rd_VentilationNoted)+"','"+cf.encode(cf.etcomments_obs23_optc.getText().toString())+"','"+cf.encode(rd_CrawlSpaceAccessible)+"','"+cf.encode(cf.etcomments_obs23_optd.getText().toString())+"','"+cf.encode(rd_RoofSagNoted)+"','"+cf.encode(cf.etcomments_obs24.getText().toString())+"','"+cf.encode(rd_RoofLeakageEvidence)+"','"+cf.encode(cf.etcomments_obs25.getText().toString())+"','"+cf.encode(rd_DeferredMaintanenceIssuesNoted)+"','"+cf.encode(cf.etcomments_obs26.getText().toString())+"','"+cf.encode(rd_DeterioratedNoted)+"','"+cf.encode(cf.etcomments_obs26_opta.getText().toString())+"','"+cf.encode(rd_DeterioratedExteriorFinishesNoted)+"','"+cf.encode(cf.etcomments_obs26_optb.getText().toString())+"','"+cf.encode(rd_DeterioratedExteriorSidingNoted)+"','"+cf.encode(cf.etcomments_obs26_optc.getText().toString())+"','"+cf.encode(rd_SafetyNoted)+"','"+cf.encode(cf.etcomments_obs27.getText().toString())+"','"+cf.encode(cf.addednumcomments.getText().toString())+"')");
							 cf.ShowToast("Observation 23-27 has been saved sucessfully.", 1);
							 nextlayout();
				}
				catch (Exception E)
				{
					String strerrorlog="Insert the data into Observation4 table not working ";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table Creation at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
		}
		catch(Exception e)
		{
			String strerrorlog="Selection of the Observation4 table not working ";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);

		}
		
	}
	private void nextlayout() {
		// TODO Auto-generated method stub
		Intent intimg = new Intent(Observation4.this,
				photos_signature.class);
		intimg.putExtra("homeid", cf.selectedhomeid);
	    intimg.putExtra("InspectionType", cf.onlinspectionid);
		intimg.putExtra("status", cf.onlstatus);
	    startActivity(intimg);
	}
	public void obs_tablelayout_hide() {
			// TODO Auto-generated method stub
		
			cf.tbl_layout_obs23.setVisibility(cf.show.GONE);
			cf.tbl_layout_obs24.setVisibility(cf.show.GONE);
			cf.tbl_layout_obs25.setVisibility(cf.show.GONE);
			cf.tbl_layout_obs26.setVisibility(cf.show.GONE);
			cf.tbl_layout_obs27.setVisibility(cf.show.GONE);
			
			cf.tbl_row_obs23.setBackgroundResource(R.drawable.backrepeatnor);
			cf.tbl_row_obs24.setBackgroundResource(R.drawable.backrepeatnor);
			cf.tbl_row_obs25.setBackgroundResource(R.drawable.backrepeatnor);
			cf.tbl_row_obs26.setBackgroundResource(R.drawable.backrepeatnor);
			cf.tbl_row_obs27.setBackgroundResource(R.drawable.backrepeatnor);
			
		}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(cf.strschdate.equals("")){
				cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
			}else{Intent  myintent = new Intent(Observation4.this,Observation3.class);
			cf.putExtras(myintent);
			startActivity(myintent);}
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {

		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			switch (resultCode) {
 			case 0:
 				break;
 			case -1:
 				try {

 					String[] projection = { MediaStore.Images.Media.DATA };
 					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
 							null, null, null);
 					int column_index_data = cursor
 							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
 					cursor.moveToFirst();
 					String capturedImageFilePath = cursor.getString(column_index_data);
 					cf.showselectedimage(capturedImageFilePath);
 				} catch (Exception e) {
 					
 				}
 				
 				break;

 		}

 	}

}

package idsoft.inspectiondepot.sinkholeinspection;
import java.io.BufferedReader;



import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.PublicKey;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteAbortException;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SinkholeInspectionActivity extends Activity {
	CommonFunctions cf;
	Button clear_pass;
	int show_handler;
	ProgressDialog progressDialog2;
	 /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf = new CommonFunctions(this);
        setContentView(R.layout.newlogin);
        Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String value = extras.get("back").toString();
			if (value.equals("exit")) {
				Intent startMain = new Intent(Intent.ACTION_MAIN);
				startMain.addCategory(Intent.CATEGORY_HOME);
				startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(startMain);
			}
		}
		
        /*INSPECTOR LOGIN CREATION TABLE*/
        cf.Create_Table(1);
       
        try {
            Cursor c_chkinsp = cf.SelectTablefunction(cf.inspectorlogin,
					"where Fld_InspectorFlag='1'");
			int r_chkinsp = c_chkinsp.getCount();
			if (r_chkinsp == 1) {
				Call_NextLayout();

			} else if (r_chkinsp > 1) {
				/** More than 0ne user logged in, so we log out the users **/
				cf.sh_db.execSQL("update " + cf.inspectorlogin
						+ " set Fld_InspectorFlag=0 where Fld_InspectorFlag=1");
				if(cf.application_sta)
				{
					Intent loginpage = new Intent(Intent.ACTION_MAIN);
					  loginpage.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
					  startActivity(loginpage);
				}
			}
			else if(r_chkinsp==0 && cf.application_sta)
	        {
	        	  Intent loginpage = new Intent(Intent.ACTION_MAIN);
				  loginpage.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
				  startActivity(loginpage);
	        }
		} catch (Exception e) {
			cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ getApplicationContext()+" "+" in the stage of Checking Rows in Inspector Login table at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
          
        /*DECLARATION OF EDIT TEXTBOX FOR USERNAME AND PASSWORD*/
      // public AutoCompleteTextView r; 
        cf.et_username = (AutoCompleteTextView) this.findViewById(R.id.eusername);
		cf.et_password = (EditText) this.findViewById(R.id.epwd);
	
		cf.et_username.addTextChangedListener(new AF_watcher());
        /*DECLARATION OF LOGIN BUTTION AND ITS CLICK EVENT*/
		clear_pass =(Button) findViewById(R.id.clear_pass);
		clear_pass.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cf.sh_db.execSQL("UPDATE "
						+ cf.inspectorlogin
						+ " SET Fld_Remember_pass=0"
						+ " WHERE Fld_InspectorUserName ='"
						+ cf.encode(cf.et_username.getText().toString())+ "'");
				clear_pass.setVisibility(View.GONE);
				cf.et_password.setText("");
			}
		});
	
        Button btn_login = (Button)findViewById(R.id.login);
        
        btn_login.setOnClickListener(new OnClickListener()
        {
        	 @Override
            public void onClick(View v) 
            {
        		 	if (!"".equals(cf.et_username.getText().toString())
						|| !"".equals(cf.et_password.getText().toString())) {/*CHECK FOR USERNAME AND PASSWORD NOT BE EMPTY*/
					if (!"".equals(cf.et_username.getText().toString())) {/*CHECK FOR USERNAME NOT BE EMPTY*/
						if (!"".equals(cf.et_password.getText()
								.toString())) {/*CHECK FOR PASSWORD NOT BE EMPTY*/
							Cursor sel_logcur = null;
								try{
									sel_logcur = cf.SelectTablefunction(cf.inspectorlogin,
												"where Fld_InspectorUserName='"+cf.encode(cf.et_username.getText().toString().toLowerCase())+ "'");
                              	int rws = sel_logcur.getCount();
                              	if(rws==0) 
								{      
									if(cf.isInternetOn()==true)/*CHECK FOR INTERNET CONNECTION*/
									{
										/*GETTING VALUES FROM WEBSERVICE*/
									cf.show_ProgressDialog("Processing ");
								
									new Thread() {
										public void run() {
											Looper.prepare();
											try {
												SoapObject chklogin = cf.Calling_WS(cf.et_username.getText().toString(),cf.et_password.getText()
														.toString(),"CheckUserAuthentication");
												SoapObject obj = (SoapObject) chklogin.getProperty(0);
												String chkauth = String.valueOf(obj.getProperty("userAuthentication"));
												if(chkauth.equals("true")) /*USERAUTHENTICATION IS TRUE*/
												{
													InsertData(obj);
												}
												else /*USERAUTHENTICATION IS FALSE*/
												{
													show_handler=1;
													handler.sendEmptyMessage(0);
													
													
												}
											} catch (SocketException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=3;
												handler.sendEmptyMessage(0);
												
											} catch (NetworkErrorException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=3;
												handler.sendEmptyMessage(0);
												
											} catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=3;
												handler.sendEmptyMessage(0);
												
											} catch (TimeoutException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=3;
												handler.sendEmptyMessage(0);
												
											} catch (XmlPullParserException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=3;
												handler.sendEmptyMessage(0);
												
											}catch (Exception e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=4;
												handler.sendEmptyMessage(0);
												
											}
											
											
										}
										private void InsertData(SoapObject chklogin) throws NetworkErrorException,
											SocketTimeoutException, IOException, XmlPullParserException,Exception{
										// TODO Auto-generated method stub
								    	cf.status = String.valueOf(chklogin.getProperty("AndroidStatus"));
										if (cf.status.equals("true")) {
											final String id = String.valueOf(chklogin.getProperty("userId"));
											
											cf.Device_Information();
											SoapObject add_property = cf.export_header("ExportDeviceInformation");
											add_property.addProperty("Deviceid", cf.deviceId);
											add_property.addProperty("ModelNumber", cf.model);
											add_property.addProperty("Manufacturer", cf.manuf);
											add_property.addProperty("OSVersion", cf.devversion);
											add_property.addProperty("APILevel", cf.apiLevel);
											add_property.addProperty("IPAddress", cf.ipAddress);
											add_property.addProperty("InspectorLd", id.toString());
											add_property.addProperty("Date", cf.datewithtime);
											String result_export_deviceinfo=cf.export_footer(cf.envelope,add_property,"ExportDeviceInformation");
											try{
												Cursor cur = cf.SelectTablefunction(cf.inspectorlogin,
													" where Fld_InspectorId = '" + id.toString()
															+ "' and Fld_InspectorFlag='1'");
											int rws = cur.getCount();
											if (rws == 0) {
												LoginInsert(id);
												String flag1 = "1";
												cf.sh_db.execSQL("UPDATE " + cf.inspectorlogin
														+ " SET Fld_InspectorFlag='" + flag1 + "'"
														+ " WHERE Fld_InspectorId ='" + id.toString() + "'");

											} else {

											}
                                            cf.pd.dismiss();
                                            show_handler=12;handler.sendEmptyMessage(0);
                                         
											}
											catch(Exception e)
											{
												cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ SinkholeInspectionActivity.this+" "+" in the stage of Checking rows of Inspector Login table while clicking login button at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
												
											}
										}
										else
										{
											show_handler=2;handler.sendEmptyMessage(0);
										}
										
									  }
										
										private void LoginInsert(
												String id) {
											// TODO Auto-generated method stub
											try {
												SoapObject chklogin = cf.Calling_WS1(id,"InspectorDetail");
												System.out.println("chklogin"+chklogin);
												cf.Insp_id = String.valueOf(chklogin.getProperty("Inspectorid"));
												cf.Insp_firstname = (String.valueOf(chklogin.getProperty("Inspectorfirstname")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectorfirstname")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectorfirstname")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectorfirstname"));
												cf.Insp_middlename =(String.valueOf(chklogin.getProperty("Inspectormiddlename")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectormiddlename")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectormiddlename")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectormiddlename"));
												cf.Insp_lastname = (String.valueOf(chklogin.getProperty("Inspectorlastname")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectorlastname")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectorlastname")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectorlastname"));
												cf.Insp_address = (String.valueOf(chklogin.getProperty("Inspectoraddress")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectoraddress")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectoraddress")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectoraddress"));
												cf.Insp_companyname = (String.valueOf(chklogin.getProperty("Inspectorcompanyname")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectorcompanyname")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectorcompanyname")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectorcompanyname"));
												cf.Insp_companyId = (String.valueOf(chklogin.getProperty("InspectorcompanyId")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("InspectorcompanyId")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("InspectorcompanyId")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("InspectorcompanyId"));
												cf.Insp_username = (String.valueOf(chklogin.getProperty("Inspectorusername")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectorusername")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectorusername")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectorusername"));
												cf.Insp_password = (String.valueOf(chklogin.getProperty("Inspectorpassword")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectorpassword")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectorpassword")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectorpassword"));
												cf.Insp_Photo = (String.valueOf(chklogin.getProperty("InspectorPhoto")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("InspectorPhoto")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("InspectorPhoto")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("InspectorPhoto"));
												cf.Insp_PhotoExtn = (String.valueOf(chklogin.getProperty("InspectorPhotoExt")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("InspectorPhotoExt")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("InspectorPhotoExt")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("InspectorPhotoExt"));
												cf.Insp_Status = (String.valueOf(chklogin.getProperty("AndroidStatus")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("AndroidStatus")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("AndroidStatus")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("AndroidStatus"));
												cf.Insp_email = (String.valueOf(chklogin.getProperty("InspectorEmail")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("InspectorEmail")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("InspectorEmail")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("InspectorEmail"));
												cf.Insp_flag1 = "0";
												System.out.println("cf.Insp_companyname"+cf.Insp_companyname);
											//	byte[] decode = Base64.decode(cf.Insp_Photo.toString(), 0);
												String headshot=(String.valueOf(chklogin.getProperty("Headshot")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Headshot")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Headshot")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Headshot"));
												
												try
												{
													URL ulrn = new URL(headshot);
												    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
												    InputStream is = con.getInputStream();
												    Bitmap bmp = BitmapFactory.decodeStream(is);
												   
												    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
												    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
												    byte[] b = baos.toByteArray();
												    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
												    byte[] decode = Base64.decode(headshot_insp.toString(), 0);

												   
												    try
													{
														String FILENAME = cf.Insp_id +cf.Insp_PhotoExtn;
														FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
														fos.write(decode);
														fos.close();	
													}
													catch (IOException e){
														System.out.println("we found isseus in the webservice "+e.getMessage());
													}
												    
												}
												catch (Exception e1){
												
													System.out.println("Imagge headshot disp = "+e1.getMessage());
												}
												/*try
												{
													String FILENAME = cf.Insp_id +cf.Insp_PhotoExtn;
													FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
													fos.write(decode);
													fos.close();	
												}
												catch (IOException e){
													cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ SinkholeInspectionActivity.this+" "+" in the stage of retrieving image bytes of Inspector photo from its webservice at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
												}*/
												
												try {

													cf.sh_db.execSQL("INSERT INTO "
															+ cf.inspectorlogin
															+ " (Fld_InspectorId,Fld_InspectorFirstName,Fld_InspectorMiddleName,Fld_InspectorLastName,Fld_InspectorAddress,Fld_InspectorCompanyName,Fld_InspectorCompanyId,Fld_InspectorUserName,Fld_InspectorPassword,Fld_InspectorPhotoExtn,Android_status,Fld_InspectorFlag,Fld_InspectorEmail)"
															+ " VALUES ('" + cf.Insp_id + "','"
															+ cf.encode(cf.Insp_firstname) + "','"
															+ cf.encode(cf.Insp_middlename) + "','"
															+ cf.encode(cf.Insp_lastname) + "','"
															+ cf.encode(cf.Insp_address) + "','"
															+ cf.encode(cf.Insp_companyname) + "','"
															+ cf.encode(cf.Insp_companyId) + "','"
															+ cf.encode(cf.Insp_username.toLowerCase())
															+ "','" + cf.encode(cf.Insp_password) + "','"+cf.Insp_PhotoExtn+"','"
															+  cf.status + "','" + cf.Insp_flag1 + "','"+ cf.encode(cf.Insp_email)+"')");
												} catch (Exception e) {
													cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ SinkholeInspectionActivity.this+" "+" in the inserting Inspector details in Inspector Login table at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
													
												}
											} catch (SocketException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=3;
												handler.sendEmptyMessage(0);
												
											} catch (NetworkErrorException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=3;
												handler.sendEmptyMessage(0);
												
											} catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=3;
												handler.sendEmptyMessage(0);
												
											} catch (TimeoutException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=3;
												handler.sendEmptyMessage(0);
												
											} catch (XmlPullParserException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=3;
												handler.sendEmptyMessage(0);
												
											}catch (Exception e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=4;
												handler.sendEmptyMessage(0);
												
											} 
											
										}
										private Handler handler = new Handler() {
											@Override
											public void handleMessage(Message msg) {
												cf.pd.dismiss();
												if(show_handler==1){
													show_handler=0;
												cf.ShowToast("Invalid UserName or Password.",1);
												cf.et_username.setText("");
												cf.et_password.setText("");
												cf.et_username.requestFocus();	
												}
												else if(show_handler==2)
												{
													show_handler=0;
													cf.ShowToast("You are not eligible to login.",1);
													
												}
												else if(show_handler==3)
												{
													show_handler=0;
													cf.ShowToast("There is a problem on your Network. Please try again later with better Network.",2);
													
												}
												else if(show_handler==4)
												{
													show_handler=0;
													cf.ShowToast("There is a problem on your application. Please contact Paperless administrator.",2);
													
												}
												else if(show_handler==12)
												{
													show_handler=0;
													Call_RememberPass();
												}
												
											}
										};
									}.start();
									}
									else
									{
										cf.ShowToast("Internet connection is not available.",1);
										
									}
								}
								else
								{
									/*GETTING VALUES FROM DATABASE*/
								
									sel_logcur.moveToFirst();
									
									if (cf.et_username.getText().toString().toLowerCase().equals(cf.decode(sel_logcur.getString(sel_logcur.getColumnIndex("Fld_InspectorUserName"))))
											&& cf.et_password.getText().toString().equals(cf.decode(sel_logcur.getString(sel_logcur.getColumnIndex("Fld_InspectorPassword"))))) {
										cf.sh_db.execSQL("UPDATE "
												+ cf.inspectorlogin
												+ " SET Fld_InspectorFlag=1"
												+ " WHERE Fld_InspectorId ='"
												+ sel_logcur.getString(sel_logcur.getColumnIndex("Fld_InspectorId")) + "'");
										
										Call_RememberPass();
									
									}
									else
									{
										cf.ShowToast("Invalid UserName or Password.",0);
										cf.et_username.setText("");
										cf.et_password.setText("");
										cf.et_username.requestFocus();	
									}
								}
								}
								catch(Exception e)
								{
								  cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ SinkholeInspectionActivity.this+" "+" in the stage of Checking Login Webservice at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
								  cf.ShowToast("Invalid UserName or Password.",0);
									cf.et_username.setText("");
									cf.et_password.setText("");
									cf.et_username.requestFocus();
								}
								
							
						}
						else
						{
							cf.ShowToast("Please enter Password.",0);
							cf.et_password.requestFocus();
						}
					}
					else
					{
						cf.ShowToast("Please enter UserName.",0);
						cf.et_username.requestFocus();
					}
            	}
            	else
            	{
            		cf.ShowToast("Please enter the UserName and Password.",0);
					cf.et_username.requestFocus();
            	}
            }
        });
        
        /*DECLARATION OF CANCEL BUTTON AND ITS CLICK EVENT*/        
        Button btn_cancellogin = (Button)findViewById(R.id.cancellogin);
        btn_cancellogin.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
            	Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
      
            }
        });
        if(!cf.application_sta)
        {
        	AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(SinkholeInspectionActivity.this);
			alertDialog1
		.setMessage("In order to use the latest version of Sinkhole Inspection, You must update IDMA. Please click below to update IDMA.");
		alertDialog1.setCancelable(false);
      	alertDialog1.setPositiveButton("Download",
		new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,
					int which) {
					progressDialog2 = ProgressDialog.show(SinkholeInspectionActivity.this, "","Downloading IDMA. Please wait...");
				new Thread() {
					private int usercheck = 0;
					private int myhandler;

					public void run() {
						Looper.prepare();
						if(cf.isInternetOn())
						{
						SoapObject webresult;
						SoapObject request = new SoapObject(cf.NAMESPACE, "GeAPKFile_IDMS");
						
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope.dotNet = true;
						//request.addProperty("InspectorID",cf.Insp_id);
						envelope.setOutputSoapObject(request);
						      
						HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);

						try {
							androidHttpTransport.call(cf.NAMESPACE+"GeAPKFile_IDMS", envelope);
							Object response = envelope.getResponse();
							byte[] b;
							if (cf.checkresponce(response)) // check the response is valid or
															// not
							{
								b = Base64.decode(response.toString(),Base64.DEFAULT);

								try {
									String PATH = Environment.getExternalStorageDirectory()
											+ "/Download/IDMA";
									File file = new File(PATH);
									file.mkdirs();

									File outputFile = new File(PATH + "/IDMA.apk");
									FileOutputStream fileOuputStream = new FileOutputStream(
											outputFile);
									fileOuputStream.write(b);
									fileOuputStream.close();

									cf.fn_logout(cf.Insp_id);  //FOR LOGOUT
									
									myhandler=2;
									//progressDialog1.dismiss();
									
								} catch (IOException e) {
									System.out.println("catch");
									myhandler=1;
								}
							}
							else {
								//progressDialog1.dismiss();
								myhandler=0;
								}
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							myhandler=0;
						} catch (XmlPullParserException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							myhandler=0;
						}
					}
						else
						{
							myhandler=25;
						}
						handler2.sendEmptyMessage(0);
					};
					private Handler handler2 = new Handler() {
						

						@Override
						public void handleMessage(Message msg) {
							
							progressDialog2.dismiss();
							
							if(myhandler==0)
							{
								cf.ShowToast("There is a problem on your Network.\n Please try again later with better Network.",1);
								startActivity(new Intent(SinkholeInspectionActivity.this,SinkholeInspectionActivity.class));
							}
							else if(myhandler==1)
							{
								cf.ShowToast("Update error!",1);
								startActivity(new Intent(SinkholeInspectionActivity.this,SinkholeInspectionActivity.class));
							}
							else if(myhandler==25)
							{
								cf.ShowToast("Please enable internet connection and try again.",1);
								startActivity(new Intent(SinkholeInspectionActivity.this,SinkholeInspectionActivity.class));
							}
							else if(myhandler==2)
							{
								Intent intent = new Intent(Intent.ACTION_VIEW);
								intent.setDataAndType(Uri.fromFile(new File(Environment
										.getExternalStorageDirectory()
										+ "/Download/IDMA/"
										+ "IDMA.apk")),
										"application/vnd.android.package-archive");
								startActivityForResult(intent,12);
							}
						}
					};}.start();
			

			}
		});
      	alertDialog1.setCancelable(false);
      	alertDialog1.show();
        }
    }
  
    protected void Call_NextLayout() {
		// TODO Auto-generated method stub
    	Intent iInspectionList = new Intent(SinkholeInspectionActivity.this,HomeScreen.class);
		startActivity(iInspectionList);
	}

	

    /*CLICK EVENT OF BACK KEY*/

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}
	public void Call_RememberPass() {
		// TODO Auto-generated method stub
	//	try{
		Cursor	sel_logcur = cf.SelectTablefunction(cf.inspectorlogin,
				"where Fld_InspectorUserName='"+ cf.encode(cf.et_username.getText().toString()).toLowerCase()+ "'");
		if(sel_logcur!=null)
		{
			if(sel_logcur.getCount()>=0)
			{
				sel_logcur.moveToFirst();
				if(sel_logcur.getString(sel_logcur.getColumnIndex("Fld_Remember_pass")).equals("0"))
				{
					Show_alert(sel_logcur);
				}
				else
				{
					Call_NextLayout();
				}
			}
			else
			{
				Call_NextLayout();
			}
		}
		else
		{
			Call_NextLayout();
		}
		
	}

	private void Show_alert(final Cursor sel_logcur) {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(SinkholeInspectionActivity.this,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alert);
			LinearLayout maintbl=(LinearLayout) dialog1.findViewById(R.id.maintable);
			maintbl.setVisibility(View.GONE);
			LinearLayout curtbl=(LinearLayout) dialog1.findViewById(R.id.remember_password);
			curtbl.setVisibility(View.VISIBLE);
		  	Button RP_yes = (Button) dialog1.findViewById(R.id.RP_yes);
		  	Button RP_no = (Button) dialog1.findViewById(R.id.RP_no);
			Button btn_helpclose_t = (Button) dialog1.findViewById(R.id.RP_close);
			
			btn_helpclose_t.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					Call_NextLayout();
			
				}
				
				
			});
			RP_yes.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					cf.sh_db.execSQL("UPDATE "
							+ cf.inspectorlogin
							+ " SET Fld_Remember_pass=1"
							+ " WHERE Fld_InspectorId ='"
							+ sel_logcur.getString(sel_logcur.getColumnIndex("Fld_InspectorId")) + "'");
					cf.ShowToast("Password has been saved sucessfully.", 1);
					Call_NextLayout();
				
				}	
			});
			RP_no.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					Call_NextLayout();
				
				}	
			});
			dialog1.setCancelable(false);
			dialog1.show();
 }
	 class AF_watcher implements TextWatcher
	 {

		@Override
		public void afterTextChanged(Editable arg0) {
			// TODO Auto-generated method stub
			try {
				Cursor cur1 = cf.sh_db.rawQuery("select * from "+ cf.inspectorlogin + " where Fld_InspectorUserName like '" + cf.encode(cf.et_username.getText().toString().toLowerCase()) + "%'",null);
			String[] autousername = new String[cur1.getCount()];
				cur1.moveToFirst();
				if(cur1.getCount()!=0)
				{
					if (cur1 != null) {
						int i = 0;
						do {
							autousername[i] = cf.decode(cur1.getString(cur1.getColumnIndex("Fld_InspectorUserName")));
							
							if (autousername[i].contains("null")) {
								autousername[i] = autousername[i].replace("null", "");
							}
							 
							i++;
						} while (cur1.moveToNext());
					}
					cur1.close();
				}
				 ArrayAdapter<String> adapter = new ArrayAdapter<String>(SinkholeInspectionActivity.this,R.layout.loginnamelist,autousername);
				 cf.et_username.setThreshold(1);
				 cf.et_username.setAdapter(adapter);
				 Cursor cur2 = cf.sh_db.rawQuery("select * from "+ cf.inspectorlogin + " where Fld_InspectorUserName = '" + cf.encode(cf.et_username.getText().toString().toLowerCase()) + "' and Fld_Remember_pass='1'",null);
				 /** Set the password for the remember option enable**/
				 cur2.moveToFirst();
				 if(cur2.getCount()>0)
				 {
					if(cf.decode(cur2.getString(cur2.getColumnIndex("Fld_InspectorUserName"))).equals(cf.et_username.getText().toString().toLowerCase()) && cur2.getString(cur2.getColumnIndex("Fld_Remember_pass")).equals("1"))
					{
						cf.et_password.setText(cf.decode(cur2.getString(cur2.getColumnIndex("Fld_InspectorPassword"))));
						clear_pass.setVisibility(View.VISIBLE);
					}
					else
					{
						cf.et_password.setText("");
						clear_pass.setVisibility(View.GONE);
					}
					/** Set the password for the remember option enable Ends here **/
				 }
				 else
					{
						cf.et_password.setText("");
						clear_pass.setVisibility(View.GONE);
					}
			}
			catch(Exception e)
			{
				cf.Error_LogFile_Creation("Error in the autofill of the login information query ");
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		 
	 }
	
	 @Override
		protected void onStop() {
			// TODO Auto-generated method stub
			
		//	finish();
			super.onStop();
		}
	 @Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			// TODO Auto-generated method stub
			super.onActivityResult(requestCode, resultCode, data);
			if(requestCode==12 && resultCode==RESULT_CANCELED)
			{
				startActivity(new Intent(this,SinkholeInspectionActivity.class));
				
			}
		//	System.out.println("comes in the on activity result"+resultCode+requestCode);
		} 
}
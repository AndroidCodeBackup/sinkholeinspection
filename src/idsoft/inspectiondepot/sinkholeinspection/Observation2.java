package idsoft.inspectiondepot.sinkholeinspection;

import idsoft.inspectiondepot.sinkholeinspection.Observation1.Obs_clicker;
import idsoft.inspectiondepot.sinkholeinspection.Observation3.SH_textwatcher;
import idsoft.inspectiondepot.sinkholeinspection.Observation4.Touch_Listener;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Observation2 extends Activity{
	LinearLayout SH_OBS8P,SH_OBS9P,SH_OBS10P,SH_OBS10AP,SH_OBS10BP,SH_OBS10CP,SH_OBS10DP,
	SH_OBS10EP,SH_OBS11P,SH_OBS12P,SH_OBS13P,SH_OBS8,SH_OBS9,SH_OBS10,SH_OBS10A,SH_OBS10B,
	SH_OBS10C,SH_OBS10D,SH_OBS10E,SH_OBS11,SH_OBS12,SH_OBS13;
	TextView SH_OBS8T,SH_OBS9T,SH_OBS10T,SH_OBS10AT,SH_OBS10BT,SH_OBS10CT,SH_OBS10DT,SH_OBS10ET,
	SH_OBS11T,SH_OBS12T,SH_OBS13T;
	String strhomeid,rd_PoolDeckSlabNoted="",rd_PoolShellPlumbLevel="",rd_ExcessiveSettlement="",rd_WallLeaningNoted="",rd_WallsVisiblyNotLevel="",rd_WallsVisiblyBulgingNoted="",rd_OpeningsOutofSquare="",
			rd_DamagedFinishesNoted="",rd_SeperationCrackNoted="",rd_ExteriorOpeningCracksNoted="",rd_ObservationSettlementNoted="",
			chk_Value105="",chk_Value104="",chk_Value103="",chk_Value102="",chk_Value101="",
			chk_Value11="",chk_Value121="",chk_Value122="",chk_Value123="",chk_Value124="",chk_Value125="",
			chk_Value135="",chk_Value134="",chk_Value133="",chk_Value132="",chk_Value131="";
	CommonFunctions cf;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
	    	if (extras != null) {
	    		cf.selectedhomeid = strhomeid= extras.getString("homeid");
	    		cf.onlinspectionid = extras.getString("InspectionType");
	    	    cf.onlstatus = extras.getString("status");
	     	}
	        setContentView(R.layout.observation2);
	        cf.Create_Table(10);
	        cf.getDeviceDimensions();
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,cf));
			LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.observationsubmenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 42, 1,cf));
			
			/** DECLARATION AND CLCIK EVENT OF OBSERVATION8,9,10,11,12,13 ROW **/
			 cf.tbl_row_obs8 = (TableRow)findViewById(R.id.firstobsrow);
			 cf.tbl_row_obs8.setOnClickListener(new Obs_clicker());
			 
			 cf.tbl_row_obs9 = (TableRow)findViewById(R.id.secondobsrow);
			 cf.tbl_row_obs9.setOnClickListener(new Obs_clicker());
			 
			 cf.tbl_row_obs10 = (TableRow)findViewById(R.id.thirdobsrow);
			 cf.tbl_row_obs10.setOnClickListener(new Obs_clicker());
			 
			 cf.tbl_row_obs11= (TableRow)findViewById(R.id.fourthobsrow);
			 cf.tbl_row_obs11.setOnClickListener(new Obs_clicker());
			 
			 cf.tbl_row_obs12 = (TableRow)findViewById(R.id.fifthobsrow);
			 cf.tbl_row_obs12.setOnClickListener(new Obs_clicker());
			
			 cf.tbl_row_obs13 = (TableRow)findViewById(R.id.sixthobsrow);
			 cf.tbl_row_obs13.setOnClickListener(new Obs_clicker());
			 
			/** DECLARATION OF OBSERVATION8,9,10,11,12,13 TABLE LAYOUTS**/
			cf.tbl_layout_obs8 = (TableLayout)findViewById(R.id.obs8_table);
			cf.tbl_layout_obs9 = (TableLayout)findViewById(R.id.obs9_table);
			cf.tbl_layout_obs10 = (TableLayout)findViewById(R.id.obs10_table);
			cf.tbl_layout_obs11 = (TableLayout)findViewById(R.id.obs11_table);
			cf.tbl_layout_obs12 = (TableLayout)findViewById(R.id.obs12_table);
			cf.tbl_layout_obs13 = (TableLayout)findViewById(R.id.obs13_table);
			
			/** DECLARATION OF YES LAYOUT FOR OBSERVATION8,9,10,11,12,13 **/
			cf.show_layt_obs9_opt1 = (RelativeLayout)findViewById(R.id.show_obs9_opt1_y);
			cf.show_layt_obs10_opt1 = (RelativeLayout)findViewById(R.id.show_obs10_opt1_y);
			cf.show_layt_obs11_opt1 = (RelativeLayout)findViewById(R.id.show_obs11_opt1_y);
			cf.show_layt_obs12_opt1 = (RelativeLayout)findViewById(R.id.show_obs12_opt1_y);
			cf.show_layt_obs13_opt1 = (RelativeLayout)findViewById(R.id.show_obs13_opt1_y);
			
			 cf.obs8_tbl_row_txt = (TextView)findViewById(R.id.txtobservation8);
			 cf.obs9_tbl_row_txt = (TextView)findViewById(R.id.txtobservation9);
			 cf.obs10_tbl_row_txt = (TextView)findViewById(R.id.txtobservation10);
			 cf.obs11_tbl_row_txt = (TextView)findViewById(R.id.txtobservation11);
			 cf.obs12_tbl_row_txt = (TextView)findViewById(R.id.txtobservation12);
			 cf.obs13_tbl_row_txt = (TextView)findViewById(R.id.txtobservation13);
			 
				
			/** DECLARATION AND CLICK EENT OF OBSERVATION8,9,10,11,12,13 - OPTION1 - RADIOBUTTON YES **/
		    cf.rd_obs8_opt1_yes = (RadioButton) findViewById(R.id.obs8_opt1_rd_y);
			cf.rd_obs8_opt1_yes.setOnClickListener(new Obs_clicker());
			cf.rd_obs9_opt1_yes = (RadioButton) findViewById(R.id.obs9_opt1_rd_y);
			cf.rd_obs9_opt1_yes.setOnClickListener(new Obs_clicker());
			cf.rd_obs10_opt1_yes = (RadioButton) findViewById(R.id.obs10_opt1_rd_y);
			cf.rd_obs10_opt1_yes.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIONA - RADIOBUTTON YES  **/
			cf.rd_obs10_opta_yes = (RadioButton) findViewById(R.id.obs10_opta_rd_yes);
			cf.rd_obs10_opta_yes.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIONB - RADIOBUTTON YES  **/
			cf.rd_obs10_optb_yes = (RadioButton) findViewById(R.id.obs10_optb_rd_yes);
			cf.rd_obs10_optb_yes.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIONC - RADIOBUTTON YES  **/
			cf.rd_obs10_optc_yes = (RadioButton) findViewById(R.id.obs10_optc_rd_yes);
			cf.rd_obs10_optc_yes.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIOND - RADIOBUTTON YES  **/
			cf.rd_obs10_optd_yes = (RadioButton) findViewById(R.id.obs10_optd_rd_yes);
			cf.rd_obs10_optd_yes.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIONE - RADIOBUTTON YES  **/
			cf.rd_obs10_opte_yes = (RadioButton) findViewById(R.id.obs10_opte_rd_yes);
			cf.rd_obs10_opte_yes.setOnClickListener(new Obs_clicker());
			
			
			cf.rd_obs11_opt1_yes = (RadioButton) findViewById(R.id.obs11_opt1_rd_y);
			cf.rd_obs11_opt1_yes.setOnClickListener(new Obs_clicker());
			cf.rd_obs12_opt1_yes = (RadioButton) findViewById(R.id.obs12_opt1_rd_y);
			cf.rd_obs12_opt1_yes.setOnClickListener(new Obs_clicker());
			cf.rd_obs13_opt1_yes = (RadioButton) findViewById(R.id.obs13_opt1_rd_y);
			cf.rd_obs13_opt1_yes.setOnClickListener(new Obs_clicker());
			
			/** DECLARATION AND CLICK EENT OF OBSERVATION8,9,10,11,12,13 - OPTION1 - RADIOBUTTON NO **/
			cf.rd_obs8_opt1_no = (RadioButton)findViewById(R.id.obs8_opt1_rd_n);
			cf.rd_obs8_opt1_no.setOnClickListener(new Obs_clicker());
			cf.rd_obs9_opt1_no = (RadioButton)findViewById(R.id.obs9_opt1_rd_n);
			cf.rd_obs9_opt1_no.setOnClickListener(new Obs_clicker());
			cf.rd_obs10_opt1_no = (RadioButton)findViewById(R.id.obs10_opt1_rd_n);
			cf.rd_obs10_opt1_no.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIONA - RADIOBUTTON NO  **/
			cf.rd_obs10_opta_no = (RadioButton) findViewById(R.id.obs10_opta_rd_no);
			cf.rd_obs10_opta_no.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIONB - RADIOBUTTON NO  **/
			cf.rd_obs10_optb_no = (RadioButton) findViewById(R.id.obs10_optb_rd_no);
			cf.rd_obs10_optb_no.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIONC - RADIOBUTTON NO  **/
			cf.rd_obs10_optc_no = (RadioButton) findViewById(R.id.obs10_optc_rd_no);
			cf.rd_obs10_optc_no.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIOND - RADIOBUTTON NO  **/
			cf.rd_obs10_optd_no = (RadioButton) findViewById(R.id.obs10_optd_rd_no);
			cf.rd_obs10_optd_no.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIONE - RADIOBUTTON NO  **/
			cf.rd_obs10_opte_no = (RadioButton) findViewById(R.id.obs10_opte_rd_no);
			cf.rd_obs10_opte_no.setOnClickListener(new Obs_clicker());
			
			cf.rd_obs11_opt1_no = (RadioButton)findViewById(R.id.obs11_opt1_rd_n);
			cf.rd_obs11_opt1_no.setOnClickListener(new Obs_clicker());
			cf.rd_obs12_opt1_no = (RadioButton)findViewById(R.id.obs12_opt1_rd_n);
			cf.rd_obs12_opt1_no.setOnClickListener(new Obs_clicker());
			cf.rd_obs13_opt1_no = (RadioButton)findViewById(R.id.obs13_opt1_rd_n);
			cf.rd_obs13_opt1_no.setOnClickListener(new Obs_clicker());
			
			/** DECLARATION AND CLICK EENT OF OBSERVATION8,9,10,11,12,13 - OPTION1 - RADIOBUTTON NOT DETERMINED **/
			cf.rd_obs8_opt1_nd = (RadioButton)findViewById(R.id.obs8_opt1_rd_nd);
			cf.rd_obs8_opt1_nd.setOnClickListener(new Obs_clicker());
			cf.rd_obs9_opt1_nd = (RadioButton)findViewById(R.id.obs9_opt1_rd_nd);
			cf.rd_obs9_opt1_nd.setOnClickListener(new Obs_clicker());
			cf.rd_obs10_opt1_nd = (RadioButton)findViewById(R.id.obs10_opt1_rd_nd);
			cf.rd_obs10_opt1_nd.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIONA - RADIOBUTTON NOT DETERMINED  **/
			cf.rd_obs10_opta_nd = (RadioButton) findViewById(R.id.obs10_opta_rd_nd);
			cf.rd_obs10_opta_nd.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIONB - RADIOBUTTON NOT DETERMINED  **/
			cf.rd_obs10_optb_nd = (RadioButton) findViewById(R.id.obs10_optb_rd_nd);
			cf.rd_obs10_optb_nd.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIONC - RADIOBUTTON NOT DETERMINED  **/
			cf.rd_obs10_optc_nd = (RadioButton) findViewById(R.id.obs10_optc_rd_nd);
			cf.rd_obs10_optc_nd.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIOND - RADIOBUTTON NOT DETERMINED  **/
			cf.rd_obs10_optd_nd = (RadioButton) findViewById(R.id.obs10_optd_rd_nd);
			cf.rd_obs10_optd_nd.setOnClickListener(new Obs_clicker());
			
			/**DECLARATION AND CLICK EVENT OF OBSERVATION10 - OPTIONE - RADIOBUTTON NOT DETERMINED  **/
			cf.rd_obs10_opte_nd = (RadioButton) findViewById(R.id.obs10_opte_rd_nd);			
			cf.rd_obs10_opte_nd.setOnClickListener(new Obs_clicker());
			
			cf.rd_obs11_opt1_nd = (RadioButton)findViewById(R.id.obs11_opt1_rd_nd);
			cf.rd_obs11_opt1_nd.setOnClickListener(new Obs_clicker());
			cf.rd_obs12_opt1_nd = (RadioButton)findViewById(R.id.obs12_opt1_rd_nd);
			cf.rd_obs12_opt1_nd.setOnClickListener(new Obs_clicker());
			cf.rd_obs13_opt1_nd = (RadioButton)findViewById(R.id.obs13_opt1_rd_nd);
			cf.rd_obs13_opt1_nd.setOnClickListener(new Obs_clicker());
			
			 /** DECLARATION FOR COMMENTS OBSERVATION8,9,10,11,12,13 **/
	        cf.etcomments_obs8_opt1=(EditText)findViewById(R.id.obs8_opt1_etcomments);
	        cf.etcomments_obs9_opt1=(EditText)findViewById(R.id.obs9_opt1_etcomments);
	        cf.etcomments_obs10_opt1=(EditText)findViewById(R.id.obs10_opt1_etcomments);
	        
	        /** DECLARATION FOR COMMENTS OBSERVATION10- OPTIONA,B,C,D,E **/
	        cf.etcomments_obs10_opta=(EditText)findViewById(R.id.obs10_opta_etcomments);
	        cf.etcomments_obs10_optb=(EditText)findViewById(R.id.obs10_optb_etcomments);
	        cf.etcomments_obs10_optc=(EditText)findViewById(R.id.obs10_optc_etcomments);
	        cf.etcomments_obs10_optd=(EditText)findViewById(R.id.obs10_optd_etcomments);
	        cf.etcomments_obs10_opte=(EditText)findViewById(R.id.obs10_opte_etcomments);
	        
	        cf.etcomments_obs11_opt1=(EditText)findViewById(R.id.obs11_opt1_etcomments);
	        cf.etcomments_obs12_opt1=(EditText)findViewById(R.id.obs12_opt1_etcomments);
	        cf.etcomments_obs13_opt1=(EditText)findViewById(R.id.obs13_opt1_etcomments);
	        
	        /** DECLARATION AND CLICK EVENT FOR OBSERVATION11 - OPTION1 - CHECKBOXES **/
	        cf.cb_obs11[0]=(CheckBox) findViewById(R.id.obs11_opt1_newaddi);
	        cf.cb_obs11[1]=(CheckBox) findViewById(R.id.obs11_opt1_differ);
	        cf.cb_obs11[2]=(CheckBox) findViewById(R.id.obs11_opt1_other);
	        cf.cb_obs11[2].setOnClickListener(new Obs_clicker());
	        cf.etother_obs11_opt1=(EditText)findViewById(R.id.obs11_opt1_etother);
	        
	        /** DECLARATION AND CLICK EVENT FOR OBSERVATION12 - CHECKBOXES **/
	        cf.cb_obs121[0]=(CheckBox) findViewById(R.id.obs12_vert);
	        cf.cb_obs121[1]=(CheckBox) findViewById(R.id.obs12_dia);
	        cf.cb_obs121[2]=(CheckBox) findViewById(R.id.obs12_ran);
	        cf.cb_obs121[3]=(CheckBox) findViewById(R.id.obs12_hor);
	        cf.cb_obs121[4]=(CheckBox) findViewById(R.id.obs12_typeofcrack_other);
	        cf.cb_obs121[4].setOnClickListener(new Obs_clicker());
	        cf.etother_obs12_typeofcrack=(EditText)findViewById(R.id.obs12_typeofcrack_etother);
	        
	        cf.cb_obs122[0]=(CheckBox) findViewById(R.id.obs12_front);
	        cf.cb_obs122[1]=(CheckBox) findViewById(R.id.obs12_rear);
	        cf.cb_obs122[2]=(CheckBox) findViewById(R.id.obs12_right);
	        cf.cb_obs122[3]=(CheckBox) findViewById(R.id.obs12_left);
	        cf.cb_obs122[4]=(CheckBox) findViewById(R.id.obs12_location_other);
	        cf.cb_obs122[4].setOnClickListener(new Obs_clicker());
	        cf.etother_obs12_location=(EditText)findViewById(R.id.obs12_location_etother);
	        
	        cf.cb_obs123[0]=(CheckBox) findViewById(R.id.obs12_16th);
	        cf.cb_obs123[1]=(CheckBox) findViewById(R.id.obs12_8th);
	        cf.cb_obs123[2]=(CheckBox) findViewById(R.id.obs12_4th);
	        cf.cb_obs123[3]=(CheckBox) findViewById(R.id.obs12_2th);
	        cf.cb_obs123[4]=(CheckBox) findViewById(R.id.obs12_approx_other);
	        cf.cb_obs123[4].setOnClickListener(new Obs_clicker());
	        cf.etother_obs12_approx=(EditText)findViewById(R.id.obs12_approx_etother);
	        
	        cf.etother_obs12_etlen=(EditText)findViewById(R.id.obs12_etlen);
	        
	        cf.cb_obs124[0]=(CheckBox) findViewById(R.id.obs12_clean);
	        cf.cb_obs124[1]=(CheckBox) findViewById(R.id.obs12_colob);
	        cf.cb_obs124[2]=(CheckBox) findViewById(R.id.obs12_old);
	        cf.cb_obs124[3]=(CheckBox) findViewById(R.id.obs12_ovr);
	        cf.cb_obs124[4]=(CheckBox) findViewById(R.id.obs12_caur);
	        cf.cb_obs124[5]=(CheckBox) findViewById(R.id.obs12_repair);
	        
	        cf.cb_obs125[0]=(CheckBox) findViewById(R.id.obs12_initial);
	        cf.cb_obs125[1]=(CheckBox) findViewById(R.id.obs12_shrink);
	        cf.cb_obs125[2]=(CheckBox) findViewById(R.id.obs12_sub);
	        cf.cb_obs125[3]=(CheckBox) findViewById(R.id.obs12_drain);
	        cf.cb_obs125[4]=(CheckBox) findViewById(R.id.obs12_nd);
	        cf.cb_obs125[5]=(CheckBox) findViewById(R.id.obs12_sinkhole);
	        cf.cb_obs125[6]=(CheckBox) findViewById(R.id.obs12_cause_other);
	        cf.cb_obs125[6].setOnClickListener(new Obs_clicker());
	        cf.etother_obs12_cause=(EditText)findViewById(R.id.obs12_cause_etother);
	        
	        /** DECLARATION AND CLICK EVENT FOR OBSERVATION13 - CHECKBOXES **/
	        cf.cb_obs131[0]=(CheckBox) findViewById(R.id.obs13_vert);
	        cf.cb_obs131[1]=(CheckBox) findViewById(R.id.obs13_dia);
	        cf.cb_obs131[2]=(CheckBox) findViewById(R.id.obs13_ran);
	        cf.cb_obs131[3]=(CheckBox) findViewById(R.id.obs13_hor);
	        cf.cb_obs131[4]=(CheckBox) findViewById(R.id.obs13_typeofcrack_other);
	        cf.cb_obs131[4].setOnClickListener(new Obs_clicker());
	        cf.etother_obs13_typeofcrack=(EditText)findViewById(R.id.obs13_typeofcrack_etother);
	        
	        cf.cb_obs132[0]=(CheckBox) findViewById(R.id.obs13_front);
	        cf.cb_obs132[1]=(CheckBox) findViewById(R.id.obs13_rear);
	        cf.cb_obs132[2]=(CheckBox) findViewById(R.id.obs13_right);
	        cf.cb_obs132[3]=(CheckBox) findViewById(R.id.obs13_left);
	        cf.cb_obs132[4]=(CheckBox) findViewById(R.id.obs13_location_other);
	        cf.cb_obs132[4].setOnClickListener(new Obs_clicker());
	        cf.etother_obs13_location=(EditText)findViewById(R.id.obs13_location_etother);
	        
	        cf.cb_obs133[0]=(CheckBox) findViewById(R.id.obs13_16th);
	        cf.cb_obs133[1]=(CheckBox) findViewById(R.id.obs13_8th);
	        cf.cb_obs133[2]=(CheckBox) findViewById(R.id.obs13_4th);
	        cf.cb_obs133[3]=(CheckBox) findViewById(R.id.obs13_2th);
	        cf.cb_obs133[4]=(CheckBox) findViewById(R.id.obs13_approx_other);
	        cf.cb_obs133[4].setOnClickListener(new Obs_clicker());
	        cf.etother_obs13_approx=(EditText)findViewById(R.id.obs13_approx_etother);
	        
	        cf.etother_obs13_etlen=(EditText)findViewById(R.id.obs13_etlen);
	        
	        cf.cb_obs134[0]=(CheckBox) findViewById(R.id.obs13_clean);
	        cf.cb_obs134[1]=(CheckBox) findViewById(R.id.obs13_colob);
	        cf.cb_obs134[2]=(CheckBox) findViewById(R.id.obs13_old);
	        cf.cb_obs134[3]=(CheckBox) findViewById(R.id.obs13_ovr);
	        cf.cb_obs134[4]=(CheckBox) findViewById(R.id.obs13_caur);
	        cf.cb_obs134[5]=(CheckBox) findViewById(R.id.obs13_repair);
	        
	        cf.cb_obs135[0]=(CheckBox) findViewById(R.id.obs13_initial);
	        cf.cb_obs135[1]=(CheckBox) findViewById(R.id.obs13_shrink);
	        cf.cb_obs135[2]=(CheckBox) findViewById(R.id.obs13_sub);
	        cf.cb_obs135[3]=(CheckBox) findViewById(R.id.obs13_drain);
	        cf.cb_obs135[4]=(CheckBox) findViewById(R.id.obs13_nd);
	        cf.cb_obs135[5]=(CheckBox) findViewById(R.id.obs13_sinkhole);
	        cf.cb_obs135[6]=(CheckBox) findViewById(R.id.obs13_cause_other);
	        cf.cb_obs135[6].setOnClickListener(new Obs_clicker());
	        
	        cf.etother_obs13_cause=(EditText)findViewById(R.id.obs13_cause_etother);
	        
	        /** DECLARATION AND CLICK EVENT FOR OBSERVATION10 - CHECKBOXES **/
	        cf.cb_obs101[0]=(CheckBox) findViewById(R.id.obs10_vert);
	        cf.cb_obs101[1]=(CheckBox) findViewById(R.id.obs10_dia);
	        cf.cb_obs101[2]=(CheckBox) findViewById(R.id.obs10_ran);
	        cf.cb_obs101[3]=(CheckBox) findViewById(R.id.obs10_hor);
	        cf.cb_obs101[4]=(CheckBox) findViewById(R.id.obs10_typeofcrack_other);
	        cf.cb_obs101[4].setOnClickListener(new Obs_clicker());
	        cf.etother_obs10_typeofcrack=(EditText)findViewById(R.id.obs10_typeofcrack_etother);
	        
	        cf.cb_obs102[0]=(CheckBox) findViewById(R.id.obs10_front);
	        cf.cb_obs102[1]=(CheckBox) findViewById(R.id.obs10_rear);
	        cf.cb_obs102[2]=(CheckBox) findViewById(R.id.obs10_right);
	        cf.cb_obs102[3]=(CheckBox) findViewById(R.id.obs10_left);
	        cf.cb_obs102[4]=(CheckBox) findViewById(R.id.obs10_location_other);
	        cf.cb_obs102[4].setOnClickListener(new Obs_clicker());
	        cf.etother_obs10_location=(EditText)findViewById(R.id.obs10_location_etother);
	        
	        cf.cb_obs103[0]=(CheckBox) findViewById(R.id.obs10_16th);
	        cf.cb_obs103[1]=(CheckBox) findViewById(R.id.obs10_8th);
	        cf.cb_obs103[2]=(CheckBox) findViewById(R.id.obs10_4th);
	        cf.cb_obs103[3]=(CheckBox) findViewById(R.id.obs10_2th);
	        cf.cb_obs103[4]=(CheckBox) findViewById(R.id.obs10_approx_other);
	        cf.cb_obs103[4].setOnClickListener(new Obs_clicker());
	        cf.etother_obs10_approx=(EditText)findViewById(R.id.obs10_approx_etother);
	        
	        cf.etother_obs10_etlen=(EditText)findViewById(R.id.obs10_etlen);
	        
	        cf.cb_obs104[0]=(CheckBox) findViewById(R.id.obs10_clean);
	        cf.cb_obs104[1]=(CheckBox) findViewById(R.id.obs10_colob);
	        cf.cb_obs104[2]=(CheckBox) findViewById(R.id.obs10_old);
	        cf.cb_obs104[3]=(CheckBox) findViewById(R.id.obs10_ovr);
	        cf.cb_obs104[4]=(CheckBox) findViewById(R.id.obs10_caur);
	        cf.cb_obs104[5]=(CheckBox) findViewById(R.id.obs10_repair);
	       
	        cf.cb_obs105[0]=(CheckBox) findViewById(R.id.obs10_initial);
	        cf.cb_obs105[1]=(CheckBox) findViewById(R.id.obs10_shrink);
	        cf.cb_obs105[2]=(CheckBox) findViewById(R.id.obs10_sub);
	        cf.cb_obs105[3]=(CheckBox) findViewById(R.id.obs10_drain);
	        cf.cb_obs105[4]=(CheckBox) findViewById(R.id.obs10_nd);
	        cf.cb_obs105[5]=(CheckBox) findViewById(R.id.obs10_sinkhole);
	        cf.cb_obs105[6]=(CheckBox) findViewById(R.id.obs10_cause_other);
	        cf.cb_obs105[6].setOnClickListener(new Obs_clicker());
	        cf.etother_obs10_cause=(EditText)findViewById(R.id.obs10_cause_etother);
	        
	        /*DECLARATION OF EDITTEXT LIMIT EXCEEDS */
			  SH_OBS8P = (LinearLayout) findViewById(R.id.SH_OBS8_ED_parrent);
			  SH_OBS8 = (LinearLayout) findViewById(R.id.SH_OB8);
			  SH_OBS8T = (TextView) findViewById(R.id.SH_OB8_TV);
			  
			  SH_OBS9P = (LinearLayout) findViewById(R.id.SH_OBS9_ED_parrent);
			  SH_OBS9 = (LinearLayout) findViewById(R.id.SH_OB9);
			  SH_OBS9T = (TextView) findViewById(R.id.SH_OB9_TV);
			  
			  SH_OBS10P = (LinearLayout) findViewById(R.id.SH_OBS10_ED_parrent);
			  SH_OBS10 = (LinearLayout) findViewById(R.id.SH_OB10);
			  SH_OBS10T = (TextView) findViewById(R.id.SH_OB10_TV);
			  
			  SH_OBS10AP = (LinearLayout) findViewById(R.id.SH_OBS10A_ED_parrent);
			  SH_OBS10A = (LinearLayout) findViewById(R.id.SH_OB10A);
			  SH_OBS10AT = (TextView) findViewById(R.id.SH_OB10A_TV);
			  
			  SH_OBS10BP = (LinearLayout) findViewById(R.id.SH_OBS10B_ED_parrent);
			  SH_OBS10B = (LinearLayout) findViewById(R.id.SH_OB10B);
			  SH_OBS10BT = (TextView) findViewById(R.id.SH_OB10B_TV);
			  
			  SH_OBS10CP = (LinearLayout) findViewById(R.id.SH_OBS10C_ED_parrent);
			  SH_OBS10C = (LinearLayout) findViewById(R.id.SH_OB10C);
			  SH_OBS10CT = (TextView) findViewById(R.id.SH_OB10C_TV);
			  
			  SH_OBS10DP = (LinearLayout) findViewById(R.id.SH_OBS10D_ED_parrent);
			  SH_OBS10D = (LinearLayout) findViewById(R.id.SH_OB10D);
			  SH_OBS10DT = (TextView) findViewById(R.id.SH_OB10D_TV);
			  
			  SH_OBS10EP = (LinearLayout) findViewById(R.id.SH_OBS10E_ED_parrent);
			  SH_OBS10E = (LinearLayout) findViewById(R.id.SH_OB10E);
			  SH_OBS10ET = (TextView) findViewById(R.id.SH_OB10E_TV);
			  
			  SH_OBS11P = (LinearLayout) findViewById(R.id.SH_OBS11_ED_parrent);
			  SH_OBS11 = (LinearLayout) findViewById(R.id.SH_OB11);
			  SH_OBS11T = (TextView) findViewById(R.id.SH_OB11_TV);
			  
			  SH_OBS12P = (LinearLayout) findViewById(R.id.SH_OBS12_ED_parrent);
			  SH_OBS12 = (LinearLayout) findViewById(R.id.SH_OB12);
			  SH_OBS12T = (TextView) findViewById(R.id.SH_OB12_TV);
			  
			  SH_OBS13P = (LinearLayout) findViewById(R.id.SH_OBS13_ED_parrent);
			  SH_OBS13 = (LinearLayout) findViewById(R.id.SH_OB13);
			  SH_OBS13T = (TextView) findViewById(R.id.SH_OB13_TV);
			  
			  cf.etcomments_obs8_opt1.setOnTouchListener(new Touch_Listener(1));
			  cf.etcomments_obs9_opt1.setOnTouchListener(new Touch_Listener(2));
			  cf.etcomments_obs10_opt1.setOnTouchListener(new Touch_Listener(3));
			  cf.etcomments_obs10_opta.setOnTouchListener(new Touch_Listener(4));
			  cf.etcomments_obs10_optb.setOnTouchListener(new Touch_Listener(5));
			  cf.etcomments_obs10_optc.setOnTouchListener(new Touch_Listener(6));
			  cf.etcomments_obs10_optd.setOnTouchListener(new Touch_Listener(7));
			  cf.etcomments_obs10_opte.setOnTouchListener(new Touch_Listener(8));
			  cf.etcomments_obs11_opt1.setOnTouchListener(new Touch_Listener(9));
			  cf.etcomments_obs12_opt1.setOnTouchListener(new Touch_Listener(10));
			  cf.etcomments_obs13_opt1.setOnTouchListener(new Touch_Listener(11));
			  
			  cf.etcomments_obs8_opt1.addTextChangedListener(new SH_textwatcher(1));
			  cf.etcomments_obs9_opt1.addTextChangedListener(new SH_textwatcher(2));
			  cf.etcomments_obs10_opt1.addTextChangedListener(new SH_textwatcher(3));
			  cf.etcomments_obs10_opta.addTextChangedListener(new SH_textwatcher(4));
			  cf.etcomments_obs10_optb.addTextChangedListener(new SH_textwatcher(5));
			  cf.etcomments_obs10_optc.addTextChangedListener(new SH_textwatcher(6));
			  cf.etcomments_obs10_optd.addTextChangedListener(new SH_textwatcher(7));
			  cf.etcomments_obs10_opte.addTextChangedListener(new SH_textwatcher(8));
			  cf.etcomments_obs11_opt1.addTextChangedListener(new SH_textwatcher(9));
			  cf.etcomments_obs12_opt1.addTextChangedListener(new SH_textwatcher(10));
			  cf.etcomments_obs13_opt1.addTextChangedListener(new SH_textwatcher(11));
			  
			  /* DISPLAY THE VALUES FROM DATABASE*/
			  OBS2_SetValues();
	 }
	 class Touch_Listener implements OnTouchListener
		{
			   public int type;
			   Touch_Listener(int type)
				{
					this.type=type;
					
				}
			    @Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
			    	if(this.type==1)
					{
			    		cf.setFocus(cf.etcomments_obs8_opt1);
					}
			    	else if(this.type==2)
					{
				    		cf.setFocus(cf.etcomments_obs9_opt1);
					}
			    	else if(this.type==3)
					{
				    		cf.setFocus(cf.etcomments_obs10_opt1);
					}
			    	else if(this.type==4)
					{
				    		cf.setFocus(cf.etcomments_obs10_opta);
					}
			    	else if(this.type==5)
					{
				    		cf.setFocus(cf.etcomments_obs10_optb);
					}
			    	else if(this.type==6)
					{
				    		cf.setFocus(cf.etcomments_obs10_optc);
					}
			    	else if(this.type==7)
					{
				    		cf.setFocus(cf.etcomments_obs10_optd);
					}
			    	else if(this.type==8)
					{
				    		cf.setFocus(cf.etcomments_obs10_opte);
					}
			    	else if(this.type==9)
					{
				    		cf.setFocus(cf.etcomments_obs11_opt1);
					}
			    	else if(this.type==10)
					{
				    		cf.setFocus(cf.etcomments_obs12_opt1);
					}
			    	else if(this.type==11)
					{
				    		cf.setFocus(cf.etcomments_obs13_opt1);
					}
			    			    	
					return false;
				}
			 
		}
	 private void OBS2_SetValues() {
		// TODO Auto-generated method stub
		try
		{
			Cursor OBS2_Retrive=cf.SelectTablefunction(cf.Observation2, " where SH_OBS2_SRID ='"+cf.encode(cf.selectedhomeid)+"'");
			if(OBS2_Retrive.getCount()>0)
			{
				OBS2_Retrive.moveToFirst();
				/* SETTING QUESTION NO.8 */
				rd_PoolDeckSlabNoted=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("PoolDeckSlabNoted")));
				setvalueToRadio(cf.rd_obs8_opt1_yes,cf.rd_obs8_opt1_no,cf.rd_obs8_opt1_nd,rd_PoolDeckSlabNoted); 
				cf.etcomments_obs8_opt1.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("PoolDeckSlabComments"))));
			   
				/* SETTING QUESTION NO.9 */
				rd_PoolShellPlumbLevel=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("PoolShellPlumbLevel")));
				setvalueToRadio(cf.rd_obs9_opt1_yes,cf.rd_obs9_opt1_no,cf.rd_obs9_opt1_nd,rd_PoolShellPlumbLevel); 
				cf.etcomments_obs9_opt1.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("PoolShellPlumbLevelComments"))));
			   
				/* SETTING QUESTION NO.10 */
				rd_ExcessiveSettlement=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExcessiveSettlement")));
				setValueToRadio(cf.rd_obs10_opt1_yes,cf.rd_obs10_opt1_no,cf.rd_obs10_opt1_nd,rd_ExcessiveSettlement,cf.show_layt_obs10_opt1); 
				cf.etcomments_obs10_opt1.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExcessiveSettlementComments"))));
			    chk_Value101=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExcessiveSettlementTypeofCrack")));
			    cf.setvaluechk(chk_Value101,cf.cb_obs101,cf.etother_obs10_typeofcrack);
			    chk_Value102=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExcessiveSettlementLocation")));
			    cf.setvaluechk(chk_Value102,cf.cb_obs102,cf.etother_obs10_location);
			    chk_Value103=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExcessiveSettlementWidthofCrack")));
			    cf.setvaluechk(chk_Value103,cf.cb_obs103,cf.etother_obs10_approx);
			    cf.etother_obs10_etlen.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExcessiveSettlementLengthofCrack"))));
			    chk_Value104=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExcessiveSettlementCleanliness")));
			    cf.setvaluechk(chk_Value104,cf.cb_obs104,cf.etother_obs10_approx);
			    chk_Value105=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExcessiveSettlementProbableCause")));
			    cf.setvaluechk(chk_Value105,cf.cb_obs105,cf.etother_obs10_cause);
			    
			    /* SETTING QUESTION NO.10(A) */
			    rd_WallLeaningNoted=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("WallLeaningNoted")));
				setvalueToRadio(cf.rd_obs10_opta_yes,cf.rd_obs10_opta_no,cf.rd_obs10_opta_nd,rd_WallLeaningNoted); 
				cf.etcomments_obs10_opta.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("WallLeaningComments"))));
				
				 /* SETTING QUESTION NO.10(B) */
			    rd_WallsVisiblyNotLevel=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("WallsVisiblyNotLevel")));
				setvalueToRadio(cf.rd_obs10_optb_yes,cf.rd_obs10_optb_no,cf.rd_obs10_optb_nd,rd_WallsVisiblyNotLevel); 
				cf.etcomments_obs10_optb.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("WallsVisiblyNotLevelComments"))));
			   
				 /* SETTING QUESTION NO.10(C) */
			    rd_WallsVisiblyBulgingNoted=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("WallsVisiblyBulgingNoted")));
				setvalueToRadio(cf.rd_obs10_optc_yes,cf.rd_obs10_optc_no,cf.rd_obs10_optc_nd,rd_WallsVisiblyBulgingNoted); 
				cf.etcomments_obs10_optc.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("WallsVisiblyBulgingComments"))));
			   
				 /* SETTING QUESTION NO.10(D) */
			    rd_OpeningsOutofSquare=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("OpeningsOutofSquare")));
				setvalueToRadio(cf.rd_obs10_optd_yes,cf.rd_obs10_optd_no,cf.rd_obs10_optd_nd,rd_OpeningsOutofSquare); 
				cf.etcomments_obs10_optd.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("OpeningsOutofSquareComments"))));
			   
				 /* SETTING QUESTION NO.10(E) */
			    rd_DamagedFinishesNoted=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("DamagedFinishesNoted")));
				setvalueToRadio(cf.rd_obs10_opte_yes,cf.rd_obs10_opte_no,cf.rd_obs10_opte_nd,rd_DamagedFinishesNoted); 
				cf.etcomments_obs10_opte.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("DamagedFinishesComments"))));
			   
				/* SETTING QUESTION NO.11 */
				rd_SeperationCrackNoted=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("SeperationCrackNoted")));
				setValueToRadio(cf.rd_obs11_opt1_yes,cf.rd_obs11_opt1_no,cf.rd_obs11_opt1_nd,rd_SeperationCrackNoted,cf.show_layt_obs11_opt1); 
				cf.etcomments_obs11_opt1.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("SeperationCrackComments"))));
			    chk_Value11=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("SeperationCrackTypeofCrack")));
			    cf.setvaluechk(chk_Value11,cf.cb_obs11,cf.etother_obs11_opt1);
			   
			    /* SETTING QUESTION NO.12 */
				rd_ExteriorOpeningCracksNoted=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExteriorOpeningCracksNoted")));
				setValueToRadio(cf.rd_obs12_opt1_yes,cf.rd_obs12_opt1_no,cf.rd_obs12_opt1_nd,rd_ExteriorOpeningCracksNoted,cf.show_layt_obs12_opt1); 
				cf.etcomments_obs12_opt1.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExteriorOpeningCracksComments"))));
			    chk_Value121=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExteriorOpeningCracksTypeofCrack")));
			    cf.setvaluechk(chk_Value121,cf.cb_obs121,cf.etother_obs12_typeofcrack);
			    chk_Value122=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExteriorOpeningCracksLocation")));
			    cf.setvaluechk(chk_Value122,cf.cb_obs122,cf.etother_obs12_location);
			    chk_Value123=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExteriorOpeningCracksWidthofCrack")));
			    cf.setvaluechk(chk_Value123,cf.cb_obs123,cf.etother_obs12_approx);
			    cf.etother_obs12_etlen.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExteriorOpeningCracksLengthofCrack"))));
			    chk_Value124=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExteriorOpeningCracksCleanliness")));
			    cf.setvaluechk(chk_Value124,cf.cb_obs124,cf.etother_obs12_approx);
			    chk_Value125=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ExteriorOpeningCracksProbableCause")));
			    cf.setvaluechk(chk_Value125,cf.cb_obs125,cf.etother_obs12_cause);
			    
			    /* SETTING QUESTION NO.13 */
			    rd_ObservationSettlementNoted=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ObservationSettlementNoted")));
				setValueToRadio(cf.rd_obs13_opt1_yes,cf.rd_obs13_opt1_no,cf.rd_obs13_opt1_nd,rd_ObservationSettlementNoted,cf.show_layt_obs13_opt1); 
				cf.etcomments_obs13_opt1.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ObservationSettlementComments"))));
			    chk_Value131=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ObservationSettlementTypeofCrack")));
			    cf.setvaluechk(chk_Value131,cf.cb_obs131,cf.etother_obs13_typeofcrack);
			    chk_Value132=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ObservationSettlementLocation")));
			    cf.setvaluechk(chk_Value132,cf.cb_obs132,cf.etother_obs13_location);
			    chk_Value133=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ObservationSettlementWidthofCrack")));
			    cf.setvaluechk(chk_Value133,cf.cb_obs133,cf.etother_obs13_approx);
			    cf.etother_obs13_etlen.setText(cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ObservationSettlementLengthofCrack"))));
			    chk_Value134=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ObservationSettlementCleanliness")));
			    cf.setvaluechk(chk_Value134,cf.cb_obs134,cf.etother_obs13_approx);
			    chk_Value135=cf.decode(OBS2_Retrive.getString(OBS2_Retrive.getColumnIndex("ObservationSettlementProbableCause")));
			    cf.setvaluechk(chk_Value135,cf.cb_obs135,cf.etother_obs13_cause);
			}
			
		}
		catch (Exception E)
		{
			String strerrorlog="Selection of the Observation2 table not working ";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	
	private void setValueToRadio(RadioButton rd_obs10_opt1_yes,
			RadioButton rd_obs10_opt1_no, RadioButton rd_obs10_opt1_nd,
			String rd_ExcessiveSettlement2, RelativeLayout show_layt_obs10_opt1) {
		// TODO Auto-generated method stub
		if(rd_obs10_opt1_yes.getText().toString().equals(rd_ExcessiveSettlement2))
		{
			rd_obs10_opt1_yes.setChecked(true);
			show_layt_obs10_opt1.setVisibility(cf.show.VISIBLE);
		}
		else if(rd_obs10_opt1_no.getText().toString().equals(rd_ExcessiveSettlement2))
		{
			rd_obs10_opt1_no.setChecked(true);
		}
		else if(rd_obs10_opt1_nd.getText().toString().equals(rd_ExcessiveSettlement2))
		{
			rd_obs10_opt1_nd.setChecked(true);
		}
	}
	private void setvalueToRadio(RadioButton rd_obs8_opt1_yes,
			RadioButton rd_obs8_opt1_no, RadioButton rd_obs8_opt1_nd,
			String rd_PoolDeckSlabNoted2) {
		// TODO Auto-generated method stub
		if(rd_obs8_opt1_yes.getText().toString().equals(rd_PoolDeckSlabNoted2))
		{
			rd_obs8_opt1_yes.setChecked(true);
			
		}
		else if(rd_obs8_opt1_no.getText().toString().equals(rd_PoolDeckSlabNoted2))
		{
			rd_obs8_opt1_no.setChecked(true);
		}
		else if(rd_obs8_opt1_nd.getText().toString().equals(rd_PoolDeckSlabNoted2))
		{
			rd_obs8_opt1_nd.setChecked(true);
		}
	}

	class SH_textwatcher implements TextWatcher
		{
	         public int type;
			SH_textwatcher(int type)
			{
				this.type=type;
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(this.type==1)
				{
					cf.showing_limit(s.toString(),SH_OBS8P,SH_OBS8,SH_OBS8T,"499");
				}
				else if(this.type==2)
				{
					cf.showing_limit(s.toString(),SH_OBS9P,SH_OBS9,SH_OBS9T,"499");
				}
				else if(this.type==3)
				{
					cf.showing_limit(s.toString(),SH_OBS10P,SH_OBS10,SH_OBS10T,"499");
				}
				else if(this.type==4)
				{
					cf.showing_limit(s.toString(),SH_OBS10AP,SH_OBS10A,SH_OBS10AT,"499");
				}
				else if(this.type==5)
				{
					cf.showing_limit(s.toString(),SH_OBS10BP,SH_OBS10B,SH_OBS10BT,"499");
				}
				else if(this.type==6)
				{
					cf.showing_limit(s.toString(),SH_OBS10CP,SH_OBS10C,SH_OBS10CT,"499");
				}
				else if(this.type==7)
				{
					cf.showing_limit(s.toString(),SH_OBS10DP,SH_OBS10D,SH_OBS10DT,"499");
				}
				else if(this.type==8)
				{
					cf.showing_limit(s.toString(),SH_OBS10EP,SH_OBS10E,SH_OBS10ET,"499");
				}
				else if(this.type==9)
				{
					cf.showing_limit(s.toString(),SH_OBS11P,SH_OBS11,SH_OBS11T,"499");
				}
				else if(this.type==10)
				{
					cf.showing_limit(s.toString(),SH_OBS12P,SH_OBS12,SH_OBS12T,"499");
				}
				else if(this.type==11)
				{
					cf.showing_limit(s.toString(),SH_OBS13P,SH_OBS13,SH_OBS13T,"499");
				}
			
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}
			
		}		 
	 class Obs_clicker implements OnClickListener	  {

		  private static final int visibility = 0;

		@Override
		  public void onClick(View v) {
		   // TODO Auto-generated method stub
			  switch(v.getId()){
			  case R.id.firstobsrow:/** SELECTING OBSERVATION8 QUESTION ROW **/
				  obs_tablelayout_hide();cf.tbl_layout_obs8.setVisibility(visibility);
				  cf.tbl_row_obs8.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs8_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.secondobsrow:/** SELECTING OBSERVATION9 QUESTION ROW **/
				  obs_tablelayout_hide();cf.tbl_layout_obs9.setVisibility(visibility);
				  cf.tbl_row_obs9.setBackgroundResource(R.drawable.subbackrepeatnor);
				   cf.obs9_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				   if(rd_PoolDeckSlabNoted.equals("Not Applicable"))
				   {
					  cf.rd_obs9_opt1_nd.setChecked(true);
					  cf.etcomments_obs9_opt1.setText("No pool present.");
					  rd_PoolShellPlumbLevel=retrieve_radioname(cf.rd_obs9_opt1_nd);
				   }
				  break;
			  case R.id.thirdobsrow:/** SELECTING OBSERVATION10 QUESTION ROW **/
				  obs_tablelayout_hide();cf.tbl_layout_obs10.setVisibility(visibility);
				  cf.tbl_row_obs10.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs10_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.fourthobsrow:/** SELECTING OBSERVATION11 QUESTION ROW **/
				  obs_tablelayout_hide();cf.tbl_layout_obs11.setVisibility(visibility);
				  cf.tbl_row_obs11.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs11_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.fifthobsrow:/** SELECTING OBSERVATION12 QUESTION ROW **/
				  obs_tablelayout_hide();cf.tbl_layout_obs12.setVisibility(visibility);
				  cf.tbl_row_obs12.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs12_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.sixthobsrow:/** SELECTING OBSERVATION913 QUESTION ROW **/
				  obs_tablelayout_hide();cf.tbl_layout_obs13.setVisibility(visibility);
				  TextView obs13_note = (TextView)findViewById(R.id.obs13_note);
				  obs13_note.setText(Html.fromHtml("<b> Note: </b> Inspectors must outline limitations on adjoining owners inspection / conditions."));
				  cf.tbl_row_obs13.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs13_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.obs8_opt1_rd_y: /** SELECTING OBSERVATION8 RADIOBUTTON YES **/
				  cf.etcomments_obs8_opt1.setText("");
				  rd_PoolDeckSlabNoted=retrieve_radioname(cf.rd_obs8_opt1_yes);
				  break;
			  case R.id.obs9_opt1_rd_y: /** SELECTING OBSERVATION9 RADIOBUTTON YES **/
				  cf.show_layt_obs9_opt1.setVisibility(visibility);
				  cf.etcomments_obs9_opt1.setText("");
				  rd_PoolShellPlumbLevel=retrieve_radioname(cf.rd_obs9_opt1_yes);
				  break;
			  case R.id.obs10_opt1_rd_y: /** SELECTING OBSERVATION10 RADIOBUTTON YES **/
				  cf.show_layt_obs10_opt1.setVisibility(visibility);
				  cf.etcomments_obs10_opt1.setText("");
				  rd_ExcessiveSettlement=retrieve_radioname(cf.rd_obs10_opt1_yes);
				  break;
			  case R.id.obs10_opta_rd_yes: /** SELECTING OBSERVATION10 - OPTIONA RADIOBUTTON YES **/
				  cf.etcomments_obs10_opta.setText("");
				  rd_WallLeaningNoted=retrieve_radioname(cf.rd_obs10_opta_yes);
				  break;
			  case R.id.obs10_optb_rd_yes: /** SELECTING OBSERVATION10 - OPTIONB RADIOBUTTON YES **/
				  cf.etcomments_obs10_optb.setText("");
				  rd_WallsVisiblyNotLevel=retrieve_radioname(cf.rd_obs10_optb_yes);
				  break;
			  case R.id.obs10_optc_rd_yes: /** SELECTING OBSERVATION10 - OPTIONC RADIOBUTTON YES **/
				  cf.etcomments_obs10_optc.setText("");
				  rd_WallsVisiblyBulgingNoted=retrieve_radioname(cf.rd_obs10_optc_yes);
				  break;
			  case R.id.obs10_optd_rd_yes: /** SELECTING OBSERVATION10 - OPTIOND RADIOBUTTON YES **/
				  cf.etcomments_obs10_optd.setText("");
				  rd_OpeningsOutofSquare=retrieve_radioname(cf.rd_obs10_optd_yes);
				  break;
			  case R.id.obs10_opte_rd_yes: /** SELECTING OBSERVATION10 - OPTIONE RADIOBUTTON YES **/
				  cf.etcomments_obs10_opte.setText("");
				  rd_DamagedFinishesNoted=retrieve_radioname(cf.rd_obs10_opte_yes);
				  break;
			  case R.id.obs11_opt1_rd_y: /** SELECTING OBSERVATION11 RADIOBUTTON YES **/
				  cf.show_layt_obs11_opt1.setVisibility(visibility);
				  cf.etcomments_obs11_opt1.setText("");
				  rd_SeperationCrackNoted=retrieve_radioname(cf.rd_obs11_opt1_yes);
				  break;
			  case R.id.obs12_opt1_rd_y: /** SELECTING OBSERVATION12 RADIOBUTTON YES **/
				  cf.show_layt_obs12_opt1.setVisibility(visibility);
				  cf.etcomments_obs12_opt1.setText("");
				  rd_ExteriorOpeningCracksNoted=retrieve_radioname(cf.rd_obs12_opt1_yes);
				  break;
			  case R.id.obs13_opt1_rd_y: /** SELECTING OBSERVATION13 RADIOBUTTON YES **/
				  cf.show_layt_obs13_opt1.setVisibility(visibility);
				  cf.etcomments_obs13_opt1.setText("");
				  rd_ObservationSettlementNoted=retrieve_radioname(cf.rd_obs13_opt1_yes);
				  break;
			  case R.id.obs8_opt1_rd_n: /** SELECTING OBSERVATION8 RADIOBUTTON NO **/
				  cf.etcomments_obs8_opt1.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_PoolDeckSlabNoted=retrieve_radioname(cf.rd_obs8_opt1_no);
				  break;
			  case R.id.obs9_opt1_rd_n: /** SELECTING OBSERVATION9 RADIOBUTTON NO **/
				  cf.show_layt_obs9_opt1.setVisibility(v.GONE);
				  cf.etcomments_obs9_opt1.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_PoolShellPlumbLevel=retrieve_radioname(cf.rd_obs9_opt1_no);
				  break;
			  case R.id.obs10_opt1_rd_n: /** SELECTING OBSERVATION10 RADIOBUTTON NO **/
				  cf.show_layt_obs10_opt1.setVisibility(v.GONE);
				  cf.etcomments_obs10_opt1.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_ExcessiveSettlement=retrieve_radioname(cf.rd_obs10_opt1_no);
			      cf.Set_UncheckBox(cf.cb_obs101,cf.etother_obs10_typeofcrack);
			      cf.Set_UncheckBox(cf.cb_obs102,cf.etother_obs10_location);
			      cf.Set_UncheckBox(cf.cb_obs103,cf.etother_obs10_approx);
			      cf.etother_obs10_etlen.setText("");
			      cf.Set_UncheckBox(cf.cb_obs104,cf.etother_obs10_approx);
			      cf.Set_UncheckBox(cf.cb_obs105,cf.etother_obs10_cause);
				  break;
			  case R.id.obs10_opta_rd_no: /** SELECTING OBSERVATION10 - OPTIONA RADIOBUTTON NO **/
				  cf.etcomments_obs10_opta.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_WallLeaningNoted=retrieve_radioname(cf.rd_obs10_opta_no);
				  break;
			  case R.id.obs10_optb_rd_no: /** SELECTING OBSERVATION10 - OPTIONB RADIOBUTTON NO **/
				  cf.etcomments_obs10_optb.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_WallsVisiblyNotLevel=retrieve_radioname(cf.rd_obs10_optb_no);
				  break;
			  case R.id.obs10_optc_rd_no: /** SELECTING OBSERVATION10 - OPTIONC RADIOBUTTON NO **/
				  cf.etcomments_obs10_optc.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_WallsVisiblyBulgingNoted=retrieve_radioname(cf.rd_obs10_optc_no);
				  break;
			  case R.id.obs10_optd_rd_no: /** SELECTING OBSERVATION10 - OPTIOND RADIOBUTTON NO **/
				  cf.etcomments_obs10_optd.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_OpeningsOutofSquare=retrieve_radioname(cf.rd_obs10_optd_no);
				  break;
			  case R.id.obs10_opte_rd_no: /** SELECTING OBSERVATION10 - OPTIONE RADIOBUTTON NO **/
				  cf.etcomments_obs10_opte.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_DamagedFinishesNoted=retrieve_radioname(cf.rd_obs10_opte_no);
				  break;
			  case R.id.obs11_opt1_rd_n: /** SELECTING OBSERVATION11 RADIOBUTTON NO **/
				  cf.show_layt_obs11_opt1.setVisibility(v.GONE);
				  cf.etcomments_obs11_opt1.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_SeperationCrackNoted=retrieve_radioname(cf.rd_obs11_opt1_no);
				  cf.Set_UncheckBox(cf.cb_obs11,cf.etother_obs11_opt1);
			     
				  break;
			  case R.id.obs12_opt1_rd_n: /** SELECTING OBSERVATION12 RADIOBUTTON NO **/
				  cf.show_layt_obs12_opt1.setVisibility(v.GONE);
				  cf.etcomments_obs12_opt1.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_ExteriorOpeningCracksNoted=retrieve_radioname(cf.rd_obs12_opt1_no);
				  cf.Set_UncheckBox(cf.cb_obs121,cf.etother_obs12_typeofcrack);
			      cf.Set_UncheckBox(cf.cb_obs122,cf.etother_obs12_location);
			      cf.Set_UncheckBox(cf.cb_obs123,cf.etother_obs12_approx);
			      cf.etother_obs12_etlen.setText("");
			      cf.Set_UncheckBox(cf.cb_obs124,cf.etother_obs12_approx);
			      cf.Set_UncheckBox(cf.cb_obs125,cf.etother_obs12_cause);
				  break;
			  case R.id.obs13_opt1_rd_n: /** SELECTING OBSERVATION13 RADIOBUTTON NO **/
				  cf.show_layt_obs13_opt1.setVisibility(v.GONE);
				  cf.etcomments_obs13_opt1.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_ObservationSettlementNoted=retrieve_radioname(cf.rd_obs13_opt1_no);
				  cf.Set_UncheckBox(cf.cb_obs131,cf.etother_obs13_typeofcrack);
			      cf.Set_UncheckBox(cf.cb_obs132,cf.etother_obs13_location);
			      cf.Set_UncheckBox(cf.cb_obs133,cf.etother_obs13_approx);
			      cf.etother_obs13_etlen.setText("");
			      cf.Set_UncheckBox(cf.cb_obs134,cf.etother_obs13_approx);
			      cf.Set_UncheckBox(cf.cb_obs135,cf.etother_obs13_cause);
				  break;
			  case R.id.obs8_opt1_rd_nd:/** SELECTING OBSERVATION8 RADIOBUTTON NOTDETERMINED **/
				  cf.etcomments_obs8_opt1.setText("No pool present.");
				  rd_PoolDeckSlabNoted=retrieve_radioname(cf.rd_obs8_opt1_nd);
				  break;
			  case R.id.obs9_opt1_rd_nd:/** SELECTING OBSERVATION9 RADIOBUTTON NOTDETERMINED **/
				  cf.show_layt_obs9_opt1.setVisibility(v.GONE);
				  cf.etcomments_obs9_opt1.setText("No pool present.");
				  rd_PoolShellPlumbLevel=retrieve_radioname(cf.rd_obs9_opt1_nd);
				  break;
			  case R.id.obs10_opt1_rd_nd:/** SELECTING OBSERVATION10 RADIOBUTTON NOTDETERMINED **/
				  cf.show_layt_obs10_opt1.setVisibility(v.GONE);
				  cf.etcomments_obs10_opt1.setText("");
				  rd_ExcessiveSettlement=retrieve_radioname(cf.rd_obs10_opt1_nd);
				  cf.Set_UncheckBox(cf.cb_obs101,cf.etother_obs10_typeofcrack);
			      cf.Set_UncheckBox(cf.cb_obs102,cf.etother_obs10_location);
			      cf.Set_UncheckBox(cf.cb_obs103,cf.etother_obs10_approx);
			      cf.etother_obs10_etlen.setText("");
			      cf.Set_UncheckBox(cf.cb_obs104,cf.etother_obs10_approx);
			      cf.Set_UncheckBox(cf.cb_obs105,cf.etother_obs10_cause);
				  break;
			  case R.id.obs10_opta_rd_nd:/** SELECTING OBSERVATION10 - OPTIONA RADIOBUTTON NOTDETERMINED **/
				  cf.etcomments_obs10_opta.setText("");
				  rd_WallLeaningNoted=retrieve_radioname(cf.rd_obs10_opta_nd);
				  break;
			  case R.id.obs10_optb_rd_nd:/** SELECTING OBSERVATION10 - OPTIONB RADIOBUTTON NOTDETERMINED **/
				  cf.etcomments_obs10_optb.setText("");
				  rd_WallsVisiblyNotLevel=retrieve_radioname(cf.rd_obs10_optb_nd);
				  break;
			  case R.id.obs10_optc_rd_nd:/** SELECTING OBSERVATION10 - OPTIONC RADIOBUTTON NOTDETERMINED **/
				  cf.etcomments_obs10_optc.setText("");
				  rd_WallsVisiblyBulgingNoted=retrieve_radioname(cf.rd_obs10_optc_nd);
				  break;
			  case R.id.obs10_optd_rd_nd:/** SELECTING OBSERVATION10 - OPTIOND RADIOBUTTON NOTDETERMINED **/
				  cf.etcomments_obs10_optd.setText("");
				  rd_OpeningsOutofSquare=retrieve_radioname(cf.rd_obs10_optd_nd);
				  break;
			  case R.id.obs10_opte_rd_nd:/** SELECTING OBSERVATION10 - OPTIONE RADIOBUTTON NOTDETERMINED **/
				  cf.etcomments_obs10_opte.setText("");
				  rd_DamagedFinishesNoted=retrieve_radioname(cf.rd_obs10_opte_nd);
				  break;
			  case R.id.obs11_opt1_rd_nd:/** SELECTING OBSERVATION11 RADIOBUTTON NOTDETERMINED **/
				  cf.show_layt_obs11_opt1.setVisibility(v.GONE);
				  cf.etcomments_obs11_opt1.setText("");
				  rd_SeperationCrackNoted=retrieve_radioname(cf.rd_obs11_opt1_nd);
				  cf.Set_UncheckBox(cf.cb_obs11,cf.etother_obs11_opt1);
				  break;
			  case R.id.obs12_opt1_rd_nd:/** SELECTING OBSERVATION12 RADIOBUTTON NOTDETERMINED **/
				  cf.show_layt_obs12_opt1.setVisibility(v.GONE);
				  cf.etcomments_obs12_opt1.setText("");
				  rd_ExteriorOpeningCracksNoted=retrieve_radioname(cf.rd_obs12_opt1_nd);
				  cf.Set_UncheckBox(cf.cb_obs121,cf.etother_obs12_typeofcrack);
			      cf.Set_UncheckBox(cf.cb_obs122,cf.etother_obs12_location);
			      cf.Set_UncheckBox(cf.cb_obs123,cf.etother_obs12_approx);
			      cf.etother_obs12_etlen.setText("");
			      cf.Set_UncheckBox(cf.cb_obs124,cf.etother_obs12_approx);
			      cf.Set_UncheckBox(cf.cb_obs125,cf.etother_obs12_cause);
				  break;
			  case R.id.obs13_opt1_rd_nd:/** SELECTING OBSERVATION13 RADIOBUTTON NOTDETERMINED **/
				  cf.show_layt_obs13_opt1.setVisibility(v.GONE);
				  cf.etcomments_obs13_opt1.setText("");
				  rd_ObservationSettlementNoted=retrieve_radioname(cf.rd_obs13_opt1_nd);
				  cf.Set_UncheckBox(cf.cb_obs131,cf.etother_obs13_typeofcrack);
			      cf.Set_UncheckBox(cf.cb_obs132,cf.etother_obs13_location);
			      cf.Set_UncheckBox(cf.cb_obs133,cf.etother_obs13_approx);
			      cf.etother_obs13_etlen.setText("");
			      cf.Set_UncheckBox(cf.cb_obs134,cf.etother_obs13_approx);
			      cf.Set_UncheckBox(cf.cb_obs135,cf.etother_obs13_cause);
				  break;
			 case R.id.obs11_opt1_other:/**SELECTING CHECKBOX - OBSERVATION11 -  OTHER **/
				  if(cf.cb_obs11[cf.cb_obs11.length-1].isChecked())
				  {
					  cf.etother_obs11_opt1.setVisibility(visibility);
					  cf.etother_obs11_opt1.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs11_opt1.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			 case R.id.obs12_typeofcrack_other:/** SELECTING CHECKBOX - OBSERVATION12 - OTHER **/
				  if(cf.cb_obs121[cf.cb_obs121.length-1].isChecked())
				  {
					  cf.etother_obs12_typeofcrack.setVisibility(visibility);
				      cf.etother_obs12_typeofcrack.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs12_typeofcrack.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			 case R.id.obs12_location_other:/** SELECTING CHECKBOX - OBSERVATION12 - LOCATION- OTHER **/
				  if(cf.cb_obs122[cf.cb_obs122.length-1].isChecked())
				  {
					  cf.etother_obs12_location.setVisibility(visibility);
					  cf.etother_obs12_location.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs12_location.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			 case R.id.obs12_approx_other:/** SELECTING CHECKBOX - OBSERVATION12 - LOCATION- OTHER **/
				  if(cf.cb_obs123[cf.cb_obs123.length-1].isChecked())
				  {
					  cf.etother_obs12_approx.setVisibility(visibility);
					  cf.etother_obs12_approx.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs12_approx.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			 case R.id.obs12_cause_other:/** SELECTING CHECKBOX - OBSERVATION12 - CAUSE - OTHER **/
				  if(cf.cb_obs125[cf.cb_obs125.length-1].isChecked())
				  {
					  cf.etother_obs12_cause.setVisibility(visibility);
					  cf.etother_obs12_cause.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs12_cause.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			 case R.id.obs13_typeofcrack_other:/** SELECTING CHECKBOX - OBSERVATION13 - OTHER **/
				  if(cf.cb_obs131[cf.cb_obs131.length-1].isChecked())
				  {
					 cf.etother_obs13_typeofcrack.setVisibility(visibility);
					 cf.etother_obs13_typeofcrack.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs13_typeofcrack.setVisibility(cf.show.GONE);
					 
				  }
			 case R.id.obs13_location_other:/** SELECTING CHECKBOX - OBSERVATION13 - LOCATION- OTHER **/
				  if(cf.cb_obs132[cf.cb_obs132.length-1].isChecked())
				  {
					 cf.etother_obs13_location.setVisibility(visibility);
					 cf.etother_obs13_location.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs13_location.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			 case R.id.obs13_approx_other:/** SELECTING CHECKBOX - OBSERVATION13 - LOCATION- OTHER **/
				  if(cf.cb_obs133[cf.cb_obs133.length-1].isChecked())
				  {
					 cf.etother_obs13_approx.setVisibility(visibility);
					 cf.etother_obs13_approx.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs13_approx.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			 case R.id.obs13_cause_other:/** SELECTING CHECKBOX - OBSERVATION13 - CAUSE - OTHER **/
				  if(cf.cb_obs135[cf.cb_obs135.length-1].isChecked())
				  {
					 cf.etother_obs13_cause.setVisibility(visibility);
					 cf.etother_obs13_cause.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs13_cause.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			 case R.id.obs10_typeofcrack_other:/** SELECTING CHECKBOX - OBSERVATION10 - OTHER **/
				  if(cf.cb_obs101[cf.cb_obs101.length-1].isChecked())
				  {
					 cf.etother_obs10_typeofcrack.setVisibility(visibility);
				     cf.etother_obs10_typeofcrack.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs10_typeofcrack.setVisibility(cf.show.GONE);
					 
				  }
			     break;
			 case R.id.obs10_location_other:/** SELECTING CHECKBOX - OBSERVATION10 - LOCATION- OTHER **/
				  if(cf.cb_obs102[cf.cb_obs102.length-1].isChecked())
				  {
					  cf.etother_obs10_location.setVisibility(visibility);
					  cf.etother_obs10_location.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs10_location.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			 case R.id.obs10_approx_other:/** SELECTING CHECKBOX - OBSERVATION10 - LOCATION- OTHER **/
				  if(cf.cb_obs103[cf.cb_obs103.length-1].isChecked())
				  {
					  cf.etother_obs10_approx.setVisibility(visibility);
					  cf.etother_obs10_approx.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs10_approx.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			 case R.id.obs10_cause_other:/** SELECTING CHECKBOX - OBSERVATION10 - CAUSE - OTHER **/
				  if(cf.cb_obs105[cf.cb_obs105.length-1].isChecked())
				  {
					 cf.etother_obs10_cause.setVisibility(visibility);
					 cf.etother_obs10_cause.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs10_cause.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			
			  }
		}
	 }
	 public void clicker(View v) {

         switch(v.getId()){
			  case R.id.obs8_opt1_help: /** HELP FOR OBSERVATION8**/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Pool deck - cracking /settlement is a common finding on almost all homes with a pool " +
				  		"incorporating a concrete surface - finish.  Most times the cracking is " +
				  		"related to initial settlement and shrinkage whereby the ground " +
				  		"underneath the slab was not properly compacted or the thickness of " +
				  		"concrete/construction workmanship was not correct.</p><p>Pool deck " +
				  		"cracking of all nature must be recorded, photographed and outlined " +
				  		"within this report.  If hairline fractures are noted at the corners, " +
				  		"these are typical.  Areas of more serious concern are those where " +
				  		"displacement is evident.</p><p>Displacement is where one side of the " +
				  		"patio/slab cracking is higher or uneven compared to the opposite side of " +
				  		"the crack.  Particular attention should be drawn to the pool shell where " +
				  		"it meets the pool deck to ensure the pool is properly installed, level " +
				  		"and plumb and any holes noted are not corresponding on the pool " +
				  		"structure.</p><p>Inspectors should compare the water level with the top " +
				  		"tile or perimeter of the pool.  If level and plumb, the water level will " +
				  		"be the same distance around the perimeter, measured to the closest " +
				  		"tile.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs9_opt1_help: /** HELP FOR OBSERVATION9**/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>If pools are present, " +
				  		"Inspectors should take a 4\" spirit level on the pool perimeter " +
				  		"and photograph.  Inspectors can do this by reviewing the water line, to " +
				  		"check for out of plumb compared to the perimeter tile.  The distance " +
				  		"between the top of the water and the coping or tile should be the same " +
				  		"throughout the perimeter of the pool.  Any evidence of displacement or " +
				  		"unevenness should be recorded as a potentially poorly constructed or off " +
				  		"level pool or other settlement issue.</p><p>Settling pools will also " +
				  		"change electrical and plumbing lines causing additional damage.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs10_opt1_help: /** HELP FOR OBSERVATION10**/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>An inadequately " +
				  		"ventilated crawlspace can cause potential rot and decay to the main " +
				  		"floor structure and unevenness to the floor as a result of floor " +
				  		"structure failure.  This is particularly prevalent to areas where the " +
				  		"undercarriage is exposed with limited ventilation and the water table is " +
				  		"high or standing water was present within the crawlspace " +
				  		"area.</p><p>Proper ventilation is crucial for the life cycle of the " +
				  		"building in question.</p><p>Any issues associated with ventilation or " +
				  		"lack thereof should be recorded and photographed accordingly.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs10_opta_help: /** HELP FOR OBSERVATION10 - OPTIONA **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors need to pay " +
				  		"close attention to all exterior wall surfaces on every floor level, " +
				  		"around every opening, building corners, at roof level, etc. to ensure " +
				  		"walls are not leaning or off plumb in any area.</p><p>A 4 foot level " +
				  		"should be used and photographed to verify the inspector\'s " +
				  		"findings in relation to the exterior wall survey.</p><p>If the Inspector " +
				  		"is unable to determine or evaluate the entire exterior area because of " +
				  		"the type of structure, zero lot line or other, this needs to be " +
				  		"identified in the comment area accordingly.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs10_optb_help: /** HELP FOR OBSERVATION10 - OPTIONB **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>All horizontal surfaces, " +
				  		"including windowsills, door openings, window openings, decorative " +
				  		"features or any other feature where horizontal surfaces are present on " +
				  		"the exterior of a structure should be checked for any unevenness as a " +
				  		"result of settlement.  Inspectors need to photograph horizontal sills " +
				  		"with a small level (torpedo level) to verify to the quality assurance " +
				  		"department that sills were or were not affected and comment " +
				  		"accordingly.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs10_optc_help: /** HELP FOR OBSERVATION10 - OPTIONC **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors need to pay " +
				  		"close attention to the exterior wall surfaces/finishes of every floor " +
				  		"level, for any evidence of bulging or unevenness.</p><p>A 4 foot level " +
				  		"should be used and photographed to verify the inspector\'s " +
				  		"findings in relation to the exterior wall survey.</p><p>If the Inspector " +
				  		"is unable to determine or evaluate the entire exterior area because of " +
				  		"the type of structure, zero lot line or other, this should be clearly " +
				  		"identified in the comment area.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs10_optd_help: /** HELP FOR OBSERVATION10 - OPTIOND **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors are required " +
				  		"to observe all openings on the exterior of the structure, example tests " +
				  		"using a 4\"; spirit level and verify using photographs and level " +
				  		"in place and record that openings were not out of square or uneven.  " +
				  		"Photograph annotations should clearly say \"No openings or sills " +
				  		"were found out of square or not level\".</p><p>Inspectors are " +
				  		"required to sample or examine using a 4\" spirit level and a " +
				  		"torpedo level as needed, a representative sampling of openings and sills " +
				  		"- horizontal surface  including a minimum of 1 per elevation and " +
				  		"1 per floor.</p><p>If any openings are not visible or accessible based " +
				  		"on the perimeter or surrounding features such as large trees, " +
				  		"structures, etc., this should be outlined within the Comment " +
				  		"area/photograph section.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs10_opte_help: /** HELP FOR OBSERVATION10 - OPTIONE **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors should pay " +
				  		"close attention to all finishes throughout the exterior of the home to " +
				  		"determine if any damage exists that may be associated with settlement " +
				  		"issues.</p><p>For example, cracks in stucco associated with structural " +
				  		"settlement should be outlined.  Scrapes and scratches relating to " +
				  		"lawnmower damage are not relevant and should merely be " +
				  		"photographed.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs11_opt1_help: /** HELP FOR OBSERVATION11**/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors are required " +
				  		"to determine what type of separation cracks are present by either " +
				  		"selecting, New Addition, Differing Materials or outlining using the " +
				  		"Other checkbox selection and describing what separation cracks were " +
				  		"noted.</p><p>Separation cracks are normally associated with additions " +
				  		"where new and old constructions meet, differing materials adjoining, " +
				  		"where siding possibly meets stucco and plaster applications or around " +
				  		"windows where windows may be separating or caulk splitting.</p><p>All " +
				  		"separation cracks should be properly identified, photographed and " +
				  		"commented if present.  If any of these cracks are possibly related to " +
				  		"sinkhole or major structural issues, these should be outlined clearly on " +
				  		"the photograph annotations and comment areas.  Use a 4\" spirit " +
				  		"level or torpedo level in photographs as needed to augment your " +
				  		"findings.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs12_opt1_help: /** HELP FOR OBSERVATION12**/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>All Inspectors need to " +
				  		"review all openings present on the exterior of the home, including " +
				  		"windows and doors.  Cracking of any nature, including diagonal cracking " +
				  		"from top corners, cracking along the bottom or at the junction of the " +
				  		"window frames and openings needs to be recorded, photographed and " +
				  		"commented.</p><p>Typical hairline fractures are to be expected and this " +
				  		"must be outlined via the various selections and the probable cause if " +
				  		"noted.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs13_opt1_help: /** HELP FOR OBSERVATION13**/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors are required " +
				  		"to observe the conditions associated with all adjoining property which " +
				  		"includes properties on both sides, back and front, where visible and " +
				  		"accessible.</p><p>Inspectors should walk along the footpath on the front " +
				  		"of adjoining homes to verify if any issues exist that may be associated " +
				  		"with potential sinkhole issues.</p><p>Based on the limitations at the " +
				  		"time of inspection, Inspectors should clearly outline the extent of " +
				  		"inspections conducted and any limitations related to these " +
				  		"observations.</p><p>At no time should Inspectors be entering adjoining " +
				  		"homes, properties as part of this inspection service.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.home: /*SELECTING HOME */
				  cf.gohome();
				  break;
			  case R.id.save: /*SELECTING SAVE*/
				  fn_save();
				  break;
						  
         }
	 }
	
	 private void fn_save() {

		// TODO Auto-generated method stub
		    if(rd_PoolDeckSlabNoted.equals(""))/* CHECK QUESTION NO.8 RADIO BUTON*/
			{
				
				cf.ShowToast("Please select any answer for Question No.8", 0);
				cf.rd_obs8_opt1_yes.requestFocus();
			}
			else
			{
				if(cf.etcomments_obs8_opt1.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.8 */
				{
					cf.ShowToast("Please enter Question No.8 Comments", 0);
					cf.etcomments_obs8_opt1.requestFocus();
				}
				else
				{
					if(rd_PoolShellPlumbLevel.equals(""))/* CHECK QUESTION NO.9 RADIO BUTON*/
					{
						
						cf.ShowToast("Please select any answer for Question No.9", 0);
						cf.rd_obs9_opt1_yes.requestFocus();
					}
					else
					{
						if(cf.etcomments_obs9_opt1.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.9 */
						{
							cf.ShowToast("Please enter Question No.9 Comments", 0);
							cf.etcomments_obs9_opt1.requestFocus();
						}
						else
						{
							if(rd_ExcessiveSettlement.equals(""))/* CHECK QUESTION NO.10 RADIO BUTON*/
							{
								
								cf.ShowToast("Please select any answer for Question No.10", 0);
								cf.rd_obs9_opt1_yes.requestFocus();
							}
							else
							{
								if(rd_ExcessiveSettlement.equals("Yes"))
								{
									 chk_Value101= cf.getselected_chk(cf.cb_obs101);
									 String chk_Value101other =(cf.cb_obs101[cf.cb_obs101.length-1].isChecked())? "^"+cf.etother_obs10_typeofcrack.getText().toString():""; // append the other text value in to the selected option
									 chk_Value101+=chk_Value101other;
									 if(chk_Value101.equals(""))
									 {
										 
										 cf.ShowToast("Please select any checkbox for Question No.10 Types of cracks noted" , 0);
									 }
									 else
									 {
										 if(cf.cb_obs101[cf.cb_obs101.length-1].isChecked())
										 {
											 if(chk_Value101other.trim().equals("^"))
											 {
												 cf.ShowToast("Please enter other text for Question No.10 Types of cracks noted", 0);
												 cf.etother_obs10_typeofcrack.requestFocus();
											 }
											 else
											 {
												 chk_Value101 +=(cf.cb_obs101[cf.cb_obs101.length-1].isChecked())? "^"+cf.etother_obs10_typeofcrack.getText().toString():""; // append the other text value in to the selected option
												 chk_LocationOfCracks();	
											 }
										 }
										 else
										 {
											 chk_LocationOfCracks();
										 }
									 }
								}
								else
								{
									 Chk_Obs10_Comments();	
								}
							}
						}
					}
				}
			}
	}

	private void chk_LocationOfCracks() {
		// TODO Auto-generated method stub
		 chk_Value102= cf.getselected_chk(cf.cb_obs102);
		 String chk_Value102other =(cf.cb_obs102[cf.cb_obs102.length-1].isChecked())? "^"+cf.etother_obs10_location.getText().toString():""; // append the other text value in to the selected option
		 chk_Value102+=chk_Value102other;
		 if(chk_Value102.equals(""))
		 {
			 
			 cf.ShowToast("Please select any checkbox for Question No.10 Location of cracks." , 0);
		 }
		 else
		 {
			 if(cf.cb_obs102[cf.cb_obs102.length-1].isChecked())
			 {
				 if(chk_Value102other.trim().equals("^"))
				 {
					 cf.ShowToast("Please enter other text for Question No.10 Location of cracks.", 0);
					 cf.etother_obs10_location.requestFocus();
				 }
				 else
				 {
					 chk_Value102 +=(cf.cb_obs102[cf.cb_obs102.length-1].isChecked())? "^"+cf.etother_obs10_location.getText().toString():""; // append the other text value in to the selected option
					 chk_WidthOfCracks();	
				 }
			 }
			 else
			 {
				 chk_WidthOfCracks();
			 }
		 }
	}

	private void chk_WidthOfCracks() {
		// TODO Auto-generated method stub
		 chk_Value103= cf.getselected_chk(cf.cb_obs103);
		 String chk_Value103other =(cf.cb_obs103[cf.cb_obs103.length-1].isChecked())? "^"+cf.etother_obs10_approx.getText().toString():""; // append the other text value in to the selected option
		 chk_Value103+=chk_Value103other;
		 if(chk_Value103.equals(""))
		 {
			 
			 cf.ShowToast("Please select any checkbox for Question No.10 Approximate width of cracks." , 0);
		 }
		 else
		 {
			 if(cf.cb_obs103[cf.cb_obs103.length-1].isChecked())
			 {
				 if(chk_Value103other.trim().equals("^"))
				 {
					 cf.ShowToast("Please enter other text for Question No.10 Approximate width of cracks.", 0);
					 cf.etother_obs10_approx.requestFocus();
				 }
				 else
				 {
					 chk_Value103 +=(cf.cb_obs103[cf.cb_obs103.length-1].isChecked())? "^"+cf.etother_obs10_approx.getText().toString():""; // append the other text value in to the selected option
					 chk_LengthOfCracks();	
				 }
			 }
			 else
			 {
				 chk_LengthOfCracks();
			 }
		 }
	}

	private void chk_LengthOfCracks() {
		// TODO Auto-generated method stub
		if(cf.etother_obs10_etlen.getText().toString().equals("")) /*CHECK LENGTH QUESTION NO.10 */
		{
			cf.ShowToast("Please enter Question No.10 Approximate Length of cracks.", 0);
			cf.etother_obs10_etlen.requestFocus();
		}
		else
		{
			 chk_Value104= cf.getselected_chk(cf.cb_obs104);
			 if(chk_Value104.equals(""))
			 {
				 
				 cf.ShowToast("Please select any checkbox for Question No.10 Conditions of cracks noted." , 0);
			 }
			 else
			 {
				 chk_Value105= cf.getselected_chk(cf.cb_obs105);
				 String chk_Value105other =(cf.cb_obs105[cf.cb_obs105.length-1].isChecked())? "^"+cf.etother_obs10_cause.getText().toString():""; // append the other text value in to the selected option
				 chk_Value105+=chk_Value105other;
				 if(chk_Value105.equals(""))
				 {
					 
					 cf.ShowToast("Please select any checkbox for Question No.10 Probable cause." , 0);
				 }
				 else
				 {
					 if(cf.cb_obs105[cf.cb_obs105.length-1].isChecked())
					 {
						 if(chk_Value105other.trim().equals("^"))
						 {
							 cf.ShowToast("Please enter other text for Question No.10 Probable cause.", 0);
							 cf.etother_obs10_cause.requestFocus();
						 }
						 else
						 {
							 chk_Value105 +=(cf.cb_obs105[cf.cb_obs105.length-1].isChecked())? "^"+cf.etother_obs10_cause.getText().toString():""; // append the other text value in to the selected option
							 Chk_Obs10_Comments();	
						 }
					 }
					 else
					 {
						 Chk_Obs10_Comments();
					 }
				 }
			 }
		}
	}

	private void Chk_Obs10_Comments() {
		// TODO Auto-generated method stub
		if(cf.etcomments_obs10_opt1.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.10 */
		{
			cf.ShowToast("Please enter Question No.10 Comments", 0);
			cf.etcomments_obs10_opt1.requestFocus();
		}
		else
		{
			if(rd_WallLeaningNoted.equals(""))/* CHECK QUESTION NO.10(A) RADIO BUTON*/
			{
				cf.ShowToast("Please select any answer for Question No.10(A)", 0);
				cf.rd_obs10_opta_yes.requestFocus();
			}
			else
			{
				if(cf.etcomments_obs10_opta.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.10(A) */
				{
					cf.ShowToast("Please enter Question No.10(A) Comments", 0);
					cf.etcomments_obs10_opta.requestFocus();
				}
				else
				{
					if(rd_WallsVisiblyNotLevel.equals(""))/* CHECK QUESTION NO.10(B) RADIO BUTON*/
					{
						cf.ShowToast("Please select any answer for Question No.10(B)", 0);
						cf.rd_obs10_optb_yes.requestFocus();
					}
					else
					{
						if(cf.etcomments_obs10_optb.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.10(B) */
						{
							cf.ShowToast("Please enter Question No.10(B) Comments", 0);
							cf.etcomments_obs10_optb.requestFocus();
						}
						else
						{
							if(rd_WallsVisiblyBulgingNoted.equals(""))/* CHECK QUESTION NO.10(C) RADIO BUTON*/
							{
								cf.ShowToast("Please select any answer for Question No.10(C)", 0);
								cf.rd_obs10_optc_yes.requestFocus();
							}
							else
							{
								if(cf.etcomments_obs10_optc.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.10(C) */
								{
									cf.ShowToast("Please enter Question No.10(C) Comments", 0);
									cf.etcomments_obs10_optc.requestFocus();
								}
								else
								{
									if(rd_OpeningsOutofSquare.equals(""))/* CHECK QUESTION NO.10(D) RADIO BUTON*/
									{
										cf.ShowToast("Please select any answer for Question No.10(D)", 0);
										cf.rd_obs10_optd_yes.requestFocus();
									}
									else
									{
										if(cf.etcomments_obs10_optd.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.10(d) */
										{
											cf.ShowToast("Please enter Question No.10(D) Comments", 0);
											cf.etcomments_obs10_optd.requestFocus();
										}
										else
										{
											if(rd_DamagedFinishesNoted.equals(""))/* CHECK QUESTION NO.10(E) RADIO BUTON*/
											{
												cf.ShowToast("Please select any answer for Question No.10(E)", 0);
												cf.rd_obs10_opte_yes.requestFocus();
											}
											else
											{
												if(cf.etcomments_obs10_opte.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.10(E) */
												{
													cf.ShowToast("Please enter Question No.10(E) Comments", 0);
													cf.etcomments_obs10_opte.requestFocus();
												}
												else
												{
													if(rd_SeperationCrackNoted.equals(""))/* CHECK QUESTION NO.11 RADIO BUTON*/
													{
														cf.ShowToast("Please select any answer for Question No.11", 0);
														cf.rd_obs11_opt1_yes.requestFocus();
													}
													else
													{
														if(rd_SeperationCrackNoted.equals("Yes"))
														{
															 chk_Value11= cf.getselected_chk(cf.cb_obs11);
															 String chk_Value11other =(cf.cb_obs11[cf.cb_obs11.length-1].isChecked())? "^"+cf.etother_obs11_opt1.getText().toString():""; // append the other text value in to the selected option
															 chk_Value11+=chk_Value11other;
															 if(chk_Value11.equals(""))
															 {
																 
																 cf.ShowToast("Please select any checkbox for Question No.11 " , 0);
															 }
															 else
															 {
																 if(cf.cb_obs11[cf.cb_obs11.length-1].isChecked())
																 {
																	 if(chk_Value11other.trim().equals("^"))
																	 {
																		 cf.ShowToast("Please enter other text for Question No.11.", 0);
																		 cf.etother_obs11_opt1.requestFocus();
																	 }
																	 else
																	 {
																		 chk_Value11 +=(cf.cb_obs11[cf.cb_obs11.length-1].isChecked())? "^"+cf.etother_obs11_opt1.getText().toString():""; // append the other text value in to the selected option
																		 Chk_Obs11_Comments();	
																	 }
																 }
																 else
																 {
																	 Chk_Obs11_Comments();
																 }
															 }
														}
														else
														{
															 Chk_Obs11_Comments();	
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private void Chk_Obs11_Comments() {
		// TODO Auto-generated method stub
		if(cf.etcomments_obs11_opt1.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.11 */
		{
			cf.ShowToast("Please enter Question No.11 Comments", 0);
			cf.etcomments_obs11_opt1.requestFocus();
		}
		else
		{
			if(rd_ExteriorOpeningCracksNoted.equals(""))/* CHECK QUESTION NO.12 RADIO BUTON*/
			{
				cf.ShowToast("Please select any answer for Question No.12", 0);
				cf.rd_obs12_opt1_yes.requestFocus();
			}
			else
			{
				if(rd_ExteriorOpeningCracksNoted.equals("Yes"))
				{
					 chk_Value121= cf.getselected_chk(cf.cb_obs121);
					 String chk_Value121other =(cf.cb_obs121[cf.cb_obs121.length-1].isChecked())? "^"+cf.etother_obs12_typeofcrack.getText().toString():""; // append the other text value in to the selected option
					 chk_Value121+=chk_Value121other;
					 if(chk_Value121.equals(""))
					 {
						 
						 cf.ShowToast("Please select any checkbox for Question No.12 Types of cracks noted" , 0);
					 }
					 else
					 {
						 if(cf.cb_obs121[cf.cb_obs121.length-1].isChecked())
						 {
							 if(chk_Value121other.trim().equals("^"))
							 {
								 cf.ShowToast("Please enter other text for Question No.12 Types of cracks noted", 0);
								 cf.etother_obs12_typeofcrack.requestFocus();
							 }
							 else
							 {
								 chk_Value121 +=(cf.cb_obs121[cf.cb_obs121.length-1].isChecked())? "^"+cf.etother_obs12_typeofcrack.getText().toString():""; // append the other text value in to the selected option
								 chk_Obs12_LocationOfCracks();	
							 }
						 }
						 else
						 {
							 chk_Obs12_LocationOfCracks();
						 }
					 }
				}
				else
				{
					 Chk_Obs12_Comments();	
				}
			}
		}
	}

	private void chk_Obs12_LocationOfCracks() {
		// TODO Auto-generated method stub
		 chk_Value122= cf.getselected_chk(cf.cb_obs122);
		 String chk_Value122other =(cf.cb_obs122[cf.cb_obs122.length-1].isChecked())? "^"+cf.etother_obs12_location.getText().toString():""; // append the other text value in to the selected option
		 chk_Value122+=chk_Value122other;
		 if(chk_Value122.equals(""))
		 {
			 
			 cf.ShowToast("Please select any checkbox for Question No.12 Location of cracks." , 0);
		 }
		 else
		 {
			 if(cf.cb_obs122[cf.cb_obs122.length-1].isChecked())
			 {
				 if(chk_Value122other.trim().equals("^"))
				 {
					 cf.ShowToast("Please enter other text for Question No.12 Location of cracks.", 0);
					 cf.etother_obs12_location.requestFocus();
				 }
				 else
				 {
					 chk_Value122 +=(cf.cb_obs122[cf.cb_obs122.length-1].isChecked())? "^"+cf.etother_obs12_location.getText().toString():""; // append the other text value in to the selected option
					 chk_Obs12_WidthOfCracks();	
				 }
			 }
			 else
			 {
				 chk_Obs12_WidthOfCracks();
			 }
		 }
	}

	private void chk_Obs12_WidthOfCracks() {
		// TODO Auto-generated method stub
		 chk_Value123= cf.getselected_chk(cf.cb_obs123);
		 String chk_Value123other =(cf.cb_obs123[cf.cb_obs123.length-1].isChecked())? "^"+cf.etother_obs12_approx.getText().toString():""; // append the other text value in to the selected option
		 chk_Value123+=chk_Value123other;
		 if(chk_Value123.equals(""))
		 {
			 
			 cf.ShowToast("Please select any checkbox for Question No.12 Approximate width of cracks." , 0);
		 }
		 else
		 {
			 if(cf.cb_obs123[cf.cb_obs123.length-1].isChecked())
			 {
				 if(chk_Value123other.trim().equals("^"))
				 {
					 cf.ShowToast("Please enter other text for Question No.12 Approximate width of cracks.", 0);
					 cf.etother_obs12_approx.requestFocus();
				 }
				 else
				 {
					 chk_Value123 +=(cf.cb_obs123[cf.cb_obs123.length-1].isChecked())? "^"+cf.etother_obs12_approx.getText().toString():""; // append the other text value in to the selected option
					 chk_Obs12_LengthOfCracks();	
				 }
			 }
			 else
			 {
				 chk_Obs12_LengthOfCracks();
			 }
		 }
	}

	private void chk_Obs12_LengthOfCracks() {
		// TODO Auto-generated method stub
		if(cf.etother_obs12_etlen.getText().toString().equals("")) /*CHECK LENGTH QUESTION NO.12 */
		{
			cf.ShowToast("Please enter Question No.12 Approximate Length of cracks.", 0);
			cf.etother_obs12_etlen.requestFocus();
		}
		else
		{
			 chk_Value124= cf.getselected_chk(cf.cb_obs124);
			 if(chk_Value124.equals(""))
			 {
				 
				 cf.ShowToast("Please select any checkbox for Question No.12 Conditions of cracks noted." , 0);
			 }
			 else
			 {
				 chk_Value125= cf.getselected_chk(cf.cb_obs125);
				 String chk_Value125other =(cf.cb_obs125[cf.cb_obs125.length-1].isChecked())? "^"+cf.etother_obs12_cause.getText().toString():""; // append the other text value in to the selected option
				 chk_Value125+=chk_Value125other;
				 if(chk_Value125.equals(""))
				 {
					 
					 cf.ShowToast("Please select any checkbox for Question No.12 Probable cause." , 0);
				 }
				 else
				 {
					 if(cf.cb_obs125[cf.cb_obs125.length-1].isChecked())
					 {
						 if(chk_Value125other.trim().equals("^"))
						 {
							 cf.ShowToast("Please enter other text for Question No.12 Probable cause.", 0);
							 cf.etother_obs12_cause.requestFocus();
						 }
						 else
						 {
							 chk_Value125 +=(cf.cb_obs125[cf.cb_obs125.length-1].isChecked())? "^"+cf.etother_obs12_cause.getText().toString():""; // append the other text value in to the selected option
							 Chk_Obs12_Comments();	
						 }
					 }
					 else
					 {
						 Chk_Obs12_Comments();
					 }
				 }
			 }
		}
	}

	private void Chk_Obs12_Comments() {
		// TODO Auto-generated method stub
		if(cf.etcomments_obs12_opt1.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.12 */
		{
			cf.ShowToast("Please enter Question No.12 Comments.", 0);
			cf.etcomments_obs12_opt1.requestFocus();
		}
		else
		{
			if(rd_ObservationSettlementNoted.equals(""))/* CHECK QUESTION NO.13 RADIO BUTON*/
			{
				cf.ShowToast("Please select any answer for Question No.13", 0);
				cf.rd_obs13_opt1_yes.requestFocus();
			}
			else
			{
				if(rd_ObservationSettlementNoted.equals("Yes"))
				{
					 chk_Value131= cf.getselected_chk(cf.cb_obs131);
					 String chk_Value131other =(cf.cb_obs131[cf.cb_obs131.length-1].isChecked())? "^"+cf.etother_obs13_typeofcrack.getText().toString():""; // append the other text value in to the selected option
					 chk_Value131+=chk_Value131other;
					 if(chk_Value131.equals(""))
					 {
						 
						 cf.ShowToast("Please select any checkbox for Question No.13 Types of cracks noted" , 0);
					 }
					 else
					 {
						 if(cf.cb_obs131[cf.cb_obs131.length-1].isChecked())
						 {
							 if(chk_Value131other.trim().equals("^"))
							 {
								 cf.ShowToast("Please enter other text for Question No.13 Types of cracks noted", 0);
								 cf.etother_obs13_typeofcrack.requestFocus();
							 }
							 else
							 {
								 chk_Value131 +=(cf.cb_obs131[cf.cb_obs131.length-1].isChecked())? "^"+cf.etother_obs13_typeofcrack.getText().toString():""; // append the other text value in to the selected option
								 chk_Obs13_LocationOfCracks();	
							 }
						 }
						 else
						 {
							 chk_Obs13_LocationOfCracks();
						 }
					 }
				}
				else
				{
					 Chk_Obs13_Comments();	
				}
			}
		}
	}

	private void chk_Obs13_LocationOfCracks() {
		// TODO Auto-generated method stub
		 chk_Value132= cf.getselected_chk(cf.cb_obs132);
		 String chk_Value132other =(cf.cb_obs132[cf.cb_obs132.length-1].isChecked())? "^"+cf.etother_obs13_location.getText().toString():""; // append the other text value in to the selected option
		 chk_Value132+=chk_Value132other;
		 if(chk_Value132.equals(""))
		 {
			 
			 cf.ShowToast("Please select any checkbox for Question No.13 Location of cracks." , 0);
		 }
		 else
		 {
			 if(cf.cb_obs132[cf.cb_obs132.length-1].isChecked())
			 {
				 if(chk_Value132other.trim().equals("^"))
				 {
					 cf.ShowToast("Please enter other text for Question No.13 Location of cracks.", 0);
					 cf.etother_obs13_location.requestFocus();
				 }
				 else
				 {
					 chk_Value132 +=(cf.cb_obs132[cf.cb_obs132.length-1].isChecked())? "^"+cf.etother_obs13_location.getText().toString():""; // append the other text value in to the selected option
					 chk_OBS13_WidthOfCracks();	
				 }
			 }
			 else
			 {
				 chk_OBS13_WidthOfCracks();
			 }
		 }
	}

	private void chk_OBS13_WidthOfCracks() {
		// TODO Auto-generated method stub
		 chk_Value133= cf.getselected_chk(cf.cb_obs133);
		 String chk_Value133other =(cf.cb_obs133[cf.cb_obs133.length-1].isChecked())?"^"+cf.etother_obs13_approx.getText().toString():""; // append the other text value in to the selected option
		 chk_Value133+=chk_Value133other;
		 if(chk_Value133.equals(""))
		 {
			 
			 cf.ShowToast("Please select any checkbox for Question No.13 Approximate width of cracks." , 0);
		 }
		 else
		 {
			 if(cf.cb_obs133[cf.cb_obs133.length-1].isChecked())
			 {
				 if(chk_Value133other.trim().equals("^"))
				 {
					 cf.ShowToast("Please enter other text for Question No.13 Approximate width of cracks.", 0);
					 cf.etother_obs13_approx.requestFocus();
				 }
				 else
				 {
					 chk_Value133 +=(cf.cb_obs133[cf.cb_obs133.length-1].isChecked())? "^"+cf.etother_obs13_approx.getText().toString():""; // append the other text value in to the selected option
					 chk_OBS13_LengthOfCracks();	
				 }
			 }
			 else
			 {
				 chk_OBS13_LengthOfCracks();
			 }
		 }
	}

	private void chk_OBS13_LengthOfCracks() {
		// TODO Auto-generated method stub
		if(cf.etother_obs13_etlen.getText().toString().equals("")) /*CHECK LENGTH QUESTION NO.13 */
		{
			cf.ShowToast("Please enter Question No.13 Approximate Length of cracks.", 0);
			cf.etother_obs13_etlen.requestFocus();
		}
		else
		{
			 chk_Value134= cf.getselected_chk(cf.cb_obs134);
			 if(chk_Value134.equals(""))
			 {
				 
				 cf.ShowToast("Please select any checkbox for Question No.13 Conditions of cracks noted." , 0);
			 }
			 else
			 {
				 chk_Value135= cf.getselected_chk(cf.cb_obs135);
				 String chk_Value135other =(cf.cb_obs135[cf.cb_obs135.length-1].isChecked())? "^"+cf.etother_obs13_cause.getText().toString():""; // append the other text value in to the selected option
				 chk_Value135+=chk_Value135other;
				 if(chk_Value135.equals(""))
				 {
					 
					 cf.ShowToast("Please select any checkbox for Question No.13 Probable cause." , 0);
				 }
				 else
				 {
					 if(cf.cb_obs135[cf.cb_obs135.length-1].isChecked())
					 {
						 if(chk_Value135other.trim().equals("^"))
						 {
							 cf.ShowToast("Please enter other text for Question No.13 Probable cause.", 0);
							 cf.etother_obs13_cause.requestFocus();
						 }
						 else
						 {
							 chk_Value135 +=(cf.cb_obs135[cf.cb_obs135.length-1].isChecked())? "^"+cf.etother_obs13_cause.getText().toString():""; // append the other text value in to the selected option
							 Chk_Obs13_Comments();	
						 }
					 }
					 else
					 {
						 Chk_Obs13_Comments();
					 }
				 }
			 }
		}
	}

	private void Chk_Obs13_Comments() {
		// TODO Auto-generated method stub
		if(cf.etcomments_obs13_opt1.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.13 */
		{
			cf.ShowToast("Please enter Question No.13 Comments", 0);
			cf.etcomments_obs13_opt1.requestFocus();
		}
		else
		{
			OBS2_InsertOrUpdate();
		}
	}

	private void OBS2_InsertOrUpdate() {
		// TODO Auto-generated method stub
		try
		{
			Cursor OBS_save=cf.SelectTablefunction(cf.Observation2, " where SH_OBS2_SRID='"+cf.encode(cf.selectedhomeid)+"'");
		 	if(OBS_save.getCount()>0)
			{
				/* UPDATE THE OBSERVATION2 TABLE*/
		 		cf.sh_db.execSQL("UPDATE "+cf.Observation2+ " SET PoolDeckSlabNoted='"+cf.encode(rd_PoolDeckSlabNoted)+"',PoolDeckSlabComments='"+cf.encode(cf.etcomments_obs8_opt1.getText().toString())+"',"
		 				+"PoolShellPlumbLevel='"+cf.encode(rd_PoolShellPlumbLevel)+"',PoolShellPlumbLevelComments='"+cf.encode(cf.etcomments_obs9_opt1.getText().toString())+"',"
		 				+"ExcessiveSettlement='"+cf.encode(rd_ExcessiveSettlement)+"',ExcessiveSettlementTypeofCrack='"+cf.encode(chk_Value101)+"',"
		 				+"ExcessiveSettlementLocation='"+cf.encode(chk_Value102)+"',ExcessiveSettlementWidthofCrack='"+cf.encode(chk_Value103)+"',"
		 				+"ExcessiveSettlementLengthofCrack='"+cf.encode(cf.etother_obs10_etlen.getText().toString())+"',"
		 				+"ExcessiveSettlementCleanliness='"+cf.encode(chk_Value104)+"',ExcessiveSettlementProbableCause='"+cf.encode(chk_Value105)+"',"
		 				+"ExcessiveSettlementComments='"+cf.encode(cf.etcomments_obs10_opt1.getText().toString())+"',"
		 				+"WallLeaningNoted='"+cf.encode(rd_WallsVisiblyNotLevel)+"',WallLeaningComments='"+cf.encode(cf.etcomments_obs10_opta.getText().toString())+"',"
		 				+"WallsVisiblyNotLevel='"+cf.encode(rd_WallsVisiblyNotLevel)+"',WallsVisiblyNotLevelComments='"+cf.encode(cf.etcomments_obs10_optb.getText().toString())+"',"
		 				+"WallsVisiblyBulgingNoted='"+cf.encode(rd_WallsVisiblyBulgingNoted)+"',WallsVisiblyBulgingComments='"+cf.encode(cf.etcomments_obs10_optc.getText().toString())+"',"
		 				+"OpeningsOutofSquare='"+cf.encode(rd_OpeningsOutofSquare)+"',OpeningsOutofSquareComments='"+cf.encode(cf.etcomments_obs10_optd.getText().toString())+"',"
		 				+"DamagedFinishesNoted='"+cf.encode(rd_DamagedFinishesNoted)+"',DamagedFinishesComments='"+cf.encode(cf.etcomments_obs10_opte.getText().toString())+"',"
		 				+"SeperationCrackNoted='"+cf.encode(rd_SeperationCrackNoted)+"',SeperationCrackTypeofCrack='"+cf.encode(chk_Value11)+"',"
		 				+"SeperationCrackComments='"+cf.encode(cf.etcomments_obs11_opt1.getText().toString())+"',ExteriorOpeningCracksNoted='"+cf.encode(rd_ExteriorOpeningCracksNoted)+"',"
		 				+"ExteriorOpeningCracksTypeofCrack='"+cf.encode(chk_Value121)+"',ExteriorOpeningCracksLocation='"+cf.encode(chk_Value122)+"',"
		 				+"ExteriorOpeningCracksWidthofCrack='"+cf.encode(chk_Value123)+"',ExteriorOpeningCracksLengthofCrack='"+cf.encode(cf.etother_obs12_etlen.getText().toString())+"',"
		 				+"ExteriorOpeningCracksCleanliness='"+cf.encode(chk_Value124)+"',ExteriorOpeningCracksProbableCause='"+cf.encode(chk_Value125)+"',"
		 				+"ExteriorOpeningCracksComments='"+cf.encode(cf.etcomments_obs12_opt1.getText().toString())+"',ObservationSettlementNoted='"+cf.encode(rd_ObservationSettlementNoted)+"',"
		 				+"ObservationSettlementTypeofCrack='"+cf.encode(chk_Value131)+"',ObservationSettlementLocation='"+cf.encode(chk_Value132)+"',"
		 				+"ObservationSettlementWidthofCrack='"+cf.encode(chk_Value133)+"',ObservationSettlementLengthofCrack='"+cf.encode(cf.etother_obs13_etlen.getText().toString())+"',"
		 				+"ObservationSettlementCleanliness='"+cf.encode(chk_Value134)+"',ObservationSettlementProbableCause='"+cf.encode(chk_Value135)+"',"
		 				+"ObservationSettlementComments='"+cf.encode(cf.etcomments_obs13_opt1.getText().toString())+"' where SH_OBS2_SRID='"+cf.encode(cf.selectedhomeid)+"'");
		 		cf.ShowToast("Observation 8-13 has been updated sucessfully.", 1);
				nextlayout();
			}
		 	else
			{
				/* INSERT INTO OBSERVATION2 TABLE*/
		 		cf.sh_db.execSQL("INSERT INTO "+cf.Observation2+ "(SH_OBS2_SRID,PoolDeckSlabNoted,PoolDeckSlabComments,PoolShellPlumbLevel,PoolShellPlumbLevelComments ,ExcessiveSettlement,ExcessiveSettlementTypeofCrack,ExcessiveSettlementLocation,ExcessiveSettlementWidthofCrack,ExcessiveSettlementLengthofCrack  ,ExcessiveSettlementCleanliness,ExcessiveSettlementProbableCause,ExcessiveSettlementComments,WallLeaningNoted,WallLeaningComments,WallsVisiblyNotLevel ,WallsVisiblyNotLevelComments,WallsVisiblyBulgingNoted,WallsVisiblyBulgingComments,OpeningsOutofSquare,OpeningsOutofSquareComments,DamagedFinishesNoted ,DamagedFinishesComments,SeperationCrackNoted,SeperationCrackTypeofCrack,SeperationCrackComments,ExteriorOpeningCracksNoted,ExteriorOpeningCracksTypeofCrack  ,ExteriorOpeningCracksLocation,ExteriorOpeningCracksWidthofCrack,ExteriorOpeningCracksLengthofCrack,ExteriorOpeningCracksCleanliness ,ExteriorOpeningCracksProbableCause,ExteriorOpeningCracksComments,ObservationSettlementNoted,ObservationSettlementTypeofCrack   ,ObservationSettlementLocation,ObservationSettlementWidthofCrack,ObservationSettlementLengthofCrack,ObservationSettlementCleanliness  ,ObservationSettlementProbableCause,ObservationSettlementComments)" +
		 											"VALUES ('"+cf.encode(cf.selectedhomeid)+"','"+cf.encode(rd_PoolDeckSlabNoted)+"','"+cf.encode(cf.etcomments_obs8_opt1.getText().toString())+"','"+cf.encode(rd_PoolShellPlumbLevel)+"','"+cf.encode(cf.etcomments_obs9_opt1.getText().toString())+"','"+cf.encode(rd_ExcessiveSettlement)+"','"+cf.encode(chk_Value101)+"','"+cf.encode(chk_Value102)+"','"+cf.encode(chk_Value103)+"','"+cf.encode(cf.etother_obs10_etlen.getText().toString())+"','"+cf.encode(chk_Value104)+"','"+cf.encode(chk_Value105)+"','"+cf.encode(cf.etcomments_obs10_opt1.getText().toString())+"','"+cf.encode(rd_WallLeaningNoted)+"','"+cf.encode(cf.etcomments_obs10_opta.getText().toString())+"','"+cf.encode(rd_WallsVisiblyNotLevel)+"','"+cf.encode(cf.etcomments_obs10_optb.getText().toString())+"','"+cf.encode(rd_WallsVisiblyBulgingNoted)+"','"+cf.encode(cf.etcomments_obs10_optc.getText().toString())+"','"+cf.encode(rd_OpeningsOutofSquare)+"','"+cf.encode(cf.etcomments_obs10_optd.getText().toString())+"','"+cf.encode(rd_DamagedFinishesNoted)+"','"+cf.encode(cf.etcomments_obs10_opte.getText().toString())+"','"+cf.encode(rd_SeperationCrackNoted)+"','"+cf.encode(chk_Value11)+"','"+cf.encode(cf.etcomments_obs11_opt1.getText().toString())+"','"+cf.encode(rd_ExteriorOpeningCracksNoted)+"','"+cf.encode(chk_Value121)+"','"+cf.encode(chk_Value122)+"','"+cf.encode(chk_Value123)+"','"+cf.encode(cf.etother_obs12_etlen.getText().toString())+"','"+cf.encode(chk_Value124)+"','"+cf.encode(chk_Value125)+"','"+cf.encode(cf.etcomments_obs12_opt1.getText().toString())+"','"+cf.encode(rd_ObservationSettlementNoted)+"','"+cf.encode(chk_Value131)+"','"+cf.encode(chk_Value132)+"','"+cf.encode(chk_Value133)+"','"+cf.encode(cf.etother_obs13_etlen.getText().toString())+"','"+cf.encode(chk_Value134)+"','"+cf.encode(chk_Value135)+"','"+cf.encode(cf.etcomments_obs13_opt1.getText().toString())+"')");

		 		cf.ShowToast("Observation 8-13 has been saved sucessfully.", 1);
				nextlayout();
			}
		 		
		}
	   catch(Exception e)
		{
			String strerrorlog="Selection of the Observation2 table not working ";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
	
		}
	}

	private void nextlayout() {
		// TODO Auto-generated method stub
		Intent intimg = new Intent(Observation2.this,
				Observation3.class);
		intimg.putExtra("homeid", cf.selectedhomeid);
	    intimg.putExtra("InspectionType", cf.onlinspectionid);
		intimg.putExtra("status", cf.onlstatus);
	    startActivity(intimg);
	}

	public String retrieve_radioname(RadioButton rd_obs8_opt1_yes) {
		// TODO Auto-generated method stub
		 return rd_obs8_opt1_yes.getText().toString();
	}

	public void obs_tablelayout_hide() {
			// TODO Auto-generated method stub
			cf.tbl_layout_obs8.setVisibility(cf.show.GONE);
			cf.tbl_layout_obs9.setVisibility(cf.show.GONE);
			cf.tbl_layout_obs10.setVisibility(cf.show.GONE);
			cf.tbl_layout_obs11.setVisibility(cf.show.GONE);
			cf.tbl_layout_obs12.setVisibility(cf.show.GONE);
			cf.tbl_layout_obs13.setVisibility(cf.show.GONE);
			cf.tbl_row_obs8.setBackgroundResource(R.drawable.backrepeatnor);
			cf.tbl_row_obs9.setBackgroundResource(R.drawable.backrepeatnor);
			cf.tbl_row_obs10.setBackgroundResource(R.drawable.backrepeatnor);
			cf.tbl_row_obs11.setBackgroundResource(R.drawable.backrepeatnor);
			cf.tbl_row_obs12.setBackgroundResource(R.drawable.backrepeatnor);
			cf.tbl_row_obs13.setBackgroundResource(R.drawable.backrepeatnor);
			
		}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(cf.strschdate.equals("")){
				cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
			}else{Intent  myintent = new Intent(Observation2.this,Observation1.class);
			cf.putExtras(myintent);
			startActivity(myintent);}
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {

		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
 			switch (resultCode) {
 			case 0:
 				break;
 			case -1:
 				try {

 					String[] projection = { MediaStore.Images.Media.DATA };
 					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
 							null, null, null);
 					int column_index_data = cursor
 							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
 					cursor.moveToFirst();
 					String capturedImageFilePath = cursor.getString(column_index_data);
 					cf.showselectedimage(capturedImageFilePath);
 				} catch (Exception e) {
 					
 				}
 				
 				break;

 		}

 	}

}

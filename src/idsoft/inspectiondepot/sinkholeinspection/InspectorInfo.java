package idsoft.inspectiondepot.sinkholeinspection;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class InspectorInfo extends Activity{
	CommonFunctions cf;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
         	}
			setContentView(R.layout.inspectorinfo);
	        cf.getDeviceDimensions();
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,cf));
			LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.generalsubmenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 13, 1,cf));
			ScrollView scr = (ScrollView)findViewById(R.id.scr);
		    scr.setMinimumHeight(cf.ht);
			TextView geninfotxt = (TextView) findViewById(R.id.topgentext);
			geninfotxt.setText(Html.fromHtml(cf.toptextvalue));
			
			TextView txt_inspectorname = (TextView) findViewById(R.id.txtinspectorname);
			
			TextView txt_inspectioncompanyname = (TextView) findViewById(R.id.txtinspectioncompanyname);
			cf.getInspectorId();
			try {
				Cursor c2 = cf.sh_db.rawQuery("SELECT * FROM "
						+ cf.inspectorlogin + " WHERE Fld_InspectorId='" + cf.Insp_id + "'", null);
				int rws = c2.getCount();
				c2.moveToFirst();
				if (c2 != null) {
					do {
						txt_inspectorname.setText(cf.decode(c2.getString(c2.getColumnIndex("Fld_InspectorFirstName"))+" "+c2.getString(c2.getColumnIndex("Fld_InspectorLastName"))));
						txt_inspectioncompanyname.setText(cf.decode(c2.getString(c2.getColumnIndex("Fld_InspectorCompanyName"))));
			    } while (c2.moveToNext());
			   }
	         } catch (Exception e) {
				cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ InspectorInfo.this +" "+" in the processing stage of retrieving data from Inspector table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
	         }			
	 }
	 public void clicker(View v)
	 {
		  switch(v.getId())
		  {
		  case R.id.home:
			  cf.gohome();
			  break;
		  }
	 }
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				Intent  myintent = new Intent(InspectorInfo.this,PolicyHolder.class);
				myintent.putExtra("InspectionType", cf.onlinspectionid);
				myintent.putExtra("status", cf.onlstatus);
				myintent.putExtra("homeid", cf.selectedhomeid);
				startActivity(myintent);
				return true;
			}
			if (keyCode == KeyEvent.KEYCODE_MENU) {

			}
			return super.onKeyDown(keyCode, event);
		}
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
				switch (resultCode) {
	 			case 0:
	 				break;
	 			case -1:
	 				try {

	 					String[] projection = { MediaStore.Images.Media.DATA };
	 					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
	 							null, null, null);
	 					int column_index_data = cursor
	 							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	 					cursor.moveToFirst();
	 					String capturedImageFilePath = cursor.getString(column_index_data);
	 					cf.showselectedimage(capturedImageFilePath);
	 				} catch (Exception e) {
	 				
	 				}
	 				
	 				break;

	 		}

	 	}

}

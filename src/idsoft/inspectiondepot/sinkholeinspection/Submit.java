package idsoft.inspectiondepot.sinkholeinspection;

import java.net.InetAddress;


import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Random;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class Submit extends Activity {
	private static final int visibility = 0;
	private static final String TAG = null;
	RadioButton accept, decline;
	EditText etsetword, etgetword;
	Resources res;
	private String[] myString;
	String word,strhomeid,data="";
	TextView tv1, tv2;
	
	CommonFunctions cf;
	private static final Random rgenerator = new Random();

	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.selectedhomeid = strhomeid= extras.getString("homeid");
			cf.onlinspectionid = extras.getString("InspectionType");
		    cf.onlstatus = extras.getString("status");
	 	}
		cf.getInspectorId();
		setContentView(R.layout.submit);
		/** menu **/
		cf.getDeviceDimensions();
		LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
		mainmenu_layout.setMinimumWidth(cf.wd);
	    mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(),9, 0,cf));
			
		res = getResources();
		myString = res.getStringArray(R.array.myArray);
		accept = (RadioButton) this.findViewById(R.id.accept);
		decline = (RadioButton) this.findViewById(R.id.decline);
		tv1 = (TextView) findViewById(R.id.textView2);
		tv2 = (TextView) findViewById(R.id.textView1);

		etsetword = (EditText) findViewById(R.id.wrdedt);
		etgetword = (EditText) findViewById(R.id.wrdedt1);
		word = myString[rgenerator.nextInt(myString.length)];
		
		etsetword.setText(word);
		if (word.contains(" ")) {
			word = word.replace(" ", "");
		}
		try {

		    Cursor cm = cf.SelectTablefunction(cf.inspectorlogin,
					" where Fld_InspectorId='" + cf.encode(cf.Insp_id) + "'");
			int rws1 = cm.getCount();
			cm.moveToFirst();
			data = cf.decode(
					cm.getString(cm.getColumnIndex("Fld_InspectorFirstName")))
					.toLowerCase();
			data += " ";
			data += cf.decode(
					cm.getString(cm.getColumnIndex("Fld_InspectorLastName")))
					.toLowerCase();

		} catch (Exception e) {
			
		}
		tv1.setText(Html
				.fromHtml("<font color=black>I, "
						+ "</font>"
						+ "<b><font color=#bddb00>  "
						+ data 
						+ "</font></b><font color=black> (Field Inspector) have conducted this inspection in accordance with the required standards and processes and confirm that I am properly licensed to conduct and sign off on the same inspection. I have made every attempt to collect the data required, have spoken to the Policyholder and/or Agent of record and have conducted the necessary research requirements to ensure permitting, replacement cost valuations or other information are properly completed as part of this inspection process."
						+ "</font>"));
		tv2.setText(Html
				.fromHtml("<font color=black>I, "
						+ "</font>"
						+ "<b><font color=#bddb00> "+data
						+ "</font></b><font color=black> (Field Inspector) also understand that failure to properly conduct this inspection and submit the verifiable inspection data is grounds for termination and considered possible insurance fraud."
						+ "</font>"));
		accept.setOnClickListener(onClickAnswer1);
		decline.setOnClickListener(onClickAnswer2);

		
	}

	public OnClickListener onClickAnswer1 = new OnClickListener() {

		public void onClick(View arg0) {
			accept.setSelected(true);
			decline.setChecked(false);
		}

	};
	public OnClickListener onClickAnswer2 = new OnClickListener() {

		public void onClick(View arg0) {
			
			accept.setSelected(false);
			decline.setChecked(true);
		}

	};

	public void clicker(View v) {
		switch (v.getId()) {

		case R.id.submit:
			if (accept.isChecked() == true) {
				if (etgetword.getText().toString().equals("")) {
                    cf.ShowToast("Please enter Word Verification.",0);
					etgetword.requestFocus();
				} else {
				if (
						etgetword.getText().toString()
							.equals(word.trim().toString()))
					{
						cf.sh_db.execSQL("UPDATE " + cf.policyholder
								+ " SET SH_PH_IsInspected=1,SH_PH_Status=1 WHERE SH_PH_SRID ='"
								+ cf.encode(cf.selectedhomeid) + "' and SH_PH_InspectorId = '"
								+ cf.encode(cf.Insp_id) + "'");
						cf.ShowToast("Saved sucessfully.", 1);
						Intent inte = new Intent(getApplicationContext(),ExportInspection.class);
						 inte.putExtra("type", "export");
						  startActivity(inte);
						
					}
					else{
					cf.ShowToast("Please enter valid Word Verification(case sensitive).", 1);
				    etgetword.setText("");
					etgetword.requestFocus();
					}
				}
			}      
			else
			{
				cf.ShowToast("Please select Accept Radio Button.", 1);
			}
			break;
		case R.id.home:
			  cf.gohome();
			  break;
		case R.id.S_refresh:
			
			word = myString[rgenerator.nextInt(myString.length)];
			
			etsetword.setText(word);
			if (word.contains(" ")) {
				word = word.replace(" ", "");
			}
			/*Intent inte = new Intent(getApplicationContext(),Submit.class);
			 inte.putExtra("homeid", cf.selectedhomeid);
			 inte.putExtra("InspectionType", cf.onlinspectionid);
			 inte.putExtra("status", cf.onlstatus);
			startActivity(inte);*/
			break;
		}
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(cf.strschdate.equals("")){
				cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
			}else{ cf.netalert("FGS");}
			return true;
		}
		return true;
	}
	
	
}

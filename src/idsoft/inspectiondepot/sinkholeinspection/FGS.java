package idsoft.inspectiondepot.sinkholeinspection;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
public class FGS extends FragmentActivity implements Runnable {

	static GeoPoint point;
	final static int MAX_RESULT = 10;
	private static final String TAG = null;
	CommonFunctions cf;
    GoogleMap googleMap;
    String strhomeid,hmeownrname,hmeownraddress,hmeownrcity,hmeownrzip,hmeownrphnnum,source;
	ProgressDialog pd;
	GeoPoint geoPoint=null,geoPoint1=null;
	int Cnt;
	Double hmeownrlat=null,hmeownrlong=null;
	Double lat=null,longt=null;
	
	Drawable drawable,drawable1;
	SoapObject fgsinfo;
	int show_handler=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        cf=new CommonFunctions(this);
        Bundle bunQ1extras1 = getIntent().getExtras();

		if (bunQ1extras1 != null) {
			cf.selectedhomeid =  bunQ1extras1.getString("homeid");
			cf.onlinspectionid = bunQ1extras1.getString("InspectionType");


		}


        cf.getInspectorId();
		
		cf.getDeviceDimensions();
        // Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        
        if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        }else { // Google Play Services are available

            // Getting reference to the SupportMapFragment of activity_main.xml
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapview);

            // Getting GoogleMap object from the fragment
            googleMap = fm.getMap();

            // Enabling MyLocation Layer of Google Map
            googleMap.setMyLocationEnabled(true);
            googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
    		mainmenu_layout.setMinimumWidth(cf.wd);
    	    mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 8, 0,cf));

    		
    		
    		ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    		NetworkInfo info = conMgr.getActiveNetworkInfo();
    		if (info != null && info.isConnected()) {
    			String source = "<b><font color=#00FF33>Loading map. Please wait..."
        				+ "</font></b>";
        		pd = ProgressDialog.show(FGS.this, "", Html.fromHtml(source), true);
    			Thread thread = new Thread(FGS.this);
        		thread.start();
    		}

    		
            // Getting LocationManager object from System Service LOCATION_SERVICE
            
        }
    }
   
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			
			SoapObject request = new SoapObject(cf.NAMESPACE,"GetSinkholeLatituteAndLogitute");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		
		request.addProperty("SRID",cf.selectedhomeid);
		request.addProperty("InspectorID",cf.Insp_id);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		androidHttpTransport.call(cf.NAMESPACE+"GetSinkholeLatituteAndLogitute",envelope);
		
		 fgsinfo = (SoapObject) envelope.getResponse();
		
		Cnt = fgsinfo.getPropertyCount();
		if(Cnt!=0){
		SoapObject obj = (SoapObject) fgsinfo.getProperty(0);
        hmeownrlat = Double.parseDouble((String.valueOf(obj.getProperty("HomeOwnerLatitude")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("HomeOwnerLatitude")).equals("Not Available"))?"":(String.valueOf(obj.getProperty("HomeOwnerLatitude")).equals("null"))?"":String.valueOf(obj.getProperty("HomeOwnerLatitude")));
        hmeownrlong = Double.parseDouble((String.valueOf(obj.getProperty("HomeOwnerLogitude")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("HomeOwnerLogitude")).equals("Not Available"))?"":(String.valueOf(obj.getProperty("HomeOwnerLogitude")).equals("null"))?"":String.valueOf(obj.getProperty("HomeOwnerLogitude")));
        hmeownrname = (String.valueOf(obj.getProperty("OwnerName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OwnerName")).equals("Not Available"))?"":(String.valueOf(obj.getProperty("OwnerName")).equals("null"))?"":String.valueOf(obj.getProperty("OwnerName"));
        hmeownraddress = (String.valueOf(obj.getProperty("OwnerAddress")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OwnerAddress")).equals("Not Available"))?"":(String.valueOf(obj.getProperty("OwnerAddress")).equals("null"))?"":String.valueOf(obj.getProperty("OwnerAddress"));
        hmeownrcity= (String.valueOf(obj.getProperty("OwnerCity")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OwnerCity")).equals("Not Available"))?"":(String.valueOf(obj.getProperty("OwnerCity")).equals("null"))?"":String.valueOf(obj.getProperty("OwnerCity"));
        hmeownrzip = (String.valueOf(obj.getProperty("OwnerZipCode")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OwnerZipCode")).equals("Not Available"))?"":(String.valueOf(obj.getProperty("OwnerZipCode")).equals("null"))?"":String.valueOf(obj.getProperty("OwnerZipCode"));
        hmeownrphnnum = (String.valueOf(obj.getProperty("OwnerPhoneNumber")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OwnerPhoneNumber")).equals("Not Available"))?"":(String.valueOf(obj.getProperty("OwnerPhoneNumber")).equals("null"))?"":String.valueOf(obj.getProperty("OwnerPhoneNumber"));
        show_handler=1;
		}
		else
		{
			 show_handler=3;
		}
		 
		}
		catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ FGS.this+" "+" in the stage of displaying map locations at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			  
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ FGS.this+" "+" in the stage of displaying map locations at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			 
			
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ FGS.this+" "+" in the stage of displaying map locations at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			 
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ FGS.this+" "+" in the stage of displaying map locations at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			 
		} handler.sendEmptyMessage(0);
	}
	private void view(SoapObject fgsinfo) {
		// TODO Auto-generated method stub
		
	}
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			if(show_handler==1){
			pd.dismiss();	
			
			for (int i = 0; i < Cnt; i++) {
				  SoapObject obj1 = (SoapObject) fgsinfo.getProperty(i);
				 lat = Double.parseDouble((String.valueOf(obj1.getProperty("Latitude")).equals("anyType{}"))?"":(String.valueOf(obj1.getProperty("Latitude")).equals("Not Available"))?"":(String.valueOf(obj1.getProperty("Latitude")).equals("null"))?"":String.valueOf(obj1.getProperty("Latitude")));
				 longt = Double.parseDouble((String.valueOf(obj1.getProperty("Logitude")).equals("anyType{}"))?"":(String.valueOf(obj1.getProperty("Logitude")).equals("Not Available"))?"":(String.valueOf(obj1.getProperty("Logitude")).equals("null"))?"":String.valueOf(obj1.getProperty("Logitude")));
				OverlayItem overlayitem1 = new OverlayItem(geoPoint1,"", lat+"\n"+longt);
				googleMap.addMarker(new MarkerOptions().position(new LatLng(lat,longt)).title("Sinkhole").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker1icon)));
								 }
			
			if(hmeownraddress.equals(""))
			{
				hmeownraddress="N/A";
			}
			if(hmeownrcity.equals(""))
			{
				hmeownrcity="N/A";
			}
			if(hmeownrzip.equals("0")||hmeownrzip.equals(""))
			{
				hmeownrzip="N/A";
			}
			 
			OverlayItem overlayitem = new OverlayItem(geoPoint, "HomeOwner", hmeownrname+"\n"+hmeownraddress+","+hmeownrcity+","+hmeownrzip);
			 
			googleMap.addMarker(new MarkerOptions().position(new LatLng(hmeownrlat,hmeownrlong)).title("Home Owner").icon(BitmapDescriptorFactory.fromResource(R.drawable.iconmarker)).snippet(hmeownrname+"\n"+hmeownraddress+","+hmeownrcity+","+hmeownrzip));
			googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(hmeownrlat,hmeownrlong)));
			}
			else if(show_handler==2)
			{
				show_handler=0;pd.dismiss();
				cf.ShowToast("There is problem on your network. Please try again later.", 0);
				
			}
			else if(show_handler==3)
			{
				show_handler=0;pd.dismiss();
				cf.ShowToast("There are no FGS Locations available.", 0);
				
			}
			
		}
	};

	/*private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			pd.dismiss();
			if (k == 1) {
				System.out.println("no more ieuse "+point);
				if(googleMap!=null)
				{   
					System.out.println("comes correc2");
					googleMap.addMarker(new MarkerOptions().position(new LatLng(lat,lon)).title("Home Owner").icon(BitmapDescriptorFactory.fromResource(R.drawable.iconmarker)).snippet(straddr));
					googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(lat,lon)));
				}

			} else if (k == 2) {
				cf.ShowToast("Sorry, There is no Network availability.",1);

			}

		}
	};*/
	

	
public void clicker(View v)
{
	if(v.getId()==R.id.home)
	{
		cf.gohome();
	}
}
}


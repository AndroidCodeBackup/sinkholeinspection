package idsoft.inspectiondepot.sinkholeinspection;

import idsoft.inspectiondepot.sinkholeinspection.Observation2.SH_textwatcher;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.widget.TableRow;
import android.widget.TableLayout;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class Observation1 extends Activity   {
	LinearLayout SH_OBS1P,SH_OBS1AP,SH_OBS2P,SH_OBS3P,SH_OBS4P,SH_OBS5P,SH_OBS6P,SH_OBS6AP,
	SH_OBS6BP,SH_OBS6CP,SH_OBS7P,SH_OBS7AP,SH_OB1,SH_OB1A,SH_OB2,SH_OB3,SH_OB4,SH_OB5,SH_OB6,
	SH_OB6A,SH_OB6B,SH_OB6C,SH_OB7,SH_OB7A;
	TextView SH_OB1T,SH_OB1AT,SH_OB2T,SH_OB3T,SH_OB4T,SH_OB5T,SH_OB6T,SH_OB6AT,SH_OB6BT,SH_OB6CT,
	SH_OB7T,SH_OB7AT;
	CommonFunctions cf;
	GestureDetector gestureDetector;
	int x,y;
	HorizontalScrollView hsv;
	String strhomeid,rd_IrregularLandSurface="",rd_VisibleBuriedDebris="",rd_IrregularLandSurfaceAdjacent="",
			rd_SoilCollapse="",rd_SoilErosionAroundFoundation="",rd_DrivewaysCracksNoted="",rd_UpliftDrivewaysSurface="",
		    rd_LargeTree="",rd_CypressTree="",rd_LargeTreesRemoved="",rd_FoundationCrackNoted="",rd_RetainingWallsServiceable="",
		    chk_Value1="",chk_Value1A="",chk_Value2="",chk_Value3="",chk_Value4="",chk_Value5="",chk_Value6="",chk_Value6A="",
		    chk_Value6B="",chk_Value6C="",chk_Value71="",chk_Value72="",chk_Value73="",chk_Value74="",chk_Value75="",chk_Value7A="";
	 @Override
	    public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
	    	if (extras != null) {
	    		cf.selectedhomeid = strhomeid= extras.getString("homeid");
	    		//cf.selectedhomeid ="RI138825";
	    		cf.onlinspectionid = extras.getString("InspectionType");
	    	    cf.onlstatus = extras.getString("status");
	     	}
	    	setContentView(R.layout.observation1);
	        cf.Create_Table(8);
	        cf.Create_Table(9);
	        cf.getDeviceDimensions();
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	         mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,cf));
			LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.observationsubmenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 41, 1,cf));
			ScrollView scr = (ScrollView)findViewById(R.id.obserscr);
			scr.smoothScrollBy(x,y);scr.setSmoothScrollingEnabled(true);scr.smoothScrollTo(x,y);
			HorizontalScrollView hscr = (HorizontalScrollView) findViewById(R.id.HorizontalScrollView01);
			hscr.setMinimumWidth(cf.wd);
			
			/** DECLARATION AND CLCIK EVENT OF OBSERVATION1 ROW **/
			 cf.tbl_row_obs1 = (TableRow)findViewById(R.id.firstobsrow);
			 cf.tbl_row_obs1.setOnClickListener(new Obs_clicker());
			    
			 cf.obs1_tbl_row_txt = (TextView)findViewById(R.id.txtobservation1);
			
			/** DECLARATION OF OBSERVATION1 TABLE **/
			cf.tbl_obs1 = (TableLayout)findViewById(R.id.obs1_table);
				
			/** declaration and click events for option1 for radiobutton yes **/
		    cf.obs1optn1y = (RadioButton) findViewById(R.id.obs1_opt1rdy);
			cf.obs1optn1y.setOnClickListener(new Obs_clicker());
			//cf.obs1optn1y.requestFocus();
			
			/** declaration and click events for option1 for radiobutton no **/
			cf.obs1optn1n = (RadioButton)findViewById(R.id.obs1_opt1rdn);
			cf.obs1optn1n.setOnClickListener(new Obs_clicker());
			
			/** declaration and click events for option1 for radiobutton notdetermined **/
			cf.obs1optn1nd = (RadioButton)findViewById(R.id.obs1_opt1rdnd);
			cf.obs1optn1nd.setOnClickListener(new Obs_clicker());
			
			/** declaration of locations for radiobutton yes for option1**/
			cf.show_obs1_optn1y = (RelativeLayout)findViewById(R.id.show_obs1_optn1y);
			
			/** declaration and click event for option1 location checkboxes**/
			cf.cb_obs1[0]=(CheckBox) findViewById(R.id.front1);
	        cf.cb_obs1[1]=(CheckBox) findViewById(R.id.rear1);
	        cf.cb_obs1[2]=(CheckBox) findViewById(R.id.right1);
	        cf.cb_obs1[3]=(CheckBox) findViewById(R.id.left1);
	        cf.cb_obs1[4]=(CheckBox) findViewById(R.id.other1);
	        cf.cb_obs1[4].setOnClickListener(new Obs_clicker());
	        /** DECLARATION FOR OPTION1 OTHER EDIT TEXTBOX **/
	        cf.etother_obs1_optn1=(EditText)findViewById(R.id.etother1);
	        
	        /** DECLARATION FOR OPTION1 COOMENTS **/
	        cf.etcomments_obs1_opt1=(EditText)findViewById(R.id.opt1etcomments);
	      

	        /** declaration and click events for optionA for radiobutton yes **/
		    cf.obs1optnay = (RadioButton) findViewById(R.id.obs1_optardy);
			cf.obs1optnay.setOnClickListener(new Obs_clicker());
			
			/** declaration and click events for optionA for radiobutton no **/
			cf.obs1optnan = (RadioButton)findViewById(R.id.obs1_optardn);
			cf.obs1optnan.setOnClickListener(new Obs_clicker());
			
			/** declaration and click events for optionA for radiobutton notdetermined **/
			cf.obs1optnand = (RadioButton)findViewById(R.id.obs1_optardnd);
			cf.obs1optnand.setOnClickListener(new Obs_clicker());
			
			/** declaration of locations for radiobutton yes for optionA**/
			cf.show_obs1_optnay = (RelativeLayout)findViewById(R.id.show_obs1_optnay);
			
			/** declaration and click event for optionA location checkboxes**/
			cf.cb_obs1a[0]=(CheckBox) findViewById(R.id.fronta);
	        cf.cb_obs1a[1]=(CheckBox) findViewById(R.id.reara);
	        cf.cb_obs1a[2]=(CheckBox) findViewById(R.id.righta);
	        cf.cb_obs1a[3]=(CheckBox) findViewById(R.id.lefta);
	        cf.cb_obs1a[4]=(CheckBox) findViewById(R.id.othera);
	        cf.cb_obs1a[4].setOnClickListener(new Obs_clicker());
	        /** DECLARATION FOR OPTIONA OTHER EDIT TEXTBOX **/
	        cf.etother_obs1_optna=(EditText)findViewById(R.id.etothera);
	        
	        /** DECLARATION FOR OPTIONA COOMENTS **/
	        cf.etcomments_obs1_opta=(EditText)findViewById(R.id.optaetcomments);
	        
	       	/** DECLARATION AND CLCIK EVENT OF OBSERVATION2 ROW **/
			 cf.tbl_row_obs2 = (TableRow)findViewById(R.id.secondobs);
			 cf.tbl_row_obs2.setOnClickListener(new Obs_clicker());
			 
			 cf.obs2_tbl_row_txt = (TextView)findViewById(R.id.txtobservation2);
			
			 /** DECLARATION OF OBSERVATION2 TABLE **/
			 cf.tbl_obs2 = (TableLayout)findViewById(R.id.obs2_table);
			
			 /** declaration and click events for observation2 for radiobutton yes **/
			    cf.obs2optny = (RadioButton) findViewById(R.id.obs2_optrdy);
				cf.obs2optny.setOnClickListener(new Obs_clicker());
			
				/** declaration and click events for observation2 for radiobutton no **/
				cf.obs2optnn = (RadioButton)findViewById(R.id.obs2_optrdn);
				cf.obs2optnn.setOnClickListener(new Obs_clicker());
				
				/** declaration and click events for observation2 for radiobutton notdetermined **/
				cf.obs2optnnd = (RadioButton)findViewById(R.id.obs2_optrdnd);
				cf.obs2optnnd.setOnClickListener(new Obs_clicker());
				
				/** declaration of locations for radiobutton yes for observation2**/
				cf.show_obs2_optny = (RelativeLayout)findViewById(R.id.show_obs2_optny);
				
				/** declaration and click event for observation2 location checkboxes**/
				cf.cb_obs2[0]=(CheckBox) findViewById(R.id.obs2_front);
		        cf.cb_obs2[1]=(CheckBox) findViewById(R.id.obs2_rear);
		        cf.cb_obs2[2]=(CheckBox) findViewById(R.id.obs2_right);
		        cf.cb_obs2[3]=(CheckBox) findViewById(R.id.obs2_left);
		        cf.cb_obs2[4]=(CheckBox) findViewById(R.id.obs2_other);
		        cf.cb_obs2[4].setOnClickListener(new Obs_clicker());
		        /** DECLARATION FOR OBSERVATION2 OTHER EDIT TEXTBOX **/
		        cf.etother_obs2=(EditText)findViewById(R.id.obs2_etother);
		        
		        /** DECLARATION FOR OBSERVATION2 COOMENTS **/
		        cf.etcomments_obs2=(EditText)findViewById(R.id.obs2_etcomments);
		        
		        /** DECLARATION AND CLCIK EVENT OF OBSERVATION3 ROW **/
				 cf.tbl_row_obs3 = (TableRow)findViewById(R.id.thirdobs);
				 cf.tbl_row_obs3.setOnClickListener(new Obs_clicker());
				
				 cf.obs3_tbl_row_txt = (TextView)findViewById(R.id.txtobservation3);
				

				/** DECLARATION OF OBSERVATION3 TABLE **/
				cf.tbl_obs3 = (TableLayout)findViewById(R.id.obs3_table);
				
				 /** declaration and click events for observation3 for radiobutton yes **/
			    cf.obs3optny = (RadioButton) findViewById(R.id.obs3_optrdy);
				cf.obs3optny.setOnClickListener(new Obs_clicker());
			
				/** declaration and click events for observation3 for radiobutton no **/
				cf.obs3optnn = (RadioButton)findViewById(R.id.obs3_optrdn);
				cf.obs3optnn.setOnClickListener(new Obs_clicker());
				
				/** declaration and click events for observation3 for radiobutton notdetermined **/
				cf.obs3optnnd = (RadioButton)findViewById(R.id.obs3_optrdnd);
				cf.obs3optnnd.setOnClickListener(new Obs_clicker());
				
				/** declaration of locations for radiobutton yes for observation3**/
				cf.show_obs3_optny = (RelativeLayout)findViewById(R.id.show_obs3_optny);
				
				/** declaration and click event for observation3 location checkboxes**/
				cf.cb_obs3[0]=(CheckBox) findViewById(R.id.obs3_front);
		        cf.cb_obs3[1]=(CheckBox) findViewById(R.id.obs3_rear);
		        cf.cb_obs3[2]=(CheckBox) findViewById(R.id.obs3_right);
		        cf.cb_obs3[3]=(CheckBox) findViewById(R.id.obs3_left);
		        cf.cb_obs3[4]=(CheckBox) findViewById(R.id.obs3_other);
		        cf.cb_obs3[4].setOnClickListener(new Obs_clicker());
		        /** DECLARATION FOR OBSERVATION3 OTHER EDIT TEXTBOX **/
		        cf.etother_obs3=(EditText)findViewById(R.id.obs3_etother);
		        
		        /** DECLARATION FOR OBSERVATION3 COOMENTS **/
		        cf.etcomments_obs3=(EditText)findViewById(R.id.obs3_etcomments);
				
				 /** DECLARATION AND CLCIK EVENT OF OBSERVATION4 ROW **/
				 cf.tbl_row_obs4 = (TableRow)findViewById(R.id.fourthobs);
				 cf.tbl_row_obs4.setOnClickListener(new Obs_clicker());
				
				 cf.obs4_tbl_row_txt = (TextView)findViewById(R.id.txtobservation4);
				

				/** DECLARATION OF OBSERVATION4 TABLE **/
				cf.tbl_obs4 = (TableLayout)findViewById(R.id.obs4_table);
				
				 /** declaration and click events for observation4 for radiobutton yes **/
			    cf.obs4optny = (RadioButton) findViewById(R.id.obs4_optrdy);
				cf.obs4optny.setOnClickListener(new Obs_clicker());
			
				/** declaration and click events for observation4 for radiobutton no **/
				cf.obs4optnn = (RadioButton)findViewById(R.id.obs4_optrdn);
				cf.obs4optnn.setOnClickListener(new Obs_clicker());
				
				/** declaration and click events for observation4 for radiobutton notdetermined **/
				cf.obs4optnnd = (RadioButton)findViewById(R.id.obs4_optrdnd);
				cf.obs4optnnd.setOnClickListener(new Obs_clicker());
				
				/** declaration of locations for radiobutton yes for observation4**/
				cf.show_obs4_optny = (RelativeLayout)findViewById(R.id.show_obs4_optny);
				
				/** declaration and click event for observation4 location checkboxes**/
				cf.cb_obs4[0]=(CheckBox) findViewById(R.id.obs4_found);
		        cf.cb_obs4[1]=(CheckBox) findViewById(R.id.obs4_wall);
		        cf.cb_obs4[2]=(CheckBox) findViewById(R.id.obs4_drive);
		        cf.cb_obs4[3]=(CheckBox) findViewById(R.id.obs4_yard);
		        cf.cb_obs4[4]=(CheckBox) findViewById(R.id.obs4_down);
		        cf.cb_obs4[5]=(CheckBox) findViewById(R.id.obs4_adj);
		        cf.cb_obs4[6]=(CheckBox) findViewById(R.id.obs4_brok);
		        cf.cb_obs4[7]=(CheckBox) findViewById(R.id.obs4_other);
		        cf.cb_obs4[7].setOnClickListener(new Obs_clicker());
		       /** DECLARATION FOR observation4 OTHER EDIT TEXTBOX **/
		        cf.etother_obs4=(EditText)findViewById(R.id.obs4_etother);
		        
		        /** DECLARATION FOR observation4 COOMENTS **/
		        cf.etcomments_obs4=(EditText)findViewById(R.id.obs4_etcomments);
				
				 /** DECLARATION AND CLCIK EVENT OF OBSERVATION5 ROW **/
				 cf.tbl_row_obs5 = (TableRow)findViewById(R.id.fifthobs);
				 cf.tbl_row_obs5.setOnClickListener(new Obs_clicker());
				 
				 cf.obs5_tbl_row_txt = (TextView)findViewById(R.id.txtobservation5);
				
				

				/** DECLARATION OF OBSERVATION5 TABLE **/
				cf.tbl_obs5 = (TableLayout)findViewById(R.id.obs5_table);
				
				 /** declaration and click events for observation5 for radiobutton yes **/
			    cf.obs5optny = (RadioButton) findViewById(R.id.obs5_optrdy);
				cf.obs5optny.setOnClickListener(new Obs_clicker());
			
				/** declaration and click events for observation5 for radiobutton no **/
				cf.obs5optnn = (RadioButton)findViewById(R.id.obs5_optrdn);
				cf.obs5optnn.setOnClickListener(new Obs_clicker());
				
				/** declaration and click events for observation5 for radiobutton notdetermined **/
				cf.obs5optnnd = (RadioButton)findViewById(R.id.obs5_optrdnd);
				cf.obs5optnnd.setOnClickListener(new Obs_clicker());
				
				/** declaration of locations for radiobutton yes for observation5**/
				cf.show_obs5_optny = (RelativeLayout)findViewById(R.id.show_obs5_optny);
				
				/** declaration and click event for observation5 location checkboxes**/
				cf.cb_obs5[0]=(CheckBox) findViewById(R.id.obs5_front);
		        cf.cb_obs5[1]=(CheckBox) findViewById(R.id.obs5_rear);
		        cf.cb_obs5[2]=(CheckBox) findViewById(R.id.obs5_right);
		        cf.cb_obs5[3]=(CheckBox) findViewById(R.id.obs5_left);
		        cf.cb_obs5[4]=(CheckBox) findViewById(R.id.obs5_other);
		        cf.cb_obs5[4].setOnClickListener(new Obs_clicker());
		        /** DECLARATION FOR observation5 OTHER EDIT TEXTBOX **/
		        cf.etother_obs5=(EditText)findViewById(R.id.obs5_etother);
		        
		        /** DECLARATION FOR observation5 COOMENTS **/
		        cf.etcomments_obs5=(EditText)findViewById(R.id.obs5_etcomments);
				
				
				 /** DECLARATION AND CLCIK EVENT OF OBSERVATION6 ROW **/
				 cf.tbl_row_obs6 = (TableRow)findViewById(R.id.sixthobs);
				 cf.tbl_row_obs6.setOnClickListener(new Obs_clicker());
				 
				 cf.obs6_tbl_row_txt = (TextView)findViewById(R.id.txtobservation6);
				

				/** DECLARATION OF OBSERVATION6 TABLE **/
				cf.tbl_obs6 = (TableLayout)findViewById(R.id.obs6_table);
				
					        
		        /** declaration and click events for observation6-option1 for radiobutton yes **/
			    cf.obs6opt1ny = (RadioButton) findViewById(R.id.obs6_opt1rdy);
				cf.obs6opt1ny.setOnClickListener(new Obs_clicker());
			
				/** declaration and click events for observation6-option1 for radiobutton no **/
				cf.obs6opt1nn = (RadioButton)findViewById(R.id.obs6_opt1rdn);
				cf.obs6opt1nn.setOnClickListener(new Obs_clicker());
				
				/** declaration and click events for observation6-option1 for radiobutton notdetermined **/
				cf.obs6opt1nnd = (RadioButton)findViewById(R.id.obs6_opt1rdnd);
				cf.obs6opt1nnd.setOnClickListener(new Obs_clicker());
				
				/** declaration of locations for radiobutton yes for observation6-option1**/
				cf.show_obs6_opt1ny = (RelativeLayout)findViewById(R.id.show_obs6_opt1ny);
				
				/** declaration and click event for observation6-option1 location checkboxes**/
				cf.cb_obs6[0]=(CheckBox) findViewById(R.id.obs6_opt1_front);
		        cf.cb_obs6[1]=(CheckBox) findViewById(R.id.obs6_opt1_rear);
		        cf.cb_obs6[2]=(CheckBox) findViewById(R.id.obs6_opt1_right);
		        cf.cb_obs6[3]=(CheckBox) findViewById(R.id.obs6_opt1_left);
		        cf.cb_obs6[4]=(CheckBox) findViewById(R.id.obs6_opt1_other);
		        cf.cb_obs6[4].setOnClickListener(new Obs_clicker());
		        /** DECLARATION FOR observation6-option1 OTHER EDIT TEXTBOX **/
		        cf.etother_obs6_opt1=(EditText)findViewById(R.id.obs6_opt1_etother);
		        
		        /** DECLARATION FOR observation6-option1 COOMENTS **/
		        cf.etcomments_obs6_opt1=(EditText)findViewById(R.id.obs6_opt1_etcomments);
		        
		        /** declaration and click events for observation6-optiona for radiobutton yes **/
			    cf.obs6optany = (RadioButton) findViewById(R.id.obs6_optardy);
				cf.obs6optany.setOnClickListener(new Obs_clicker());
			
				/** declaration and click events for observation6-optiona for radiobutton no **/
				cf.obs6optann = (RadioButton)findViewById(R.id.obs6_optardn);
				cf.obs6optann.setOnClickListener(new Obs_clicker());
				
				/** declaration and click events for observation6-optiona for radiobutton notdetermined **/
				cf.obs6optannd = (RadioButton)findViewById(R.id.obs6_optardnd);
				cf.obs6optannd.setOnClickListener(new Obs_clicker());
				
				/** declaration of locations for radiobutton yes for observation6-optiona**/
				cf.show_obs6_optany = (RelativeLayout)findViewById(R.id.show_obs6_optnay);
				
				/** declaration and click event for observation6-optiona location checkboxes**/
				cf.cb_obs6A[0]=(CheckBox) findViewById(R.id.obs6_fronta);
		        cf.cb_obs6A[1]=(CheckBox) findViewById(R.id.obs6_reara);
		        cf.cb_obs6A[2]=(CheckBox) findViewById(R.id.obs6_righta);
		        cf.cb_obs6A[3]=(CheckBox) findViewById(R.id.obs6_lefta);
		        cf.cb_obs6A[4]=(CheckBox) findViewById(R.id.obs6_othera);
		        cf.cb_obs6A[4].setOnClickListener(new Obs_clicker());
		        /** DECLARATION FOR observation6-optiona OTHER EDIT TEXTBOX **/
		        cf.etother_obs6_opta=(EditText)findViewById(R.id.obs6_etothera);
		        
		        cf.etinch_obs6a=(EditText)findViewById(R.id.obs6_optaetinch);
		        cf.etfeet_obs6a=(EditText)findViewById(R.id.obs6_optaetfeet);
		        cf.onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		        
		        /** DECLARATION FOR observation6-optiona COOMENTS **/
		        cf.etcomments_obs6_opta=(EditText)findViewById(R.id.obs6_optaetcomments);
				
		        /** declaration and click events for observation6-optionb for radiobutton yes **/
			    cf.obs6optbny = (RadioButton) findViewById(R.id.obs6_optbrdy);
				cf.obs6optbny.setOnClickListener(new Obs_clicker());
			
				/** declaration and click events for observation6-optionb for radiobutton no **/
				cf.obs6optbnn = (RadioButton)findViewById(R.id.obs6_optbrdn);
				cf.obs6optbnn.setOnClickListener(new Obs_clicker());
				
				/** declaration and click events for observation6-optionb for radiobutton notdetermined **/
				cf.obs6optbnnd = (RadioButton)findViewById(R.id.obs6_optbrdnd);
				cf.obs6optbnnd.setOnClickListener(new Obs_clicker());
				
				/** declaration of locations for radiobutton yes for observation6-optionb**/
				cf.show_obs6_optbny = (RelativeLayout)findViewById(R.id.show_obs6_optnby);
				
				/** declaration and click event for observation6-optionb location checkboxes**/
				cf.cb_obs6B[0]=(CheckBox) findViewById(R.id.frontb);
		        cf.cb_obs6B[1]=(CheckBox) findViewById(R.id.rearb);
		        cf.cb_obs6B[2]=(CheckBox) findViewById(R.id.rightb);
		        cf.cb_obs6B[3]=(CheckBox) findViewById(R.id.leftb);
		        cf.cb_obs6B[4]=(CheckBox) findViewById(R.id.otherb);
		        cf.cb_obs6B[4].setOnClickListener(new Obs_clicker());
		        /** DECLARATION FOR observation6-optionb OTHER EDIT TEXTBOX **/
		        cf.etother_obs6_optb=(EditText)findViewById(R.id.etotherb);
		        
		        /** DECLARATION FOR observation6-optionb COOMENTS **/
		        cf.etcomments_obs6_optb=(EditText)findViewById(R.id.optbetcomments);
		        
		        /** declaration and click events for observation6-optionc for radiobutton yes **/
			    cf.obs6optcny = (RadioButton) findViewById(R.id.obs6_optcrdy);
				cf.obs6optcny.setOnClickListener(new Obs_clicker());
			
				/** declaration and click events for observation6-optionc for radiobutton no **/
				cf.obs6optcnn = (RadioButton)findViewById(R.id.obs6_optcrdn);
				cf.obs6optcnn.setOnClickListener(new Obs_clicker());
				
				/** declaration and click events for observation6-optionc for radiobutton notdetermined **/
				cf.obs6optcnnd = (RadioButton)findViewById(R.id.obs6_optcrdnd);
				cf.obs6optcnnd.setOnClickListener(new Obs_clicker());
				
				/** declaration of locations for radiobutton yes for observation6-optionc**/
				cf.show_obs6_optcny = (RelativeLayout)findViewById(R.id.show_obs6_optncy);
				
				/** declaration and click event for observation6-optionc location checkboxes**/
				cf.cb_obs6C[0]=(CheckBox) findViewById(R.id.frontc);
		        cf.cb_obs6C[1]=(CheckBox) findViewById(R.id.rearc);
		        cf.cb_obs6C[2]=(CheckBox) findViewById(R.id.rightc);
		        cf.cb_obs6C[3]=(CheckBox) findViewById(R.id.leftc);
		        cf.cb_obs6C[4]=(CheckBox) findViewById(R.id.otherc);
		        cf.cb_obs6C[4].setOnClickListener(new Obs_clicker());
		        /** DECLARATION FOR observation6-optionc OTHER EDIT TEXTBOX **/
		        cf.etother_obs6_optc=(EditText)findViewById(R.id.etotherc);
		        
		        /** DECLARATION FOR observation6-optionc COOMENTS **/
		        cf.etcomments_obs6_optc=(EditText)findViewById(R.id.optcetcomments);
				
				 /** DECLARATION AND CLCIK EVENT OF OBSERVATION7 ROW **/
				 cf.tbl_row_obs7 = (TableRow)findViewById(R.id.seventhobs);
				 cf.tbl_row_obs7.setOnClickListener(new Obs_clicker());
				
				 cf.obs7_tbl_row_txt = (TextView)findViewById(R.id.txtobservation7);
				 
					

				/** DECLARATION OF OBSERVATION7 TABLE **/
				cf.tbl_obs7 = (TableLayout)findViewById(R.id.obs7_table);
				
		        
		        /** declaration and click events for observation7-option1 for radiobutton yes **/
			    cf.obs7opt1ny = (RadioButton) findViewById(R.id.obs7_opt1rdy);
				cf.obs7opt1ny.setOnClickListener(new Obs_clicker());
			
				/** declaration and click events for observation7-option1 for radiobutton no **/
				cf.obs7opt1nn = (RadioButton)findViewById(R.id.obs7_opt1rdn);
				cf.obs7opt1nn.setOnClickListener(new Obs_clicker());
				
				/** declaration and click events for observation7-option1 for radiobutton notdetermined **/
				cf.obs7opt1nnd = (RadioButton)findViewById(R.id.obs7_opt1rdnd);
				cf.obs7opt1nnd.setOnClickListener(new Obs_clicker());
				
				/** declaration of locations for radiobutton yes for observation7-option1**/
				cf.show_obs7_opt1ny = (RelativeLayout)findViewById(R.id.show_obs7_opt1ny);
				
				/** declaration and click event for observation7-option1 type of cracks checkboxes**/
				cf.cb_obs71[0]=(CheckBox) findViewById(R.id.obs7_opt1_vert);
		        cf.cb_obs71[1]=(CheckBox) findViewById(R.id.obs7_opt1_dia);
		        cf.cb_obs71[2]=(CheckBox) findViewById(R.id.obs7_opt1_hor);
		        cf.cb_obs71[3]=(CheckBox) findViewById(R.id.obs7_opt1_ran);
		        cf.cb_obs71[4]=(CheckBox) findViewById(R.id.obs7_opt1_txty1other);
		        cf.cb_obs71[4].setOnClickListener(new Obs_clicker());
		        /** DECLARATION FOR observation7-option1 OTHER EDIT TEXTBOX **/
		        cf.etother_obs7_opt1txty1=(EditText)findViewById(R.id.obs7_opt1_ettxty1other);
		        
		        /** declaration and click event for observation7-option1 location cracks checkboxes**/
				cf.cb_obs72[0]=(CheckBox) findViewById(R.id.obs7_opt1_front);
		        cf.cb_obs72[1]=(CheckBox) findViewById(R.id.obs7_opt1_rear);
		        cf.cb_obs72[2]=(CheckBox) findViewById(R.id.obs7_opt1_right);
		        cf.cb_obs72[3]=(CheckBox) findViewById(R.id.obs7_opt1_left);
		        cf.cb_obs72[4]=(CheckBox) findViewById(R.id.obs7_opt1_txty2other);
		        cf.cb_obs72[4].setOnClickListener(new Obs_clicker());
		        /** DECLARATION FOR observation7-option1 OTHER EDIT TEXTBOX **/
		        cf.etother_obs7_opt1txty2=(EditText)findViewById(R.id.obs7_opt1_ettxty2other);
		        
		        /** declaration and click event for observation7-option1 approximate width of cracks notes checkboxes**/
				cf.cb_obs73[0]=(CheckBox) findViewById(R.id.obs7_opt1_16th);
		        cf.cb_obs73[1]=(CheckBox) findViewById(R.id.obs7_opt1_8th);
		        cf.cb_obs73[2]=(CheckBox) findViewById(R.id.obs7_opt1_4th);
		        cf.cb_obs73[3]=(CheckBox) findViewById(R.id.obs7_opt1_2th);
		        cf.cb_obs73[4]=(CheckBox) findViewById(R.id.obs7_opt1_txty3other);
		        cf.cb_obs73[4].setOnClickListener(new Obs_clicker());
		        /** DECLARATION FOR observation7-option1 OTHER EDIT TEXTBOX **/
		        cf.etother_obs7_opt1txty3=(EditText)findViewById(R.id.obs7_opt1_ettxty3other);
		        
		        cf.etlen_obs7=(EditText)findViewById(R.id.obs7_opt1_etlen);
		        
		        /** declaration and click event for observation7-option1 conditions of cracks notes checkboxes**/
				cf.cb_obs74[0]=(CheckBox) findViewById(R.id.obs7_opt1_clean);
		        cf.cb_obs74[1]=(CheckBox) findViewById(R.id.obs7_opt1_colob);
		        cf.cb_obs74[2]=(CheckBox) findViewById(R.id.obs7_opt1_old);
		        cf.cb_obs74[3]=(CheckBox) findViewById(R.id.obs7_opt1_ovr);
		        cf.cb_obs74[4]=(CheckBox) findViewById(R.id.obs7_opt1_caur);
		        cf.cb_obs74[5]=(CheckBox) findViewById(R.id.obs7_opt1_repair);
		        		        
		        /** declaration and click event for observation7-option1 probable cause checkboxes**/
				cf.cb_obs75[0]=(CheckBox) findViewById(R.id.obs7_opt1_initial);
		        cf.cb_obs75[1]=(CheckBox) findViewById(R.id.obs7_opt1_shrink);
		        cf.cb_obs75[2]=(CheckBox) findViewById(R.id.obs7_opt1_sub);
		        cf.cb_obs75[3]=(CheckBox) findViewById(R.id.obs7_opt1_drain);
		        cf.cb_obs75[4]=(CheckBox) findViewById(R.id.obs7_opt1_nd);
		        cf.cb_obs75[5]=(CheckBox) findViewById(R.id.obs7_opt1_sinkhole);
		        cf.cb_obs75[6]=(CheckBox) findViewById(R.id.obs7_opt1_txty4other);
		        cf.cb_obs75[6].setOnClickListener(new Obs_clicker());
		        
		        cf.etother_obs7_txty4other=(EditText)findViewById(R.id.obs7_opt1_ettxty4other);
		        
		        
		        /** DECLARATION FOR observation7-option1 COOMENTS **/
		        cf.etcomments_obs7_opt1=(EditText)findViewById(R.id.obs7_opt1_etcomments);
		        
		        /** declaration and click events for obervation7-optionA for radiobutton yes **/
			    cf.obs7optnay = (RadioButton) findViewById(R.id.obs7_optardy);
				cf.obs7optnay.setOnClickListener(new Obs_clicker());
				
				/** declaration and click events for obervation7-optionA for radiobutton no **/
				cf.obs7optnan = (RadioButton)findViewById(R.id.obs7_optardn);
				cf.obs7optnan.setOnClickListener(new Obs_clicker());
				
				/** declaration and click events for obervation7-optionA for radiobutton notdetermined **/
				cf.obs7optnand = (RadioButton)findViewById(R.id.obs7_optardnd);
				cf.obs7optnand.setOnClickListener(new Obs_clicker());
				
				/** declaration of locations for radiobutton yes for obervation7-optionA**/
				cf.show_obs7_optnay = (RelativeLayout)findViewById(R.id.show_obs7_optnay);
				
				/** declaration and click event for obervation7-optionA location checkboxes**/
				cf.cb_obs7A[0]=(CheckBox) findViewById(R.id.obs7_lean);
		        cf.cb_obs7A[1]=(CheckBox) findViewById(R.id.obs7_erod);
		        cf.cb_obs7A[2]=(CheckBox) findViewById(R.id.obs7_wash);
		        cf.cb_obs7A[3]=(CheckBox) findViewById(R.id.obs7_def);
		           
		        /** DECLARATION FOR observation7-optiona COOMENTS **/
		        cf.etcomments_obs7_opta=(EditText)findViewById(R.id.obs7_optaetcomments);
		        
		        /*DECLARATION OF EDITTEXT LIMIT EXCEEDS */
				  SH_OBS1P = (LinearLayout) findViewById(R.id.SH_OBS1_ED_parrent);
				  SH_OB1 = (LinearLayout) findViewById(R.id.SH_OB1);
				  SH_OB1T = (TextView) findViewById(R.id.SH_OB1_TV);
				  
				  SH_OBS1AP = (LinearLayout) findViewById(R.id.SH_OBS1A_ED_parrent);
				  SH_OB1A = (LinearLayout) findViewById(R.id.SH_OB1A);
				  SH_OB1AT = (TextView) findViewById(R.id.SH_OB1A_TV);
				  
				  SH_OBS2P = (LinearLayout) findViewById(R.id.SH_OBS2_ED_parrent);
				  SH_OB2 = (LinearLayout) findViewById(R.id.SH_OB2);
				  SH_OB2T = (TextView) findViewById(R.id.SH_OB2_TV);
				  
				  SH_OBS3P = (LinearLayout) findViewById(R.id.SH_OBS3_ED_parrent);
				  SH_OB3 = (LinearLayout) findViewById(R.id.SH_OB3);
				  SH_OB3T = (TextView) findViewById(R.id.SH_OB3_TV);
				  
				  SH_OBS4P = (LinearLayout) findViewById(R.id.SH_OBS4_ED_parrent);
				  SH_OB4 = (LinearLayout) findViewById(R.id.SH_OB4);
				  SH_OB4T = (TextView) findViewById(R.id.SH_OB4_TV);
				  
				  SH_OBS5P = (LinearLayout) findViewById(R.id.SH_OBS5_ED_parrent);
				  SH_OB5 = (LinearLayout) findViewById(R.id.SH_OB5);
				  SH_OB5T = (TextView) findViewById(R.id.SH_OB5_TV);
				  
				  SH_OBS6P = (LinearLayout) findViewById(R.id.SH_OBS6_ED_parrent);
				  SH_OB6 = (LinearLayout) findViewById(R.id.SH_OB6);
				  SH_OB6T = (TextView) findViewById(R.id.SH_OB6_TV);
				  
				  SH_OBS6AP = (LinearLayout) findViewById(R.id.SH_OBS6A_ED_parrent);
				  SH_OB6A = (LinearLayout) findViewById(R.id.SH_OB6A);
				  SH_OB6AT = (TextView) findViewById(R.id.SH_OB6A_TV);
				  
				  SH_OBS6BP = (LinearLayout) findViewById(R.id.SH_OBS6B_ED_parrent);
				  SH_OB6B = (LinearLayout) findViewById(R.id.SH_OB6B);
				  SH_OB6BT = (TextView) findViewById(R.id.SH_OB6B_TV);
				  
				  SH_OBS6CP = (LinearLayout) findViewById(R.id.SH_OBS6C_ED_parrent);
				  SH_OB6C = (LinearLayout) findViewById(R.id.SH_OB6C);
				  SH_OB6CT = (TextView) findViewById(R.id.SH_OB6C_TV);
				  
				  SH_OBS7P = (LinearLayout) findViewById(R.id.SH_OBS7_ED_parrent);
				  SH_OB7 = (LinearLayout) findViewById(R.id.SH_OB7);
				  SH_OB7T = (TextView) findViewById(R.id.SH_OB7_TV);
				  
				  SH_OBS7AP = (LinearLayout) findViewById(R.id.SH_OBS7A_ED_parrent);
				  SH_OB7A = (LinearLayout) findViewById(R.id.SH_OB7A);
				  SH_OB7AT = (TextView) findViewById(R.id.SH_OB7A_TV);
				 
				  cf.etcomments_obs1_opt1.setOnTouchListener(new Touch_Listener(1));
				  cf.etcomments_obs1_opta.setOnTouchListener(new Touch_Listener(2));
				  cf.etcomments_obs2.setOnTouchListener(new Touch_Listener(3));
				  cf.etcomments_obs3.setOnTouchListener(new Touch_Listener(4));
				  cf.etcomments_obs4.setOnTouchListener(new Touch_Listener(5));
				  cf.etcomments_obs5.setOnTouchListener(new Touch_Listener(6));
				  cf.etcomments_obs6_opt1.setOnTouchListener(new Touch_Listener(7));
				  cf.etcomments_obs6_opta.setOnTouchListener(new Touch_Listener(8));
				  cf.etcomments_obs6_optb.setOnTouchListener(new Touch_Listener(9));
				  cf.etcomments_obs6_optc.setOnTouchListener(new Touch_Listener(10));
				  cf.etcomments_obs7_opt1.setOnTouchListener(new Touch_Listener(11));
				  cf.etcomments_obs7_opta.setOnTouchListener(new Touch_Listener(12));
				  cf.etinch_obs6a.setOnTouchListener(new Touch_Listener(13));
				  cf.etfeet_obs6a.setOnTouchListener(new Touch_Listener(14));
				  
				  cf.etcomments_obs1_opt1.addTextChangedListener(new SH_textwatcher(1));
				  cf.etcomments_obs1_opta.addTextChangedListener(new SH_textwatcher(2));
				  cf.etcomments_obs2.addTextChangedListener(new SH_textwatcher(3));
				  cf.etcomments_obs3.addTextChangedListener(new SH_textwatcher(4));
				  cf.etcomments_obs4.addTextChangedListener(new SH_textwatcher(5));
				  cf.etcomments_obs5.addTextChangedListener(new SH_textwatcher(6));
				  cf.etcomments_obs6_opt1.addTextChangedListener(new SH_textwatcher(7));
				  cf.etcomments_obs6_opta.addTextChangedListener(new SH_textwatcher(8));
				  cf.etcomments_obs6_optb.addTextChangedListener(new SH_textwatcher(9));
				  cf.etcomments_obs6_optc.addTextChangedListener(new SH_textwatcher(10));
				  cf.etcomments_obs7_opt1.addTextChangedListener(new SH_textwatcher(11));
				  cf.etcomments_obs7_opta.addTextChangedListener(new SH_textwatcher(12));
				  
				  
				 /*  DECLARATION OF TAKE PHOTO 
				  tkpht_obs1 = (TextView)findViewById(R.id.tkpht_obs1_opt1);
				  tkpht_obs1.setText(cf.phototextdisplay());*/
				  /* DISPLAY THE VALUES FROM DATABASE*/
				  OBS1_SetValues();
	 }
	 @Override
	 public boolean onTouchEvent(MotionEvent event) {
	      x = (int)event.getX();
	      y = (int)event.getY();
	     
	 return false;
	 }


	 private void OBS1_SetValues() {
		// TODO Auto-generated method stub
		 try
			{
			 
				Cursor OBS1_Retrive=cf.SelectTablefunction(cf.Observation1, " where SH_OBS1_SRID ='"+cf.encode(cf.selectedhomeid)+"'");
				if(OBS1_Retrive.getCount()>0)
				{
					OBS1_Retrive.moveToFirst();
					/* SETTING QUESTION NO.1 */
					rd_IrregularLandSurface=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("IrregularLandSurface")));
					setvalueToRadio(cf.obs1optn1y,cf.obs1optn1n,cf.obs1optn1nd,rd_IrregularLandSurface,cf.show_obs1_optn1y); 
					cf.etcomments_obs1_opt1.setText(cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("IrregularLandSurfaceComments"))));
					chk_Value1=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("IrregularLandSurfaceLocation")));
				    cf.setvaluechk(chk_Value1,cf.cb_obs1,cf.etother_obs1_optn1);
				    
				    /* SETTING QUESTION NO.1(A) */
					rd_VisibleBuriedDebris=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("VisibleBuriedDebris")));
					setvalueToRadio(cf.obs1optnay,cf.obs1optnan,cf.obs1optnand,rd_VisibleBuriedDebris,cf.show_obs1_optnay); 
					cf.etcomments_obs1_opta.setText(cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("VisibleBuriedDebrisComments"))));
					chk_Value1A=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("VisibleBuriedDebrisLocation")));
				    cf.setvaluechk(chk_Value1A,cf.cb_obs1a,cf.etother_obs1_optna);
				    
				    /* SETTING QUESTION NO.2 */
					rd_IrregularLandSurfaceAdjacent=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("IrregularLandSurfaceAdjacent")));
					setvalueToRadio(cf.obs2optny,cf.obs2optnn,cf.obs2optnnd,rd_IrregularLandSurfaceAdjacent,cf.show_obs2_optny); 
					cf.etcomments_obs2.setText(cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("IrregularLandSurfaceAdjacentComments"))));
					chk_Value2=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("IrregularLandSurfaceAdjacentLocation")));
				    cf.setvaluechk(chk_Value2,cf.cb_obs2,cf.etother_obs2);
				    
				    /* SETTING QUESTION NO.3 */
					rd_SoilCollapse=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("SoilCollapse")));
					setvalueToRadio(cf.obs3optny,cf.obs3optnn,cf.obs3optnnd,rd_SoilCollapse,cf.show_obs3_optny); 
					cf.etcomments_obs3.setText(cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("SoilCollapseComments"))));
					chk_Value3=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("SoilCollapseLocation")));
				    cf.setvaluechk(chk_Value3,cf.cb_obs3,cf.etother_obs3);
				    
				    /* SETTING QUESTION NO.4 */
					rd_SoilErosionAroundFoundation=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("SoilErosionAroundFoundation")));
					setvalueToRadio(cf.obs4optny,cf.obs4optnn,cf.obs4optnnd,rd_SoilErosionAroundFoundation,cf.show_obs4_optny); 
					cf.etcomments_obs4.setText(cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("SoilErosionAroundFoundationComments"))));
					chk_Value4=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("SoilErosionAroundFoundationOption")));
				    cf.setvaluechk(chk_Value4,cf.cb_obs4,cf.etother_obs4);
				    
				    /* SETTING QUESTION NO.5 */
					rd_DrivewaysCracksNoted=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("DrivewaysCracksNoted")));
					setvalueToRadio(cf.obs5optny,cf.obs5optnn,cf.obs5optnnd,rd_DrivewaysCracksNoted,cf.show_obs5_optny); 
					cf.etcomments_obs5.setText(cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("DrivewaysCracksComments"))));
					chk_Value5=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("DrivewaysCracksOption")));
				    cf.setvaluechk(chk_Value5,cf.cb_obs5,cf.etother_obs5);
				    
				    /* SETTING QUESTION NO.6 */
					rd_UpliftDrivewaysSurface=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("UpliftDrivewaysSurface")));
					setvalueToRadio(cf.obs6opt1ny,cf.obs6opt1nn,cf.obs6opt1nnd,rd_UpliftDrivewaysSurface,cf.show_obs6_opt1ny); 
					cf.etcomments_obs6_opt1.setText(cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("UpliftDrivewaysSurfaceComments"))));
					chk_Value6=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("UpliftDrivewaysSurfaceOption")));
				    cf.setvaluechk(chk_Value6,cf.cb_obs6,cf.etother_obs6_opt1);
				    
				    /* SETTING QUESTION NO.6(A) */
					rd_LargeTree=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("LargeTree")));
					setvalueToRadio(cf.obs6optany,cf.obs6optann,cf.obs6optannd,rd_LargeTree,cf.show_obs6_optany); 
					cf.etcomments_obs6_opta.setText(cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("LargeTreeComments"))));
					dbquery();
					
					/* SETTING QUESTION NO.6(B) */
					rd_CypressTree=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("CypressTree")));
					setvalueToRadio(cf.obs6optbny,cf.obs6optbnn,cf.obs6optbnnd,rd_CypressTree,cf.show_obs6_optbny); 
					cf.etcomments_obs6_optb.setText(cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("CypressTreeComments"))));
					chk_Value6B=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("CypressTreeLocation")));
				    cf.setvaluechk(chk_Value6B,cf.cb_obs6B,cf.etother_obs6_optb);
				    
				    /* SETTING QUESTION NO.6(C) */
					rd_LargeTreesRemoved=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("LargeTreesRemoved")));
					setvalueToRadio(cf.obs6optcny,cf.obs6optcnn,cf.obs6optcnnd,rd_LargeTreesRemoved,cf.show_obs6_optcny); 
					cf.etcomments_obs6_optc.setText(cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("LargeTreesRemovedComments"))));
					chk_Value6C=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("LargeTreesRemovedLocation")));
				    cf.setvaluechk(chk_Value6C,cf.cb_obs6C,cf.etother_obs6_optc);
				    
				    /* SETTING QUESTION NO.7 */
					rd_FoundationCrackNoted=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("FoundationCrackNoted")));
					setvalueToRadio(cf.obs7opt1ny,cf.obs7opt1nn,cf.obs7opt1nnd,rd_FoundationCrackNoted,cf.show_obs7_opt1ny); 
					cf.etcomments_obs7_opt1.setText(cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("FoundationComments"))));
				    chk_Value71=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("FoundationTypeofCrack")));
				    cf.setvaluechk(chk_Value71,cf.cb_obs71,cf.etother_obs7_opt1txty1);
				    chk_Value72=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("FoundationLocation")));
				    cf.setvaluechk(chk_Value72,cf.cb_obs72,cf.etother_obs7_opt1txty2);
				    chk_Value73=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("FoundationWidthofCrack")));
				    cf.setvaluechk(chk_Value73,cf.cb_obs73,cf.etother_obs7_opt1txty3);
				    cf.etlen_obs7.setText(cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("FoundationLengthofCrack"))));
				    chk_Value74=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("FoundationCleanliness")));
				    cf.setvaluechk(chk_Value74,cf.cb_obs74,cf.etother_obs7_opt1txty3);
				    chk_Value75=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("FoundationProbableCause")));
				    cf.setvaluechk(chk_Value75,cf.cb_obs75,cf.etother_obs7_txty4other);
				    
				    /* SETTING QUESTION NO.7(A)*/
					rd_RetainingWallsServiceable=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("RetainingWallsServiceable")));
					setvalueToRadio(cf.obs7optnay,cf.obs7optnan,cf.obs7optnand,rd_RetainingWallsServiceable,cf.show_obs7_optnay); 
					cf.etcomments_obs7_opta.setText(cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("RetainingWallsServiceComments"))));
				    chk_Value7A=cf.decode(OBS1_Retrive.getString(OBS1_Retrive.getColumnIndex("RetainingWallsServiceOption")));
				    cf.setvaluechk(chk_Value7A,cf.cb_obs7A,cf.etother_obs7_opt1txty1);
				   
				    
				}
	    }catch (Exception E)
		{
			String strerrorlog="Selection of the Observation1 table not working ";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
	   }
	}
	 private void dbquery() {
			cf.data = null;
			cf.inspdata = "";
			cf.countarr = null;
			cf.rws = 0;
			cf.sql = "select * from " + cf.Observation1T
					+ " where SH_OBS1T_SRID='" + cf.encode(cf.selectedhomeid)+ "'";
			Cursor cur = cf.sh_db.rawQuery(cf.sql, null);
			cf.rws = cur.getCount();
			cf.data = new String[cf.rws];
			cf.countarr = new String[cf.rws];
			int j = 0;
			cur.moveToFirst();
			if (cur.getCount() >= 1) {

				do {
					
							cf.inspdata += " "+ cf.decode(cur.getString(cur.getColumnIndex("Width")))
									+ " | ";
							cf.inspdata += cf.decode(cur.getString(cur
									.getColumnIndex("Height"))) + " | ";
							cf.inspdata += cf.decode(cur.getString(cur
									.getColumnIndex("Location")))
									+ "~";
							cf.countarr[j] = cur.getString(cur
									.getColumnIndex("OB1TId"));
							j++;

							
					
				} while (cur.moveToNext());display();
			 }
			else
			{
				cf.onlinspectionlist.removeAllViews();
			}
				
		
		}

		private void display() {

			cf.onlinspectionlist.removeAllViews();
			cf.sv = new ScrollView(this);
			cf.onlinspectionlist.addView(cf.sv);

			final LinearLayout l1 = new LinearLayout(this);
			l1.setOrientation(LinearLayout.VERTICAL);
			cf.sv.addView(l1);

				this.cf.data = cf.inspdata.split("~");
				for (int i = 0; i < cf.data.length; i++) {
					cf.tvstatus = new TextView[cf.rws];
					cf.deletebtn = new Button[cf.rws];
					LinearLayout l2 = new LinearLayout(this);
					l2.setOrientation(LinearLayout.HORIZONTAL);
					l1.addView(l2);

					LinearLayout lchkbox = new LinearLayout(this);
					LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
							400, ViewGroup.LayoutParams.WRAP_CONTENT);
					paramschk.topMargin = 8;
					paramschk.leftMargin = 20;
					paramschk.bottomMargin = 10;
					l2.addView(lchkbox);
					cf.tvstatus[i] = new TextView(this);
					cf.tvstatus[i].setMinimumWidth(380);
					cf.tvstatus[i].setMaxWidth(380);
					cf.tvstatus[i].setTag("textbtn" + i);
					if(cf.data[i].contains("^"))
					{
						cf.data[i]=cf.data[i].replace( "^",",");
					}
					cf.tvstatus[i].setText(cf.data[i]);
					cf.tvstatus[i].setTextColor(Color.BLACK);
					cf.tvstatus[i].setTextSize(16);
					lchkbox.addView(cf.tvstatus[i], paramschk);

					LinearLayout ldelbtn = new LinearLayout(this);
					LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
							93, 37);
					paramsdelbtn.rightMargin = 10;
					paramsdelbtn.bottomMargin = 10;
					paramsdelbtn.topMargin = 5;
					l2.addView(ldelbtn);

					cf.deletebtn[i] = new Button(this);
					cf.deletebtn[i].setBackgroundResource(R.drawable.deletebtn1);
					cf.deletebtn[i].setTag("deletebtn" + i);
					cf.deletebtn[i].setPadding(30, 0, 0, 0);
					ldelbtn.addView(cf.deletebtn[i], paramsdelbtn);

					
					cf.deletebtn[i].setOnClickListener(new View.OnClickListener() {
						public void onClick(final View v) {
							String getidofselbtn = v.getTag().toString();
							final String repidofselbtn = getidofselbtn.replace(
									"deletebtn", "");
							final int cvrtstr = Integer.parseInt(repidofselbtn);
							final String dt = cf.countarr[cvrtstr];
							 cf.alerttitle="Delete";
							  cf.alertcontent="Are you sure want to delete?";
							    final Dialog dialog1 = new Dialog(Observation1.this,android.R.style.Theme_Translucent_NoTitleBar);
								dialog1.getWindow().setContentView(R.layout.alertsync);
								TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
								txttitle.setText( cf.alerttitle);
								TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
								txt.setText(Html.fromHtml( cf.alertcontent));
								Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
								Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
								btn_yes.setOnClickListener(new OnClickListener()
								{
				                	@Override
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										try {
											cf.sh_db.execSQL("DELETE FROM "
													+ cf.Observation1T
													+ " WHERE OB1TId ='" + cf.encode(dt)
													+ "'");
											cf.ShowToast("Deleted Sucessfully", 1);
											dbquery();
											
										} catch (Exception e) {

										}
										
									}
									
								});
								btn_cancel.setOnClickListener(new OnClickListener()
								{

									@Override
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										
									}
									
								});
								dialog1.setCancelable(false);
								dialog1.show();
							      	}
									});
						

				}
			
		}
	private void setvalueToRadio(RadioButton obs1optn1y,
			RadioButton obs1optn1n, RadioButton obs1optn1nd,
			String rd_IrregularLandSurface2, RelativeLayout show_obs1_optn1y) {
		// TODO Auto-generated method stub
		if(obs1optn1y.getText().toString().equals(rd_IrregularLandSurface2))
		{
			obs1optn1y.setChecked(true);
			show_obs1_optn1y.setVisibility(cf.show.VISIBLE);
		}
		else if(obs1optn1n.getText().toString().equals(rd_IrregularLandSurface2))
		{
			obs1optn1n.setChecked(true);
		}
		else if(obs1optn1nd.getText().toString().equals(rd_IrregularLandSurface2))
		{
			obs1optn1nd.setChecked(true);
		}
	}
	class Touch_Listener implements OnTouchListener
	{
		   public int type;
		   Touch_Listener(int type)
			{
				this.type=type;
				
			}
		    @Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	if(this.type==1)
				{
		    		cf.setFocus(cf.etcomments_obs1_opt1);
				}
		    	else if(this.type==2)
				{
			    		cf.setFocus(cf.etcomments_obs1_opta);
				}
		    	else if(this.type==3)
				{
			    		cf.setFocus(cf.etcomments_obs2);
				}
		    	else if(this.type==4)
				{
			    		cf.setFocus(cf.etcomments_obs3);
				}
		    	else if(this.type==5)
				{
			    		cf.setFocus(cf.etcomments_obs4);
				}
		    	else if(this.type==6)
				{
			    		cf.setFocus(cf.etcomments_obs5);
				}
		    	else if(this.type==7)
				{
			    		cf.setFocus(cf.etcomments_obs6_opt1);
				}
		    	else if(this.type==8)
				{
			    		cf.setFocus(cf.etcomments_obs6_opta);
				}
		    	else if(this.type==9)
				{
			    		cf.setFocus(cf.etcomments_obs6_optb);
				}
		    	else if(this.type==10)
				{
			    		cf.setFocus(cf.etcomments_obs6_optc);
				}
		    	else if(this.type==11)
				{
			    		cf.setFocus(cf.etcomments_obs7_opt1);
				}
		    	else if(this.type==12)
				{
			    		cf.setFocus(cf.etcomments_obs7_opta);
				}
		    	else if(this.type==13)
				{
			    		cf.setFocus(cf.etinch_obs6a);
				}
		    	else if(this.type==14)
				{
			    		cf.setFocus(cf.etfeet_obs6a);
				}
		    	
				return false;
			}
		 
	}
	class SH_textwatcher implements TextWatcher
		{
	         public int type;
			SH_textwatcher(int type)
			{
				this.type=type;
				
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(this.type==1)
				{
					  cf.showing_limit(s.toString(),SH_OBS1P,SH_OB1,SH_OB1T,"499");
					
				}
				else if(this.type==2)
				{
					cf.showing_limit(s.toString(),SH_OBS1AP,SH_OB1A,SH_OB1AT,"499");
				}
				else if(this.type==3)
				{
					cf.showing_limit(s.toString(),SH_OBS2P,SH_OB2,SH_OB2T,"499");
				}
				else if(this.type==4)
				{
					cf.showing_limit(s.toString(),SH_OBS3P,SH_OB3,SH_OB3T,"499");
				}
				else if(this.type==5)
				{
					cf.showing_limit(s.toString(),SH_OBS4P,SH_OB4,SH_OB4T,"499");
				}
				else if(this.type==6)
				{
					cf.showing_limit(s.toString(),SH_OBS5P,SH_OB5,SH_OB5T,"499");
				}
				else if(this.type==7)
				{
					cf.showing_limit(s.toString(),SH_OBS6P,SH_OB6,SH_OB6T,"499");
				}
				else if(this.type==8)
				{
					cf.showing_limit(s.toString(),SH_OBS6AP,SH_OB6A,SH_OB6AT,"499");
				}
				else if(this.type==9)
				{
					cf.showing_limit(s.toString(),SH_OBS6BP,SH_OB6B,SH_OB6BT,"499");
				}
				else if(this.type==10)
				{
					cf.showing_limit(s.toString(),SH_OBS6CP,SH_OB6C,SH_OB6CT,"499");
				}
				else if(this.type==11)
				{
					cf.showing_limit(s.toString(),SH_OBS7P,SH_OB7,SH_OB7T,"499");
				}
				else if(this.type==12)
				{
					cf.showing_limit(s.toString(),SH_OBS7AP,SH_OB7A,SH_OB7AT,"499");
				}
				else if(this.type==12)
				{
					cf.showing_limit(s.toString(),SH_OBS7AP,SH_OB7A,SH_OB7AT,"499");
				}
				
			
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
			}
			
		}		 
	  class Obs_clicker implements OnClickListener	  {




	  private static final int visibility = 0;

	@Override
	  public void onClick(View v) {


	   // TODO Auto-generated method stub
		  switch(v.getId()){
			  case R.id.obs1_opt1rdy:/** option1 radiobutton yes **/
				  cf.show_obs1_optn1y.setVisibility(visibility);
				  cf.etcomments_obs1_opt1.setText("");
				  rd_IrregularLandSurface=retrieve_radioname(cf.obs1optn1y);
				  break;
			  case R.id.obs1_opt1rdn:/** option1 radiobutton no **/
				  cf.show_obs1_optn1y.setVisibility(v.GONE);
				  cf.etcomments_obs1_opt1.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_IrregularLandSurface=retrieve_radioname(cf.obs1optn1n);
				  cf.Set_UncheckBox(cf.cb_obs1,cf.etother_obs1_optn1);
				  break;
			  case R.id.obs1_opt1rdnd:/** option1 radiobutton not determined **/
				  cf.show_obs1_optn1y.setVisibility(v.GONE);
				  cf.etcomments_obs1_opt1.setText("Not all areas visible or accessible at time of inspection. From the areas observed no depressions or irregular land surfaces were noted.");
				  rd_IrregularLandSurface=retrieve_radioname(cf.obs1optn1nd);
				  cf.Set_UncheckBox(cf.cb_obs1,cf.etother_obs1_optn1);
				  break;
			  case R.id.other1:/**OPTION1 CHECKBOX OTHER **/
				  if(cf.cb_obs1[cf.cb_obs1.length-1].isChecked())
				  {
					  cf.etother_obs1_optn1.setVisibility(visibility);
					  cf.etother_obs1_optn1.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs1_optn1.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  case R.id.obs1_optardy:/** optionA radiobutton yes **/
				  cf.show_obs1_optnay.setVisibility(visibility);
				  cf.etcomments_obs1_opta.setText("");
				  rd_VisibleBuriedDebris=retrieve_radioname(cf.obs1optnay);
				  break;
			  case R.id.obs1_optardn:/** optionA radiobutton no **/
				  cf.show_obs1_optnay.setVisibility(v.GONE);
				  cf.etcomments_obs1_opta.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_VisibleBuriedDebris=retrieve_radioname(cf.obs1optnan);
				  cf.Set_UncheckBox(cf.cb_obs1a,cf.etother_obs1_optna);
				  break;
			  case R.id.obs1_optardnd:/** optionA radiobutton not determined **/
				  cf.show_obs1_optnay.setVisibility(v.GONE);
				  cf.etcomments_obs1_opta.setText("No visible clues or other information disclosed or reported at time of inspection.");
				  rd_VisibleBuriedDebris=retrieve_radioname(cf.obs1optnand);
				  cf.Set_UncheckBox(cf.cb_obs1a,cf.etother_obs1_optna);
				  break;
			 case R.id.othera:/**OPTIONA CHECKBOX OTHER **/
				 if(cf.cb_obs1a[cf.cb_obs1a.length-1].isChecked())
				  {
					 cf.etother_obs1_optna.setVisibility(visibility);
					 cf.etother_obs1_optna.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs1_optna.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  case R.id.firstobsrow:/** SELECTING OBSERVATION1 QUESTION ROW **/
				  obs_tablelayout_hide();cf.tbl_obs1.setVisibility(visibility);
				  cf.tbl_row_obs1.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs1_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.secondobs:/** SELECTING OBSERVATION2 QUESTION ROW **/
				  obs_tablelayout_hide();cf.tbl_obs2.setVisibility(visibility);
				  cf.tbl_row_obs2.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs2_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.obs2_optrdy:/** observation2 radiobutton yes **/
				  cf.show_obs2_optny.setVisibility(visibility);
				  cf.etcomments_obs2.setText("");
				  rd_IrregularLandSurfaceAdjacent=retrieve_radioname(cf.obs2optny);
				  break;
			  case R.id.obs2_optrdn:/** observation2 radiobutton no **/
				  cf.show_obs2_optny.setVisibility(v.GONE);
				  cf.etcomments_obs2.setText("None noted to the visible / accessible areas surveyed.");
				  rd_IrregularLandSurfaceAdjacent=retrieve_radioname(cf.obs2optnn);
				  cf.Set_UncheckBox(cf.cb_obs2,cf.etother_obs2);
				  break;
			  case R.id.obs2_optrdnd:/** observation2 radiobutton not determined **/
				  cf.show_obs2_optny.setVisibility(v.GONE);
				 // cf.etcomments_obs2.setText("Limited access to adjoining properties.");
				  cf.etcomments_obs2.setText("Not all areas visible or accessible at time of inspection. From the areas observed no depressions or irregular land surfaces were noted.");
				  rd_IrregularLandSurfaceAdjacent=retrieve_radioname(cf.obs2optnnd);
				  cf.Set_UncheckBox(cf.cb_obs2,cf.etother_obs2);
				  break;
			 case R.id.obs2_other:/**observation2 CHECKBOX OTHER **/
				  if(cf.cb_obs2[cf.cb_obs2.length-1].isChecked())
				  { 
					  cf.etother_obs2.setVisibility(visibility);
					  cf.etother_obs2.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs2.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  case R.id.thirdobs:/** SELECTING OBSERVATION3 QUESTION ROW **/
				  obs_tablelayout_hide();cf.tbl_obs3.setVisibility(visibility);
				  cf.tbl_row_obs3.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs3_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.obs3_optrdy:/** observation3 radiobutton yes **/
				  cf.show_obs3_optny.setVisibility(visibility);
				  cf.etcomments_obs3.setText("");
				  rd_SoilCollapse=retrieve_radioname(cf.obs3optny);
				  break;
			  case R.id.obs3_optrdn:/** observation3 radiobutton no **/
				  cf.show_obs3_optny.setVisibility(v.GONE);
				  cf.etcomments_obs3.setText("None noted to the visible / accessible areas surveyed.");
				  rd_SoilCollapse=retrieve_radioname(cf.obs3optnn);
				  cf.Set_UncheckBox(cf.cb_obs3,cf.etother_obs3);
				  break;
			  case R.id.obs3_optrdnd:/** observation3 radiobutton not determined **/
				  cf.show_obs3_optny.setVisibility(v.GONE);
				  cf.etcomments_obs3.setText("Not all areas visible or accessible at time of inspection. From the areas observed no evidence of soil collapse features were noted.");
				  rd_SoilCollapse=retrieve_radioname(cf.obs3optnnd);
				  cf.Set_UncheckBox(cf.cb_obs3,cf.etother_obs3);
				  break;
			  case R.id.obs3_other:/**observation3 CHECKBOX OTHER **/
				  if(cf.cb_obs3[cf.cb_obs3.length-1].isChecked())
				  { 
					  cf.etother_obs3.setVisibility(visibility);
					  cf.etother_obs3.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs3.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  case R.id.fourthobs:/** SELECTING OBSERVATION4 QUESTION ROW **/
				  obs_tablelayout_hide();cf.tbl_obs4.setVisibility(visibility);
				  cf.tbl_row_obs4.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs4_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.obs4_optrdy:/** observation4 radiobutton yes **/
				  cf.show_obs4_optny.setVisibility(visibility);
				  cf.etcomments_obs4.setText("");
				  rd_SoilErosionAroundFoundation=retrieve_radioname(cf.obs4optny);
				  break;
			  case R.id.obs4_optrdn:/** observation4 radiobutton no **/
				  cf.show_obs4_optny.setVisibility(v.GONE);
				  cf.etcomments_obs4.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_SoilErosionAroundFoundation=retrieve_radioname(cf.obs4optnn);
				  cf.Set_UncheckBox(cf.cb_obs4,cf.etother_obs4);
				  break;
			  case R.id.obs4_optrdnd:/** observation4 radiobutton not determined **/
				  cf.show_obs4_optny.setVisibility(v.GONE);
				  cf.etcomments_obs4.setText("Not all areas visible or accessible at time of inspection. From the areas observed no evidence of soil erosion was noted around foundation.");
				  rd_SoilErosionAroundFoundation=retrieve_radioname(cf.obs4optnnd);
				  cf.Set_UncheckBox(cf.cb_obs4,cf.etother_obs4);
				  break;
			 case R.id.obs4_other:/**observation4 CHECKBOX OTHER **/
				  if(cf.cb_obs4[cf.cb_obs4.length-1].isChecked())
				  { 
					  cf.etother_obs4.setVisibility(visibility);
					  cf.etother_obs4.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs4.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  case R.id.fifthobs:/** SELECTING OBSERVATION5 QUESTION ROW **/
				  obs_tablelayout_hide();cf.tbl_obs5.setVisibility(visibility);
				  cf.tbl_row_obs5.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs5_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.obs5_optrdy:/** observation5 radiobutton yes **/
				  cf.show_obs5_optny.setVisibility(visibility);
				  cf.etcomments_obs5.setText("");
				  rd_DrivewaysCracksNoted=retrieve_radioname(cf.obs5optny);
				  break;
			  case R.id.obs5_optrdn:/** observation5 radiobutton no **/
				  cf.show_obs5_optny.setVisibility(v.GONE);
				  cf.etcomments_obs5.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_DrivewaysCracksNoted=retrieve_radioname(cf.obs5optnn);
				  cf.Set_UncheckBox(cf.cb_obs5,cf.etother_obs5);
				  break;
			  case R.id.obs5_optrdnd:/** observation5 radiobutton not determined **/
				  cf.show_obs5_optny.setVisibility(v.GONE);
				  cf.etcomments_obs5.setText("Not all areas visible or accessible at time of inspection. From the areas observed no depressions or irregular land surfaces noted.");
				  rd_DrivewaysCracksNoted=retrieve_radioname(cf.obs5optnnd);
				  cf.Set_UncheckBox(cf.cb_obs5,cf.etother_obs5);
				  break;
			 case R.id.obs5_other:/**observation5 CHECKBOX OTHER **/
				  if(cf.cb_obs5[cf.cb_obs5.length-1].isChecked())
				  { 
					  cf.etother_obs5.setVisibility(visibility);
					  cf.etother_obs5.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs5.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  case R.id.sixthobs:/** SELECTING OBSERVATION6 QUESTION ROW **/
				  obs_tablelayout_hide();cf.tbl_obs6.setVisibility(visibility);
				  cf.tbl_row_obs6.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs6_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.obs6_opt1rdy:/** observation6-option1 radiobutton yes **/
				  cf.show_obs6_opt1ny.setVisibility(visibility);
				  cf.etcomments_obs6_opt1.setText("");
				  rd_UpliftDrivewaysSurface=retrieve_radioname(cf.obs6opt1ny);
				  break;
			  case R.id.obs6_opt1rdn:/** observation6-option1 radiobutton no **/
				  cf.show_obs6_opt1ny.setVisibility(v.GONE);
				  cf.etcomments_obs6_opt1.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_UpliftDrivewaysSurface=retrieve_radioname(cf.obs6opt1nn);
				  cf.etinch_obs6a.setText("");
				  cf.etfeet_obs6a.setText("");
				  cf.Set_UncheckBox(cf.cb_obs6,cf.etother_obs6_opt1);
				  break;
			  case R.id.obs6_opt1rdnd:/** observation6-option1 radiobutton not determined **/
				  cf.show_obs6_opt1ny.setVisibility(v.GONE);
				  cf.etcomments_obs6_opt1.setText("");
				  rd_UpliftDrivewaysSurface=retrieve_radioname(cf.obs6opt1nnd);
				  cf.etinch_obs6a.setText("");
				  cf.etfeet_obs6a.setText("");
				  cf.Set_UncheckBox(cf.cb_obs6,cf.etother_obs6_opt1);
				  break;
			  case R.id.obs6_opt1_other:/**observation6-option1 CHECKBOX OTHER **/
				  if(cf.cb_obs6[cf.cb_obs6.length-1].isChecked())
				  { 
					  cf.etother_obs6_opt1.setVisibility(visibility);
					  cf.etother_obs6_opt1.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs6_opt1.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			   case R.id.obs6_optardy:/** observation6-optiona radiobutton yes **/
					  cf.show_obs6_optany.setVisibility(visibility);
					  cf.etcomments_obs6_opta.setText("");
					  rd_LargeTree=retrieve_radioname(cf.obs6optany);
					  dbquery();
					  break;
				  case R.id.obs6_optardn:/** observation6-optiona radiobutton no **/
					  cf.show_obs6_optany.setVisibility(v.GONE);
					  cf.etcomments_obs6_opta.setText("None noted at time of inspection to accessible areas surveyed.");
					  rd_LargeTree=retrieve_radioname(cf.obs6optann);
					  cf.Set_UncheckBox(cf.cb_obs6A,cf.etother_obs6_opta);
					  break;
				  case R.id.obs6_optardnd:/** observation6-optiona radiobutton not determined **/
					  cf.show_obs6_optany.setVisibility(v.GONE);
					  cf.etcomments_obs6_opta.setText("");
					  rd_LargeTree=retrieve_radioname(cf.obs6optannd);
					  cf.Set_UncheckBox(cf.cb_obs6A,cf.etother_obs6_opta);
					  break;
				 case R.id.obs6_othera:/**observation6-optiona CHECKBOX OTHER **/
					  if(cf.cb_obs6A[cf.cb_obs6A.length-1].isChecked())
					  {
						 cf.etother_obs6_opta.setVisibility(visibility);
						 cf.etother_obs6_opta.requestFocus();
					  }
					  else
					  {
						  cf.etother_obs6_opta.setVisibility(cf.show.GONE);
						 
					  }
					  break;
				   case R.id.obs6_optbrdy:/** observation6-optionb radiobutton yes **/
						  cf.show_obs6_optbny.setVisibility(visibility);
						  cf.etcomments_obs6_optb.setText("");
						  rd_CypressTree=retrieve_radioname(cf.obs6optbny);
						  break;
					  case R.id.obs6_optbrdn:/** observation6-optionb radiobutton no **/
						  cf.show_obs6_optbny.setVisibility(v.GONE);
						  cf.etcomments_obs6_optb.setText("None noted at time of inspection to accessible areas surveyed.");
						  rd_CypressTree=retrieve_radioname(cf.obs6optbnn);
						  cf.Set_UncheckBox(cf.cb_obs6B,cf.etother_obs6_optb);
						  break;
					  case R.id.obs6_optbrdnd:/** observation6-optionb radiobutton not determined **/
						  cf.show_obs6_optbny.setVisibility(v.GONE);
						  cf.etcomments_obs6_optb.setText("");
						  rd_CypressTree=retrieve_radioname(cf.obs6optbnnd);
						  cf.Set_UncheckBox(cf.cb_obs6B,cf.etother_obs6_optb);
						  break;
					   case R.id.otherb:/**observation6-optionb CHECKBOX OTHER **/
						   if(cf.cb_obs6B[cf.cb_obs6B.length-1].isChecked())
							  { 
							     cf.etother_obs6_optb.setVisibility(visibility);
							     cf.etother_obs6_optb.requestFocus();
							 }
						     else
							 {
								  cf.etother_obs6_optb.setVisibility(cf.show.GONE);
								 
							 }
						  break;
					   case R.id.obs6_optcrdy:/** observation6-optionc radiobutton yes **/
							  cf.show_obs6_optcny.setVisibility(visibility);
							  cf.etcomments_obs6_optc.setText("");
							  rd_LargeTreesRemoved=retrieve_radioname(cf.obs6optcny);
							  break;
						  case R.id.obs6_optcrdn:/** observation6-optionc radiobutton no **/
							  cf.show_obs6_optcny.setVisibility(v.GONE);
							  cf.etcomments_obs6_optc.setText("None noted at time of inspection to accessible areas surveyed.");
							  rd_LargeTreesRemoved=retrieve_radioname(cf.obs6optcnn);
							  cf.Set_UncheckBox(cf.cb_obs6C,cf.etother_obs6_optc);
							  break;
						  case R.id.obs6_optcrdnd:/** observation6-optionc radiobutton not determined **/
							  cf.show_obs6_optcny.setVisibility(v.GONE);
							  cf.etcomments_obs6_optc.setText("");
							  rd_LargeTreesRemoved=retrieve_radioname(cf.obs6optcnnd);
							  cf.Set_UncheckBox(cf.cb_obs6C,cf.etother_obs6_optc);
							  break;
						  case R.id.otherc:/**observation6-optionc CHECKBOX OTHER **/
							  if(cf.cb_obs6C[cf.cb_obs6C.length-1].isChecked())
							  { 
								  cf.etother_obs6_optc.setVisibility(visibility);
								  cf.etother_obs6_optc.requestFocus();
							  }
							  else
							  {
									  cf.etother_obs6_optc.setVisibility(cf.show.GONE);
									 
								}
							  break;
			  case R.id.seventhobs:/** SELECTING OBSERVATION7 QUESTION ROW **/
				  obs_tablelayout_hide();cf.tbl_obs7.setVisibility(visibility);
				  cf.tbl_row_obs7.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs7_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.obs7_opt1rdy:/** observation7-option1 radiobutton yes **/
				  cf.show_obs7_opt1ny.setVisibility(visibility);
				  cf.etcomments_obs7_opt1.setText("");
				  rd_FoundationCrackNoted=retrieve_radioname(cf.obs7opt1ny);
				  break;
			  case R.id.obs7_opt1rdn:/** observation7-option1 radiobutton no **/
				  cf.show_obs7_opt1ny.setVisibility(v.GONE);
				  cf.etcomments_obs7_opt1.setText("None noted at time of inspection to accessible areas surveyed.");
				  rd_FoundationCrackNoted=retrieve_radioname(cf.obs7opt1nn);
			      cf.Set_UncheckBox(cf.cb_obs71,cf.etother_obs7_opt1txty1);
			      cf.Set_UncheckBox(cf.cb_obs72,cf.etother_obs7_opt1txty2);
			      cf.Set_UncheckBox(cf.cb_obs73,cf.etother_obs7_opt1txty3);
			      cf.etlen_obs7.setText("");
			      cf.Set_UncheckBox(cf.cb_obs74,cf.etother_obs7_opt1txty3);
			      cf.Set_UncheckBox(cf.cb_obs75,cf.etother_obs7_txty4other);
				  break;
			  case R.id.obs7_opt1rdnd:/** observation7-option1 radiobutton not determined **/
				  cf.show_obs7_opt1ny.setVisibility(v.GONE);
				  cf.etcomments_obs7_opt1.setText("");
				  rd_FoundationCrackNoted=retrieve_radioname(cf.obs7opt1nnd);
				  cf.Set_UncheckBox(cf.cb_obs71,cf.etother_obs7_opt1txty1);
			      cf.Set_UncheckBox(cf.cb_obs72,cf.etother_obs7_opt1txty2);
			      cf.Set_UncheckBox(cf.cb_obs73,cf.etother_obs7_opt1txty3);
			      cf.etlen_obs7.setText("");
			      cf.Set_UncheckBox(cf.cb_obs74,cf.etother_obs7_opt1txty3);
			      cf.Set_UncheckBox(cf.cb_obs75,cf.etother_obs7_txty4other);
				  break;
			   case R.id.obs7_opt1_txty1other:/**observation7-option1 CHECKBOX OTHER **/
				   if(cf.cb_obs71[cf.cb_obs71.length-1].isChecked())
				   {
					   cf.etother_obs7_opt1txty1.setVisibility(visibility);
					   cf.etother_obs7_opt1txty1.requestFocus();
				   }
				   else
				   {
					   cf.etother_obs7_opt1txty1.setVisibility(cf.show.GONE);
				   }
				  break;
			   case R.id.obs7_opt1_txty2other:/**observation7-option1 CHECKBOX inch OTHER **/
				   if(cf.cb_obs72[cf.cb_obs72.length-1].isChecked())
				   {
					   cf.etother_obs7_opt1txty2.setVisibility(visibility);
					   cf.etother_obs7_opt1txty2.requestFocus();
				   }
				   else
				   {
					   cf.etother_obs7_opt1txty2.setVisibility(cf.show.GONE);
				   }
					  break;
			   case R.id.obs7_opt1_txty3other:/**observation7-option1 CHECKBOX inch OTHER **/
				   if(cf.cb_obs73[cf.cb_obs73.length-1].isChecked())
				   {
					   cf.etother_obs7_opt1txty3.setVisibility(visibility);
					   cf.etother_obs7_opt1txty3.requestFocus();
				   }
				   else
				   {
					   cf.etother_obs7_opt1txty3.setVisibility(cf.show.GONE);
				   }
					  break;
				  		  case R.id.obs7_opt1_txty4other:/**observation7-option1 CHECKBOX other **/
				  			 if(cf.cb_obs73[cf.cb_obs73.length-1].isChecked())
							   {
				  				 cf.etother_obs7_txty4other.setVisibility(visibility);
				  				cf.etother_obs7_txty4other.requestFocus();
							   }
				  			  else
							   {
				  				cf.etother_obs7_txty4other.setVisibility(cf.show.GONE);
							   }
							  break;
						  case R.id.obs7_optardy:/** observation7-optiona radiobutton yes **/
							  cf.show_obs7_optnay.setVisibility(visibility);
							  cf.etcomments_obs7_opta.setText("");
							  rd_RetainingWallsServiceable=retrieve_radioname(cf.obs7optnay);
							  break;
						  case R.id.obs7_optardn:/** observation7-optiona radiobutton no **/
							  cf.show_obs7_optnay.setVisibility(v.GONE);
							  cf.etcomments_obs7_opta.setText("No visible issues noted at time of inspection");
							  rd_RetainingWallsServiceable=retrieve_radioname(cf.obs7optnan);
							  cf.Set_UncheckBox(cf.cb_obs7A,cf.etother_obs7_opt1txty1);
							  break;
						  case R.id.obs7_optardnd:/** observation7-optiona radiobutton not determined **/
							  cf.show_obs7_optnay.setVisibility(v.GONE);
							  cf.etcomments_obs7_opta.setText("No retaining wall present.");
							  rd_RetainingWallsServiceable=retrieve_radioname(cf.obs7optnand);
							  cf.Set_UncheckBox(cf.cb_obs7A,cf.etother_obs7_opt1txty1);
							  break;
		    }

	  }
	   
	   
	  }
	  public void clicker(View v) {





          switch(v.getId()){
			  case R.id.opt1_ndhelp: /** HELP FOR OPTION1 NOTDETERMINED **/
				  cf.alerttitle="Not Determined";
				  //cf.alertcontent="<div style='width: 100%;font-family: Tahoma;font-size: 11px;'><p><strong></strong></p><table cellpadding='3' cellspacing='0' style='border: solid 1px #E33431; font-family: Tahoma;font-size: 11px;'><tr><td colspan='2' style='border-bottom: solid 0px #E33431;background-color: #E33431;height: 30px; color: #ffffff;'><strong>Home Owner</strong></td></tr><tr><td align='right' style='width:50%;'> ExportWSI1802ElevationPicture :</td><td align='left' style='width:50%;'>101</td></tr><tr><td align='right' style='width:50%;'> ExportWSI1802Questions :</td><td align='left' style='width:50%;'>5</td></tr><tr><td align='right' style='width:50%;'> ExportWSI1802RoofCoverType :</td><td align='left' style='width:50%;'>4</td></tr><tr><td align='right' style='width:50%;'> ExportWSIInsComments :</td><td align='left' style='width:50%;'>5</td></tr><tr><td align='right' style='width:50%;'> UpdateInspectionStatus :</td><td align='left' style='width:50%;'>1</td></tr></table></div>";
				  cf.alertcontent="Inspection should use Not Determined if the full building lot is not accessible due to conditions such as woods, overgrown vegetation, etc. Please explain limitaion in full in the overall comment area after Question No. 22.";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.opt1_help: /** HELP FOR OPTION1 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors need to pay "+
"close attention to yard areas on every elevation of the insured "+
"premises, looking for any undulations or other abnormal activity "+
"possibly related to a potential sinkhole issue.  These undulations are "+
"particularly important to sinkhole-prone areas.</p><p>A photographic "+
"record of all the surrounding areas of the home or building must be "+
"provided as part of the inspector\'s process which will be "+
"examined by the Quality Assurance Department prior to release of "+
"report.</p><p>Where any issues exist, inspectors must comment if any "+ 
"issues exist relating to the regular land surfaces.</p><p>If the "+
"Inspector is unable to determine the condition of the entire lot based "+ 
"on size, complexity, extent of vegetation or wooded area, this must be "+
"marked as \"Not Determined\" and comments/photographs  "+
"must be provided.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.opta_ndhelp: /** HELP FOR OPTIONA NOTDETERMINED **/
				  cf.alerttitle="Not Determined";
				  cf.alertcontent="Inspection should use Not Determined if the full building lot is not accessible due to conditions such as woods, overgrown vegetation, etc. Please explain limitaion in full in the overall comment area after Question No. 22.";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.opta_help: /** HELP FOR OPTIONA **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>If there is any evidence "+
"of buried debris or disclosure of any nature, this must be clearly "+
"outlined on the inspection report with ample comments.  Clear "+
"photographs must be provided of all yard and surrounding "+
"areas.</p><p>Please note that this condition must be outlined, commented "+
"and photographed within the report.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs2_help: /** HELP FOR OBSERVATION2 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors need to review "+
"the adjoining property on both sides of the street or behind, if needed, "+
"to determine if any evidence of previous sinkhole or settlement activity "+
"is noted.  Pay close attention to previous repairs, potential major "+
"settlement, in addition to any undulations or issues noted to the "+
"surrounding lot-ground.  Clear photographs, documentation and "+
"commentary must be provided.  Inspectors are not required to enter "+
"adjoining premises and to report on any elevation not visible.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs3_help: /** HELP FOR OBSERVATION3 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>A \"collapse feature\" is an actual hole in the ground that " +
				  		"resembles a " +
				  		"potential sinkhole.   The Inspector should clearly comment, photograph " +
				  		"and outline any area that resembles a sinkhole or soil collapse found " +
				  		"within the insureds premises or adjoining property.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs4_help: /** HELP FOR OBSERVATION4 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Soil Erosion/washout can "+						  
                      "be caused by many reasons, many of which are normal, typically caused by "+
					  "deferred maintenance.   Examples include broken sprinkler lines, "+
"inadequate rainwater control, downspout terminations, to mention a few.  "+
"Where soil erosion is noted, it should be properly identified, "+
"photographed and commented in addition to whether settlement activity of "+
"any nature is found or if the washout is severe enough to undermine the "+
"supporting structure or require emergency repair procedures.</p><p>If "+
"the washout or erosion is associated with potential soil collapse or "+
"sinkhole issues, this should be clearly outlined in the "+
"report.</p><p>Photographs should include both close-up and general views "+
"of the issues and building lines to show auditors and underwriting, the "+
"condition of the building lines from a uniform - level and plumb "+
"perspective.  The close up pictures should have a photograph of a "+
"4\" spirit level against the wall showing clearly the plumbness "+
"of the wall next to the soil erosion areas.  Multiple photographs will "+
"be required if various areas of soil erosion is found.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs5_help: /** HELP FOR OBSERVATION5 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Cracking is common to " +
				  		"almost all concrete surfaces outside of a home/building because of the " +
				  		"nature of the curing process, exposure, surrounding features such as " +
				  		"large trees, washout from broken sprinkler lines, running water run-off, " +
				  		"etc.</p><p>All cracking must be documented and recorded if present and " +
				  		"must be photographed and provided within the inspection report.  The " +
				  		"exact cause of the cracking cannot be determined without specialist " +
				  		"testing techniques and monitoring.  Inspectors should be cautioned to " +
				  		"use terms such as \"Hairline cracking or cracking appears to be " +
				  		"the result of..\"</p><p>If cracking is caused by tree " +
				  		"root damage from adjoining trees or shrubs, or from broken sprinkler " +
				  		"lines, this should be clearly photographed and outlined.  Severe " +
				  		"cracking, sinking, trip hazards, etc. should all be commented and " +
				  		"photographed.</p><p>If cracks are hairline in nature only and typically " +
				  		"the result of inadequate compaction or poor construction practices, this " +
				  		"should be pointed out within the comment areas also.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs6_opt1_help: /** HELP FOR OBSERVATION6 OPTION1 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>If trip hazards are noted " +
				  		"to any concrete hard surface, these need to be clearly photographed and " +
				  		"commented with the most probable reason as to the cause if " +
				  		"available.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs6_opta_help: /** HELP FOR OBSERVATION6 OPTIONA **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>All large trees, " +
				  		"approximately 12inches or larger must be documented on the inspection " +
				  		" report.  Photographs and commentary on the proximity, i.e. distance to " +
				  		" home, must be provided as part of the inspection report.  If large trees " +
				  		" from adjoining property are encroaching on the insured premises, this " +
				  		" needs to be outlined by using the \"Other\" selection with " +
				  		" comments and photograph captions.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs6_optb_help: /** HELP FOR OBSERVATION6 OPTIONB **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Cypress trees are " +
				  		"associated with wet/saturated soils.  Any cypress trees or knees located " +
				  		"on the property should be identified, photographed and commented with " +
				  		"the approximate distance from the home or building being inspected.  " +
				  		"Cypress trees on adjoining properties within close proximity should also " +
				  		"be outlined.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs6_optc_help: /** HELP FOR OBSERVATION6 OPTIONC **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Large trees more recently " +
				  		"removed may not be visible and it is for this reason that questions must " +
				  		"be raised to the policyholder relating to this requirement.  Uneven " +
				  		"ground surfaces should be questioned and recorded.  Such conditions may " +
				  		"affect the insured\'s ability to gain property insurance and full " +
				  		"disclaimer with policyholder should be made.</p><p>Large trees recently " +
				  		"removed within proximity of the building structure are concerning as " +
				  		"tree roots left in the ground may decay and cause subsidence or " +
				  		"settlement in these regionalized areas down line.</p><p>Inspectors " +
				  		"should pay attention to the adjoining property and neighborhood to " +
				  		"determine the general tree coverage and how it compares to the subject " +
				  		"property.</p><p>Clear photographs and comments are needed.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs7_opt1_help: /** HELP FOR OBSERVATION7 OPTION1 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Foundation cracks, while " +
				  		"not uncommon are more serious in nature depending on the location, " +
				  		"extent and settlement observed.  Foundation cracks at building corners, " +
				  		"changes in direction, and more recent additions are more common and " +
				  		"typically related to differential settlement or differential materials. " +
				  		"This type of cracking will be hairline and typically vertical in " +
				  		"nature.</p><p>Foundation cracking that is diagonal or vertical in nature " +
				  		"where one end of the crack is wider than the other may be symptomatic to " +
				  		"a more serious issue.  Inspectors should measure and describe the " +
				  		"conditions noted, in addition to the surrounding conditions including " +
				  		"but not limited to building lines, elevations, providing photographs " +
				  		"using a 4&amp;#34; spirit level, both close up and from a " +
				  		"distance.</p><p>Where cracks are present and measurements are needed, " +
				  		"these should be accompanied by clear photographs of the measurements " +
				  		"taken and proper captions.</p><p>While diagnosing cracks, the conditions " +
				  		"of cracks are also indicative of the potential for future or ongoing " +
				  		"settlement.   Older cracks associated with initial settlement are " +
				  		"typically covered with dust, debris and cobwebs, whereas more serious " +
				  		"cracks, or cracks which have occurred more recently will have sharp " +
				  		"edges and will be clean.  Cracking through joints in block work or brick " +
				  		"work is also typically considered gradual verses cracking through the " +
				  		"block or brickwork which can be considered more sudden or " +
				  		"serious.</p><p>If any building finish such as paint or caulking is found " +
				  		"to be squeezed out, particularly if painting has occurred since original " +
				  		"construction, this is indicative of settlement that has occurred since " +
				  		"the alterations or remodeling has taken place.  Evidence of more recent " +
				  		"settlement is always a concern.</p><p>Full documentation of the " +
				  		"observations should be provided.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs7_opta_help: /** HELP FOR OBSERVATION7-OPTIONA **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Retaining walls are " +
				  		"typically not found on most homes however where homes are built on a " +
				  		"slope or a swimming pool is in close proximity to adjoining lakes and " +
				  		"ponds, retaining walls are used.</p><p>Inspectors should properly " +
				  		"examine retaining walls and look for any evidence of unevenness, " +
				  		"settlement, leaning, decay, erosion, water washout, and properly " +
				  		"document and comment any issues that were found with the retaining walls " +
				  		"noted.  Retaining wall features, depending on type and proximity to the " +
				  		"home, could undermine the home support system.</p><p>Inspectors should " +
				  		"also familiarize themselves with the various retaining walls used in " +
				  		"residential and commercial construction.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.home: /*SELECTING HOME */
				  cf.gohome();
				  break;
			  case R.id.save: /*SELECTING SAVE*/
				  fn_save();
				  break;
			  case R.id.obs6_opt1_save:
				 	if(cf.etinch_obs6a.getText().toString().equals(""))
					{
								cf.ShowToast("Please enter Approximate width of the tree for Question No.6(A)", 0);
								cf.etinch_obs6a.requestFocus();
							}
							else
							{
								if(cf.etfeet_obs6a.getText().toString().equals(""))
								{
									cf.ShowToast("Please enter Approximate height of the tree for Question No.6(A)", 0);
									cf.etfeet_obs6a.requestFocus();
								}
								else
								{
									 chk_Value6A= cf.getselected_chk(cf.cb_obs6A);
									 String chk_Value6Aother =(cf.cb_obs6A[cf.cb_obs6A.length-1].isChecked())? cf.etother_obs6_opta.getText().toString():""; // append the other text value in to the selected option
									 if(chk_Value6A.equals(""))
									 {
										 chk_Value6A+=chk_Value6Aother;
									 }
									 else if(!chk_Value6Aother.equals(""))
									 {
										 chk_Value6A+=","+chk_Value6Aother;
									 }
									 
									 if(chk_Value6A.equals(""))
									 {
										 if(cf.cb_obs6A[cf.cb_obs6A.length-1].isChecked()){
											 cf.ShowToast("Please enter other text for Question No.6(A) Locations." , 0);
											 cf.etother_obs6_opta.requestFocus();
										 }
										 else{
										 cf.ShowToast("Please select any checkbox for Question No.6(A) Locations." , 0);}
									 }
									 else 
									 {
										 if(cf.cb_obs6A[cf.cb_obs6A.length-1].isChecked())
										 {
											 if(chk_Value6Aother.trim().equals(""))
											 {
												 cf.ShowToast("Please enter other text for Question No.6(A) Locations.", 0);
												 cf.etother_obs6_opta.requestFocus();
											 }
											 else
											 {
												Chk_OBS6A_Insert();	
											 }
										 }
										 else
										 {
											 Chk_OBS6A_Insert();
										 }
									 }
								}
							}
					
				  break;
			  }
		
	  }
	private void Chk_OBS6A_Insert() {
		// TODO Auto-generated method stub
		cf.sh_db.execSQL("INSERT INTO "+cf.Observation1T+ "(SH_OBS1T_SRID,Width,Height,Location)" +
					"VALUES ('"+cf.encode(cf.selectedhomeid)+"','"+cf.encode(cf.etinch_obs6a.getText().toString())+"','"+cf.encode(cf.etfeet_obs6a.getText().toString())+"','"+cf.encode(chk_Value6A)+"')");
        dbquery();
        cf.etinch_obs6a.setText("");
	    cf.etfeet_obs6a.setText("");
		cf.Set_UncheckBox(cf.cb_obs6A,cf.etother_obs6_opta);
	}
	public String retrieve_radioname(RadioButton obs1optn1y) {
		// TODO Auto-generated method stub
		return obs1optn1y.getText().toString();
	}
	private void fn_save() {

		// TODO Auto-generated method stub
		if(rd_IrregularLandSurface.equals(""))/* CHECK QUESTION NO.1 RADIO BUTON*/
		{
			
			cf.ShowToast("Please select any answer for Question No.1", 0);
			cf.obs1optn1y.requestFocus();
		}
		else
		{
			if(rd_IrregularLandSurface.equals("Yes"))
			{
			 	 chk_Value1= cf.getselected_chk(cf.cb_obs1);
				 String chk_Value1other =(cf.cb_obs1[cf.cb_obs1.length-1].isChecked())? "^"+cf.etother_obs1_optn1.getText().toString():""; // append the other text value in to the selected option
				 chk_Value1+=chk_Value1other;
				 if(chk_Value1.equals(""))
				 {
					 
					 cf.ShowToast("Please select any checkbox for Question No.1 Locations." , 0);
					
				 }
				 else
				 {
					 if(cf.cb_obs1[cf.cb_obs1.length-1].isChecked())
					 {	 
						 if(chk_Value1other.trim().equals("^"))
						 {
							 cf.ShowToast("Please enter other text for Question No.1 Locations.", 0);
							 cf.etother_obs1_optn1.requestFocus();
						 }
						 else
						 {
							 chk_Value1 +=(cf.cb_obs1[cf.cb_obs1.length-1].isChecked())? "^"+cf.etother_obs1_optn1.getText().toString():""; // append the other text value in to the selected option
							 Chk_OBS1_Comments();	
						 }
					 }
					 else
					 {
						 Chk_OBS1_Comments();
					 }
				 }
			}
			else
			{
				Chk_OBS1_Comments();
			}
		}
	}
	private void Chk_OBS1_Comments() {
		// TODO Auto-generated method stub
		if(cf.etcomments_obs1_opt1.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.1 */
		{
			cf.ShowToast("Please enter Question No.1 Comments.", 0);
			cf.etcomments_obs1_opt1.requestFocus();
		}
		else
		{
			if(rd_VisibleBuriedDebris.equals(""))/* CHECK QUESTION NO.1(A) RADIO BUTON*/
			{
				
				cf.ShowToast("Please select any answer for Question No.1(A)", 0);
				cf.obs1optnay.requestFocus();
			}
			else
			{
				if(rd_VisibleBuriedDebris.equals("Yes"))
				{
				 	 chk_Value1A= cf.getselected_chk(cf.cb_obs1a);
					 String chk_Value1Aother =(cf.cb_obs1a[cf.cb_obs1a.length-1].isChecked())? "^"+cf.etother_obs1_optna.getText().toString():""; // append the other text value in to the selected option
					 chk_Value1A+=chk_Value1Aother;
					 if(chk_Value1A.equals(""))
					 {
						 
						 cf.ShowToast("Please select any checkbox for Question No.1(A) Locations." , 0);
					 }
					 else
					 {
						 if(cf.cb_obs1a[cf.cb_obs1a.length-1].isChecked())
						 {
							 if(chk_Value1Aother.trim().equals("^"))
							 {
								 cf.ShowToast("Please enter other text for Question No.1(A) Locations.", 0);
								 cf.etother_obs1_optna.requestFocus();
							 }
							 else
							 {
								 chk_Value1A +=(cf.cb_obs1a[cf.cb_obs1a.length-1].isChecked())? "^"+cf.etother_obs1_optna.getText().toString():""; // append the other text value in to the selected option
								 Chk_OBS1A_Comments();	
							 }
						 }
						 else
						 {
							 Chk_OBS1A_Comments();
						 }
					 }
				}
				else
				{
					Chk_OBS1A_Comments();
				}
			}
		}
	}
	private void Chk_OBS1A_Comments() {
		// TODO Auto-generated method stub
		if(cf.etcomments_obs1_opta.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.1(A) */
		{
			cf.ShowToast("Please enter Question No.1(A) Comments.", 0);
			cf.etcomments_obs1_opta.requestFocus();
		}
		else
		{
			if(rd_IrregularLandSurfaceAdjacent.equals(""))/* CHECK QUESTION NO.2 RADIO BUTON*/
			{
				
				cf.ShowToast("Please select any answer for Question No.2", 0);
				cf.obs2optny.requestFocus();
			}
			else
			{
				if(rd_IrregularLandSurfaceAdjacent.equals("Yes"))
				{
				 	 chk_Value2= cf.getselected_chk(cf.cb_obs2);
					 String chk_Value2other =(cf.cb_obs2[cf.cb_obs2.length-1].isChecked())? "^"+cf.etother_obs2.getText().toString():""; // append the other text value in to the selected option
					 chk_Value2+=chk_Value2other;
					 if(chk_Value2.equals(""))
					 {
						 
						 cf.ShowToast("Please select any checkbox for Question No.2 Locations." , 0);
					 }
					 else
					 {
						 if(cf.cb_obs2[cf.cb_obs2.length-1].isChecked())
						 {
							 if(chk_Value2other.trim().equals("^"))
							 {
								 cf.ShowToast("Please enter other text for Question No.2 Locations.", 0);
								 cf.etother_obs2.requestFocus();
							 }
							 else
							 {
								 chk_Value2 +=(cf.cb_obs2[cf.cb_obs2.length-1].isChecked())? "^"+cf.etother_obs2.getText().toString():""; // append the other text value in to the selected option
								 Chk_OBS2_Comments();	
							 }
						 }
						 else
						 {
							 Chk_OBS2_Comments();
						 }
					 }
				}
				else
				{
					Chk_OBS2_Comments();
				}
			}
		}
	}
	private void Chk_OBS2_Comments() {
		// TODO Auto-generated method stub
		if(cf.etcomments_obs2.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.2 */
		{
			cf.ShowToast("Please enter Question No.2 Comments.", 0);
			cf.etcomments_obs2.requestFocus();
		}
		else
		{
			if(rd_SoilCollapse.equals(""))/* CHECK QUESTION NO.3 RADIO BUTON*/
			{
				
				cf.ShowToast("Please select any answer for Question No.3", 0);
				cf.obs3optny.requestFocus();
			}
			else
			{
				if(rd_SoilCollapse.equals("Yes"))
				{
				 	 chk_Value3= cf.getselected_chk(cf.cb_obs3);
					 String chk_Value3other =(cf.cb_obs3[cf.cb_obs3.length-1].isChecked())? "^"+cf.etother_obs3.getText().toString():""; // append the other text value in to the selected option
					 chk_Value3+=chk_Value3other;
					 if(chk_Value3.equals(""))
					 {
						 
						 cf.ShowToast("Please select any checkbox for Question No.3 Locations." , 0);
					 }
					 else
					 {
						 if(cf.cb_obs3[cf.cb_obs3.length-1].isChecked())
						 {
							 if(chk_Value3other.trim().equals("^"))
							 {
								 cf.ShowToast("Please enter other text for Question No.3 Locations.", 0);
								 cf.etother_obs3.requestFocus();
							 }
							 else
							 {
								 chk_Value3 +=(cf.cb_obs3[cf.cb_obs3.length-1].isChecked())? "^"+cf.etother_obs3.getText().toString():""; // append the other text value in to the selected option
								 Chk_OBS3_Comments();	
							 }
						 }
						 else
						 {
							 Chk_OBS3_Comments();
						 }
					 }
				}
				else
				{
					Chk_OBS3_Comments();
				}
			}
		}
	}
	private void Chk_OBS3_Comments() {
		// TODO Auto-generated method stub
		if(cf.etcomments_obs3.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.3 */
		{
			cf.ShowToast("Please enter Question No.3 Comments.", 0);
			cf.etcomments_obs3.requestFocus();
		}
		else
		{
			if(rd_SoilErosionAroundFoundation.equals(""))/* CHECK QUESTION NO.3 RADIO BUTON*/
			{
				
				cf.ShowToast("Please select any answer for Question No.4", 0);
				cf.obs4optny.requestFocus();
			}
			else
			{
				if(rd_SoilErosionAroundFoundation.equals("Yes"))
				{
				 	 chk_Value4= cf.getselected_chk(cf.cb_obs4);
					 String chk_Value4other =(cf.cb_obs4[cf.cb_obs4.length-1].isChecked())? "^"+cf.etother_obs4.getText().toString():""; // append the other text value in to the selected option
					 chk_Value4+=chk_Value4other;
					 if(chk_Value4.equals(""))
					 {
						 
						 cf.ShowToast("Please select any checkbox for Question No.4 Locations." , 0);
					 }
					 else
					 {
						 if(cf.cb_obs4[cf.cb_obs4.length-1].isChecked())
						 {
							 if(chk_Value4other.trim().equals("^"))
							 {
								 cf.ShowToast("Please enter other text for Question No.4 Locations.", 0);
								 cf.etother_obs4.requestFocus();
							 }
							 else
							 {
								 chk_Value4 +=(cf.cb_obs4[cf.cb_obs4.length-1].isChecked())? "^"+cf.etother_obs4.getText().toString():""; // append the other text value in to the selected option
								 Chk_OBS4_Comments();	
							 }
						 }
						 else
						 {
							 Chk_OBS4_Comments();
						 }
					 }
				}
				else
				{
					Chk_OBS4_Comments();
				}
			}
		}
	}
	private void Chk_OBS4_Comments() {
		// TODO Auto-generated method stub
		if(cf.etcomments_obs4.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.4 */
		{
			cf.ShowToast("Please enter Question No.4 Comments.", 0);
			cf.etcomments_obs4.requestFocus();
		}
		else
		{
			if(rd_DrivewaysCracksNoted.equals(""))/* CHECK QUESTION NO.5 RADIO BUTON*/
			{
				
				cf.ShowToast("Please select any answer for Question No.5", 0);
				cf.obs5optny.requestFocus();
			}
			else
			{
				if(rd_DrivewaysCracksNoted.equals("Yes"))
				{
				 	 chk_Value5= cf.getselected_chk(cf.cb_obs5);
					 String chk_Value5other =(cf.cb_obs5[cf.cb_obs5.length-1].isChecked())? "^"+cf.etother_obs5.getText().toString():""; // append the other text value in to the selected option
					 chk_Value5+=chk_Value5other;
					 if(chk_Value5.equals(""))
					 {
						 
						 cf.ShowToast("Please select any checkbox for Question No.5 Locations." , 0);
					 }
					 else
					 {
						 if(cf.cb_obs5[cf.cb_obs5.length-1].isChecked())
						 {
							 if(chk_Value5other.trim().equals("^"))
							 {
								 cf.ShowToast("Please enter other text for Question No.5 Locations.", 0);
								 cf.etother_obs5.requestFocus();
							 }
							 else
							 {
								 chk_Value5 +=(cf.cb_obs5[cf.cb_obs5.length-1].isChecked())? "^"+cf.etother_obs5.getText().toString():""; // append the other text value in to the selected option
								 Chk_OBS5_Comments();	
							 }
						 }
						 else
						 {
							 Chk_OBS5_Comments();
						 }
					 }
				}
				else
				{
					Chk_OBS5_Comments();
				}
			}
		}
	}
	private void Chk_OBS5_Comments() {
		// TODO Auto-generated method stub
		if(cf.etcomments_obs5.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.5 */
		{
			cf.ShowToast("Please enter Question No.5 Comments.", 0);
			cf.etcomments_obs5.requestFocus();
		}
		else
		{
			if(rd_UpliftDrivewaysSurface.equals(""))/* CHECK QUESTION NO.6 RADIO BUTON*/
			{
				
				cf.ShowToast("Please select any answer for Question No.6", 0);
				cf.obs6opt1ny.requestFocus();
			}
			else
			{
				if(rd_UpliftDrivewaysSurface.equals("Yes"))
				{
				 	 chk_Value6= cf.getselected_chk(cf.cb_obs6);
					 String chk_Value6other =(cf.cb_obs6[cf.cb_obs6.length-1].isChecked())? "^"+cf.etother_obs6_opt1.getText().toString():""; // append the other text value in to the selected option
					 chk_Value6+=chk_Value6other;
					 if(chk_Value6.equals(""))
					 {
						 
						 cf.ShowToast("Please select any checkbox for Question No.6 Locations." , 0);
					 }
					 else
					 {
						 if(cf.cb_obs6[cf.cb_obs6.length-1].isChecked())
						 {
							 if(chk_Value6other.trim().equals("^"))
							 {
								 cf.ShowToast("Please enter other text for Question No.6 Locations.", 0);
								 cf.etother_obs6_opt1.requestFocus();
							 }
							 else
							 {
								 chk_Value6 +=(cf.cb_obs6[cf.cb_obs6.length-1].isChecked())? "^"+cf.etother_obs6_opt1.getText().toString():""; // append the other text value in to the selected option
								 Chk_OBS6_Comments();	
							 }
						 }
						 else
						 {
							 Chk_OBS6_Comments();
						 }
					 }
				}
				else
				{
					Chk_OBS6_Comments();
				}
			}
		}
	}
	private void Chk_OBS6_Comments() {

		// TODO Auto-generated method stub
		if(cf.etcomments_obs6_opt1.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.6 */
		{
			cf.ShowToast("Please enter Question No.6 Comments.", 0);
			cf.etcomments_obs6_opt1.requestFocus();
		}
		else
		{
			if(rd_LargeTree.equals(""))/* CHECK QUESTION NO.6(A) RADIO BUTON*/
			{
				
				cf.ShowToast("Please select any answer for Question No.6(A)", 0);
				cf.obs6optany.requestFocus();
			}
			else
			{
				if(rd_LargeTree.equals("Yes"))
				{
					/*try
					{
						Cursor OBS_save=cf.SelectTablefunction(cf.Observation1T, " where SH_OBS1T_SRID='"+cf.encode(cf.selectedhomeid)+"'");
						if(OBS_save.getCount()>0)
						{
							Chk_OBS6A_Comments();
						}
						else
						{
							cf.ShowToast("Please enter values in Large Tree for Question No.6(A)", 0);
							cf.etinch_obs6a.requestFocus();
						}
					}catch(Exception e)
					{
								String strerrorlog="Selection of the Observation1 Tree table not working ";
								cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					}*/
					Chk_OBS6A_Comments();
					
				}
				else
				{
					Chk_OBS6A_Comments();
				}
			}
		}
	}
	private void Chk_OBS6A_Comments() {

		// TODO Auto-generated method stub
		/*if(cf.etcomments_obs6_opta.getText().toString().trim().equals("")) CHECK COMMENTS QUESTION NO.6(A) 
		{
			cf.ShowToast("Please enter Question No.6(A) Comments.", 0);
			cf.etcomments_obs6_opta.requestFocus();
		}
		else
		{*/
			if(rd_CypressTree.equals(""))/* CHECK QUESTION NO.6(B) RADIO BUTON*/
			{
				
				cf.ShowToast("Please select any answer for Question No.6(B)", 0);
				cf.obs6optbny.requestFocus();
			}
			else
			{
				if(rd_CypressTree.equals("Yes"))
				{
				 	 chk_Value6B= cf.getselected_chk(cf.cb_obs6B);
					 String chk_Value6bother =(cf.cb_obs6B[cf.cb_obs6B.length-1].isChecked())? "^"+cf.etother_obs6_optb.getText().toString():""; // append the other text value in to the selected option
					 chk_Value6B+=chk_Value6bother;
					 if(chk_Value6B.equals(""))
					 {
						 
						 cf.ShowToast("Please select any checkbox for Question No.6(B) Locations." , 0);
					 }
					 else
					 {
						 if(cf.cb_obs6B[cf.cb_obs6B.length-1].isChecked())
						 {
							 if(chk_Value6bother.trim().equals("^"))
							 {
								 cf.ShowToast("Please enter other text for Question No.6(B) Locations.", 0);
								 cf.etother_obs6_optb.requestFocus();
							 }
							 else
							 {
								 chk_Value6B +=(cf.cb_obs6B[cf.cb_obs6B.length-1].isChecked())? "^"+cf.etother_obs6_optb.getText().toString():""; // append the other text value in to the selected option
								 Chk_OBS6B_Comments();	
							 }
						 }
						 else
						 {
							 Chk_OBS6B_Comments();
						 }
					 }
				}
				else
				{
					Chk_OBS6B_Comments();
				}
			}
		//}
	}
	private void Chk_OBS6B_Comments() {

		// TODO Auto-generated method stub
		if(cf.etcomments_obs6_optb.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.6(B) */
		{
			cf.ShowToast("Please enter Question No.6(B) Comments.", 0);
			cf.etcomments_obs6_optb.requestFocus();
		}
		else
		{
			if(rd_LargeTreesRemoved.equals(""))/* CHECK QUESTION NO.6(C) RADIO BUTON*/
			{
				
				cf.ShowToast("Please select any answer for Question No.6(C)", 0);
				cf.obs6optcny.requestFocus();
			}
			else
			{
				if(rd_LargeTreesRemoved.equals("Yes"))
				{
				 	 chk_Value6C= cf.getselected_chk(cf.cb_obs6C);
					 String chk_Value6cother =(cf.cb_obs6C[cf.cb_obs6C.length-1].isChecked())? "^"+cf.etother_obs6_optc.getText().toString():""; // append the other text value in to the selected option
					 chk_Value6C+=chk_Value6cother;
					 if(chk_Value6C.equals(""))
					 {
						 
						 cf.ShowToast("Please select any checkbox for Question No.6(C) Locations." , 0);
					 }
					 else
					 {
						 if(cf.cb_obs6C[cf.cb_obs6C.length-1].isChecked())
						 {
							 if(chk_Value6cother.trim().equals("^"))
							 {
								 cf.ShowToast("Please enter other text for Question No.6(C) Locations.", 0);
								 cf.etother_obs6_optc.requestFocus();
							 }
							 else
							 {
								 chk_Value6C +=(cf.cb_obs6C[cf.cb_obs6C.length-1].isChecked())? "^"+cf.etother_obs6_optc.getText().toString():""; // append the other text value in to the selected option
								 Chk_OBS6C_Comments();	
							 }
						 }
						 else
						 {
							 Chk_OBS6C_Comments();
						 }
					 }
				}
				else
				{
					Chk_OBS6C_Comments();
				}
			}
		}
	}
	private void Chk_OBS6C_Comments() {

		// TODO Auto-generated method stub
		if(cf.etcomments_obs6_optc.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.6(C) */
		{
			cf.ShowToast("Please enter Question No.6(C) Comments.", 0);
			cf.etcomments_obs6_optc.requestFocus();
		}
		else
		{
			if(rd_FoundationCrackNoted.equals(""))/* CHECK QUESTION NO.7 RADIO BUTON*/
		    {
			
			   cf.ShowToast("Please select any answer for Question No.7", 0);
			   cf.obs7opt1ny.requestFocus();
		    }
			else
			{
				if(rd_FoundationCrackNoted.equals("Yes"))
				{
					 chk_Value71= cf.getselected_chk(cf.cb_obs71);
					 String chk_Value71other =(cf.cb_obs71[cf.cb_obs71.length-1].isChecked())? "^"+cf.etother_obs7_opt1txty1.getText().toString():""; // append the other text value in to the selected option
					 chk_Value71+=chk_Value71other;
					 if(chk_Value71.equals(""))
					 {
						 
						 cf.ShowToast("Please select any checkbox for Question No.7 Types of cracks noted" , 0);
					 }
					 else
					 {
						 if(cf.cb_obs71[cf.cb_obs71.length-1].isChecked())
						 {
							 if(chk_Value71other.trim().equals("^"))
							 {
								 cf.ShowToast("Please enter other text for Question No.7 Types of cracks noted", 0);
								 cf.etother_obs7_opt1txty1.requestFocus();
							 }
							 else
							 {
								 chk_Value71 +=(cf.cb_obs71[cf.cb_obs71.length-1].isChecked())? "^"+cf.etother_obs7_opt1txty1.getText().toString():""; // append the other text value in to the selected option
								 chk_LocationOfCracks();	
							 }
						 }
						 else
						 {
							 chk_LocationOfCracks();
						 }
					 }
				}
				else
				{
					 Chk_Obs7_Comments();	
				}
			}
			
		}
	}
	private void chk_LocationOfCracks() {
		// TODO Auto-generated method stub
		 chk_Value72= cf.getselected_chk(cf.cb_obs72);
		 String chk_Value72other =(cf.cb_obs72[cf.cb_obs72.length-1].isChecked())? "^"+cf.etother_obs7_opt1txty2.getText().toString():""; // append the other text value in to the selected option
		 chk_Value72+=chk_Value72other;
		 if(chk_Value72.equals(""))
		 {
			 
			 cf.ShowToast("Please select any checkbox for Question No.10 Location of cracks." , 0);
		 }
		 else
		 {
			 if(cf.cb_obs72[cf.cb_obs72.length-1].isChecked())
			 {
				 if(chk_Value72other.trim().equals("^"))
				 {
					 cf.ShowToast("Please enter other text for Question No.7 Location of cracks.", 0);
					 cf.etother_obs7_opt1txty2.requestFocus();
				 }
				 else
				 {
					 chk_Value72 +=(cf.cb_obs72[cf.cb_obs72.length-1].isChecked())? "^"+cf.etother_obs7_opt1txty2.getText().toString():""; // append the other text value in to the selected option
					 chk_WidthOfCracks();	
				 }
			 }
			 else
			 {
				 chk_WidthOfCracks();
			 }
		 }
	}
	private void chk_WidthOfCracks() {
		// TODO Auto-generated method stub
		 chk_Value73= cf.getselected_chk(cf.cb_obs73);
		 String chk_Value73other =(cf.cb_obs73[cf.cb_obs73.length-1].isChecked())? "^"+cf.etother_obs7_opt1txty3.getText().toString():""; // append the other text value in to the selected option
		 chk_Value73+=chk_Value73other;
		 if(chk_Value73.equals(""))
		 {
			 
			 cf.ShowToast("Please select any checkbox for Question No.7 Approximate width of cracks." , 0);
		 }
		 else
		 {
			 if(cf.cb_obs73[cf.cb_obs73.length-1].isChecked())
			 {
				 if(chk_Value73other.trim().equals("^"))
				 {
					 cf.ShowToast("Please enter other text for Question No.7 Approximate width of cracks.", 0);
					 cf.etother_obs7_opt1txty3.requestFocus();
				 }
				 else
				 {
					 chk_Value73 +=(cf.cb_obs73[cf.cb_obs73.length-1].isChecked())? "^"+cf.etother_obs7_opt1txty3.getText().toString():""; // append the other text value in to the selected option
					 chk_LengthOfCracks();	
				 }
			 }
			 else
			 {
				 chk_LengthOfCracks();
			 }
		 }
	}
	private void chk_LengthOfCracks() {
		// TODO Auto-generated method stub
		if(cf.etlen_obs7.getText().toString().equals("")) /*CHECK LENGTH QUESTION NO.7 */
		{
			cf.ShowToast("Please enter Question No.7 Approximate Length of cracks.", 0);
			cf.etlen_obs7.requestFocus();
		}
		else
		{
			 chk_Value74= cf.getselected_chk(cf.cb_obs74);
			 if(chk_Value74.equals(""))
			 {
				 
				 cf.ShowToast("Please select any checkbox for Question No.7 Conditions of cracks noted." , 0);
			 }
			 else
			 {
				 chk_Value75= cf.getselected_chk(cf.cb_obs75);
				 String chk_Value75other =(cf.cb_obs75[cf.cb_obs75.length-1].isChecked())? "^"+cf.etother_obs7_txty4other.getText().toString():""; // append the other text value in to the selected option
				 chk_Value75+=chk_Value75other;
				 if(chk_Value75.equals(""))
				 {
					 
					 cf.ShowToast("Please select any checkbox for Question No.7 Probable cause." , 0);
				 }
				 else
				 {
					 if(cf.cb_obs75[cf.cb_obs75.length-1].isChecked())
					 {
						 if(chk_Value75other.trim().equals("^"))
						 {
							 cf.ShowToast("Please enter other text for Question No.7 Probable cause.", 0);
							 cf.etother_obs7_txty4other.requestFocus();
						 }
						 else
						 {
							 chk_Value75 +=(cf.cb_obs75[cf.cb_obs75.length-1].isChecked())? "^"+cf.etother_obs7_txty4other.getText().toString():""; // append the other text value in to the selected option
							 Chk_Obs7_Comments();	
						 }
					 }
					 else
					 {
						 Chk_Obs7_Comments();
					 }
				 }
			 }
		}
	}
	private void Chk_Obs7_Comments() {
		// TODO Auto-generated method stub
		if(cf.etcomments_obs7_opt1.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.7 */
		{
			cf.ShowToast("Please enter Question No.7 Comments", 0);
			cf.etcomments_obs7_opt1.requestFocus();
		}
		else
		{
			if(rd_RetainingWallsServiceable.equals(""))/* CHECK QUESTION NO.7(A) RADIO BUTON*/
		    {
			
			   cf.ShowToast("Please select any answer for Question No.7(A)", 0);
			   cf.obs7optnay.requestFocus();
		    }
			else
			{
				if(rd_RetainingWallsServiceable.equals("Yes"))
				{
					 chk_Value7A= cf.getselected_chk(cf.cb_obs7A);
					 if(chk_Value7A.equals(""))
					 {
						 
						 cf.ShowToast("Please select any checkbox for Question No.7(A) Locations." , 0);
					 }
					 else
					 {
						 Chk_Obs7A_Comments();
					 }
				}
				else
				{
					 Chk_Obs7A_Comments();	
				}
		   }
		}
	}
	private void Chk_Obs7A_Comments() {

		// TODO Auto-generated method stub
		if(cf.etcomments_obs7_opta.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.7(A) */
		{
			cf.ShowToast("Please enter Question No.7(A) Comments", 0);
			cf.etcomments_obs7_opta.requestFocus();
		}
		else
		{
			OBS1_Check();
		}
	}
	private void OBS1_Check()
	{
		Cursor OBS1T_save = null;
		try
		{
			 OBS1T_save=cf.SelectTablefunction(cf.Observation1T, " where SH_OBS1T_SRID='"+cf.encode(cf.selectedhomeid)+"'");
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		if(rd_LargeTree.equals("Yes"))
		{
			
				
			
			 chk_Value6A= cf.getselected_chk(cf.cb_obs6A);
			 String chk_Value6Aother1 =(cf.cb_obs6A[cf.cb_obs6A.length-1].isChecked())? cf.etother_obs6_opta.getText().toString():""; // append the other text value in to the selected option
			if(chk_Value6A.equals(""))
			 {
				 chk_Value6A+=chk_Value6Aother1;
			 }
			 else if(!chk_Value6Aother1.equals(""))
			 {
				 chk_Value6A+=","+chk_Value6Aother1;
			 }
			System.out.println("chk_Value6A="+chk_Value6A);
			if(!cf.etinch_obs6a.getText().toString().equals("")||!cf.etfeet_obs6a.getText().toString().equals("")||
					!chk_Value6A.equals(""))
			{
				 if(cf.etinch_obs6a.getText().toString().equals(""))
			       {
						cf.ShowToast("Please enter Approximate width of the tree for Question No.6(A)", 0);
						cf.etinch_obs6a.requestFocus();
					}
					else
					{
						if(cf.etfeet_obs6a.getText().toString().equals(""))
						{
							cf.ShowToast("Please enter Approximate height of the tree for Question No.6(A)", 0);
							cf.etfeet_obs6a.requestFocus();
						}
						else
						{
							 chk_Value6A= cf.getselected_chk(cf.cb_obs6A);
							 String chk_Value6Aother =(cf.cb_obs6A[cf.cb_obs6A.length-1].isChecked())? cf.etother_obs6_opta.getText().toString():""; // append the other text value in to the selected option
							 if(chk_Value6A.equals(""))
							 {
								 chk_Value6A+=chk_Value6Aother;
							 }
							 else if(!chk_Value6Aother.equals(""))
							 {
								 chk_Value6A+=","+chk_Value6Aother;
							 }
							 
							if(chk_Value6A.equals(""))
							 {
								 if(cf.cb_obs6A[cf.cb_obs6A.length-1].isChecked()){
									 cf.ShowToast("Please enter other text for Question No.6(A) Locations." , 0);
									 cf.etother_obs6_opta.requestFocus();
								 }
								 else{
								 cf.ShowToast("Please select any checkbox for Question No.6(A) Locations." , 0);}
							 }
							 else 
							 {
								 if(cf.cb_obs6A[cf.cb_obs6A.length-1].isChecked())
								 {
									 if(chk_Value6Aother.trim().equals(""))
									 {
										 cf.ShowToast("Please enter other text for Question No.6(A) Locations.", 0);
										 cf.etother_obs6_opta.requestFocus();
									 }
									 else
									 {
										 	cf.sh_db.execSQL("INSERT INTO "+cf.Observation1T+ "(SH_OBS1T_SRID,Width,Height,Location)" +
													"VALUES ('"+cf.encode(cf.selectedhomeid)+"','"+cf.encode(cf.etinch_obs6a.getText().toString())+"','"+cf.encode(cf.etfeet_obs6a.getText().toString())+"','"+cf.encode(chk_Value6A)+"')");
									        dbquery();
									        cf.etinch_obs6a.setText("");
										    cf.etfeet_obs6a.setText("");
											cf.Set_UncheckBox(cf.cb_obs6A,cf.etother_obs6_opta);
											OBS1_InsertOrUpdate();
									 }
								 }
								 else
								 {
										cf.sh_db.execSQL("INSERT INTO "+cf.Observation1T+ "(SH_OBS1T_SRID,Width,Height,Location)" +
												"VALUES ('"+cf.encode(cf.selectedhomeid)+"','"+cf.encode(cf.etinch_obs6a.getText().toString())+"','"+cf.encode(cf.etfeet_obs6a.getText().toString())+"','"+cf.encode(chk_Value6A)+"')");
								        dbquery();
								        cf.etinch_obs6a.setText("");
									    cf.etfeet_obs6a.setText("");
										cf.Set_UncheckBox(cf.cb_obs6A,cf.etother_obs6_opta);
										OBS1_InsertOrUpdate();
								 }
							 }
						}
	
	             }
			}
			else
			{
				 
				 if(OBS1T_save.getCount()!=0)
				 {
					 OBS1_InsertOrUpdate();
				 }
				 else
				 {
					 cf.ShowToast("Please enter mandatory for Large tree." , 0);
				 }
				//OBS1_InsertOrUpdate();
			}
			
		}
		else
		{
			OBS1_InsertOrUpdate();
		}
	}
	private void OBS1_InsertOrUpdate() {
		// TODO Auto-generated method stub
		
				      
		
			try
			{
				Cursor OBS1_save=cf.SelectTablefunction(cf.Observation1, " where SH_OBS1_SRID='"+cf.encode(cf.selectedhomeid)+"'");
			 	if(OBS1_save.getCount()>0)
				{
					/* UPDATE THE OBSERVATION1 TABLE*/
			 		cf.sh_db.execSQL("UPDATE "+cf.Observation1+ " SET IrregularLandSurface='"+cf.encode(rd_IrregularLandSurface)+"',"
			 				+"IrregularLandSurfaceLocation='"+cf.encode(chk_Value1)+"',IrregularLandSurfaceComments='"+cf.encode(cf.etcomments_obs1_opt1.getText().toString())+"',"
			 				+"VisibleBuriedDebris='"+cf.encode(rd_VisibleBuriedDebris)+"',VisibleBuriedDebrisLocation='"+cf.encode(chk_Value1A)+"',"
			 				+"VisibleBuriedDebrisComments='"+cf.encode(cf.etcomments_obs1_opta.getText().toString())+"',IrregularLandSurfaceAdjacent='"+cf.encode(rd_IrregularLandSurfaceAdjacent)+"',"
			 				+"IrregularLandSurfaceAdjacentLocation='"+cf.encode(chk_Value2)+"',IrregularLandSurfaceAdjacentComments='"+cf.encode(cf.etcomments_obs2.getText().toString())+"',"
			 				+"SoilCollapse='"+cf.encode(rd_SoilCollapse)+"',SoilCollapseLocation='"+cf.encode(chk_Value3)+"',"
			 				+"SoilCollapseComments='"+cf.encode(cf.etcomments_obs3.getText().toString())+"',SoilErosionAroundFoundation='"+cf.encode(rd_SoilErosionAroundFoundation)+"',"
			 				+"SoilErosionAroundFoundationOption='"+cf.encode(chk_Value4)+"',SoilErosionAroundFoundationComments='"+cf.encode(cf.etcomments_obs4.getText().toString())+"',"
			 				+"DrivewaysCracksNoted='"+cf.encode(rd_DrivewaysCracksNoted)+"',DrivewaysCracksOption='"+cf.encode(chk_Value5)+"',"
			 				+"DrivewaysCracksComments='"+cf.encode(cf.etcomments_obs5.getText().toString())+"',UpliftDrivewaysSurface='"+cf.encode(rd_UpliftDrivewaysSurface)+"',"
			 				+"UpliftDrivewaysSurfaceOption='"+cf.encode(chk_Value6)+"',UpliftDrivewaysSurfaceComments='"+cf.encode(cf.etcomments_obs6_opt1.getText().toString())+"',"
			 				+"LargeTree='"+cf.encode(rd_LargeTree)+"',LargeTreeComments='"+cf.encode(cf.etcomments_obs6_opta.getText().toString())+"',"
			 				+"CypressTree='"+cf.encode(rd_CypressTree)+"',CypressTreeLocation='"+cf.encode(chk_Value6B)+"',"
			 				+"CypressTreeComments='"+cf.encode(cf.etcomments_obs6_optb.getText().toString())+"',LargeTreesRemoved='"+cf.encode(rd_LargeTreesRemoved)+"',"
			 				+"LargeTreesRemovedLocation='"+cf.encode(chk_Value6C)+"',LargeTreesRemovedComments='"+cf.encode(cf.etcomments_obs6_optc.getText().toString())+"',"
			 				+"FoundationCrackNoted='"+cf.encode(rd_FoundationCrackNoted)+"',FoundationTypeofCrack='"+cf.encode(chk_Value71)+"',"
			 				+"FoundationLocation='"+cf.encode(chk_Value72)+"',FoundationWidthofCrack ='"+cf.encode(chk_Value73)+"',"
			 				+"FoundationLengthofCrack='"+cf.encode(cf.etlen_obs7.getText().toString())+"',FoundationCleanliness='"+cf.encode(chk_Value74)+"',"
			 				+"FoundationProbableCause='"+cf.encode(chk_Value75)+"',FoundationComments='"+cf.encode(cf.etcomments_obs7_opta.getText().toString())+"',"
			 				+"RetainingWallsServiceable='"+cf.encode(rd_RetainingWallsServiceable)+"',RetainingWallsServiceOption='"+cf.encode(chk_Value7A)+"',"
			 				+"RetainingWallsServiceComments='"+cf.encode(cf.etcomments_obs7_opta.getText().toString())+"' where SH_OBS1_SRID='"+cf.encode(cf.selectedhomeid)+"'");
			 		cf.ShowToast("Observation 1-7 has been updated sucessfully.", 1);
					nextlayout();
				}
			 	else
				{
					/* INSERT INTO OBSERVATION1 TABLE*/
			 		cf.sh_db.execSQL("INSERT INTO "+cf.Observation1+ "(SH_OBS1_SRID,IrregularLandSurface,IrregularLandSurfaceLocation,IrregularLandSurfaceComments,VisibleBuriedDebris,VisibleBuriedDebrisLocation,VisibleBuriedDebrisComments,IrregularLandSurfaceAdjacent,IrregularLandSurfaceAdjacentLocation,IrregularLandSurfaceAdjacentComments,SoilCollapse,SoilCollapseLocation,SoilCollapseComments,SoilErosionAroundFoundation,SoilErosionAroundFoundationOption,SoilErosionAroundFoundationComments,DrivewaysCracksNoted,DrivewaysCracksOption,DrivewaysCracksComments,UpliftDrivewaysSurface,UpliftDrivewaysSurfaceOption,UpliftDrivewaysSurfaceComments,LargeTree,LargeTreeComments,CypressTree,CypressTreeLocation,CypressTreeComments,LargeTreesRemoved,LargeTreesRemovedLocation,LargeTreesRemovedComments,FoundationCrackNoted,FoundationTypeofCrack,FoundationLocation,FoundationWidthofCrack ,FoundationLengthofCrack,FoundationCleanliness,FoundationProbableCause,FoundationComments,RetainingWallsServiceable,RetainingWallsServiceOption,RetainingWallsServiceComments)" +
			 						 											"VALUES ('"+cf.encode(cf.selectedhomeid)+"','"+cf.encode(rd_IrregularLandSurface)+"','"+cf.encode(chk_Value1)+"','"+cf.encode(cf.etcomments_obs1_opt1.getText().toString())+"','"+cf.encode(rd_VisibleBuriedDebris)+"','"+cf.encode(chk_Value1A)+"','"+cf.encode(cf.etcomments_obs1_opta.getText().toString())+"','"+cf.encode(rd_IrregularLandSurfaceAdjacent)+"','"+cf.encode(chk_Value2)+"','"+cf.encode(cf.etcomments_obs2.getText().toString())+"','"+cf.encode(rd_SoilCollapse)+"','"+cf.encode(chk_Value3)+"','"+cf.encode(cf.etcomments_obs3.getText().toString())+"','"+cf.encode(rd_SoilErosionAroundFoundation)+"','"+cf.encode(chk_Value4)+"','"+cf.encode(cf.etcomments_obs4.getText().toString())+"','"+cf.encode(rd_DrivewaysCracksNoted)+"','"+cf.encode(chk_Value5)+"','"+cf.encode(cf.etcomments_obs5.getText().toString())+"','"+cf.encode(rd_UpliftDrivewaysSurface)+"','"+cf.encode(chk_Value6)+"','"+cf.encode(cf.etcomments_obs6_opt1.getText().toString())+"','"+cf.encode(rd_LargeTree)+"','"+cf.encode(cf.etcomments_obs6_opta.getText().toString())+"','"+cf.encode(rd_CypressTree)+"','"+cf.encode(chk_Value6B)+"','"+cf.encode(cf.etcomments_obs6_optb.getText().toString())+"','"+cf.encode(rd_LargeTreesRemoved)+"','"+cf.encode(chk_Value6C)+"','"+cf.encode(cf.etcomments_obs6_optc.getText().toString())+"','"+cf.encode(rd_FoundationCrackNoted)+"','"+cf.encode(chk_Value71)+"','"+cf.encode(chk_Value72)+"','"+cf.encode(chk_Value73)+"','"+cf.encode(cf.etlen_obs7.getText().toString())+"','"+cf.encode(chk_Value74)+"','"+cf.encode(chk_Value75)+"','"+cf.encode(cf.etcomments_obs7_opta.getText().toString())+"','"+cf.encode(rd_RetainingWallsServiceable)+"','"+cf.encode(chk_Value7A)+"','"+cf.encode(cf.etcomments_obs7_opta.getText().toString())+"')");
	
			 		cf.ShowToast("Observation 1-7 has been saved sucessfully.", 1);
					nextlayout();
				}
			}
			   catch(Exception e)
				{
					String strerrorlog="Selection of the Observation1 table not working ";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			
				}
		
	}
	private void nextlayout() {
		// TODO Auto-generated method stub
		Intent intimg = new Intent(Observation1.this,
				Observation2.class);
		intimg.putExtra("homeid", cf.selectedhomeid);
	    intimg.putExtra("InspectionType", cf.onlinspectionid);
		intimg.putExtra("status", cf.onlstatus);
	    startActivity(intimg);
	}
	public void obs_tablelayout_hide() {
		// TODO Auto-generated method stub
		cf.tbl_obs1.setVisibility(cf.show.GONE);
		cf.tbl_obs2.setVisibility(cf.show.GONE);
		cf.tbl_obs3.setVisibility(cf.show.GONE);
		cf.tbl_obs4.setVisibility(cf.show.GONE);
		cf.tbl_obs5.setVisibility(cf.show.GONE);
		cf.tbl_obs6.setVisibility(cf.show.GONE);
		cf.tbl_obs7.setVisibility(cf.show.GONE);
		cf.tbl_row_obs1.setBackgroundResource(R.drawable.backrepeatnor);
		cf.tbl_row_obs2.setBackgroundResource(R.drawable.backrepeatnorobs);
		cf.tbl_row_obs3.setBackgroundResource(R.drawable.backrepeatnor);
		cf.tbl_row_obs4.setBackgroundResource(R.drawable.backrepeatnor);
		cf.tbl_row_obs5.setBackgroundResource(R.drawable.backrepeatnor);
		cf.tbl_row_obs6.setBackgroundResource(R.drawable.backrepeatnor);
		cf.tbl_row_obs7.setBackgroundResource(R.drawable.backrepeatnor);
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(cf.strschdate.equals("")){
				cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
			}else{
			Intent  myintent = new Intent(Observation1.this,BuildingInformation.class);
			cf.putExtras(myintent);
			startActivity(myintent);}
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {

		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			switch (resultCode) {
 			case 0:
 				break;
 			case -1:
 				try {

 					String[] projection = { MediaStore.Images.Media.DATA };
 					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
 							null, null, null);
 					int column_index_data = cursor
 							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
 					cursor.moveToFirst();
 					String capturedImageFilePath = cursor.getString(column_index_data);
 					cf.showselectedimage(capturedImageFilePath);
 				} catch (Exception e) {
 					
 				}
 				
 				break;

 		}

 	}

	
	
}

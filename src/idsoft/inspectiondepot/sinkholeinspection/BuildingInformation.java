package idsoft.inspectiondepot.sinkholeinspection;

import idsoft.inspectiondepot.sinkholeinspection.SummaryQuest.Touch_Listener;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

public class BuildingInformation extends Activity {
	CommonFunctions cf;
public String SH_BI_LBC_BD ,SH_BI_LBC_PF,SH_BI_LBC_PAI,SH_BI_LBC_PAIO,SH_BI_GBI_BT,SH_BI_GBI_YOC,SH_BI_GBI_YOCO,SH_BI_GBI_BS,SH_BI_GBI_FW,SH_BI_GBI_FR,SH_BI_NofStories,SH_BI_WSC_URM,SH_BI_WSC_RM,SH_BI_WSC_RC,SH_BI_WSC_WMF,SH_BI_WSC_TNBR,SH_BI_WSC_TNEWO,SH_BI_WSC_TNEDO = "" ,SH_BI_WC_AS,SH_BI_WC_VS,SH_BI_WC_WS,SH_BI_WC_other,SH_BI_WC_Stucco,SH_BI_WC_BV,SH_BI_WC_PB,SH_BI_AD_cmd;
public ArrayAdapter adapter1,adapter2,adapter3;
String strhomeid;
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    cf = new CommonFunctions(this);
    Bundle extras = getIntent().getExtras();
	if (extras != null) {
		cf.HomeId=cf.selectedhomeid = strhomeid= extras.getString("homeid");
		cf.onlinspectionid = extras.getString("InspectionType");
	    cf.onlstatus = extras.getString("status");
 	}
    setContentView(R.layout.buildinginformation);
    cf.getInspectorId();
    cf.getDeviceDimensions();
    LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
    mainmenu_layout.setMinimumWidth(cf.wd);
    ScrollView scr=(ScrollView)findViewById(R.id.scr);
    scr.setMinimumHeight(cf.ht);
      scr.smoothScrollTo(0,0);
	  HorizontalScrollView hscr=(HorizontalScrollView)findViewById(R.id.HorizontalScrollView01);
	   hscr.smoothScrollTo(0,0);
    mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,cf));
     setObjectsToView();
    
     cf.BI_LBC_txt3_PAI.setOnTouchListener(new Touch_Listener(1));
     cf.BI_GBI_BS.setOnTouchListener(new Touch_Listener(2));
    /*set the listener for the object */
    cf.BI_LBC_lin.setOnClickListener(new BI_clicker());
    cf.BI_GBI_lin.setOnClickListener(new BI_clicker());
    cf.BI_WSC_lin.setOnClickListener(new BI_clicker());
    cf.BI_AC_lin.setOnClickListener(new BI_clicker());
    cf.BI_savenext.setOnClickListener(new BI_clicker());
    cf.BI_GBI_txt2_sp1.setOnItemSelectedListener(new spinerlisner());
    cf.WSI_N_S.setOnItemSelectedListener(new spinerlisner());
    
    /* addin value to the spinner */
    
   cf.Create_Table(5); 
   
   /* set the value if availabel mean in the respective fields */
   SQ_setValue();
   /* set the value if availabel mean in the respective fields */
    
}
class Touch_Listener implements OnTouchListener
{
	   public int type;
	   Touch_Listener(int type)
		{
			this.type=type;
			
		}
	    @Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
	    	if(this.type==1)
			{
	    		cf.setFocus(cf.BI_LBC_txt3_PAI);
			}
	    	else if(this.type==2)
	    	{
	    		cf.setFocus(cf.BI_GBI_BS);
	    	}
	    	
			return false;
		}
	 
}
private void SQ_setValue() {
	// TODO Auto-generated method stub
	cf.BI_AC_ed1.addTextChangedListener(new BI_textwatcher());
	Cursor BI_retrive;
	   try
		{
			 BI_retrive=cf.SelectTablefunction(cf.BIQ_table, " where SH_SQ_Homeid='"+cf.encode(cf.selectedhomeid)+"'");
			 if(BI_retrive.getCount()>0)
			 {  
				 BI_retrive.moveToFirst();
				 SH_BI_LBC_BD=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_LBC_BD")));
				 SH_BI_LBC_PF=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_LBC_PF")));
				 SH_BI_LBC_PAI=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_LBC_PAI")));
				 SH_BI_LBC_PAIO=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_LBC_PAIO")));
				 SH_BI_GBI_BT=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_GBI_BT")));
				 SH_BI_GBI_YOC=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_GBI_YOC")));
				 SH_BI_GBI_YOCO=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_GBI_YOCO")));
				 SH_BI_GBI_BS=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_GBI_BS")));
				 SH_BI_GBI_FW=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_GBI_FW")));
				 SH_BI_GBI_FR=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_GBI_FR")));
				 SH_BI_NofStories=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_NofStories")));
				 SH_BI_WSC_URM=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WSC_URM")));
				 SH_BI_WSC_RM=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WSC_RM")));
				 SH_BI_WSC_RC=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WSC_RC")));
				 SH_BI_WSC_WMF=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WSC_WMF")));
				 SH_BI_WSC_TNBR=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WSC_TNBR")));
				 SH_BI_WSC_TNEWO=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WSC_TNEWO")));
				 SH_BI_WSC_TNEDO =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WSC_TNEDO")));
				 SH_BI_WC_AS=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WC_AS")));
				 SH_BI_WC_VS=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WC_VS")));
				 SH_BI_WC_WS=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WC_WS")));
				 SH_BI_WC_other=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WC_other")));
				 SH_BI_WC_Stucco=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WC_Stucco")));
				 SH_BI_WC_BV=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WC_BV")));
				 SH_BI_WC_PB=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_WC_PB")));
				 SH_BI_AD_cmd=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("SH_BI_AD_cmd")));
				 
								 
				 cf.setvalueradio(SH_BI_LBC_BD,cf.BI_LBC_txt1_opt,cf.BI_LBC_txt2_other); // BI_LBC_txt2_other this send for the dummy purpose only  
				 cf.setvaluechk(SH_BI_LBC_PF,cf.BI_LBC_txt2_opt,cf.BI_LBC_txt2_other);
				 cf.BI_LBC_txt3_PAI.setText(SH_BI_LBC_PAI);
				 cf.setvalueradio(SH_BI_LBC_PAIO,cf.BI_LBC_txt3_opt,cf.BI_LBC_txt3_other);
				 cf.BI_LBC_txt3_other.setVisibility(View.GONE);
				 cf.setvaluechk(SH_BI_GBI_BT, cf.BI_GBI_txt1_opt,cf.BI_GBI_txt1_other);
				 
				 if(getFromArray(SH_BI_GBI_YOC,cf.BI_GBI_year))
				 {
					 cf.BI_GBI_txt2_sp1.setSelection(adapter1.getPosition("Other"));
					 cf.BI_GBI_spin1_other.setText(SH_BI_GBI_YOC); 
				 }
				 else
				 {
					
					 cf.BI_GBI_txt2_sp1.setSelection(adapter1.getPosition(SH_BI_GBI_YOC));
				 }
				 
				 cf.setvalueradio(SH_BI_GBI_YOCO, cf.BI_GBI_txt2_opt,cf.BI_GBI_txt2_other);
				 cf.BI_GBI_BS.setText(SH_BI_GBI_BS);
				 cf.setvaluechk(SH_BI_GBI_FW, cf.BI_GBI_txt3_opt,cf.BI_GBI_txt3_other);
				 cf.setvaluechk(SH_BI_GBI_FR, cf.BI_GBI_txt4_opt,cf.BI_GBI_txt3_other);
				 cf.WSI_N_S.setSelection(adapter3.getPosition(SH_BI_NofStories));
				 cf.setvaluechk(SH_BI_WSC_URM, cf.BI_WSC_txt1_opt,cf.BI_GBI_txt3_other);
				 cf.setvaluechk(SH_BI_WSC_RM, cf.BI_WSC_txt2_opt,cf.BI_GBI_txt3_other);
				 cf.setvaluechk(SH_BI_WSC_RC, cf.BI_WSC_txt3_opt,cf.BI_GBI_txt3_other);
				 cf.setvaluechk(SH_BI_WSC_WMF, cf.BI_WSC_txt4_opt,cf.BI_GBI_txt3_other);
				 cf.WSC_sp1.setSelection(adapter2.getPosition(SH_BI_WSC_TNBR)); // set the spiner value by it postion
				 cf.BI_WSC_TNEWO.setText(SH_BI_WSC_TNEWO);
				 cf.BI_WSC_TNEDO.setText(SH_BI_WSC_TNEDO);
				 cf.setvaluechk(SH_BI_WC_AS, cf.BI_WC_txt1_opt,cf.BI_GBI_txt3_other);
				 cf.setvaluechk(SH_BI_WC_Stucco, cf.BI_WC_txt2_opt,cf.BI_GBI_txt3_other);
				 cf.setvaluechk(SH_BI_WC_VS, cf.BI_WC_txt3_opt,cf.BI_GBI_txt3_other);
				 cf.setvaluechk(SH_BI_WC_BV, cf.BI_WC_txt4_opt,cf.BI_GBI_txt3_other);
				 cf.setvaluechk(SH_BI_WC_WS, cf.BI_WC_txt5_opt,cf.BI_GBI_txt3_other);
				 cf.setvaluechk(SH_BI_WC_PB, cf.BI_WC_txt6_opt,cf.BI_GBI_txt3_other);
				 cf.setvaluechk(SH_BI_WC_other, cf.BI_WC_txt7_opt,cf.BI_GBI_txt3_other);
				 cf.BI_AC_ed1.setText(SH_BI_AD_cmd);
				 cf.showing_limit(SH_BI_AD_cmd,cf.BI_ED_parrant,cf.BI_ED,cf.BI_ED_TV); 
				 
			 }
			 else
			 {
				 /** Auto population for the year of home **/
				 Cursor c2=cf.SelectTablefunction(cf.policyholder, " where SH_PH_SRID='"+cf.selectedhomeid+"'");
				 if(c2.getCount()>0)
				 { c2.moveToFirst();
					 String Y_O_C=c2.getString(c2.getColumnIndex("SH_YearBuilt"));
					 if(!Y_O_C.trim().equals(""))
						{
							if(getFromArray(Y_O_C,cf.BI_GBI_year))
							 {
								cf.BI_GBI_txt2_sp1.setSelection(adapter1.getPosition("Other"));
								cf.BI_GBI_spin1_other.setText(Y_O_C); 
							 }
							 else
							 {
								
								 cf.BI_GBI_txt2_sp1.setSelection(adapter1.getPosition(Y_O_C));
							 }
						}	 
				 }
			 }
		}
	catch (Exception E)
	{
		String strerrorlog="Selection of the Building information   table not working ";
		cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
	}
}
  
private void ArrayListenerClickCheck(CheckBox[] bI_click) {
	// TODO Auto-generated method stub
	for(int i=0;i<bI_click.length;i++)
	{
		bI_click[i].setOnClickListener(new BI_clicker());
	}
}
private void ArrayListenerClickradio(RadioButton[] bI_click) {
	// TODO Auto-generated method stub
	for(int i=0;i<bI_click.length;i++)
	{
		bI_click[i].setOnClickListener(new BI_clicker());
	}
}
private void ArrayListener(RadioButton[] bI_LBC_txt1_opt) {
	// TODO Auto-generated method stub
	for(int i=0;i<bI_LBC_txt1_opt.length;i++)
	{
		bI_LBC_txt1_opt[i].setOnClickListener(new RadioArrayclicker());
	}
}
private void enableCheckBox(CheckBox[] BI_chk) {
	// TODO Auto-generated method stub
	for(int i=1;i<BI_chk.length;i++)
	{
		BI_chk[i].setEnabled(true);
	}
	
}
private void disableCheckBox(CheckBox[] BI_chk) {
	// TODO Auto-generated method stub
	for(int i=1;i<BI_chk.length;i++)
	{
		BI_chk[i].setEnabled(false);
		BI_chk[i].setChecked(false);
	}
	
}
private void enableCheckBox_all(CheckBox[] BI_chk) {
	// TODO Auto-generated method stub
	for(int i=0;i<BI_chk.length;i++)
	{
		BI_chk[i].setEnabled(true);
	}
	
}
private void disableCheckBox_all(CheckBox[] BI_chk) {
	// TODO Auto-generated method stub
	for(int i=0;i<BI_chk.length;i++)
	{
		BI_chk[i].setEnabled(false);
		BI_chk[i].setChecked(false);
	}
	
}
public void healp_click(View v)
{
	switch (v.getId())	
	{
	case R.id.BI_LBC_help:
	  cf.alerttitle="HELP";
	  cf.alertcontent="Inspectors should properly identify the orientation of the home or building being inspected and if any part of the property fronts or touches a water source.  Inspectors need to identify the proximity of the water source in comparison with the foundation within the Comment area of this section.";
	  cf.showhelp(cf.alerttitle,cf.alertcontent);
	  break;
	case R.id.BI_GBI_help:
		  cf.alerttitle="HELP";
		  cf.alertcontent="It is important for Inspectors to determine the correct construction type and conditions noted as this information is used by quality assurance oversight staff when analyzing the extent of any settlement noted within the report.  Quality Assurance staff will be looking at anticipated dead loads based on the material selected, when attempting to understand the inspector�s submitted report.  For example, masonry wall and concrete structures are heavier in nature than frame structures and are more susceptible to initial settlement.  However, frame structures are potentially more susceptible to shrinkage settlement or moisture intrusion cracking.";
		  cf.showhelp(cf.alerttitle,cf.alertcontent);
	break;
	case R.id.BI_WSC_help:
		  cf.alerttitle="HELP";
		  cf.alertcontent="Inspectors must verify the type of exterior wall structure and clearly outline which floor the wall structure belongs too.  Photographs of the verification process and comments must be provided such as wall cladding";
		  cf.showhelp(cf.alerttitle,cf.alertcontent);
	break;
	case R.id.BI_WC_help:
		  cf.alerttitle="HELP";
		  cf.alertcontent="Inspectors much clearly outline the various exterior finishes � cladding materials used in the construction of the home or building being inspected.  A clear outline of the different materials used must also be made.</p><p>During the inspection the inspector should pay close attention to any displacement, settlement or evidence of pull out and provide proper documentation/back up information.";
		  cf.showhelp(cf.alerttitle,cf.alertcontent);
	break;
	  
	}
}

class spinerlisner implements OnItemSelectedListener
{

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		String s;
		switch (arg0.getId())
		{
		
		 case R.id.BI_GBI_txt2_sp1:
			  s=cf.BI_GBI_txt2_sp1.getSelectedItem().toString();
			 if(s.equals("Other"))
			 {
				 cf.BI_GBI_spin1_other.setVisibility(arg1.VISIBLE);
			 }
			 else
			 {
				 cf.BI_GBI_spin1_other.setVisibility(arg1.GONE);
			 }
				 
		 break;
		 case R.id.WSC_N_S:
			  s=cf.WSI_N_S.getSelectedItem().toString();
			  if(s.equals("Select"))
			  {
				  	 disableCheckBox_all(cf.BI_WSC_txt1_opt);
					 disableCheckBox_all(cf.BI_WSC_txt2_opt);
					 disableCheckBox_all(cf.BI_WSC_txt3_opt);
					 disableCheckBox_all(cf.BI_WSC_txt4_opt);
					 
					 disableCheckBox_all(cf.BI_WC_txt1_opt);
					 disableCheckBox_all(cf.BI_WC_txt2_opt);
					 disableCheckBox_all(cf.BI_WC_txt3_opt);
					 disableCheckBox_all(cf.BI_WC_txt4_opt);
					 disableCheckBox_all(cf.BI_WC_txt5_opt);
					 disableCheckBox_all(cf.BI_WC_txt6_opt);
					 disableCheckBox_all(cf.BI_WC_txt7_opt); 
			  }
			  else if(s.equals("1 Storey"))
			 {
					
				  /** First enable alla then disable the respective check box only **/  
				  	 enableCheckBox_all(cf.BI_WSC_txt1_opt);
					 enableCheckBox_all(cf.BI_WSC_txt2_opt);
					 enableCheckBox_all(cf.BI_WSC_txt3_opt);
					 enableCheckBox_all(cf.BI_WSC_txt4_opt);
					 
					 enableCheckBox_all(cf.BI_WC_txt1_opt);
					 enableCheckBox_all(cf.BI_WC_txt2_opt);
					 enableCheckBox_all(cf.BI_WC_txt3_opt);
					 enableCheckBox_all(cf.BI_WC_txt4_opt);
					 enableCheckBox_all(cf.BI_WC_txt5_opt);
					 enableCheckBox_all(cf.BI_WC_txt6_opt);
					 enableCheckBox_all(cf.BI_WC_txt7_opt);
					 /** First enable alla then disable the respective check box only Ends **/
					 
				 disableCheckBox(cf.BI_WSC_txt1_opt);
				 disableCheckBox(cf.BI_WSC_txt2_opt);
				 disableCheckBox(cf.BI_WSC_txt3_opt);
				 disableCheckBox(cf.BI_WSC_txt4_opt);
				 
				 disableCheckBox(cf.BI_WC_txt1_opt);
				 disableCheckBox(cf.BI_WC_txt2_opt);
				 disableCheckBox(cf.BI_WC_txt3_opt);
				 disableCheckBox(cf.BI_WC_txt4_opt);
				 disableCheckBox(cf.BI_WC_txt5_opt);
				 disableCheckBox(cf.BI_WC_txt6_opt);
				 disableCheckBox(cf.BI_WC_txt7_opt);
			 }
			 else
			 {
				  /** enable allcheck boxes in the array**/  
			  	 enableCheckBox_all(cf.BI_WSC_txt1_opt);
				 enableCheckBox_all(cf.BI_WSC_txt2_opt);
				 enableCheckBox_all(cf.BI_WSC_txt3_opt);
				 enableCheckBox_all(cf.BI_WSC_txt4_opt);
				 
				 enableCheckBox_all(cf.BI_WC_txt1_opt);
				 enableCheckBox_all(cf.BI_WC_txt2_opt);
				 enableCheckBox_all(cf.BI_WC_txt3_opt);
				 enableCheckBox_all(cf.BI_WC_txt4_opt);
				 enableCheckBox_all(cf.BI_WC_txt5_opt);
				 enableCheckBox_all(cf.BI_WC_txt6_opt);
				 enableCheckBox_all(cf.BI_WC_txt7_opt);
				 /** enable allcheck boxes in the array Ends**/
				 
			 }
		 break;
		}
		 
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		 cf.BI_GBI_spin1_other.setVisibility(arg0.GONE);
		
	}
	
}
class BI_clicker implements OnClickListener
{

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	switch (v.getId())	
	{
		case R.id.BI_LBC_lin:
			cf.BI_LBC_table1.setVisibility(v.VISIBLE);
			cf.BI_GBI_table1.setVisibility(v.GONE);
			cf.BI_WSC_rel1.setVisibility(v.GONE);
			cf.BI_AC_lin2.setVisibility(v.GONE);
			
			cf.BI_LBC_lin.setBackgroundResource(R.drawable.subbackrepeatnor);
			cf.BI_GBI_lin.setBackgroundResource(R.drawable.backrepeatnor);
			cf.BI_WSC_lin.setBackgroundResource(R.drawable.backrepeatnor);
			cf.BI_AC_lin.setBackgroundResource(R.drawable.backrepeatnor);
		break;
		case R.id.BI_GBI_lin:
			cf.BI_LBC_table1.setVisibility(v.GONE);
			cf.BI_GBI_table1.setVisibility(v.VISIBLE);
			cf.BI_WSC_rel1.setVisibility(v.GONE);
			cf.BI_AC_lin2.setVisibility(v.GONE);
			
			cf.BI_GBI_lin.setBackgroundResource(R.drawable.subbackrepeatnor);
			cf.BI_LBC_lin.setBackgroundResource(R.drawable.backrepeatnor);
			cf.BI_WSC_lin.setBackgroundResource(R.drawable.backrepeatnor);
			cf.BI_AC_lin.setBackgroundResource(R.drawable.backrepeatnor);
		break;
		case R.id.BI_WSC_lin:
			cf.BI_LBC_table1.setVisibility(v.GONE);
			cf.BI_GBI_table1.setVisibility(v.GONE);
			cf.BI_WSC_rel1.setVisibility(v.VISIBLE);
			cf.BI_AC_lin2.setVisibility(v.GONE);
			
			cf.BI_WSC_lin.setBackgroundResource(R.drawable.subbackrepeatnor);
			cf.BI_LBC_lin.setBackgroundResource(R.drawable.backrepeatnor);
			cf.BI_GBI_lin.setBackgroundResource(R.drawable.backrepeatnor);
			cf.BI_AC_lin.setBackgroundResource(R.drawable.backrepeatnor);
		break;
		case R.id.BI_AC_lin:
			cf.BI_LBC_table1.setVisibility(v.GONE);
			cf.BI_GBI_table1.setVisibility(v.GONE);
			cf.BI_WSC_rel1.setVisibility(v.GONE);
			cf.BI_AC_lin2.setVisibility(v.VISIBLE);
			
			cf.BI_AC_lin.setBackgroundResource(R.drawable.subbackrepeatnor);
			cf.BI_LBC_lin.setBackgroundResource(R.drawable.backrepeatnor);
			cf.BI_WSC_lin.setBackgroundResource(R.drawable.backrepeatnor);
			cf.BI_GBI_lin.setBackgroundResource(R.drawable.backrepeatnor);
		break;
		case R.id.BI_LBC_txt2_opt9:
			if(cf.BI_LBC_txt2_opt[8].isChecked())
			{
				//cf.BI_LBC_txt2_opt[8].setChecked(true);
				cf.BI_LBC_txt2_other.setVisibility(v.VISIBLE);
			}
			else
			{
				//cf.BI_LBC_txt2_opt[8].setChecked(false);
				cf.BI_LBC_txt2_other.setVisibility(v.GONE);
				
			}
		break;
		case R.id.BI_LBC_txt3_opt1:
			
				cf.BI_LBC_txt3_other.setVisibility(v.GONE);
			
			
		break;
		case R.id.BI_LBC_txt3_opt2:
			
					//cf.BI_LBC_txt2_opt[8].setChecked(true);
					cf.BI_LBC_txt3_other.setVisibility(v.GONE);
				
		break;
		case R.id.BI_LBC_txt3_opt3:
			if(cf.BI_LBC_txt3_opt[2].isChecked())
			{
				//cf.BI_LBC_txt2_opt[8].setChecked(true);
				//cf.BI_LBC_txt3_other.setVisibility(v.VISIBLE);
			}
			else
			{
				//cf.BI_LBC_txt2_opt[8].setChecked(false);
				cf.BI_LBC_txt3_other.setVisibility(v.GONE);
				
			}
		break;
		case R.id.BI_GBI_txt1_opt7:
			if(cf.BI_GBI_txt1_opt[6].isChecked())
			{
				//cf.BI_LBC_txt2_opt[8].setChecked(true);
				cf.BI_GBI_txt1_other.setVisibility(v.VISIBLE);
			}
			else
			{
				//cf.BI_LBC_txt2_opt[8].setChecked(false);
				cf.BI_GBI_txt1_other.setVisibility(v.GONE);
				
			}
		break;
		
		case R.id.BI_GBI_txt2_opt1:
			cf.BI_GBI_txt2_other.setVisibility(v.GONE);
		break;
		case R.id.BI_GBI_txt2_opt2:
			cf.BI_GBI_txt2_other.setVisibility(v.GONE);
		break;
		case R.id.BI_GBI_txt2_opt3:
			cf.BI_GBI_txt2_other.setVisibility(v.GONE);
		break;
		case R.id.BI_GBI_txt2_opt4:
			if(cf.BI_GBI_txt2_opt[3].isChecked())
			{
				//cf.BI_LBC_txt2_opt[8].setChecked(true);
				cf.BI_GBI_txt2_other.setVisibility(v.VISIBLE);
			}
			else
			{
				//cf.BI_LBC_txt2_opt[8].setChecked(false);
				cf.BI_GBI_txt2_other.setVisibility(v.GONE);
				
			}
		break;
		case R.id.BI_GBI_txt3_opt9:
			if(cf.BI_GBI_txt3_opt[8].isChecked())
			{
				
				cf.BI_GBI_txt3_other.setVisibility(v.VISIBLE);
			}
			else
			{
				
				cf.BI_GBI_txt3_other.setVisibility(v.GONE);
				
			}
		break;
		case R.id.BI_save:
			BI_Save_Validation();
		break;
		
	
	}
	
	}

	
}
public void BI_Save_Validation() {
	// TODO Auto-generated method stub	
	// get the data for storing //
	SH_BI_LBC_BD=cf.getslected_radio(cf.BI_LBC_txt1_opt);// common function which return the selected text 
	SH_BI_LBC_PF=cf.getselected_chk(cf.BI_LBC_txt2_opt); // coomon function returns the slected chk bocx result
	SH_BI_LBC_PF +=(cf.BI_LBC_txt2_opt[8].isChecked())? "^"+cf.BI_LBC_txt2_other.getText().toString():""; // append the other text value in to the selected option
	SH_BI_LBC_PAI = cf.BI_LBC_txt3_PAI.getText().toString();
	SH_BI_LBC_PAIO = cf.getslected_radio(cf.BI_LBC_txt3_opt);// common function which return the selected text
	//SH_BI_LBC_PAIO = (cf.BI_LBC_txt3_opt[2].isChecked())? cf.BI_LBC_txt3_other.getText().toString():SH_BI_LBC_PAIO; // repalce the text if the other text has selected 
	SH_BI_LBC_PAIO = (cf.BI_LBC_txt3_opt[2].isChecked())? "Other":SH_BI_LBC_PAIO; // repalce the text if the other text has selected
	
	SH_BI_GBI_BT=cf.getselected_chk(cf.BI_GBI_txt1_opt); // coomon function returns the slected chk bocx result
	SH_BI_GBI_BT +=(cf.BI_GBI_txt1_opt[cf.BI_GBI_txt1_opt.length-1].isChecked())? "^"+cf.BI_GBI_txt1_other.getText().toString():""; // append the other text value in to the selected option
	SH_BI_GBI_YOC = cf.BI_GBI_txt2_sp1.getSelectedItem().toString();
	SH_BI_GBI_YOC = (SH_BI_GBI_YOC.equals("Other"))?cf.BI_GBI_spin1_other.getText().toString():SH_BI_GBI_YOC;
	SH_BI_GBI_YOCO = cf.getslected_radio(cf.BI_GBI_txt2_opt); // coomon function returns the slected chk bocx result
	SH_BI_GBI_YOCO =(cf.BI_GBI_txt2_opt[cf.BI_GBI_txt2_opt.length-1].isChecked())? cf.BI_GBI_txt2_other.getText().toString():SH_BI_GBI_YOCO;
	SH_BI_GBI_BS=cf.BI_GBI_BS.getText().toString();
	SH_BI_GBI_FW=cf.getselected_chk(cf.BI_GBI_txt3_opt); // coomon function returns the slected chk bocx result
	SH_BI_GBI_FW +=(cf.BI_GBI_txt3_opt[cf.BI_GBI_txt3_opt.length-1].isChecked())? "^"+cf.BI_GBI_txt3_other.getText().toString():""; // append the other text value in to the selected option
	SH_BI_GBI_FR = cf.getselected_chk(cf.BI_GBI_txt4_opt);
	
	SH_BI_NofStories= cf.WSI_N_S.getSelectedItem().toString();
	SH_BI_WSC_URM = cf.getselected_chk(cf.BI_WSC_txt1_opt);
	
	SH_BI_WSC_RM = cf.getselected_chk(cf.BI_WSC_txt2_opt);
	SH_BI_WSC_RC = cf.getselected_chk(cf.BI_WSC_txt3_opt);
	SH_BI_WSC_WMF = cf.getselected_chk(cf.BI_WSC_txt4_opt);
	SH_BI_WSC_TNBR= cf.WSC_sp1.getSelectedItem().toString();
	SH_BI_WSC_TNEWO= cf.BI_WSC_TNEWO.getText().toString();
	SH_BI_WSC_TNEDO= cf.BI_WSC_TNEDO.getText().toString();
	
	SH_BI_WC_AS = cf.getselected_chk(cf.BI_WC_txt1_opt);
	SH_BI_WC_Stucco = cf.getselected_chk(cf.BI_WC_txt2_opt);
	SH_BI_WC_VS = cf.getselected_chk(cf.BI_WC_txt3_opt);
	SH_BI_WC_BV = cf.getselected_chk(cf.BI_WC_txt4_opt);
	SH_BI_WC_WS = cf.getselected_chk(cf.BI_WC_txt5_opt);
	SH_BI_WC_PB= cf.getselected_chk(cf.BI_WC_txt6_opt);
	SH_BI_WC_other = cf.getselected_chk(cf.BI_WC_txt7_opt);
   
	SH_BI_AD_cmd = cf.BI_AC_ed1.getText().toString();
	
	// validation starts  //
	boolean b[]=new boolean[6];
	b[0]=(cf.BI_LBC_txt2_opt[cf.BI_LBC_txt2_opt.length-1].isChecked())? ((cf.BI_LBC_txt2_other.getText().toString().equals(""))? false:true):true;
	b[1]=true;//(cf.BI_LBC_txt3_opt[cf.BI_LBC_txt3_opt.length-1].isChecked())? ((cf.BI_LBC_txt3_other.getText().toString().equals(""))? false:true):true;
	b[2]=(cf.BI_GBI_txt1_opt[cf.BI_GBI_txt1_opt.length-1].isChecked())? ((cf.BI_GBI_txt1_other.getText().toString().equals(""))? false:true):true;
	b[3]=(cf.BI_GBI_txt2_sp1.getSelectedItem().toString().toLowerCase().equals("other"))? ((cf.BI_GBI_spin1_other.getText().toString().equals(""))? false:true):true;
	b[4]=(cf.BI_GBI_txt2_opt[cf.BI_GBI_txt2_opt.length-1].isChecked())? ((cf.BI_GBI_txt2_other.getText().toString().equals(""))? false:true):true;
	b[5]=(cf.BI_GBI_txt3_opt[cf.BI_GBI_txt3_opt.length-1].isChecked())? ((cf.BI_GBI_txt3_other.getText().toString().equals(""))? false:true):true;
	
	if(!SH_BI_AD_cmd.trim().equals(""))
	{
		if(!SH_BI_LBC_PAI.trim().equals("")|| cf.BI_LBC_txt3_opt[0].isChecked())
		{
			if(b[0] && b[1] && b[2] && b[3] && b[4] && b[4] && b[5])
			{int TNEDO=0;
				if(!SH_BI_WSC_TNEDO.trim().equals(""))
				{
					try
					{
						TNEDO=Integer.parseInt(SH_BI_WSC_TNEDO.trim());
					}
					catch(Exception e)
					{
						TNEDO=0;
					}
				}
				if(TNEDO<=1)
					cf.ShowToast("Total no of external door openings must be atleast two doors in Wall Structure ", 0);
				else
				{
					BI_InsertOrUpdate(); // insert the data
				}
			}
			else
			{
				if(!b[0] || !b[1])
					cf.ShowToast("The other text in Land and Boundary condition is not found.", 0);
				else if(!b[2] || !b[3] || !b[4] || !b[5])
					cf.ShowToast("The other text in General Information is not found.", 0);
				else
					BI_InsertOrUpdate();
			}
		}
		else
		{
			 cf.ShowToast("Please Enter Present At Inspection.", 0);
		}
	}
	else
	{
		 cf.ShowToast("Please Enter comments in the Additional Comments. ", 0);
	}
}

private void BI_InsertOrUpdate() {
	// TODO Auto-generated method stub
	
	

	
		
	Cursor BI_save=null;
	try
	{
		 BI_save=cf.SelectTablefunction(cf.BIQ_table, " where SH_SQ_Homeid='"+cf.encode(cf.HomeId)+"'");
		 if(BI_save.getCount()>0)
			{
				try
				{
						cf.sh_db.execSQL("UPDATE "+cf.BIQ_table+ " set SH_BI_Inspectorid='"+cf.encode(cf.Insp_id)+"',SH_SQ_Homeid='"+cf.encode(cf.selectedhomeid)+"',SH_BI_LBC_BD='"+cf.encode(SH_BI_LBC_BD)+"',SH_BI_LBC_PF='"+cf.encode(SH_BI_LBC_PF)+"'" +
								",SH_BI_LBC_PAI='"+cf.encode(SH_BI_LBC_PAI)+"',SH_BI_LBC_PAIO='"+cf.encode(SH_BI_LBC_PAIO)+"',SH_BI_GBI_BT='"+cf.encode(SH_BI_GBI_BT)+"',SH_BI_GBI_YOC='"+cf.encode(SH_BI_GBI_YOC)+"',SH_BI_GBI_YOCO='"+cf.encode(SH_BI_GBI_YOCO)+"',SH_BI_GBI_BS='"+cf.encode(SH_BI_GBI_BS)+"',SH_BI_GBI_FW='"+cf.encode(SH_BI_GBI_FW)+"',SH_BI_GBI_FR='"+cf.encode(SH_BI_GBI_FR)+"',SH_BI_NofStories='"+cf.encode(SH_BI_NofStories)+"',SH_BI_WSC_URM='"+cf.encode(SH_BI_WSC_URM)+"',SH_BI_WSC_RM='"+cf.encode(SH_BI_WSC_RM)+"',SH_BI_WSC_RC='"+cf.encode(SH_BI_WSC_RC)+"',SH_BI_WSC_WMF='"+cf.encode(SH_BI_WSC_WMF)+"',SH_BI_WSC_TNBR='"+cf.encode(SH_BI_WSC_TNBR)+"',SH_BI_WSC_TNEWO='"+cf.encode(SH_BI_WSC_TNEWO)+"'" +
										",SH_BI_WSC_TNEDO='"+cf.encode(SH_BI_WSC_TNEDO)+"',SH_BI_WC_AS='"+cf.encode(SH_BI_WC_AS)+"',SH_BI_WC_VS='"+cf.encode(SH_BI_WC_VS)+"',SH_BI_WC_WS='"+cf.encode(SH_BI_WC_WS)+"',SH_BI_WC_other='"+cf.encode(SH_BI_WC_other)+"',SH_BI_WC_Stucco='"+cf.encode(SH_BI_WC_Stucco)+"',SH_BI_WC_BV='"+cf.encode(SH_BI_WC_BV)+"',SH_BI_WC_PB='"+cf.encode(SH_BI_WC_PB)+"',SH_BI_AD_cmd='"+cf.encode(SH_BI_AD_cmd)+"' where SH_SQ_Homeid='"+cf.encode(cf.selectedhomeid)+"' ");
						 cf.ShowToast("Building Information has been saved sucessfully.", 1);
						// move the page to next page // 
						 nextlayout();
						//	startActivity(new Intent(getApplicationContext(),Observation1.class));
						// move the page to next page ends
				}
				catch (Exception E)
				{
					String strerrorlog="update the Builing question  table not working ";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table update at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			else
			{
					
					try
					{
							cf.sh_db.execSQL("INSERT INTO "+cf.BIQ_table+ "(SH_BI_Inspectorid,SH_SQ_Homeid,SH_BI_LBC_BD,SH_BI_LBC_PF,SH_BI_LBC_PAI,SH_BI_LBC_PAIO,SH_BI_GBI_BT,SH_BI_GBI_YOC,SH_BI_GBI_YOCO,SH_BI_GBI_BS,SH_BI_GBI_FW,SH_BI_GBI_FR,SH_BI_NofStories,SH_BI_WSC_URM,SH_BI_WSC_RM,SH_BI_WSC_RC,SH_BI_WSC_WMF,SH_BI_WSC_TNBR,SH_BI_WSC_TNEWO,SH_BI_WSC_TNEDO,SH_BI_WC_AS,SH_BI_WC_VS,SH_BI_WC_WS,SH_BI_WC_other,SH_BI_WC_Stucco,SH_BI_WC_BV,SH_BI_WC_PB,SH_BI_AD_cmd)" +
							"VALUES ('"+cf.encode(cf.Insp_id)+"','"+cf.encode(cf.selectedhomeid)+"','"+cf.encode(SH_BI_LBC_BD)+"','"+cf.encode(SH_BI_LBC_PF)+"','"+cf.encode(SH_BI_LBC_PAI)+"','"+cf.encode(SH_BI_LBC_PAIO)+"','"+cf.encode(SH_BI_GBI_BT)+"','"+cf.encode(SH_BI_GBI_YOC)+"','"+cf.encode(SH_BI_GBI_YOCO)+"','"+cf.encode(SH_BI_GBI_BS)+"','"+cf.encode(SH_BI_GBI_FW)+"','"+cf.encode(SH_BI_GBI_FR)+"','"+cf.encode(SH_BI_NofStories)+"','"+cf.encode(SH_BI_WSC_URM)+"','"+cf.encode(SH_BI_WSC_RM)+"','"+cf.encode(SH_BI_WSC_RC)+"','"+cf.encode(SH_BI_WSC_WMF)+"','"+cf.encode(SH_BI_WSC_TNBR)+"','"+cf.encode(SH_BI_WSC_TNEWO)+"','"+cf.encode(SH_BI_WSC_TNEDO)+"','"+cf.encode(SH_BI_WC_AS)+"','"+cf.encode(SH_BI_WC_VS)+"','"+cf.encode(SH_BI_WC_WS)+"','"+cf.encode(SH_BI_WC_other)+"','"+cf.encode(SH_BI_WC_Stucco)+"','"+cf.encode(SH_BI_WC_BV)+"','"+cf.encode(SH_BI_WC_PB)+"','"+cf.encode(SH_BI_AD_cmd)+"' );");
							
							 cf.ShowToast("Building Information has been saved sucessfully.", 1);
							// move the page to next page // 
							 nextlayout();
								//startActivity(new Intent(getApplicationContext(),Observation1.class));
							// move the page to next page ends
					}
					catch (Exception E)
					{
						String strerrorlog="Insert the Building  question  table not working ";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					}
					
				
			}
	}
	catch (Exception E)
	{
		String strerrorlog="Selection of the Building information   table not working ";
		cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
	}
		
	
}

private void nextlayout() {
	// TODO Auto-generated method stub
	Intent intimg = new Intent(BuildingInformation.this,
			Observation1.class);
	intimg.putExtra("homeid", cf.selectedhomeid);
    intimg.putExtra("InspectionType", cf.onlinspectionid);
	intimg.putExtra("status", cf.onlstatus);
	startActivity(intimg);
}

public boolean getFromArray(String sH_BI_GBI_YOC2, String[] bI_GBI_year)
{
	for(int i=0;i < bI_GBI_year.length;i++)
	 {
		 if(sH_BI_GBI_YOC2.equals(bI_GBI_year[i]))
		 {
			 return false;
		 }
	 }
	return true;
}

class  RadioArrayclicker implements OnClickListener
{

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		for(int i=0;i<cf.BI_LBC_txt1_opt.length;i++)
		{
			if(v.getId()==cf.BI_LBC_txt1_opt[i].getId())
			{
				cf.BI_LBC_txt1_opt[i].setChecked(true);
			}
			else
			{
				cf.BI_LBC_txt1_opt[i].setChecked(false);
			}
		}
	}
	
}
//setting the limit for the edit texts  
	class BI_textwatcher implements TextWatcher
	{
      @Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
		    	  
				cf.showing_limit(s.toString(),cf.BI_ED_parrant,cf.BI_ED,cf.BI_ED_TV);
		
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}
private void setObjectsToView() {
	// TODO Auto-generated method stub
	  // start the declaring the objects for the controls 
    cf.BI_savenext= (Button) findViewById(R.id.BI_save);
    cf.BI_LBC_lin = (LinearLayout) findViewById(R.id.BI_LBC_lin);
    cf.BI_GBI_lin = (LinearLayout) findViewById(R.id.BI_GBI_lin);
    cf.BI_WSC_lin = (LinearLayout) findViewById(R.id.BI_WSC_lin);
    cf.BI_AC_lin = (LinearLayout) findViewById(R.id.BI_AC_lin);
    cf.BI_LBC_table1 = (TableLayout) findViewById(R.id.BI_LBC_table1);
    cf.BI_GBI_table1 = (TableLayout) findViewById(R.id.BI_GBI_table1);
    cf.BI_WSC_rel1 = (RelativeLayout) findViewById(R.id.BI_WSC_rel1);
    cf.BI_AC_lin2 = (LinearLayout) findViewById(R.id.BI_AC_lin2);
    cf.BI_AC_ed1 = (EditText) findViewById(R.id.BI_AC_ed1);
    cf.BI_ED_parrant=(LinearLayout) findViewById(R.id.BI_ED_parrant);
	cf.BI_ED=(LinearLayout) findViewById(R.id.BI_ED);
	cf.BI_ED_TV = (TextView) findViewById(R.id.BI_ED_TV);
    
    cf.BI_LBC_txt1_opt[0]=(RadioButton) findViewById(R.id.BI_LBC_txt1_opt1);
    cf.BI_LBC_txt1_opt[1]=(RadioButton) findViewById(R.id.BI_LBC_txt1_opt2);
    cf.BI_LBC_txt1_opt[2]=(RadioButton) findViewById(R.id.BI_LBC_txt1_opt3);
    cf.BI_LBC_txt1_opt[3]=(RadioButton) findViewById(R.id.BI_LBC_txt1_opt4);
    cf.BI_LBC_txt1_opt[4]=(RadioButton) findViewById(R.id.BI_LBC_txt1_opt5);
    cf.BI_LBC_txt1_opt[5]=(RadioButton) findViewById(R.id.BI_LBC_txt1_opt6);
    cf.BI_LBC_txt1_opt[6]=(RadioButton) findViewById(R.id.BI_LBC_txt1_opt7);
    cf.BI_LBC_txt1_opt[7]=(RadioButton) findViewById(R.id.BI_LBC_txt1_opt8);
    ArrayListener(cf.BI_LBC_txt1_opt); // set the onclick lister for select only on radio at a time
    
    
    cf.BI_LBC_txt2_opt[0]=(CheckBox) findViewById(R.id.BI_LBC_txt2_opt1);
    cf.BI_LBC_txt2_opt[1]=(CheckBox) findViewById(R.id.BI_LBC_txt2_opt2);
    cf.BI_LBC_txt2_opt[2]=(CheckBox) findViewById(R.id.BI_LBC_txt2_opt3);
    cf.BI_LBC_txt2_opt[3]=(CheckBox) findViewById(R.id.BI_LBC_txt2_opt4);
    cf.BI_LBC_txt2_opt[4]=(CheckBox) findViewById(R.id.BI_LBC_txt2_opt5);
    cf.BI_LBC_txt2_opt[5]=(CheckBox) findViewById(R.id.BI_LBC_txt2_opt6);
    cf.BI_LBC_txt2_opt[6]=(CheckBox) findViewById(R.id.BI_LBC_txt2_opt7);
    cf.BI_LBC_txt2_opt[7]=(CheckBox) findViewById(R.id.BI_LBC_txt2_opt8);
    cf.BI_LBC_txt2_opt[8]=(CheckBox) findViewById(R.id.BI_LBC_txt2_opt9);
    cf.BI_LBC_txt2_other =(EditText) findViewById(R.id.BI_LBC_txt2_other);
    ArrayListenerClickCheck(cf.BI_LBC_txt2_opt); // set the onclick class for the check box cicke
    
    cf.BI_LBC_txt3_opt[0]=(RadioButton) findViewById(R.id.BI_LBC_txt3_opt1);
    cf.BI_LBC_txt3_opt[1]=(RadioButton) findViewById(R.id.BI_LBC_txt3_opt2);
    cf.BI_LBC_txt3_opt[2]=(RadioButton) findViewById(R.id.BI_LBC_txt3_opt3);
    cf.BI_LBC_txt3_other =(EditText) findViewById(R.id.BI_LBC_txt3_other);
    cf.BI_LBC_txt3_PAI =(EditText) findViewById(R.id.BI_LBC_txt3_et1);
    
    ArrayListenerClickradio(cf.BI_LBC_txt3_opt); // set the onclick class for the Radio box cicke
    
    cf.BI_GBI_txt1_opt[0]=(CheckBox) findViewById(R.id.BI_GBI_txt1_opt1);
    cf.BI_GBI_txt1_opt[1]=(CheckBox) findViewById(R.id.BI_GBI_txt1_opt2);
    cf.BI_GBI_txt1_opt[2]=(CheckBox) findViewById(R.id.BI_GBI_txt1_opt3);
    cf.BI_GBI_txt1_opt[3]=(CheckBox) findViewById(R.id.BI_GBI_txt1_opt4);
    cf.BI_GBI_txt1_opt[4]=(CheckBox) findViewById(R.id.BI_GBI_txt1_opt5);
    cf.BI_GBI_txt1_opt[5]=(CheckBox) findViewById(R.id.BI_GBI_txt1_opt6);
    cf.BI_GBI_txt1_opt[6]=(CheckBox) findViewById(R.id.BI_GBI_txt1_opt7);
    cf.BI_GBI_txt1_other=(EditText) findViewById(R.id.BI_GBI_txt1_other);
    ArrayListenerClickCheck(cf.BI_GBI_txt1_opt); // set the onclick class for the check box cicke
    
    
    cf.BI_GBI_txt2_opt[0]=(RadioButton) findViewById(R.id.BI_GBI_txt2_opt1);
    cf.BI_GBI_txt2_opt[1]=(RadioButton) findViewById(R.id.BI_GBI_txt2_opt2);
    cf.BI_GBI_txt2_opt[2]=(RadioButton) findViewById(R.id.BI_GBI_txt2_opt3);
    cf.BI_GBI_txt2_opt[3]=(RadioButton) findViewById(R.id.BI_GBI_txt2_opt4);
    cf.BI_GBI_txt2_other=(EditText) findViewById(R.id.BI_GBI_txt2_other);
    ArrayListenerClickradio(cf.BI_GBI_txt2_opt); // set the onclick class for the check box cicke
    
    cf.BI_GBI_BS =(EditText) findViewById(R.id.BI_BS_sp1); 
    
    cf.BI_GBI_txt3_opt[0]=(CheckBox) findViewById(R.id.BI_GBI_txt3_opt1);
    cf.BI_GBI_txt3_opt[1]=(CheckBox) findViewById(R.id.BI_GBI_txt3_opt2);
    cf.BI_GBI_txt3_opt[2]=(CheckBox) findViewById(R.id.BI_GBI_txt3_opt3);
    cf.BI_GBI_txt3_opt[3]=(CheckBox) findViewById(R.id.BI_GBI_txt3_opt4);
    cf.BI_GBI_txt3_opt[4]=(CheckBox) findViewById(R.id.BI_GBI_txt3_opt5);
    cf.BI_GBI_txt3_opt[5]=(CheckBox) findViewById(R.id.BI_GBI_txt3_opt6);
    cf.BI_GBI_txt3_opt[6]=(CheckBox) findViewById(R.id.BI_GBI_txt3_opt7);
    cf.BI_GBI_txt3_opt[7]=(CheckBox) findViewById(R.id.BI_GBI_txt3_opt8);
    cf.BI_GBI_txt3_opt[8]=(CheckBox) findViewById(R.id.BI_GBI_txt3_opt9);
    cf.BI_GBI_txt3_other=(EditText) findViewById(R.id.BI_GBI_txt3_other);
    ArrayListenerClickCheck(cf.BI_GBI_txt3_opt); // set the onclick class for the check box cicke
    
    cf.BI_GBI_txt4_opt[0]=(CheckBox) findViewById(R.id.BI_GBI_txt4_opt1);
    cf.BI_GBI_txt4_opt[1]=(CheckBox) findViewById(R.id.BI_GBI_txt4_opt2);
    cf.BI_GBI_txt4_opt[2]=(CheckBox) findViewById(R.id.BI_GBI_txt4_opt3);
    cf.BI_GBI_txt4_opt[3]=(CheckBox) findViewById(R.id.BI_GBI_txt4_opt4);
    cf.BI_GBI_txt4_opt[4]=(CheckBox) findViewById(R.id.BI_GBI_txt4_opt5);
    
    cf.BI_WSC_txt1_opt[0]=(CheckBox) findViewById(R.id.BI_WSC_txt1_opt1);
    cf.BI_WSC_txt1_opt[1]=(CheckBox) findViewById(R.id.BI_WSC_txt1_opt2);
    cf.BI_WSC_txt1_opt[2]=(CheckBox) findViewById(R.id.BI_WSC_txt1_opt3);
    cf.BI_WSC_txt1_opt[3]=(CheckBox) findViewById(R.id.BI_WSC_txt1_opt4);
    cf.BI_WSC_txt1_opt[4]=(CheckBox) findViewById(R.id.BI_WSC_txt1_opt5);
    ArrayListenerClickCheck(cf.BI_WSC_txt1_opt); // set the onclick class for the check box cicke
    
    
    cf.BI_WSC_txt2_opt[0]=(CheckBox) findViewById(R.id.BI_WSC_txt2_opt1);
    cf.BI_WSC_txt2_opt[1]=(CheckBox) findViewById(R.id.BI_WSC_txt2_opt2);
    cf.BI_WSC_txt2_opt[2]=(CheckBox) findViewById(R.id.BI_WSC_txt2_opt3);
    cf.BI_WSC_txt2_opt[3]=(CheckBox) findViewById(R.id.BI_WSC_txt2_opt4);
    cf.BI_WSC_txt2_opt[4]=(CheckBox) findViewById(R.id.BI_WSC_txt2_opt5);
    ArrayListenerClickCheck(cf.BI_WSC_txt2_opt); // set the onclick class for the check box cicke
    
    cf.BI_WSC_txt3_opt[0]=(CheckBox) findViewById(R.id.BI_WSC_txt3_opt1);
    cf.BI_WSC_txt3_opt[1]=(CheckBox) findViewById(R.id.BI_WSC_txt3_opt2);
    cf.BI_WSC_txt3_opt[2]=(CheckBox) findViewById(R.id.BI_WSC_txt3_opt3);
    cf.BI_WSC_txt3_opt[3]=(CheckBox) findViewById(R.id.BI_WSC_txt3_opt4);
    cf.BI_WSC_txt3_opt[4]=(CheckBox) findViewById(R.id.BI_WSC_txt3_opt5);
    ArrayListenerClickCheck(cf.BI_WSC_txt3_opt); // set the onclick class for the check box cicke

    cf.BI_WSC_txt4_opt[0]=(CheckBox) findViewById(R.id.BI_WSC_txt4_opt1);
    cf.BI_WSC_txt4_opt[1]=(CheckBox) findViewById(R.id.BI_WSC_txt4_opt2);
    cf.BI_WSC_txt4_opt[2]=(CheckBox) findViewById(R.id.BI_WSC_txt4_opt3);
    cf.BI_WSC_txt4_opt[3]=(CheckBox) findViewById(R.id.BI_WSC_txt4_opt4);
    cf.BI_WSC_txt4_opt[4]=(CheckBox) findViewById(R.id.BI_WSC_txt4_opt5);
    ArrayListenerClickCheck(cf.BI_WSC_txt4_opt); // set the onclick class for the check box cicke
    
    cf.BI_WC_txt1_opt[0]=(CheckBox) findViewById(R.id.BI_WC_txt1_opt1);
    cf.BI_WC_txt1_opt[1]=(CheckBox) findViewById(R.id.BI_WC_txt1_opt2);
    cf.BI_WC_txt1_opt[2]=(CheckBox) findViewById(R.id.BI_WC_txt1_opt3);
    cf.BI_WC_txt1_opt[3]=(CheckBox) findViewById(R.id.BI_WC_txt1_opt4);
    cf.BI_WC_txt1_opt[4]=(CheckBox) findViewById(R.id.BI_WC_txt1_opt5);
    ArrayListenerClickCheck(cf.BI_WC_txt1_opt); // set the onclick class for the check box cicke
    
    
    cf.BI_WC_txt2_opt[0]=(CheckBox) findViewById(R.id.BI_WC_txt2_opt1);
    cf.BI_WC_txt2_opt[1]=(CheckBox) findViewById(R.id.BI_WC_txt2_opt2);
    cf.BI_WC_txt2_opt[2]=(CheckBox) findViewById(R.id.BI_WC_txt2_opt3);
    cf.BI_WC_txt2_opt[3]=(CheckBox) findViewById(R.id.BI_WC_txt2_opt4);
    cf.BI_WC_txt2_opt[4]=(CheckBox) findViewById(R.id.BI_WC_txt2_opt5);
    ArrayListenerClickCheck(cf.BI_WC_txt2_opt); // set the onclick class for the check box cicke
    
    cf.BI_WC_txt3_opt[0]=(CheckBox) findViewById(R.id.BI_WC_txt3_opt1);
    cf.BI_WC_txt3_opt[1]=(CheckBox) findViewById(R.id.BI_WC_txt3_opt2);
    cf.BI_WC_txt3_opt[2]=(CheckBox) findViewById(R.id.BI_WC_txt3_opt3);
    cf.BI_WC_txt3_opt[3]=(CheckBox) findViewById(R.id.BI_WC_txt3_opt4);
    cf.BI_WC_txt3_opt[4]=(CheckBox) findViewById(R.id.BI_WC_txt3_opt5);
    ArrayListenerClickCheck(cf.BI_WC_txt3_opt); // set the onclick class for the check box cicke

    cf.BI_WC_txt4_opt[0]=(CheckBox) findViewById(R.id.BI_WC_txt4_opt1);
    cf.BI_WC_txt4_opt[1]=(CheckBox) findViewById(R.id.BI_WC_txt4_opt2);
    cf.BI_WC_txt4_opt[2]=(CheckBox) findViewById(R.id.BI_WC_txt4_opt3);
    cf.BI_WC_txt4_opt[3]=(CheckBox) findViewById(R.id.BI_WC_txt4_opt4);
    cf.BI_WC_txt4_opt[4]=(CheckBox) findViewById(R.id.BI_WC_txt4_opt5);
    ArrayListenerClickCheck(cf.BI_WC_txt4_opt); // set the onclick class for the check box cicke
    
    cf.BI_WC_txt5_opt[0]=(CheckBox) findViewById(R.id.BI_WC_txt5_opt1);
    cf.BI_WC_txt5_opt[1]=(CheckBox) findViewById(R.id.BI_WC_txt5_opt2);
    cf.BI_WC_txt5_opt[2]=(CheckBox) findViewById(R.id.BI_WC_txt5_opt3);
    cf.BI_WC_txt5_opt[3]=(CheckBox) findViewById(R.id.BI_WC_txt5_opt4);
    cf.BI_WC_txt5_opt[4]=(CheckBox) findViewById(R.id.BI_WC_txt5_opt5);
    ArrayListenerClickCheck(cf.BI_WC_txt5_opt); // set the onclick class for the check box cicke
    cf.BI_WSC_TNEWO =(EditText) findViewById(R.id.WSI_ed1);
    cf.BI_WSC_TNEDO =(EditText) findViewById(R.id.WSI_ed2);
    
    cf.BI_WC_txt6_opt[0]=(CheckBox) findViewById(R.id.BI_WC_txt6_opt1);
    cf.BI_WC_txt6_opt[1]=(CheckBox) findViewById(R.id.BI_WC_txt6_opt2);
    cf.BI_WC_txt6_opt[2]=(CheckBox) findViewById(R.id.BI_WC_txt6_opt3);
    cf.BI_WC_txt6_opt[3]=(CheckBox) findViewById(R.id.BI_WC_txt6_opt4);
    cf.BI_WC_txt6_opt[4]=(CheckBox) findViewById(R.id.BI_WC_txt6_opt5);
    ArrayListenerClickCheck(cf.BI_WC_txt6_opt); // set the onclick class for the check box cicke
    
    cf.BI_WC_txt7_opt[0]=(CheckBox) findViewById(R.id.BI_WC_txt7_opt1);
    cf.BI_WC_txt7_opt[1]=(CheckBox) findViewById(R.id.BI_WC_txt7_opt2);
    cf.BI_WC_txt7_opt[2]=(CheckBox) findViewById(R.id.BI_WC_txt7_opt3);
    cf.BI_WC_txt7_opt[3]=(CheckBox) findViewById(R.id.BI_WC_txt7_opt4);
    cf.BI_WC_txt7_opt[4]=(CheckBox) findViewById(R.id.BI_WC_txt7_opt5);
    ArrayListenerClickCheck(cf.BI_WC_txt7_opt); // set the onclick class for the check box cicke

    
    adapter1 = new ArrayAdapter(BuildingInformation.this,android.R.layout.simple_spinner_item, cf.BI_GBI_year);
    cf.BI_GBI_txt2_sp1 = (Spinner) findViewById(R.id.BI_GBI_txt2_sp1);
    cf.BI_GBI_txt2_sp1.setAdapter(adapter1);
    cf.BI_GBI_spin1_other = (EditText) findViewById(R.id.BI_GBI_spin1_other);
    
    
     adapter2 = new ArrayAdapter(BuildingInformation.this,android.R.layout.simple_spinner_item, cf.BI_WSC_bedroom);
    cf.WSC_sp1 = (Spinner) findViewById(R.id.WSC_sp1);
    cf.WSC_sp1.setAdapter(adapter2);
    
     adapter3 = new ArrayAdapter(BuildingInformation.this,android.R.layout.simple_spinner_item, cf.BI_WSC_storey);
    cf.WSI_N_S = (Spinner) findViewById(R.id.WSC_N_S);
    cf.WSI_N_S.setAdapter(adapter3);
    cf.WSI_N_S.setSelection(0);

}
public boolean onKeyDown(int keyCode, KeyEvent event) {
	// replaces the default 'Back' button action
	if (keyCode == KeyEvent.KEYCODE_BACK) {
		if(cf.strschdate.equals("")){
			cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
		}else{
		Intent  myintent = new Intent(BuildingInformation.this,SummaryQuest.class);
		cf.putExtras(myintent);
		startActivity(myintent);
		}
		return true;
	}
	if (keyCode == KeyEvent.KEYCODE_MENU) {

	}
	return super.onKeyDown(keyCode, event);
}
public void clicker(View v)
{
	switch(v.getId())
	{
	case R.id.home:
	cf.gohome();	
	break;
	}
}
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			switch (resultCode) {
			case 0:
				break;
			case -1:
				try {

					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {	
				}
				
				break;

		}

	}

}
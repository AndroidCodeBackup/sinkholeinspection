package idsoft.inspectiondepot.sinkholeinspection;

import idsoft.inspectiondepot.sinkholeinspection.Observation1.Touch_Listener;

import java.io.IOException;

import java.net.SocketTimeoutException;
import java.util.Calendar;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Html;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Callattempt extends Activity {
	CommonFunctions cf;
	private int mYear, mMonth, mDay;
	static final int DATE_DIALOG_ID = 0;
	protected static final int visibility = 0;
	EditText etgen, Calldate, Callday, Calltime, Numbercalled, Personanswered,
			Initialcomments, OtherTitle,resul_other;
	RadioButton chkbx1, chkbx2,h_ph,w_ph,c_ph,a_ph,a_c_ph;
	TextView tvgen;
	SoapObject res;
	View v1;
	ListView slist;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	final Calendar cal = Calendar.getInstance();
	Button Submitcallattempt, getdate, Currenttime, Submitandcancelinspection,
			seldate;
	TableLayout perstbl, tittbl, comttbl, lsttblhdr;
	TextView viewhomeph, txtPersonanswered, txtInitialcomments, txtTitle,
			txtOther, txtnolist;
	String titlespin, resultspin, phnnum, homeid, InspectionType, status,
			selitem, selresitem, seltitle, strcaldat, strcalday, strcaltime,
			strcalcom, strcalnum, strcalres, strcaltit, chkalert, assdate;
	static String anytype = "anyType{}";
	String[] array_unable, array_insp, array_res, array_title;
	int value, Count, c, selitmflg, selresitmflg, seltitflg, k, verify = 0,
			day, cnt, ichk;
	static String[] arrtit, arrnum, arrres, arrcom, arrdat, arrday, arrtime;
	private ArrayAdapter<CharSequence> loUnableschedule, loInspectionrefused,
			loResult, loTitle;
	Spinner Unableschedule, Inspectionrefused, Result, Title;
	TableRow tr1, tr2, tr3, tr4;
	Cursor cur1;
	ProgressDialog pd;
	TextView policyholderinfo;
	TextView edit_link[]=new TextView[5];
	private AlertDialog alertDialog;
	String W_phone,H_phone,C_phone, A_phone,A_C_phone; 
	  public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
       	}
       	   setContentView(R.layout.callattemptinfo);
       	   cf.getDeviceDimensions();
       	   LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
       	    mainmenu_layout.setMinimumWidth(cf.wd);
	       mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,cf));
		   LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.generalsubmenu);
		   submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 14, 1,cf));
		   ScrollView scr=(ScrollView)findViewById(R.id.scr);
		   scr.setMinimumHeight(cf.ht);
		   cf.getInspectorId();
           cf.getinspectiondate();
           DateFormat df = new android.text.format.DateFormat();
   		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
   		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());

   		this.chkbx1 = (RadioButton) this.findViewById(R.id.chk1);
		chkbx1.setOnClickListener(OnClickListener);
		this.chkbx2 = (RadioButton) this.findViewById(R.id.chk2);
		chkbx2.setOnClickListener(OnClickListener);

		this.Unableschedule = (Spinner) this
				.findViewById(R.id.spunableschedule);
		this.Inspectionrefused = (Spinner) this
				.findViewById(R.id.spinspectionrefused);
		TextView gencomtxt = (TextView) findViewById(R.id.gencmt);
		gencomtxt.setText(Html.fromHtml(cf.redcolor+" "+"General Comments :"));
		Unableschedule.setEnabled(false);
		Inspectionrefused.setEnabled(false);
		TextView calldatetxt = (TextView) findViewById(R.id.txtcalldate);
		calldatetxt.setText(Html.fromHtml(cf.redcolor+" "+"Call Date :"));
		
		TextView calltimetxt = (TextView) findViewById(R.id.txtcalltime);
		calltimetxt.setText(Html.fromHtml(cf.redcolor+" "+"Call Time :"));
		
		TextView numtxt = (TextView) findViewById(R.id.txtnumbercalled);
		numtxt.setText(Html.fromHtml(cf.redcolor+" "+"Number Called :"));
		
		TextView restxt = (TextView) findViewById(R.id.txtresult);
		restxt.setText(Html.fromHtml(cf.redcolor+" "+"Result :"));
		
		TextView persntxt = (TextView) findViewById(R.id.txtpersonanswered);
		persntxt.setText(Html.fromHtml(cf.redcolor+" "+"Person Answered :"));
		
		TextView tittxt = (TextView) findViewById(R.id.txttitle);
		tittxt.setText(Html.fromHtml(cf.redcolor+" "+"Title :"));
		
		this.tvgen = (TextView) this.findViewById(R.id.gencmt);
		this.etgen = (EditText) this.findViewById(R.id.ed1);
		this.Calldate = (EditText) this.findViewById(R.id.calldate);
		this.getdate = (Button) this.findViewById(R.id.currentdate);
		this.seldate = (Button) this.findViewById(R.id.seldate);
		this.Callday = (EditText) this.findViewById(R.id.callday);
		this.Calltime = (EditText) this.findViewById(R.id.calltime);
		this.Currenttime = (Button) this.findViewById(R.id.currenttime);
		this.Numbercalled = (EditText) this.findViewById(R.id.numbercalled);
		this.Result = (Spinner) this.findViewById(R.id.spresult);
		resul_other = (EditText) findViewById(R.id.resul_other);
		this.txtPersonanswered = (TextView) this
				.findViewById(R.id.txtpersonanswered);
		this.Personanswered = (EditText) this.findViewById(R.id.personanswered);
		this.txtTitle = (TextView) this.findViewById(R.id.txttitle);
		this.Title = (Spinner) this.findViewById(R.id.sptitle);
		Title.setVisibility(View.GONE);
		this.txtOther = (TextView) this.findViewById(R.id.txttitle11);
		
		this.OtherTitle = (EditText) this.findViewById(R.id.othertitle);
		OtherTitle.setVisibility(View.GONE);
		this.txtInitialcomments = (TextView) this
				.findViewById(R.id.txtinitialcomment);
		
		this.Initialcomments = (EditText) this
				.findViewById(R.id.initialcomment);
		Initialcomments.setOnTouchListener(new Touch_Listener(1));
		resul_other.setOnTouchListener(new Touch_Listener(2));
		h_ph=(RadioButton) findViewById(R.id.Call_H_ph);
		w_ph=(RadioButton) findViewById(R.id.Call_W_ph);
		c_ph=(RadioButton) findViewById(R.id.Call_C_ph);
		a_ph=(RadioButton) findViewById(R.id.Call_A_ph);
		a_c_ph=(RadioButton) findViewById(R.id.Call_A_C_ph);
		edit_link[0]=(TextView) findViewById(R.id.c_edit_H_ph);
		edit_link[1]=(TextView) findViewById(R.id.c_edit_W_ph);
		edit_link[2]=(TextView) findViewById(R.id.c_edit_C_ph);
		edit_link[3]=(TextView) findViewById(R.id.c_edit_A_ph);
		edit_link[4]=(TextView) findViewById(R.id.c_edit_A_C_ph);
		
		 
		
		try
		{
			Cursor cur1=cf.SelectTablefunction(cf.policyholder, " where SH_PH_SRID='" + cf.selectedhomeid + "' and SH_PH_InspectorId='"	+ cf.Insp_id + "'");
			
			if(cur1.getCount()>0)
			{
				cur1.moveToFirst();
				Initialcomments.setText(cf.decode(cur1.getString(cur1
					.getColumnIndex("SH_PH_FirstName")))
					+ " "
					+cf.decode( cur1.getString(cur1.getColumnIndex("SH_PH_LastName"))));
                  assdate=cf.decode(cur1.getString(cur1.getColumnIndex("SH_Schedule_AssignedDate")));
                  H_phone=cf.decode(cur1.getString(cur1.getColumnIndex("SH_PH_HomePhone"))).trim();
                  W_phone=cf.decode(cur1.getString(cur1.getColumnIndex("SH_PH_WorkPhone"))).trim();
                  C_phone=cf.decode(cur1.getString(cur1.getColumnIndex("SH_PH_CellPhone"))).trim();
                  if(H_phone.equals(""))
                  {
                	  h_ph.setText("");
                	  
                	  h_ph.setVisibility(View.INVISIBLE);
                	  edit_link[0].setVisibility(View.INVISIBLE);
                	  
                  }
                  else
                  {
                	  h_ph.setText(H_phone);
                	  edit_link[0].setVisibility(View.VISIBLE);
                	  h_ph.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							
							// TODO Auto-generated method stub
							
							if(h_ph.isChecked())
							{
								w_ph.setChecked(false);
								c_ph.setChecked(false);
								a_ph.setChecked(false);
								a_c_ph.setChecked(false);
								H_phone=H_phone.replace("(", "");
								H_phone=H_phone.replace(")", "");
								H_phone=H_phone.replace("-", "");
								
								Numbercalled.setText(H_phone);
							}
						}
					});
                  }
                  
                  if(W_phone.equals(""))
                  {
                	  
                	  w_ph.setText("");
                	  w_ph.setVisibility(View.INVISIBLE);
                	  edit_link[1].setVisibility(View.INVISIBLE);
                	  
                  }
                  
                  else
                  {
                	  w_ph.setText(W_phone);   
                	  edit_link[1].setVisibility(View.VISIBLE);
                	  w_ph.setOnClickListener(new OnClickListener() {
  						
  						@Override
  						public void onClick(View v) {
  							// TODO Auto-generated method stub
  							
  							if(w_ph.isChecked())
  							{
  								
  								h_ph.setChecked(false);
								c_ph.setChecked(false);
								a_ph.setChecked(false);
  								W_phone=W_phone.replace("(", "");
  								W_phone=W_phone.replace(")", "");
  								W_phone=W_phone.replace("-", "");
  								Numbercalled.setText(W_phone);
  								a_c_ph.setChecked(false);
  							}
  						}
  					});
                  }
                  if(C_phone.equals(""))
                  {
                	  c_ph.setText("");
                	  edit_link[2].setVisibility(View.INVISIBLE);
                	  c_ph.setVisibility(View.INVISIBLE);
                  }
                  else
                  {
                	  c_ph.setText(C_phone);
                	  edit_link[2].setVisibility(View.VISIBLE);
                	  c_ph.setOnClickListener(new OnClickListener() {
  						
  						@Override
  						public void onClick(View v) {
  							// TODO Auto-generated method stub
  							if(c_ph.isChecked())
  							{
  								h_ph.setChecked(false);
								w_ph.setChecked(false);   
								a_ph.setChecked(false);
								a_c_ph.setChecked(false);
  								C_phone=C_phone.replace("(", "");
  								C_phone=C_phone.replace(")", "");
  								C_phone=C_phone.replace("-", "");
  								Numbercalled.setText(C_phone);
  							}
  						}
  					});
                  }
                  
                  
                  
                  
                  
			}
			cf.Create_Table(17);
			Cursor c2=cf.SelectTablefunction(cf.Agent_tabble, " where SH_AI_SRID='" + cf.selectedhomeid + "'");
			if(c2.getCount()>0)
			{
				c2.moveToFirst();
				A_phone=c2.getString(c2.getColumnIndex("SH_AI_AgentOffPhone")).trim();
				
				A_C_phone=c2.getString(c2.getColumnIndex("SH_AI_AgentContactPhone")).trim();
				 if(A_phone.equals(""))
                 {  
               	  a_ph.setText("");
               	  edit_link[3].setVisibility(View.INVISIBLE);
               	  a_ph.setVisibility(View.INVISIBLE);
                 }
                 else
                 {
               	  a_ph.setText(A_phone);
               	  edit_link[3].setVisibility(View.VISIBLE);
               	  a_ph.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(a_ph.isChecked())
						{
							h_ph.setChecked(false);
							c_ph.setChecked(false);
							w_ph.setChecked(false);
							a_c_ph.setChecked(false);
							A_phone=A_phone.replace("(", "");
							A_phone=A_phone.replace(")", "");
							A_phone=A_phone.replace("-", "");
							Numbercalled.setText(A_phone);
						}
					}
				});
                 }
				 if(A_C_phone.equals(""))
                 {  
               	  a_c_ph.setText("");
               	  edit_link[4].setVisibility(View.INVISIBLE);
               	  a_c_ph.setVisibility(View.INVISIBLE);
                 }
                 else
                 {
               	  a_c_ph.setText(A_C_phone);
               	  edit_link[4].setVisibility(View.VISIBLE);
               	  a_c_ph.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(a_c_ph.isChecked())
						{
							h_ph.setChecked(false);
							c_ph.setChecked(false);
							w_ph.setChecked(false);
							a_ph.setChecked(false);
							A_C_phone=A_C_phone.replace("(", "");
							A_C_phone=A_C_phone.replace(")", "");
							A_C_phone=A_C_phone.replace("-", "");
							Numbercalled.setText(A_C_phone);
						}
					}
				});
                 }
				
			}
			else
			{
				A_phone="0";
				 a_ph.setVisibility(View.INVISIBLE);
			}
		}
		catch(Exception e)
		{
			cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ Callattempt.this +" "+" in the processing stage of retrieving data from PH table  at call attempton create  "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
		
		
		this.Submitcallattempt = (Button) this.findViewById(R.id.submt);
		this.Submitandcancelinspection = (Button) this.findViewById(R.id.cancl);
		this.txtnolist = (TextView) this.findViewById(R.id.nolist);
		
		
		slist = (ListView) findViewById(R.id.list);
		
		UnablescheduleList();
		InspectionrefusedList();
		
		ResultList();
		TitleList();
		final Calendar cal = Calendar.getInstance();
		mYear = cal.get(Calendar.YEAR);
		mMonth = cal.get(Calendar.MONTH);
		mDay = cal.get(Calendar.DAY_OF_MONTH);
		
		String source = "<font color=#FFFFFF>Loading data. Please wait..."
				+ "</font>";
		ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conMgr.getActiveNetworkInfo();
	
		if (cf.isInternetOn()) {
			ichk = 0;
			String source1 = "<font color=#FFFFFF>Loading data. Please wait..."
					+ "</font>";
			
			pd = ProgressDialog.show(Callattempt.this, "",Html.fromHtml(source1), true);
			
			CallattemptDownload();
			k = 1;
			handler.sendEmptyMessage(0);
			
		} else {
			ichk = 1;
		}

	   this.getdate.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				final Calendar c = Calendar.getInstance();
				mYear = c.get(Calendar.YEAR);
				mMonth = c.get(Calendar.MONTH);
				mDay = c.get(Calendar.DAY_OF_MONTH);
				int day = c.get(Calendar.DAY_OF_WEEK);

				Calldate.setText(new StringBuilder().append(mMonth + 1)
						.append("/").append(mDay).append("/").append(mYear)
						.append(" "));
				if (day == 1) {
					Callday.setText(new StringBuilder().append("Sunday"));
				}
				if (day == 2) {
					Callday.setText(new StringBuilder().append("Monday"));
				}
				if (day == 3) {
					Callday.setText(new StringBuilder().append("Tuesday"));
				}
				if (day == 4) {
					Callday.setText(new StringBuilder().append("Wednesday"));
				}
				if (day == 5) {
					Callday.setText(new StringBuilder().append("Thursday"));
				}
				if (day == 6) {
					Callday.setText(new StringBuilder().append("Friday"));
				}
				if (day == 7) {
					Callday.setText(new StringBuilder().append("Saturday"));
				}

			}
		});
		this.Currenttime.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				Calendar c = Calendar.getInstance();
				int hours = c.get(Calendar.HOUR);
				int minutes = c.get(Calendar.MINUTE);
				int amorpm = c.get(Calendar.AM_PM);

				if (amorpm == 0) {
					Calltime.setText(new StringBuilder()
							// Month is 0 based so add 1
							.append(hours).append(":").append(minutes)
							.append(" ").append("AM").append(" "));
				}
				if (amorpm == 1) {
					Calltime.setText(new StringBuilder()
							// Month is 0 based so add 1
							.append(hours).append(":").append(minutes)
							.append(" ").append("PM").append(" "));
				}
			}
		});
		this.seldate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Calldate.setText("");
				Callday.setText("");
				showDialog(DATE_DIALOG_ID);

			}
		});

		this.Result
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {

						if (!"--Select--".equals(Result.getSelectedItem()
								.toString())) {
							resultspin = Result.getSelectedItem().toString();

							/*if ("Answered".equals(Result.getSelectedItem()
									.toString())
									|| "Re Order Left voice mail".equals(Result
											.getSelectedItem().toString())
									|| "Others".equals(Result.getSelectedItem()
											.toString())) {*/
							if("Answered".equals(Result.getSelectedItem()
									.toString())){
								
								txtPersonanswered.setVisibility(View.VISIBLE);
								Personanswered.setVisibility(View.VISIBLE);
								txtTitle.setVisibility(View.VISIBLE);
								Title.setVisibility(View.VISIBLE);
							} else {
								Noanswered();
							}
						}
						else
						{
							Noanswered();
						}
						if("Others".equals(Result.getSelectedItem()
								.toString())){
							resul_other.setVisibility(View.VISIBLE);
						}
						else
						{
							resul_other.setVisibility(View.GONE);
							resul_other.setText("");
						}

					}

					public void onNothingSelected(AdapterView<?> arg0) {

					}
				});
		this.Title
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {

						if (!"--Select--".equals(Title.getSelectedItem()
								.toString())) {
							titlespin = Title.getSelectedItem().toString();
							if ("Other".equals(Title.getSelectedItem()
									.toString())) {

								txtOther.setVisibility(View.VISIBLE);
								OtherTitle.setVisibility(View.VISIBLE);

							} else {
								txtOther.setVisibility(View.GONE);
								OtherTitle.setVisibility(View.GONE);
							}
						}
						else
						{
							txtOther.setVisibility(View.GONE);
							OtherTitle.setVisibility(View.GONE);
						}

					}

					public void onNothingSelected(AdapterView<?> arg0) {

					}
				});
		this.Submitcallattempt.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {

				if (!"".equals(Calldate.getText().toString())
						&& !"".equals(Calltime.getText().toString())
						&& !"--Select--".equals(Result.getSelectedItem()
								.toString())
						&& !"".equals(Numbercalled.getText().toString())) {
					if (checkfordateistoday(Calldate.getText().toString()) == "true") {
						if (checkforassigndate(assdate, Calldate.getText()
								.toString()) == "true") {
							if (strhmephnvalid(Numbercalled.getText()
									.toString()) == "yes") {
								phnnum = Numbercalled.getText().toString();
								if ("Answered".equals(Result.getSelectedItem()
										.toString())) {
									
									if (!"".equals(Calldate.getText()
											.toString())
											&& !"--Select--".equals(Title
													.getSelectedItem()
													.toString()) && !Personanswered.getText().toString().trim().equals("")) {
										if (!"Other".equals(Title.getSelectedItem()
												.toString()) || !OtherTitle.getText().toString().trim().equals("")) {
											
											submitprocess1();
										}
										else
										{
											cf.ShowToast("Please enter other text value for title", 1);
										}

									} else {
										/*ShowToast toast = new ShowToast(
												
												"Please enter * field.");*/
										if ("--Select--".equals(Title.getSelectedItem().toString()))
										{
											cf.ShowToast("Please select Title.",1);
										}
										if(Personanswered.getText().toString().trim().equals(""))
										{
											cf.ShowToast("Please enter Person answered.",1);
										}
										
									}

								} else {
									if(("Others".equals(Result.getSelectedItem()
											.toString())) && resul_other.getText().toString().trim().equals(""))
									{
										cf.ShowToast("Please enter other text value for Result", 1);
										
									}
									else if ((!"Other".equals(Title.getSelectedItem()
											.toString()) || !OtherTitle.getText().toString().trim().equals(""))) {
										Noanswered();
										submitprocess1();
									}
									else
									{
										cf.ShowToast("Please enter other text value for title", 1);
									}
									

								}
							} else {
								Numbercalled.setText("");
								Numbercalled.requestFocus();
								
								cf.ShowToast("Please enter the Called Number in 10 digits.",1);

							}
						} else {
							cf.ShowToast("Call Date should be greater than or equal to Assign Date.",1);

							Calldate.setText("");
							Calldate.requestFocus();
						}
					} else {
						cf.ShowToast("Call Date should not be greater than Today's Date.",1);
						Calldate.setText("");
						Calldate.requestFocus();
					}
				} else {
					/*cf.ShowToast(
							"Please enter * field.");*/
					
					if ("".equals(Calldate.getText().toString()))
					{
						cf.ShowToast("Please enter Call Date.",1);
					}
					else if("".equals(Calltime.getText().toString()))
					{
						cf.ShowToast("Please enter Call Time.",1);
					}
					else if("--Select--".equals(Result.getSelectedItem().toString()))
					{
						cf.ShowToast("Please select Result",1);
					}
					else if("".equals(Numbercalled.getText().toString()))
					{
						cf.ShowToast("Please enter Number Called.",1);
					}

				}
			}

		});
		this.Submitandcancelinspection
				.setOnClickListener(new View.OnClickListener() {

					public void onClick(View arg0) {

						if (chkbx1.isChecked()
								&& !"--Select--".equals(Unableschedule
										.getSelectedItem().toString())
								&& !"".equals(Initialcomments.getText()
										.toString())) {
							if(!etgen.getText().toString().trim().equals(""))
							{
							if ("No Contact with Policyholder"
									.equals(Unableschedule.getSelectedItem()
											.toString())
									|| "Scheduled then Cancelled"
											.equals(Unableschedule
													.getSelectedItem()
													.toString())) {
								if (cnt >= 3) {

									validation();
								} else {
									cf.ShowToast("Please submit minimum three Call Attempts.",1);

								}

							} else {
								validation();
							}
							}
							else
							{
								cf.ShowToast("Please enter general comment.",1);
							}
						} else if (chkbx2.isChecked()
								&& !"--Select--".equals(Inspectionrefused
										.getSelectedItem().toString())
								&& !"".equals(Initialcomments.getText()
										.toString())) {
							if(!etgen.getText().toString().trim().equals(""))
							{
						
							validation();
							}
							else
							{
								cf.ShowToast("Please enter general comment.",1);
							}
						} else {
							cf.ShowToast("Please select either Unable to Schedule or Inspection Refused option.",1);

						}
					}
				});
		 OtherTitle.setOnTouchListener(new Touch_Listener(3));
	}
	  class Touch_Listener implements OnTouchListener
		{
			   public int type;
			   Touch_Listener(int type)
				{
					this.type=type;
					
				}
			    @Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
			    	if(this.type==1)
					{
			    		cf.setFocus(Initialcomments);
					}
			    	else if(this.type==2)
			    	{
			    		cf.setFocus(resul_other);
			    	}
			    	else if(this.type==3)
			    	{
			    		cf.setFocus(OtherTitle);
			    	}
					return false;
				}
			   
		}
	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.chk1:
				c = 1;
				chkbx1.setChecked(true);
				chkbx2.setChecked(false);
				chkbx2.setSelected(false);
				Unableschedule.setEnabled(true);
				Inspectionrefused.setEnabled(false);
				tvgen.setVisibility(visibility);
				etgen.setVisibility(visibility);
				Inspectionrefused.setSelection(0);

				break;

			case R.id.chk2:
				c = 2;
				chkbx2.setChecked(true);
				chkbx1.setSelected(false);
				chkbx1.setChecked(false);
				Unableschedule.setEnabled(false);
				Inspectionrefused.setEnabled(true);
				tvgen.setVisibility(visibility);
				etgen.setVisibility(visibility);
				Unableschedule.setSelection(0);

				break;
			}
		}
	};

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;

			Calldate.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(mMonth + 1).append("/").append(mDay).append("/")
					.append(mYear).append(" "));
			int no_day = mDay + Month(mMonth + 1, mYear);
			int y1 = mYear - 1900;
			int leap = (y1 - 1) / 4;
			y1 = (y1 * 365) + leap;
			no_day += y1;
			int day = (no_day) % 7;
			switch (day) {
			case 0:
				day = 1;
				break;
			case 1:
				day = 2;
				break;
			case 2:
				day = 3;
				break;
			case 3:
				day = 4;
				break;
			case 4:
				day = 5;
				break;
			case 5:
				day = 6;
				break;
			case 6:
				day = 7;
				break;
			}
			if (day == 1) {
				Callday.setText(new StringBuilder().append("Sunday"));
			}
			if (day == 2) {
				Callday.setText(new StringBuilder().append("Monday"));
			}
			if (day == 3) {
				Callday.setText(new StringBuilder().append("Tuesday"));
			}
			if (day == 4) {
				Callday.setText(new StringBuilder().append("Wednesday"));
			}
			if (day == 5) {
				Callday.setText(new StringBuilder().append("Thursday"));
			}
			if (day == 6) {
				Callday.setText(new StringBuilder().append("Friday"));
			}
			if (day == 7) {
				Callday.setText(new StringBuilder().append("Saturday"));
			}

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);
		}
		return null;
	}

	protected int Month(int month, int year) {
		// TODO Auto-generated method stub
		int days = 0, m = month, y = year;
		if (m > 2) {
			if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0) {
				days += 1;
			}
		}
		for (int i = 1; i < m; i++) {
			if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10
					|| i == 12) {
				days += 31;
			} else {
				if (i == 4 || i == 6 || i == 9 || i == 11) {
					days += 30;
				} else {
					if (i == 2) {
						days += 28;
					}
				}
			}
		} // no of days in months

		return days;
	}

	private void validation() {
		if (!"".equals(Calldate.getText().toString())
				&& !"".equals(Calltime.getText().toString())
				&& !"--Select--".equals(Result.getSelectedItem().toString())
				&& !"".equals(Numbercalled.getText().toString())) {
			if (checkfordateistoday(Calldate.getText().toString()) == "true") {
				if ("Answered".equals(Result.getSelectedItem().toString())) {
					if (!"".equals(Calldate.getText().toString())
							&& !"--Select--".equals(Title.getSelectedItem()
									.toString()) && !Personanswered.getText().equals("")) {
						if (strhmephnvalid(Numbercalled.getText().toString()) == "yes") {
							phnnum = Numbercalled.getText().toString();
							if ("Answered".equals(Result.getSelectedItem()
									.toString())) {
								if (!"".equals(Calldate.getText().toString())
										&& !"--Select--".equals(Title
												.getSelectedItem().toString()) && !Personanswered.getText().toString().trim().equals("")) {
									if (!"Other".equals(Title.getSelectedItem()
											.toString()) || !OtherTitle.getText().toString().trim().equals("")) {
										
										submitprocess();
									}
									else
									{
										cf.ShowToast("Please enter other text for title.", 1);
										OtherTitle.requestFocus();
									}
								
								} else {
									
									
									if(Personanswered.getText().toString().trim().equals(""))
									{
										cf.ShowToast("Please enter Person answered.",1);
										Personanswered.requestFocus();
									}
									if ("--Select--".equals(Title.getSelectedItem().toString()))
									{
										cf.ShowToast("Please select Title.",1);
									}
									
								}

							} else {
								if(("Others".equals(Result.getSelectedItem()
										.toString())) && resul_other.getText().toString().trim().equals(""))
								{
									cf.ShowToast("Please enter other text value for Result", 1);
									
								}
								else if ((!"Other".equals(Title.getSelectedItem()
										.toString()) || !OtherTitle.getText().toString().trim().equals(""))){
									
									Noanswered();
									submitprocess();

								}
								else
								{
									cf.ShowToast("Please enter other text value for title", 1);
								}
								
							}
						} else {
							Numbercalled.setText("");
							Numbercalled.requestFocus();
							cf.ShowToast("Please enter the Called Number in 10 digits.",1);

						}

					} else {
						/*cf.ShowToast(
								"Please enter * field.");*/
						if ("--Select--".equals(Title.getSelectedItem().toString()))
						{
							cf.ShowToast("Please select Title.",1);
						}
						
					}

				} else {
					if(!("Others".equals(Result.getSelectedItem()
							.toString())) || !resul_other.getText().toString().trim().equals(""))
					{

					if (strhmephnvalid(Numbercalled.getText().toString()) == "yes") {
						phnnum = Numbercalled.getText().toString();

						submitprocess();
					} else {
						Numbercalled.setText("");
						Numbercalled.requestFocus();
						cf.ShowToast("Please enter the Called Number in 10 digits.",1);
					}
					}
					else
					{
						cf.ShowToast("Please enter other text value for Result", 1);
					}

				}

			} else {
				cf.ShowToast("Call Date should not be greater than Today's Date.",1);
			}
		} else {
			/*cf.ShowToast(
					"Please enter * field.");*/
			if ("".equals(Calldate.getText().toString()))
			{
				cf.ShowToast("Please enter Call Date.",1);
			}
			else if("".equals(Calltime.getText().toString()))
			{
				cf.ShowToast("Please enter Call Time.",1);
			}
			else if("--Select--".equals(Result.getSelectedItem().toString()))
			{
				cf.ShowToast("Please select Result",1);
			}
			else if("".equals(Numbercalled.getText().toString()))
			{
				cf.ShowToast("Please enter Number Called.",1);
			}
		}

	}

	private String checkfordateistoday(String getinspecdate2) {
		// TODO Auto-generated method stub
		String chkdate = null;
		int i1 = getinspecdate2.indexOf("/");
		String result = getinspecdate2.substring(0, i1);
		int i2 = getinspecdate2.lastIndexOf("/");
		String result1 = getinspecdate2.substring(i1 + 1, i2);
		String result2 = getinspecdate2.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);
		final Calendar c = Calendar.getInstance();
		int thsyr = c.get(Calendar.YEAR);
		int curmnth = c.get(Calendar.MONTH);
		int curdate = c.get(Calendar.DAY_OF_MONTH);
		int day = c.get(Calendar.DAY_OF_WEEK);
		curmnth = curmnth + 1;

		if (j < thsyr || (j1 < curmnth && j <= thsyr)

		|| (j2 <= curdate && j1 <= curmnth && j <= thsyr)) {
			chkdate = "true";
		} else {
			chkdate = "false";
		}

		return chkdate;

	}

	private String checkforassigndate(String getinsurancedate,
			String getinspectiondate) {
		try
		{
		if (!getinsurancedate.trim().equals("")
				|| getinsurancedate.equals("N/A")
				|| getinsurancedate.equals("Not Available")
				|| getinsurancedate.equals("anytype")
				|| getinsurancedate.equals("Null")) {
			String chkdate = null;
			int i1 = getinsurancedate.indexOf("/");
			String result = getinsurancedate.substring(0, i1);
			int i2 = getinsurancedate.lastIndexOf("/");
			String result1 = getinsurancedate.substring(i1 + 1, i2);
			String result2 = getinsurancedate.substring(i2 + 1);
			result2 = result2.trim();
			int j1 = Integer.parseInt(result);
			int j2 = Integer.parseInt(result1);
			int j = Integer.parseInt(result2);

			int i3 = getinspectiondate.indexOf("/");
			String result3 = getinspectiondate.substring(0, i3);
			int i4 = getinspectiondate.lastIndexOf("/");
			String result4 = getinspectiondate.substring(i3 + 1, i4);
			String result5 = getinspectiondate.substring(i4 + 1);
			result5 = result5.trim();
			int k1 = Integer.parseInt(result3);
			int k2 = Integer.parseInt(result4);
			int k = Integer.parseInt(result5);

			if (j > k) {
				chkdate = "false";
			} else if (j < k) {
				chkdate = "true";
			} else if (j == k) {

				if (j1 > k1) {

					chkdate = "false";
				} else if (j1 < k1) {
					chkdate = "true";
				} else if (j1 == k1) {
					if (j2 > k2) {
						chkdate = "false";
					} else if (j2 < k2) {
						chkdate = "true";
					} else if (j2 == k2) {

						chkdate = "true";
					}

				}

			}

			return chkdate;
		} else {
			return "true";
		}
		}
		catch (Exception e)
		{
			return "true";
		}
	}

	private String strhmephnvalid(String strhmephn2) {

		String chk;
		if (strhmephn2.length() == 10) {
			StringBuilder sVowelBuilder = new StringBuilder(strhmephn2);
			sVowelBuilder.insert(0, "(");
			sVowelBuilder.insert(4, ") ");
			sVowelBuilder.insert(9, " ");
			phnnum = sVowelBuilder.toString();

			chk = "yes";

		} else {
			chk = "no";
		}

		return chk;
	}

	private void newCallattemptDownload() {
		View v = null;
		try {
			SoapObject request = new SoapObject(cf.NAMESPACE, "LoadCallAttempt");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID", cf.selectedhomeid.toString());
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			androidHttpTransport.call("http://tempuri.org/LoadCallAttempt", envelope);
			res = (SoapObject) envelope.getResponse();
			
			if (res != null) {
				cnt = res.getPropertyCount();
				if (cnt != 0) {
					strcaldat = "";
					strcalday = "";
					strcaltime = "";
					strcalnum = "";
					strcalres = "";
					strcaltit = "";
					strcalcom = "";
					HorizontalScrollView.LayoutParams mParam = new HorizontalScrollView.LayoutParams(LayoutParams.FILL_PARENT,(int)((cnt*40)+20));
		     		slist.setLayoutParams(mParam);
					for (int i = 0; i < cnt; i++) {
						SoapObject obj = (SoapObject) res.getProperty(i);

						strcaldat += String.valueOf(obj
								.getProperty("CallAttemptDate")) + "~";
						strcalday += String.valueOf(obj
								.getProperty("CallAttemptDay")) + "~";
						strcaltime += String.valueOf(obj
								.getProperty("CallAttemptTime")) + "~";
						strcalnum += String.valueOf(obj
								.getProperty("CallAttemptNumberCalled")) + "~";
						strcalres += String.valueOf(obj
								.getProperty("CallAttemptResult")) + "~";
						strcaltit += String.valueOf(obj
								.getProperty("CallAttemptTitle")) + "~";
						strcalcom += String.valueOf(obj
								.getProperty("CallAttemptIntialsComment"))
								+ "~";
						if (strcalcom.contains(anytype)) {
							strcalcom = strcalcom.replace(anytype, "NILL");
						}
						if (strcalnum.contains(anytype)) {
							strcalnum = strcalnum.replace(anytype, "NILL");
						}
					}
					
					arrdat = strcaldat.split("~");
					arrday = strcalday.split("~");
					arrtime = strcaltime.split("~");
					arrnum = strcalnum.split("~");
					arrres = strcalres.split("~");
					arrcom = strcalcom.split("~");
					arrtit = strcaltit.split("~");
					txtnolist.setVisibility(v.GONE);
					slist.setVisibility(visibility);
					slist.setAdapter(new EfficientAdapter(getApplicationContext()));
				} else {
					txtnolist.setVisibility(visibility);
					slist.setVisibility(v.GONE);
					txtnolist.setText("No call attempt information yet");
				}
			} else {
				pd.dismiss();
				verify = 3;
				cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
			}
		} catch (Exception e) {
			
		}

	}


		

	private void CallattemptDownload() {
		View v = null;
		try {
			SoapObject request = new SoapObject(cf.NAMESPACE, "LoadCallAttempt");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID", cf.selectedhomeid.toString());
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			try {
				androidHttpTransport.call("http://tempuri.org/LoadCallAttempt", envelope);
			} catch (Exception e) {
				throw e;
			}
			res = (SoapObject) envelope.getResponse();
			cnt = res.getPropertyCount();

		} catch (SocketTimeoutException s) {
			cf.ShowToast("Please check your network connection and try again.",1);

		} catch (NetworkErrorException n) {
			cf.ShowToast("Please check your network connection and try again.",1);
		} catch (IOException io) {
			cf.ShowToast("Please check your network connection and try again.",1);
		} catch (XmlPullParserException x) {
			cf.ShowToast("Please check your network connection and try again.",1);
		} catch (Exception e) {
			cf.ShowToast("Please check your network connection and try again.",1);
		}
	}

	public static class EfficientAdapter extends BaseAdapter {
		private static final String TAG = null;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;

		public EfficientAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
		}

		public int getCount() {
			int retlen = 0;
			retlen = arrdat.length;

			return retlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {

				holder = new ViewHolder();

				convertView = mInflater.inflate(R.layout.scorelistview, null);
				holder.tv = (TextView) convertView
						.findViewById(R.id.grid_item_text);
				holder.tv1 = (TextView) convertView
						.findViewById(R.id.grid_item_text1);
				holder.tv2 = (TextView) convertView
						.findViewById(R.id.grid_item_text2);
				holder.tv3 = (TextView) convertView
						.findViewById(R.id.grid_item_text3);
				holder.tv4 = (TextView) convertView
						.findViewById(R.id.grid_item_text4);
				holder.tv5 = (TextView) convertView
						.findViewById(R.id.grid_item_text5);
				holder.tv6 = (TextView) convertView
						.findViewById(R.id.grid_item_text6);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {

				if ((arrdat[position]).contains("null")
						|| arrday[position].contains("null")
						|| arrtime[position].contains("null")
						|| arrnum[position].contains("null")
						|| arrres[position].contains("null")
						|| arrcom[position].contains("null")
						|| arrtit[position].contains("null")) {
					arrdat[position] = arrdat[position].replace("null", "");
					arrday[position] = arrday[position].replace("null", "");
					arrtime[position] = arrtime[position].replace("null", "");
					arrnum[position] = arrnum[position].replace("null", "");
					arrres[position] = arrres[position].replace("null", "");
					arrcom[position] = arrcom[position].replace("null", "");
					arrtit[position] = arrtit[position].replace("null", "");

				}
				if (arrday[position].equals("1")) {
					arrday[position] = "Monday";
				} else if (arrday[position].equals("2")) {
					arrday[position] = "Tuesday";
				} else if (arrday[position].equals("3")) {
					arrday[position] = "Wednesday";
				} else if (arrday[position].equals("4")) {
					arrday[position] = "Thursday";
				} else if (arrday[position].equals("5")) {
					arrday[position] = "Friday";
				} else if (arrday[position].equals("6")) {
					arrday[position] = "Saturday";
				} else if (arrday[position].equals("7")) {
					arrday[position] = "Sunday";
				}
				if (arrres[position].equals("Answered") || arrres[position].equals("Others") ) {
					arrtit[position] = arrtit[position];
				} else {
					arrtit[position] = "NIL";
				}
				holder.tv.setText(arrdat[position]);
				holder.tv1.setText(arrday[position]);
				holder.tv2.setText(arrtime[position]);
				holder.tv3.setText(arrnum[position]);
				holder.tv4.setText(arrres[position]);
				holder.tv5.setText(arrtit[position]);
				holder.tv6.setText(arrcom[position]);

			} catch (Exception e) {
				
			}

			return convertView;
		}

		static class ViewHolder {
			TextView tv, tv1, tv2, tv3, tv4, tv5, tv6;

		}

	}

	protected void submitprocess() {

		if (ichk == 0) {
			pd = ProgressDialog.show(Callattempt.this, "",
					"Submitting Call Attempt...");

			new Thread() {
				private boolean toastst = true;

				public void run() {
					Looper.prepare();
					try {
						cf.Chk_Inspector=cf.fn_CurrentInspector(cf.Insp_id,cf.selectedhomeid);
						if(cf.Chk_Inspector.equals("true"))
						{
							if (CancelStatusCheck() == "true") {
								verify = 2;
	
								handler.
	
								sendEmptyMessage(0);
								pd.dismiss();
							} else {
								handler.
	
								sendEmptyMessage(0);
								pd.dismiss();
							}
						}	else
							{
								verify = 3;
								handler.sendEmptyMessage(0);
								pd.dismiss();
							}
					} catch (Exception e) {

					}
				}

				private String CancelStatusCheck() {

					String chk;
					try {
						

						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope.dotNet = true;
						SoapObject ad_property = new SoapObject(cf.NAMESPACE,
								"ExportUTSCallAttempt");

						ad_property.addProperty("InspectorID", cf.Insp_id);
						
						ad_property.addProperty("Srid",cf.selectedhomeid);
						ad_property.addProperty("CallDate", Calldate.getText()
								.toString());
						ad_property.addProperty("CallTime", Calltime.getText()
								.toString());
						if (phnnum.length() == 10) {
							StringBuilder sVowelBuilder = new StringBuilder(
									phnnum);
							sVowelBuilder.insert(3, " ");
							sVowelBuilder.insert(7, " ");
							phnnum = sVowelBuilder.toString();

						}
						ad_property.addProperty("NumberCalled", phnnum);
						int start = Result.getSelectedItemPosition();
						ad_property.addProperty("CallAttemptResultId", start);
						if (start == 1 || start == 7) {
							ad_property.addProperty("PersonAnswered",
									Personanswered.getText().toString());
							int end = Title.getSelectedItemPosition();
							//ad_property.addProperty("CallAttemptTitleId", end);
							if (end == 7) {
								ad_property.addProperty("PersonAnswered", "");
								ad_property.addProperty("CallAttemptTitleId", end);
								ad_property.addProperty("OtherTitle",
										OtherTitle.getText().toString());
							} else if(start == 7) {
								try
								{
									ad_property.addProperty("PersonAnswered", "");
									ad_property.addProperty("CallAttemptTitleId", 7);
									ad_property.addProperty("OtherTitle", resul_other.getText().toString());
								}
								catch (Exception e)
								{
									ad_property.addProperty("OtherTitle", "");	
									cf.Error_LogFile_Creation("error tracking at the callattempt when i send the result as oters text value ");
								}
							}
							else
							{
								ad_property.addProperty("PersonAnswered", "");
								ad_property.addProperty("CallAttemptTitleId", 8);
								ad_property.addProperty("OtherTitle", "");
							}

						}
						
						else
						{
							ad_property.addProperty("PersonAnswered", "");
							ad_property.addProperty("CallAttemptTitleId", 8);
							ad_property.addProperty("OtherTitle", "");
						}

						ad_property.addProperty("Comments", Initialcomments
								.getText().toString());
						ad_property.addProperty("CreatedDate", cd.toString());
						ad_property.addProperty("ModifiedDate", md.toString());
						if (c == 0) {
							ad_property.addProperty("UnableToSchedule", false);
							ad_property.addProperty("InspectionRefused", false);
							ad_property.addProperty("ddlReason", "");
							ad_property.addProperty("ddlRefusedReason", "");
							ad_property.addProperty("GeneralComment", "");

						} else if (c == 1) {
							ad_property.addProperty("UnableToSchedule", true);
							ad_property.addProperty("ddlReason", Unableschedule
									.getSelectedItem().toString());
							ad_property.addProperty("GeneralComment", etgen
									.getText().toString());
							ad_property.addProperty("InspectionRefused", false);
							ad_property.addProperty("ddlRefusedReason", "");

						} else if (c == 2) {
							ad_property.addProperty("UnableToSchedule", false);
							ad_property.addProperty("ddlReason", "");
							ad_property.addProperty("GeneralComment", etgen
									.getText().toString());
							ad_property.addProperty("InspectionRefused", true);
							ad_property.addProperty("ddlRefusedReason",
									Inspectionrefused.getSelectedItem()
											.toString());
						}
						envelope.setOutputSoapObject(ad_property);
						HttpTransportSE androidHttpTransport1 = new HttpTransportSE(
								cf.URL);
						
						try {
							androidHttpTransport1.call("http://tempuri.org/ExportUTSCallAttempt",
									envelope);
						} catch (Exception e) {
							throw e;
						}
						SoapObject result = (SoapObject) envelope.bodyIn;
						String result1 = String.valueOf(envelope.getResponse());
						if (result1
								.contains("Inspection Status updated successfully")) {
							if (c == 1) {
								cf.sh_db.execSQL("UPDATE "
										+ cf.policyholder
										+ " SET  SH_PH_Status='110' where SH_PH_SRID='"
										+ cf.encode(cf.selectedhomeid) + "'");
							} else if (c == 2) {
								cf.sh_db.execSQL("UPDATE "
										+ cf.policyholder
										+ " SET  SH_PH_Status='110' where SH_PH_SRID='"
										+ cf.encode(cf.selectedhomeid) + "'");
							}

							Toast toast = Toast.makeText(
									getApplicationContext(),
									Html.fromHtml(result1.toString()),
									Toast.LENGTH_LONG);
							TextView v = (TextView) toast.getView()
									.findViewById(android.R.id.message);
							v.setTextColor(Color.YELLOW);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							Intent Star = new Intent(getApplicationContext(),
									HomeScreen.class);
							Star.putExtra("keyName", 1);
							startActivity(Star);
						}

						else {
							Toast toast = Toast.makeText(
									getApplicationContext(),
									Html.fromHtml(result1.toString()),
									Toast.LENGTH_LONG);
							TextView v = (TextView) toast.getView()
									.findViewById(android.R.id.message);
							v.setTextColor(Color.YELLOW);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							toastst = false;
						}
						chk = "true";
					} catch (SocketTimeoutException s) {
						verify = 1;
						chk = "false";

					} catch (NetworkErrorException n) {

						verify = 1;
						chk = "false";
					} catch (IOException io) {
						verify = 1;
						chk = "false";
					} catch (XmlPullParserException x) {
						verify = 1;
						chk = "false";
					} catch (Exception e) {
						chk = "false";
						System.out.println("Issues comes here ");

					}

					return chk;
				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {

						if (verify == 2 && toastst) {
							cf.ShowToast("Call Attempt has been submitted sucessfully.",1);
							Calldate.setText("");
							Callday.setText("");
							Calltime.setText("");
							//Numbercalled.setText("");
							Personanswered.setText("");
							etgen.setText("");
							Inspectionrefused.setSelection(0);
							Unableschedule.setSelection(0);
							Unableschedule.setEnabled(false);
							Inspectionrefused.setEnabled(false);
							chkbx1.setChecked(false);
							chkbx2.setChecked(false);
							Result.setSelection(0);
							Noanswered();
							newCallattemptDownload();
						} else if (verify == 1) {
							cf.ShowToast("Please check your network connection and try again.",1);

						} else {
							if (toastst) {
								System.out.println("Issues comes here = "+verify);
								cf.ShowToast("Call Attempt has not been submitted. Please try again later.",1);
								verify = 0;
								newCallattemptDownload();
							}

						}

					}
				};
			}.start();
		} else {
			cf.ShowToast(
					"Internet connection is not available.",1);

		}
	}

	protected void submitprocess1() {
      
		if (ichk == 0) {
			pd = ProgressDialog.show(Callattempt.this, "",
					"Submitting Call Attempt...");

			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						cf.Chk_Inspector=cf.fn_CurrentInspector(cf.Insp_id,cf.selectedhomeid);
						if(cf.Chk_Inspector.equals("true"))
						{
							if (CancelStatusCheck1() == "true") {
								verify = 2;
								handler1.sendEmptyMessage(0);
								pd.dismiss();
							} else {
								pd.dismiss();
								handler1.sendEmptyMessage(0);
	
							}
						}
						else
						{
							verify = 3;
							handler1.sendEmptyMessage(0);
							pd.dismiss();
						}
					} catch (Exception e) {

					}
				}

				private String CancelStatusCheck1() {
					String chk;
					try {

						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope.dotNet = true;
						SoapObject ad_property = new SoapObject(cf.NAMESPACE,
								"ExportCallAttempt");

						ad_property.addProperty("InspectorID", cf.Insp_id);
						ad_property.addProperty("Srid", cf.selectedhomeid);
						ad_property.addProperty("CallDate", Calldate.getText()
								.toString());
						ad_property.addProperty("CallTime", Calltime.getText()
								.toString());
						if (phnnum.length() == 10) {
							StringBuilder sVowelBuilder = new StringBuilder(
									phnnum);
							sVowelBuilder.insert(3, " ");
							sVowelBuilder.insert(7, " ");
							phnnum = sVowelBuilder.toString();

						}
						ad_property.addProperty("NumberCalled", phnnum);
						int start = Result.getSelectedItemPosition();
						ad_property.addProperty("CallAttemptResultId", start);
						if (start == 1 || start == 6 ) {
							ad_property.addProperty("PersonAnswered",
									Personanswered.getText().toString());
							int end = Title.getSelectedItemPosition();
							ad_property.addProperty("CallAttemptTitleId", end);
							if (end == 7) {
								ad_property.addProperty("OtherTitle",
										OtherTitle.getText().toString());
							} 
							else {
								ad_property.addProperty("OtherTitle", "");
							}

						} else if(start == 7) {
							try
							{
							ad_property.addProperty("PersonAnswered", "");
							ad_property.addProperty("CallAttemptTitleId", 7);
							ad_property.addProperty("OtherTitle", resul_other.getText().toString());
							}
							catch (Exception e)
							{
								ad_property.addProperty("OtherTitle", "");	
								cf.Error_LogFile_Creation("error tracking at the callattempt when i send the result as oters text value ");
							}
						} else {
							ad_property.addProperty("PersonAnswered", "");
							ad_property.addProperty("CallAttemptTitleId", 8);
							ad_property.addProperty("OtherTitle", "");
						}

						ad_property.addProperty("Comments", Initialcomments
								.getText().toString());
						ad_property.addProperty("CreatedDate", cd.toString());
						ad_property.addProperty("ModifiedDate", md.toString());
						System.out.println("dshjfvg"+ad_property);
						envelope.setOutputSoapObject(ad_property);
						HttpTransportSE androidHttpTransport1 = new HttpTransportSE(
								cf.URL);
						
						try {
							androidHttpTransport1.call("http://tempuri.org/ExportCallAttempt",
									envelope);
							SoapObject result = (SoapObject) envelope.bodyIn;
							String result1 = String.valueOf(envelope
									.getResponse());
							System.out.println("result1"+result1);
							chk = "true";
						} catch (Exception e) {
							
							throw e;
						}

					}

					catch (SocketTimeoutException s) {
						verify = 1;
						chk = "false";
						System.out.println("Issues comes here s="+s);
					} catch (NetworkErrorException n) {
						verify = 1;
						chk = "false";
						System.out.println("Issues comes here n"+n);
					} catch (IOException io) {
						verify = 1;
						chk = "false";
						System.out.println("Issues comes here io"+io);
					} catch (XmlPullParserException x) {
						verify = 1;
						chk = "false";
						System.out.println("Issues comes here x"+x);
					} catch (Exception e) {
						verify = 1;
						System.out.println("Issues comes here e"+e);
						chk = "false";
                 	}

					return chk;
				}

				private Handler handler1 = new Handler() {
					
					@Override
					public void handleMessage(Message msg) {
						System.out.println("verify"+verify);
						if (verify == 2) {
							Calldate.setText("");
							Callday.setText("");
							Calltime.setText("");
							Personanswered.setText("");
							etgen.setText("");
							Inspectionrefused.setSelection(0);
							Unableschedule.setSelection(0);
							Unableschedule.setEnabled(false);
							Inspectionrefused.setEnabled(false);
							chkbx1.setChecked(false);
							chkbx2.setChecked(false);
							Result.setSelection(0);
							Noanswered();
							newCallattemptDownload();
							cf.ShowToast(
									"Call Attempt has been submitted sucessfully.",1);
							
						}
						else if(verify==3){
							cf.ShowToast("Sorry. This record has been reallocated to another inspector.", 1);
							
						}
						else {
							cf.ShowToast(
									"Call Attempt has not been submitted. Please try again later.",1);
							verify = 0;

						}

					}
				};
			}.start();
		} else {
			cf.ShowToast(
					"Internet connection is not available.",1);

		}
	}

	private void UnablescheduleList() {
		Unableschedule = (Spinner) findViewById(R.id.spunableschedule);
		loUnableschedule = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loUnableschedule
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Unableschedule.setAdapter(loUnableschedule);
		loUnableschedule.add("--Select--");
		loUnableschedule.add("Bad Contact Information");
		loUnableschedule.add("No Contact with Policyholder");
		loUnableschedule.add("Scheduled then Cancelled");
		loUnableschedule.add("Seasonal Resident");
		loUnableschedule.add("Policyholder on Military Deployment");
		loUnableschedule.add("Not Insured by Carrier Anymore");
	}

	private void InspectionrefusedList() {
		Inspectionrefused = (Spinner) findViewById(R.id.spinspectionrefused);
		loInspectionrefused = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loInspectionrefused
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Inspectionrefused.setAdapter(loInspectionrefused);
		loInspectionrefused.add("--Select--");
		loInspectionrefused.add("Out of Coverage Area");
		loInspectionrefused.add("Unable to meet Cycle Times");
		loInspectionrefused.add("Conflict � Did Original Inspection");

	}

	private void ResultList() {
		Result = (Spinner) findViewById(R.id.spresult);
		loResult = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loResult.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Result.setAdapter(loResult);
		loResult.add("--Select--");
		loResult.add("Answered");
		loResult.add("No Answer");
		loResult.add("Left Voicemail");
		loResult.add("Number Disconnected");
		loResult.add("Answered & Disconnected");
		loResult.add("Re Order Left voice mail");
		loResult.add("Others");

	}

	private void TitleList() {
		//Title = (Spinner) findViewById(R.id.sptitle);
		loTitle = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loTitle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Title.setAdapter(loTitle);
		loTitle.add("--Select--");
		loTitle.add("Policyholder");
		loTitle.add("Agent");
		loTitle.add("Agency CSR");
		loTitle.add("Property Manager");
		loTitle.add("Family Member");
		loTitle.add("Realtor");
		loTitle.add("Other");
		Title.setSelection(0);
	}

	private void Noanswered() {
		try
		{
			txtPersonanswered.setVisibility(View.GONE);
			Personanswered.setVisibility(View.GONE);
			txtTitle.setVisibility(View.GONE);
			Title.setVisibility(View.GONE);
			Title.setSelection(0);
			txtOther.setVisibility(View.GONE);
			OtherTitle.setVisibility(View.GONE);
			OtherTitle.setText("");
			txtOther.setText("");
			Personanswered.setText("");
		}
		catch(Exception e)
		{
			
		}

	}

	public void clicker(View v) {
		//cf.ShowToast("clicker worked", 1);
		final Dialog dialog1;
		TextView txt_v;
		Button bt_save;
		final EditText ed_number;
		switch (v.getId()) {

		case R.id.hme:
					cf.gohome();
		break;
		case R.id.c_edit_H_ph:
		  dialog1=showalert();
		   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
		    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
		   ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
		  txt_v.setText("Please Enter the Home phone number ");
		  H_phone=H_phone.replace("(", "");
			H_phone=H_phone.replace(")", "");
			H_phone=H_phone.replace("-", "");
		  ed_number.setText(H_phone);
		  bt_save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try
				{
					if(!ed_number.getText().toString().trim().equals("") )
					{
						if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
						{
							
							cf.sh_db.execSQL("update "+cf.policyholder+" set SH_PH_HomePhone='"+cf.encode(cf.newphone)+"' where SH_PH_SRID='"+cf.selectedhomeid+"'");
							h_ph.setText(cf.newphone);
							cf.newphone=cf.newphone.replace("(", "");
							cf.newphone=cf.newphone.replace(")", "");
							cf.newphone=cf.newphone.replace("-", "");
							H_phone=cf.newphone;
							dialog1.setCancelable(true);
							dialog1.dismiss();
							cf.ShowToast("Phone number Updated Successfully  ", 1);
							
						}
						else
						{
							cf.ShowToast("Please enter valid phone number  ", 1);
						}
						
					}
					else
					{
						cf.ShowToast("Please enter valid phone number  ", 1);
					}
				}
				catch(Exception e)
				{
					cf.ShowToast("Phone number not updated  ", 1);
					dialog1.setCancelable(true);
					dialog1.dismiss();
					System.out.println("Error in the query"+e.getMessage());
					cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
				}
			}
		});
		  dialog1.setCancelable(false);
		  dialog1.show();
		break;
		case R.id.c_edit_W_ph:
			  dialog1=showalert();
			   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
			    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
			   ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
			  txt_v.setText("Please Enter the Work phone number ");
			  	W_phone=W_phone.replace("(", "");
				W_phone=W_phone.replace(")", "");
				W_phone=W_phone.replace("-", "");
			  ed_number.setText(W_phone);
			  bt_save.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try
					{
						if(!ed_number.getText().toString().trim().equals("") )
						{
							if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
							{
								
								cf.sh_db.execSQL("update "+cf.policyholder+" set SH_PH_WorkPhone='"+cf.encode(cf.newphone)+"' where SH_PH_SRID='"+cf.selectedhomeid+"'");
								w_ph.setText(cf.newphone);
								cf.newphone=cf.newphone.replace("(", "");
								cf.newphone=cf.newphone.replace(")", "");
								cf.newphone=cf.newphone.replace("-", "");
								W_phone=cf.newphone;
								dialog1.setCancelable(true);
								dialog1.dismiss();
								cf.ShowToast("Phone number Updated Successfully  ", 1);
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number  ", 1);
							}
							
						}
						else
						{
							cf.ShowToast("Please enter valid phone number  ", 1);
						}
					}
					catch(Exception e)
					{
						cf.ShowToast("Phone number not updated  ", 1);
						dialog1.setCancelable(true);
						dialog1.dismiss();
						System.out.println("Error in the query"+e.getMessage());
						cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
					}
				}
			});
			  dialog1.setCancelable(false);
			  dialog1.show();
			break;
		case R.id.c_edit_C_ph:
			  dialog1=showalert();
			   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
			    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
			   ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
			  txt_v.setText("Please Enter the Cell phone number ");
			  	C_phone=C_phone.replace("(", "");
				C_phone=C_phone.replace(")", "");
				C_phone=C_phone.replace("-", "");
			  ed_number.setText(C_phone);
			  bt_save.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try
					{
						if(!ed_number.getText().toString().trim().equals("") )
						{
							if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
							{
								
								cf.sh_db.execSQL("update "+cf.policyholder+" set SH_PH_CellPhone='"+cf.encode(cf.newphone)+"' where SH_PH_SRID='"+cf.selectedhomeid+"'");
								c_ph.setText(cf.newphone);
								cf.newphone=cf.newphone.replace("(", "");
								cf.newphone=cf.newphone.replace(")", "");
								cf.newphone=cf.newphone.replace("-", "");
								C_phone=cf.newphone;
								dialog1.setCancelable(true);
								dialog1.dismiss();
								cf.ShowToast("Phone number Updated Successfully  ", 1);
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number  ", 1);
							}
							
						}
						else
						{
							cf.ShowToast("Please enter valid phone number  ", 1);
						}
					}
					catch(Exception e)
					{
						cf.ShowToast("Phone number not updated  ", 1);
						dialog1.setCancelable(true);
						dialog1.dismiss();
						System.out.println("Error in the query"+e.getMessage());
						cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
					}
				}
			});
			  dialog1.setCancelable(false);
			  dialog1.show();
			break;
		case R.id.c_edit_A_C_ph:
			  dialog1=showalert();
			   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
			    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
			   ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
			  txt_v.setText("Please Enter the Agent office phone number ");
			  A_C_phone=A_C_phone.replace("(", "");
				A_C_phone=A_C_phone.replace(")", "");
				A_C_phone=A_C_phone.replace("-", "");
			  ed_number.setText(A_C_phone);
			  bt_save.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try
					{
						if(!ed_number.getText().toString().trim().equals("") )
						{
							if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
							{
								
								cf.sh_db.execSQL("update "+cf.Agent_tabble+" set SH_AI_AgentContactPhone='"+cf.newphone+"' where SH_AI_SRID ='"+cf.selectedhomeid+"'");
								a_c_ph.setText(cf.newphone);
								cf.newphone=cf.newphone.replace("(", "");
								cf.newphone=cf.newphone.replace(")", "");
								cf.newphone=cf.newphone.replace("-", "");
								A_C_phone=cf.newphone;
								dialog1.setCancelable(true);
								dialog1.dismiss();
								cf.ShowToast("Phone number Updated Successfully  ", 1);
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number  ", 1);
							}
							
						}
						else
						{
							cf.ShowToast("Please enter valid phone number  ", 1);
						}
					}
					catch(Exception e)
					{
						cf.ShowToast("Phone number not updated  ", 1);
						dialog1.setCancelable(true);
						dialog1.dismiss();
						System.out.println("Error in the query"+e.getMessage());
						cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
					}
				}
			});
			  dialog1.setCancelable(false);
			  dialog1.show();
			break;
		case R.id.c_edit_A_ph:
			  dialog1=showalert();
			   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
			    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
			   ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
			   	A_phone=A_phone.replace("(", "");
				A_phone=A_phone.replace(")", "");
				A_phone=A_phone.replace("-", "");
			   ed_number.setText(A_phone);
			  txt_v.setText("Please Enter the Agent contact phone number ");
			  bt_save.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try
					{
						if(!ed_number.getText().toString().trim().equals("") )
						{
							if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
							{
								
								cf.sh_db.execSQL("update "+cf.Agent_tabble+" set SH_AI_AgentOffPhone='"+cf.newphone+"' where SH_AI_SRID ='"+cf.selectedhomeid+"'");
								a_ph.setText(cf.newphone);
								cf.newphone=cf.newphone.replace("(", "");
								cf.newphone=cf.newphone.replace(")", "");
								cf.newphone=cf.newphone.replace("-", "");
								A_phone=cf.newphone;
								dialog1.setCancelable(true);
								dialog1.dismiss();
								cf.ShowToast("Phone number Updated Successfully  ", 1);
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number  ", 1);
							}
							
						}
						else
						{
							cf.ShowToast("Please enter valid phone number  ", 1);
						}
					}
					catch(Exception e)
					{
						cf.ShowToast("Phone number not updated  ", 1);
						dialog1.setCancelable(true);
						dialog1.dismiss();
						System.out.println("Error in the query"+e.getMessage());
						cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
					}
				}
			});
			  dialog1.setCancelable(false);
			  dialog1.show();
			break;

		}
	}

	private Dialog showalert() {
		// TODO Auto-generated method stub
		
		
		final Dialog dialog1 = new Dialog(Callattempt.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		LinearLayout maintable= (LinearLayout) dialog1.findViewById(R.id.maintable);
		LinearLayout currenttable= (LinearLayout) dialog1.findViewById(R.id.Edit_number);
		maintable.setVisibility(View.GONE);
		currenttable.setVisibility(View.VISIBLE);
		 Button bt_cancel=(Button) dialog1.findViewById(R.id.EN_cancel);
		  Button bt_close=(Button) dialog1.findViewById(R.id.EN_close);
		   bt_close.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
				cf.ShowToast("Phone number not updated", 1);
			}
		});
		  bt_cancel.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					cf.ShowToast("Phone number not updated", 1);
				}
			});  
		return dialog1;
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(getApplicationContext(), InspectorInfo.class);
			intimg.putExtra("homeid", cf.selectedhomeid);
			intimg.putExtra("InspectionType", cf.onlinspectionid);
			intimg.putExtra("status", cf.onlstatus);
			
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			pd.dismiss();
			if (k == 1) {
				View v = null;
				if (cnt != 0) {
					strcaldat = "";
					strcalday = "";
					strcaltime = "";
					strcalnum = "";
					strcalres = "";
					strcaltit = "";
					strcalcom = "";
					HorizontalScrollView.LayoutParams mParam = new HorizontalScrollView.LayoutParams(LayoutParams.FILL_PARENT,(int)((cnt*40)+20));
		     		slist.setLayoutParams(mParam);
					for (int i = 0; i < cnt; i++) {
						SoapObject obj = (SoapObject) res.getProperty(i);

						strcaldat += String.valueOf(obj
								.getProperty("CallAttemptDate")) + "~";
						strcalday += String.valueOf(obj
								.getProperty("CallAttemptDay")) + "~";
						strcaltime += String.valueOf(obj
								.getProperty("CallAttemptTime")) + "~";
						strcalnum += String.valueOf(obj
								.getProperty("CallAttemptNumberCalled")) + "~";
						strcalres += String.valueOf(obj
								.getProperty("CallAttemptResult")) + "~";
						strcaltit += String.valueOf(obj
								.getProperty("CallAttemptTitle")) + "~";
						strcalcom += String.valueOf(obj
								.getProperty("CallAttemptIntialsComment"))
								+ "~";
						if (strcalcom.contains(anytype)) {
							strcalcom = strcalcom.replace(anytype, "NIL");
						}
						if (strcalnum.contains(anytype)) {
							strcalnum = strcalnum.replace(anytype, "NIL");
						}
						
					}
					arrdat = strcaldat.split("~");
					arrday = strcalday.split("~");
					arrtime = strcaltime.split("~");
					arrnum = strcalnum.split("~");
					arrres = strcalres.split("~");
					arrcom = strcalcom.split("~");
					arrtit = strcaltit.split("~");
					txtnolist.setVisibility(v.GONE);
					slist.setVisibility(visibility);
					slist.setAdapter(new EfficientAdapter(getApplicationContext()));
				} else {
					txtnolist.setVisibility(visibility);
					slist.setVisibility(v.GONE);
					txtnolist.setText("No call attempt information.");
				}

			} else if (k == 2) {
				cf.ShowToast(
						"Internet connection is not available.",1);
			}

		}
	};
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			switch (resultCode) {
 			case 0:
 				break;
 			case -1:
 				try {

 					String[] projection = { MediaStore.Images.Media.DATA };
 					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
 							null, null, null);
 					int column_index_data = cursor
 							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
 					cursor.moveToFirst();
 					String capturedImageFilePath = cursor.getString(column_index_data);
 					cf.showselectedimage(capturedImageFilePath);
 				} catch (Exception e) {
 					
 				}
 				
 				break;

 		}

 	}


}

package idsoft.inspectiondepot.sinkholeinspection;

import idsoft.inspectiondepot.sinkholeinspection.Observation2.Obs_clicker;
import idsoft.inspectiondepot.sinkholeinspection.Observation4.SH_textwatcher;
import idsoft.inspectiondepot.sinkholeinspection.Observation4.Touch_Listener;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Observation3 extends Activity{
	CommonFunctions cf;
	LinearLayout SH_OBS14P,SH_OBS15P,SH_OBS16P,SH_OBS17P,SH_OBS18P,SH_OBS19P,SH_OBS20P,SH_OBS21P,SH_OBS22P,

	             SH_OBS14,SH_OBS15,SH_OBS16,SH_OBS17,SH_OBS18,SH_OBS19,SH_OBS20,SH_OBS21,SH_OBS22;
	TextView SH_OBS14T,SH_OBS15T,SH_OBS16T,SH_OBS17T,SH_OBS18T,SH_OBS19T,SH_OBS20T,SH_OBS21T,SH_OBS22T;
	String rd_InteriorWallCrackNoted="",rd_WallSlabCrackNoted="",rd_InteriorCeilingCrackNoted="",rd_WindowFrameOutofSquare="",rd_FloorSlopingNoted="",rd_CrackedGlazingNoted="",
	       rd_PreviousCorrectiveMeasureNoted="",rd_BindingDoorOpeningNoted="",rd_InteriorFloorCrackNoted="",
	       chk_Value14="",chk_Value15="",chk_Value16="",chk_Value17="",chk_Value18="",chk_Value19="",
	       chk_Value20="",chk_Value21="",chk_Value22="",strhomeid;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
	    	if (extras != null) {
	    		cf.selectedhomeid = strhomeid= extras.getString("homeid");
	    		cf.onlinspectionid = extras.getString("InspectionType");
	    	    cf.onlstatus = extras.getString("status");
	     	}
	        setContentView(R.layout.observation3);
	        cf.Create_Table(11);
	        cf.getDeviceDimensions();
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,cf));
			LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.observationsubmenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 43, 1,cf));
			
			/** DECLARATION AND CLCIK EVENT OF OBSERVATION14,15,16,17,18,19,20,21,22 ROW **/
			 cf.tbl_row_obs14 = (TableRow)findViewById(R.id.firstobsrow);
			 cf.tbl_row_obs14.setOnClickListener(new Obs_clicker());
			 
			 cf.tbl_row_obs15 = (TableRow)findViewById(R.id.secondobsrow);
			 cf.tbl_row_obs15.setOnClickListener(new Obs_clicker());
			 
			 cf.tbl_row_obs16 = (TableRow)findViewById(R.id.thirdobsrow);
			 cf.tbl_row_obs16.setOnClickListener(new Obs_clicker());
			 
			 cf.tbl_row_obs17= (TableRow)findViewById(R.id.fourthobsrow);
			 cf.tbl_row_obs17.setOnClickListener(new Obs_clicker());
			 
			 cf.tbl_row_obs18 = (TableRow)findViewById(R.id.fifthobsrow);
			 cf.tbl_row_obs18.setOnClickListener(new Obs_clicker());
			
			 cf.tbl_row_obs19 = (TableRow)findViewById(R.id.sixthobsrow);
			 cf.tbl_row_obs19.setOnClickListener(new Obs_clicker());
			 
			 cf.tbl_row_obs20 = (TableRow)findViewById(R.id.seventhobsrow);
			 cf.tbl_row_obs20.setOnClickListener(new Obs_clicker());
			 
			 cf.tbl_row_obs21 = (TableRow)findViewById(R.id.eigthobsrow);
			 cf.tbl_row_obs21.setOnClickListener(new Obs_clicker());
			 
			 cf.tbl_row_obs22 = (TableRow)findViewById(R.id.ninthobsrow);
			 cf.tbl_row_obs22.setOnClickListener(new Obs_clicker());
			 
			 /** DECKARATION OF TABLE HEADERS OF OBSERVATION14,15,16,17,18,19,20,21,22 **/
			 cf.obs14_tbl_row_txt = (TextView)findViewById(R.id.txtobservation14);
			 cf.obs15_tbl_row_txt = (TextView)findViewById(R.id.txtobservation15);
			 cf.obs16_tbl_row_txt = (TextView)findViewById(R.id.txtobservation16);
			 cf.obs17_tbl_row_txt = (TextView)findViewById(R.id.txtobservation17);
			 cf.obs18_tbl_row_txt = (TextView)findViewById(R.id.txtobservation18);
			 cf.obs19_tbl_row_txt = (TextView)findViewById(R.id.txtobservation19);
			 cf.obs20_tbl_row_txt = (TextView)findViewById(R.id.txtobservation20);
			 cf.obs21_tbl_row_txt = (TextView)findViewById(R.id.txtobservation21);
			 cf.obs22_tbl_row_txt = (TextView)findViewById(R.id.txtobservation22);
			 
			/** DECLARATION OF YES LAYOUT FOR OBSERVATION14,15,16,17,18,19,20,21,22 **/
			 cf.tbl_layout_obs14 = (TableLayout)findViewById(R.id.obs14_table);
			 cf.tbl_layout_obs15 = (TableLayout)findViewById(R.id.obs15_table);
			 cf.tbl_layout_obs16 = (TableLayout)findViewById(R.id.obs16_table);
			 cf.tbl_layout_obs17 = (TableLayout)findViewById(R.id.obs17_table);
			 cf.tbl_layout_obs18 = (TableLayout)findViewById(R.id.obs18_table);
			 cf.tbl_layout_obs19 = (TableLayout)findViewById(R.id.obs19_table);
			 cf.tbl_layout_obs20 = (TableLayout)findViewById(R.id.obs20_table);
			 cf.tbl_layout_obs21 = (TableLayout)findViewById(R.id.obs21_table);
			 cf.tbl_layout_obs22 = (TableLayout)findViewById(R.id.obs22_table);
			 
			 /** DECLARATION AND CLICK EVENT OF OBSERVATION14,15,16,17,18,19,20,21,22 - RADIOBUTTON YES **/
			 cf.rd_obs14_yes = (RadioButton)findViewById(R.id.obs14_rd_y);
			 cf.rd_obs14_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs15_yes = (RadioButton)findViewById(R.id.obs15_rd_y);
			 cf.rd_obs15_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs16_yes = (RadioButton)findViewById(R.id.obs16_rd_y);
			 cf.rd_obs16_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs17_yes = (RadioButton)findViewById(R.id.obs17_rd_y);
			 cf.rd_obs17_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs18_yes = (RadioButton)findViewById(R.id.obs18_rd_y);
			 cf.rd_obs18_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs19_yes = (RadioButton)findViewById(R.id.obs19_rd_y);
			 cf.rd_obs19_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs20_yes = (RadioButton)findViewById(R.id.obs20_rd_y);
			 cf.rd_obs20_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs21_yes = (RadioButton)findViewById(R.id.obs21_rd_y);
			 cf.rd_obs21_yes.setOnClickListener(new Obs_clicker());
			 cf.rd_obs22_yes = (RadioButton)findViewById(R.id.obs22_rd_y);
			 cf.rd_obs22_yes.setOnClickListener(new Obs_clicker());
			 
			 /** DECLARATION AND CLICK EVENT OF OBSERVATION14,15,16,17,18,19,20,21,22 - RADIOBUTTON NO **/
			 cf.rd_obs14_no = (RadioButton)findViewById(R.id.obs14_rd_n);
			 cf.rd_obs14_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs15_no = (RadioButton)findViewById(R.id.obs15_rd_n);
			 cf.rd_obs15_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs16_no = (RadioButton)findViewById(R.id.obs16_rd_n);
			 cf.rd_obs16_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs17_no = (RadioButton)findViewById(R.id.obs17_rd_n);
			 cf.rd_obs17_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs18_no = (RadioButton)findViewById(R.id.obs18_rd_n);
			 cf.rd_obs18_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs19_no = (RadioButton)findViewById(R.id.obs19_rd_n);
			 cf.rd_obs19_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs20_no = (RadioButton)findViewById(R.id.obs20_rd_n);
			 cf.rd_obs20_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs21_no = (RadioButton)findViewById(R.id.obs21_rd_n);
			 cf.rd_obs21_no.setOnClickListener(new Obs_clicker());
			 cf.rd_obs22_no = (RadioButton)findViewById(R.id.obs22_rd_n);
			 cf.rd_obs22_no.setOnClickListener(new Obs_clicker());
			 			 
			 /** DECLARATION AND CLICK EVENT OF OBSERVATION14,15,16,17,18,19,20,21,22 - RADIOBUTTON NOTDETERMINED **/
			 cf.rd_obs14_nd = (RadioButton)findViewById(R.id.obs14_rd_nd);
			 cf.rd_obs14_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs15_nd = (RadioButton)findViewById(R.id.obs15_rd_nd);
			 cf.rd_obs15_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs16_nd = (RadioButton)findViewById(R.id.obs16_rd_nd);
			 cf.rd_obs16_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs17_nd = (RadioButton)findViewById(R.id.obs17_rd_nd);
			 cf.rd_obs17_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs18_nd = (RadioButton)findViewById(R.id.obs18_rd_nd);
			 cf.rd_obs18_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs19_nd = (RadioButton)findViewById(R.id.obs19_rd_nd);
			 cf.rd_obs19_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs20_nd = (RadioButton)findViewById(R.id.obs20_rd_nd);
			 cf.rd_obs20_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs21_nd = (RadioButton)findViewById(R.id.obs21_rd_nd);
			 cf.rd_obs21_nd.setOnClickListener(new Obs_clicker());
			 cf.rd_obs22_nd = (RadioButton)findViewById(R.id.obs22_rd_nd);
			 cf.rd_obs22_nd.setOnClickListener(new Obs_clicker());
			 
			 /** DECLARATION FOR COMMENTS OBSERVATION14,15,16,17,18,19,20,21,22 **/
		      cf.etcomments_obs14=(EditText)findViewById(R.id.obs14_etcomments);
		      cf.etcomments_obs15=(EditText)findViewById(R.id.obs15_etcomments);
		      cf.etcomments_obs16=(EditText)findViewById(R.id.obs16_etcomments);
		      cf.etcomments_obs17=(EditText)findViewById(R.id.obs17_etcomments);
		      cf.etcomments_obs18=(EditText)findViewById(R.id.obs18_etcomments);
		      cf.etcomments_obs19=(EditText)findViewById(R.id.obs19_etcomments);
		      cf.etcomments_obs20=(EditText)findViewById(R.id.obs20_etcomments);
		      cf.etcomments_obs21=(EditText)findViewById(R.id.obs21_etcomments);
		      cf.etcomments_obs22=(EditText)findViewById(R.id.obs22_etcomments);
		      
		      /** DECLARATION OF YES LAYOUT FOR OBSERVATION8,9,10,11,12,13 **/
			  cf.show_tbl_obs14 = (TableLayout)findViewById(R.id.obs14_tbl_yes);
			  cf.show_tbl_obs15 = (TableLayout)findViewById(R.id.obs15_tbl_yes);
			  cf.show_tbl_obs16 = (TableLayout)findViewById(R.id.obs16_tbl_yes);
			  cf.show_tbl_obs17 = (TableLayout)findViewById(R.id.obs17_tbl_yes);
			  cf.show_tbl_obs18 = (TableLayout)findViewById(R.id.obs18_tbl_yes);
			  cf.show_tbl_obs19 = (TableLayout)findViewById(R.id.obs19_tbl_yes);
			  cf.show_tbl_obs20 = (TableLayout)findViewById(R.id.obs20_tbl_yes);
			  cf.show_tbl_obs21 = (TableLayout)findViewById(R.id.obs21_tbl_yes);
			  cf.show_tbl_obs22 = (TableLayout)findViewById(R.id.obs22_tbl_yes);
			 		      
		      /** DECLARATION OF OBSERVATION14 - CHECKBOXES **/
			   cf.cb_obs14[0]=(CheckBox)findViewById(R.id.obs14_allvisible);
			   cf.cb_obs14[1] = (CheckBox)findViewById(R.id.obs14_entrance);
			   cf.cb_obs14[2] = (CheckBox)findViewById(R.id.obs14_foyer);
			   cf.cb_obs14[3] = (CheckBox)findViewById(R.id.obs14_living);
			   cf.cb_obs14[4] = (CheckBox)findViewById(R.id.obs14_den);
			   cf.cb_obs14[5] = (CheckBox)findViewById(R.id.obs14_diningroom);
			   cf.cb_obs14[6] = (CheckBox)findViewById(R.id.obs14_kitchen);
			   cf.cb_obs14[7] = (CheckBox)findViewById(R.id.obs14_breakfast);
			   cf.cb_obs14[8] = (CheckBox)findViewById(R.id.obs14_masterbedroom);
			   cf.cb_obs14[9] = (CheckBox)findViewById(R.id.obs14_bedroom);
			   cf.cb_obs14[10] = (CheckBox)findViewById(R.id.obs14_playroom);
			   cf.cb_obs14[11] = (CheckBox)findViewById(R.id.obs14_bathroom);
			   cf.cb_obs14[12] = (CheckBox)findViewById(R.id.obs14_masterbathroom);
			   cf.cb_obs14[13] = (CheckBox)findViewById(R.id.obs14_halfbathroom);
			   cf.cb_obs14[14] = (CheckBox)findViewById(R.id.obs14_study);
			   cf.cb_obs14[15] = (CheckBox)findViewById(R.id.obs14_closest);
			   cf.cb_obs14[16] = (CheckBox)findViewById(R.id.obs14_utility);
			   cf.cb_obs14[17] = (CheckBox)findViewById(R.id.obs14_storage);
			   cf.cb_obs14[18] = (CheckBox)findViewById(R.id.obs14_garage);
			   cf.cb_obs14[19] = (CheckBox)findViewById(R.id.obs14_stairwall);
			   cf.cb_obs14[20] = (CheckBox)findViewById(R.id.obs14_pantry);
			   cf.cb_obs14[21] = (CheckBox)findViewById(R.id.obs14_laundry);
			   cf.cb_obs14[22] = (CheckBox)findViewById(R.id.obs14_screenenclosure);
			   cf.cb_obs14[23] = (CheckBox)findViewById(R.id.obs14_floridaroom);
			   cf.cb_obs14[24] = (CheckBox)findViewById(R.id.obs14_other);
			   
			   /** DECLARATION OF OBSERVATION15- CHECKBOXES **/
			   cf.cb_obs15[0]=(CheckBox)findViewById(R.id.obs15_allvisible);
			   cf.cb_obs15[1] = (CheckBox)findViewById(R.id.obs15_entrance);
			   cf.cb_obs15[2] = (CheckBox)findViewById(R.id.obs15_foyer);
			   cf.cb_obs15[3] = (CheckBox)findViewById(R.id.obs15_living);
			   cf.cb_obs15[4] = (CheckBox)findViewById(R.id.obs15_den);
			   cf.cb_obs15[5] = (CheckBox)findViewById(R.id.obs15_diningroom);
			   cf.cb_obs15[6] = (CheckBox)findViewById(R.id.obs15_kitchen);
			   cf.cb_obs15[7] = (CheckBox)findViewById(R.id.obs15_breakfast);
			   cf.cb_obs15[8] = (CheckBox)findViewById(R.id.obs15_masterbedroom);
			   cf.cb_obs15[9] = (CheckBox)findViewById(R.id.obs15_bedroom);
			   cf.cb_obs15[10] = (CheckBox)findViewById(R.id.obs15_playroom);
			   cf.cb_obs15[11] = (CheckBox)findViewById(R.id.obs15_bathroom);
			   cf.cb_obs15[12] = (CheckBox)findViewById(R.id.obs15_masterbathroom);
			   cf.cb_obs15[13] = (CheckBox)findViewById(R.id.obs15_halfbathroom);
			   cf.cb_obs15[14] = (CheckBox)findViewById(R.id.obs15_study);
			   cf.cb_obs15[15] = (CheckBox)findViewById(R.id.obs15_closest);
			   cf.cb_obs15[16] = (CheckBox)findViewById(R.id.obs15_utility);
			   cf.cb_obs15[17] = (CheckBox)findViewById(R.id.obs15_storage);
			   cf.cb_obs15[18] = (CheckBox)findViewById(R.id.obs15_garage);
			   cf.cb_obs15[19] = (CheckBox)findViewById(R.id.obs15_stairwall);
			   cf.cb_obs15[20] = (CheckBox)findViewById(R.id.obs15_pantry);
			   cf.cb_obs15[21] = (CheckBox)findViewById(R.id.obs15_laundry);
			   cf.cb_obs15[22] = (CheckBox)findViewById(R.id.obs15_screenenclosure);
			   cf.cb_obs15[23] = (CheckBox)findViewById(R.id.obs15_floridaroom);
			   cf.cb_obs15[24] = (CheckBox)findViewById(R.id.obs15_other);
			   
			   /** DECLARATION OF OBSERVATION16- CHECKBOXES **/
			   cf.cb_obs16[0]=(CheckBox)findViewById(R.id.obs16_allvisible);
			   cf.cb_obs16[1] = (CheckBox)findViewById(R.id.obs16_entrance);
			   cf.cb_obs16[2] = (CheckBox)findViewById(R.id.obs16_foyer);
			   cf.cb_obs16[3] = (CheckBox)findViewById(R.id.obs16_living);
			   cf.cb_obs16[4] = (CheckBox)findViewById(R.id.obs16_den);
			   cf.cb_obs16[5] = (CheckBox)findViewById(R.id.obs16_diningroom);
			   cf.cb_obs16[6] = (CheckBox)findViewById(R.id.obs16_kitchen);
			   cf.cb_obs16[7] = (CheckBox)findViewById(R.id.obs16_breakfast);
			   cf.cb_obs16[8] = (CheckBox)findViewById(R.id.obs16_masterbedroom);
			   cf.cb_obs16[9] = (CheckBox)findViewById(R.id.obs16_bedroom);
			   cf.cb_obs16[10] = (CheckBox)findViewById(R.id.obs16_playroom);
			   cf.cb_obs16[11] = (CheckBox)findViewById(R.id.obs16_bathroom);
			   cf.cb_obs16[12] = (CheckBox)findViewById(R.id.obs16_masterbathroom);
			   cf.cb_obs16[13] = (CheckBox)findViewById(R.id.obs16_halfbathroom);
			   cf.cb_obs16[14] = (CheckBox)findViewById(R.id.obs16_study);
			   cf.cb_obs16[15] = (CheckBox)findViewById(R.id.obs16_closest);
			   cf.cb_obs16[16] = (CheckBox)findViewById(R.id.obs16_utility);
			   cf.cb_obs16[17] = (CheckBox)findViewById(R.id.obs16_storage);
			   cf.cb_obs16[18] = (CheckBox)findViewById(R.id.obs16_garage);
			   cf.cb_obs16[19] = (CheckBox)findViewById(R.id.obs16_stairwall);
			   cf.cb_obs16[20] = (CheckBox)findViewById(R.id.obs16_pantry);
			   cf.cb_obs16[21] = (CheckBox)findViewById(R.id.obs16_laundry);
			   cf.cb_obs16[22] = (CheckBox)findViewById(R.id.obs16_screenenclosure);
			   cf.cb_obs16[23] = (CheckBox)findViewById(R.id.obs16_floridaroom);
			   cf.cb_obs16[24] = (CheckBox)findViewById(R.id.obs16_other);
			   
			   /** DECLARATION OF OBSERVATION17- CHECKBOXES **/
			   cf.cb_obs17[0]=(CheckBox)findViewById(R.id.obs17_allvisible);
			   cf.cb_obs17[1] = (CheckBox)findViewById(R.id.obs17_entrance);
			   cf.cb_obs17[2] = (CheckBox)findViewById(R.id.obs17_foyer);
			   cf.cb_obs17[3] = (CheckBox)findViewById(R.id.obs17_living);
			   cf.cb_obs17[4] = (CheckBox)findViewById(R.id.obs17_den);
			   cf.cb_obs17[5] = (CheckBox)findViewById(R.id.obs17_diningroom);
			   cf.cb_obs17[6] = (CheckBox)findViewById(R.id.obs17_kitchen);
			   cf.cb_obs17[7] = (CheckBox)findViewById(R.id.obs17_breakfast);
			   cf.cb_obs17[8] = (CheckBox)findViewById(R.id.obs17_masterbedroom);
			   cf.cb_obs17[9] = (CheckBox)findViewById(R.id.obs17_bedroom);
			   cf.cb_obs17[10] = (CheckBox)findViewById(R.id.obs17_playroom);
			   cf.cb_obs17[11] = (CheckBox)findViewById(R.id.obs17_bathroom);
			   cf.cb_obs17[12] = (CheckBox)findViewById(R.id.obs17_masterbathroom);
			   cf.cb_obs17[13] = (CheckBox)findViewById(R.id.obs17_halfbathroom);
			   cf.cb_obs17[14] = (CheckBox)findViewById(R.id.obs17_study);
			   cf.cb_obs17[15] = (CheckBox)findViewById(R.id.obs17_closest);
			   cf.cb_obs17[16] = (CheckBox)findViewById(R.id.obs17_utility);
			   cf.cb_obs17[17] = (CheckBox)findViewById(R.id.obs17_storage);
			   cf.cb_obs17[18] = (CheckBox)findViewById(R.id.obs17_garage);
			   cf.cb_obs17[19] = (CheckBox)findViewById(R.id.obs17_stairwall);
			   cf.cb_obs17[20] = (CheckBox)findViewById(R.id.obs17_pantry);
			   cf.cb_obs17[21] = (CheckBox)findViewById(R.id.obs17_laundry);
			   cf.cb_obs17[22] = (CheckBox)findViewById(R.id.obs17_screenenclosure);
			   cf.cb_obs17[23] = (CheckBox)findViewById(R.id.obs17_floridaroom);
			   cf.cb_obs17[24] = (CheckBox)findViewById(R.id.obs17_other);
			   
			   /** DECLARATION OF OBSERVATION18- CHECKBOXES **/
			   cf.cb_obs18[0]=(CheckBox)findViewById(R.id.obs18_allvisible);
			   cf.cb_obs18[1] = (CheckBox)findViewById(R.id.obs18_entrance);
			   cf.cb_obs18[2] = (CheckBox)findViewById(R.id.obs18_foyer);
			   cf.cb_obs18[3] = (CheckBox)findViewById(R.id.obs18_living);
			   cf.cb_obs18[4] = (CheckBox)findViewById(R.id.obs18_den);
			   cf.cb_obs18[5] = (CheckBox)findViewById(R.id.obs18_diningroom);
			   cf.cb_obs18[6] = (CheckBox)findViewById(R.id.obs18_kitchen);
			   cf.cb_obs18[7] = (CheckBox)findViewById(R.id.obs18_breakfast);
			   cf.cb_obs18[8] = (CheckBox)findViewById(R.id.obs18_masterbedroom);
			   cf.cb_obs18[9] = (CheckBox)findViewById(R.id.obs18_bedroom);
			   cf.cb_obs18[10] = (CheckBox)findViewById(R.id.obs18_playroom);
			   cf.cb_obs18[11] = (CheckBox)findViewById(R.id.obs18_bathroom);
			   cf.cb_obs18[12] = (CheckBox)findViewById(R.id.obs18_masterbathroom);
			   cf.cb_obs18[13] = (CheckBox)findViewById(R.id.obs18_halfbathroom);
			   cf.cb_obs18[14] = (CheckBox)findViewById(R.id.obs18_study);
			   cf.cb_obs18[15] = (CheckBox)findViewById(R.id.obs18_closest);
			   cf.cb_obs18[16] = (CheckBox)findViewById(R.id.obs18_utility);
			   cf.cb_obs18[17] = (CheckBox)findViewById(R.id.obs18_storage);
			   cf.cb_obs18[18] = (CheckBox)findViewById(R.id.obs18_garage);
			   cf.cb_obs18[19] = (CheckBox)findViewById(R.id.obs18_stairwall);
			   cf.cb_obs18[20] = (CheckBox)findViewById(R.id.obs18_pantry);
			   cf.cb_obs18[21] = (CheckBox)findViewById(R.id.obs18_laundry);
			   cf.cb_obs18[22] = (CheckBox)findViewById(R.id.obs18_screenenclosure);
			   cf.cb_obs18[23] = (CheckBox)findViewById(R.id.obs18_floridaroom);
			   cf.cb_obs18[24] = (CheckBox)findViewById(R.id.obs18_other);
			   
			   /** DECLARATION OF OBSERVATION19- CHECKBOXES **/
			   cf.cb_obs19[0]=(CheckBox)findViewById(R.id.obs19_allvisible);
			   cf.cb_obs19[1] = (CheckBox)findViewById(R.id.obs19_entrance);
			   cf.cb_obs19[2] = (CheckBox)findViewById(R.id.obs19_foyer);
			   cf.cb_obs19[3] = (CheckBox)findViewById(R.id.obs19_living);
			   cf.cb_obs19[4] = (CheckBox)findViewById(R.id.obs19_den);
			   cf.cb_obs19[5] = (CheckBox)findViewById(R.id.obs19_diningroom);
			   cf.cb_obs19[6] = (CheckBox)findViewById(R.id.obs19_kitchen);
			   cf.cb_obs19[7] = (CheckBox)findViewById(R.id.obs19_breakfast);
			   cf.cb_obs19[8] = (CheckBox)findViewById(R.id.obs19_masterbedroom);
			   cf.cb_obs19[9] = (CheckBox)findViewById(R.id.obs19_bedroom);
			   cf.cb_obs19[10] = (CheckBox)findViewById(R.id.obs19_playroom);
			   cf.cb_obs19[11] = (CheckBox)findViewById(R.id.obs19_bathroom);
			   cf.cb_obs19[12] = (CheckBox)findViewById(R.id.obs19_masterbathroom);
			   cf.cb_obs19[13] = (CheckBox)findViewById(R.id.obs19_halfbathroom);
			   cf.cb_obs19[14] = (CheckBox)findViewById(R.id.obs19_study);
			   cf.cb_obs19[15] = (CheckBox)findViewById(R.id.obs19_closest);
			   cf.cb_obs19[16] = (CheckBox)findViewById(R.id.obs19_utility);
			   cf.cb_obs19[17] = (CheckBox)findViewById(R.id.obs19_storage);
			   cf.cb_obs19[18] = (CheckBox)findViewById(R.id.obs19_garage);
			   cf.cb_obs19[19] = (CheckBox)findViewById(R.id.obs19_stairwall);
			   cf.cb_obs19[20] = (CheckBox)findViewById(R.id.obs19_pantry);
			   cf.cb_obs19[21] = (CheckBox)findViewById(R.id.obs19_laundry);
			   cf.cb_obs19[22] = (CheckBox)findViewById(R.id.obs19_screenenclosure);
			   cf.cb_obs19[23] = (CheckBox)findViewById(R.id.obs19_floridaroom);
			   cf.cb_obs19[24] = (CheckBox)findViewById(R.id.obs19_other);
			   
			   /** DECLARATION OF OBSERVATION20- CHECKBOXES **/
			   cf.cb_obs20[0]=(CheckBox)findViewById(R.id.obs20_allvisible);
			   cf.cb_obs20[1] = (CheckBox)findViewById(R.id.obs20_entrance);
			   cf.cb_obs20[2] = (CheckBox)findViewById(R.id.obs20_foyer);
			   cf.cb_obs20[3] = (CheckBox)findViewById(R.id.obs20_living);
			   cf.cb_obs20[4] = (CheckBox)findViewById(R.id.obs20_den);
			   cf.cb_obs20[5] = (CheckBox)findViewById(R.id.obs20_diningroom);
			   cf.cb_obs20[6] = (CheckBox)findViewById(R.id.obs20_kitchen);
			   cf.cb_obs20[7] = (CheckBox)findViewById(R.id.obs20_breakfast);
			   cf.cb_obs20[8] = (CheckBox)findViewById(R.id.obs20_masterbedroom);
			   cf.cb_obs20[9] = (CheckBox)findViewById(R.id.obs20_bedroom);
			   cf.cb_obs20[10] = (CheckBox)findViewById(R.id.obs20_playroom);
			   cf.cb_obs20[11] = (CheckBox)findViewById(R.id.obs20_bathroom);
			   cf.cb_obs20[12] = (CheckBox)findViewById(R.id.obs20_masterbathroom);
			   cf.cb_obs20[13] = (CheckBox)findViewById(R.id.obs20_halfbathroom);
			   cf.cb_obs20[14] = (CheckBox)findViewById(R.id.obs20_study);
			   cf.cb_obs20[15] = (CheckBox)findViewById(R.id.obs20_closest);
			   cf.cb_obs20[16] = (CheckBox)findViewById(R.id.obs20_utility);
			   cf.cb_obs20[17] = (CheckBox)findViewById(R.id.obs20_storage);
			   cf.cb_obs20[18] = (CheckBox)findViewById(R.id.obs20_garage);
			   cf.cb_obs20[19] = (CheckBox)findViewById(R.id.obs20_stairwall);
			   cf.cb_obs20[20] = (CheckBox)findViewById(R.id.obs20_pantry);
			   cf.cb_obs20[21] = (CheckBox)findViewById(R.id.obs20_laundry);
			   cf.cb_obs20[22] = (CheckBox)findViewById(R.id.obs20_screenenclosure);
			   cf.cb_obs20[23] = (CheckBox)findViewById(R.id.obs20_floridaroom);
			   cf.cb_obs20[24] = (CheckBox)findViewById(R.id.obs20_other);
			   
			   /** DECLARATION OF OBSERVATION21- CHECKBOXES **/
			   cf.cb_obs21[0]=(CheckBox)findViewById(R.id.obs21_allvisible);
			   cf.cb_obs21[1] = (CheckBox)findViewById(R.id.obs21_entrance);
			   cf.cb_obs21[2] = (CheckBox)findViewById(R.id.obs21_foyer);
			   cf.cb_obs21[3] = (CheckBox)findViewById(R.id.obs21_living);
			   cf.cb_obs21[4] = (CheckBox)findViewById(R.id.obs21_den);
			   cf.cb_obs21[5] = (CheckBox)findViewById(R.id.obs21_diningroom);
			   cf.cb_obs21[6] = (CheckBox)findViewById(R.id.obs21_kitchen);
			   cf.cb_obs21[7] = (CheckBox)findViewById(R.id.obs21_breakfast);
			   cf.cb_obs21[8] = (CheckBox)findViewById(R.id.obs21_masterbedroom);
			   cf.cb_obs21[9] = (CheckBox)findViewById(R.id.obs21_bedroom);
			   cf.cb_obs21[10] = (CheckBox)findViewById(R.id.obs21_playroom);
			   cf.cb_obs21[11] = (CheckBox)findViewById(R.id.obs21_bathroom);
			   cf.cb_obs21[12] = (CheckBox)findViewById(R.id.obs21_masterbathroom);
			   cf.cb_obs21[13] = (CheckBox)findViewById(R.id.obs21_halfbathroom);
			   cf.cb_obs21[14] = (CheckBox)findViewById(R.id.obs21_study);
			   cf.cb_obs21[15] = (CheckBox)findViewById(R.id.obs21_closest);
			   cf.cb_obs21[16] = (CheckBox)findViewById(R.id.obs21_utility);
			   cf.cb_obs21[17] = (CheckBox)findViewById(R.id.obs21_storage);
			   cf.cb_obs21[18] = (CheckBox)findViewById(R.id.obs21_garage);
			   cf.cb_obs21[19] = (CheckBox)findViewById(R.id.obs21_stairwall);
			   cf.cb_obs21[20] = (CheckBox)findViewById(R.id.obs21_pantry);
			   cf.cb_obs21[21] = (CheckBox)findViewById(R.id.obs21_laundry);
			   cf.cb_obs21[22] = (CheckBox)findViewById(R.id.obs21_screenenclosure);
			   cf.cb_obs21[23] = (CheckBox)findViewById(R.id.obs21_floridaroom);
			   cf.cb_obs21[24] = (CheckBox)findViewById(R.id.obs21_other);
			   
			   /** DECLARATION OF OBSERVATION22- CHECKBOXES **/
			   cf.cb_obs22[0]=(CheckBox)findViewById(R.id.obs22_allvisible);
			   cf.cb_obs22[1] = (CheckBox)findViewById(R.id.obs22_entrance);
			   cf.cb_obs22[2] = (CheckBox)findViewById(R.id.obs22_foyer);
			   cf.cb_obs22[3] = (CheckBox)findViewById(R.id.obs22_living);
			   cf.cb_obs22[4] = (CheckBox)findViewById(R.id.obs22_den);
			   cf.cb_obs22[5] = (CheckBox)findViewById(R.id.obs22_diningroom);
			   cf.cb_obs22[6] = (CheckBox)findViewById(R.id.obs22_kitchen);
			   cf.cb_obs22[7] = (CheckBox)findViewById(R.id.obs22_breakfast);
			   cf.cb_obs22[8] = (CheckBox)findViewById(R.id.obs22_masterbedroom);
			   cf.cb_obs22[9] = (CheckBox)findViewById(R.id.obs22_bedroom);
			   cf.cb_obs22[10] = (CheckBox)findViewById(R.id.obs22_playroom);
			   cf.cb_obs22[11] = (CheckBox)findViewById(R.id.obs22_bathroom);
			   cf.cb_obs22[12] = (CheckBox)findViewById(R.id.obs22_masterbathroom);
			   cf.cb_obs22[13] = (CheckBox)findViewById(R.id.obs22_halfbathroom);
			   cf.cb_obs22[14] = (CheckBox)findViewById(R.id.obs22_study);
			   cf.cb_obs22[15] = (CheckBox)findViewById(R.id.obs22_closest);
			   cf.cb_obs22[16] = (CheckBox)findViewById(R.id.obs22_utility);
			   cf.cb_obs22[17] = (CheckBox)findViewById(R.id.obs22_storage);
			   cf.cb_obs22[18] = (CheckBox)findViewById(R.id.obs22_garage);
			   cf.cb_obs22[19] = (CheckBox)findViewById(R.id.obs22_stairwall);
			   cf.cb_obs22[20] = (CheckBox)findViewById(R.id.obs22_pantry);
			   cf.cb_obs22[21] = (CheckBox)findViewById(R.id.obs22_laundry);
			   cf.cb_obs22[22] = (CheckBox)findViewById(R.id.obs22_screenenclosure);
			   cf.cb_obs22[23] = (CheckBox)findViewById(R.id.obs22_floridaroom);
			   cf.cb_obs22[24] = (CheckBox)findViewById(R.id.obs22_other);
			   
			   /** CLICK EVENT OF OTHER CHECKBOX - OBSERVATION14,15,16,17,18,19,20,21,22 **/
			   cf.cb_obs14[24].setOnClickListener(new Obs_clicker());
			   cf.cb_obs15[24].setOnClickListener(new Obs_clicker());
			   cf.cb_obs16[24].setOnClickListener(new Obs_clicker());
			   cf.cb_obs17[24].setOnClickListener(new Obs_clicker());
			   cf.cb_obs18[24].setOnClickListener(new Obs_clicker());
			   cf.cb_obs19[24].setOnClickListener(new Obs_clicker());
			   cf.cb_obs20[24].setOnClickListener(new Obs_clicker());
			   cf.cb_obs21[24].setOnClickListener(new Obs_clicker());
			   cf.cb_obs22[24].setOnClickListener(new Obs_clicker());
			   
			   /** DECLARATION OF EDITTEXT BOX OF OTHER - OBSERVATION14,15,16,17,18,19,20,21,22 **/
			   cf.etother_obs14 = (EditText)findViewById(R.id.obs14_etother);
			   cf.etother_obs15 = (EditText)findViewById(R.id.obs15_etother);
			   cf.etother_obs16 = (EditText)findViewById(R.id.obs16_etother);
			   cf.etother_obs17 = (EditText)findViewById(R.id.obs17_etother);
			   cf.etother_obs18 = (EditText)findViewById(R.id.obs18_etother);
			   cf.etother_obs19 = (EditText)findViewById(R.id.obs19_etother);
			   cf.etother_obs20 = (EditText)findViewById(R.id.obs20_etother);
			   cf.etother_obs21 = (EditText)findViewById(R.id.obs21_etother);
			   cf.etother_obs22 = (EditText)findViewById(R.id.obs22_etother);
			   
			   /*DECLARATION OF EDITTEXT LIMIT EXCEEDS */
				  SH_OBS14P = (LinearLayout) findViewById(R.id.SH_OBS14_ED_parrent);
				  SH_OBS14 = (LinearLayout) findViewById(R.id.SH_OB14);
				  SH_OBS14T = (TextView) findViewById(R.id.SH_OB14_TV);
				  
				  SH_OBS15P = (LinearLayout) findViewById(R.id.SH_OBS15_ED_parrent);
				  SH_OBS15 = (LinearLayout) findViewById(R.id.SH_OB15);
				  SH_OBS15T = (TextView) findViewById(R.id.SH_OB15_TV);
				  
				  SH_OBS16P = (LinearLayout) findViewById(R.id.SH_OBS16_ED_parrent);
				  SH_OBS16 = (LinearLayout) findViewById(R.id.SH_OB16);
				  SH_OBS16T = (TextView) findViewById(R.id.SH_OB16_TV);
				  
				  SH_OBS17P = (LinearLayout) findViewById(R.id.SH_OBS17_ED_parrent);
				  SH_OBS17 = (LinearLayout) findViewById(R.id.SH_OB17);
				  SH_OBS17T = (TextView) findViewById(R.id.SH_OB17_TV);
				  
				  SH_OBS18P = (LinearLayout) findViewById(R.id.SH_OBS18_ED_parrent);
				  SH_OBS18 = (LinearLayout) findViewById(R.id.SH_OB18);
				  SH_OBS18T = (TextView) findViewById(R.id.SH_OB18_TV);
				  
				  SH_OBS19P = (LinearLayout) findViewById(R.id.SH_OBS19_ED_parrent);
				  SH_OBS19 = (LinearLayout) findViewById(R.id.SH_OB19);
				  SH_OBS19T = (TextView) findViewById(R.id.SH_OB19_TV);
				  
				  SH_OBS20P = (LinearLayout) findViewById(R.id.SH_OBS20_ED_parrent);
				  SH_OBS20 = (LinearLayout) findViewById(R.id.SH_OB20);
				  SH_OBS20T = (TextView) findViewById(R.id.SH_OB20_TV);
				  
				  SH_OBS21P = (LinearLayout) findViewById(R.id.SH_OBS21_ED_parrent);
				  SH_OBS21 = (LinearLayout) findViewById(R.id.SH_OB21);
				  SH_OBS21T = (TextView) findViewById(R.id.SH_OB21_TV);
				  
				  SH_OBS22P = (LinearLayout) findViewById(R.id.SH_OBS22_ED_parrent);
				  SH_OBS22 = (LinearLayout) findViewById(R.id.SH_OB22);
				  SH_OBS22T = (TextView) findViewById(R.id.SH_OB22_TV);
				  
				  cf.etcomments_obs14.setOnTouchListener(new Touch_Listener(1));
				  cf.etcomments_obs15.setOnTouchListener(new Touch_Listener(2));
				  cf.etcomments_obs16.setOnTouchListener(new Touch_Listener(3));
				  cf.etcomments_obs17.setOnTouchListener(new Touch_Listener(4));
				  cf.etcomments_obs18.setOnTouchListener(new Touch_Listener(5));
				  cf.etcomments_obs19.setOnTouchListener(new Touch_Listener(6));
				  cf.etcomments_obs20.setOnTouchListener(new Touch_Listener(7));
				  cf.etcomments_obs21.setOnTouchListener(new Touch_Listener(8));
				  cf.etcomments_obs22.setOnTouchListener(new Touch_Listener(9));
				  
				  
				  cf.etcomments_obs14.addTextChangedListener(new SH_textwatcher(1));
				  cf.etcomments_obs15.addTextChangedListener(new SH_textwatcher(2));
				  cf.etcomments_obs16.addTextChangedListener(new SH_textwatcher(3));
				  cf.etcomments_obs17.addTextChangedListener(new SH_textwatcher(4));
				  cf.etcomments_obs18.addTextChangedListener(new SH_textwatcher(5));
				  cf.etcomments_obs19.addTextChangedListener(new SH_textwatcher(6));
				  cf.etcomments_obs20.addTextChangedListener(new SH_textwatcher(7));
				  cf.etcomments_obs21.addTextChangedListener(new SH_textwatcher(8));
				  cf.etcomments_obs22.addTextChangedListener(new SH_textwatcher(9));
				  
				  /* DISPLAY THE VALUES FROM DATABASE*/
				  OBS3_SetValues();
			
	 }
	 class Touch_Listener implements OnTouchListener
		{
			   public int type;
			   Touch_Listener(int type)
				{
					this.type=type;
					
				}
			    @Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
			    	if(this.type==1)
					{
			    		cf.setFocus(cf.etcomments_obs14);
					}
			    	else if(this.type==2)
					{
				    		cf.setFocus(cf.etcomments_obs15);
					}
			    	else if(this.type==3)
					{
				    		cf.setFocus(cf.etcomments_obs16);
					}
			    	else if(this.type==4)
					{
				    		cf.setFocus(cf.etcomments_obs17);
					}
			    	else if(this.type==5)
					{
				    		cf.setFocus(cf.etcomments_obs18);
					}
			    	else if(this.type==6)
					{
				    		cf.setFocus(cf.etcomments_obs19);
					}
			    	else if(this.type==7)
					{
				    		cf.setFocus(cf.etcomments_obs20);
					}
			    	else if(this.type==8)
					{
				    		cf.setFocus(cf.etcomments_obs21);
					}
			    	else if(this.type==9)
					{
				    		cf.setFocus(cf.etcomments_obs22);
					}
			    	
					return false;
				}
			 
		}
	 private void OBS3_SetValues() {
		// TODO Auto-generated method stub
		 try
			{
				Cursor OBS3_Retrive=cf.SelectTablefunction(cf.Observation3, " where SH_OBS3_SRID ='"+cf.encode(cf.selectedhomeid)+"'");
				if(OBS3_Retrive.getCount()>0)
				{
					OBS3_Retrive.moveToFirst();
					/* SETTING QUESTION NO.14 */
					rd_InteriorWallCrackNoted=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("InteriorWallCrackNoted")));
					setvalueToRadio(cf.rd_obs14_yes,cf.rd_obs14_no,cf.rd_obs14_nd,rd_InteriorWallCrackNoted,cf.show_tbl_obs14); 
					cf.etcomments_obs14.setText(cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("InteriorWallCrackComments"))));
				    chk_Value14=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("InteriorWallCrackOption")));
				    cf.setvaluechk(chk_Value14,cf.cb_obs14,cf.etother_obs14);
				    
				    /* SETTING QUESTION NO.15 */
					rd_WallSlabCrackNoted=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("WallSlabCrackNoted")));
					setvalueToRadio(cf.rd_obs15_yes,cf.rd_obs15_no,cf.rd_obs15_nd,rd_WallSlabCrackNoted,cf.show_tbl_obs15); 
					cf.etcomments_obs15.setText(cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("WallSlabCrackComments"))));
				    chk_Value15=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("WallSlabCrackOption")));
				    cf.setvaluechk(chk_Value15,cf.cb_obs15,cf.etother_obs15);
				    				    
				    /* SETTING QUESTION NO.16 */
					rd_InteriorCeilingCrackNoted=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("InteriorCeilingCrackNoted")));
					setvalueToRadio(cf.rd_obs16_yes,cf.rd_obs16_no,cf.rd_obs16_nd,rd_InteriorCeilingCrackNoted,cf.show_tbl_obs16); 
					cf.etcomments_obs16.setText(cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("InteriorCeilingCrackComments"))));
				    chk_Value16=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("InteriorCeilingCrackOption")));
				    cf.setvaluechk(chk_Value16,cf.cb_obs16,cf.etother_obs16);
				    
				    /* SETTING QUESTION NO.17 */
					rd_InteriorFloorCrackNoted=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("InteriorFloorCrackNoted")));
					setvalueToRadio(cf.rd_obs17_yes,cf.rd_obs17_no,cf.rd_obs17_nd,rd_InteriorFloorCrackNoted,cf.show_tbl_obs17); 
					cf.etcomments_obs17.setText(cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("InteriorFloorCrackComments"))));
				    chk_Value17=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("InteriorFloorCrackOption")));
				    cf.setvaluechk(chk_Value17,cf.cb_obs17,cf.etother_obs17);
				    
				    /* SETTING QUESTION NO.18 */
					rd_WindowFrameOutofSquare=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("WindowFrameOutofSquare")));
					setvalueToRadio(cf.rd_obs18_yes,cf.rd_obs18_no,cf.rd_obs18_nd,rd_WindowFrameOutofSquare,cf.show_tbl_obs18); 
					cf.etcomments_obs18.setText(cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("WindowFrameOutofSquareComments"))));
				    chk_Value18=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("WindowFrameOutofSquareOption")));
				    cf.setvaluechk(chk_Value18,cf.cb_obs18,cf.etother_obs18);
				    
				    /* SETTING QUESTION NO.19 */
					rd_FloorSlopingNoted=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("FloorSlopingNoted")));
					setvalueToRadio(cf.rd_obs19_yes,cf.rd_obs19_no,cf.rd_obs19_nd,rd_FloorSlopingNoted,cf.show_tbl_obs19); 
					cf.etcomments_obs19.setText(cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("FloorSlopingComments"))));
				    chk_Value19=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("FloorSlopingOption")));
				    cf.setvaluechk(chk_Value19,cf.cb_obs19,cf.etother_obs19);
				    
				    /* SETTING QUESTION NO.20 */
					rd_CrackedGlazingNoted=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("CrackedGlazingNoted")));
					setvalueToRadio(cf.rd_obs20_yes,cf.rd_obs20_no,cf.rd_obs20_nd,rd_CrackedGlazingNoted,cf.show_tbl_obs20); 
					cf.etcomments_obs20.setText(cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("CrackedGlazingComments"))));
				    chk_Value20=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("CrackedGlazingOption")));
				    cf.setvaluechk(chk_Value20,cf.cb_obs20,cf.etother_obs20);
				    
				    /* SETTING QUESTION NO.21 */
					rd_PreviousCorrectiveMeasureNoted=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("PreviousCorrectiveMeasureNoted")));
					setvalueToRadio(cf.rd_obs21_yes,cf.rd_obs21_no,cf.rd_obs21_nd,rd_PreviousCorrectiveMeasureNoted,cf.show_tbl_obs21); 
					cf.etcomments_obs21.setText(cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("PreviousCorrectiveMeasureComments"))));
				    chk_Value21=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("PreviousCorrectiveMeasureOption")));
				    cf.setvaluechk(chk_Value21,cf.cb_obs21,cf.etother_obs21);
				    
				    /* SETTING QUESTION NO.22 */
					rd_BindingDoorOpeningNoted=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("BindingDoorOpeningNoted")));
					setvalueToRadio(cf.rd_obs22_yes,cf.rd_obs22_no,cf.rd_obs22_nd,rd_BindingDoorOpeningNoted,cf.show_tbl_obs22); 
					cf.etcomments_obs22.setText(cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("BindingDoorOpeningComments"))));
				    chk_Value22=cf.decode(OBS3_Retrive.getString(OBS3_Retrive.getColumnIndex("BindingDoorOpeningOption")));
				    cf.setvaluechk(chk_Value22,cf.cb_obs22,cf.etother_obs22);
				    
				}
		}
		catch (Exception E)
		{
			String strerrorlog="Selection of the Observation3 table not working ";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void setvalueToRadio(RadioButton rd_obs14_yes,
			RadioButton rd_obs14_no, RadioButton rd_obs14_nd,
			String rd_InteriorWallCrackNoted2, TableLayout show_tbl_obs14) {
		// TODO Auto-generated method stub
		if(rd_obs14_yes.getText().toString().equals(rd_InteriorWallCrackNoted2))
		{
			rd_obs14_yes.setChecked(true);
			show_tbl_obs14.setVisibility(cf.show.VISIBLE);
		}
		else if(rd_obs14_no.getText().toString().equals(rd_InteriorWallCrackNoted2))
		{
			rd_obs14_no.setChecked(true);
		}
		else if(rd_obs14_nd.getText().toString().equals(rd_InteriorWallCrackNoted2))
		{
			rd_obs14_nd.setChecked(true);
		}
	}
	class SH_textwatcher implements TextWatcher
		{
	         public int type;
			SH_textwatcher(int type)
			{
				this.type=type;
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(this.type==1)
				{
					cf.showing_limit(s.toString(),SH_OBS14P,SH_OBS14,SH_OBS14T,"499");
				}
				else if(this.type==2)
				{
					cf.showing_limit(s.toString(),SH_OBS15P,SH_OBS15,SH_OBS15T,"499");
				}
				else if(this.type==3)
				{
					cf.showing_limit(s.toString(),SH_OBS16P,SH_OBS16,SH_OBS16T,"499");
				}
				else if(this.type==4)
				{
					cf.showing_limit(s.toString(),SH_OBS17P,SH_OBS17,SH_OBS17T,"499");
				}
				else if(this.type==5)
				{
					cf.showing_limit(s.toString(),SH_OBS18P,SH_OBS18,SH_OBS18T,"499");
				}
				else if(this.type==6)
				{
					cf.showing_limit(s.toString(),SH_OBS19P,SH_OBS19,SH_OBS19T,"499");
				}
				else if(this.type==7)
				{
					cf.showing_limit(s.toString(),SH_OBS20P,SH_OBS20,SH_OBS20T,"499");
				}
				else if(this.type==8)
				{
					cf.showing_limit(s.toString(),SH_OBS21P,SH_OBS21,SH_OBS21T,"499");
				}
				else if(this.type==9)
				{
					cf.showing_limit(s.toString(),SH_OBS22P,SH_OBS22,SH_OBS22T,"499");
				}
			
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}
			
		}		 
	 class Obs_clicker implements OnClickListener	  {


		  private static final int visibility = 0;

		@Override
		  public void onClick(View v) {

		   // TODO Auto-generated method stub
			  switch(v.getId()){
			  case R.id.firstobsrow:/** SELECTING OBSERVATION14 QUESTION ROW **/
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs14.setVisibility(visibility);
				  cf.tbl_row_obs14.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs14_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.secondobsrow:/** SELECTING OBSERVATION15 QUESTION ROW **/
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs15.setVisibility(visibility);
				  cf.tbl_row_obs15.setBackgroundResource(R.drawable.subbackrepeatnor);
				   cf.obs15_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.thirdobsrow:/** SELECTING OBSERVATION16 QUESTION ROW **/
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs16.setVisibility(visibility);
				  cf.tbl_row_obs16.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs16_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.fourthobsrow:/** SELECTING OBSERVATION17 QUESTION ROW **/
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs17.setVisibility(visibility);
				  cf.tbl_row_obs17.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs17_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.fifthobsrow:/** SELECTING OBSERVATION18 QUESTION ROW **/
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs18.setVisibility(visibility);
				  cf.tbl_row_obs18.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs18_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.sixthobsrow:/** SELECTING OBSERVATION19 QUESTION ROW **/
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs19.setVisibility(visibility);
				  cf.tbl_row_obs19.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs19_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.seventhobsrow:/** SELECTING OBSERVATION20 QUESTION ROW **/
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs20.setVisibility(visibility);
				  cf.tbl_row_obs20.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs20_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.eigthobsrow:/** SELECTING OBSERVATION21 QUESTION ROW **/
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs21.setVisibility(visibility);
				  cf.tbl_row_obs21.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs21_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.ninthobsrow:/** SELECTING OBSERVATION22 QUESTION ROW **/
				  obs_tablelayout_hide();
				  cf.tbl_layout_obs22.setVisibility(visibility);
				  cf.tbl_row_obs22.setBackgroundResource(R.drawable.subbackrepeatnor);
				  cf.obs22_tbl_row_txt.setTextColor(Color.parseColor("#000000"));
				  break;
			  case R.id.obs14_rd_y: /** SELECTING OBSERVATION14 RADIOBUTTON YES **/
				  cf.show_tbl_obs14.setVisibility(visibility);
				  cf.etcomments_obs14.setText("");
				  rd_InteriorWallCrackNoted=retrieve_radioname(cf.rd_obs14_yes);
				 break;
			  case R.id.obs15_rd_y: /** SELECTING OBSERVATION15 RADIOBUTTON YES **/
				  cf.show_tbl_obs15.setVisibility(visibility);
				  cf.etcomments_obs15.setText("");
				  rd_WallSlabCrackNoted=retrieve_radioname(cf.rd_obs15_yes);
				 break;
			  case R.id.obs16_rd_y: /** SELECTING OBSERVATION16 RADIOBUTTON YES **/
				  cf.show_tbl_obs16.setVisibility(visibility);
				  cf.etcomments_obs16.setText("");
				  rd_InteriorCeilingCrackNoted=retrieve_radioname(cf.rd_obs16_yes);
				 break;
			  case R.id.obs17_rd_y: /** SELECTING OBSERVATION17 RADIOBUTTON YES **/
				  cf.show_tbl_obs17.setVisibility(visibility);
				  cf.etcomments_obs17.setText("");
				  rd_InteriorFloorCrackNoted=retrieve_radioname(cf.rd_obs17_yes);
				 break;
			  case R.id.obs18_rd_y: /** SELECTING OBSERVATION18 RADIOBUTTON YES **/
				  cf.show_tbl_obs18.setVisibility(visibility);
				  cf.etcomments_obs18.setText("");
				  rd_WindowFrameOutofSquare=retrieve_radioname(cf.rd_obs18_yes);
				 break;
			  case R.id.obs19_rd_y: /** SELECTING OBSERVATION19 RADIOBUTTON YES **/
				  cf.show_tbl_obs19.setVisibility(visibility);
				  cf.etcomments_obs19.setText("");
				  rd_FloorSlopingNoted=retrieve_radioname(cf.rd_obs19_yes);
				 break;
			  case R.id.obs20_rd_y: /** SELECTING OBSERVATION20 RADIOBUTTON YES **/
				  cf.show_tbl_obs20.setVisibility(visibility);
				  cf.etcomments_obs20.setText("");
				  rd_CrackedGlazingNoted=retrieve_radioname(cf.rd_obs20_yes);
				 break;
			  case R.id.obs21_rd_y: /** SELECTING OBSERVATION21 RADIOBUTTON YES **/
				  cf.show_tbl_obs21.setVisibility(visibility);
				  cf.etcomments_obs21.setText("");
				  rd_PreviousCorrectiveMeasureNoted=retrieve_radioname(cf.rd_obs21_yes);
				 break;
			  case R.id.obs22_rd_y: /** SELECTING OBSERVATION22 RADIOBUTTON YES **/
				  cf.show_tbl_obs22.setVisibility(visibility);
				  cf.etcomments_obs22.setText("");
				  rd_BindingDoorOpeningNoted=retrieve_radioname(cf.rd_obs22_yes);
				 break;
			  case R.id.obs14_rd_n: /** SELECTING OBSERVATION14 RADIOBUTTON NO **/
				  cf.show_tbl_obs14.setVisibility(v.GONE);
				  cf.etcomments_obs14.setText(cf.nostr);
				  rd_InteriorWallCrackNoted=retrieve_radioname(cf.rd_obs14_no);
				  cf.Set_UncheckBox(cf.cb_obs14,cf.etother_obs14);
				 break;
			  case R.id.obs15_rd_n: /** SELECTING OBSERVATION15 RADIOBUTTON NO **/
				  cf.show_tbl_obs15.setVisibility(v.GONE);
				  cf.etcomments_obs15.setText(cf.nostr);
				  rd_WallSlabCrackNoted=retrieve_radioname(cf.rd_obs15_no);
				 cf.Set_UncheckBox(cf.cb_obs15,cf.etother_obs15);
				 break;
			  case R.id.obs16_rd_n: /** SELECTING OBSERVATION16 RADIOBUTTON NO **/
				  cf.show_tbl_obs16.setVisibility(v.GONE);
				  cf.etcomments_obs16.setText(cf.nostr);
				  rd_InteriorCeilingCrackNoted=retrieve_radioname(cf.rd_obs16_no);
				  cf.Set_UncheckBox(cf.cb_obs16,cf.etother_obs16);
				 break;
			  case R.id.obs17_rd_n: /** SELECTING OBSERVATION17 RADIOBUTTON NO **/
				  cf.show_tbl_obs17.setVisibility(v.GONE);
				  cf.etcomments_obs17.setText(cf.nostr);
				  rd_InteriorFloorCrackNoted=retrieve_radioname(cf.rd_obs17_no);
				  cf.Set_UncheckBox(cf.cb_obs17,cf.etother_obs17);
				 break;
			  case R.id.obs18_rd_n: /** SELECTING OBSERVATION18 RADIOBUTTON NO **/
				  cf.show_tbl_obs18.setVisibility(v.GONE);
				  cf.etcomments_obs18.setText(cf.nostr);
				  rd_WindowFrameOutofSquare=retrieve_radioname(cf.rd_obs18_no);
				  cf.Set_UncheckBox(cf.cb_obs18,cf.etother_obs18);
				 break;
			  case R.id.obs19_rd_n: /** SELECTING OBSERVATION19 RADIOBUTTON NO **/
				  cf.show_tbl_obs19.setVisibility(v.GONE);
				  cf.etcomments_obs19.setText(cf.nostr);
				  rd_FloorSlopingNoted=retrieve_radioname(cf.rd_obs19_no);
				  cf.Set_UncheckBox(cf.cb_obs19,cf.etother_obs19);
				 break;
			  case R.id.obs20_rd_n: /** SELECTING OBSERVATION20 RADIOBUTTON NO **/
				  cf.show_tbl_obs20.setVisibility(v.GONE);
				  cf.etcomments_obs20.setText(cf.nostr);
				  rd_CrackedGlazingNoted=retrieve_radioname(cf.rd_obs20_no);
				  cf.Set_UncheckBox(cf.cb_obs20,cf.etother_obs20);
				 break;
			  case R.id.obs21_rd_n: /** SELECTING OBSERVATION21 RADIOBUTTON NO **/
				  cf.show_tbl_obs21.setVisibility(v.GONE);
				  cf.etcomments_obs21.setText(cf.nostr);
				  rd_PreviousCorrectiveMeasureNoted=retrieve_radioname(cf.rd_obs21_no);
				  cf.Set_UncheckBox(cf.cb_obs21,cf.etother_obs21);
				 break;
			  case R.id.obs22_rd_n: /** SELECTING OBSERVATION22 RADIOBUTTON NO **/
				  cf.show_tbl_obs22.setVisibility(v.GONE);
				  cf.etcomments_obs22.setText(cf.nostr);
				  rd_BindingDoorOpeningNoted=retrieve_radioname(cf.rd_obs22_no);
				  cf.Set_UncheckBox(cf.cb_obs22,cf.etother_obs22);
				 break;
			  case R.id.obs14_rd_nd: /** SELECTING OBSERVATION14 RADIOBUTTON NOTDETERMINED **/
				  cf.show_tbl_obs14.setVisibility(v.GONE);
				  cf.etcomments_obs14.setText("");
				  rd_InteriorWallCrackNoted=retrieve_radioname(cf.rd_obs14_nd);
				  cf.Set_UncheckBox(cf.cb_obs14,cf.etother_obs14);
				 break;
			  case R.id.obs15_rd_nd: /** SELECTING OBSERVATION15 RADIOBUTTON NOTDETERMINED **/
				  cf.show_tbl_obs15.setVisibility(v.GONE);
				  cf.etcomments_obs15.setText("");
				  rd_WallSlabCrackNoted=retrieve_radioname(cf.rd_obs15_nd);
				  cf.Set_UncheckBox(cf.cb_obs15,cf.etother_obs15);
				 break;
			  case R.id.obs16_rd_nd: /** SELECTING OBSERVATION16 RADIOBUTTON NOTDETERMINED **/
				  cf.show_tbl_obs16.setVisibility(v.GONE);
				  cf.etcomments_obs16.setText("");
				  rd_InteriorCeilingCrackNoted=retrieve_radioname(cf.rd_obs16_nd);
				  cf.Set_UncheckBox(cf.cb_obs16,cf.etother_obs16);
				 break;
			  case R.id.obs17_rd_nd: /** SELECTING OBSERVATION17 RADIOBUTTON NOTDETERMINED **/
				  cf.show_tbl_obs17.setVisibility(v.GONE);
				  cf.etcomments_obs17.setText("");
				  rd_InteriorFloorCrackNoted=retrieve_radioname(cf.rd_obs17_nd);
				  cf.Set_UncheckBox(cf.cb_obs17,cf.etother_obs17);
				 break;
			  case R.id.obs18_rd_nd: /** SELECTING OBSERVATION18 RADIOBUTTON NOTDETERMINED **/
				  cf.show_tbl_obs18.setVisibility(v.GONE);
				  cf.etcomments_obs18.setText("");
				  rd_WindowFrameOutofSquare=retrieve_radioname(cf.rd_obs18_nd);
				  cf.Set_UncheckBox(cf.cb_obs18,cf.etother_obs18);
				 break;
			  case R.id.obs19_rd_nd: /** SELECTING OBSERVATION19 RADIOBUTTON NOTDETERMINED **/
				  cf.show_tbl_obs19.setVisibility(v.GONE);
				  cf.etcomments_obs19.setText("");
				  rd_FloorSlopingNoted=retrieve_radioname(cf.rd_obs19_nd);
				  cf.Set_UncheckBox(cf.cb_obs19,cf.etother_obs19);
				 break;
			  case R.id.obs20_rd_nd: /** SELECTING OBSERVATION20 RADIOBUTTON NOTDETERMINED **/
				  cf.show_tbl_obs20.setVisibility(v.GONE);
				  cf.etcomments_obs20.setText("");
				  rd_CrackedGlazingNoted=retrieve_radioname(cf.rd_obs20_nd);
				  cf.Set_UncheckBox(cf.cb_obs20,cf.etother_obs20);
				 break;
			  case R.id.obs21_rd_nd: /** SELECTING OBSERVATION21 RADIOBUTTON NOTDETERMINED **/
				  cf.show_tbl_obs21.setVisibility(v.GONE);
				  cf.etcomments_obs21.setText("");
				  rd_PreviousCorrectiveMeasureNoted=retrieve_radioname(cf.rd_obs21_nd);
				  cf.Set_UncheckBox(cf.cb_obs21,cf.etother_obs21);
				 break;
			  case R.id.obs22_rd_nd: /** SELECTING OBSERVATION22 RADIOBUTTON NOTDETERMINED **/
				  cf.show_tbl_obs22.setVisibility(v.GONE);
				  cf.etcomments_obs22.setText("");
				  rd_BindingDoorOpeningNoted=retrieve_radioname(cf.rd_obs22_nd);
				  cf.Set_UncheckBox(cf.cb_obs22,cf.etother_obs22);
				 break;
			  case R.id.obs14_other:/** SELECTING CHECKBOX - OBSERVATION14 - OTHER **/
				  if(cf.cb_obs14[24].isChecked())
				  {
					  cf.etother_obs14.setVisibility(visibility);
					  cf.etother_obs14.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs14.setVisibility(cf.show.GONE);
					 
				  }
				  
				  break;
			  case R.id.obs15_other:/** SELECTING CHECKBOX - OBSERVATION15 - OTHER **/
				  if(cf.cb_obs15[24].isChecked())
				  {
					  cf.etother_obs15.setVisibility(visibility);
				      cf.etother_obs15.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs15.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  case R.id.obs16_other:/** SELECTING CHECKBOX - OBSERVATION16 - OTHER **/
				  if(cf.cb_obs16[24].isChecked())
				  {
				     cf.etother_obs16.setVisibility(visibility);
				     cf.etother_obs16.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs16.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  case R.id.obs17_other:/** SELECTING CHECKBOX - OBSERVATION17 - OTHER **/
				  if(cf.cb_obs17[24].isChecked())
				  {
					  cf.etother_obs17.setVisibility(visibility);
					  cf.etother_obs17.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs17.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  case R.id.obs18_other:/** SELECTING CHECKBOX - OBSERVATION18 - OTHER **/
				  if(cf.cb_obs18[24].isChecked())
				  {
					  cf.etother_obs18.setVisibility(visibility);
					  cf.etother_obs18.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs18.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  case R.id.obs19_other:/** SELECTING CHECKBOX - OBSERVATION19 - OTHER **/
				  if(cf.cb_obs19[24].isChecked())
				  {
					  cf.etother_obs19.setVisibility(visibility);
					  cf.etother_obs19.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs19.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  case R.id.obs20_other:/** SELECTING CHECKBOX - OBSERVATION20 - OTHER **/
				  if(cf.cb_obs20[24].isChecked())
				  {
					  cf.etother_obs20.setVisibility(visibility);
					  cf.etother_obs20.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs20.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  case R.id.obs21_other:/** SELECTING CHECKBOX - OBSERVATION21 - OTHER **/
				  if(cf.cb_obs21[24].isChecked())
				  {
					  cf.etother_obs21.setVisibility(visibility);
					  cf.etother_obs21.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs21.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  case R.id.obs22_other:/** SELECTING CHECKBOX - OBSERVATION22 - OTHER **/
				  if(cf.cb_obs22[24].isChecked())
				  {
					  cf.etother_obs22.setVisibility(visibility);
					  cf.etother_obs22.requestFocus();
				  }
				  else
				  {
					  cf.etother_obs22.setVisibility(cf.show.GONE);
					 
				  }
				  break;
			  }
		}
	 }
		 public void obs_tablelayout_hide() {
				// TODO Auto-generated method stub
			 
				cf.tbl_layout_obs14.setVisibility(cf.show.GONE);
				cf.tbl_layout_obs15.setVisibility(cf.show.GONE);
				cf.tbl_layout_obs16.setVisibility(cf.show.GONE);
				cf.tbl_layout_obs17.setVisibility(cf.show.GONE);
				cf.tbl_layout_obs18.setVisibility(cf.show.GONE);
				cf.tbl_layout_obs19.setVisibility(cf.show.GONE);
				cf.tbl_layout_obs20.setVisibility(cf.show.GONE);
				cf.tbl_layout_obs21.setVisibility(cf.show.GONE);
				cf.tbl_layout_obs22.setVisibility(cf.show.GONE);
				cf.tbl_row_obs14.setBackgroundResource(R.drawable.backrepeatnor);
				cf.tbl_row_obs15.setBackgroundResource(R.drawable.backrepeatnor);
				cf.tbl_row_obs16.setBackgroundResource(R.drawable.backrepeatnor);
				cf.tbl_row_obs17.setBackgroundResource(R.drawable.backrepeatnor);
				cf.tbl_row_obs18.setBackgroundResource(R.drawable.backrepeatnor);
				cf.tbl_row_obs19.setBackgroundResource(R.drawable.backrepeatnor);
				cf.tbl_row_obs20.setBackgroundResource(R.drawable.backrepeatnor);
				cf.tbl_row_obs21.setBackgroundResource(R.drawable.backrepeatnor);
				cf.tbl_row_obs22.setBackgroundResource(R.drawable.backrepeatnor);
				
			}
		 public String retrieve_radioname(RadioButton rd_obs14_yes) {
			// TODO Auto-generated method stub
			 return rd_obs14_yes.getText().toString();
		}
		public void clicker(View v) {

	         switch(v.getId()){
				  case R.id.obs14_help: /** HELP FOR OBSERVATION14 **/
					  cf.alerttitle="HELP";
					  cf.alertcontent="<p>Inspectors must analyze " +
					  		"all interior areas, examining all visible wall surfaces looking for " +
					  		"evidence or clues of any settlement or cracking associated with " +
					  		"shrinkage, sinkhole or other structural issue.</p><p>Interior wall " +
					  		"cracks relating to serious structural settlement typically can be found " +
					  		"in a diagonal flashing above openings along the drywall.  Inspectors " +
					  		"should pay close attention in these areas for displacement of adjoining " +
					  		"floor ceiling areas to determine if any previous cracking or repairs " +
					  		"were carried out, including recent painting.</p><p>Inspectors should " +
					  		"comment and photograph all areas where cracking is noted during the " +
					  		"internal inspection.</p>";
					  cf.showhelp(cf.alerttitle,cf.alertcontent);
							  break;
				  case R.id.obs15_help: /** HELP FOR OBSERVATION15 **/
					  cf.alerttitle="HELP";
					  cf.alertcontent="<p>Inspectors need to pay " +
					  		"close attention to all visible and accessible areas of wall and floor " +
					  		"junctions to determine if any separation or movement has occurred " +
					  		"between the slab and the main exterior and interior wall " +
					  		"structures.</p><p>Slab separation from the wall structure is indicative " +
					  		"of settlement of some nature and must be documented and photographed by " +
					  		"all inspectors.  Limitations to this inspection should be clearly " +
					  		"outlined, particularly where carpets, furniture and other " +
					  		"owners\'; belongings are present.</p><p>Inspectors should separate " +
					  		"typical shrinkage and separation cracks along the baseboards  to that of " +
					  		" settling slabs or building movement.</p><p>Where any separation is " +
					  		"noted, Inspectors are required to photograph using a 4\" spirit " +
					  		"level, the floor area involved clearly identifying on the photograph the " +
					  		"4\"; spirit level and its findings in relation to a level and " +
					  		"plumb floor.</p>";
					  cf.showhelp(cf.alerttitle,cf.alertcontent);
							  break;
				  case R.id.obs16_help: /** HELP FOR OBSERVATION16 **/
					  cf.alerttitle="HELP";
					  cf.alertcontent="<p>Inspectors need to pay " +
					  		"attention to the ceiling and wall junctions throughout the interior " +
					  		"survey.  Separation between ceiling and walls is indicative of potential " +
					  		"movement and should be photographed and recorded.  Where noted, ceiling " +
					  		"and wall separation is required to be photographed using a 4\" " +
					  		"spirit level, with the 4\" spirit level in place on the " +
					  		"photograph provided.  Ample commentary should be made to ensure auditors " +
					  		"and underwriters are fully versed on the severity of the conditions " +
					  		"found.</p>";
					  cf.showhelp(cf.alerttitle,cf.alertcontent);
							  break;
				  case R.id.obs17_help: /** HELP FOR OBSERVATION17 **/
					  cf.alerttitle="HELP";
					  cf.alertcontent="<p>While most of the floor " +
					  		"areas will in essence be covered with floor covering, Inspectors need to " +
					  		"pay particularly attention to areas of the floor that are accessible or " +
					  		"visible such as the garage floor, tiled flooring or linoleum covered " +
					  		"areas.  Floor cracking typically is evident along the grout joints, can " +
					  		"be seen under linoleum-type finishes and visible to unfinished areas " +
					  		"such as garages.</p><p>Inspectors should photograph and document any " +
					  		"cracking evident, ensuring that a 4\" spirit level is included " +
					  		"within the photographs provided.</p><p>Inspectors should follow the line " +
					  		"of cracking in any area to the exterior wall to determine if any " +
					  		"corresponding cracks are evident on the exterior foundation wall.  The " +
					  		"examination of the interior drywall at the crack termination with the " +
					  		"exterior walls or surrounding locations should be conducted and reported " +
					  		"within the report.</p><p>Inspectors should comment and photograph all " +
					  		"relevant findings.</p>";
					  cf.showhelp(cf.alerttitle,cf.alertcontent);
							  break;
				  case R.id.obs18_help: /** HELP FOR OBSERVATION18 **/
					  cf.alerttitle="HELP";
					  cf.alertcontent="<p>Inspectors need to pay " +
					  		"close attention to the exterior wall surfaces/finishes of every floor " +
					  		"level, for any evidence of bulging or unevenness.</p><p>A 4 foot level " +
					  		"should be used and photographed to verify the inspector\'s " +
					  		"findings in relation to the exterior wall survey.</p><p>If the Inspector " +
					  		"is unable to determine or evaluate the entire exterior area because of " +
					  		"the type of structure, zero lot line or other, this should be clearly " +
					  		"identified in the comment area.</p>";
					  cf.showhelp(cf.alerttitle,cf.alertcontent);
							  break;
				  case R.id.obs19_help: /** HELP FOR OBSERVATION19 **/
					  cf.alerttitle="HELP";
					  cf.alertcontent="<p>Inspectors need to pay " +
					  		"close attention as they survey the interior of the home for any evidence " +
					  		"of uneven flooring or out of square flooring.</p><p>At least one " +
					  		"photograph will be required from the inspector of a floor area where a " +
					  		"4\" spirit level was used, clearly depicting the 4\" " +
					  		"spirit level and slope of the floor.</p><p>Any adverse situations should " +
					  		"be properly cataloged, photographed, annotated and commented on.</p>";
					  cf.showhelp(cf.alerttitle,cf.alertcontent);
							  break;
				  case R.id.obs20_help: /** HELP FOR OBSERVATION20 **/
					  cf.alerttitle="HELP";
					  cf.alertcontent="<p>Inspectors are required " +
					  		"to identify and record all cracked glazing and clearly state within the " +
					  		"report that the cracked glazing is a result of impact or other normal " +
					  		"occurrence.</p><p>Settlement or sinkhole activities relating to glazing, " +
					  		"cracked glass found which is associated to compression or settlement as " +
					  		"a result of abnormal structural activity needs to be " +
					  		"reported.</p><p>Compression breakage will have other ancillary visible " +
					  		"issues such as damaged or buckled framing, cracking around openings, " +
					  		"binding windows, etc. which should all be analyzed and reported on as " +
					  		"part of the inspection service if cracked glazing is " +
					  		"found.</p><p>Cracked glazing associated with normal wear and tear, will " +
					  		"also need to be recorded by the inspector, in addition to confirming " +
					  		"that all openings were working properly and no other issues were " +
					  		"present.</p><p>Photographs are required to back up the verification " +
					  		"process.</p>";
					  cf.showhelp(cf.alerttitle,cf.alertcontent);
							  break;
				  case R.id.obs21_help: /** HELP FOR OBSERVATION21 **/
					  cf.alerttitle="HELP";
					  cf.alertcontent="<p>Inspectors, as part of " +
					  		"this inspection should attempt to identify any previous structural " +
					  		"repairs or issues that have occurred and report accordingly.</p><p>Any " +
					  		"evidence of structural repairs, rebuilding, extensive cracking or " +
					  		"remodeling should all be recorded and identified.  The policyholder " +
					  		"should also be questioned as to the extent of repairs or any other " +
					  		"issues that were conducted over the history of the home.</p>";
					  cf.showhelp(cf.alerttitle,cf.alertcontent);
							  break;
				  case R.id.obs22_help: /** HELP FOR OBSERVATION22 **/
					  cf.alerttitle="HELP";
					  cf.alertcontent="<p>Any evidence of " +
					  		"settlement around window openings requires inspectors to open and " +
					  		"operate all doors and other hardware during the inspection to confirm " +
					  		"proper operation.  Any separation, binding or large gaps present or " +
					  		"found during the inspection should be properly photographed and " +
					  		"recorded.</p>";
					  cf.showhelp(cf.alerttitle,cf.alertcontent);
							  break;
				  case R.id.home: /*SELECTING HOME */
					  cf.gohome();
					  break;
				  case R.id.save: /*SELECTING SAVE*/
					  fn_save();
					  break;
	         }
		 }
		private void fn_save() {
			// TODO Auto-generated method stub
			if(rd_InteriorWallCrackNoted.equals(""))/* CHECK QUESTION NO.23 RADIO BUTON*/
			{
				
				cf.ShowToast("Please select any answer for Question No.14", 0);
				cf.rd_obs14_yes.requestFocus();
			}
			else
			{
				 if(rd_InteriorWallCrackNoted.equals("Yes"))
				 {
					 chk_Value14= cf.getselected_chk(cf.cb_obs14);
					 String chk_Value14other =(cf.cb_obs14[cf.cb_obs14.length-1].isChecked())? "^"+cf.etother_obs14.getText().toString():""; // append the other text value in to the selected option
					 chk_Value14+=chk_Value14other;
					 if(chk_Value14.equals(""))
					 {
						 
						 cf.ShowToast("Please select any checkbox for Question No.14 Observation" , 0);
					 }
					 else
					 {
						 if(cf.cb_obs14[cf.cb_obs14.length-1].isChecked())
						 {
							 if(chk_Value14other.trim().equals("^"))
							 {
								 cf.ShowToast("Please enter other text for Question No.14 Observation", 0);
								 cf.etother_obs14.requestFocus();
							 }
							 else
							 {
								 chk_Value14 +=(cf.cb_obs14[cf.cb_obs14.length-1].isChecked())? "^"+cf.etother_obs14.getText().toString():""; // append the other text value in to the selected option
								 Chk_Obs14_Comments();	
							 }
						 }
						 else
						 {
							 Chk_Obs14_Comments();
						 }
						 
					 }
				 }
				 else
				 {
					 Chk_Obs14_Comments();
				 }
				
			}
		}
		private void Chk_Obs14_Comments() {
			// TODO Auto-generated method stub
			if(cf.etcomments_obs14.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.14 */
			{
				cf.ShowToast("Please enter Question No.14 Comments", 0);
				cf.etcomments_obs14.requestFocus();
			}
			else
			{
				if(rd_WallSlabCrackNoted.equals(""))/* CHECK QUESTION NO.15 RADIO BUTON*/
				{
					cf.ShowToast("Please select any answer for Question No.15", 0);
					cf.rd_obs15_yes.requestFocus();
				}
				else
				{
					 if(rd_WallSlabCrackNoted.equals("Yes"))
					 {
						 chk_Value15= cf.getselected_chk(cf.cb_obs15);
						 String chk_Value15other =(cf.cb_obs15[cf.cb_obs15.length-1].isChecked())? "^"+cf.etother_obs15.getText().toString():""; // append the other text value in to the selected option
						 chk_Value15+=chk_Value15other;
						 if(chk_Value15.equals(""))
						 {
							 
							 cf.ShowToast("Please select any checkbox for Question No.15 Observation" , 0);
						 }
						 else
						 {
							 if(cf.cb_obs15[cf.cb_obs15.length-1].isChecked())
							 {
								 if(chk_Value15other.trim().equals("^"))
								 {
									 cf.ShowToast("Please enter other text for Question No.15 Observation", 0);
									 cf.etother_obs15.requestFocus();
								 }
								 else
								 {
									 chk_Value15 +=(cf.cb_obs15[cf.cb_obs15.length-1].isChecked())? "^"+cf.etother_obs15.getText().toString():""; // append the other text value in to the selected option
									 Chk_Obs15_Comments();	
								 }
							 }
							 else
							 {
								 Chk_Obs15_Comments();
							 }
							 
						 }
					 }
					 else
					 {
						 Chk_Obs15_Comments();
					 }
				}
			}
		}
		private void Chk_Obs15_Comments() {
			// TODO Auto-generated method stub
			if(cf.etcomments_obs15.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.15 */
			{
				cf.ShowToast("Please enter Question No.15 Comments", 0);
				cf.etcomments_obs15.requestFocus();
			}
			else
			{
				if(rd_InteriorCeilingCrackNoted.equals(""))/* CHECK QUESTION NO.16 RADIO BUTON*/
				{
					cf.ShowToast("Please select any answer for Question No.16", 0);
					cf.rd_obs16_yes.requestFocus();
				}
				else
				{
					 if(rd_InteriorCeilingCrackNoted.equals("Yes"))
					 {
						 chk_Value16= cf.getselected_chk(cf.cb_obs16);
						 String chk_Value16other =(cf.cb_obs16[cf.cb_obs16.length-1].isChecked())? "^"+cf.etother_obs16.getText().toString():""; // append the other text value in to the selected option
						 chk_Value16+=chk_Value16other;
						 if(chk_Value16.equals(""))
						 {
							 
							 cf.ShowToast("Please select any checkbox for Question No.16 Observation" , 0);
						 }
						 else
						 {
							 if(cf.cb_obs16[cf.cb_obs16.length-1].isChecked())
							 {
								 if(chk_Value16other.trim().equals("^"))
								 {
									 cf.ShowToast("Please enter other text for Question No.16 Observation", 0);
									 cf.etother_obs16.requestFocus();
								 }
								 else
								 {
									 chk_Value16 +=(cf.cb_obs16[cf.cb_obs16.length-1].isChecked())? "^"+cf.etother_obs16.getText().toString():""; // append the other text value in to the selected option
									 Chk_Obs16_Comments();	
								 }
							 }
							 else
							 {
								 Chk_Obs16_Comments();
							 }
							 
						 }
					 }
					 else
					 {
						 Chk_Obs16_Comments();
					 }
				}
			}
		}
		private void Chk_Obs16_Comments() {
			// TODO Auto-generated method stub
			if(cf.etcomments_obs16.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.16 */
			{
				cf.ShowToast("Please enter Question No.16 Comments", 0);
				cf.etcomments_obs16.requestFocus();
			}
			else
			{
				if(rd_InteriorFloorCrackNoted.equals(""))/* CHECK QUESTION NO.17 RADIO BUTON*/
				{
					cf.ShowToast("Please select any answer for Question No.17", 0);
					cf.rd_obs17_yes.requestFocus();
				}
				else
				{
					 if(rd_InteriorFloorCrackNoted.equals("Yes"))
					 {
						 chk_Value17= cf.getselected_chk(cf.cb_obs17);
						 String chk_Value17other =(cf.cb_obs17[cf.cb_obs17.length-1].isChecked())? "^"+cf.etother_obs17.getText().toString():""; // append the other text value in to the selected option
						 chk_Value17+=chk_Value17other;
						 if(chk_Value17.equals(""))
						 {
							 
							 cf.ShowToast("Please select any checkbox for Question No.17 Observation" , 0);
						 }
						 else
						 {
							 if(cf.cb_obs17[cf.cb_obs17.length-1].isChecked())
							 {
								 if(chk_Value17other.trim().equals("^"))
								 {
									 cf.ShowToast("Please enter other text for Question No.17 Observation", 0);
									 cf.etother_obs17.requestFocus();
								 }
								 else
								 {
									 chk_Value17 +=(cf.cb_obs17[cf.cb_obs17.length-1].isChecked())? "^"+cf.etother_obs17.getText().toString():""; // append the other text value in to the selected option
									 Chk_Obs17_Comments();	
								 }
							 }
							 else
							 {
								 Chk_Obs17_Comments();
							 }
							 
						 }
					 }
					 else
					 {
						 Chk_Obs17_Comments();
					 }
				}
			}
		}
		private void Chk_Obs17_Comments() {
			// TODO Auto-generated method stub
			if(cf.etcomments_obs17.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.17 */
			{
				cf.ShowToast("Please enter Question No.17 Comments", 0);
				cf.etcomments_obs17.requestFocus();
			}
			else
			{
				if(rd_WindowFrameOutofSquare.equals(""))/* CHECK QUESTION NO.18 RADIO BUTON*/
				{
					cf.ShowToast("Please select any answer for Question No.18", 0);
					cf.rd_obs18_yes.requestFocus();
				}
				else
				{
					 if(rd_WindowFrameOutofSquare.equals("Yes"))
					 {
						 chk_Value18= cf.getselected_chk(cf.cb_obs18);
						 String chk_Value18other =(cf.cb_obs18[cf.cb_obs18.length-1].isChecked())?"^"+cf.etother_obs18.getText().toString():""; // append the other text value in to the selected option
						 chk_Value18+=chk_Value18other;
						 if(chk_Value18.equals(""))
						 {
							 
							 cf.ShowToast("Please select any checkbox for Question No.18 Observation" , 0);
						 }
						 else
						 {
							 if(cf.cb_obs18[cf.cb_obs18.length-1].isChecked())
							 {
								 if(chk_Value18other.trim().equals("^"))
								 {
									 cf.ShowToast("Please enter other text for Question No.18 Observation", 0);
									 cf.etother_obs18.requestFocus();
								 }
								 else
								 {
									 chk_Value18 +=(cf.cb_obs18[cf.cb_obs18.length-1].isChecked())? "^"+cf.etother_obs18.getText().toString():""; // append the other text value in to the selected option
									 Chk_Obs18_Comments();	
								 }
							 }
							 else
							 {
								 Chk_Obs18_Comments();
							 }
							 
						 }
			        }
					 else
					 {
						 Chk_Obs18_Comments();
					 }
			   }
			}
		}
		private void Chk_Obs18_Comments() {
			// TODO Auto-generated method stub
			if(cf.etcomments_obs18.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.18 */
			{
				cf.ShowToast("Please enter Question No.18 Comments", 0);
				cf.etcomments_obs18.requestFocus();
			}
			else
			{
				if(rd_FloorSlopingNoted.equals(""))/* CHECK QUESTION NO.19 RADIO BUTON*/
				{
					cf.ShowToast("Please select any answer for Question No.19", 0);
					cf.rd_obs19_yes.requestFocus();
				}
				else
				{
					 if(rd_FloorSlopingNoted.equals("Yes"))
					 {
						 chk_Value19= cf.getselected_chk(cf.cb_obs19);
						 String chk_Value19other =(cf.cb_obs19[cf.cb_obs19.length-1].isChecked())? "^"+cf.etother_obs19.getText().toString():""; // append the other text value in to the selected option
						 chk_Value19+=chk_Value19other;
						 if(chk_Value19.equals(""))
						 {
							 
							 cf.ShowToast("Please select any checkbox for Question No.19 Observation" , 0);
						 }
						 else
						 {
							 if(cf.cb_obs19[cf.cb_obs19.length-1].isChecked())
							 {
								 if(chk_Value19other.trim().equals("^"))
								 {
									 cf.ShowToast("Please enter other text for Question No.19 Observation", 0);
									 cf.etother_obs19.requestFocus();
								 }
								 else
								 {
									 chk_Value19 +=(cf.cb_obs19[cf.cb_obs19.length-1].isChecked())? "^"+cf.etother_obs19.getText().toString():""; // append the other text value in to the selected option
									 Chk_Obs19_Comments();	
								 }
							 }
							 else
							 {
								 Chk_Obs19_Comments();
							 }
							 
						 }
					 }
					 else
					 {
						 Chk_Obs19_Comments();
					 }
				}
			}
		}
		private void Chk_Obs19_Comments() {
			// TODO Auto-generated method stub
			if(cf.etcomments_obs19.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.19 */
			{
				cf.ShowToast("Please enter Question No.19 Comments", 0);
				cf.etcomments_obs19.requestFocus();
			}
			else
			{
				if(rd_CrackedGlazingNoted.equals(""))/* CHECK QUESTION NO.20 RADIO BUTON*/
				{
					cf.ShowToast("Please select any answer for Question No.20", 0);
					cf.rd_obs20_yes.requestFocus();
				}
				else
				{
					 if(rd_CrackedGlazingNoted.equals("Yes"))
					 {
						 chk_Value20= cf.getselected_chk(cf.cb_obs20);
						 String chk_Value20other =(cf.cb_obs20[cf.cb_obs20.length-1].isChecked())? "^"+cf.etother_obs20.getText().toString():""; // append the other text value in to the selected option
						 chk_Value20+=chk_Value20other;
						 if(chk_Value20.equals(""))
						 {
							 
							 cf.ShowToast("Please select any checkbox for Question No.20 Observation" , 0);
						 }
						 else
						 {
							 if(cf.cb_obs20[cf.cb_obs20.length-1].isChecked())
							 {
								 if(chk_Value20other.trim().equals("^"))
								 {
									 cf.ShowToast("Please enter other text for Question No.20 Observation", 0);
									 cf.etother_obs20.requestFocus();
								 }
								 else
								 {
									 chk_Value20 +=(cf.cb_obs20[cf.cb_obs20.length-1].isChecked())? "^"+cf.etother_obs20.getText().toString():""; // append the other text value in to the selected option
									 Chk_Obs20_Comments();	
								 }
							 }
							 else
							 {
								 Chk_Obs20_Comments();
							 }
							 
						 }
					 }
					 else
					 {
						 Chk_Obs20_Comments();
					 }
				}
			}
		}
		private void Chk_Obs20_Comments() {
			// TODO Auto-generated method stub
			if(cf.etcomments_obs20.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.20 */
			{
				cf.ShowToast("Please enter Question No.20 Comments", 0);
				cf.etcomments_obs20.requestFocus();
			}
			else
			{
				if(rd_PreviousCorrectiveMeasureNoted.equals(""))/* CHECK QUESTION NO.21 RADIO BUTON*/
				{
					cf.ShowToast("Please select any answer for Question No.21", 0);
					cf.rd_obs21_yes.requestFocus();
				}
				else
				{
					 if(rd_PreviousCorrectiveMeasureNoted.equals("Yes"))
					 {
						 chk_Value21= cf.getselected_chk(cf.cb_obs21);
						 String chk_Value21other =(cf.cb_obs21[cf.cb_obs21.length-1].isChecked())?"^"+cf.etother_obs21.getText().toString():""; // append the other text value in to the selected option
						 chk_Value21+=chk_Value21other;
						 if(chk_Value21.equals(""))
						 {
							 
							 cf.ShowToast("Please select any checkbox for Question No.21 Observation" , 0);
						 }
						 else
						 {
							 if(cf.cb_obs21[cf.cb_obs21.length-1].isChecked())
							 {
								 if(chk_Value21other.trim().equals("^"))
								 {
									 cf.ShowToast("Please enter other text for Question No.21 Observation", 0);
									 cf.etother_obs21.requestFocus();
								 }
								 else
								 {
									 chk_Value21 +=(cf.cb_obs21[cf.cb_obs21.length-1].isChecked())? "^"+cf.etother_obs21.getText().toString():""; // append the other text value in to the selected option
									 Chk_Obs21_Comments();	
								 }
							 }
							 else
							 {
								 Chk_Obs21_Comments();
							 }
							 
						 }
					 }
					 else
					 {
						 Chk_Obs21_Comments();
					 }
				}
			}
		}
		private void Chk_Obs21_Comments() {
			// TODO Auto-generated method stub
			if(cf.etcomments_obs21.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.21 */
			{
				cf.ShowToast("Please enter Question No.21 Comments", 0);
				cf.etcomments_obs21.requestFocus();
			}
			else
			{
				if(rd_BindingDoorOpeningNoted.equals(""))/* CHECK QUESTION NO.22 RADIO BUTON*/
				{
					cf.ShowToast("Please select any answer for Question No.22", 0);
					cf.rd_obs22_yes.requestFocus();
				}
				else
				{
					 if(rd_BindingDoorOpeningNoted.equals("Yes"))
					 {
						 chk_Value22= cf.getselected_chk(cf.cb_obs22);
						 String chk_Value22other =(cf.cb_obs22[cf.cb_obs22.length-1].isChecked())? "^"+cf.etother_obs22.getText().toString():""; // append the other text value in to the selected option
						 chk_Value22+=chk_Value22other;
						 if(chk_Value22.equals(""))
						 {
							 
							 cf.ShowToast("Please select any checkbox for Question No.22 Observation" , 0);
						 }
						 else
						 {
							 if(cf.cb_obs22[cf.cb_obs22.length-1].isChecked())
							 {
								 if(chk_Value22other.trim().equals("^"))
								 {
									 cf.ShowToast("Please enter other text for Question No.22 Observation", 0);
									 cf.etother_obs22.requestFocus();
								 }
								 else
								 {
									 chk_Value22 +=(cf.cb_obs22[cf.cb_obs22.length-1].isChecked())? "^"+cf.etother_obs22.getText().toString():""; // append the other text value in to the selected option
									 Chk_Obs22_Comments();	
								 }
							 }
							 else
							 {
								 Chk_Obs22_Comments();
							 }
							 
						 }
					 }
					 else
					 {
						 Chk_Obs22_Comments();
					 }
				}
			}
		}
		private void Chk_Obs22_Comments() {
			// TODO Auto-generated method stub
			if(cf.etcomments_obs22.getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.22 */
			{
				cf.ShowToast("Please enter Question No.22 Comments", 0);
				cf.etcomments_obs22.requestFocus();
			}
			else
			{
				OBS3_InsertOrUpdate();
			}
		}
		private void OBS3_InsertOrUpdate() {
			// TODO Auto-generated method stub
			try
			{
			 	Cursor OBS_save=cf.SelectTablefunction(cf.Observation3, " where SH_OBS3_SRID='"+cf.encode(cf.selectedhomeid)+"'");
			 	if(OBS_save.getCount()>0)
				{
					/* UPDATE THE OBSERVATION3 TABLE*/
			 		cf.sh_db.execSQL("UPDATE "+cf.Observation3+ " SET InteriorWallCrackNoted='"+cf.encode(rd_InteriorWallCrackNoted)+"',"
			 				+"InteriorWallCrackOption='"+cf.encode(chk_Value14)+"',InteriorWallCrackComments='"+cf.encode(cf.etcomments_obs14.getText().toString())+"',"
			 				+"WallSlabCrackNoted='"+cf.encode(rd_WallSlabCrackNoted)+"',WallSlabCrackOption='"+cf.encode(chk_Value15)+"',"
			 				+"WallSlabCrackComments='"+cf.encode(cf.etcomments_obs15.getText().toString())+"',InteriorCeilingCrackNoted='"+cf.encode(rd_InteriorCeilingCrackNoted)+"',"
			 				+"InteriorCeilingCrackOption='"+cf.encode(chk_Value16)+"',InteriorCeilingCrackComments='"+cf.encode(cf.etcomments_obs16.getText().toString())+"',"
			 				+"InteriorFloorCrackNoted='"+cf.encode(rd_InteriorFloorCrackNoted)+"',InteriorFloorCrackOption='"+cf.encode(chk_Value17)+"',"
			 				+"InteriorFloorCrackComments='"+cf.encode(cf.etcomments_obs17.getText().toString())+"',WindowFrameOutofSquare='"+cf.encode(rd_WindowFrameOutofSquare)+"',"
			 				+"WindowFrameOutofSquareOption='"+cf.encode(chk_Value18)+"',WindowFrameOutofSquareComments='"+cf.encode(cf.etcomments_obs18.getText().toString())+"',"
			 				+"FloorSlopingNoted='"+cf.encode(rd_FloorSlopingNoted)+"',FloorSlopingOption='"+cf.encode(chk_Value19)+"',"
			 				+"FloorSlopingComments='"+cf.encode(cf.etcomments_obs19.getText().toString())+"',CrackedGlazingNoted='"+cf.encode(rd_CrackedGlazingNoted)+"',"
			 				+"CrackedGlazingOption='"+cf.encode(chk_Value20)+"',CrackedGlazingComments='"+cf.encode(cf.etcomments_obs20.getText().toString())+"',"
			 				+"PreviousCorrectiveMeasureNoted='"+cf.encode(rd_PreviousCorrectiveMeasureNoted)+"',PreviousCorrectiveMeasureOption='"+cf.encode(chk_Value21)+"',"
			 				+"PreviousCorrectiveMeasureComments='"+cf.encode(cf.etcomments_obs21.getText().toString())+"',BindingDoorOpeningNoted='"+cf.encode(rd_BindingDoorOpeningNoted)+"',"
			 				+"BindingDoorOpeningOption='"+cf.encode(chk_Value22)+"',BindingDoorOpeningComments='"+cf.encode(cf.etcomments_obs22.getText().toString())+"' Where SH_OBS3_SRID='"+cf.encode(cf.selectedhomeid)+"'");
					
			 		cf.ShowToast("Observation 14-22 has been updated sucessfully.", 1);
					nextlayout();
				}
				else
				{
					/* INSERT INTO OBSERVATION3 TABLE*/
					cf.sh_db.execSQL("INSERT INTO "+cf.Observation3+ "(SH_OBS3_SRID,InteriorWallCrackNoted,InteriorWallCrackOption,InteriorWallCrackComments,WallSlabCrackNoted,WallSlabCrackOption,WallSlabCrackComments,InteriorCeilingCrackNoted,InteriorCeilingCrackOption,InteriorCeilingCrackComments,InteriorFloorCrackNoted,InteriorFloorCrackOption,InteriorFloorCrackComments,WindowFrameOutofSquare,WindowFrameOutofSquareOption,WindowFrameOutofSquareComments,FloorSlopingNoted,FloorSlopingOption,FloorSlopingComments,CrackedGlazingNoted,CrackedGlazingOption,CrackedGlazingComments,PreviousCorrectiveMeasureNoted,PreviousCorrectiveMeasureOption,PreviousCorrectiveMeasureComments,BindingDoorOpeningNoted,BindingDoorOpeningOption,BindingDoorOpeningComments)" +
							"VALUES ('"+cf.encode(cf.selectedhomeid)+"','"+cf.encode(rd_InteriorWallCrackNoted)+"','"+cf.encode(chk_Value14)+"','"+cf.encode(cf.etcomments_obs14.getText().toString())+"','"+cf.encode(rd_WallSlabCrackNoted)+"','"+cf.encode(chk_Value15)+"','"+cf.encode(cf.etcomments_obs15.getText().toString())+"','"+cf.encode(rd_InteriorCeilingCrackNoted)+"','"+cf.encode(chk_Value16)+"','"+cf.encode(cf.etcomments_obs16.getText().toString())+"','"+cf.encode(rd_InteriorFloorCrackNoted)+"','"+cf.encode(chk_Value17)+"','"+cf.encode(cf.etcomments_obs17.getText().toString())+"','"+cf.encode(rd_WindowFrameOutofSquare)+"','"+cf.encode(chk_Value18)+"','"+cf.encode(cf.etcomments_obs18.getText().toString())+"','"+cf.encode(rd_FloorSlopingNoted)+"','"+cf.encode(chk_Value19)+"','"+cf.encode(cf.etcomments_obs19.getText().toString())+"','"+cf.encode(rd_CrackedGlazingNoted)+"','"+cf.encode(chk_Value20)+"','"+cf.encode(cf.etcomments_obs20.getText().toString())+"','"+cf.encode(rd_PreviousCorrectiveMeasureNoted)+"','"+cf.encode(chk_Value21)+"','"+cf.encode(cf.etcomments_obs21.getText().toString())+"','"+cf.encode(rd_BindingDoorOpeningNoted)+"','"+cf.encode(chk_Value22)+"','"+cf.encode(cf.etcomments_obs22.getText().toString())+"')");
						
					cf.ShowToast("Observation 14-22 has been saved sucessfully.", 1);
					nextlayout();
				}
			}
			catch(Exception e)
			{
				String strerrorlog="Selection of the Observation3 table not working ";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);

			}
		}
		private void nextlayout() {
			// TODO Auto-generated method stub
			Intent intimg = new Intent(Observation3.this,
					Observation4.class);
			intimg.putExtra("homeid", cf.selectedhomeid);
		    intimg.putExtra("InspectionType", cf.onlinspectionid);
			intimg.putExtra("status", cf.onlstatus);
		    startActivity(intimg);
		}
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				if(cf.strschdate.equals("")){
					cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
				}else{Intent  myintent = new Intent(Observation3.this,Observation2.class);
				cf.putExtras(myintent);
				startActivity(myintent);}
				return true;
			}
			if (keyCode == KeyEvent.KEYCODE_MENU) {

			}
			return super.onKeyDown(keyCode, event);
		}
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
				switch (resultCode) {
	 			case 0:
	 				break;
	 			case -1:
	 				try {

	 					String[] projection = { MediaStore.Images.Media.DATA };
	 					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
	 							null, null, null);
	 					int column_index_data = cursor
	 							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	 					cursor.moveToFirst();
	 					String capturedImageFilePath = cursor.getString(column_index_data);
	 					cf.showselectedimage(capturedImageFilePath);
	 				} catch (Exception e) {
	 					
	 				}
	 				
	 				break;

	 		}

	 	}

}

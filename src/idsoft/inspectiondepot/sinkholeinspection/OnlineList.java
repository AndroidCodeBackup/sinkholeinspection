package idsoft.inspectiondepot.sinkholeinspection;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import org.ksoap2.serialization.SoapObject;
import org.xmlpull.v1.XmlPullParserException;



import idsoft.inspectiondepot.sinkholeinspection.R;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class OnlineList extends Activity implements Runnable{
	CommonFunctions cf;
	String strtit;
	TextView title;
	int hand_msg=0;
	SoapObject chklogin;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        setContentView(R.layout.online);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.onlstatus = extras.getString("Status");
				cf.onlsubstatus = extras.getString("SubStatus");
				cf.onlinspectionid = extras.getString("InspectionType");
			}
			TextView tvheader = (TextView) findViewById(R.id.information);
			if (cf.onlstatus.equals("30")&&cf.onlsubstatus.equals("0")) {
				tvheader.setText("Assign");
			} else if (cf.onlstatus.equals("40")&&cf.onlsubstatus.equals("0")) {
				tvheader.setText("Schedule");
			} else if (cf.onlstatus.equals("110")&&cf.onlsubstatus.equals("111")) {
				tvheader.setText("Unable to Schedule Inspection");
			} else if (cf.onlstatus.equals("111")&&cf.onlsubstatus.equals("0")) {
				tvheader.setText("Cancelled Inspection");
			}
			else if (cf.onlstatus.equals("40")&&cf.onlsubstatus.equals("41")) {
				tvheader.setText("Completed in Online");
			}
			cf.onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
			cf.releasecode = (TextView)findViewById(R.id.releasecode);
	       // cf.releasecode.setText(cf.apkrc);
			cf.setRcvalue(cf.releasecode);
	        cf.getInspectorId();
	        cf.welcome = (TextView) this.findViewById(R.id.welcomename);
	        cf.welcome.setText(cf.Insp_firstname+" "+cf.Insp_lastname);
	         title = (TextView) this.findViewById(R.id.deleteiinformation);
			if (cf.onlinspectionid.equals("18")) {
				strtit = cf.strret;
			} else {
				strtit = cf.strcarr;
			}
			cf.search = (Button) this.findViewById(R.id.search);
			cf.search_text = (EditText)findViewById(R.id.search_text);
			cf.search_clear_txt = (Button)findViewById(R.id.search_clear_txt);
			cf.search_clear_txt.setOnClickListener(new OnClickListener() {
            	public void onClick(View arg0) {
					// TODO Auto-generated method stub
					cf.search_text.setText("");
					cf.res = "";
					dbquery();
            	}
          });
			cf.search.setOnClickListener(new OnClickListener() {
                   public void onClick(View v) {
					// TODO Auto-generated method stub

					String temp = cf.encode(cf.search_text.getText()
							.toString());
					if (temp.equals("")) {
						cf.ShowToast("Please enter the Name or Policy Number to search.", 1);
						cf.search_text.requestFocus();
					} else {
						cf.res = temp;
						dbquery();
					}

				}

			});
			if(cf.isInternetOn()==true)
			{
               String source = "<font color=#FFFFFF>Loading data. Please wait..."
						+ "</font>";
				cf.pd = ProgressDialog.show(OnlineList.this,"", Html.fromHtml(source), true);
				Thread thread = new Thread(OnlineList.this);
				thread.start();
			}
			else
			{
				cf.ShowToast("Internet connection is not available.",1);
			}
			
			
	 }
	 private void retrievedata(SoapObject chklogin) throws NetworkErrorException,
		 IOException, XmlPullParserException, SocketTimeoutException {
		 cf.dropTable(1);	
	     cf.Create_Table(6);
	 
	     int Cnt = chklogin.getPropertyCount();
     	for (int i = 0; i < Cnt; i++) {
             SoapObject obj = (SoapObject) chklogin.getProperty(i);
		     try {
		    	 cf.HomeId = (String.valueOf(obj.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SRID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SRID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SRID"));
				 cf.InspectorId = (String.valueOf(obj.getProperty("InspectorId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectorId"));
				 cf.ScheduledDate = (String.valueOf(obj.getProperty("ScheduledDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScheduledDate"));
				 cf.InspectionStartTime = (String.valueOf(obj.getProperty("InspectionStartTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionStartTime"));
				 cf.InspectionEndTime = (String.valueOf(obj.getProperty("InspectionEndTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionEndTime"));
				 cf.PH_Fname = (String.valueOf(obj.getProperty("FirstName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("NA"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("N/A"))?"":String.valueOf(obj.getProperty("FirstName"));
				 cf.PH_Lname = (String.valueOf(obj.getProperty("LastName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("LastName")).equals("NA"))?"":(String.valueOf(obj.getProperty("LastName")).equals("N/A"))?"":String.valueOf(obj.getProperty("LastName"));
				 cf.PH_Address1 = (String.valueOf(obj.getProperty("Address1")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Address1")).equals("NA"))?"":(String.valueOf(obj.getProperty("Address1")).equals("N/A"))?"":String.valueOf(obj.getProperty("Address1"));
				 cf.PH_City = (String.valueOf(obj.getProperty("City")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("City")).equals("NA"))?"":(String.valueOf(obj.getProperty("City")).equals("N/A"))?"":String.valueOf(obj.getProperty("City"));
				 cf.PH_State = (String.valueOf(obj.getProperty("State")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("State")).equals("NA"))?"":(String.valueOf(obj.getProperty("State")).equals("N/A"))?"":String.valueOf(obj.getProperty("State"));
				 cf.PH_Zip = (String.valueOf(obj.getProperty("Zip")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Zip")).equals("NA"))?"":(String.valueOf(obj.getProperty("Zip")).equals("N/A"))?"":String.valueOf(obj.getProperty("Zip"));
				 cf.PH_County = (String.valueOf(obj.getProperty("Country")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Country")).equals("NA"))?"":(String.valueOf(obj.getProperty("Country")).equals("N/A"))?"":String.valueOf(obj.getProperty("Country"));
				 cf.PH_Policyno = (String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("NA"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("N/A"))?"":String.valueOf(obj.getProperty("OwnerPolicyNo"));
				 cf.PH_InspectionTypeId = (String.valueOf(obj.getProperty("InspectionTypeId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionTypeId")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionTypeId")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionTypeId"));
				 cf.PH_Status = (String.valueOf(obj.getProperty("Status")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Status")).equals("NA"))?"":(String.valueOf(obj.getProperty("Status")).equals("N/A"))?"":String.valueOf(obj.getProperty("Status"));
				 cf.PH_SubStatus = (String.valueOf(obj.getProperty("SubStatusID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SubStatusID"));
				 
			      cf.sh_db.execSQL("INSERT INTO "
					+ cf.Onlinepolicyholder
					+ " (SH_OL_PH_SRID,SH_OL_PH_InspectorId,SH_OL_PH_Status,SH_OL_PH_SubStatus,SH_OL_PH_InspectionTypeId,SH_OL_PH_FirstName,SH_OL_PH_LastName,SH_OL_PH_Address1,SH_OL_PH_City,SH_OL_PH_State,SH_OL_PH_County,SH_OL_PH_Zip,SH_OL_PH_Policyno,SH_OL_Schedule_ScheduledDate,SH_OL_Schedule_InspectionStartTime,SH_OL_Schedule_InspectionEndTime)"
					+ " VALUES ('" + cf.encode(cf.HomeId)  + "','"+ cf.encode(cf.Insp_id)+"','"
					+ cf.encode(cf.PH_Status)+"','"
					+ cf.encode(cf.PH_SubStatus)+"','"+cf.encode(cf.PH_InspectionTypeId)+"','"
					+ cf.encode(cf.PH_Fname) + "','"
					+ cf.encode(cf.PH_Lname) + "','"
					+ cf.encode(cf.PH_Address1) + "','"
					+ cf.encode(cf.PH_City) + "','"
					+ cf.encode(cf.PH_State) + "','"
					+ cf.encode(cf.PH_County) + "','" + cf.encode(cf.PH_Zip)
					+ "','" + cf.encode(cf.PH_Policyno) + "','"
					+ cf.encode(cf.ScheduledDate) + "','" + cf.encode(cf.InspectionStartTime) + "','"
					+ cf.encode(cf.InspectionEndTime)+"')");
			
			
		} catch (Exception e) {
			 cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ OnlineList.this +" "+" in the stage of(catch) retrieving policyholder details from webservice at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				
		}
		
	}

	}
	 private void dbquery() {

			int k = 1;
			cf.data = null;
			cf.inspdata = "";
			cf.sql = "select * from " + cf.Onlinepolicyholder;
			if (!cf.res.equals("")) {

				cf.sql += " where (SH_OL_PH_FirstName like '%" + cf.encode(cf.res)
						+ "%' or SH_OL_PH_LastName like '%" + cf.encode(cf.res)
						+ "%' or SH_OL_PH_Policyno like '%" + cf.encode(cf.res)
						+ "%') and SH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
						+ "' and SH_OL_PH_InspectorId='" + cf.Insp_id + "'";
				if (cf.onlstatus.equals("110")&& cf.onlsubstatus.equals("111")) {
					cf.sql += " and  SH_OL_PH_Status=110 and SH_OL_PH_SubStatus=111 and SH_OL_PH_InspectionTypeId='" + cf.onlinspectionid + "'";
				} else if (cf.onlstatus.equals("90")&& cf.onlsubstatus.equals("0")) {
					cf.sql += " and SH_OL_PH_Status=90 and SH_OL_PH_SubStatus=0 and SH_OL_PH_InspectionTypeId='" + cf.onlinspectionid + "'";
				} else if (cf.onlstatus.equals("40")&& cf.onlsubstatus.equals("41")) {
					cf.sql += " and SH_OL_PH_Status=40 and SH_OL_PH_SubStatus='41' and SH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
							+ "'";
				}
				 else if (cf.onlstatus.equals("40")&& cf.onlsubstatus.equals("0")) {
					 cf.sql += " and SH_OL_PH_Status=40 and SH_OL_PH_SubStatus='0' and SH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
								+ "'";
					}
				 else if (cf.onlstatus.equals("30")&& cf.onlsubstatus.equals("0")) {
					 cf.sql += " and SH_OL_PH_Status=30 and SH_OL_PH_SubStatus='0' and SH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
								+ "'";
					}

			} else {
				if (cf.onlstatus.equals("110")&& cf.onlsubstatus.equals("111")) {
					cf.sql += " where SH_OL_PH_Status=110 and SH_OL_PH_SubStatus=111 and SH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
							+ "' and SH_OL_PH_InspectorId='" + cf.Insp_id + "'";
				} else if (cf.onlstatus.equals("90")&& cf.onlsubstatus.equals("0")) {
					cf.sql += " where SH_OL_PH_Status=90  and SH_OL_PH_SubStatus=0 and SH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
							+ "' and SH_OL_PH_InspectorId='" + cf.Insp_id + "'";
				}  else if (cf.onlstatus.equals("40")&& cf.onlsubstatus.equals("41")) {
					cf.sql += " where SH_OL_PH_Status=40 and SH_OL_PH_SubStatus='41' and SH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
							+ "' and SH_OL_PH_InspectorId='" + cf.Insp_id + "'";
				}
				 else if (cf.onlstatus.equals("40")&& cf.onlsubstatus.equals("0")) {
					 cf.sql += " where SH_OL_PH_Status=40 and SH_OL_PH_SubStatus='0' and SH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
								+ "' and SH_OL_PH_InspectorId='" + cf.Insp_id + "'";
					}
				 else if (cf.onlstatus.equals("30")&& cf.onlsubstatus.equals("0")) {
					 cf.sql += " where SH_OL_PH_Status=30 and SH_OL_PH_SubStatus='0' and SH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
								+ "' and SH_OL_PH_InspectorId='" + cf.Insp_id + "'";
					}

			}
			Cursor cur = cf.sh_db.rawQuery(cf.sql, null);
			cf.rws = cur.getCount();
			title.setText(strtit + "\n" + "Total Record : " + cf.rws);
			cf.data = new String[cf.rws];
			int j = 0;
			cur.moveToFirst();
			if (cur.getCount() >0) {
				do {
					cf.inspdata += " "+ cf.decode(cur.getString(cur.getColumnIndex("SH_OL_PH_FirstName"))) + " ";
					cf.inspdata += cf.decode(cur.getString(cur.getColumnIndex("SH_OL_PH_LastName"))) + " | ";
					cf.inspdata += cf.decode(cur.getString(cur.getColumnIndex("SH_OL_PH_Policyno"))) + " \n ";
					cf.inspdata += cf.decode(cur.getString(cur.getColumnIndex("SH_OL_PH_Address1"))) + " | ";
					cf.inspdata += cf.decode(cur.getString(cur.getColumnIndex("SH_OL_PH_City"))) + " | ";
					cf.inspdata += cf.decode(cur.getString(cur.getColumnIndex("SH_OL_PH_State"))) + " | ";
					cf.inspdata += cf.decode(cur.getString(cur.getColumnIndex("SH_OL_PH_County"))) + " | ";
					cf.inspdata += cf.decode(cur.getString(cur.getColumnIndex("SH_OL_PH_Zip")))+ " \n ";
					cf.inspdata += cf.decode(cur.getString(cur.getColumnIndex("SH_OL_Schedule_ScheduledDate")))+ " | ";
					cf.inspdata += cf.decode(cur.getString(cur.getColumnIndex("SH_OL_Schedule_InspectionStartTime"))) + " - ";
					cf.inspdata += cf.decode(cur.getString(cur.getColumnIndex("SH_OL_Schedule_InspectionEndTime"))) + "~";
                	j++;

					if (cf.inspdata.contains("null")) {
						cf.inspdata = cf.inspdata.replace("null", "");
					}
					if (cf.inspdata.contains(" | -")) {
						cf.inspdata = cf.inspdata.replace(" | -", "");
					}
					if (cf.inspdata.contains("N/A |")) {
						cf.inspdata = cf.inspdata.replace("N/A |", "");
					}
					if (cf.inspdata.contains("N/A - N/A")) {
						cf.inspdata = cf.inspdata.replace("N/A - N/A", "");
					}
					
				} while (cur.moveToNext());
				cf.search_text.setText("");
				display();
			} else {
				cf.ShowToast("Sorry, No results found.", 1);
				cf.onlinspectionlist.removeAllViews();
			}

		}
	 private void display() {
		 cf.onlinspectionlist.removeAllViews();
			cf.sv = new ScrollView(this);
			cf.onlinspectionlist.addView(cf.sv);

			final LinearLayout l1 = new LinearLayout(this);
			l1.setOrientation(LinearLayout.VERTICAL);
			cf.sv.addView(l1);

			if (!cf.inspdata.equals(null) && !cf.inspdata.equals("null")
					&& !cf.inspdata.equals("")) {
				this.cf.data = cf.inspdata.split("~");
				for (int i = 0; i < cf.data.length; i++) {
					cf.tvstatus = new TextView[cf.rws];
					LinearLayout l2 = new LinearLayout(this);
					l2.setOrientation(LinearLayout.HORIZONTAL);
					l1.addView(l2);

					LinearLayout lchkbox = new LinearLayout(this);
					LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
							900, ViewGroup.LayoutParams.WRAP_CONTENT);
					paramschk.topMargin = 8;
					paramschk.leftMargin = 20;
					paramschk.bottomMargin = 10;
					l2.addView(lchkbox);

					cf.tvstatus[i] = new TextView(this);
					cf.tvstatus[i].setMinimumWidth(580);
					cf.tvstatus[i].setMaxWidth(580);
					cf.tvstatus[i].setTag("textbtn" + i);
					cf.tvstatus[i].setText(cf.data[i]);
					cf.tvstatus[i].setTextColor(Color.WHITE);
					cf.tvstatus[i].setTextSize(14);
					lchkbox.addView(cf.tvstatus[i], paramschk);

					LinearLayout ldelbtn = new LinearLayout(this);
					LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
							93, 37);
					paramsdelbtn.rightMargin = 10;
					paramsdelbtn.bottomMargin = 10;
					l2.addView(ldelbtn);
			
				}
			}

		}
	 public void clicker(View v)
	 {
		  switch(v.getId())
		  {
		  case R.id.deletehome:
			  cf.gohome();
			  break;
		  }
	 }
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				startActivity(new Intent(OnlineList.this,Dashboard.class));
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			chklogin = cf.Calling_WS2(cf.onlstatus,cf.onlsubstatus,cf.onlinspectionid,cf.Insp_id,"OnlineSyncPolicyInfo");
			if (chklogin.equals("anyType{}")) {
				hand_msg=2;
				handler.sendEmptyMessage(0);
			/*	cf.ShowToast("Server is busy. Please try again later.", 2);
				startActivity(new Intent(OnlineList.this,HomeScreen.class));*/
			} else {
				
				hand_msg=1;
				handler.sendEmptyMessage(0);
			 		//retrievedata(chklogin);
					//dbquery();
			}
			
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			cf.pd.dismiss();
			if(hand_msg==1)
			{
				try {
					retrievedata(chklogin);
					//dbquery();
				} catch (SocketTimeoutException e) {
					// TODO Auto-generated catch block
					cf.ShowToast("No data found.", 2);
					e.printStackTrace();
				} catch (NetworkErrorException e) {
					// TODO Auto-generated catch block
					cf.ShowToast("No data found.", 2);
					e.printStackTrace();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					cf.ShowToast("No data found.", 2);
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					cf.ShowToast("No data found.", 2);
					e.printStackTrace();
				}
				dbquery();
			}
			else if(hand_msg==2)
			{
				cf.ShowToast("Server is busy. Please try again later.", 2);
				startActivity(new Intent(OnlineList.this,HomeScreen.class));
			}
			else if(hand_msg==3)
			{
				cf.ShowToast("Problem in Connecting server . Please try again later.", 2);
				startActivity(new Intent(OnlineList.this,HomeScreen.class));
			}
				
		}
	};
}

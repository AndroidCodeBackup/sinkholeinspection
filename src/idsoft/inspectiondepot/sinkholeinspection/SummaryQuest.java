package idsoft.inspectiondepot.sinkholeinspection;

import idsoft.inspectiondepot.sinkholeinspection.Observation4.Touch_Listener;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;


import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class SummaryQuest extends Activity {
	CommonFunctions cf;
	String strhomeid;
		@Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
	    	if (extras != null) {
	    		cf.selectedhomeid = strhomeid= extras.getString("homeid");
	    		cf.onlinspectionid = extras.getString("InspectionType");
	    	    cf.onlstatus = extras.getString("status");
	     	}
	        setContentView(R.layout.summaryquest);
	        cf.getDeviceDimensions();
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 2, 0,cf));
	        // start the declaring the objects for the controls 
           cf.getInspectorId();
	        cf.Sc_SIFC_rad1_yes=(RadioButton) findViewById(R.id.SC_SIFC_rad1_yes);
	        cf.SQ_HPHC_chk_enable=(CheckBox) findViewById(R.id.SQ_HPHC_chk_enable);
	        cf.SQ_HPHC_tbl_enable = (TableLayout) findViewById(R.id.SQ_HPHC_tbl_enable);
	        cf.SQ_HC_relative = (RelativeLayout) findViewById(R.id.relative4);
	        cf.SQ_saveNext= (Button) findViewById(R.id.SQ_saveNext);
	        cf.SQ_SC_ed1 =(EditText) findViewById(R.id.SQ_SC_ed1);
	        cf.SQ_HC_ed1 =(EditText) findViewById(R.id.SQ_HC_ed1);
	        // for showing limit in //
	        cf.SQ_TV_type1 = (TextView) findViewById(R.id.SQ_TV_type1);
	        cf.SQ_ED_type1_parrant = (LinearLayout) findViewById(R.id.SQ_ED_type1_parrant);
	        cf.SQ_ED_type1 = (LinearLayout) findViewById(R.id.SQ_ED_type1);
	        cf.SQ_TV_type2 = (TextView) findViewById(R.id.SQ_TV_type2);
	        cf.SQ_ED_type2_parrant = (LinearLayout) findViewById(R.id.SQ_ED_type2_parrant);
	        cf.SQ_ED_type2 = (LinearLayout) findViewById(R.id.SQ_ED_type2);
	        // for showing limit in ends  //
	        // Question one 
	        cf.SC_SIFC_rad1_yes = (RadioButton) findViewById(R.id.SC_SIFC_rad1_yes);
	        cf.SC_SIFC_rad1_No = (RadioButton) findViewById(R.id.SC_SIFC_rad1_No);
	        cf.SC_SIFC_rad1_N_D = (RadioButton) findViewById(R.id.SC_SIFC_rad1_N_D);
	        // Question Two	        
	        cf.SC_SIFC_rad2_yes = (RadioButton) findViewById(R.id.SC_SIFC_rad2_yes);
	        cf.SC_SIFC_rad2_N_A_P = (RadioButton) findViewById(R.id.SC_SIFC_rad2_N_A_P);
	        cf.SC_SIFC_rad2_N_D = (RadioButton) findViewById(R.id.SC_SIFC_rad2_N_D);
	        
	        /*  HPHC starts */
	        // Question one 
	        cf.SQ_HPHC_yes[0] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad1_yes);
	        cf.SQ_HPHC_No[0] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad1_No);
	        cf.SQ_HPHC_N_D[0] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad1_N_D);
	     // Question two 
	        cf.SQ_HPHC_yes[1] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad2_yes);
	        cf.SQ_HPHC_No[1] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad2_No);
	        cf.SQ_HPHC_N_D[1] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad2_N_D);
	        
	     // Question 3 
	        cf.SQ_HPHC_yes[2] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad3_yes);
	        cf.SQ_HPHC_No[2] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad3_No);
	        cf.SQ_HPHC_N_D[2] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad3_N_D);
	        
	     // Question 4
	        cf.SQ_HPHC_yes[3] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad4_yes);
	        cf.SQ_HPHC_No[3] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad4_No);
	        cf.SQ_HPHC_N_D[3] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad4_N_D);
	        
	     // Question 5
	        cf.SQ_HPHC_yes[4] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad5_yes);
	        cf.SQ_HPHC_No[4] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad5_No);
	        cf.SQ_HPHC_N_D[4] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad5_N_D);
	        
	     // Question 6 
	        cf.SQ_HPHC_yes[5] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad6_yes);
	        cf.SQ_HPHC_No[5] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad6_No);
	        cf.SQ_HPHC_N_D[5] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad6_N_D);
	        
	     // Question 7
	        cf.SQ_HPHC_yes[6] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad7_yes);
	        cf.SQ_HPHC_No[6] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad7_No);
	        cf.SQ_HPHC_N_D[6] =  (RadioButton) findViewById(R.id.SQ_HPHC_rad7_N_D);
	        
	        
	        cf.SQ_HPHC_chk_enable.setOnClickListener(new SQ_clicker());
	        cf.Sc_SIFC_rad1_yes.requestFocus();
	        cf.SQ_saveNext.setOnClickListener(new SQ_clicker());
	        
	        cf.SQ_SC_ed1.setOnTouchListener(new Touch_Listener(1));
	        cf.SQ_HC_ed1.setOnTouchListener(new Touch_Listener(2));
	        
	        cf.SQ_SC_ed1.addTextChangedListener(new SQ_textwatcher(1));
	        cf.SQ_HC_ed1.addTextChangedListener(new SQ_textwatcher(2));
	        /* create table if not exiti querry will excute */
	        cf.Create_Table(4);
	        /* create table if not exiti querry will excute ends  */
	        
	        /* set the value if availabel mean in the respective fields */
	        SQ_setValue();
	        /* set the value if availabel mean in the respective fields */
	 }
		 class Touch_Listener implements OnTouchListener
			{
				   public int type;
				   Touch_Listener(int type)
					{
						this.type=type;
						
					}
				    @Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
				    	if(this.type==1)
						{
				    		cf.setFocus(cf.SQ_SC_ed1);
						}
				    	else if(this.type==2)
						{
					    		cf.setFocus(cf.SQ_HC_ed1);
						}
				    	
						return false;
					}
				 
			}
	private void SQ_setValue() {
			// TODO Auto-generated method stub
		Cursor SQ_Retrive=null;
		try
		{
			SQ_Retrive=cf.SelectTablefunction(cf.SQ_table, " where SH_SQ_Homeid='"+cf.encode(cf.selectedhomeid)+"'");
			if(SQ_Retrive.getCount()>0)
			{
				  SQ_Retrive.moveToFirst();
				  String Sq_q1=cf.decode(SQ_Retrive.getString(SQ_Retrive.getColumnIndex("SH_SQ_SIFC_Q1")));
				  setvalueToRadio(cf.Sc_SIFC_rad1_yes,cf.SC_SIFC_rad1_No,cf.SC_SIFC_rad1_N_D,Sq_q1); // set q1 value 
				  String Sq_q2=cf.decode(SQ_Retrive.getString(SQ_Retrive.getColumnIndex("SH_SQ_SIFC_Q2")));;
				  setvalueToRadio(cf.SC_SIFC_rad2_yes,cf.SC_SIFC_rad2_N_A_P,cf.SC_SIFC_rad2_N_D,Sq_q2); // set q2 value 
				  
				  String Sq_HPHC_enable=cf.decode(SQ_Retrive.getString(SQ_Retrive.getColumnIndex("SH_SQ_HPHC_present")));
				  if(Sq_HPHC_enable.equals("1"))
				  {
					  cf.SQ_HPHC_chk_enable.setChecked(true);
					  cf.SQ_HPHC_tbl_enable.setVisibility(View.GONE);
					  cf.SQ_HC_relative.setVisibility(View.GONE);
				  }
				 
				  
				  cf.SQ_SC_ed1.setText(cf.decode(SQ_Retrive.getString(SQ_Retrive.getColumnIndex("SH_SQ_SC_Comments"))));
				  cf.showing_limit(cf.decode(SQ_Retrive.getString(SQ_Retrive.getColumnIndex("SH_SQ_SC_Comments"))),cf.SQ_ED_type1_parrant,cf.SQ_ED_type1,cf.SQ_TV_type1);
				  String Sq_HPHC_q1,Sq_HPHC_q2,Sq_HPHC_q3,Sq_HPHC_q4,Sq_HPHC_q5,Sq_HPHC_q6,Sq_HPHC_q7,SQ_sc_ed1;
				  
				  if(Sq_HPHC_enable.equals("0"))
				  {
					  Sq_HPHC_q1=cf.decode(SQ_Retrive.getString(SQ_Retrive.getColumnIndex("SH_SQ_HPHC_Q1")));
					  setvalueToRadio(cf.SQ_HPHC_yes[0],cf.SQ_HPHC_No[0],cf.SQ_HPHC_N_D[0],Sq_HPHC_q1); // set q1 value
					  
					  Sq_HPHC_q2=cf.decode(SQ_Retrive.getString(SQ_Retrive.getColumnIndex("SH_SQ_HPHC_Q2")));
					  setvalueToRadio(cf.SQ_HPHC_yes[1],cf.SQ_HPHC_No[1],cf.SQ_HPHC_N_D[1],Sq_HPHC_q2); // set q2 value
					  
					  Sq_HPHC_q3=cf.decode(SQ_Retrive.getString(SQ_Retrive.getColumnIndex("SH_SQ_HPHC_Q3")));
					  setvalueToRadio(cf.SQ_HPHC_yes[2],cf.SQ_HPHC_No[2],cf.SQ_HPHC_N_D[2],Sq_HPHC_q3); // set q3 value
					  
					  Sq_HPHC_q4=cf.decode(SQ_Retrive.getString(SQ_Retrive.getColumnIndex("SH_SQ_HPHC_Q4")));
					  setvalueToRadio(cf.SQ_HPHC_yes[3],cf.SQ_HPHC_No[3],cf.SQ_HPHC_N_D[3],Sq_HPHC_q4); // set q4 value
					  
					  
					  Sq_HPHC_q5=cf.decode(SQ_Retrive.getString(SQ_Retrive.getColumnIndex("SH_SQ_HPHC_Q5")));
					  setvalueToRadio(cf.SQ_HPHC_yes[4],cf.SQ_HPHC_No[4],cf.SQ_HPHC_N_D[4],Sq_HPHC_q5); // set q5 value
					  
					  
					  Sq_HPHC_q6=cf.decode(SQ_Retrive.getString(SQ_Retrive.getColumnIndex("SH_SQ_HPHC_Q6")));
					  setvalueToRadio(cf.SQ_HPHC_yes[5],cf.SQ_HPHC_No[5],cf.SQ_HPHC_N_D[5],Sq_HPHC_q6); // set q6 value
					  
					  Sq_HPHC_q7=cf.decode(SQ_Retrive.getString(SQ_Retrive.getColumnIndex("SH_SQ_HPHC_Q7")));
					  setvalueToRadio(cf.SQ_HPHC_yes[6],cf.SQ_HPHC_No[6],cf.SQ_HPHC_N_D[6],Sq_HPHC_q7); // set q7 value
					     
					   SQ_sc_ed1=cf.decode(SQ_Retrive.getString(SQ_Retrive.getColumnIndex("SH_SQ_HC_Comments")));
					   cf.SQ_HC_ed1.setText(SQ_sc_ed1);
					   cf.showing_limit(SQ_sc_ed1,cf.SQ_ED_type2_parrant,cf.SQ_ED_type2,cf.SQ_TV_type2);
				  }
				  else
				  {
					  Sq_HPHC_q1="";
					  Sq_HPHC_q2="";
					  Sq_HPHC_q3="";
					  Sq_HPHC_q4="";
					  Sq_HPHC_q5="";
					  Sq_HPHC_q6="";
					  Sq_HPHC_q7="";
					  SQ_sc_ed1="";
					  
				  }
				  /* get the sleected radios value end */
			}
		}
		catch (Exception E)
		{
			String strerrorlog="Selection of the Summery question  table not working ";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
		
			
			
		}
	private void setvalueToRadio(RadioButton R1,RadioButton R2,RadioButton R3,String s) {
		// TODO Auto-generated method stub
		if(R1.getText().equals(s))
		{
			R1.setChecked(true);
		}
		else if(R2.getText().equals(s))
		{
			R2.setChecked(true);
		}
		else if(R3.getText().equals(s))
		{
			R3.setChecked(true);
		}
		
	}
	class SQ_clicker implements OnClickListener
	{
		
		public void onClick(View v) {
			// TODO Auto-generated method stub

			switch (v.getId()) {
			case R.id.SQ_HPHC_chk_enable:
				if (((CheckBox) v).isChecked()) {
					cf.SQ_HPHC_tbl_enable.setVisibility(v.GONE);
					cf.SQ_HC_relative.setVisibility(v.GONE);
				} else {
					cf.SQ_HPHC_tbl_enable.setVisibility(v.VISIBLE);
					cf.SQ_HC_relative.setVisibility(v.VISIBLE);
				}
				break;
			case R.id.SQ_saveNext:
				SQ_Save_Validation();
				
			break;
				
			
			}
	}

		private void SQ_Save_Validation() {
			// TODO Auto-generated method stub
			String que1= getSelectedRdi(cf.SC_SIFC_rad1_yes,cf.SC_SIFC_rad1_No,cf.SC_SIFC_rad1_N_D); // For validation
			System.out.println("question 1 va;ue"+que1);
			if(!cf.SQ_SC_ed1.getText().toString().trim().equals(""))
			{
				if(!que1.trim().equals(""))
				{
				 if(!cf.SQ_HPHC_chk_enable.isChecked())
				   {
					 if(!cf.SQ_HC_ed1.getText().toString().trim().equals(""))
					 {
					   // chek the if any one of the value has chekc ed or not or HPHC
						   if(     (cf.SQ_HPHC_yes[0].isChecked() || cf.SQ_HPHC_No[0].isChecked() || cf.SQ_HPHC_N_D[0].isChecked()) && 
								   (cf.SQ_HPHC_yes[1].isChecked() || cf.SQ_HPHC_No[1].isChecked() || cf.SQ_HPHC_N_D[1].isChecked()) &&
								   (cf.SQ_HPHC_yes[2].isChecked() || cf.SQ_HPHC_No[2].isChecked() || cf.SQ_HPHC_N_D[2].isChecked()) &&
								   (cf.SQ_HPHC_yes[3].isChecked() || cf.SQ_HPHC_No[3].isChecked() || cf.SQ_HPHC_N_D[3].isChecked()) &&
								   (cf.SQ_HPHC_yes[4].isChecked() || cf.SQ_HPHC_No[4].isChecked() || cf.SQ_HPHC_N_D[4].isChecked()) &&
								   (cf.SQ_HPHC_yes[5].isChecked() || cf.SQ_HPHC_No[5].isChecked() || cf.SQ_HPHC_N_D[5].isChecked()) &&
								   (cf.SQ_HPHC_yes[6].isChecked() || cf.SQ_HPHC_No[6].isChecked() || cf.SQ_HPHC_N_D[6].isChecked()))
						   {
							   SQ_InsertOrUpdate();  
						   }
						   else
						   {
							   cf.ShowToast("Please select all fields in the Homeowner/Policy Holder Disclosure ", 0);
						   }
					 	}
					   else
					   {
						   cf.ShowToast("Please enter the homeowner comments ", 0); 
					   }
					   //getSelectedRdi(); 
				   }
				 else
				 {
					 SQ_InsertOrUpdate();
				 }
				 
				}
				else
				{
					cf.ShowToast("Please select option  for \"Is the subject properly located in a known sinkhole county?\" in Summary of Inspectors Findings/Conditions", 0);
					// Micle request to add the validation for this 9in the date of mail 25-6-2012
				}
			}
			else
			{
				cf.ShowToast("Please enter the summary comments ", 0);
			}
		}

		private void SQ_InsertOrUpdate() {
			// TODO Auto-generated method stub
			
		  /* get the sleected radios value */
		  String Sq_q1=getSelectedRdi(cf.SC_SIFC_rad1_yes,cf.SC_SIFC_rad1_No,cf.SC_SIFC_rad1_N_D);
		  
		  String Sq_q2=getSelectedRdi(cf.SC_SIFC_rad2_yes,cf.SC_SIFC_rad2_N_A_P,cf.SC_SIFC_rad2_N_D);
		  
		  String Sq_HPHC_enable=(cf.SQ_HPHC_chk_enable.isChecked()== true)? "1":"0";
		  String Sq_HPHC_q1,Sq_HPHC_q2,Sq_HPHC_q3,Sq_HPHC_q4,Sq_HPHC_q5,Sq_HPHC_q6,Sq_HPHC_q7,SQ_sc_ed1;
		  
		  if(Sq_HPHC_enable.equals("0"))
		  {
			  Sq_HPHC_q1=getSelectedRdi(cf.SQ_HPHC_yes[0],cf.SQ_HPHC_No[0],cf.SQ_HPHC_N_D[0]);
			  Sq_HPHC_q2=getSelectedRdi(cf.SQ_HPHC_yes[1],cf.SQ_HPHC_No[1],cf.SQ_HPHC_N_D[1]);
			  Sq_HPHC_q3=getSelectedRdi(cf.SQ_HPHC_yes[2],cf.SQ_HPHC_No[2],cf.SQ_HPHC_N_D[2]);
			  Sq_HPHC_q4=getSelectedRdi(cf.SQ_HPHC_yes[3],cf.SQ_HPHC_No[3],cf.SQ_HPHC_N_D[3]);
			  Sq_HPHC_q5=getSelectedRdi(cf.SQ_HPHC_yes[4],cf.SQ_HPHC_No[4],cf.SQ_HPHC_N_D[4]);
			  Sq_HPHC_q6=getSelectedRdi(cf.SQ_HPHC_yes[5],cf.SQ_HPHC_No[5],cf.SQ_HPHC_N_D[5]);
			  Sq_HPHC_q7=getSelectedRdi(cf.SQ_HPHC_yes[6],cf.SQ_HPHC_No[6],cf.SQ_HPHC_N_D[6]);
			     
			   SQ_sc_ed1=cf.SQ_HC_ed1.getText().toString();
		  }
		  else
		  {
			  Sq_HPHC_q1="";
			  Sq_HPHC_q2="";
			  Sq_HPHC_q3="";
			  Sq_HPHC_q4="";
			  Sq_HPHC_q5="";
			  Sq_HPHC_q6="";
			  Sq_HPHC_q7="";
			  SQ_sc_ed1="";
		  }
		  /* get the sleected radios value end */
		  Cursor SQ_save=null;
			try
			{
				 SQ_save=cf.SelectTablefunction(cf.SQ_table, " where SH_SQ_Homeid='"+cf.encode(cf.selectedhomeid)+"'");
				 if(SQ_save.getCount()>0)
					{
						try
						{
							cf.sh_db.execSQL("UPDATE "+cf.SQ_table+ " SET SH_SQ_Inspectorid='"+cf.encode(cf.Insp_id)+"',SH_SQ_Homeid='"+cf.encode(cf.selectedhomeid)+"',SH_SQ_SIFC_Q1='"+cf.encode(Sq_q1)+"',SH_SQ_SIFC_Q2='"+cf.encode(Sq_q2)+"',SH_SQ_SC_Comments='"+cf.encode(cf.SQ_SC_ed1.getText().toString())+"',SH_SQ_HPHC_present='"+cf.encode(Sq_HPHC_enable)+"',SH_SQ_HPHC_Q1='"+cf.encode(Sq_HPHC_q1)+"',SH_SQ_HPHC_Q2='"+cf.encode(Sq_HPHC_q2)+"',SH_SQ_HPHC_Q3='"+cf.encode(Sq_HPHC_q3)+"',SH_SQ_HPHC_Q4='"+cf.encode(Sq_HPHC_q4)+"',SH_SQ_HPHC_Q5='"+cf.encode(Sq_HPHC_q5)+"',SH_SQ_HPHC_Q6='"+cf.encode(Sq_HPHC_q6)+"',SH_SQ_HPHC_Q7='"+cf.encode(Sq_HPHC_q7)+"',SH_SQ_HC_Comments='"+cf.encode(SQ_sc_ed1)+"' Where SH_SQ_Homeid='"+cf.encode(cf.selectedhomeid)+"' ");
							cf.ShowToast("Summary Question has been saved sucessfully.", 1);
							// move the page to next page // 
							nextlayout();
							//startActivity(new Intent(getApplicationContext(),BuildingInformation.class));
							// move the page to next page ends
							
						}
						catch (Exception E)
						{
							String strerrorlog="Upadte of the Summery question  table not working ";
							cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table Creation at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
						}
						
					}
					else
					{
						try
						{
								cf.sh_db.execSQL("INSERT INTO "+cf.SQ_table+ "(SH_SQ_Inspectorid,SH_SQ_Homeid,SH_SQ_SIFC_Q1,SH_SQ_SIFC_Q2,SH_SQ_SC_Comments,SH_SQ_HPHC_present,SH_SQ_HPHC_Q1,SH_SQ_HPHC_Q2,SH_SQ_HPHC_Q3,SH_SQ_HPHC_Q4,SH_SQ_HPHC_Q5,SH_SQ_HPHC_Q6,SH_SQ_HPHC_Q7,SH_SQ_HC_Comments)" +
								"VALUES ('"+cf.encode(cf.Insp_id)+"','"+cf.encode(cf.selectedhomeid)+"','"+cf.encode(Sq_q1)+"','"+cf.encode(Sq_q2)+"','"+cf.encode(cf.SQ_SC_ed1.getText().toString())+"','"+cf.encode(Sq_HPHC_enable)+"','"+cf.encode(Sq_HPHC_q1)+"','"+cf.encode(Sq_HPHC_q2)+"','"+cf.encode(Sq_HPHC_q3)+"','"+cf.encode(Sq_HPHC_q4)+"','"+cf.encode(Sq_HPHC_q5)+"','"+cf.encode(Sq_HPHC_q6)+"','"+cf.encode(Sq_HPHC_q7)+"','"+cf.encode(SQ_sc_ed1)+"')");
								 cf.ShowToast("Summary Question has been saved sucessfully.", 1);
								// move the page to next page // 
								 nextlayout();
								//	startActivity(new Intent(getApplicationContext(),BuildingInformation.class));
								// move the page to next page ends
						}
						catch (Exception E)
						{
							String strerrorlog="Insert the Summery question  table not working ";
							cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table Creation at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
						}
					}
			}
			catch (Exception E)
			{
				String strerrorlog="Selection of the Summery question  table not working ";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
			
		}
		public final String getSelectedRdi(RadioButton R1,RadioButton R2,RadioButton R3)
		{
			if(R1.isChecked())
			{
				return R1.getText().toString();
			}
			else if(R2.isChecked())
			{
				return R2.getText().toString();
			}
			else if(R3.isChecked())
			{
				return R3.getText().toString();
			}
			
			return "";

			
		}



}
	// setting the limit for the edit texts  
	class SQ_textwatcher implements TextWatcher
	{
         public int type;
		SQ_textwatcher(int type)
		{
			this.type=type;
		}
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if(this.type==1)
			{
				cf.showing_limit(s.toString(),cf.SQ_ED_type1_parrant,cf.SQ_ED_type1,cf.SQ_TV_type1);
			}
			else
			{
				cf.showing_limit(s.toString(),cf.SQ_ED_type2_parrant,cf.SQ_ED_type2,cf.SQ_TV_type2);
			}
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}
	public void nextlayout() {
		// TODO Auto-generated method stub
		Intent intimg = new Intent(SummaryQuest.this,
				BuildingInformation.class);
		intimg.putExtra("homeid", cf.selectedhomeid);
	    intimg.putExtra("InspectionType", cf.onlinspectionid);
		intimg.putExtra("status", cf.onlstatus);
	    startActivity(intimg);
	}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				if(cf.strschdate.equals("")){
					cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
				}else{Intent  myintent = new Intent(SummaryQuest.this,SinkholeSurvey.class);
				cf.putExtras(myintent);
				startActivity(myintent);}
				return true;
			}
			if (keyCode == KeyEvent.KEYCODE_MENU) {

			}
			return super.onKeyDown(keyCode, event);
		}
	 public void clicker(View v)
	 {
	 	switch(v.getId())
	 	{
	 	case R.id.home:
	 	cf.gohome();	
	 	break;
	 	}
	 }
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
				switch (resultCode) {
	 			case 0:
	 				break;
	 			case -1:
	 				try {

	 					String[] projection = { MediaStore.Images.Media.DATA };
	 					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
	 							null, null, null);
	 					int column_index_data = cursor
	 							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	 					cursor.moveToFirst();
	 					String capturedImageFilePath = cursor.getString(column_index_data);
	 					cf.showselectedimage(capturedImageFilePath);
	 				} catch (Exception e) {
	 					
	 				}
	 				
	 				break;

	 		}

	 	}

}

package idsoft.inspectiondepot.sinkholeinspection;

import idsoft.inspectiondepot.sinkholeinspection.Observation1.Obs_clicker;

import java.io.DataOutputStream;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.google.android.maps.OverlayItem;
import idsoft.inspectiondepot.sinkholeinspection.R;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.view.ViewGroup;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.sax.StartElementListener;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class CommonFunctions {
	    public String NAMESPACE = "http://tempuri.org/";
	    public String URL = "http://72.15.221.153:91/AndroidWebService.asmx"; // LIVE
	    public String URL_IDMA="http://72.15.221.153:91/AndroidWebService.asmx";//Live IDMA*/
	 /*public String URL="http://72.15.221.151:104/AndroidWebService.asmx";//UAT
	    public String URL_IDMA="http://72.15.221.151:89/AndroidWebService.asmx";//Uat IDMA*/
	  
	    public String MY_DATABASE_NAME = "SinkholeDatabases";
		/*TABLE DECLARATION*/
	    public static final String inspectorlogin = "tbl_inspectorlogin";
	    public static final String policyholder = "SH_Policyholder";
	    public static final String Onlinepolicyholder = "SH_Online_Policyholder";
	    public static final String SQ_table = "SH_Summary_Question";
	    public static final String BIQ_table = "SH_BuildingInformation_LBC";
	    public static final String Version = "SH_Version";
	    public static final String IDMAVersion = "ID_Version";
	    public static final String MailingPolicyHolder = "SH_Mailing_Policyholder";
	    public static final String Observation4 = "SH_Observation4";
	    public static final String Observation3 = "SH_Observation3";
	    public static final String Observation2 = "SH_Observation2";
	    public static final String Observation1 = "SH_Observation1";
	    public static final String Observation1T = "SH_Observation1Tree";
	    public static final String ImageTable="SH_Photos";
	    public static final String FeedBackInfoTable="SH_FeedBackInfoTable";
	    public static final String FeedBackDocumentTable="SH_FeedBackDocumentTable";
	    public static final String Photo_caption="SH_PhotoCaption";
	    public static final String Agent_tabble="SH_Agent_information";
	    public static final String Cloud_table="SH_Cloud_information";
	    public static final String Additional_table="SH_Additional_information";
public int sp=0,elev;
public EditText etcaption;
	    private static final int MODE_WORLD_READABLE = 0;
		SQLiteDatabase sh_db;
	    public String strerrorlog,Inspectiontypeid="",picpath="",name="";
        public Uri mCapturedImageURI;
        public static final int CAPTURE_PICTURE_INTENT = 0;
		private static final int SELECT_PICTURE = 0;
	    public android.text.format.DateFormat df;
		public CharSequence datewithtime;
		public Button btn_helpclose,btn_camcaptclos;
	    public String apkrc="RC U 05-13-2013",Chk_Inspector="false",newspin;
	    public String colorname,deviceId,model,manuf,devversion,apiLevel,strschdate="",txt="";
	    public int ipAddress,wd,spinelevval,quesid=0;
		String exprtcnt;  
	    public Dialog dialog1;
	    public ArrayAdapter adapter2;
	    public Intent myintent;
	    public SoapSerializationEnvelope envelope;
	    public SoapObject onlresult;
	    public RelativeLayout spinvw,capvw;
	    public String[] elevnames={"Select Elevation","External photos","Roof and attic photos","Ground and adjoining images","Internal photos","Additional photos"};
	    public Spinner elevspin,captionspin;
	    public TextView cameratxt,tvcamhelp;
		Context con;
		public Toast toast;
		public String strcarr= " Sinkhole Carr.Order",inspectiondate;
		public String strret= " Sinkhole ";
		public Class tempclass;
		public boolean chkbool;
		public String selectedhomeid="";
		public Class classnames[]={SinkholeInspectionActivity.class,HomeScreen.class,Import.class,Dashboard.class,HomeOwnerList.class,CompletedInspectionList.class};
		/*DECLARATION OF VARIABLES FOR LOGIN PAGE*/
		public EditText et_password;
		public AutoCompleteTextView et_username;
		public ProgressDialog pd;
		public String Insp_id="", Insp_firstname, Insp_middlename, Insp_lastname, Insp_address, Insp_companyname, Insp_companyId, Insp_username, Insp_password, Insp_flag1, status,
		              Insp_Photo,Insp_PhotoExtn,Insp_Status,Insp_email;
		
		/*DECLARATION OF VARIABLES FOR LOGIN ENDS HERE*/
		
		/*DECLARATION OF VARIABLES FOR DASHBOARD STARTS HERE*/
		 public TextView releasecode,welcome,txtversion;
		 public ImageView img_InspectorPhoto;
		 public String versionname,newcode,newversion;
		/*DECLARATION OF VARIABLES FOR DASHBOARD ENDS HERE*/
		
		 /*DECLARATION OF VARAIABLES IN IMPORT STARTS HERE*/
		 public int typeBar=1,mState,usercheck;
		 public static int RUNNING = 1;
		 public int total; // Determines type progress bar: 0 = spinner, 1 = horizontal
		 public int delay = 40; // Milliseconds of delay in the update loop
		 public int maxBarValue = 0; // Maximum value of horizontal progress bar
		 public String HomeId,InspectorId,ScheduledDate,ScheduledCreatedDate,AssignedDate,Schedulecomments,InspectionStartTime,InspectionEndTime,
	                   PH_Fname,PH_Lname,PH_Address1,PH_Address2,PH_City,PH_State,PH_Zip,PH_County,PH_Inspectionfees,
	                   PH_InsuranceCompany,PH_HPhone,PH_WPhone,PH_CPhone,PH_Email,PH_Policyno,PH_Status,PH_SubStatus,
	                   PH_InspectionTypeId,PH_IsInspected="0",PH_IsUploaded="0",PH_EmailChk="0";
		 /*DECLARATION OF VARAIABLES IN IMPORT ENDS HERE*/
		 /*DECLARATION OF VARIABLES FOR DASHBOARD STARTS HERE*/
		 public int carrschedule,carrassign,carrcio,carruts,carrcan,carrcit,retschedule,retassign,retcio,retuts,retcan,retcit,carrtotal,rettotal=0;
		 public Button btn_carrassign,btn_carrschedule,btn_carrcit,btn_carrcio,btn_carruts,btn_carrcan,btn_carrtotal,
		               btn_retassign,btn_retschedule,btn_retcit,btn_retcio,btn_retuts,btn_retcan,btn_rettotal,
		               btn_onlcarrassign,btn_onlcarrschedule,btn_onlcarrcio,btn_onlcarruts,btn_onlcarrcan,btn_onlcarrtotal,
		               btn_onlretassign,btn_onlretschedule,btn_onlretcio,btn_onlretuts,btn_onlretcan,btn_onlrettotal;
		 public int onlcarrassign,onlcarrschedule,onlcarrcio,onlcarruts,onlcarrcan,onlcarrtotal,
		               onlretassign,onlretschedule,onlretcio,onlretuts,onlretcan,onlrettotal;
		 /*DECLARATION OF VARIABLES FOR DASHBOARD ENDS HERE*/
		 /*DECLARATION OF VARIABLES FOR ONLINE LIST STARTS HERE*/
		 public String onlstatus,onlsubstatus,onlinspectionid,res="",sql,inspdata;
		 public LinearLayout onlinspectionlist;
		 public Button search,search_clear_txt;
		 public EditText search_text;
		 public int rws,count,ht;
		 public TextView tvstatus[];
		 public ScrollView sv;
		 public Button deletebtn[];
		 public String[] data,countarr;
		 /*DECLARATION OF VARIABLES FOR ONLINE LIST ENDS HERE*/
		 /*DECLARATION OF VARIABLES FOR HOMEOWNER LIST STARTS HERE*/
		 public int columnvalue;
		 public String statusofdata;
	/** Declarations for Policyholder starts **/
		public EditText et_firstname,et_lastname,et_address1,et_zip,et_address2,et_hmephn,
		       et_city,et_wrkphn,et_state,et_cellphn,et_county,et_email,et_policyno,
		       et_mailaddress1,et_mailstate,et_mailaddress2,et_mailcounty,et_mailcity,et_mailzip,
		       et_inspdate,et_commenttxt;
		public Button get_inspectiondate;
		public Spinner Sch_Spinnerstart,Sch_Spinnerend;
		public TextView txt_fees,et_companyname;
		public int mYear,mMonth,mDay;public String newphone,Sh_assignedDate;
   /** Declarations for Policyholder ends **/
		/* DECLARATION OF VARIABLES FOR SCHEDULE STARTS HERE **/
		public CheckBox schchk;
		/* DECLARATION OF VARIABLES FOR SCHEDULE ENDS HERE */
		/* summary variable declaration starts */
		public CheckBox SQ_HPHC_chk_enable;
		public TableLayout SQ_HPHC_tbl_enable;
		public RadioButton Sc_SIFC_rad1_yes;
		public Button SQ_saveNext;
		public RelativeLayout SQ_HC_relative;
		public RadioButton SC_SIFC_rad1_yes,SC_SIFC_rad1_No,SC_SIFC_rad1_N_D,SC_SIFC_rad2_yes,SC_SIFC_rad2_N_A_P,SC_SIFC_rad2_N_D;
		public EditText SQ_SC_ed1,SQ_HC_ed1;
		public RadioButton[] SQ_HPHC_yes = new RadioButton[7],SQ_HPHC_No= new RadioButton[7],SQ_HPHC_N_D = new RadioButton[7];
		public LinearLayout SQ_ED_type1_parrant,SQ_ED_type1,SQ_ED_type2_parrant,SQ_ED_type2;
		public TextView SQ_TV_type1,SQ_TV_type2;

		/* summary variable declaration Ends */
		
		/* building information variable declaration starts */
		  public TableLayout BI_LBC_table1,BI_GBI_table1;
		  public RelativeLayout BI_WSC_rel1;
		  public LinearLayout BI_LBC_lin,BI_GBI_lin,BI_WSC_lin,BI_AC_lin,BI_AC_lin2;
		  
		  public RadioButton BI_LBC_txt1_opt[]=new RadioButton[8],BI_LBC_txt3_opt[]=new RadioButton[3],BI_GBI_txt2_opt[]=new RadioButton[4];
		  
		  public CheckBox BI_LBC_txt2_opt[]=new CheckBox[9],BI_GBI_txt1_opt[]=new CheckBox[7],BI_GBI_txt3_opt[]=new CheckBox[9],
	      BI_WSC_txt1_opt[]=new CheckBox[5],BI_WSC_txt2_opt[]=new CheckBox[5],BI_WSC_txt3_opt[]=new CheckBox[5],BI_WSC_txt4_opt[]=new CheckBox[5],
		  BI_WC_txt1_opt[]=new CheckBox[5],BI_WC_txt2_opt[]=new CheckBox[5],BI_WC_txt3_opt[]=new CheckBox[5],BI_WC_txt4_opt[]=new CheckBox[5],
		  BI_WC_txt5_opt[]=new CheckBox[5],BI_WC_txt6_opt[]=new CheckBox[5],BI_WC_txt7_opt[]=new CheckBox[5];
		  
		  public EditText BI_LBC_txt2_other,BI_LBC_txt3_other,BI_GBI_txt1_other,BI_GBI_txt2_other,BI_GBI_txt3_other,BI_GBI_spin1_other;
		  
		  public Spinner BI_GBI_txt2_sp1,WSC_sp1,WSI_N_S;
		  
		  public String[] BI_GBI_year={"Select","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","Other"},
		  BI_WSC_storey ={"Select","1 Storey","2 Storey","3 Storey","4 Storey","5 Storey"},BI_WSC_bedroom={"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","Guest bedroom","Master bedroom","Jack and Jill bedroom","Media Room","Pantry"};
		  public Button BI_savenext; 
		   public EditText BI_AC_ed1,BI_LBC_txt3_PAI,BI_GBI_BS,BI_WSC_TNEWO,BI_WSC_TNEDO;
		   public CheckBox[] BI_GBI_txt4_opt= new CheckBox[5];
		   public LinearLayout BI_ED_parrant,BI_ED;
		   public TextView BI_ED_TV;

		/* building information variable declaration Ends */
		  
	/** Declaration of observation starts here **/
		public RadioButton obs1optn1y,obs1optn1n,obs1optn1nd,obs1optnay,obs1optnan,obs1optnand,obs2optny,
		obs2optnn,obs2optnnd,obs3optny,obs4optny,obs5optny,obs6opt1ny,obs6optany,obs7optny,obs3optnn,obs4optnn,
		obs5optnn,obs6opt1nn,obs7optnn,obs3optnnd,obs4optnnd,obs5optnnd,obs6opt1nnd,obs7optnnd,obs6optann,
		obs6optannd,obs6optbny,obs6optbnn,obs6optbnnd,obs6optcny,obs6optcnn,obs6optcnnd,obs7opt1ny,obs7opt1nn,
		obs7opt1nnd,obs7optnay,obs7optnan,obs7optnand;
		public RelativeLayout show_obs1_optn1y,show_obs1_optnay,show_obs2_optny,show_obs3_optny,
		show_obs4_optny,show_obs5_optny,show_obs6_opt1ny,show_obs7_optny,show_obs6_optany,
		show_obs6_optbny,show_obs6_optcny,show_obs7_opt1ny,show_obs7_optnay;
		public CheckBox cb_obs1[]=new CheckBox[5];
		public CheckBox cb_obs1a[]=new CheckBox[5];
		public CheckBox cb_obs2[]=new CheckBox[5];
		public CheckBox cb_obs3[]=new CheckBox[5];
		public CheckBox cb_obs4[]=new CheckBox[8];
		public CheckBox cb_obs5[]=new CheckBox[5];
		public CheckBox cb_obs6[]=new CheckBox[5];
		public CheckBox cb_obs6A[]=new CheckBox[5];
		public CheckBox cb_obs6B[]=new CheckBox[5];
		public CheckBox cb_obs6C[]=new CheckBox[5];
		public CheckBox cb_obs71[]=new CheckBox[5];
		public CheckBox cb_obs72[]=new CheckBox[5];
		public CheckBox cb_obs73[]=new CheckBox[5];
		public CheckBox cb_obs74[]=new CheckBox[6];
		public CheckBox cb_obs75[]=new CheckBox[7];
		public CheckBox cb_obs7A[]=new CheckBox[4];
		public EditText etother_obs1_optn1,etcomments_obs1_opt1,etother_obs1_optna,etcomments_obs1_opta,etother_obs2,
	    etcomments_obs2,etother_obs3,etother_obs4,etother_obs5,etcomments_obs6_opt1,etother_obs7,etcomments_obs3,
	    etcomments_obs4,etcomments_obs5,etother_obs6_opt1,etcomments_obs7,etother_obs6_opta,etcomments_obs6_opta,
	    etother_obs6_optb,etother_obs6_optc,etcomments_obs6_optb,etcomments_obs6_optc,etother_obs7_opt1txty1,etcomments_obs7_opt1,
	    etother_obs7_opt1txty2,etother_obs7_opt1txty3,etother_obs7_txty4other,etcomments_obs7_opta,etlen_obs7;
		public String alerttitle,alertcontent;
		public TableRow tbl_row_obs1,tbl_row_obs2,tbl_row_obs3,tbl_row_obs4,tbl_row_obs5,tbl_row_obs6,tbl_row_obs7;
		public TableLayout tbl_obs1,tbl_obs2,tbl_obs3,tbl_obs4,tbl_obs5,tbl_obs6,tbl_obs7;
		public TextView obs1_tbl_row_txt,obs2_tbl_row_txt,obs3_tbl_row_txt,obs4_tbl_row_txt,obs5_tbl_row_txt,obs6_tbl_row_txt,obs7_tbl_row_txt;
		/** declaration of abservation1 ends here **/
		
		/** DECLARATION OF OBSERVATION2 STARTS HERE **/
		public TableRow tbl_row_obs8,tbl_row_obs9,tbl_row_obs10,tbl_row_obs11,tbl_row_obs12,tbl_row_obs13;
		public TableLayout tbl_layout_obs8,tbl_layout_obs9,tbl_layout_obs10,tbl_layout_obs11,tbl_layout_obs12,tbl_layout_obs13;
		public RadioButton rd_obs8_opt1_yes,rd_obs9_opt1_yes,rd_obs10_opt1_yes,rd_obs11_opt1_yes,rd_obs12_opt1_yes,rd_obs13_opt1_yes,
		rd_obs8_opt1_no,rd_obs9_opt1_no,rd_obs10_opt1_no,rd_obs11_opt1_no,rd_obs12_opt1_no,rd_obs13_opt1_no,
		rd_obs8_opt1_nd,rd_obs9_opt1_nd,rd_obs10_opt1_nd,rd_obs11_opt1_nd,rd_obs12_opt1_nd,rd_obs13_opt1_nd,
		rd_obs10_opta_yes,rd_obs10_opta_no,rd_obs10_opta_nd,rd_obs10_optb_yes,rd_obs10_optc_yes,rd_obs10_optd_yes,rd_obs10_opte_yes,
		rd_obs10_optb_no,rd_obs10_optc_no,rd_obs10_optd_no,rd_obs10_opte_no,rd_obs10_optb_nd,rd_obs10_optc_nd,rd_obs10_optd_nd,
		rd_obs10_opte_nd;
		public RelativeLayout show_layt_obs9_opt1,show_layt_obs10_opt1,show_layt_obs11_opt1,
		show_layt_obs12_opt1,show_layt_obs13_opt1;
		public EditText etcomments_obs8_opt1,etcomments_obs9_opt1,etcomments_obs10_opt1,etcomments_obs11_opt1,etcomments_obs12_opt1,
		etcomments_obs13_opt1,etother_obs11_opt1,etother_obs12_typeofcrack,etother_obs12_location,etother_obs12_approx,
		etother_obs12_cause,etother_obs13_typeofcrack,etother_obs13_location,etother_obs13_approx,etother_obs13_cause,
		etother_obs10_typeofcrack,etother_obs10_location,etother_obs10_approx,etother_obs10_cause,etcomments_obs10_opta,
		etcomments_obs10_optb,etcomments_obs10_optc,etcomments_obs10_optd,etcomments_obs10_opte,etother_obs10_etlen,
		etother_obs12_etlen,etother_obs13_etlen,etinch_obs6a,etfeet_obs6a;
		public CheckBox cb_obs101[]=new CheckBox[5];
		public CheckBox cb_obs102[]=new CheckBox[5];
		public CheckBox cb_obs103[]=new CheckBox[5];
		public CheckBox cb_obs104[]=new CheckBox[6];
		public CheckBox cb_obs105[]=new CheckBox[7];
		public CheckBox cb_obs11[]=new CheckBox[3];
		public CheckBox cb_obs121[]=new CheckBox[5];
		public CheckBox cb_obs122[]=new CheckBox[5];
		public CheckBox cb_obs123[]=new CheckBox[5];
		public CheckBox cb_obs124[]=new CheckBox[6];
		public CheckBox cb_obs125[]=new CheckBox[7];
		public CheckBox cb_obs131[]=new CheckBox[5];
		public CheckBox cb_obs132[]=new CheckBox[5];
		public CheckBox cb_obs133[]=new CheckBox[5];
		public CheckBox cb_obs134[]=new CheckBox[6];
		public CheckBox cb_obs135[]=new CheckBox[7];
		public TextView obs8_tbl_row_txt,obs10_tbl_row_txt,obs11_tbl_row_txt,obs12_tbl_row_txt,obs13_tbl_row_txt,obs9_tbl_row_txt;
		/** DECLARATION OF OBSERVATION2 ENDS HERE **/
		
		/**DECARTION OF OBSERVATION3 STARTS HERE **/
		public TableRow tbl_row_obs14,tbl_row_obs15,tbl_row_obs16,tbl_row_obs17,tbl_row_obs18,tbl_row_obs19,tbl_row_obs20,tbl_row_obs21,tbl_row_obs22;
		public TextView obs14_tbl_row_txt,obs15_tbl_row_txt,obs16_tbl_row_txt,obs17_tbl_row_txt,obs18_tbl_row_txt,obs19_tbl_row_txt,obs20_tbl_row_txt,
		                obs21_tbl_row_txt,obs22_tbl_row_txt;
		public TableLayout tbl_layout_obs14,tbl_layout_obs15,tbl_layout_obs16,tbl_layout_obs17,tbl_layout_obs18,tbl_layout_obs19,tbl_layout_obs20,
		                    tbl_layout_obs21,tbl_layout_obs22,show_tbl_obs14,show_tbl_obs15,show_tbl_obs16,show_tbl_obs17,show_tbl_obs18,show_tbl_obs19,
		                    show_tbl_obs20,show_tbl_obs21,show_tbl_obs22;
		public RadioButton rd_obs14_no,rd_obs15_no,rd_obs16_no,rd_obs17_no,rd_obs18_no,rd_obs19_no,rd_obs20_no,rd_obs21_no,rd_obs22_no,
		                   rd_obs14_nd,rd_obs15_nd,rd_obs16_nd,rd_obs17_nd,rd_obs18_nd,rd_obs19_nd,rd_obs20_nd,rd_obs21_nd,rd_obs22_nd,
		                   rd_obs14_yes,rd_obs15_yes,rd_obs16_yes,rd_obs17_yes,rd_obs18_yes,rd_obs19_yes,rd_obs20_yes,rd_obs21_yes,rd_obs22_yes;
		public String nostr="None noted at time of inspection to accessible areas surveyed.";
		public EditText etcomments_obs14,etcomments_obs15,etcomments_obs16,etcomments_obs17,etcomments_obs18,etcomments_obs19,etcomments_obs20,
		                etcomments_obs21,etcomments_obs22,etother_obs14,etother_obs15,etother_obs16,etother_obs17,etother_obs18,etother_obs19,
		                etother_obs20,etother_obs21,etother_obs22;
		public CheckBox cb_obs14[]=new CheckBox[25];
		public CheckBox cb_obs15[]=new CheckBox[25];
		public CheckBox cb_obs16[]=new CheckBox[25];
		public CheckBox cb_obs17[]=new CheckBox[25];
		public CheckBox cb_obs18[]=new CheckBox[25];
		public CheckBox cb_obs19[]=new CheckBox[25];
		public CheckBox cb_obs20[]=new CheckBox[25];
		public CheckBox cb_obs21[]=new CheckBox[25];
		public CheckBox cb_obs22[]=new CheckBox[25];
		/** DECLARATION OF OBSERVATION3 ENDS HERE **/
		/* Photos variable declaration */
		public int ph_type=51;  
		/* Photos variable declaration ends */
		/*DECLARATION OF OBSERVATION4 STARTS HERE*/
		public TableRow tbl_row_obs23,tbl_row_obs24,tbl_row_obs25,tbl_row_obs26,tbl_row_obs27;
		public TextView obs23_tbl_row_txt,obs24_tbl_row_txt,obs25_tbl_row_txt,obs26_tbl_row_txt,obs27_tbl_row_txt,addenum_tbl_row_txt;
		public TableLayout tbl_layout_obs23,tbl_layout_obs24,tbl_layout_obs25,tbl_layout_obs26,tbl_layout_obs27;
		public String obs4nostr="This is not a crawl space home.",obs4nastr="Beyond scope of inspection/survey.";
		public RadioButton rd_obs23_yes,rd_obs24_yes,rd_obs25_yes,rd_obs26_yes,rd_obs27_yes,rd_obs23_no,rd_obs24_no,rd_obs25_no,rd_obs26_no,rd_obs27_no,
		                   rd_obs23_nd,rd_obs24_nd,rd_obs25_nd,rd_obs26_nd,rd_obs27_nd,rd_obs23_opta_yes,rd_obs23_optb_yes,rd_obs23_optc_yes,
		                   rd_obs23_optd_yes,rd_obs23_opta_no,rd_obs23_optb_no,rd_obs23_optc_no,rd_obs23_optd_no,rd_obs23_opta_na,rd_obs23_optb_na,
		                   rd_obs23_optc_na,rd_obs23_optd_na,rd_obs26_opta_yes,rd_obs26_optb_yes,rd_obs26_optc_yes,rd_obs26_opta_no,rd_obs26_optb_no,
		                   rd_obs26_optc_no,rd_obs26_opta_nd,rd_obs26_optb_nd,rd_obs26_optc_nd;
		public EditText etcomments_obs23,etcomments_obs24,etcomments_obs25,etcomments_obs26,etcomments_obs27,etcomments_obs23_opta,etcomments_obs23_optb,
		                addednumcomments,etcomments_obs23_optc,etcomments_obs23_optd,etcomments_obs26_optb,etcomments_obs26_opta,etcomments_obs26_optc;
		/*DECLARATION OF OBSERVATION4 ENDS HERE*/
		public View show;
	    
	public String toptextvalue=""; /*"<font color=#bddb00> Policyholder Name : </font>" +
			"<b><font color=#ffffff> TEST INSPECTOR </font></b>"+"\t"+
			"<font color=#bddb00> Policy Number : </font>" +
			"<b><font color=#ffffff> FR123987654 </font></b>"+"\t"+
			"<font color=#bddb00> Inspection Date : </font>" +
			"<b><font color=#ffffff> 11/29/2011  </font></b>";*/
	public String redcolor ="<font color=red> * </font>";
	public String yearbuilt;
	public boolean application_sta=false;
	public boolean db_avb=false;
	CommonFunctions(Context con) {
		this.con = con;
		PackageManager m = con.getPackageManager();
		String s = con.getPackageName();
		PackageInfo p;
		try {
			
			p = m.getPackageInfo(s, 0);
			s = p.applicationInfo.dataDir;
			File f=new File(s+"/databases/"+MY_DATABASE_NAME);
			
	        if(f.exists())
	        {
	        	this.sh_db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
	        	PackageManager packageManager = con.getPackageManager();
        	    ComponentName componentName = new ComponentName(con,Dummy_luncher.class);
        	    packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED,PackageManager.DONT_KILL_APP);
	        }
	        else
	        {
	        	
	        	 this.sh_db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
	        	 String currentDBPath = "/data/com.sinkholeinspection/databases/"+MY_DATABASE_NAME;
	             //String backupDBPath = MY_DATABASE_NAME;
	        	 File data = Environment.getDataDirectory();
	             File currentDB = new File(data, currentDBPath);
	             File backupDB = new File(f.toString());
	            
	             if(currentDB.exists())
	             {
	            	 try
	            	 {
	            		
	            	 FileChannel src = new FileInputStream(currentDB).getChannel();
	                 FileChannel dst = new FileOutputStream(backupDB).getChannel();
	                
	                 dst.transferFrom(src, 0, src.size()); 
	                 
	                 src.close();
	                 dst.close();
	                 this.sh_db.execSQL("UPDATE "+inspectorlogin+" SET  Fld_InspectorFlag='0'");
	                //byte b[]=outputFile.to
								
	                 /*final String state = Environment.getExternalStorageState();
	                 if (Environment.MEDIA_MOUNTED.equals(state)) {
	                 Files[] files = Environment.getExternalStorageDirectory().listFiles();
	                 } else {
	                 ...
	                 }*/
	                 db_avb=true;
	            	 }
	            	 catch (Exception e) {
						// TODO: handle exception
	            		 System.out.println("Exception"+e.getMessage());
					}

	             }
	             else
	             {
	            	 this.sh_db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
	             }
	                 
	        }
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("error occure"+e.getMessage());
		}
		sh_db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
		//this.con = con;
		df = new android.text.format.DateFormat();
		datewithtime = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		

		/***checking for the All risk report application **/
		final PackageManager pm = con.getPackageManager();
        //get a list of installed apps.
		        List<ApplicationInfo> packages = pm
		                .getInstalledApplications(PackageManager.GET_META_DATA);
		
        for (ApplicationInfo packageInfo : packages) {

	          
			  if(packageInfo.packageName.toString().trim().equals("idsoft.inspectiondepot.IDMA")) // for checking the main app has installed  or not 
	            {
				  application_sta=true; // check if the main appliccation IDinspection has installed  or not 
	            }
			  if(packageInfo.packageName.toString().trim().equals("com.sinkholeinspection")) // for checking the main app has installed  or not 
	            {
				  db_avb=true; // check if the main appliccation IDinspection has installed  or not 
	            }
        }
        /***checking for the All risk report application Ends here  **/
       
	}
	
	public void showhelp(String alerttitle,String alertcontent) {
		// TODO Auto-generated method stub
		
		final Dialog dialog = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.getWindow().setContentView(R.layout.alert);
		TextView txt = (TextView) dialog.findViewById(R.id.txtid);
		txt.setText(Html.fromHtml(alertcontent));
		Button btn_helpclose = (Button) dialog.findViewById(R.id.helpclose);
		btn_helpclose.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
			
		});
		dialog.setCancelable(true);
		dialog.show();
	}

	public void ShowToast(String s, int i) {
		
		switch(i)
		{
		case 0:
			colorname="#000000";
			break;
		case 1:
			colorname="#890200";
			break;
		case 2:
			colorname="#F3C3C3";
			break;
		}
		 toast = new Toast(this.con);
		 LayoutInflater inflater = (LayoutInflater)this.con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				 View layout = inflater.inflate(R.layout.toast, null);
		
		TextView tv = (TextView) layout.findViewById(
				R.id.text);
		tv.setTextColor(Color.parseColor(colorname));
		toast.setGravity(Gravity.CENTER, 0, 0);
		tv.setText(s);
		//tv.setTextSize(Typeface.BOLD);
		
		//toast.setGravity(Gravity.BOTTOM, 10, 80);
		toast.setView(layout);
		fireLongToast();
		

	}
	
	private void fireLongToast() {

	        Thread t = new Thread() {
	            public void run() {
	                int count = 0;
	                try {
	                    while (true && count < 10) {
	                        toast.show();
	                        sleep(3);
	                        count++;

	                        // do some logic that breaks out of the while loop
	                    }
	                } catch (Exception e) {
	                   
	                }
	            }
	        };
	        t.start();
	    }

	public void Device_Information()
	{
		deviceId = Settings.System.getString(this.con.getContentResolver(), Settings.System.ANDROID_ID);
		model = android.os.Build.MODEL;
		manuf = android.os.Build.MANUFACTURER;
		devversion = android.os.Build.VERSION.RELEASE;
		apiLevel = android.os.Build.VERSION.SDK;
		WifiManager wifiManager = (WifiManager)(this.con.getSystemService(this.con.WIFI_SERVICE));
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		ipAddress = wifiInfo.getIpAddress();
	}
	public void Create_Table(int select) {
		switch (select) {
		case 1:
			/*INSPECTOR LOGIN*/
			try {
				sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ inspectorlogin
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,Fld_InspectorId varchar(50),Fld_InspectorFirstName varchar(50),Fld_InspectorMiddleName varchar(50),Fld_InspectorLastName varchar(50),Fld_InspectorAddress varchar(150),Fld_InspectorCompanyName varchar(150),Fld_InspectorCompanyId varchar(100),Fld_InspectorUserName varchar(50),Fld_InspectorPassword varchar(50),Fld_InspectorPhotoExtn Varchar(10),Android_status bit,Fld_InspectorFlag bit,Fld_Remember_pass  bit DEFAULT 0,Fld_InspectorEmail varchar(100));");
			} catch (Exception e) {
				strerrorlog="Inspector Login table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Inspector Login Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;
		case 2:
			/*VERSION TABLE*/ 
		     try {
				// sh_db("DROP TABLE IF  EXISTS "
				// + version + " ");
		    	 sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Version
						+ "(VId INTEGER PRIMARY KEY Not null,SH_VersionCode varchar(50),SH_VersionName varchar(50));");

			} catch (Exception e) {
				strerrorlog="Version Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Inspector Login Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;
		case 3:
			/* POLICYHOLDER */
			try {
				sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ policyholder
						+ " (Ph_Id INTEGER PRIMARY KEY AUTOINCREMENT,SH_PH_InspectorId varchar(50) NOT NULL,SH_PH_SRID varchar(50) NOT NULL,SH_PH_FirstName varchar(100) NOT NULL,SH_PH_LastName varchar(100) NOT NULL,SH_PH_Address1 varchar(100),SH_PH_Address2 varchar(100),SH_PH_City varchar(100),SH_PH_Zip varchar(100),SH_PH_State varchar(100),SH_PH_County varchar(100),SH_PH_Policyno varchar(100),SH_PH_Inspectionfees varchar(100),SH_PH_InsuranceCompany varchar(100),SH_PH_HomePhone varchar(100),SH_PH_WorkPhone varchar(100),SH_PH_CellPhone varchar(100),SH_PH_Email varchar(100),SH_PH_EmailChkbx Integer,SH_PH_InspectionTypeId Integer,SH_PH_IsInspected Integer,SH_PH_IsUploaded Integer,SH_PH_Status Integer,SH_PH_SubStatus Integer,SH_Schedule_ScheduledDate varchar(100),SH_Schedule_InspectionStartTime varchar(100),SH_Schedule_InspectionEndTime varchar(100),SH_Schedule_Comments varchar(500),SH_Schedule_ScheduleCreatedDate varchar(100),SH_Schedule_AssignedDate varchar(100),SH_ScheduleFlag Integer,SH_YearBuilt varchar(100));");
			} catch (Exception e) {
				strerrorlog="PolicyHolder table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of PolicyHolder Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;
		case 4:
			/*Summery question */
			try{
				
				sh_db.execSQL("CREATE TABLE IF NOT EXISTS "+SQ_table+" (SH_SQ_Id INTEGER PRIMARY KEY AUTOINCREMENT,SH_SQ_Inspectorid varchar(50),SH_SQ_Homeid varchar(50),SH_SQ_SIFC_Q1 varchar(20),SH_SQ_SIFC_Q2 varchar(20),SH_SQ_SC_Comments varchar(1000),SH_SQ_HPHC_present varchar(20),SH_SQ_HPHC_Q1 varchar(20),SH_SQ_HPHC_Q2 varchar(20),SH_SQ_HPHC_Q3 varchar(20),SH_SQ_HPHC_Q4 varchar(20),SH_SQ_HPHC_Q5 varchar(20),SH_SQ_HPHC_Q6 varchar(20),SH_SQ_HPHC_Q7 varchar(20),SH_SQ_HC_Comments varchar(1000));");
			}
			catch (Exception e) {
				strerrorlog="Summery question table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
		break;
		case 5:
			/*Building question  */
			try{
				
				sh_db.execSQL("CREATE TABLE IF NOT EXISTS "+BIQ_table+" (SH_BI_Id INTEGER PRIMARY KEY AUTOINCREMENT,SH_BI_Inspectorid varchar(50),SH_SQ_Homeid varchar(50),SH_BI_LBC_BD varchar(50),SH_BI_LBC_PF varchar(100),SH_BI_LBC_PAI varchar(100),SH_BI_LBC_PAIO varchar(100),SH_BI_GBI_BT varchar(200),SH_BI_GBI_YOC varchar(20),SH_BI_GBI_YOCO varchar(50),SH_BI_GBI_BS varchar(50),SH_BI_GBI_FW varchar(250),SH_BI_GBI_FR varchar(250)," +
						"SH_BI_NofStories varchar(50),SH_BI_WSC_URM varchar(20),SH_BI_WSC_RM varchar(20),SH_BI_WSC_RC varchar(20),SH_BI_WSC_WMF varchar(20),SH_BI_WSC_TNBR varchar(50),SH_BI_WSC_TNEWO varchar(50),SH_BI_WSC_TNEDO varchar(50),SH_BI_WC_AS varchar(20),SH_BI_WC_VS varchar(20),SH_BI_WC_WS varchar(20),SH_BI_WC_other varchar(20),SH_BI_WC_Stucco varchar(20),SH_BI_WC_BV varchar(20),SH_BI_WC_PB varchar(20),SH_BI_AD_cmd varchar(1000));");
			}
			catch (Exception e) {
				strerrorlog="Building question table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
		break;

		case 6:
			/* ONLINE POLICYHOLDER */
			try {
				sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Onlinepolicyholder
						+ " (On_Ph_Id INTEGER PRIMARY KEY AUTOINCREMENT,SH_OL_PH_InspectorId varchar(50) NOT NULL,SH_OL_PH_SRID varchar(50) NOT NULL,SH_OL_PH_FirstName varchar(100) NOT NULL,SH_OL_PH_LastName varchar(100) NOT NULL,SH_OL_PH_Address1 varchar(100),SH_OL_PH_City varchar(100),SH_OL_PH_Zip varchar(100),SH_OL_PH_State varchar(100),SH_OL_PH_County varchar(100),SH_OL_PH_Policyno varchar(100),SH_OL_PH_InspectionTypeId Integer,SH_OL_PH_Status Integer,SH_OL_PH_SubStatus Integer,SH_OL_Schedule_ScheduledDate varchar(100),SH_OL_Schedule_InspectionStartTime varchar(100),SH_OL_Schedule_InspectionEndTime varchar(100));");
			} catch (Exception e) {
				strerrorlog="Online PolicyHolder table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of PolicyHolder Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;
		case 7:
			/* MAILING DETAILS OF POLICYHOLDER */
			try {
				sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ MailingPolicyHolder
						+ " (MA_Ph_Id INTEGER PRIMARY KEY AUTOINCREMENT,SH_ML_PH_InspectorId varchar(50) NOT NULL,SH_ML_PH_SRID varchar(50) NOT NULL,SH_ML Integer,SH_ML_PH_Address1 varchar(100),SH_ML_PH_Address2 varchar(100),SH_ML_PH_City varchar(100),SH_ML_PH_Zip varchar(100),SH_ML_PH_State varchar(100),SH_ML_PH_County varchar(100));");
			} catch (Exception e) {
				strerrorlog="Mailing PolicyHolder table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of PolicyHolder Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;
		case 8:
		      /*OBSERVATION1  TABLE*/ 
		          try {
		       // sh_db("DROP TABLE IF  EXISTS "
		       // + Observation1+ " ");
		           sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
		         + Observation1
		         + "(OB1Id INTEGER PRIMARY KEY Not null,SH_OBS1_SRID varchar(50),IrregularLandSurface varchar(50),IrregularLandSurfaceLocation varchar(250),IrregularLandSurfaceComments varchar(500),VisibleBuriedDebris varchar(50),VisibleBuriedDebrisLocation varchar(250),VisibleBuriedDebrisComments varchar(500),IrregularLandSurfaceAdjacent varchar(50),IrregularLandSurfaceAdjacentLocation varchar(250),IrregularLandSurfaceAdjacentComments varchar(500),SoilCollapse varchar(50),SoilCollapseLocation varchar(250),SoilCollapseComments varchar(500),SoilErosionAroundFoundation varchar(50),SoilErosionAroundFoundationOption varchar(300),SoilErosionAroundFoundationComments varchar(500),DrivewaysCracksNoted varchar(50),DrivewaysCracksOption varchar(200),DrivewaysCracksComments varchar(500),UpliftDrivewaysSurface varchar(50),UpliftDrivewaysSurfaceOption varchar(200),UpliftDrivewaysSurfaceComments varchar(500),LargeTree varchar(50),LargeTreeComments varchar(500),CypressTree varchar(50),CypressTreeLocation varchar(250),CypressTreeComments varchar(500),LargeTreesRemoved varchar(50),LargeTreesRemovedLocation varchar(250),LargeTreesRemovedComments varchar(500),FoundationCrackNoted varchar(50),FoundationTypeofCrack varchar(200),FoundationLocation varchar(250),FoundationWidthofCrack  varchar(250),FoundationLengthofCrack varchar(100),FoundationCleanliness varchar(250),FoundationProbableCause varchar(250),FoundationComments varchar(500),RetainingWallsServiceable varchar(100),RetainingWallsServiceOption varchar(250),RetainingWallsServiceComments varchar(500));");

		      } catch (Exception e) {
		       strerrorlog="Observation1 Table not created";
		       Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Observation1 Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
		       
		      }
		              break;

		case 9:
			   /*OBSERVATION1 Tree TABLE*/ 
			       try {
			    // sh_db("DROP TABLE IF  EXISTS "
			    // + Observation1T+ " ");
			        sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
			      + Observation1T
			      + "(OB1TId INTEGER PRIMARY KEY Not null,SH_OBS1T_SRID varchar(50),Width varchar(100),Height varchar(100),Location varchar(300));");

			   } catch (Exception e) {
			    strerrorlog="Observation1 Tree Table not created";
			    Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Observation1 Tree Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			    
			   }
			           break;

		case 10:
			   /*OBSERVATION2 TABLE*/ 
			       try {
			    // sh_db("DROP TABLE IF  EXISTS "
			    // + Observation2+ " ");
			        sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
			      + Observation2
			      + "(OB2Id INTEGER PRIMARY KEY Not null,SH_OBS2_SRID varchar(50),PoolDeckSlabNoted varchar(25),PoolDeckSlabComments varchar(500),PoolShellPlumbLevel varchar(25),PoolShellPlumbLevelComments varchar(500),ExcessiveSettlement varchar(25),ExcessiveSettlementTypeofCrack varchar(200),ExcessiveSettlementLocation varchar(250),ExcessiveSettlementWidthofCrack varchar(250),ExcessiveSettlementLengthofCrack varchar(100),ExcessiveSettlementCleanliness varchar(250),ExcessiveSettlementProbableCause varchar(250),ExcessiveSettlementComments varchar(500),WallLeaningNoted varchar(25),WallLeaningComments varchar(500),WallsVisiblyNotLevel varchar(25),WallsVisiblyNotLevelComments varchar(500),WallsVisiblyBulgingNoted varchar(25),WallsVisiblyBulgingComments varchar(500),OpeningsOutofSquare varchar(25),OpeningsOutofSquareComments varchar(500),DamagedFinishesNoted varchar(25),DamagedFinishesComments varchar(500),SeperationCrackNoted varchar(25),SeperationCrackTypeofCrack varchar(200),SeperationCrackComments varchar(500),ExteriorOpeningCracksNoted varchar(25),ExteriorOpeningCracksTypeofCrack varchar(200),ExteriorOpeningCracksLocation varchar(250),ExteriorOpeningCracksWidthofCrack varchar(250),ExteriorOpeningCracksLengthofCrack varchar(100),ExteriorOpeningCracksCleanliness varchar(250),ExteriorOpeningCracksProbableCause varchar(250),ExteriorOpeningCracksComments varchar(500),ObservationSettlementNoted varchar(100),ObservationSettlementTypeofCrack varchar (200),ObservationSettlementLocation varchar (250),ObservationSettlementWidthofCrack varchar (250),ObservationSettlementLengthofCrack varchar (100),ObservationSettlementCleanliness varchar (250),ObservationSettlementProbableCause varchar (250),ObservationSettlementComments varchar (500));");

			   } catch (Exception e) {
			    strerrorlog="Observation2 Table not created";
			    Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Observation2 Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			    
			   }
			           break;

		case 11:
			/*OBSERVATION3 TABLE*/ 
		     try {
				// sh_db("DROP TABLE IF  EXISTS "
				// + Observation3+ " ");
		    	 sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Observation3
						+ "(OB3Id INTEGER PRIMARY KEY Not null,SH_OBS3_SRID varchar(50),InteriorWallCrackNoted varchar(25),InteriorWallCrackOption varchar(500),InteriorWallCrackComments varchar(500),WallSlabCrackNoted varchar(25),WallSlabCrackOption varchar(500),WallSlabCrackComments varchar(500),InteriorCeilingCrackNoted varchar(25),InteriorCeilingCrackOption varchar(500),InteriorCeilingCrackComments varchar(500),InteriorFloorCrackNoted  varchar(25),InteriorFloorCrackOption  varchar(500),InteriorFloorCrackComments varchar(500),WindowFrameOutofSquare varchar(25),WindowFrameOutofSquareOption varchar(500),WindowFrameOutofSquareComments varchar(500),FloorSlopingNoted varchar(25),FloorSlopingOption varchar(500),FloorSlopingComments varchar(500),CrackedGlazingNoted varchar(25),CrackedGlazingOption varchar(500),CrackedGlazingComments varchar(500),PreviousCorrectiveMeasureNoted varchar(25),PreviousCorrectiveMeasureOption varchar(500),PreviousCorrectiveMeasureComments varchar(500),BindingDoorOpeningNoted	varchar(25),BindingDoorOpeningOption varchar(500),BindingDoorOpeningComments varchar(500));");

			} catch (Exception e) {
				strerrorlog="Observation3 Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Observation3 Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
           break;



		case 12:
			/*OBSERVATION4 TABLE*/ 
		     try {
				// sh_db("DROP TABLE IF  EXISTS "
				// + Observation4+ " ");
		    	 sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Observation4
						+ "(OB4Id INTEGER PRIMARY KEY Not null,SH_OBS4_SRID varchar(50),CrawlSpacePresent varchar(20),CrawlSpaceComments varchar(500),StandingWaterNoted varchar(20),StandingWaterComments varchar(500),LeakingPlumbNoted varchar(20),LeakingPlumbComments  varchar(500),VentilationNoted varchar(20),VentilationComments  varchar(500),CrawlSpaceAccessible  varchar(20),CrawlSpaceAccessibleComments  varchar(500),RoofSagNoted  varchar(20),RoofSagComments  varchar(500),RoofLeakageEvidence  varchar(20),RoofLeakageComments  varchar(500),DeferredMaintanenceIssuesNoted  varchar(20),DeferredMaintanenceComments  varchar(500),DeterioratedNoted  varchar(20),DeterioratedComments  varchar (500),DeterioratedExteriorFinishesNoted  varchar(20),DeterioratedExteriorFinishesComments  varchar(500),DeterioratedExteriorSidingNoted  varchar(20),DeterioratedExteriorSidingComments  varchar(500),SafetyNoted  varchar(20),SafetyComments  varchar(500),OverallComments varchar(1000));");

			} catch (Exception e) {
				strerrorlog="Observation4 Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Observation4 Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
           break;

		case 13:
			/** Feedback info table **/
			try {
				
				// + FeedBackInfoTable + " ");
				sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ this.FeedBackInfoTable
						+ " (SH_FI_id INTEGER PRIMARY KEY Not null,SH_FI_Srid Varchar(50) ,SH_FI_IsCusServiceCompltd Bit Not null Default(0),SH_FI_PresentatInspection Varchar(50) ,SH_FI_IsInspectionPaperAvbl Bit Not null Default(0),SH_FI_IsManufacturerInfo Bit Not null Default(0),SH_FI_FeedbackComments Varchar(2000),SH_FI_CreatedOn Datetime Null,SH_FI_othertxt Varchar(50));");

			} catch (Exception e) {
				strerrorlog="Feed back info  table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Feed back  Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}

			break;
		case 14:
			/** Creating feedbackdocuments table **/
			try {
				
				// + FeedBackDocumentTable + " ");
				sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ this.FeedBackDocumentTable
						+ " (SH_FI_DocumentId INTEGER PRIMARY KEY Not null,SH_FI_SRID Varchar(50) ,SH_FI_DocumentTitle Varchar(50),SH_FI_FileName Varchar(50) ,SH_FI_Nameext Varchar(150),SH_FI_ImageOrder INTEGER Not null default(0),SH_FI_IsOfficeUse Bit Not null Default(0),SH_FI_CreatedOn Datetime Null,SH_FI_ModifiedDate Datetime Null);");

			} catch (Exception e) {
				strerrorlog="Feed back Document  table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Feed back  Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 15:
			/** creating image table **/
			try
			{
				sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
			    + ImageTable + " (SH_IM_ID INTEGER PRIMARY KEY Not null,SH_IM_SRID Varchar(50) ,SH_IM_Ques_Id INTEGER Not null Default(0),SH_IM_Elevation INTEGER Not null Default(0),SH_IM_ImageName Varchar(150) ,SH_IM_Nameext Varchar(150),SH_IM_Description Varchar(150),SH_IM_CreatedOn Datetime Null,SH_IM_ModifiedOn Datetime Null,SH_IM_ImageOrder INTEGER Not null default(0),SH_IM_Delflag INTEGER);");

			} catch (Exception e) {
				strerrorlog="images table creation table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of images Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
		break;
		case 16:
			/** creating Photocaption table **/
			try
			{
				sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
					    + Photo_caption + " (SH_IMP_ID INTEGER PRIMARY KEY Not null,SH_IMP_INSP_ID Varchar(50) ,SH_IMP_Caption Varchar(150) ,SH_IMP_Elevation INTEGER Not null Default(0),SH_IMP_ImageOrder Varchar(150));");
			} catch (Exception e) {
				strerrorlog="images table creation table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of images Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
	break;
		case 17: 
			/** creating Agent information table **/
			try
			{
				sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
			    + Agent_tabble + " (SH_AI_id INTEGER PRIMARY KEY Not null,SH_AI_SRID  Varchar(50) ,SH_AI_AgencyName  Varchar(50) ,SH_AI_AgentName  Varchar(50) ,SH_AI_AgentAddress  Varchar(50) ,SH_AI_AgentAddress2  Varchar(50) ,SH_AI_AgentCity  Varchar(50) ,SH_AI_AgentCounty  Varchar(50) ,SH_AI_AgentRole  Varchar(50) ,SH_AI_AgentState  Varchar(50) ,SH_AI_AgentZip  Varchar(50) ,SH_AI_AgentOffPhone  Varchar(50) ,SH_AI_AgentContactPhone  Varchar(50) ,SH_AI_AgentFax  Varchar(50) ,SH_AI_AgentEmail  Varchar(50) ,SH_AI_AgentWebSite Varchar(50));");

			} catch (Exception e) {
				strerrorlog="Agent info  table creation table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Agent info Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			
	break;
		case 18: 
			/** creating Agent information table **/
			try
			{
				sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
			    + Cloud_table + " (SH_CI_id INTEGER PRIMARY KEY Not null, SRID Varchar(100) ,  assignmentId  Varchar(100) , assignmentMasterId  Varchar(100) , inspectionPolicy_Id  Varchar(100) , " +
			    "priorAssignmentId  Varchar(100) , inspectionType  Varchar(100) , assignmentType  Varchar(100) , policyId  Varchar(100) , policyVersion  Varchar(100) , " +
			    "policyEndorsement  Varchar(100) , policyEffectiveDate  Varchar(100) , policyForm  Varchar(100) , policySystem  Varchar(100) , lob  Varchar(100) , " +
			    "structureCount  Varchar(100) , structureNumber  Varchar(100) , structureDescription  Varchar(100) , propertyAddress  Varchar(100) , propertyAddress2  " +
			    "Varchar(100) , propertyCity  Varchar(100) , propertyState  Varchar(100) , propertyCounty  Varchar(100) , propertyZip  Varchar(100) , insuredFirstName  " +
			    "Varchar(100) , insuredLastName  Varchar(100) , insuredHomePhone  Varchar(100) , insuredWorkPhone  Varchar(100) , insuredAlternatePhone  Varchar(100) , " +
			    "insuredEmail  Varchar(100) , insuredMailingAddress  Varchar(100) , insuredMailingAddress2  Varchar(100) , insuredMailingCity  Varchar(100) , " +
			    "insuredMailingState  Varchar(100) , insuredMailingZip  Varchar(100) , agencyID  Varchar(100) , agencyName  Varchar(100) , agencyPhone  Varchar(100) ," +
			    " agencyFax  Varchar(100) , agencyEmail  Varchar(100) , agencyPrincipalFirstName  Varchar(100) , agencyPrincipalLastName  Varchar(100) ," +
			    " agencyPrincipalEmail  Varchar(100) , agencyFEIN  Varchar(100) , agencyMailingAddress  Varchar(100) , agencyMailingAddress2  Varchar(100) ," +
			    " agencyMailingCity  Varchar(100) , agencyMailingState  Varchar(100) , agencyMailingZip  Varchar(100) , agentID  Varchar(100) , agentFirstName " +
			    " Varchar(100) , agentLastName  Varchar(100) , agentEmail  Varchar(100) , previousInspectionDate  Varchar(100) , previousInspectionCompany  " +
			    "Varchar(100) , previousInspectorFirstName  Varchar(100) , previousInspectorLastName  Varchar(100) , previousInspectorLicense  Varchar(100) ," +
			    " yearBuilt  Varchar(100) , squareFootage  Varchar(100) , numberOfStories  Varchar(100) , construction  Varchar(100) , roofCovering  Varchar(100)" +
			    " , roofDeckAttachment  Varchar(100) , roofWallAttachment  Varchar(100) , roofShape  Varchar(100) , secondaryWaterResistance  Varchar(100) , " +
			    "openingCoverage  Varchar(100) , buildingType Varchar(100));");

			} catch (Exception e) {
				strerrorlog="Cloud info  table creation not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of cloud info Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			
	break;
	
		case 19: 
			/** creating Addiitonal information table **/
			try
			{
				sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
			    + Additional_table + " (SH_AD_id INTEGER PRIMARY KEY Not null,SH_AD_SRID  Varchar(50) ,SH_AD_INSPID Varchar(50),SH_AD_IsRecord boolean,SH_AD_UserTypeName Varchar(50),SH_AD_ContactEmail varchar(50),SH_AD_PhoneNumber varchar(15),SH_AD_MobileNumber varchar(15),SH_AD_BestTimetoCallYou varchar(50),SH_AD_FirstChoice varchar(100),SH_AD_SecondChoice varchar(50),SH_AD_ThirdChoice varchar(50),SF_AD_FeedbackComments varchar(500));");

			} catch (Exception e) {
				strerrorlog="Additional info  table creation not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Agent info Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			
	break;
		case 20:/** CREATING IDMA VERSION TABLE **/
			try {
	
					
					sh_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ IDMAVersion
							+ "(VId INTEGER PRIMARY KEY Not null,ID_VersionCode varchar(50),ID_VersionName varchar(50),ID_VersionType varchar(50));");

				} catch (Exception e) {
					
					
				}
			
			break;

		}

	}
	public SoapObject export_header(String string)
	{
		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		SoapObject add_property = new SoapObject(NAMESPACE,string);
		return add_property;
	}
    public void Error_LogFile_Creation(String strerrorlog2)
    {
        try {
 			
 			 DataOutputStream out = new DataOutputStream(this.con.openFileOutput("errorlogfile.txt", this.con.MODE_APPEND));
 			 out.writeUTF(strerrorlog2);
 			 
    	 }
    	 catch(Exception e)
    	 {
    		 
    	 }
    }
    public String encode(String oldstring) {
		if(oldstring==null)
		{
			oldstring="";
		}
		try {
			oldstring = URLEncoder.encode(oldstring, "UTF-8");
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return oldstring;

	}
    public String decode(String newstring) {
		try {
			newstring = URLDecoder.decode(newstring, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newstring;

	}
    public Cursor SelectTablefunction(String tablename, String wherec) {
		Cursor cur = sh_db.rawQuery("select * from " + tablename + " " + wherec, null);
		return cur;

	}
    public SoapObject Calling_WS(String string, String string2, String string3) throws SocketException,IOException,NetworkErrorException,TimeoutException, XmlPullParserException,Exception
    {
    	SoapObject request = new SoapObject(NAMESPACE,string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("UserName",string);
		request.addProperty("Password",string2);
		envelope.setOutputSoapObject(request);;
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE+string3,envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;
		
    }

	public void show_ProgressDialog(String string) {
		// TODO Auto-generated method stub
		String source = "<b><font color=#00FF33>"+string+" . Please wait...</font></b>";
		pd = ProgressDialog.show(this.con, "", Html.fromHtml(source), true);
	}

	public SoapObject Calling_WS1(String id, String string) throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException, XmlPullParserException {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(NAMESPACE,string);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("inspectorid",id);
		envelope.setOutputSoapObject(request);;
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE+string,envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;
	}

	public String export_footer(SoapSerializationEnvelope envelope22,
			SoapObject add_property, String string) {
		// TODO Auto-generated method stub
		envelope22.setOutputSoapObject(add_property);
		HttpTransportSE androidHttpTransport11 = new HttpTransportSE(URL);
		try {
			androidHttpTransport11.call(NAMESPACE+string, envelope22);
			String result = String.valueOf(envelope22.getResponse());
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "false";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "false";
		}
		
		
	}

	public void getInspectorId() {

		// TODO Auto-generated method stub
		try {
			Cursor cur = this.SelectTablefunction(this.inspectorlogin,
					" where Fld_InspectorFlag=1");
			cur.moveToFirst();
			if (cur != null) {
				do {
					Insp_id = decode(cur.getString(cur.getColumnIndex("Fld_InspectorId")));
					//Insp_id="1655";
					Insp_firstname = decode(cur.getString(cur.getColumnIndex("Fld_InspectorFirstName")));
					Insp_lastname = decode(cur.getString(cur.getColumnIndex("Fld_InspectorLastName")));
					Insp_address = decode(cur.getString(cur.getColumnIndex("Fld_InspectorAddress")));
					Insp_companyname = decode(cur.getString(cur.getColumnIndex("Fld_InspectorCompanyName")));
					Insp_email = decode(cur.getString(cur.getColumnIndex("Fld_InspectorEmail")));
					
					//Insp_firstname="ANDREW";Insp_lastname="DULCIE";
				} while (cur.moveToNext());
			}
			cur.close();
		} catch (Exception e) {
			Error_LogFile_Creation(e.getMessage()+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of getting inspector details from Inspector Login table at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
		}
	}

	public String getdeviceversionname() {
		// TODO Auto-generated method stub
		try {
			versionname = this.con.getPackageManager().getPackageInfo(this.con.getPackageName(), 0).versionName;
			
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			Error_LogFile_Creation(e.getMessage()+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of getting device version name at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
		}
		return versionname;
	}

	public void fn_logout(final String insp_id2) {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		LinearLayout lin = (LinearLayout)dialog1.findViewById(R.id.remember_password);
		lin.setVisibility(show.VISIBLE);
		TextView txttitle = (TextView) dialog1.findViewById(R.id.RP_txthelp);
		txttitle.setText("Logout");//txttitle.setTextAppearance(con,Typeface.BOLD);
		Button btn_helpclose = (Button) dialog1.findViewById(R.id.RP_close);
		TextView txt = (TextView) dialog1.findViewById(R.id.RP_txtid);
		txt.setText("Are you sure,Do you want to Exit the application?");
		Button btn_yes= (Button) dialog1.findViewById(R.id.RP_yes);
		Button btn_no= (Button) dialog1.findViewById(R.id.RP_no);
		btn_no.setText("   No   ");
		
		btn_helpclose.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
			
		});
		btn_no.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
			
		});
		btn_yes.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				try{sh_db.execSQL("UPDATE " + inspectorlogin
						+ " SET Fld_InspectorFlag=0" + " WHERE Fld_InspectorId ='"+ insp_id2 + "'");
				Intent loginpage;
				if(application_sta)
				{
					loginpage = new Intent(Intent.ACTION_MAIN);
					loginpage.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.LogOut"));
				}
				else
				{
				loginpage = new Intent(con,SinkholeInspectionActivity.class);
				
				}
				con.startActivity(loginpage);
				}catch(Exception e)
				{
					Error_LogFile_Creation(e.getMessage()+" "+" at "+ con.getClass().getName().toString()+" "+" in the stage of logout(catch) at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
					
				}
				dialog1.dismiss();
				ShowToast("You are Log out sucessfully.", 0);
			}
			
		});
		dialog1.show();
	
	}
	public final boolean isInternetOn() {
			boolean chk = false;
			ConnectivityManager conMgr = (ConnectivityManager) this.con.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = conMgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				chk = true;
			} else {
				chk = false;
			}

			return chk;
		}

	public void showalert(String alerttitle2, String alertcontent2) {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
		txttitle.setText(alerttitle2);
		TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
		txt.setText(Html.fromHtml(alertcontent2));
		Button btn_helpclose = (Button) dialog1.findViewById(R.id.helpclose);
		btn_helpclose.setVisibility(show.GONE);
		Button btn_ok = (Button) dialog1.findViewById(R.id.ok);
		btn_ok.setVisibility(show.VISIBLE);
		btn_ok.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.dismiss();
				con.startActivity(new Intent(con,Import.class));
			}
			
		});
		Button btn_dash = (Button) dialog1.findViewById(R.id.dash);
		btn_dash.setVisibility(show.VISIBLE);
		btn_dash.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.dismiss();
				con.startActivity(new Intent(con,Dashboard.class));
			}
			
		});
		dialog1.setCancelable(false);
		dialog1.show();
	}
	

	public void gohome() {
		// TODO Auto-generated method stub
		this.con.startActivity(new Intent(this.con,HomeScreen.class));
	}

	public void MoveTo_HomeOwner(String string, int carrcit2, String string2, int i) {
		// TODO Auto-generated method stub
		 String name,classname ;
		if(string.equals("19"))
		{
			name=strcarr;
		}
		else
		{
			name=strret;
		}
		switch(i)
		{
		case 1:
			tempclass=classnames[4];
			
			break;
		case 2:
			tempclass=classnames[5];
			break;
		}
		if(carrcit2==0)
		{
			ShowToast("Sorry, No records found for "+string2+" status in "+name+".",0);
		}
		else
		{
			Intent intent = new Intent(this.con,tempclass);
			intent.putExtra("InspectionType", string);
			intent.putExtra("status", string2);
			this.con.startActivity(intent);
		}
		
	}

	public void MoveTo_OnlineList(String string, int onlcarrassign2,
			String string2, String string3, String string4) {
		// TODO Auto-generated method stub
		   String name;
			if(string.equals("19"))
			{
				name=strcarr;
			}
			else
			{
				name=strret;
			}
			if(onlcarrassign2==0)
			{
				ShowToast("Sorry, No records found for "+string4+" status in "+name+".",0);
			}
			else
			{
				Intent intent = new Intent(this.con,OnlineList.class);
				intent.putExtra("InspectionType", string);
				intent.putExtra("Status", string2);
				intent.putExtra("SubStatus", string3);
				this.con.startActivity(intent);
			}
	}

	public SoapObject Calling_WS2(String onlstatus2, String onlsubstatus2,
			String onlinspectionid2, String insp_id2, String string) {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(NAMESPACE,string);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectionStatus",onlstatus2);
		request.addProperty("SubStatus",onlsubstatus2);
		request.addProperty("InspectiontypeID",onlinspectionid2);
		request.addProperty("inspectorid",insp_id2);
		envelope.setOutputSoapObject(request);;
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		try {
			androidHttpTransport.call(NAMESPACE+string,envelope);
			SoapObject result = (SoapObject) envelope.getResponse();
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		
	}
	public void dropTable(int select) {
		switch (select) {
		case 1:
			/** drop OnlineTable **/
			try {
				sh_db.execSQL("DROP TABLE IF EXISTS "+ Onlinepolicyholder );
				
			} catch (Exception e) {
				strerrorlog="Online Policy holder table not droped";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Inspector Login Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;
		}
    }

	public boolean Checkbox(CheckBox cb_mailing) {
		// TODO Auto-generated method stub
		if(cb_mailing.isChecked())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	public String PhoneNo_validation(String decode) {
		// TODO Auto-generated method stub
		if (decode.length() == 10) {
			StringBuilder sVowelBuilder = new StringBuilder(decode);
			sVowelBuilder.insert(0, "(");
			sVowelBuilder.insert(4, ")");
			sVowelBuilder.insert(8, "-");
			newphone = sVowelBuilder.toString();
			return "Yes";
            
		} else {
			return "No";
		}


}

	public String Email_Validation(String string) {
		// TODO Auto-generated method stub
		
			//Pattern emailPattern = Pattern.compile(".+[@].+\\.[a-z]+");
        Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
  		Matcher emailMatcher = emailPattern.matcher(string);
  		if (emailMatcher.matches()) {              return "Yes";    		} else {    			return "No";    		}	}

	public CharSequence Set_phoneno(String strphn) {
		// TODO Auto-generated method stub
		StringBuilder sVowelBuilder1 = new StringBuilder(strphn);
		sVowelBuilder1.deleteCharAt(0);
		sVowelBuilder1.deleteCharAt(3);
		sVowelBuilder1.deleteCharAt(6);
		strphn = sVowelBuilder1.toString();
	return strphn;
	}
	public void showing_limit(String s,LinearLayout lp,LinearLayout l,TextView t, String i) // Need to send the text in edit text and the parrent li then li then the text view whic show the percnetag   
	{
		int length=s.toString().length();
		double par_width= lp.getWidth();
		double par_unit;
		if(par_width==0)
		{
			par_width=Double.parseDouble(i); // SET THE DEFAULT VALUE FOR THE FIRST TIME LOADING 
		}
		
		par_unit=par_width/Double.parseDouble(i);
		LinearLayout.LayoutParams mParam = new LinearLayout.LayoutParams((int)(length*par_unit),(int)(8));
		l.setLayoutParams(mParam);
		double totalle=(length*par_unit);
	    int percent=(int) ((length)*(100.00/Double.parseDouble(i)));
	    if(length==Integer.parseInt(i))
	    {
	    	t.setText("   "+percent+" % Exceed limit");
	    }
	    else
	    {
	    	t.setText("   "+percent+" %");
	    }
		
	}
	public void showing_limit(String s,LinearLayout lp,LinearLayout l,TextView t) // Need to send the text in edit text and the parrent li then li then the text view whic show the percnetag   
	{
		int length=s.toString().length();
		double par_width= lp.getWidth();
		double par_unit;
		if(par_width==0)
		{
			par_width=940; // SET THE DEFAULT VALUE FOR THE FIRST TIME LOADING 
		}
		
		par_unit=par_width/999.00;
		LinearLayout.LayoutParams mParam = new LinearLayout.LayoutParams((int)(length*par_unit),(int)(8));
		l.setLayoutParams(mParam);
		double totalle=(length*par_unit);
	    int percent=(int) ((length)*(100.00/999.00));
	    if(length==999)
	    {
	    	t.setText("   "+percent+" % Exceed limit");
	    }
	    else
	    {
	    	t.setText("   "+percent+" %");
	    }
		
	}

	// get the selected radio button form the group of radiuo buttons 
	public String getslected_radio(RadioButton[] R) {
		// TODO Auto-generated method stub
		for(int i=0;i<R.length;i++)
		{
			if(R[i].isChecked() && !R[i].getText().toString().equals("Other"))
			{
				return R[i].getText().toString();
			}
		}
		return "";
	}

	public String getselected_chk(CheckBox[] CB) {
		// TODO Auto-generated method stub
		
		String s = "";
		for(int i=0;i<CB.length;i++)
		{
			if(CB[i].isChecked() && !CB[i].getText().toString().equals("Other"))
			{
				 s+=CB[i].getText().toString()+"^";
			}
		}
		if(s.length()>=2)
		{
			s=s.substring(0, s.length()-1);
		}
		return s;
	}

	public void setvalueradio(String s,RadioButton[] R,EditText other) {
		// TODO Auto-generated method stub
		if(!s.equals(""))
		{
			for(int i=0;i<R.length;i++)
			{
				if(s.equals(R[i].getText().toString()))
				{
					R[i].setChecked(true);
					return;
				}
			}
			R[R.length-1].setChecked(true); // no matches found so we set the other text to true and pass the value to that edit text view 
			other.setVisibility(View.VISIBLE);
			other.setText(s);
			return;
		}
	}

	public void setvaluechk(String s,CheckBox[] CB,EditText other) {
		// TODO Auto-generated method stub
		
		if(!s.equals(""))
		{
			String k=s.replace("^", "~");
			String temp[]=k.split("~");
			
			int j=0;
			do{
				for(int i=0;i<CB.length;i++)
				{
					
					if(temp[j].equals(CB[i].getText().toString()))
					{
						
						CB[i].setChecked(true);
						j++;
						if(j==temp.length)
						{
							return;
						}
					}
					
				}
				if(j!=temp.length)
				{
					
					CB[CB.length-1].setChecked(true); // no matches found for the last value in the split text so we set the other to tru  
					other.setVisibility(View.VISIBLE);
					other.setText(temp[j]);
					j++;	
				}
			}while(j<temp.length);
		}
	}

	public void putExtras(Intent myintent) {
		// TODO Auto-generated method stub
		myintent.putExtra("homeid", selectedhomeid );
		myintent.putExtra("InspectionType", onlinspectionid );
		myintent.putExtra("status", onlstatus );
		myintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
	}

	public void getCalender() {
		// TODO Auto-generated method stub
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
	}

	public void Set_UncheckBox(CheckBox[] cb_obs142, EditText etother_obs142) {
		// TODO Auto-generated method stub
		for(int i=0;i<cb_obs142.length;i++)
		{
			 cb_obs142[i].setChecked(false);
			 if(cb_obs142[i].getText().toString().equals("Other"))
			 {
				 etother_obs142.setText("");
				 etother_obs142.setVisibility(show.GONE);
			 }
		}
	}
public void getinspectiondate() {
		
		try {
			Cursor cur = this.SelectTablefunction(this.policyholder,
					" where SH_PH_SRID='" + this.selectedhomeid + "' and SH_PH_InspectorId='"
							+ this.Insp_id + "'");
			cur.moveToFirst();
			this.inspectiondate = cur.getString(cur
					.getColumnIndex("SH_Schedule_ScheduledDate"));
			
			
		} catch (Exception e) {
		
		}
	}
	public Bitmap ShrinkBitmap(String file, int width, int height) {
		 Bitmap bitmap =null;
			try {
				BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
				bmpFactoryOptions.inJustDecodeBounds = true;
			    bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

				int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
						/ (float) height);
				int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
						/ (float) width);
               
				if (heightRatio > 1 || widthRatio > 1) {
					if (heightRatio > widthRatio) {
						bmpFactoryOptions.inSampleSize = heightRatio;
					} else {
						bmpFactoryOptions.inSampleSize = widthRatio;
					}
				}

				bmpFactoryOptions.inJustDecodeBounds = false;
				bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
				return bitmap;
			} catch (Exception e) {
			
				return bitmap;
			}

		}
	
	public Dialog showalert() {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
		TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
		Button btn_helpclose = (Button) dialog1.findViewById(R.id.helpclose_ex);
		Button btn_helpclose_t = (Button) dialog1.findViewById(R.id.helpclose_ex_t);
		
		btn_helpclose.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
			
		});
		btn_helpclose_t.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
			
		});
		return dialog1;
		
	}
	public void showing_limitwithdefaultwidth(String s,LinearLayout lp,LinearLayout l,TextView t,int defaultwidth) // Need to send the text in edit text and the parrent li then li then the text view whic show the percnetag   
	{
		int length=s.toString().length();
		double par_width= lp.getWidth();
		double par_unit;
		if(par_width==0)
		{
			par_width=defaultwidth; // SET THE DEFAULT VALUE FOR THE FIRST TIME LOADING 
		}
		
		par_unit=par_width/999.00;
		LinearLayout.LayoutParams mParam = new LinearLayout.LayoutParams((int)(length*par_unit),(int)(8));
		l.setLayoutParams(mParam);
		double totalle=(length*par_unit);
	    int percent=(int) ((length)*(100.00/999.00));
	    if(length==999)
	    {
	    	t.setText("   "+percent+" % Exceed limit");
	    }
	    else
	    {
	    	t.setText("   "+percent+" %");
	    }
		
	}
	public boolean getinspectionstatus(String id, String string) throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(NAMESPACE,string);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectorID",id);
		request.addProperty("SRID",this.selectedhomeid);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE+"GetInspectionStatus",envelope);
		String result =  envelope.getResponse().toString();
		
		if(result.trim().equals("40")||result.trim().equals("41"))
		{
			return true;
		}
		else
		{		
			return false;
		}
		
	}

	public String[] setvalueToArray(Cursor tbl) {
		// TODO Auto-generated method stub
		String[] arr=null;
		if(tbl!=null && tbl.getCount()==1)
		{
			tbl.moveToFirst();
			arr=new String[tbl.getColumnCount()];
			for(int i=0;i<tbl.getColumnCount();i++)
			{
				if(decode(tbl.getString(i))==null)
				{
					arr[i]="";	
				}
				else
				{
					arr[i]=decode(tbl.getString(i));
				}
				
			}
		}
			return arr;
		
	}

	public boolean check_Status(String result) {
		// TODO Auto-generated method stub
		try
		{
			if(result!=null)
			{
			if(result.trim().equals("true"))
			{
				return true;
			}
			else
			{
				return false;
			}
			}
			else
			{
				return false;
			}
		}
		catch (Exception e)
		{
			return false;
		}
	}

	public CharSequence phototextdisplay() {
		// TODO Auto-generated method stub
		  SpannableString content = new SpannableString("Take Photo");
		  content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		 
		return content;
	}

	public void getInspecionTypeid(String srid)
	{
		try {
			Cursor c2 = sh_db.rawQuery("SELECT * FROM "
					+ policyholder + " WHERE SH_PH_SRID='" + encode(srid)
					+ "' and SH_PH_InspectorId='" + encode(Insp_id) + "'", null);
			
		
			c2.moveToFirst();
			if(c2.getCount()==1)
			{
				Inspectiontypeid=decode(c2.getString(c2.getColumnIndex("SH_PH_InspectionTypeId")));
			}
		}
		catch(Exception e)
		{
		    strerrorlog="Retrieving policy number ";
			Error_LogFile_Creation(strerrorlog+" "+" at "+ con.getClass().getName().toString()+" "+" in the stage of Inspector Login Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			
		}
		
	}

	public void setFocus(EditText etcomments_obs1_opt12) {
		// TODO Auto-generated method stub
		etcomments_obs1_opt12.setFocusableInTouchMode(true);
		etcomments_obs1_opt12.requestFocus();
	}

	public void fn_delete(String dt) {
		// TODO Auto-generated method stub
		Create_Table(1);Create_Table(2);Create_Table(3);Create_Table(4);
		Create_Table(5);Create_Table(6);Create_Table(7);Create_Table(8);
		Create_Table(9);Create_Table(10);Create_Table(11);Create_Table(12);
		Create_Table(13);Create_Table(14);Create_Table(15);Create_Table(16);
		try {
			sh_db.execSQL("DELETE FROM " + policyholder
					+ " WHERE SH_PH_SRID ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		try {
			sh_db.execSQL("DELETE FROM " + MailingPolicyHolder
					+ " WHERE SH_ML_PH_SRID ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		try {
			sh_db.execSQL("DELETE FROM " + SQ_table
					+ " WHERE SH_SQ_Homeid ='" + encode(dt) + "'");
		} catch (Exception e) {
		}
		try {
			sh_db.execSQL("DELETE FROM " + BIQ_table
					+ " WHERE SH_SQ_Homeid ='" + encode(dt) + "'");
		} catch (Exception e) {
		}
		try {
			sh_db.execSQL("DELETE FROM " + Observation1
					+ " WHERE SH_OBS1_SRID ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		try {
			sh_db.execSQL("DELETE FROM " + Observation1T
					+ " WHERE SH_OBS1T_SRID ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		try {
			sh_db.execSQL("DELETE FROM " + Observation2
					+ " WHERE SH_OBS2_SRID ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		try {
			sh_db.execSQL("DELETE FROM " + Observation3
					+ " WHERE SH_OBS3_SRID ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		try {
			sh_db.execSQL("DELETE FROM " + Observation4
					+ " WHERE SH_OBS4_SRID ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		try {
			sh_db.execSQL("DELETE FROM " + ImageTable
					+ " WHERE SH_IM_SRID ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		try {
			sh_db.execSQL("DELETE FROM " + FeedBackInfoTable
					+ " WHERE SH_FI_Srid ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		try {
			sh_db.execSQL("DELETE FROM " + FeedBackDocumentTable
					+ " WHERE SH_FI_SRID ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		ShowToast("Deleted sucessfully.",1);
	}

	public void getDeviceDimensions() {
		// TODO Auto-generated method stub
		    DisplayMetrics metrics = new DisplayMetrics();
			((Activity) this.con).getWindowManager().getDefaultDisplay().getMetrics(metrics);

			wd = metrics.widthPixels;
			ht = metrics.heightPixels;
	}

	public void startCameraActivity() {
		// TODO Auto-generated method stub
		String fileName = "temp.jpg";
 		ContentValues values = new ContentValues();
 		values.put(MediaStore.Images.Media.TITLE, fileName);
 		mCapturedImageURI =this.con.getContentResolver().insert(
 				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
 		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
 		intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
 		((Activity) this.con).startActivityForResult(intent, CAPTURE_PICTURE_INTENT);
		/*Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		((Activity) this.con).startActivityForResult(Intent.createChooser(intent, "Select Picture"),SELECT_PICTURE);*/
	}
	public void showselectedimage(String capturedImageFilePath) {


		// TODO Auto-generated method stub
		picpath=capturedImageFilePath;
		BitmapFactory.Options o = new BitmapFactory.Options();
	 		o.inJustDecodeBounds = true;
		
	 		dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alert);
			
			/** get the help and update relative layou and set the visbilitys for the respective relative layout */
			LinearLayout Re=(LinearLayout) dialog1.findViewById(R.id.maintable);
			Re.setVisibility(View.GONE);
			LinearLayout Reup=(LinearLayout) dialog1.findViewById(R.id.updateimage);
			Reup.setVisibility(View.GONE);
			LinearLayout camerapic=(LinearLayout) dialog1.findViewById(R.id.cameraimage);
			camerapic.setVisibility(View.VISIBLE);
			tvcamhelp = (TextView)dialog1.findViewById(R.id.camtxthelp);
			tvcamhelp.setText("Save Picture");
			tvcamhelp.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
			spinvw = (RelativeLayout)dialog1.findViewById(R.id.cam_photo_update);
			capvw = (RelativeLayout)dialog1.findViewById(R.id.camaddcaption);
			/** get the help and update relative layou and set the visbilitys for the respective relative layout */
			
			ImageView upd_img = (ImageView) dialog1.findViewById(R.id.cameraimg);
			btn_helpclose = (Button) dialog1.findViewById(R.id.camhelpclose);
			btn_camcaptclos  = (Button) dialog1.findViewById(R.id.camcapthelpclose);
			Button btn_save = (Button) dialog1.findViewById(R.id.camsave);
			Button btn_cancl= (Button) dialog1.findViewById(R.id.camdelete);
			elevspin = (Spinner) dialog1.findViewById(R.id.cameraelev);
			cameratxt = (TextView)dialog1.findViewById(R.id.captxt);
			captionspin = (Spinner) dialog1.findViewById(R.id.cameracaption);
			 adapter2 = new ArrayAdapter(this.con,android.R.layout.simple_spinner_item, elevnames);
 			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 			elevspin.setAdapter(adapter2);
 			elevspin.setOnItemSelectedListener(new MyOnItemSelectedListener());
			try {
	 				BitmapFactory.decodeStream(new FileInputStream(capturedImageFilePath),
	 						null, o);
	 				final int REQUIRED_SIZE = 400;
		 			int width_tmp = o.outWidth, height_tmp = o.outHeight;
		 			int scale = 1;
		 			while (true) {
		 				if (width_tmp / 2 < REQUIRED_SIZE
		 						|| height_tmp / 2 < REQUIRED_SIZE)
		 					break;
		 				width_tmp /= 2;
		 				height_tmp /= 2;
		 				scale *= 2;
		 				BitmapFactory.Options o2 = new BitmapFactory.Options();
			 			o2.inSampleSize = scale;
			 			Bitmap bitmap = null;
			 	    	bitmap = BitmapFactory.decodeStream(new FileInputStream(
			 	    		capturedImageFilePath), null, o2);
			 	       BitmapDrawable bmd = new BitmapDrawable(bitmap);
		 			   upd_img.setImageDrawable(bmd);
		 			}
	 		
	 		} catch (FileNotFoundException e) {
	 			
	 		}
			btn_save.setOnClickListener(new OnClickListener() {


	            public void onClick(View v) {


	            	if(!elevspin.getSelectedItem().toString().equals("Select Elevation"))
	            	{
	            		if(captionspin.getSelectedItem().toString().equals("Select")||captionspin.getSelectedItem().toString().equals("ADD PHOTO CAPTION"))
	            		{
	            			ShowToast("Please add Photo Caption.", 0);
	            		}
	            		else
	            		{
                            int j;
		            		
		            		Cursor c11 = sh_db.rawQuery("SELECT * FROM " + ImageTable
		        	 				+ " WHERE SH_IM_SRID='" + selectedhomeid + "' and SH_IM_Elevation='" + spinelevval
		        	 				+ "' and SH_IM_Delflag=0 order by SH_IM_CreatedOn DESC", null);
		        	 		int imgrws = c11.getCount();
		        	 		
			        	 		if (imgrws == 0) {
			        	 			j = imgrws + 1;
			        	 		} else {
			        	 			j = imgrws + 1;
	
			        	 		}
			        	 		String full_name="";
			        	 		if(spinelevval==1)
			        			{
			        				name="EP";
			        				full_name="External photos";
			        				
			        			}
			        			else if(spinelevval==2)
			        			{
			        				name="R&A";
			        				full_name="Roof and attic photos";
			        			}
			        			else if(spinelevval==3)
			        			{
			        				name="G&A";
			        				full_name="Ground and adjoining images";
			        			}
			        			else if(spinelevval==4)
			        			{
			        				name="I&P";
			        				full_name="Internal photos";
			        			}
			        			else if(spinelevval==5)
			        			{
			        				name="A&P";
			        				full_name="Additional photos";
			        			}
			        	 		if(imgrws<4)
			        	 		{
			        	 		String[] bits = picpath.split("/");
								String	picname = bits[bits.length - 1];
							sh_db.execSQL("INSERT INTO "
											+ ImageTable
											+ " (SH_IM_SRID,SH_IM_Ques_Id,SH_IM_Elevation,SH_IM_ImageName,SH_IM_Description,SH_IM_Nameext,SH_IM_CreatedOn,SH_IM_ModifiedOn,SH_IM_ImageOrder,SH_IM_Delflag)"
											+ " VALUES ('"
											+ selectedhomeid
											+ "','"
											+ quesid
											+ "','"
											+ spinelevval
											+ "','"
											+ encode(picpath)
											+ "','"
											+ encode(newspin) + "','"
											+ encode(picname)
											+ "','" + datewithtime + "','" + datewithtime + "','"
											+ j + "','" + 0 + "')");
			            		ShowToast("\"Taken photo successfully saved into your "+full_name+" . \"",1);
			            		dialog1.dismiss();
		            		}
		        	 		else
		        	 		{
		        	 			ShowToast(" You can add only 4 images for the "+full_name, 0);
		        	 		}
	            		}
	            	}
	            	else
	            	{
	            		ShowToast("Please select Elevation type.", 0);
	            	}
	            }
			});
			btn_cancl.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            	dialog1.dismiss();
	            }
			});
			btn_helpclose.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            	dialog1.dismiss();
	            }
			});
	 	  	dialog1.setCancelable(false);
			dialog1.show();
				
			
	}
	private class MyOnItemSelectedListener implements OnItemSelectedListener {

		
		public void onItemSelected(AdapterView<?> parent, View view,final int pos,



				long id) {
			String selected_elev = elevspin.getSelectedItem().toString();
		    if(selected_elev.equals("External photos"))
			{
		    	cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=1;
		    	
			}
			else if(selected_elev.equals("Roof and attic photos"))
			{
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=2;
			}
			else if(selected_elev.equals("Ground and adjoining images"))
			{
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=3;
			}
			else if(selected_elev.equals("Internal photos"))
			{
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=4;
			}
			else if(selected_elev.equals("Additional photos"))
			{
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=5;
			}
			else
			{
				cameratxt.setVisibility(view.GONE);
		    	captionspin.setVisibility(view.GONE);
		    	sp=0;
			}
		    
		    call_eleavtion(selected_elev,sp);
			}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			cameratxt.setVisibility(arg0.GONE);
	    	captionspin.setVisibility(arg0.GONE);
		}
		}
	public void call_eleavtion(String selected_elev, int sp2) {

		// TODO Auto-generated method stub
		String elev_name;
		spinelevval = adapter2.getPosition(selected_elev);
		elev=sp2;
		
		if(selected_elev.equals("External photos")) {elev_name="EP"; }
		else if(selected_elev.equals("Roof and attic photos")) {elev_name="R&A";}
		else if(selected_elev.equals("Ground and adjoining images")){elev_name="G&A";}
		else if(selected_elev.equals("Internal photos"))	{elev_name="I&P";}
		else if(selected_elev.equals("Additional photos")) {elev_name="A&P";}
		
		Create_Table(15);Create_Table(16);
		
		Cursor c11 = sh_db.rawQuery("SELECT * FROM " + ImageTable
				+ " WHERE SH_IM_SRID='" + this.selectedhomeid + "' and SH_IM_Elevation='" + spinelevval
				+ "' and SH_IM_Delflag=0 order by SH_IM_CreatedOn DESC", null);
	
	    int k;
		int imgrws = c11.getCount();
		if (imgrws == 0) {
			k = imgrws + 1;
		} else {
			k = imgrws + 1;

		}
        
		
		Cursor c1=SelectTablefunction(Photo_caption, " WHERE SH_IMP_Elevation='"+sp2+"' and SH_IMP_ImageOrder='"+k+"'");
		String[] temp;
		if(c1.getCount()>0)
		{
			temp=new String[c1.getCount()+2];
			temp[0]="Select";
			temp[1]="ADD PHOTO CAPTION";
			int i=2;
			c1.moveToFirst();
			do
			{
				temp[i]=decode(c1.getString(c1.getColumnIndex("SH_IMP_Caption")));	
				i++;
			}while(c1.moveToNext());
		}
		else
		{
			temp=new String[2];
			temp[0]="Select";
			temp[1]="ADD PHOTO CAPTION";
		}
		    ArrayAdapter adapter = new ArrayAdapter(this.con,android.R.layout.simple_spinner_item, temp);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			captionspin.setAdapter(adapter);
			captionspin.setOnItemSelectedListener(new MyOnItemSelectedListener2());
	}
	private class MyOnItemSelectedListener2 implements OnItemSelectedListener {


		
		public void onItemSelected(AdapterView<?> parent, View view,final int pos,



				long id) {
			newspin = captionspin.getSelectedItem().toString();
			if(newspin.equals("ADD PHOTO CAPTION"))
			{
				capvw.setVisibility(view.VISIBLE);
				btn_camcaptclos.setVisibility(view.VISIBLE);
				btn_helpclose.setVisibility(view.GONE);
				spinvw.setVisibility(view.GONE);
			
				tvcamhelp.setText("Add Caption");
				Button edt_save = (Button) dialog1.findViewById(R.id.edtsave);
				Button edt_cancl= (Button) dialog1.findViewById(R.id.edtdelete);
				etcaption = (EditText) dialog1.findViewById(R.id.addcaption);
				btn_camcaptclos.setOnClickListener(new OnClickListener() {


		            public void onClick(View v) {
		            	capvw.setVisibility(v.GONE);
						spinvw.setVisibility(v.VISIBLE);
						btn_camcaptclos.setVisibility(v.GONE);
						btn_helpclose.setVisibility(v.VISIBLE);
						captionspin.setSelection(0);
		            }
				});
				edt_cancl.setOnClickListener(new OnClickListener() {


		            public void onClick(View v) {

		            	etcaption.setText("");
		            	capvw.setVisibility(v.GONE);
						spinvw.setVisibility(v.VISIBLE);
						btn_camcaptclos.setVisibility(v.GONE);
						btn_helpclose.setVisibility(v.VISIBLE);
						captionspin.setSelection(0);
						etcaption.setFocusable(false);
						tvcamhelp.setText("Save Picture");
				
		            }
				});
				
				edt_save.setOnClickListener(new OnClickListener() {



		            public void onClick(View v) {



		            	if(etcaption.getText().toString().trim().equals(""))
		            	{
		            		ShowToast("Please enter caption.",0);
		            	}
		            	else
		            	{
		            		int j;
		            		
		            		Cursor c11 = sh_db.rawQuery("SELECT * FROM " + ImageTable
		        	 				+ " WHERE SH_IM_SRID='" + selectedhomeid + "' and SH_IM_Elevation='" + spinelevval
		        	 				+ "' and SH_IM_Delflag=0 order by SH_IM_CreatedOn DESC", null);
		        	 		int imgrws = c11.getCount();
		        	 		if (imgrws == 0) {
		        	 			j = imgrws + 1;
		        	 		} else {
		        	 			j = imgrws + 1;

		        	 		}
		        	 		if(spinelevval==1)
		        			{
		        				name="EP";
		        			}
		        			else if(spinelevval==2)
		        			{
		        				name="R&A";
		        			}
		        			else if(spinelevval==3)
		        			{
		        				name="G&A";
		        			}
		        			else if(spinelevval==4)
		        			{
		        				name="I&P";
		        			}
		        			else if(spinelevval==5)
		        			{
		        				name="A&P";
		        			}
		        	 		String[] bits = picpath.split("/");
							String	picname = bits[bits.length - 1];
							
		            		sh_db.execSQL("INSERT INTO "
									+ Photo_caption
									+ " (SH_IMP_INSP_ID,SH_IMP_Caption,SH_IMP_Elevation,SH_IMP_ImageOrder)"
									+ " VALUES ('"+Insp_id+"','" + encode(etcaption.getText().toString()) + "','" + spinelevval + "','" + j + "')");
		            
		            		ShowToast("\"Caption has been added sucessfully.\"",1);
		            		hidekeyboard();
		            		etcaption.setText("");
			            	capvw.setVisibility(v.GONE);
							spinvw.setVisibility(v.VISIBLE);
							btn_camcaptclos.setVisibility(v.GONE);
							btn_helpclose.setVisibility(v.VISIBLE);
							Create_Table(15);Create_Table(16);
							
							Cursor c12 = sh_db.rawQuery("SELECT * FROM " + ImageTable
									+ " WHERE SH_IM_SRID='" + selectedhomeid + "' and SH_IM_Elevation='" + spinelevval
									+ "' and SH_IM_Delflag=0 order by SH_IM_CreatedOn DESC", null);
						
						    int k;
							int imgrws1 = c12.getCount();
							if (imgrws1 == 0) {
								k = imgrws1 + 1;
							} else {
								k = imgrws1 + 1;

							}
					        
							
							Cursor c1=SelectTablefunction(Photo_caption, " WHERE SH_IMP_Elevation='"+elev+"' and SH_IMP_ImageOrder='"+k+"'");
							String[] temp;
							if(c1.getCount()>0)
							{
								temp=new String[c1.getCount()+2];
								temp[0]="Select";
								temp[1]="ADD PHOTO CAPTION";
								int i=2;
								c1.moveToFirst();
								do
								{
									temp[i]=decode(c1.getString(c1.getColumnIndex("SH_IMP_Caption")));	
									i++;
								}while(c1.moveToNext());
							}
							else
							{
								temp=new String[2];
								temp[0]="Select";
								temp[1]="ADD PHOTO CAPTION";
							}
							    ArrayAdapter adapter = new ArrayAdapter(con,android.R.layout.simple_spinner_item, temp);
								adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
								captionspin.setAdapter(adapter);
								captionspin.setOnItemSelectedListener(new MyOnItemSelectedListener2());
						//	captionspin.setSelection(0);
		            		//dialog1.dismiss();
		            	}
		            }
				});
		
		}
		else
		{
			tvcamhelp.setText("Save Picture");
		}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {

			// TODO Auto-generated method stub
			
		}
	}
	public void hidekeyboard()	
	{
		InputMethodManager imm = (InputMethodManager)con.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
	}
	public boolean checkresponce(Object responce) {
		if (responce != null) {
			if (!"null".equals(responce.toString())
					&& !responce.toString().equals("")) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public void netalert(final String string) {
		// TODO Auto-generated method stub
		String newstr = "";
		if(string.equals("FGS")||string.equals("MAPD"))
		{
			if(string.equals("MAPD"))
			{
				newstr=" Mapping feature";
			}else{
			newstr=" Florida Geological Survey(FGS) Map";}
		}
		else if(string.equals("MAP")){
			newstr="Feedback section has been saved sucessfully."+" Mapping feature";
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(
				 this.con);
		    builder.setCancelable(false);
			builder.setTitle("Network Connection")
			.setMessage(
					newstr+" requires an Internet Connection.\n Do you have internet Connection? ");
			builder.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
							
							if(string.equals("FGS"))
							{ 
								myintent = new Intent(con,FGS.class); 
							    putExtras(myintent);
							    con.startActivity(myintent);
							}
							else if(string.equals("MAPD")||string.equals("MAP"))
							{
								myintent = new Intent(con,Maps.class); 
								putExtras(myintent);
								con.startActivity(myintent);
								
							}
						}
					});
			builder.setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
                        // ShowToast("Internet connection is not available.",0);
						}
					});
			builder.show();
			

	}

	public boolean common(String path) {
		File f = new File(path);
		if (f.length() < 2097152) {
			return true;
		} else {
			return false;
		}

	}

	public String fn_CurrentInspector(String insp_id2, String selectedhomeid2) throws SocketException,IOException,NetworkErrorException,TimeoutException, XmlPullParserException,Exception {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(NAMESPACE,"IsCurrentInspector");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectorID",insp_id2);
		request.addProperty("Srid",selectedhomeid2);
		envelope.setOutputSoapObject(request);;	
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE+"IsCurrentInspector",envelope);
		String result =  envelope.getResponse().toString();
		return result;
		
	}

	public String fn_CheckExportCount(String insp_id2, String selectedhomeid2) throws SocketException,IOException,NetworkErrorException,TimeoutException, XmlPullParserException,Exception {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(NAMESPACE,"GetExportCount");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectorID",insp_id2);
		request.addProperty("SRID",selectedhomeid2);
		envelope.setOutputSoapObject(request);;
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE+"GetExportCount",envelope);
		String resultcnt =  envelope.getResponse().toString();
		return resultcnt;
	}
	public void  delete_all(final String where) {
		// TODO Auto-generated method stub
		 alerttitle="Delete";
		  alertcontent="Are you sure want to delete All Inspection ?";
		    final Dialog dialog1 = new Dialog(con,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alertsync);
			TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
			txttitle.setText(alerttitle);
			TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
			txt.setText(Html.fromHtml( alertcontent));
			Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
			Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
			btn_yes.setOnClickListener(new OnClickListener()
			{
	       	@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();
					//cf.fn_delete(dt);
					Create_Table(1);Create_Table(2);Create_Table(3);Create_Table(4);
					Create_Table(5);Create_Table(6);Create_Table(7);Create_Table(8);
					Create_Table(9);Create_Table(10);Create_Table(11);Create_Table(12);
					Create_Table(13);Create_Table(14);Create_Table(15);Create_Table(16);Create_Table(17);
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SH_PH_SRID in "+where;
						
						sh_db.execSQL("DELETE FROM " + policyholder
								+ " "+temp);
					} catch (Exception e) {
						System.out.println("Error "+e.getMessage());
					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SH_ML_PH_SRID in "+where;
						
						sh_db.execSQL("DELETE FROM " + MailingPolicyHolder
								+ " "+temp);
					} catch (Exception e) {
						System.out.println("Error "+e.getMessage());
					}
					try {
						
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SH_SQ_Homeid in "+where;
						
						sh_db.execSQL("DELETE FROM " + SQ_table
								+ " "+temp);
					} catch (Exception e) {
						System.out.println("Error "+e.getMessage());
					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SH_SQ_Homeid in "+where;
						sh_db.execSQL("DELETE FROM " + BIQ_table
								+ " " +temp);
					} catch (Exception e) {
						System.out.println("Error "+e.getMessage());
					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SH_OBS1_SRID in "+where;
						
						sh_db.execSQL("DELETE FROM " + Observation1
								+ " "+temp);
					} catch (Exception e) {
						System.out.println("Error "+e.getMessage());
					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SH_OBS1T_SRID in "+where;
						
						sh_db.execSQL("DELETE FROM " + Observation1T
								+ " "+ temp);
					} catch (Exception e) {
						System.out.println("Error "+e.getMessage());
					}
					try {   
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SH_OBS2_SRID in "+where;
				
						sh_db.execSQL("DELETE FROM " + Observation2
								+ " "+ temp);
					} catch (Exception e) {
						System.out.println("Error "+e.getMessage());
					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SH_OBS3_SRID in "+where;
						sh_db.execSQL("DELETE FROM " + Observation3
								+ " "+temp);
					} catch (Exception e) {
						System.out.println("Error "+e.getMessage());
					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SH_OBS4_SRID in "+where;
						
						sh_db.execSQL("DELETE FROM " + Observation4
								+ " "+temp);
					} catch (Exception e) {
						System.out.println("Error "+e.getMessage());
					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SH_IM_SRID in "+where;
						
						sh_db.execSQL("DELETE FROM " + ImageTable
								+ " "+temp);
					} catch (Exception e) {
                     System.out.println("Error "+e.getMessage());
					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SH_FI_Srid in "+where;
						
						sh_db.execSQL("DELETE FROM " + FeedBackInfoTable
								+ " "+temp);
					} catch (Exception e) {
						System.out.println("Error "+e.getMessage());
					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SH_FI_SRID in "+where;
						
						sh_db.execSQL("DELETE FROM " + FeedBackDocumentTable
								+ " "+temp);
					} catch (Exception e) {
						System.out.println("Error "+e.getMessage());
					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SH_AI_SRID in "+where;
						
						sh_db.execSQL("DELETE FROM " + Agent_tabble
								+ " "+temp);
						
					} catch (Exception e) {
						System.out.println("Error "+e.getMessage());
					} 
					//System.out.println("Error not here ");
					ShowToast("Deleted sucessfully.",1);
					con.startActivity(new Intent(con,HomeScreen.class));
					
				}
				
			});
			btn_cancel.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					
				}
				
			});
			dialog1.setCancelable(false);
			dialog1.show();
	 
	}
	public void setRcvalue(TextView releasecode2) {
		// TODO Auto-generated method stub
		if(URL.contains("72.15.221.153"))
			releasecode2.setText(apkrc.replace("U", "L"));
			else
				releasecode2.setText(apkrc);
	}

}

package idsoft.inspectiondepot.sinkholeinspection;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class PolicyHolder extends Activity{
	CommonFunctions cf;
	public String chkstatus[]={"true","true","true"};
	public String strwrkphn,strcellphn,strhmephn;
	public int mc=0,ml=0;
	public ArrayAdapter adapter1;
	public EditText Y_O_C_spn_other;
	public Spinner Y_O_C_spn;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);System.out.println("policyshow");
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
         	}
         	setContentView(R.layout.policyholder);
         	cf.Create_Table(7);
         	cf.getDeviceDimensions();
 	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
 	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,cf));
			LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.generalsubmenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 12, 1,cf));
			
			ScrollView scr =(ScrollView)findViewById(R.id.policyholderscr);
		    scr.setSmoothScrollingEnabled(true);
			   scr.smoothScrollTo(0,0);
			  HorizontalScrollView hscr=(HorizontalScrollView)findViewById(R.id.HorizontalScrollView01);
			   hscr.smoothScrollTo(0,0);
			TextView firstnametxt = (TextView) findViewById(R.id.tvfirstname);
			firstnametxt.setText(Html.fromHtml(cf.redcolor+" "+"First Name "));
			cf.et_firstname  = (EditText) findViewById(R.id.etfirstname);
			
			TextView lastnametxt = (TextView) findViewById(R.id.tvlastname);
			lastnametxt.setText(Html.fromHtml(cf.redcolor+" "+"Last Name "));
			cf.et_lastname  = (EditText) findViewById(R.id.etlastname);
			
			TextView addresstxt = (TextView) findViewById(R.id.tvaddress1);
			addresstxt.setText(Html.fromHtml(cf.redcolor+" "+"Address 1 "));
			cf.et_address1  = (EditText) findViewById(R.id.etaddress1);
			
			TextView ziptxt = (TextView) findViewById(R.id.tvzip);
			ziptxt.setText(Html.fromHtml(cf.redcolor+" "+"ZIP "));
			cf.et_zip  = (EditText) findViewById(R.id.etzip);
			
			TextView address2txt = (TextView) findViewById(R.id.tvaddress2);
			address2txt.setText(Html.fromHtml(" "+"Address 2 "));
			cf.et_address2  = (EditText) findViewById(R.id.etaddress2);
			
			TextView hmephntxt = (TextView) findViewById(R.id.tvhmephn);
			hmephntxt.setText(Html.fromHtml(cf.redcolor+" "+"Home Phone "));
			cf.et_hmephn  = (EditText) findViewById(R.id.ethmephn);
			
			TextView citytxt = (TextView) findViewById(R.id.tvcity);
			citytxt.setText(Html.fromHtml(cf.redcolor+"City "));
			cf.et_city  = (EditText) findViewById(R.id.etcity);
			
			TextView wrkphntxt = (TextView) findViewById(R.id.tvwrkphn);
			wrkphntxt.setText(Html.fromHtml(" "+"Work Phone "));
			cf.et_wrkphn  = (EditText) findViewById(R.id.etwrkphn);
			
			TextView statetxt = (TextView) findViewById(R.id.tvstate);
			statetxt.setText(Html.fromHtml(cf.redcolor+"State "));
			cf.et_state  = (EditText) findViewById(R.id.etstate);
			
			TextView cellphntxt = (TextView) findViewById(R.id.tvcellphn);
			cellphntxt.setText(Html.fromHtml(" "+"Cell Phone "));
			cf.et_cellphn  = (EditText) findViewById(R.id.etcellphn);
			
			TextView countytxt = (TextView) findViewById(R.id.tvcounty);
			countytxt.setText(Html.fromHtml(cf.redcolor+"County "));
			cf.et_county  = (EditText) findViewById(R.id.etcounty);
			
			TextView emailtxt = (TextView) findViewById(R.id.tvemail);
			emailtxt.setText(Html.fromHtml(cf.redcolor+"Email "));
			cf.et_email  = (EditText) findViewById(R.id.etemail);
			
			TextView policynotxt = (TextView) findViewById(R.id.tvpolicyno);
			policynotxt.setText(Html.fromHtml(cf.redcolor+"Policy # "));
			cf.et_policyno  = (EditText) findViewById(R.id.etpolicyno);
			
			TextView feestxt = (TextView) findViewById(R.id.tvinspectionfees);
			feestxt.setText(Html.fromHtml(" "+"Inspection fees "));
			cf.txt_fees  = (TextView) findViewById(R.id.txtinspectionfees);
			
			TextView companynametxt = (TextView) findViewById(R.id.tvinsurancecompany);
			companynametxt.setText(Html.fromHtml(" "+"Insurance Company "));
			cf.et_companyname  = (TextView) findViewById(R.id.etinsurancecompany);
			
			TextView mailaddresstxt = (TextView) findViewById(R.id.tvmailaddress1);
			mailaddresstxt.setText(Html.fromHtml(cf.redcolor+"Address 1 "));
			cf.et_mailaddress1  = (EditText) findViewById(R.id.etmailaddress1);
			
			TextView mailstatetxt = (TextView) findViewById(R.id.tvmailstate);
			mailstatetxt.setText(Html.fromHtml(cf.redcolor+"State "));
			cf.et_mailstate  = (EditText) findViewById(R.id.etmailstate);
			
			TextView mailaddress2txt = (TextView) findViewById(R.id.tvmailaddress2);
			mailaddress2txt.setText(Html.fromHtml("Address 2 "));
			cf.et_mailaddress2  = (EditText) findViewById(R.id.etmailaddress2);
			
			TextView mailcountytxt = (TextView) findViewById(R.id.tvmailcounty);
			mailcountytxt.setText(Html.fromHtml(cf.redcolor+"County "));
			cf.et_mailcounty  = (EditText) findViewById(R.id.etmailcounty);
			
			TextView mailcitytxt = (TextView) findViewById(R.id.tvmailcity);
			mailcitytxt.setText(Html.fromHtml(cf.redcolor+"City "));
			cf.et_mailcity  = (EditText) findViewById(R.id.etmailcity);
			
			TextView mailziptxt = (TextView) findViewById(R.id.tvmailzip);
			mailziptxt.setText(Html.fromHtml(cf.redcolor+"ZIP "));
			cf.et_mailzip  = (EditText) findViewById(R.id.etmailzip);
			
			
			adapter1 = new ArrayAdapter(PolicyHolder.this,android.R.layout.simple_spinner_item, cf.BI_GBI_year);
		    Y_O_C_spn = (Spinner) findViewById(R.id.PH_Y_O_C_spn);
		    Y_O_C_spn.setAdapter(adapter1);
		    Y_O_C_spn_other = (EditText) findViewById(R.id.PH_Y_O_C_spn_other);
		    Y_O_C_spn.setOnItemSelectedListener(new spinerlisner());
		    TextView Y_O_C_txt = (TextView) findViewById(R.id.PH_Y_O_C_txt);
		    Y_O_C_txt.setText(Html.fromHtml(cf.redcolor+"Year of Home "));
			cf.getInspectorId();
			if (cf.onlinspectionid.equals("18")) {
				cf.et_policyno.setEnabled(true);
				
			} else {
				cf.et_policyno.setEnabled(false);
			
			}
			   final CheckBox cb_email = (CheckBox)findViewById(R.id.chkemail);
			    cb_email.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						cf.chkbool=cf.Checkbox(cb_email);
						if(cf.chkbool==true)
						{
							 mc = 1;
							 cf.et_email.setEnabled(false);
						}
						else
						{
							 mc = 0;
							 cf.et_email.setEnabled(true);
						}
					
					}
				});
			try {
				Cursor c2 = cf.sh_db.rawQuery("SELECT * FROM "
						+ cf.policyholder + " WHERE SH_PH_SRID='" + cf.encode(cf.selectedhomeid)
						+ "' and SH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'", null);
				int rws = c2.getCount();
				c2.moveToFirst();
				if (c2 != null) {
					do {
						cf.et_firstname.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_FirstName"))));
						cf.et_lastname.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_LastName"))));
						cf.et_address1.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_Address1"))));
						cf.et_address2.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_Address2"))));
						cf.et_city.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_City"))));
						cf.et_state.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_State"))));
						cf.et_county.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_County"))));
						cf.et_policyno.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_Policyno"))));
						cf.et_companyname.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_InsuranceCompany"))));
						cf.et_zip.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_Zip"))));
						String Y_O_C=cf.decode(c2.getString(c2.getColumnIndex("SH_YearBuilt")));
						if (cf.decode(c2.getString(c2.getColumnIndex("SH_PH_HomePhone"))).equals("")) {
							cf.et_hmephn.setText("");
						}else{
						cf.et_hmephn.setText(cf.Set_phoneno(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_HomePhone")))));
						}
						if (cf.decode(c2.getString(c2.getColumnIndex("SH_PH_WorkPhone"))).equals("")) {
							cf.et_wrkphn.setText("");
						}else{
						cf.et_wrkphn.setText(cf.Set_phoneno(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_WorkPhone")))));
						}
						if (cf.decode(c2.getString(c2.getColumnIndex("SH_PH_CellPhone"))).equals("")) {
							cf.et_cellphn.setText("");
						}else{
						cf.et_cellphn.setText(cf.Set_phoneno(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_CellPhone")))));
						}
						cf.et_email.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_PH_Email"))));
						cf.txt_fees.setText("$ "+cf.decode(c2.getString(c2.getColumnIndex("SH_PH_Inspectionfees"))));
						
						cf.ScheduledDate=cf.decode(c2.getString(c2.getColumnIndex("SH_Schedule_ScheduledDate")));	
							if (cf.decode(c2.getString(c2.getColumnIndex("SH_PH_EmailChkbx"))).equals("1")) {
							mc=1;
							cb_email.setChecked(true);
							cf.et_email.setEnabled(false);
						} else {
						   mc=0;
							cb_email.setChecked(false);
							cf.et_email.setEnabled(true);
						}
						if(!Y_O_C.trim().equals(""))
						{
							if(getFromArray(Y_O_C,cf.BI_GBI_year))
							 {
								 Y_O_C_spn.setSelection(adapter1.getPosition("Other"));
								 Y_O_C_spn_other.setText(Y_O_C); 
							 }
							 else
							 {
								
								 Y_O_C_spn.setSelection(adapter1.getPosition(Y_O_C));
							 }
						}	 
					} while (c2.moveToNext());
				}
				
		  } catch (Exception e) {
					cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PolicyHolder.this +" "+" in the processing stage of retrieving data from PH table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		  }
			final CheckBox cb_mailing = (CheckBox)findViewById(R.id.chkmailing);
		    cb_mailing.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					cf.chkbool=cf.Checkbox(cb_mailing);
					if(cf.chkbool==true)
					{	
						 ml = 1;
						cf.et_mailaddress1.setText(cf.et_address1.getText().toString());
						cf.et_mailaddress2.setText(cf.et_address2.getText().toString());
						cf.et_mailcity.setText(cf.et_city.getText().toString());
						cf.et_mailstate.setText(cf.et_state.getText().toString());
						cf.et_mailcounty.setText(cf.et_county.getText().toString());
						cf.et_mailzip.setText(cf.et_zip.getText().toString());
						
		  		    }
					else
					{	ml=0;
						cf.et_mailaddress1.setText("");
						cf.et_mailaddress2.setText("");
						cf.et_mailcity.setText("");
						cf.et_mailstate.setText("");
						cf.et_mailcounty.setText("");
						cf.et_mailzip.setText("");
					}
				}
			});
			try {
				Cursor c2 = cf.sh_db.rawQuery("SELECT * FROM "
						+ cf.MailingPolicyHolder + " WHERE SH_ML_PH_SRID='" + cf.encode(cf.selectedhomeid)
						+ "' and SH_ML_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'", null);
				int rws = c2.getCount();
				c2.moveToFirst();
				if (c2 != null) {
					do {
						cf.et_mailaddress1.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_ML_PH_Address1"))));
						cf.et_mailaddress2.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_ML_PH_Address2"))));
						cf.et_mailcity.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_ML_PH_City"))));
						cf.et_mailzip.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_ML_PH_Zip"))));
						cf.et_mailstate.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_ML_PH_State"))));
						cf.et_mailcounty.setText(cf.decode(c2.getString(c2.getColumnIndex("SH_ML_PH_County"))));
						if (cf.decode(c2.getString(c2.getColumnIndex("SH_ML"))).equals("1")) {
							ml=1;
							cb_mailing.setChecked(true);
						} else {
							ml=0;
							cb_mailing.setChecked(false);
						}
					} while (c2.moveToNext());
				}
				
		  } catch (Exception e) {
					cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PolicyHolder.this +" "+" in the processing stage of retrieving data from ML table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		  }
		
	 }
	public void clicker(View v) {
			switch (v.getId()) {

			case R.id.save:
				/*if(cf.strschdate.equals(""))
				{*/
					//cf.ShowToast("You cannot submit PolicyHolder information without scheduling. ", 1);
				//}else{
				Save_PHData();//}
				break;
				  case R.id.home:
						  cf.gohome();
						  break;
				
			}
		}
	private void Save_PHData() {
		// TODO Auto-generated method stub
		/*CHECKING WORKPHONE VALIDATION*/
		if (!cf.et_wrkphn.getText().toString().trim().equals("")) {
			if (cf.PhoneNo_validation(cf.et_wrkphn.getText().toString()) != "Yes") {
				cf.ShowToast("Please enter the Work Phone in 10 digit Number.", 1);
				cf.et_wrkphn.setText("");
				cf.et_wrkphn.requestFocus();
				chkstatus[0] = "false";
			} else {
				chkstatus[0] = "true";
				strwrkphn=cf.newphone;
			}
		} else {
			chkstatus[0] = "true";
		}
		/*CHECKING CELLPHONE VALIDATION*/
		if (!cf.et_cellphn.getText().toString().trim().equals("")) {
			if (cf.PhoneNo_validation(cf.et_cellphn.getText().toString())!= "Yes") {
				cf.ShowToast("Please enter the Cell Phone in 10 digit Number.", 1);
				cf.et_cellphn.setText("");
				cf.et_cellphn.requestFocus();
				chkstatus[1] = "false";
			} else {
				chkstatus[1] = "true";
				strcellphn=cf.newphone;
			}
		} else {
			chkstatus[1] = "true";
		}
      
		/*VALIDATIONS FOR SAVE BUTTON*/
	  //  if (!"".equals(cf.ScheduledDate)) { /*CHECKING SCHEDULE DATE IA AVAILABLE OR NOT*/
	    	if (!"".equals(cf.et_firstname.getText().toString().trim())) {  /*CHECKING FIRSTNAME IS AVAILABLE OR NOT*/
	    		if (!"".equals(cf.et_lastname.getText().toString().trim())) {/*CHECKING LASTNAME IS AVAILABLE OR NOT*/
	    			 if (!"".equals(cf.et_address1.getText().toString().trim())) {/*CHECKING ADDRESS1 IS AVAILABLE OR NOT*/
					        if (!"".equals(cf.et_city.getText().toString().trim())) {/*CHECKING CITY IS AVAILABLE OR NOT*/
					        	 if (!"".equals(cf.et_state.getText().toString().trim())) {/*CHECKING STATE IS AVAILABLE OR NOT*/
					        		 if (!"".equals(cf.et_county.getText().toString().trim())) {/*CHECKING COUNTY IS AVAILABLE OR NOT*/
					        	        if (!"".equals(cf.et_zip.getText().toString().trim())) {/*CHECKING ZIP IS AVAILABLE OR NOT*/
						                  if (cf.et_zip.getText().length() == 5) { /*CHECKING LENGTH OF ZIPCODE*/
						                	 if (!"".equals(cf.et_hmephn.getText().toString().trim())) { /*CHECKING HOMEPHONE IS AVAILABLE OR NOT*/
								                    if (cf.PhoneNo_validation(cf.et_hmephn.getText().toString().trim()).equals("Yes")) { /*VALIDATION FOR HOME PHONE*/
								                    	strhmephn=cf.newphone;
								                    	if (cf.et_email.getText().toString().trim().equals("")&& (mc == 0)) { /*VALIDATION FOR EMAIL*/
															cf.ShowToast("Please enter the Email.", 1);
										                	cf.et_email.requestFocus();
															chkstatus[2] = "false";
														 } else {
															if (mc == 1) {
																chkstatus[2] = "true";
																Mailing_Validation();
															} else if (mc == 0) {
																if (cf.Email_Validation(cf.et_email.getText().toString().trim()) != "Yes") {
																	cf.ShowToast("Please enter the valid Email.", 1);
																	cf.et_email.setText("");
																	cf.et_email.requestFocus();
																	chkstatus[2] = "false";
																} else {
																	chkstatus[2] = "true";
																	Mailing_Validation();
																}
															}

														}
												    } else {
														cf.ShowToast("Please enter the Home Phone in 10 digit Number.", 1);
													    cf.et_hmephn.setText("");
													    cf.et_hmephn.requestFocus();
													}
											} else {
												cf.ShowToast("Please enter the Home Phone Number.", 1);
												cf.et_hmephn.requestFocus();
											}
                                      } else {
											cf.ShowToast("Please enter the valid ZipCode.",1);
											cf.et_zip.setText("");
											cf.et_zip.requestFocus();
										}
				        	 	} else {
									cf.ShowToast("Please enter the ZipCode.", 1);
									cf.et_zip.requestFocus();
								}
			        		  } else {
									cf.ShowToast("Please enter the County.", 1);
									cf.et_zip.requestFocus();
								}
			        	        } else {
									cf.ShowToast("Please enter the State.", 1);
									cf.et_zip.requestFocus();
							}
                             } else {
								 cf.ShowToast("Please enter the City.",1);
								 cf.et_city.requestFocus();
						}
					} else {
						cf.ShowToast("Please enter the Address.", 1);
						cf.et_address1.requestFocus();
					}
             	} else {
					cf.ShowToast("Please enter the Last Name.", 1);
				    cf.et_lastname.requestFocus();
				}
         	}
            else {
				cf.ShowToast("Please enter the First Name.", 1);
				cf.et_firstname.requestFocus();
			}

	    /*} else {
			cf.ShowToast("You cannot submit Policy Holder Information without Scheduling.", 1);
			
		}*/
	}
	public void Mailing_Validation()
	{
		if (!"".equals(cf.et_mailaddress1.getText().toString().trim())) {  /*CHECKING MAILING ADRESS1 IS AVAILABLE OR NOT*/
			// if (!"".equals(cf.et_mailaddress2.getText().toString())) {  /*CHECKING MAILING ADRESS2 IS AVAILABLE OR NOT*/
				 if (!"".equals(cf.et_mailcity.getText().toString().trim())) {  /*CHECKING MAILING CITY IS AVAILABLE OR NOT*/
					 if (!"".equals(cf.et_mailstate.getText().toString().trim())) {  /*CHECKING MAILING STATE IS AVAILABLE OR NOT*/
						 if (!"".equals(cf.et_mailcounty.getText().toString().trim())) {  /*CHECKING MAILING COUNTY IS AVAILABLE OR NOT*/
							  if (!"".equals(cf.et_mailzip.getText().toString().trim())) {/*CHECKING ZIP IS AVAILABLE OR NOT*/
					                if (cf.et_mailzip.getText().length() == 5) { /*CHECKING LENGTH OF ZIPCODE*/
					                        db_update();
					                } else {
										cf.ShowToast("Please enter the valid ZipCode.",1);
										cf.et_mailzip.setText("");
										cf.et_mailzip.requestFocus();
									}
			        	 	} else {
								cf.ShowToast("Please enter the ZipCode.", 1);
								cf.et_mailzip.requestFocus();
							}
						 }
				         else {
								cf.ShowToast("Please enter the Mailing County.", 1);
								cf.et_mailcounty.requestFocus();
							}
					 }
			         else {
							cf.ShowToast("Please enter the Mailing State.", 1);
							cf.et_mailstate.requestFocus();
						}
				 }
		         else {
						cf.ShowToast("Please enter the Mailing City.", 1);
						cf.et_mailcity.requestFocus();
					}
			/* }
	         else {
					cf.ShowToast("Please enter the Mailing Address2.", 1);
					cf.et_mailaddress2.requestFocus();
				}*/
		 }
         else {
				cf.ShowToast("Please enter the Mailing Address1.", 1);
				cf.et_mailaddress1.requestFocus();
			}
	}
	private void db_update() {
		// TODO Auto-generated method stub
		if (chkstatus[0] == "true" && chkstatus[1] == "true"
				&& chkstatus[2] == "true" ) {
			if(!Y_O_C_spn.getSelectedItem().toString().equals("Select") && (!Y_O_C_spn.getSelectedItem().toString().equals("Other") || !Y_O_C_spn_other.getText().toString().trim().equals("")))
				{
				
			 String Y_O_C=(Y_O_C_spn.getSelectedItem().toString().equals("Other"))? Y_O_C_spn_other.getText().toString():Y_O_C_spn.getSelectedItem().toString();
				try{
					cf.sh_db.execSQL("UPDATE " + cf.policyholder
							+ " SET SH_PH_FirstName='" + cf.encode(cf.et_firstname.getText().toString())
							+ "',SH_PH_LastName='"
							+ cf.encode(cf.et_lastname.getText().toString())
							+ "',SH_PH_Address1='"
							+ cf.encode(cf.et_address1.getText().toString())
							+ "',SH_PH_Address2='"
							+ cf.encode(cf.et_address2.getText().toString()) + "',SH_PH_City='"
							+ cf.encode(cf.et_city.getText().toString()) + "',SH_PH_State='"
							+ cf.encode(cf.et_state.getText().toString()) + "',SH_PH_County='"
							+ cf.encode(cf.et_county.getText().toString()) + "',SH_PH_Policyno='"
							+ cf.encode(cf.et_policyno.getText().toString())
							+ "',SH_PH_InsuranceCompany='" + cf.encode(cf.et_companyname.getText().toString())
							+ "',SH_PH_Zip='"
							+ cf.encode(cf.et_zip.getText().toString())
							+ "',SH_PH_HomePhone='" + cf.encode(strhmephn)
							+ "',SH_PH_WorkPhone='" + cf.encode(strwrkphn)
							+ "',SH_PH_CellPhone='" + cf.encode(strcellphn)
							+ "',SH_PH_Email='"
							+ cf.encode(cf.et_email.getText().toString()) + "',SH_PH_EmailChkbx='"
							+ cf.encode(Integer.toString(mc)) + "',SH_YearBuilt='"+cf.encode(Y_O_C)+"' WHERE SH_PH_SRID ='" + cf.encode(cf.selectedhomeid)
							+ "'");
					/*CHECKING MAILING ADDRESS TABLE*/
					Cursor c2 = cf.sh_db.rawQuery("SELECT * FROM "
							+ cf.MailingPolicyHolder + " WHERE SH_ML_PH_SRID='" + cf.encode(cf.selectedhomeid)
							+ "'", null);
					int rws = c2.getCount();
					if (rws == 0) {
							cf.sh_db.execSQL("INSERT INTO "
									+ cf.MailingPolicyHolder
									+ " (SH_ML_PH_SRID,SH_ML_PH_InspectorId,SH_ML,SH_ML_PH_Address1,SH_ML_PH_Address2,SH_ML_PH_City,SH_ML_PH_Zip,SH_ML_PH_State,SH_ML_PH_County)"
									+ "VALUES ('"+cf.encode(cf.selectedhomeid)+"','"+cf.encode(cf.Insp_id)+"','"+cf.encode(Integer.toString(ml))+"','"+cf.encode(cf.et_mailaddress1.getText().toString())+"','"
								    + cf.encode(cf.et_mailaddress2.getText().toString())+"','"+cf.encode(cf.et_mailcity.getText().toString())+"','"
								    + cf.encode(cf.et_mailzip.getText().toString())+"','"+cf.encode(cf.et_mailstate.getText().toString())+"','"
								    + cf.encode(cf.et_mailcounty.getText().toString())+"')");
					}
					else
					{
						cf.sh_db.execSQL("UPDATE " + cf.MailingPolicyHolder
								+ " SET SH_ML='"+ml+"',SH_ML_PH_Address1='" + cf.encode(cf.et_mailaddress1.getText().toString())
								+ "',SH_ML_PH_Address2='"
								+ cf.encode(cf.et_mailaddress2.getText().toString())
								+ "',SH_ML_PH_City='"
								+ cf.encode(cf.et_mailcity.getText().toString())
								+ "',SH_ML_PH_Zip='"
								+ cf.encode(cf.et_mailzip.getText().toString()) + "',SH_ML_PH_State='"
								+ cf.encode(cf.et_mailstate.getText().toString()) + "',SH_ML_PH_County='"
								+ cf.encode(cf.et_mailcounty.getText().toString()) + "' WHERE SH_ML_PH_SRID ='" + cf.encode(cf.selectedhomeid)
								+ "'");
					}
					cf.ShowToast("Policy Holder Information has been updated", 1);
				}
				
				catch(Exception e)
				{
					cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PolicyHolder.this +" "+" in the processing stage of updating data into PH table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}		
			}else
			{
				if(Y_O_C_spn.getSelectedItem().toString().equals("Other") && Y_O_C_spn_other.getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter  other text for year of home ", 1);
				}
				else
				{
					cf.ShowToast("Please select year of home ", 1);
				}
			}
		}
	}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				Intent  myintent = new Intent(PolicyHolder.this,SinkholeSurvey.class);
			 	myintent.putExtra("InspectionType", cf.onlinspectionid);
				myintent.putExtra("status", cf.onlstatus);
				myintent.putExtra("homeid", cf.selectedhomeid);
				startActivity(myintent);
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
				switch (resultCode) {
	 			case 0:
	 				break;
	 			case -1:
	 				try {

	 					String[] projection = { MediaStore.Images.Media.DATA };
	 					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
	 							null, null, null);
	 					int column_index_data = cursor
	 							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	 					cursor.moveToFirst();
	 					String capturedImageFilePath = cursor.getString(column_index_data);
	 					cf.showselectedimage(capturedImageFilePath);
	 				} catch (Exception e) {
	 					
	 				}
	 				
	 				break;

	 		}

	 	}
	 class spinerlisner implements OnItemSelectedListener
	 {

	 	@Override
	 	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
	 			long arg3) {
	 		// TODO Auto-generated method stub
	 		
	 			 String s=Y_O_C_spn.getSelectedItem().toString();
	 			 if(s.equals("Other"))
	 			 {
	 				Y_O_C_spn_other.setVisibility(arg1.VISIBLE);
	 			 }
	 			 else
	 			 {
	 				Y_O_C_spn_other.setVisibility(arg1.GONE);
	 			 }
	 				 
	 			 		
	 		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			Y_O_C_spn_other.setVisibility(arg0.GONE);
		}
	 		 
	 	}
	 public boolean getFromArray(String sH_BI_GBI_YOC2, String[] bI_GBI_year)
	 {
	 	for(int i=0;i < bI_GBI_year.length;i++)
	 	 {
	 		 if(sH_BI_GBI_YOC2.equals(bI_GBI_year[i]))
	 		 {
	 			 return false;
	 		 }
	 	 }
	 	return true;
	 }
}

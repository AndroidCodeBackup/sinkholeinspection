package idsoft.inspectiondepot.sinkholeinspection;

import idsoft.inspectiondepot.sinkholeinspection.Observation1.Touch_Listener;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class FeedbackDocuments extends Activity{
	private static final int SELECT_PICTURE = 0;
	private static final int SELECT_PDF = 1;
	private static final int CAPTURE_PICTURE_INTENT = 0;
	private ListView lv1;View v2;
    private static final String TAG = null;
	protected static final int visibility = 0;
	ArrayList<String> listItems=new ArrayList<String>();
    private File[] imagelist;
    String[] pdflist=null;
	int o=0,documentothertextsupp=0,documentothertextoff=0;
	Cursor curfeed1,curfeed2;
	ListAdapter adapter,adapter11;ArrayAdapter<String> adapter1,adapter2;
	View v1;File images;
	ListView doclst,offlst;
	String[] items = {"gallery","pdf"};
	String[] items1 = {"pdf"};
	static String[] docnamearr;static String[] docidarr; static String[] offdocidarr;
	static String[] docpatharr;
	static String[] offnamearr;
	TableRow tbl;
	static String[] offpatharr;
	String[] array_who,array_doc;String selected_id,offselected_id;
	Uri mCapturedImageURI;int ImgOrder =0;
	String doc_id,offdoc_id,strdoctitle,strdoctitle1,strdoctitleoff,strdoctitlesupp,homeid,strpresatins,strfbcoment,strdocname,selectedImagePath="empty",selectedImagePath1,capturedImageFilePath,
	       docname,docpath,offname,offpath,InspectionType,status,inspectiontype,picname,strother="";
	RadioButton cusyrd,cusnrd,insyrd,insnrd,insntrd,infyrd,infnrd,infntrd;
	int isCSC=0,isInsavail=0,isManf=0,rws,t,c,docrws,offrws,rws1,rws2,flagstr,inspid,value,Count,feedrws1,feedrws2;
	EditText edfbcoment,offedittitle,docpathedit,offpathedit,othrtxt,othrtxtsupp,othrtxtoff; 
	android.text.format.DateFormat df;CharSequence cd,md;Cursor c11;
	LinearLayout offtbl,doctbllst,offtbllst;
	Button docbrws,offbrws,docupd,offupd;
	private static final String mbtblInspectionList = "Retail_inpgeneralinfo";
	Spinner s,s1,s2; TextView policyholderinfo;
	ScrollView pscr,cscr;
	CommonFunctions cf;
	private int ii;
	public TextView tv1,tv2,tv3,tv4;
	private String[] pdfsample;
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this); 
		  
		   
		   /** create table **/
		   cf.Create_Table(13);
		   cf.Create_Table(14);
		   /** create table **/
		   Bundle bunQ1extras1 = getIntent().getExtras();	        
			  if (bunQ1extras1 != null) {
				cf.selectedhomeid=homeid = bunQ1extras1.getString("homeid");	
				cf.onlinspectionid = bunQ1extras1.getString("InspectionType");
			    cf.onlstatus = bunQ1extras1.getString("status");
				
			  }
			  setContentView(R.layout.feedbackdocument);
			   
		   /** menu **/
		    cf.getInspectorId();
		    cf.getDeviceDimensions();
		   LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
		   mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 6, 0,cf));
	       /** menu completed **/ 
	        /** get the date for created and modified*/
	           df = new android.text.format.DateFormat();
			   cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
			   md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
			   
			    ScrollView scr=(ScrollView)findViewById(R.id.scr);
			   scr.smoothScrollTo(0,400);
			   scr.setSmoothScrollingEnabled(true);
			  HorizontalScrollView hscr=(HorizontalScrollView)findViewById(R.id.HorizontalScrollView01);
			   hscr.smoothScrollTo(1000,0);
				cusyrd = (RadioButton)findViewById(R.id.customeryes);
				cusyrd.setOnClickListener(OnClickListener);
				cusnrd = (RadioButton)findViewById(R.id.customerno);
				cusnrd.setOnClickListener(OnClickListener);
				insyrd = (RadioButton)findViewById(R.id.insyes);
				insyrd.setOnClickListener(OnClickListener);
				insnrd = (RadioButton)findViewById(R.id.insno);
				insnrd.setOnClickListener(OnClickListener);
				insntrd = (RadioButton)findViewById(R.id.insnot);
				insntrd.setOnClickListener(OnClickListener);
				infyrd = (RadioButton)findViewById(R.id.infyes);
				infyrd.setOnClickListener(OnClickListener);
				infnrd = (RadioButton)findViewById(R.id.infno);
				infnrd.setOnClickListener(OnClickListener);
				infntrd = (RadioButton)findViewById(R.id.infnot);
				infntrd.setOnClickListener(OnClickListener);
				 s = (Spinner)findViewById(R.id.Spinner01);
				 tbl=(TableRow)findViewById(R.id.tableLayout31);
					 
				 othrtxt=(EditText)findViewById(R.id.other);
				 othrtxtsupp=(EditText)findViewById(R.id.othersupp);
				 othrtxtsupp.setOnTouchListener(new Touch_Listener(2));
				 othrtxtoff=(EditText)findViewById(R.id.otheroff);
				 othrtxtoff.setOnTouchListener(new Touch_Listener(3));
				
				 s1 = (Spinner)findViewById(R.id.Spinner02);
				 s2 = (Spinner)findViewById(R.id.Spinner03);
				edfbcoment = (EditText)findViewById(R.id.comment);
				 cf.SQ_TV_type1 = (TextView) findViewById(R.id.SQ_TV_type1);
				 
				  /** FOR SHOW THE LIMIT TO THE USER COMMENT STARTS**/
			       
			        cf.SQ_ED_type1_parrant = (LinearLayout) findViewById(R.id.SQ_ED_type1_parrant);
			        cf.SQ_ED_type1 = (LinearLayout) findViewById(R.id.SQ_ED_type1);
			        /** FOR SHOW THE LIMIT TO THE USER COMMENT ENDS**/
			        
				edfbcoment.setOnTouchListener(new Touch_Listener(1));
				edfbcoment.addTextChangedListener(new FI_watcher());
				offtbl = (LinearLayout)findViewById(R.id.tableLayout13);
				docbrws = (Button)findViewById(R.id.browsetxt);
				offbrws = (Button)findViewById(R.id.offbrowsetxt);
				docpathedit = (EditText)findViewById(R.id.pathtxt);
				docpathedit.setOnTouchListener(new Touch_Listener(4));
				offpathedit = (EditText)findViewById(R.id.offpathtxt);
				offpathedit.setOnTouchListener(new Touch_Listener(5));
				docupd = (Button)findViewById(R.id.updtxt);
				offupd = (Button)findViewById(R.id.offupdtxt);
				doclst = (ListView)findViewById(R.id.ListView01);
				offlst = (ListView)findViewById(R.id.ListView02);
				doctbllst= (LinearLayout)findViewById(R.id.tableLayout15);
				offtbllst= (LinearLayout)findViewById(R.id.tableLayout17);
			
			    array_who=new String[4];
			    array_who[0]="Owner";
			    array_who[1]="Representative";
			    array_who[2]="Agent";
			    array_who[3]="Other";
			    
			    array_doc=new String[12];
			    array_doc[0]="--Select--";
			    array_doc[1]="Acknowledgement Form";
			    array_doc[2]="CSE Form";
			    array_doc[3]="Insurance Paperwork";
			    array_doc[4]="Contractors Repair Documents";
			    array_doc[5]="Other Information";
			    array_doc[6]="Permit Information";
			    array_doc[7]="Sketch";
			    array_doc[8]="Building Permit";
			    array_doc[9]="Property Appraisal Information";
			    array_doc[10]="Field Inspection Report";
			    array_doc[11]="Field Notes";
			    Spinner s1 = (Spinner)findViewById(R.id.Spinner02);
			    Spinner s2 = (Spinner)findViewById(R.id.Spinner03); Spinner s = (Spinner)findViewById(R.id.Spinner01);
	         
				 adapter2 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, array_who);
				 adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		        s.setAdapter(adapter2);
		        
		         adapter1 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, array_doc);
		        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		        s1.setAdapter(adapter1);
		        
		        s2.setAdapter(adapter1);
		        s.setOnItemSelectedListener(new MyOnItemSelectedListenerdata1());
		        s1.setOnItemSelectedListener(new MyOnItemSelectedListenerdata2());
		        s2.setOnItemSelectedListener(new MyOnItemSelectedListenerdata3());
		        
		        tv1=(TextView) findViewById(R.id.FB_tv1);
		        tv2=(TextView) findViewById(R.id.FB_tv2);
		        tv3=(TextView) findViewById(R.id.FB_tv3);
		        tv4=(TextView) findViewById(R.id.FB_tv4);
		        
		        tv1.setText(Html.fromHtml(cf.redcolor+" "+" Customer Service Evaluation Form Completed and Signed by Homeowner"));
		        tv2.setText(Html.fromHtml(cf.redcolor+" "+" Who Was Present at Inspection"));
		        tv3.setText(Html.fromHtml(cf.redcolor+" "+" Did Homeowner have Insurance Paperwork available?"));
		        tv4.setText(Html.fromHtml(cf.redcolor+" "+" Did Homeowner have Manufacture Information ready?"));
		       
		        /** retrive the value form the data base and shoe in the form **/
		        SetValueFI();
		        
		        show_list();
		        
				showoffice_list();
				
				 offlst.setOnItemClickListener(new FI_clicker());
				 
				setaddapters();
				
	}	    
	
	class Touch_Listener implements OnTouchListener
	{
		   public int type;
		   Touch_Listener(int type)
			{
				this.type=type;
				
			}
		    @Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	if(this.type==1)
				{
		    		cf.setFocus(edfbcoment);
				}
		    	else if(this.type==2)
				{
		    		cf.setFocus(othrtxtsupp);
				}
		    	else if(this.type==3)
				{
		    		cf.setFocus(othrtxtoff);
				}
		    	else if(this.type==4)
				{
		    		cf.setFocus(docpathedit);
				}
		    	else if(this.type==5)
				{
		    		cf.setFocus(offpathedit);
				}
		    	
				return false;
			}
		 
	}
	private void SetValueFI() {
		// TODO Auto-generated method stub
	    try {
			   Cursor	c1 = cf.sh_db.rawQuery("SELECT * FROM "+ cf.FeedBackInfoTable	+ " WHERE SH_FI_Srid='"+cf.selectedhomeid+"'",null);
            rws = c1.getCount();
            if(rws!=0)
			 {
			
				 int Column1=c1.getColumnIndex("SH_FI_IsCusServiceCompltd"); 
 		     	 int Column2=c1.getColumnIndex("SH_FI_PresentatInspection"); 
 		     	 int Column3=c1.getColumnIndex("SH_FI_IsInspectionPaperAvbl");
 		     	 int Column4=c1.getColumnIndex("SH_FI_IsManufacturerInfo");
 		     	 int Column5=c1.getColumnIndex("SH_FI_FeedbackComments");
 		     	 int Column6=c1.getColumnIndex("SH_FI_othertxt");
 		     	 c1.moveToFirst();
 		     	 if(c1!=null)
 		     	 {
 		     		do{
 		     			 String csc=cf.decode(c1.getString(Column1)); 
 		     			 String pati=cf.decode(c1.getString(Column2));
 		     			 String ipa=cf.decode(c1.getString(Column3));
 		     			 String imi=cf.decode(c1.getString(Column4));
 		     			 String fc=cf.decode(c1.getString(Column5));
 		     			 if(csc.equals("1"))
 						 {
 							 cusyrd.setChecked(true);cusnrd.setChecked(false);isCSC=1;
 						 }
 						 else
 						 {
 							 cusyrd.setChecked(false);cusnrd.setChecked(true);isCSC=2;
 						 }
 		     			 int spinnerPosition = adapter2.getPosition(pati);
 				         s.setSelection(spinnerPosition);
 		     			 if(ipa.equals("1"))
 						 {
 							 insyrd.setChecked(true);insnrd.setChecked(false);insntrd.setChecked(false);
 							 isInsavail=1;
 						 }
 						 else if(ipa.equals("2"))
 						 {
 							 insyrd.setChecked(false);insnrd.setChecked(true);insntrd.setChecked(false);
 							 isInsavail=2;
 						 }
 						 else if(ipa.equals("3"))
 						 {
 							 insyrd.setChecked(false);insnrd.setChecked(false);insntrd.setChecked(true);
 							 isInsavail=3;
 						 }
 		     			 if(imi.equals("1"))
						     {
 		     				infyrd.setChecked(true);infnrd.setChecked(false);infntrd.setChecked(false);
 		     				isManf=1;
						     }
 		     			 else if(imi.equals("2"))
						     {
  		     				infyrd.setChecked(false);infnrd.setChecked(true);infntrd.setChecked(false);
  		     				isManf=2;
 						 }
 		     			 else if(imi.equals("3"))
						     {
 		     				infyrd.setChecked(false);infnrd.setChecked(false);infntrd.setChecked(true);
 		     				isManf=3;
						     }
 		     			edfbcoment.setText(fc);
 		     			if(c1.getString(c1.getColumnIndex("SH_FI_othertxt")).equals(""))
 		     			{
 		     				
 		     			}
 		     			else
 		     			{
 		     				othrtxt.setText(cf.decode(c1.getString(c1.getColumnIndex("SH_FI_othertxt"))));
 		     			}
 		     		}while(c1.moveToNext());
 		     	 }c1.close(); 
			 }
		 }
		 catch(Exception e)
		 {
			 Log.i(TAG,"error= "+e.getMessage());
		 }
	}

	private void show_list()
	{
		docname="";docpath="";doc_id="";
		 try {
			   Cursor	c2 =  cf.sh_db.rawQuery("SELECT * FROM "+  cf.FeedBackDocumentTable	+ " WHERE SH_FI_SRID='"+cf.selectedhomeid+"' and SH_FI_IsOfficeUse=0",null);
			   docrws = c2.getCount(); 
			   if(docrws!=0)
			   {
				   int Column1=c2.getColumnIndex("SH_FI_FileName");
	     	        int Column2=c2.getColumnIndex("SH_FI_DocumentTitle");
	     	        c2.moveToFirst();
	     	        int i=0;
	     	        docnamearr=new String[docrws];
	     	        docpatharr=new String[docrws];
	     	        docidarr=new String[docrws];
			     	if(c2!=null)
			     	{
			     		do{
			     			docnamearr[i]=cf.decode(c2.getString(Column1));
			     			docpatharr[i]=cf.decode(c2.getString(Column2));
			     			docidarr[i]=cf.decode(c2.getString(c2.getColumnIndex("SH_FI_DocumentId")));
			     			// docnamearr = docname.split("~");
			     			// docpatharr=docpath.split("~"); 
			     			// docidarr=doc_id.split("~");
		     				doctbllst.setVisibility(visibility);
		     				ColorDrawable sage = new ColorDrawable(this.getResources().getColor(R.color.sage));
		     				doclst.setDivider(sage);
		     				doclst.setVisibility(visibility);doclst.setDividerHeight(2);
		     			   // doclst.setAdapter(new EfficientAdapter(this));
		     			   
		     		i++;
                     }while(c2.moveToNext());
			     		TableRow.LayoutParams mParam = new TableRow.LayoutParams((int)(900),(int)(docrws * 50)+20);
			            doclst.setLayoutParams(mParam);
			            doclst.setAdapter(new EfficientAdapter(this));
	              }
			   }
			   else
			   {
				   doclst.setVisibility(v2.GONE);doclst.setVisibility(v2.GONE);
			   }
       }
		 catch(Exception e)
		 {
			 Log.i(TAG,"error= show list "+e.getMessage());
		 }
		 doclst.setOnItemClickListener(new OnItemClickListener() {
			  public void onItemClick(AdapterView<?> a, View v, int position, long id) {
			      final String selarr = docnamearr[position];
			      selected_id=docidarr[position];
			      final Dialog dialog = new Dialog(FeedbackDocuments.this);
		            dialog.setContentView(R.layout.maindialog);
		            dialog.setTitle(docpatharr[position]);
		            dialog.setCancelable(true);
		            final ImageView img = (ImageView) dialog.findViewById(R.id.ImageView01);
		            EditText ed1 = (EditText) dialog.findViewById(R.id.TextView01);
		            ed1.setVisibility(v.GONE);img.setVisibility(v.GONE);
		            
		            Button button_close = (Button) dialog.findViewById(R.id.Button01);
		    		button_close.setText("Close");//button_close.setVisibility(v2.GONE);
		    		
		            Button button_d = (Button) dialog.findViewById(R.id.Button03);
		    		button_d.setText("Delete");
		    		button_d.setVisibility(v.VISIBLE);
		    		
		    		final Button button_view = (Button) dialog.findViewById(R.id.Button02);
		    		button_view.setText("View");
		    		button_view.setVisibility(v.VISIBLE);
		    		button_d.setOnClickListener(new OnClickListener() {
			            public void onClick(View v) {
			            
			            AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackDocuments.this);
			   			builder.setMessage("Are you sure you want to delete?")
		   			       .setCancelable(false)
		   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		   			           public void onClick(DialogInterface dialog, int id) {
		   			        	 cf.sh_db.execSQL("Delete From " +  cf.FeedBackDocumentTable + "  WHERE SH_FI_SRID ='" + cf.selectedhomeid + "' and SH_FI_DocumentId='"+selected_id+"' and SH_FI_IsOfficeUse=0");
		   			        	cf.ShowToast("Document has been deleted sucessfully.",1);
  	   					   show_list();
		   			           }
		   			       })
		   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
		   			           public void onClick(DialogInterface dialog, int id) {
		   			                dialog.cancel();
		   			           }
		   			       });
		   			 builder.show();
		            
		   			 dialog.cancel(); 
	            }
   		           	
			  	 });
		    		button_view.setOnClickListener(new OnClickListener() {
			            public void onClick(View v) {
			            	if(selarr.endsWith(".pdf"))
						     {
			            		img.setVisibility(v2.GONE);
			            		String tempstr ;
			            		if(selarr.contains("file://"))
			    		    	{
			    		    		 tempstr = selarr.replace("file://","");		    		
			    		    	}
			            		else
			            		{
			            			tempstr = selarr;
			            		}
			            		 File file = new File(tempstr);
							 
				                 if (file.exists()) {
				                	Uri path = Uri.fromFile(file);
				                    Intent intent = new Intent(Intent.ACTION_VIEW);
				                    intent.setDataAndType(path, "application/pdf");
				                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				 
				                    try {
				                        startActivity(intent);
				                    } 
				                    catch (ActivityNotFoundException e) {
				                    	cf.ShowToast("No application available to view PDF.",1);
				                       
				                    }
				               
				                }
						     }
			            	else
			            	{
			            		Bitmap bitmap2=cf.ShrinkBitmap(selarr,250,250);
			            		BitmapDrawable bmd2 = new BitmapDrawable(bitmap2);
		    					img.setImageDrawable(bmd2);
		    					button_view.setVisibility(View.GONE);
		    					img.setVisibility(v2.VISIBLE);
			            		
			            	}
			            }
		    		});
		    		button_close.setOnClickListener(new OnClickListener() {
			            public void onClick(View v) {
			            	dialog.cancel();
			            }
		    		});
		    		dialog.show();

		  }
			  });
		
	}
	class FI_clicker implements OnItemClickListener
	{

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			// TODO Auto-generated method stub
			  final String offselarr = offnamearr[position];
		        offselected_id=offdocidarr[position];
		        final Dialog dialog = new Dialog(FeedbackDocuments.this);
	            dialog.setContentView(R.layout.maindialog);
	            dialog.setTitle(offpatharr[position]);  
	            dialog.setCancelable(true);
	            final ImageView img = (ImageView) dialog.findViewById(R.id.ImageView01);
	            EditText ed1 = (EditText) dialog.findViewById(R.id.TextView01);
	            ed1.setVisibility(v1.GONE);img.setVisibility(v1.GONE);
	            Button button_close = (Button) dialog.findViewById(R.id.Button01);
	    		button_close.setText("Close");//button_close.setVisibility(v2.GONE);
	            Button button_d = (Button) dialog.findViewById(R.id.Button03);
	    		button_d.setText("Delete");
	    		button_d.setVisibility(v2.VISIBLE);
	    		final Button button_view = (Button) dialog.findViewById(R.id.Button02);
	    		button_view.setText("View");
	    		button_view.setVisibility(v2.VISIBLE);
	    		button_d.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		            
		            AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackDocuments.this);
		   			builder.setMessage("Are you sure, Do you want to delete?")
	   			       .setCancelable(false)
	   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	   			           public void onClick(DialogInterface dialog, int id) {
	   			        	   cf.sh_db.execSQL("Delete From " +cf. FeedBackDocumentTable + "  WHERE SH_FI_SRID ='" + cf.selectedhomeid + "' and SH_FI_DocumentId='"+offselected_id+"' and SH_FI_IsOfficeUse=1");
	   			        	cf.ShowToast("Document has been deleted sucessfully.",1);
	   			        	
	   					   
	   					    showoffice_list();
	   			           }
	   			       })
	   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
	   			           public void onClick(DialogInterface dialog, int id) {
	   			                dialog.cancel();
	   			           }
	   			       });
	   			 builder.show();
	            
	   			 dialog.cancel();
          }
		           	
		  	 });
	    		button_view.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		            	if(offselarr.equals(""))
		            	{
		            		img.setVisibility(v2.VISIBLE);
		            	}
		            	else if(offselarr.endsWith(".pdf"))
					     {
		            		img.setVisibility(v2.GONE);
		            		String tempstr ;
		            		if(offselarr.contains("file://"))
		    		    	{
		    		    		 tempstr = offselarr.replace("file://","");		    		
		    		    	}
		            		else
		            		{
		            			tempstr = offselarr;
		            		}
		            		File file = new File(tempstr);
						 
			                 if (file.exists()) {
			                	Uri path = Uri.fromFile(file);
			                    Intent intent = new Intent(Intent.ACTION_VIEW);
			                    intent.setDataAndType(path, "application/pdf");
			                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			 
			                    try {
			                        startActivity(intent);
			                    } 
			                    catch (ActivityNotFoundException e) {
			                    	cf.ShowToast("No Application available to view PDF.",1);
			                       
			                    }
			               
			                }
					     }
		            	else
		            	{
		            		Bitmap bitmap2=cf.ShrinkBitmap(offselarr,250,250);
		            		BitmapDrawable bmd2 = new BitmapDrawable(bitmap2);
		            		img.setImageDrawable(bmd2);
		            		button_view.setVisibility(View.GONE);
	    					img.setVisibility(v2.VISIBLE);
		            		
		            	}
		            	
		            }
	    		});
	    		button_close.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		            	dialog.cancel();
		            }
	    		});
	    		dialog.show();
		     
		  
			
		}
		
	}

	
	private void showoffice_list()
	{
		try {
			   Cursor	c3 =  cf.sh_db.rawQuery("SELECT * FROM "+  cf.FeedBackDocumentTable	+ " WHERE SH_FI_SRID='"+cf.selectedhomeid+"' and SH_FI_IsOfficeUse=1",null);
			   offrws = c3.getCount();
			   if(offrws!=0)
			   {
				  int Column1=c3.getColumnIndex("SH_FI_FileName");
	     	       int Column2=c3.getColumnIndex("SH_FI_DocumentTitle");
	     	      c3.moveToFirst();
	     	    offnamearr= new String[c3.getCount()];
	     	   offpatharr= new String[c3.getCount()];
	     	  offdocidarr= new String[c3.getCount()];
			     	if(c3!=null)
			     	{int i=0;
			     		do{
			     			offnamearr[i] = cf.decode(c3.getString(Column1));
			     			 offpatharr[i]=cf.decode(c3.getString(Column2)); 
			     			offdocidarr[i]=cf.decode(c3.getString(c3.getColumnIndex("SH_FI_DocumentId")));
			     			
			     			offtbllst.setVisibility(visibility);
		     			    ColorDrawable sage = new ColorDrawable(this.getResources().getColor(R.color.sage));
			     			offlst.setDivider(sage);
			     		    offlst.setVisibility(visibility);offlst.setDividerHeight(2);
		     			   // offlst.setAdapter(new EfficientAdapter1(this));
		     			    
		     			    i++;
                    }while(c3.moveToNext());
			     		TableRow.LayoutParams mParam = new TableRow.LayoutParams((int)(900),(int)((i*50)+20));
			     		offlst.setLayoutParams(mParam);
			            offlst.setAdapter(new EfficientAdapter1(this));
	       }
			   }
			   else
			   {
				    offtbllst.setVisibility(v2.GONE);offlst.setVisibility(v2.GONE);
			   }
      }
		 catch(Exception e)
		 {
			 Log.i(TAG,"error lastr= "+e.getMessage());
		 }
		
	}	
    public static class EfficientAdapter extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;String j;

		public EfficientAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
		}

		public int getCount() {
			int arrlen = 0;
			arrlen = docpatharr.length;
			

			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {

				holder = new ViewHolder();

				convertView = mInflater.inflate(R.layout.listview, null);
				holder.text2 = (ImageView) convertView
						.findViewById(R.id.TextView02);
				holder.text3 = (TextView) convertView
						.findViewById(R.id.TextView03);
				holder.text4 = (TextView) convertView
				.findViewById(R.id.TextView04);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {

				if (docpatharr[position].contains("null")
						|| docnamearr[position].contains("null")||docidarr[position].contains("null")) {
					docpatharr[position] = docpatharr[position].replace("null", "");
					docnamearr[position] = docnamearr[position].replace("null", "");
				
					docidarr[position] = docidarr[position].replace("null", "");
				
				}
				holder.text2.setBackgroundResource(R.drawable.allfilesicon);

				  holder.text3.setText(docpatharr[position]);
				  holder.text4.setText(docnamearr[position]);
				

			} catch (Exception e) {
				Log.i(TAG, "error:  efficiant" + e.getMessage());
			}

			return convertView;
		}

		static class ViewHolder {
			TextView text3,text4;
			ImageView text2;

		}

	}
	public static class EfficientAdapter1 extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;String j;

		public EfficientAdapter1(Context context) {
			mInflater = LayoutInflater.from(context);
		}

		public int getCount() {
			int arrlen = 0;
			
				arrlen = offpatharr.length;
			
			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {

				holder = new ViewHolder();

				convertView = mInflater.inflate(R.layout.listview, null);
				holder.text2 = (ImageView) convertView
						.findViewById(R.id.TextView02);
				holder.text3 = (TextView) convertView
						.findViewById(R.id.TextView03);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {

				if (offpatharr[position].contains("null")
						|| offnamearr[position].contains("null")||offdocidarr[position].contains("null")) {
					offpatharr[position] = offpatharr[position].replace("null", "");
					offnamearr[position] = offnamearr[position].replace("null", "");
					offdocidarr[position] = offdocidarr[position].replace("null", "");
				
				}
				holder.text2.setBackgroundResource(R.drawable.allfilesicon);
            	holder.text3.setText(offpatharr[position]);
				

			} catch (Exception e) {
				Log.i(TAG, "error: efficiaNT2 " + e.getMessage());
			}

			return convertView;
		}

		static class ViewHolder {
			TextView text3;
			ImageView text2;

		}

	}
	private class MyOnItemSelectedListenerdata1 implements OnItemSelectedListener {
		
		   public void onItemSelected(AdapterView<?> parent,
			        View view, int pos, long id) {	
	    	strpresatins = parent.getItemAtPosition(pos).toString();
	    		if(strpresatins.equals("Other"))
		    	{
		    	   tbl.setVisibility(visibility);
		    		o=1;
		    	}
		    	else
		    	{
		    		tbl.setVisibility(view.GONE);
		    		o=0;
		    	}
	    	
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	private class MyOnItemSelectedListenerdata2 implements OnItemSelectedListener {

	    public void onItemSelected(AdapterView<?> parent,
	        View view, int pos, long id) {	
	    	
	    	strpresatins = parent.getItemAtPosition(pos).toString();
	    	try
	    	{
	    		if(strpresatins.equals("Other Information"))
		    	{
	    			FeedbackDocuments.this.othrtxtsupp.setVisibility(view.VISIBLE);	    
		    		
		    		documentothertextsupp=1;
		    	}
		    	else
		    	{
		    		FeedbackDocuments.this.othrtxtsupp.setVisibility(view.GONE);
		    		documentothertextsupp=0;
		    	}
	    	}
	    	catch(Exception e)
	    	{
	    		
	    	}
	    	
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}

	private class MyOnItemSelectedListenerdata3 implements OnItemSelectedListener {

    public void onItemSelected(AdapterView<?> parent,
        View view, int pos, long id) {	
    	
    	strpresatins = parent.getItemAtPosition(pos).toString();
    	try
    	{
    	if(strpresatins.equals("Other Information"))
    	{
    		FeedbackDocuments.this.othrtxtoff.setVisibility(visibility);	    	 
    		documentothertextoff=1;
    	}
    	else
    	{
    		FeedbackDocuments.this.othrtxtoff.setVisibility(view.GONE);
    		documentothertextoff=0;
    	}
    	}
    	catch(Exception e)
    	{
    		
    	}	
    	
     }

    public void onNothingSelected(AdapterView parent) {
      // Do nothing.
    }
}

	
	RadioButton.OnClickListener OnClickListener =  new RadioButton.OnClickListener()
	{

	  public void onClick(View v) {
		    switch(v.getId())
			 {
		    case R.id.customeryes:
		    	isCSC=1;cusyrd.setChecked(true);cusnrd.setChecked(false);
		    	break;
		    case R.id.customerno:
		    	isCSC=2;cusyrd.setChecked(false);cusnrd.setChecked(true);
		    	break;
		    case R.id.insyes:
		    	isInsavail=1;insyrd.setChecked(true);insnrd.setChecked(false);insntrd.setChecked(false);
		    	break;
		    case R.id.insno:
		    	isInsavail=2;insyrd.setChecked(false);insnrd.setChecked(true);insntrd.setChecked(false);
		    	break;
		    case R.id.insnot:
		    	isInsavail=3;insyrd.setChecked(false);insnrd.setChecked(false);insntrd.setChecked(true);
		    	break;
		    case R.id.infyes:
		    	isManf=1;infyrd.setChecked(true);infnrd.setChecked(false);infntrd.setChecked(false);
		    	break;
		    case R.id.infno:
		    	isManf=2;infyrd.setChecked(false);infnrd.setChecked(true);infntrd.setChecked(false);
		    	break;
		    case R.id.infnot:
		    	isManf=3;infyrd.setChecked(false);infnrd.setChecked(false);infntrd.setChecked(true);
		    	break;
		  
			 }
	  }
	  };
	private int feedrws3;
    public void clicker(View v)
		{
    	switch (v.getId())
    	{
			case R.id.save:
				 try
				 {
					 curfeed1 = cf.sh_db.rawQuery("SELECT * FROM "+ cf.FeedBackDocumentTable	+ " WHERE SH_FI_SRID='"+cf.selectedhomeid+"' and SH_FI_IsOfficeUse=0",null);
					 feedrws1 = curfeed1.getCount();
					 
					 curfeed2 = cf.sh_db.rawQuery("SELECT * FROM "+ cf.FeedBackDocumentTable	+ " WHERE SH_FI_SRID='"+cf.selectedhomeid+"' and SH_FI_IsOfficeUse=1",null);
					 feedrws2 = curfeed2.getCount();
					
					 Cursor	c1 = cf.sh_db.rawQuery("SELECT * FROM "+ cf.FeedBackInfoTable	+ " WHERE SH_FI_Srid='"+cf.selectedhomeid+"'",null);
					 feedrws3 = c1.getCount();
					 
				 }
				 catch(Exception e)
				 {
					
				 }
	
			    	strfbcoment=edfbcoment.getText().toString();
			    	if(o==1){
			    	strother = othrtxt.getText().toString();}
			    			    	
			    	else
			    	{
			    		strother="";
			    	}
			    	
			    	if(!cusyrd.isChecked() && !cusnrd.isChecked())
			    	{
			    		cf.ShowToast("Please select Customer Service Evaluation form Options in Feedback Section.",1);
	                   
			    		
			    	}
			    	
			    	else if(!insyrd.isChecked() && !insnrd.isChecked() && !insntrd.isChecked())
			    	{
			    		cf.ShowToast("Please select Insurance Paperwork available.",1);
			    		
			    	}
			    	else if(!infyrd.isChecked() && !infnrd.isChecked() && !infntrd.isChecked())
			    	{
			    		cf.ShowToast("Please select Manufacturer Information.",1);
			    		
			    	}
			    	
			    	else if(feedrws1==0)
			    	{
			    		cf.ShowToast("Please select Supplemental Documents.",1);
			    	}
			    	else if(strfbcoment.trim().equals(""))
			    	{
			    		cf.ShowToast("Please enter Feedback Comments.",1);		    		
			    	}
			    	else if(strother.trim().equals("") && (o==1))
			    	{
			    		cf.ShowToast("Please enter the text for Other.",1);
			    	}

			    	else
			    	{
			    		strpresatins=s.getSelectedItem().toString();
			    		if(feedrws3==0)
			    		{
			    			try
			    			{
			    				isCSC=(cusyrd.isChecked())? 1:(cusnrd.isChecked())? 2:0;
			    				isInsavail=(insyrd.isChecked())? 1:(insnrd.isChecked())? 2:(insntrd.isChecked())? 3:0;
			    				isManf=(infyrd.isChecked())? 1:(infnrd.isChecked())? 2:(infntrd.isChecked())? 3:0;
			    				cf.sh_db.execSQL("INSERT INTO "
									+  cf.FeedBackInfoTable
									+ " (SH_FI_Srid,SH_FI_IsCusServiceCompltd,SH_FI_PresentatInspection,SH_FI_IsInspectionPaperAvbl,SH_FI_IsManufacturerInfo,SH_FI_FeedbackComments,SH_FI_CreatedOn,SH_FI_othertxt)"
									+ " VALUES ('" + cf.encode(cf.selectedhomeid) + "','" +  isCSC+"','"+cf.encode(strpresatins)+"','"+isInsavail + "','"
									+ isManf + "','" + cf.encode(strfbcoment)+"','"+cf.encode(cd.toString())+"','"+cf.encode(strother)+"')");
			    			 
			    					
			    		    	//	cf.ShowToast("Saved successfully.",1);
			    		    		 cf.netalert("MAP");
			    				}
			    				catch(Exception e)
			    				{
			    					cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
			    				}
			    		}
			    		else
			    		{
			    			try
			    			{
			    			 cf.sh_db.execSQL("UPDATE " +  cf.FeedBackInfoTable + " SET SH_FI_IsCusServiceCompltd='"+ isCSC + "',SH_FI_PresentatInspection='"+ cf.encode(strpresatins)+"',SH_FI_othertxt='"+cf.encode(strother)+"',SH_FI_IsInspectionPaperAvbl='"+isInsavail+"',SH_FI_IsManufacturerInfo='"+isManf+"',SH_FI_FeedbackComments='"+cf.encode(strfbcoment)+"',SH_FI_CreatedOn='"+cf.encode(cd.toString())+"' WHERE SH_FI_Srid ='" + cf.selectedhomeid + "'");
			    			 
			    			 
			    			 flagstr=0;
			    			 //cf.ShowToast("Saved successfully.",1);
			    			  cf.netalert("MAP");
					    	}
				   				catch(Exception e)
				   				{
				   					cf.ShowToast("There is a problem in saving your data due to invalid character.",1);	
				   				}
			    			
			        	}
			    		
			    		
			    	}
			    	break;
			  case R.id.browsetxt:
			    	c=1;
			    	if(!"--Select--".equals(s1.getSelectedItem().toString())&& !"Other Information".equals(s1.getSelectedItem().toString()))
	    			{
			    		
	    		        show();
	    			}
			    	else if("Other Information".equals(s1.getSelectedItem().toString()))
			    	{
			    		String tmp=FeedbackDocuments.this.othrtxtsupp.getText().toString();
			    		if(tmp.trim().equals(""))
			    		{
			    			cf.ShowToast("Please enter the other text for Document Title.",1);
			    		}else
			    		{
			    			  show();
			    		}
			    	}
			    	else
			    	{
			    		cf.ShowToast("Please select the Document Title.",1);
			    		
			    	}
			    	break;
			  case R.id.offbrowsetxt:
			    	c=2;selectedImagePath="";
			    	if(!"--Select--".equals(s2.getSelectedItem().toString()) && (!"Other Information".equals(s2.getSelectedItem().toString()) || !othrtxtoff.getText().toString().trim().equals("") ))
	    			{
	    		        AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackDocuments.this);
			            builder.setTitle("Pick an option");
			            builder.setAdapter(adapter,
			                            new DialogInterface.OnClickListener() {
			                                    private String[] pdfname;

												public void onClick(DialogInterface dialog,
			                                                    int item) {
			                                    	 if(items[item].equals("gallery"))
			                                         {
			                                         	t=0;pickfromgallery();
			                                         }
			                                    	 else if(items[item].equals("pdf"))
			                                            {
			                                           
			                                            	final Dialog dialog1 = new Dialog(FeedbackDocuments.this);
			                                                dialog1.setContentView(R.layout.dialog);
			                                                dialog1.setTitle(Html.fromHtml("<h2><font  color=#bddb00>Select PDF to upload  </font></h2>"));
			                                                lv1=(ListView)dialog1.findViewById(R.id.ListView01);
			                                                images = Environment.getExternalStorageDirectory(); 
			                                                ii=0;
			                                                //pdfsample=new String[100];
			                                                walkdir(images);
			                                                pdflist= new String[ii];
			                                                 pdfname=new String[ii];
			                                              if(ii>=1)
			                                              {
			                                                for(int i=0;i<ii;i++)
			                                                {
			                                              
			                                                	
			                                                	pdflist[i]=pdfsample[i];	
			                                                	
			                                                	 String[] bits = pdfsample[i].split("/");
			                                                	 pdfname[i] = bits[bits.length - 1];
			                                                }
			                                              	                                             
				                                                lv1.setAdapter(new ArrayAdapter<String>(FeedbackDocuments.this,android.R.layout.simple_list_item_1 ,pdfname));
				                                                lv1.setOnItemClickListener(new OnItemClickListener() {
				                                                	public void onItemClick(AdapterView<?> a, View v, int position, long id) {
				                                                		PackageManager packageManager = getPackageManager(); 
					                                                    Intent testIntent = new Intent(Intent.ACTION_VIEW); 
					                                                    testIntent.setType("application/pdf"); 
					                                                    List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
					                                                    if (list.size() > 0 ) {  
					                                                        Intent intent = new Intent();
					                                                        intent.setAction(Intent.ACTION_VIEW);
					                                                        Uri uri = Uri.fromFile(new File(pdfsample[(int) id]).getAbsoluteFile());
					                                                        selectedImagePath=cf.decode(uri.toString());
					                                                         String[] bits = selectedImagePath.split("/");
					                           							     picname = bits[bits.length - 1];
				                                                		
				                                                		AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackDocuments.this);
				                                            			builder.setMessage("What would you like to do?")
				                                            			       .setCancelable(false)
				                                            			       .setPositiveButton("Preview", new DialogInterface.OnClickListener() {
				                                            			           public void onClick(DialogInterface dialog, int id) {
				                                            			        	   String suboffstring = selectedImagePath.substring(11);
				                                            			        	 
				          		                     		    					   File file = null;
																					file = new File(suboffstring);
																					
				          		                     				                   if (file.exists()) {
				          		                     				                	Uri path = Uri.fromFile(file);
				          		                     				                    Intent intent1 = new Intent(Intent.ACTION_VIEW);
				          		                     				                    intent1.setDataAndType(path, "application/pdf");
				          		                     				                    intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				          		                     				                   
				          		                     				                    try {
				          		                     				                        startActivity(intent1);
				          		                     				                    } 
				          		                     				                    catch (ActivityNotFoundException e) {
				          		                     				                    	cf.ShowToast("No Application available to view PDF.",1);
				          		                     				                      
				          		                     				                    }
				          		                     				                /* if(c==1)
					       		                                        			    {
					       		                                        			    	docpathedit.setText(selectedImagePath);
					       		                                        				    docbrws.setVisibility(v1.GONE);
					       		                                        				    docupd.setVisibility(visibility);
					       		                                        			    }
					       		                                        			    else if(c==2)
					       		                                        			    {
					       		                                        			    	offpathedit.setText(selectedImagePath);
					       		                                        				    offbrws.setVisibility(v1.GONE);
					       		                                        				    offupd.setVisibility(visibility);
					       		                                        			    }*/
				          		                     				                }
				                                            			           }
				                                            			       })
				                                            			       .setNeutralButton("Select", new DialogInterface.OnClickListener() {
	                                    			           public void onClick(DialogInterface dialog, int id) {
	                                    			        	   if(c==1 && !selectedImagePath.equals(""))
	                                                              	 
	                                               			    {
	                                               			    	docpathedit.setText(selectedImagePath);
	                                               				    docbrws.setVisibility(v1.GONE);
	                                               				    docupd.setVisibility(visibility);
	                                               			    }
	                                               			    else if(c==2 && !selectedImagePath.equals(""))
	                                               			    {
	                                               			    	offpathedit.setText(selectedImagePath);
	                                               				    offbrws.setVisibility(v1.GONE);
	                                               				    offupd.setVisibility(visibility);
	                                               			    }
	                                           				    
	                                    			           }
	                                    			       })
				                                            			       .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				                                            			           public void onClick(DialogInterface dialog, int id) {
				                                            			        	   selectedImagePath="";
				                                            			           }
			                                            			       });
			                                            			 builder.show();
			                                                	   dialog1.cancel();
			                                                     
			                                                    }
			                                                   
			                                                	}
			                                                
			                                                	});                                           
			                                                  
			                                                dialog1.show();
			                                            }// end the valdation of the pdf count 
				                                         else
				                                             {
				                                        	 cf.ShowToast("Please check whether you have a PDF in your Device.",1);
				                                              }
			                                            }
			                                            dialog.dismiss();
			                                    }
			                            });
			            AlertDialog alert = builder.create();
			            alert.show();
	    			}
			    	else
			    	{
			    		cf.ShowToast("Please select the Document Title.",1);
			    		
			    	}
			    	break;
			    case R.id.updtxt:
			    	strdoctitle=s1.getSelectedItem().toString();
			    	if(selectedImagePath.contains("file://"))
			    	{
			    		selectedImagePath = selectedImagePath.replace("file://","");		    		
			    	}
			    	boolean bu=cf.common(selectedImagePath);
			    	if(bu)
					{
			    		if((!("Other Information".equals(s1.getSelectedItem().toString())) || (!othrtxtsupp.getText().toString().trim().equals("") )))
			    		{
				    		if((documentothertextsupp==1) && (othrtxtsupp.length()!=0 ))
					      	{	
				    			strdoctitle1 = othrtxtsupp.getText().toString();
				    		}
						    else if((documentothertextoff==1)  && (othrtxtoff.length()!=0))
						    {
						    	strdoctitle1 = othrtxtoff.getText().toString();				    	
						    }
					        else
						    {
					        	strdoctitle1=strdoctitle;
						    }
				    		flagstr=0;
					      	ImgOrder = getImageOrder(cf.selectedhomeid);
					      	 try
				    			{
					      		 if(!"--Select--".equals(s1.getSelectedItem().toString())){
					      	      cf.sh_db.execSQL("INSERT INTO "
			        					+  cf.FeedBackDocumentTable
			        					+ " (SH_FI_SRID,SH_FI_DocumentTitle,SH_FI_FileName,SH_FI_Nameext,SH_FI_ImageOrder,SH_FI_IsOfficeUse,SH_FI_CreatedOn,SH_FI_ModifiedDate)"
			        					+ " VALUES ('" + cf.selectedhomeid + "','"+cf.encode(strdoctitle1)+"','"+cf.encode(selectedImagePath)+"','"+cf.encode(picname)+"','"+ImgOrder+"','"+flagstr+ "','"+ cf.encode(cd.toString())+"','"+cf.encode(md.toString())+"')");
			        	   
					      	    s1.setSelection(0); // set the select option  as default 
			        	    show_list();
			        	    docpathedit.setText("");
			        	    othrtxtsupp.setText("");othrtxtoff.setText("");
			        	    othrtxtsupp.setVisibility(v1.GONE);othrtxtoff.setVisibility(v1.GONE);

			        		docbrws.setVisibility(visibility);docupd.setVisibility(v1.GONE);
					      		 }
					      		 else
					      		 {
					      			 cf.ShowToast("Please select Document title.", 1);
					      		 }
						}
					    catch (Exception e)
					    {
					     
					    }
		    		}
		    		else
		    		{
		    			cf.ShowToast("Please enter the other text for Document Title",1);
		    		}
					}
			    	else
			    	{
			    		cf.ShowToast("Your file size exceeds 2MB.",1);
			    		 docpathedit.setText("");
			        	    othrtxtsupp.setText("");othrtxtoff.setText("");
			        	    othrtxtsupp.setVisibility(v1.GONE);othrtxtoff.setVisibility(v1.GONE);

			        		docbrws.setVisibility(visibility);docupd.setVisibility(v1.GONE);
			    	}
		        	   
	        	   
			    	break;
			    case R.id.offupdtxt:
			    	strdoctitle=s2.getSelectedItem().toString();
			    	
			    	if(selectedImagePath.contains("file://"))
			    	{
			    		selectedImagePath = selectedImagePath.replace("file://","");
			    	}
			    	boolean bu1=cf.common(selectedImagePath);
			    	if(bu1)
					{
			    		if((!("Other Information".equals(s2.getSelectedItem().toString())) || (!othrtxtoff.getText().toString().trim().equals("") )) )
			    		{
				    		
				    		if((documentothertextsupp==1) && (othrtxtsupp.length()!=0))
					      	{
				    			strdoctitle1 = othrtxtsupp.getText().toString();
				    		}
						    else if((documentothertextoff==1)  && (othrtxtoff.length()!=0))
						    {
						    	strdoctitle1 = othrtxtoff.getText().toString();				    	
						    }
					        else
						    {
					        	strdoctitle1=strdoctitle;
						    }
	
					    	flagstr=1;
					    	ImgOrder = getImageOrder(cf.selectedhomeid);
					    	if(!"--Select--".equals(s2.getSelectedItem().toString())){		try
					    	{
					    		
					    	cf.sh_db.execSQL("INSERT INTO "
			        					+  cf.FeedBackDocumentTable
			        					+ " (SH_FI_SRID,SH_FI_DocumentTitle,SH_FI_FileName,SH_FI_Nameext,SH_FI_ImageOrder,SH_FI_IsOfficeUse,SH_FI_CreatedOn,SH_FI_ModifiedDate)"
			        					+ " VALUES ('" + cf.selectedhomeid+ "','"+ cf.encode(strdoctitle1)+"','"+cf.encode(selectedImagePath)+"','"+cf.encode(picname)+"','"+ImgOrder+"','"+flagstr + "','"
			        					+ cf.encode(cd.toString())+"','"+cf.encode(md.toString())+"')");
			        	    		
					    	s2.setSelection(0); // set the select option  as default
			        	 			}
						                 
		        	    catch(Exception e)
		    			{
		    				cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
		    				
		    			}
			        	    		showoffice_list();
			        	    		offpathedit.setText("");
			        	    		  othrtxtsupp.setText("");othrtxtoff.setText("");
			        	    		  othrtxtsupp.setVisibility(v1.GONE);othrtxtoff.setVisibility(v1.GONE);

			         			 offbrws.setVisibility(visibility);offupd.setVisibility(v1.GONE);
					    	}
					    	else
					    	{
					    		cf.ShowToast("Please select office document title.",1);
					    	}
			    		}
			    		else
			    		{
			    			cf.ShowToast("Please enter the other text for Document Title",1);
			    		}
			        	    		
						}
			    	else
					{
			    			cf.ShowToast("Your file size exceeds 2MB.",1);   
			    			offpathedit.setText("");
	        	    		  othrtxtsupp.setText("");othrtxtoff.setText("");
	        	    		  othrtxtsupp.setVisibility(v1.GONE);othrtxtoff.setVisibility(v1.GONE);

	         			 offbrws.setVisibility(visibility);offupd.setVisibility(v1.GONE);
					}
	        	    		
	        	   
			    	break;
			    case R.id.home:
			    	cf.gohome();
			    	break;
    	}
		}
	private void setaddapters() {
		// TODO Auto-generated method stub
		 adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_row, items) {
		        
		        ViewHolder holder;
		        Drawable icon;
		 
		        class ViewHolder {
		                ImageView icon;
		                TextView title;
		        }
		 
		        public View getView(int position, View convertView,ViewGroup parent) {
		                final LayoutInflater inflater = (LayoutInflater) getApplicationContext()
		                                .getSystemService(
		                                                Context.LAYOUT_INFLATER_SERVICE);
		 
		                if (convertView == null) {
		                        convertView = inflater.inflate(
		                                        R.layout.list_row, null);
		 
		                        holder = new ViewHolder();
		                        holder.icon = (ImageView) convertView
		                                        .findViewById(R.id.icon);
		                        holder.title = (TextView) convertView
		                                        .findViewById(R.id.title);
		                        convertView.setTag(holder);
		                } else {
		                        // view already defined, retrieve view holder
		                        holder = (ViewHolder) convertView.getTag();
		                }              
		  
		                holder.title.setText(items[position]);
		                if(items[position].equals("gallery"))
		                {
		                	 Drawable tile = getResources().getDrawable(R.drawable.gallery);
		                	 holder.icon.setImageDrawable(tile);
		                }
		                else  if(items[position].equals("camera"))
		                {
		                	 Drawable tile1 = getResources().getDrawable(R.drawable.iconphoto);
		                	 holder.icon.setImageDrawable(tile1);
		                }
		       
             else  if(items[position].equals("pdf"))
             {
             	 Drawable tile2 = getResources().getDrawable(R.drawable.pdficon);
             	 holder.icon.setImageDrawable(tile2);
             }
		                
		 
		                return convertView;
		        }
		};
		 adapter11 = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_row, items) {
		        
		        ViewHolder holder;
		        Drawable icon;
		 
		        class ViewHolder {
		                ImageView icon;
		                TextView title;
		        }
		 
		        public View getView(int position, View convertView,ViewGroup parent) {
		                final LayoutInflater inflater = (LayoutInflater) getApplicationContext()
		                                .getSystemService(
		                                                Context.LAYOUT_INFLATER_SERVICE);
		 
		                if (convertView == null) {
		                        convertView = inflater.inflate(
		                                        R.layout.list_row, null);
		 
		                        holder = new ViewHolder();
		                        holder.icon = (ImageView) convertView
		                                        .findViewById(R.id.icon);
		                        holder.title = (TextView) convertView
		                                        .findViewById(R.id.title);
		                        convertView.setTag(holder);
		                } else {
		                        // view already defined, retrieve view holder
		                        holder = (ViewHolder) convertView.getTag();
		                }              
		  
		                holder.title.setText(items[position]);
		                if(items[position].equals("pdf"))
             {
             	 Drawable tile2 = getResources().getDrawable(R.drawable.pdficon);
             	 holder.icon.setImageDrawable(tile2);
             }
		                
		 
		                return convertView;
		        }
		};
	}

	protected void pickpdf() {
		// TODO Auto-generated method stub
		PackageManager packageManager = getPackageManager();
		Intent testIntent = new Intent(Intent.ACTION_VIEW);
		testIntent.setType("application/pdf");
		startActivityForResult(Intent.createChooser(testIntent, "Select pdf"), SELECT_PDF);

}
	protected void startCameraActivity() {
		String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		mCapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
		startActivityForResult(intent, CAPTURE_PICTURE_INTENT);

	}
	protected void pickfromgallery() {	
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Picture"),
				SELECT_PICTURE);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			if(t==0){if (resultCode == RESULT_OK) {
				if (requestCode == SELECT_PICTURE) {
					Uri selectedImageUri = data.getData();
					selectedImagePath = getPath(selectedImageUri);
					 String[] bits = selectedImagePath.split("/");
					 picname = bits[bits.length - 1];
				}
			}
			}
			else if (t == 1) {
					switch (resultCode) {
					case 0:
						break;
					case -1:
						String[] projection = { MediaStore.Images.Media.DATA };
						Cursor cursor = managedQuery(mCapturedImageURI, projection,
								null, null, null);
						int column_index_data = cursor
								.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
						cursor.moveToFirst();
						capturedImageFilePath = cursor.getString(column_index_data);
						selectedImagePath = capturedImageFilePath;
						 String[] bits = selectedImagePath.split("/");
						 picname = bits[bits.length - 1];
						break;

					}
				}
			    if(c==1 && !selectedImagePath.equals("") )
			    {
			    	docpathedit.setText(selectedImagePath);
				    docbrws.setVisibility(v1.GONE);
				    docupd.setVisibility(visibility);
			    }
			    else if(c==2 && !selectedImagePath.equals("") )
			    {
			    	offpathedit.setText(selectedImagePath);
				    offbrws.setVisibility(v1.GONE);
				    offupd.setVisibility(visibility);
			    }
			
		
	}
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	protected void offshowcommon() {
		
		offtbl.setVisibility(visibility);offedittitle.setVisibility(visibility);
	}
	public void walkdir(File dir) {
		    String pdfPattern = ".pdf";
		    File listFile[] = dir.listFiles();
        if (listFile != null) {
		    	  for (int i = 0; i < listFile.length; i++) {

		            if (listFile[i].isDirectory()) {
		                walkdir(listFile[i]);
		            } else  {
		            if (listFile[i].getName().endsWith(pdfPattern)){
		                   
		              pdfsample=dynamicarraysetting(dir+"/"+listFile[i].getName(),pdfsample);

		            	  ii++;
		              }
		            }
		        }
		    }    
		}
	public int getImageOrder(String srid)
		{
			 Cursor	c11 =  cf.sh_db.rawQuery("SELECT * FROM "+  cf.FeedBackDocumentTable	+ " WHERE SH_FI_SRID='"+srid+"' order by SH_FI_ImageOrder desc",null);
		     int imgrws = c11.getCount();
		     
		     if(imgrws==0){
		    	 ImgOrder=1;
		     }
		     else
		     {
		    	 c11.moveToFirst();
		    	 int imgordr=c11.getInt(c11.getColumnIndex("SH_FI_ImageOrder"));
		    	 ImgOrder=imgordr+1;
		    	 
		     }
		    return ImgOrder;
		}
	public void show()
	{
		selectedImagePath="";
		AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackDocuments.this);
        builder.setTitle("Pick an option");
        builder.setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
        	
                            

								private String[] pdfname;

								public void onClick(DialogInterface dialog,
                                                int item) {
                                        if(items[item].equals("gallery"))
                                        {
                                        	
                                        	t=0;pickfromgallery();
                                        }
                                        else if(items[item].equals("camera"))
                                        {
                                        	t=1;startCameraActivity();
                                        }
                                        else
                                        {
                                        	final Dialog dialog1 = new Dialog(FeedbackDocuments.this);
                                            dialog1.setContentView(R.layout.dialog);
                                            dialog1.setTitle(Html.fromHtml("<h2><font  color=#bddb00>Select PDF to upload  </font></h2>"));
                                            lv1=(ListView)dialog1.findViewById(R.id.ListView01);
                                            images = Environment.getExternalStorageDirectory(); 
                                            ii=0;
                                            //pdfsample=new String[100];
                                            walkdir(images);
                                            pdflist= new String[ii];
                                             pdfname=new String[ii];
                                          if(ii>=1)
                                          {
                                            for(int i=0;i<ii;i++)
                                            {
                                          
                                            	
                                            	pdflist[i]=pdfsample[i];	
                                            	
                                            	 String[] bits = pdfsample[i].split("/");
                                            	 pdfname[i] = bits[bits.length - 1];
                                            }
                                            
                                                                                   
                                            lv1.setAdapter(new ArrayAdapter<String>(FeedbackDocuments.this,android.R.layout.simple_list_item_1 ,pdfname));
                                            lv1.setOnItemClickListener(new OnItemClickListener() {
                                            	public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                                            		 	PackageManager packageManager = getPackageManager(); 
                                                Intent testIntent = new Intent(Intent.ACTION_VIEW); 
                                                testIntent.setType("application/pdf"); 
                                                List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
                                                if (list.size() > 0 ) {  
                                                    Intent intent = new Intent();
                                                    intent.setAction(Intent.ACTION_VIEW);
                                                    Uri uri = Uri.fromFile(new File(pdflist[(int) id]).getAbsoluteFile());
                                                   
                                                     selectedImagePath=cf.decode(uri.toString());
                                                    
                                                     String[] bits = selectedImagePath.split("/");
                       							     picname = bits[bits.length - 1];
                       							  AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackDocuments.this);
                                      			builder.setMessage("What would you like to do?")
                                      			       .setCancelable(false)
                                      			       .setPositiveButton("Preview", new DialogInterface.OnClickListener() {
                                      			           public void onClick(DialogInterface dialog, int id) {
                                      			        	   String suboffstring = selectedImagePath.substring(11);
                                      			        
                                      			        	 File file = null;
																//file = new File(URLDecoder.decode(suboffstring, "UTF-8"));
																file = new File(suboffstring);
    		                     							 
    		                     				                   if (file.exists()) {
    		                     				                	Uri path = Uri.fromFile(file);
    		                     				                    Intent intent1 = new Intent(Intent.ACTION_VIEW);
    		                     				                    intent1.setDataAndType(path, "application/pdf");
    		                     				                    intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    		                     				 
    		                     				                    try {
    		                     				                        startActivity(intent1);
    		                     				                    } 
    		                     				                    catch (ActivityNotFoundException e) {
    		                     				                    	cf.ShowToast("No Application available to view PDF.",1);
    		                     				                       
    		                     				                    }

                                             /*if(c==1 && !selectedImagePath.equals(""))
                                            	 
                                    			    {
                                    			    	docpathedit.setText(selectedImagePath);
                                    				    docbrws.setVisibility(v1.GONE);
                                    				    docupd.setVisibility(visibility);
                                    			    }
                                    			    else if(c==2 && !selectedImagePath.equals(""))
                                    			    {
                                    			    	offpathedit.setText(selectedImagePath);
                                    				    offbrws.setVisibility(v1.GONE);
                                    				    offupd.setVisibility(visibility);
                                    			    }*/
    		                     				                  }
                                    			           }
                                    			       })
                                    			       
                                      			.setNeutralButton("Select", new DialogInterface.OnClickListener() {
                                    			           public void onClick(DialogInterface dialog, int id) {
                                    			        	   if(c==1 && !selectedImagePath.equals(""))
                                                              	 
                                               			    {
                                               			    	docpathedit.setText(selectedImagePath);
                                               				    docbrws.setVisibility(v1.GONE);
                                               				    docupd.setVisibility(visibility);
                                               			    }
                                               			    else if(c==2 && !selectedImagePath.equals(""))
                                               			    {
                                               			    	offpathedit.setText(selectedImagePath);
                                               				    offbrws.setVisibility(v1.GONE);
                                               				    offupd.setVisibility(visibility);
                                               			    }
                                           				    
                                    			           }
                                    			       })
                                    			       .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    			           public void onClick(DialogInterface dialog, int id) {
                                    			        	   selectedImagePath="";
                                    			           }
                                    			       });
                                    			 builder.show();

                                    			  dialog1.cancel();
                                                  
                                                }

                                            	}
                                            
                                            	});                                           
                                              
                                            dialog1.show();
                                       } // the validation if of pdf count 
                                            else
                                            {
                                       	 cf.ShowToast("Please check whether you have PDF in your Device.",1);
                                             }
                                        }
                                        dialog.dismiss();
                                }
                        });
        AlertDialog alert = builder.create();
        alert.show();
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				if(cf.strschdate.equals("")){
					cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
				}else{Intent  myintent = new Intent(FeedbackDocuments.this,photos_signature.class);
				cf.putExtras(myintent);
				startActivity(myintent);}
				return true;
			}
			if (keyCode == KeyEvent.KEYCODE_MENU) {

			}
			return super.onKeyDown(keyCode, event);
		}
	class FI_watcher implements TextWatcher
	{
         
		
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
				cf.showing_limitwithdefaultwidth(s.toString(),cf.SQ_ED_type1_parrant,cf.SQ_ED_type1,cf.SQ_TV_type1,600);
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
		
	}
	private String[] dynamicarraysetting(String ErrorMsg,String[] pieces3) {
		// TODO Auto-generated method stub
	
		 try
		 {
		if(pieces3==null)
		{
			pieces3=new String[1];
			
			pieces3[0]=ErrorMsg;
		}
		else
		{
			
			String tmp[]=new String[pieces3.length+1];
			int i;
			for(i =0;i<pieces3.length;i++)
			{
				
				tmp[i]=pieces3[i];
			}
		
			tmp[tmp.length-1]=ErrorMsg;
			pieces3=null;
			pieces3=tmp.clone();
		}
		 }
		 catch(Exception e)
		 {
			 
		 }
		return pieces3;
	}
}

package idsoft.inspectiondepot.sinkholeinspection;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import idsoft.inspectiondepot.sinkholeinspection.R;

import android.R.string;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.Camera.Parameters;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

public class ExportInspection extends Activity implements Runnable {
	CommonFunctions cf;
	TextView title;
	/** STRING FOR THE EXPORT OPTION CHECKING **/
	String Export_chk[]={"0","0","0","0","0","0","0","0","0","0"};
	boolean Export_status[]={false,false,false,false,false,false,false,false,false,false};
	CheckBox alert_chk[]=new CheckBox[10];
	/** STRING FOR THE EXPORT OPTION CHECKING ENDS **/
	Button completed_insp,export;
	double incre_unit;
	/**STRING THATS WE ARE USED FOR THE PROGRESS BAR **/
	Dialog dialog1,dialog2;
	public ProgressThread progThread;
	ProgressDialog progDialog;
	int vcode;
	int mState;
    int typeBar = 1;
	double total; // Determines type progress bar: 0 = spinner, 1 = horizontal
	int delay = 40; // Milliseconds of delay in the update loop
	int maxBarValue = 0,show_handler; // Maximum value of horizontal progress bar
	final CheckBox sta_chk[]= new CheckBox[9];;
	int RUNNING = 1;
    String Exportbartext="",dt;
	private String[] policyinformation=null;
	private String[] policyinformation_mail;
	private String[] S_Q_information;
	private String[] B_I_Q_information;
	private String[] OB1;
    public String[] erro_trce=null;
	private String erromsg;
	private String[] feed_info;
	private Object strBase64;
	private String[] OB2,OB1T;
	private String[] OB3;
	public boolean photores=true;
	private String[] OB4;
	 String status="Uploading inspection completed ";
	private double increment_unit;   
    /**STRING THATS WE ARE USED FOR THE PROGRESS BAR ENDS **/
	 @Override
    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
			//	cf.onlinspectionid = extras.getString("InspectionType");
				cf.onlstatus = extras.getString("type");
				
			}
			cf.Create_Table(3);
			cf.Create_Table(7);
			cf.getInspectorId();
			setContentView(R.layout.completedinspectionlist);
			TextView tvheader = (TextView) findViewById(R.id.information);
			completed_insp = (Button) findViewById(R.id.completed_insp);
			((Button) findViewById(R.id.deleteall)).setVisibility(View.INVISIBLE);
			completed_insp.setOnClickListener(new clicker());
			export = (Button) findViewById(R.id.export_insp);
			export.setOnClickListener(new clicker());
			completed_insp.setVisibility(cf.show.VISIBLE);
			export.setVisibility(cf.show.VISIBLE);
			System.out.println("cf.onl"+cf.onlstatus);
			if(cf.onlstatus.equals("export")) {
				tvheader.setText("Export inspection");
				export.setBackgroundResource(R.drawable.button_back_blue);
			}
			else if(cf.onlstatus.equals("Reexport")) {
				tvheader.setText("Reexport inspection");
				completed_insp.setBackgroundResource(R.drawable.button_back_blue);
			}
			cf.onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
			cf.releasecode = (TextView)findViewById(R.id.releasecode);
	        //cf.releasecode.setText(cf.apkrc);
			cf.setRcvalue(cf.releasecode);
	        cf.getInspectorId();
	        cf.welcome = (TextView) this.findViewById(R.id.welcomename);
	        cf.welcome.setText(cf.Insp_firstname+" "+cf.Insp_lastname);
	        title = (TextView) this.findViewById(R.id.deleteiinformation);
			cf.search = (Button) this.findViewById(R.id.search);
			 Button home = (Button) this.findViewById(R.id.deletehome);
			cf.search_text = (EditText)findViewById(R.id.search_text);
			cf.search_clear_txt = (Button)findViewById(R.id.search_clear_txt);
			cf.search_clear_txt.setOnClickListener(new OnClickListener() {
            	public void onClick(View arg0) {
					// TODO Auto-generated method stub
					cf.search_text.setText("");
					cf.res = "";
					dbquery();
            	}
          });
			home.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
					// TODO Auto-generated method stub

					cf.gohome();

				}

			});
			cf.search.setOnClickListener(new OnClickListener() {
                   public void onClick(View v) {
					// TODO Auto-generated method stub

					String temp = cf.encode(cf.search_text.getText()
							.toString());
					if (temp.equals("")) {
						cf.ShowToast("Please enter the Name or Policy Number to search.", 1);
						cf.search_text.requestFocus();
					} else {
						cf.res = temp;
						dbquery();
					}

				}

			});
			completed_insp = (Button) findViewById(R.id.completed_insp);
			completed_insp.setOnClickListener(new clicker());
			export = (Button) findViewById(R.id.export_insp);
			export.setOnClickListener(new clicker());
			dbquery();
			
	 }
	
		 /** Export function starts here 
		 * @param inc_u **/
	private void StartExport(String dt, final double inc_u) {
				// TODO Auto-generated method stub
			 increment_unit=inc_u;
			 cf.selectedhomeid=dt;
			 if (cf.isInternetOn() == true) {
                   total=0.0;
                   /**We need to delete the preivious progress bar then we start new one code starts herer**/
					try
					{
					removeDialog(1);
					}
					catch (Exception e)
					{
						
					}
					/**We need to delete the preivious progress bar then we start new one code Ends herer**/
                    progDialog = new ProgressDialog(ExportInspection.this); // we creat dilog box 
					showDialog(1);
					
					new Thread() {

						private String elevvetiondes;
						private int usercheck;

						public void run() {
							Looper.prepare();

							try {
								
								
							String	selectedItem = cf.selectedhomeid;
								total = 1.0;
								Exportbartext="Check the status of inspection ";
								total=5.0;
								if (!"".equals(cf.selectedhomeid)) {
									//Check if the record in the correct status for uploading
									
									cf.Chk_Inspector=cf.fn_CurrentInspector(cf.Insp_id,cf.selectedhomeid);
									if(cf.Chk_Inspector.equals("true"))
									{
									    boolean insp_sta=false;
										try
										{
											 insp_sta=cf.getinspectionstatus(cf.Insp_id,"GetInspectionStatus");
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while retrieving the Inspection status Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while retrieving the Inspection status Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											
											throw s;
										}
										catch (IOException io) 
										{
	
											String	strerrorlog="Inputoutput exception happens while retrieving the Inspection status Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem in retrieving the Inspection status in the server please contact paperless admin ");
											
										}
										catch (XmlPullParserException x) 
										{
											String	strerrorlog="Xmlparser  exception happens while retrieving the Inspection status Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem retrieving the Inspection status information ");
											
										}
										catch (Exception ex) 
										{
											String	strerrorlog="Exception happens while Uploading the policy holder information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  retrieving the Inspection status information ");
											
										}
									
								if (insp_sta) {
									
									// Checking The option which are selected 
									if(Export_chk[0].equals("1")||Export_chk[1].equals("1"))
									{   Exportbartext="Uploading Policy holder information ";
										try
										{
											cf.Create_Table(3);
											cf.Create_Table(7);
											Export_status[0]=(Upload_Policyholder()) ? true:false;
											//Error_traker("Data moved success fully ");
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the policy holder information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the policy holder information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the policy holder information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  in the server please contact paperless admin");
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the policy holder information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Policy holder information ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the policy holder information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Policy holder information ");
										}
										total=total+inc_u;
									}
									if(Export_chk[0].equals("1")||Export_chk[2].equals("1"))
									{  Exportbartext="Uploading summary Question information ";
										try
										{	cf.Create_Table(4);
										Export_status[1]=(Upload_Summary_Question()) ? true:false;
										//Error_traker("Data moved success fully  summery");
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the Upload_Summary_Question information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the Upload_Summary_Question information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the Upload_Summary_Question information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  in the server please contact paperless admin");
											throw io;
											
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the Upload_Summary_Question information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Summery question information ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Upload_Summary_Question information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Summery Question  information ");
										}
										total=total+inc_u;
									}
									if(Export_chk[0].equals("1")||Export_chk[3].equals("1"))
									{	 Exportbartext="Uploading Building  information ";
										try
										{   cf.Create_Table(5);
											Export_status[2]=(Upload_Buildinginformation()) ? true:false;
											
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the Upload_Buildinginformation information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the Upload_Buildinginformation information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the Upload_Buildinginformation information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  in the server please contact paperless admin");
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the Upload_Buildinginformation information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Building  information ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Upload_Buildinginformation information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Building  information ");
										}
										total=total+inc_u;
									}
									if(Export_chk[0].equals("1")||Export_chk[4].equals("1"))
									{  Exportbartext="Uploading observation 1-7 information ";
										try
										{ cf.Create_Table(8);
										cf.Create_Table(9);
										Export_status[3]=(Uploading_Observation1()&& Uploading_LargeTree()) ? true:false;
											
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the Uploading_Observation3 information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the Uploading_Observation3 information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the Uploading_Observation3 information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  in the server please contact paperless admin");
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the Uploading_Observation3 information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Observation 1  information ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Uploading_Observation3 information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Observation 1  information ");
										}
										
										total=total+inc_u;
									}
									if(Export_chk[0].equals("1")||Export_chk[5].equals("1"))
									{ Exportbartext="Uploading observation 8-13 information ";
										try
										{
											cf.Create_Table(10);
											Export_status[4]=(Uploading_Observation2()) ? true:false;
											
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the Uploading_Observation2 information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the Uploading_Observation2 information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the Uploading_Observation2 information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  in the server please contact paperless admin");
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the Uploading_Observation2 information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Observation 2  information ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Uploading_Observation2 information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export insection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Observation 2  information ");
										}
										total=total+inc_u;
										
									}
									if(Export_chk[0].equals("1")||Export_chk[6].equals("1"))
									{		Exportbartext="Uploading observation 14-22 information ";
										try
										{
											cf.Create_Table(11);
											Export_status[5]=(Uploading_Observation3()) ? true:false;
											
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the Uploading_Observation3 information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the Uploading_Observation3 information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the Uploading_Observation3 information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  in the server please contact paperless admin");
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the Uploading_Observation3 information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Observation 3  information ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Uploading_Observation3 information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Observation 3  information ");
										}
										total=total+inc_u;
										
									}
									if(Export_chk[0].equals("1")||Export_chk[7].equals("1"))
									{	Exportbartext="Uploading observation 23-27 information ";
										try
										{cf.Create_Table(12);
										Export_status[6]=(Uploading_Observation4()) ? true:false;
											//Uploading_Observation4();
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the Uploading_Observation4 information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the Uploading_Observation4 information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the Uploading_Observation4 information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  in the server please contact paperless admin");
											throw io;
											
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the Uploading_Observation4 information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Observation 4  information ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Uploading_Observation4 information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Observation 4  information ");
										}
										
										total=total+inc_u;
									}
									if(Export_chk[0].equals("1")||Export_chk[8].equals("1"))
									{  Exportbartext="Uploading Photos ";
										try
										{
											cf.Create_Table(15);
											Export_status[7]=(Uploading_Photos()) ? (photores)? true:false:false;
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the Uploading_Photos(); information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the Uploading_Photos(); information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have Internet Connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the Uploading_Photos(); information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have problem  in the server please contact paperless admin");
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the Uploading_Photos(); information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have problem while uploading the photos information. ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Uploading_Photos(); information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem while uploading the photos information. ");
										}
									
									}
									if(Export_chk[0].equals("1")||Export_chk[9].equals("1"))
									{	Exportbartext="Uploading Feed back document ";
										
										try
										{
											cf.Create_Table(13);
											cf.Create_Table(14);
											Export_status[8]=(Uploading_feedback()) ? true:false;
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the Uploading_feedback information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the Uploading_feedback information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the Uploading_feedback information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have problem  in the server please contact paperless admin");
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the Uploading_feedback information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have problem while uploading the feed back  information. ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Uploading_feedback information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have problem while uploading the feed back information. ");
										}
										
									}
									/*if(Export_chk[0].equals("1")||Export_chk[9].equals("1"))
									{
									*/	Exportbartext="Changing Status from schedule to preinspected";
									
										try
										{
											show_staus();
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while getting the status  information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while  getting the  Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens  gettting the status Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while  getting the status  Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while getting the status information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
										}
										
										try
										{
											Status_change();
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while changing the status in schedule to pre inspected information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while  changing the status in schedule to pre inspected Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens  changing the status in schedule to pre inspected Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  in the server please contact paperless admin");
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while  changing the status in shedule to pre inspected Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem in moving  schedule to pre inspected ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Uploading_feedback information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the feed back information ");
										}
										total=total+5;
										try
										{
											Error_logupload();
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while uploading error log file information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while uploading error log file Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens uploading error log file Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
									
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while  uploading error log file Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens whileuploading error log file information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
										}
										total=total+5;
									//}show_staus
										
									
								

								}
									
							     }
								}
								else
								{
									total=100;
										}
							}
							catch(Exception e)
								{
								String	strerrorlog="Exception happens in the overall exception Error ="+e;
								cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
							
								}
							total=100.00;
							}

						

						
						}.start();
			 }
			 else
			 {
				 cf.ShowToast("Internet connection is not available.", 1);
			 }
						
			}   
    protected void Error_logupload()throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException  {
			// TODO Auto-generated method stub
    	
    	File assist = new File(this.getFilesDir()+"/errorlogfile.txt");
    	
    	if(cf.common(this.getFilesDir()+"/errorlogfile.txt"))
    	{
	    	SoapObject request = new SoapObject(cf.NAMESPACE,"ErrorLogFile_Tab");
	    	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	    	envelope.dotNet = true;
	    	request.addProperty("InspectorId",cf.Insp_id);
	    	
	    	if (assist.exists()) 
			{
	    		try
					{
						
						InputStream fis = new FileInputStream(assist);
						long length = assist.length();
						byte[] bytes = new byte[(int) length];
						int offset = 0;
						int numRead = 0;
						while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0)
						{
							offset += numRead;
						}
						strBase64 = Base64.encode(bytes);
						request.addProperty("imgByte", strBase64);
						envelope.setOutputSoapObject(request);
						HttpTransportSE httpTransport = new HttpTransportSE(cf.URL);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
						androidHttpTransport.call(cf.NAMESPACE+"ErrorLogFile_Tab",envelope);
						String result =  envelope.getResponse().toString();
						if(cf.check_Status(result))
						{
							
							assist.delete();
							
						}
						
					}
					catch (Exception e) 
					{
					}
						
			
		}
    	}
		}

	protected boolean Status_change() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException {
			// TODO Auto-generated method stub


			SoapObject request = new SoapObject(cf.NAMESPACE,"UpdateInspectionStatus");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("InspectorID",cf.Insp_id);
			request.addProperty("SRID",cf.selectedhomeid);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			androidHttpTransport.call(cf.NAMESPACE+"UpdateInspectionStatus",envelope);
			String result =  envelope.getResponse().toString();
			
			if(cf.check_Status(result))
			{
				status="This inspection was  uploaded completely to you online profile under \"pre-inspected\" status. \n Please log into  www.paperlessinspectors.com and audit and submit this file for QA.";
				cf.sh_db.execSQL("UPDATE "
						+ cf.policyholder
						+ " SET SH_PH_IsInspected=2,SH_PH_IsUploaded=1,SH_PH_SubStatus=41 WHERE SH_PH_SRID ='"
						+ cf.selectedhomeid
						+ "'");
				return true;
				
				
			}
			else 
			{
				status="Exporting inspection completed. Inspection in scheduled  status";
				return false;
			}
			
			
		}

		/** Export summary question inforamtion Ends here **/
	/** Upload policy holder information Starts **/
	    
	private boolean Upload_Policyholder() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException{ 
		//uploding the policy holder information  
		// TODO Auto-generated method stub
		
		SoapObject request = new SoapObject(cf.NAMESPACE,"ExportData");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		/** set the value get from the policy holder table starts **/
		Cursor C_policy= cf.SelectTablefunction(cf.policyholder, " where SH_PH_SRID='" + cf.selectedhomeid + "' and SH_PH_InspectorId='"+ cf.Insp_id + "'");
		if(C_policy.getCount()==1)
		{
			policyinformation=cf.setvalueToArray(C_policy);/** set the value to the array for the easy usage **/
			request.addProperty("InspectorID",policyinformation[1]);
			request.addProperty("SRID",policyinformation[2]);
			request.addProperty("OwnerFirstName",policyinformation[3]);
			request.addProperty("OwnerLastName",policyinformation[4]);
			request.addProperty("Address1",policyinformation[5]);
			request.addProperty("Address2",policyinformation[6]);
			request.addProperty("City",policyinformation[7]);
			request.addProperty("Zip",policyinformation[8]);
			request.addProperty("State",policyinformation[9]);
			request.addProperty("County",policyinformation[10]);
			request.addProperty("PolicyNumber",policyinformation[11]);
			request.addProperty("Inspectionfees",policyinformation[12]);
			request.addProperty("HomePhone",policyinformation[14]);
			request.addProperty("WorkPhone",policyinformation[15]);
			request.addProperty("CellPhone",policyinformation[16]);
			request.addProperty("Email",policyinformation[17]);
			cf.Create_Table(7);
			/** set the value get from the policy holder mailing  table Starts **/
			Cursor C_policymail= cf.SelectTablefunction(cf.MailingPolicyHolder, " where SH_ML_PH_SRID='" + cf.selectedhomeid + "' and SH_ML_PH_InspectorId='"+ cf.Insp_id + "'");
			policyinformation_mail=cf.setvalueToArray(C_policymail);/** set the value to the array for the easy usage **/
			if(policyinformation_mail!=null)
			{
				request.addProperty("MailingAdrdress1",policyinformation_mail[4]);
				request.addProperty("MailingAdrdress2",policyinformation_mail[5]);
				request.addProperty("MailingCity",policyinformation_mail[6]);
				request.addProperty("MailingZip",policyinformation_mail[7]);
				request.addProperty("MailingState",policyinformation_mail[8]);
				request.addProperty("MailingCounty",policyinformation_mail[9]);
			}
			else
			{
				request.addProperty("MailingAdrdress1","");
				request.addProperty("MailingAdrdress2","");
				request.addProperty("MailingCity","");
				request.addProperty("MailingZip","");
				request.addProperty("MailingState","");
				request.addProperty("MailingCounty","");
			}
			/** set the value get from the policy holder mailing  table Ends **/
			envelope.setOutputSoapObject(request);
			
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			androidHttpTransport.call(cf.NAMESPACE+"ExportData",envelope);
			String result =  envelope.getResponse().toString();
						
			return cf.check_Status(result);
		}
		/** set the value get from the policy holder table Ends **/
		
		
		
		return true;
		
		
	}
	/** Upload policy holder information Ends **/
	
	protected boolean Upload_Summary_Question() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException{
		// TODO Auto-generated method stub
	SoapObject request = new SoapObject(cf.NAMESPACE,"ExportInspectorFindingSummary");
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.dotNet = true;
	/** set the value get from the policy holder table starts **/
	Cursor C_SQ= cf.SelectTablefunction(cf.SQ_table, " where SH_SQ_Homeid='" + cf.encode(cf.selectedhomeid) + "' and SH_SQ_Inspectorid='"+ cf.encode(cf.Insp_id) + "' ");
	if(C_SQ.getCount()==1)
	{
		S_Q_information=cf.setvalueToArray(C_SQ);/** set the value to the array for the easy usage **/
		request.addProperty("InspectorID",S_Q_information[1]);
		request.addProperty("SRID",S_Q_information[2]);
		/*if(S_Q_information[3].equals("Not Determined"))
		{
			request.addProperty("Question1","");
		}
		else
		{
			request.addProperty("Question1",S_Q_information[3]);
		}
		if(S_Q_information[4].equals("Not Determined"))
		{
			request.addProperty("Question2","");
		}
		else
		{
			 request.addProperty("Question2",S_Q_information[4]);
		}*/
		request.addProperty("Question2",S_Q_information[4]);
		request.addProperty("Question1",S_Q_information[3]);
		 request.addProperty("Question3","");
		 request.addProperty("Question3Miles","");
		 request.addProperty("Question4","");
		 if(S_Q_information[6].equals("0"))
		 {
			 request.addProperty("IsPolicyHolderPresent",false);
		 }
		 else
		 {
			 request.addProperty("IsPolicyHolderPresent",true);
		 }
		 
		 request.addProperty("PolicyHolderQuestion1",S_Q_information[7]);
		 request.addProperty("PolicyHolderQuestion2",S_Q_information[8]);
		 request.addProperty("PolicyHolderQuestion3",S_Q_information[9]);
		 request.addProperty("PolicyHolderQuestion4",S_Q_information[10]);
		 request.addProperty("PolicyHolderQuestion5",S_Q_information[11]);
		 request.addProperty("PolicyHolderQuestion6",S_Q_information[12]);
		 request.addProperty("PolicyHolderQuestion7",S_Q_information[13]);
		 request.addProperty("Comments",S_Q_information[5]);
		 request.addProperty("CreatedDate",cf.datewithtime);
		 request.addProperty("ModifiedDate",cf.datewithtime);
		 request.addProperty("DisclosureComment",S_Q_information[14]);
		envelope.setOutputSoapObject(request);
		
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		androidHttpTransport.call(cf.NAMESPACE+"ExportInspectorFindingSummary",envelope);
		String result =  envelope.getResponse().toString();
		System.out.println("request"+request);
		return cf.check_Status(result);
	}else
	{
		
		return true;
	}
	
	}
	protected boolean Upload_Buildinginformation() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException {
		// TODO Auto-generated method stub
	SoapObject request = new SoapObject(cf.NAMESPACE,"ExportSinkhole_InspectionInfo_Tab3");
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.dotNet = true;
	/** set the value get from the policy holder table starts **/
	Cursor C_BQ= cf.SelectTablefunction(cf.BIQ_table, " where SH_SQ_Homeid='" + cf.encode(cf.selectedhomeid) + "' and SH_BI_Inspectorid='"+ cf.encode(cf.Insp_id) + "' ");
	
	if(C_BQ.getCount()==1)
	{
		  B_I_Q_information=cf.setvalueToArray(C_BQ);/** set the value to the array for the easy usage **/
		  request.addProperty("InspectorID",B_I_Q_information[1]);
		  request.addProperty("SRID",B_I_Q_information[2]);              
	      request.addProperty("TotalNumberofBedroom",B_I_Q_information[18]);             
	      request.addProperty("NumberofExteriorWindowOpening",B_I_Q_information[19]);
	      request.addProperty("NumberofInternalDoorOpening",B_I_Q_information[20]);
	      request.addProperty("BuildingDirection",B_I_Q_information[3]);
	      request.addProperty("PropertyFronts",B_I_Q_information[4]);
	      request.addProperty("PresentAtInspection",B_I_Q_information[5]);
	      request.addProperty("PresentAtInspectionOption",B_I_Q_information[6]);
	      request.addProperty("BuildingType",B_I_Q_information[7]);
	      request.addProperty("YearOfConstruction",B_I_Q_information[8]);
	      request.addProperty("YearOfConstructionOption",B_I_Q_information[9]);
	      request.addProperty("BuildingSize",B_I_Q_information[10]); 
	      request.addProperty("SquareFeet","");
	      request.addProperty("FoundationWall",B_I_Q_information[11]);
	      request.addProperty("FoundationRestraint",B_I_Q_information[12]);
	      request.addProperty("NumberOfStories",B_I_Q_information[13]);
	      request.addProperty("ConcreteBlockUnreinforced",B_I_Q_information[14]);
	      request.addProperty("ConcreteBlockReinforced",B_I_Q_information[15]);
	      request.addProperty("SolidConcrete",B_I_Q_information[16]);
	      request.addProperty("WoodMetalFrame",B_I_Q_information[17]);
	      request.addProperty("AluminiumSiding",B_I_Q_information[21]);
	      request.addProperty("Stucco",B_I_Q_information[25]);
	      request.addProperty("VinylSiding",B_I_Q_information[22]);
	      request.addProperty("BrickVeneer",B_I_Q_information[26]);
	      request.addProperty("WoodSiding",B_I_Q_information[23]);
	      request.addProperty("PaintedBlock",B_I_Q_information[27]);
	      request.addProperty("Other",B_I_Q_information[24]);
	      request.addProperty("Comments",B_I_Q_information[28]);
	      request.addProperty("CreatedDate",cf.datewithtime);
	      request.addProperty("ModifiedDate",cf.datewithtime);
	      request.addProperty("OverallComment",B_I_Q_information[28]);
	      envelope.setOutputSoapObject(request);
		  
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		androidHttpTransport.call(cf.NAMESPACE+"ExportSinkhole_InspectionInfo_Tab3",envelope);
		String result =  envelope.getResponse().toString();
		
		return cf.check_Status(result);
	}else
	{
		
		return true;
	}
	
	}
    /* UPLOADING OBSERVATION */
	protected boolean Uploading_Observation4() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {
		// TODO Auto-generated method stub
	SoapObject request = new SoapObject(cf.NAMESPACE,"ExportSinkhole_Question_Part4");
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.dotNet = true;
	/** set the value get from the policy holder table starts **/
	Cursor C_OB4= cf.SelectTablefunction(cf.Observation4, " where SH_OBS4_SRID='" + cf.encode(cf.selectedhomeid) + "'");
	
	if(C_OB4.getCount()==1)
	{
		  OB4=cf.setvalueToArray(C_OB4);/** set the value to the array for the easy usage **/
		  request.addProperty("InspectorID",cf.Insp_id);
		  
		     request.addProperty("SRID",OB4[1]);                                                                                                                                   
		     request.addProperty("CrawlSpacePresent",OB4[2]);                                                                
		     request.addProperty("CrawlSpaceComments",OB4[3]);                                                                   
		     request.addProperty("StandingWaterNoted",OB4[4]);                                                                
		     request.addProperty("StandingWaterComments",OB4[5]);                                                                   
		     request.addProperty("LeakingPlumbNoted",OB4[6]);                                                                   
		     request.addProperty("LeakingPlumbComments",OB4[7]);                                                              
		     request.addProperty("VentilationNoted",OB4[8]);                                                                  
		     request.addProperty("VentilationComments",OB4[9]);                                                                   
		     request.addProperty("CrawlSpaceAccessible",OB4[10]);                                                                   
		     request.addProperty("CrawlSpaceAccessibleComments",OB4[11]);                                                        
		     request.addProperty("RoofSagNoted",OB4[12]);                                                                 
		     request.addProperty("RoofSagComments",OB4[13]);                                                                 
		     request.addProperty("RoofLeakageEvidence",OB4[14]);                                                                
		     request.addProperty("RoofLeakageComments",OB4[15]);                                                                   
		     request.addProperty("DeferredMaintanenceIssuesNoted",OB4[16]);                                                                                                                                  
		     request.addProperty("DeferredMaintanenceComments",OB4[17]);                                                                  
		     request.addProperty("DeterioratedNoted",OB4[18]);                                                                  
		     request.addProperty("DeterioratedComments",OB4[19]);                                                                  
		     request.addProperty("DeterioratedExteriorFinishesNoted",OB4[20]);                                                                   
		     request.addProperty("DeterioratedExteriorFinishesComments",OB4[21]);                                                                 
		     request.addProperty("DeterioratedExteriorSidingNoted",OB4[22]);                                                                 
		     request.addProperty("DeterioratedExteriorSidingComments",OB4[23]);                                                             
		     request.addProperty("SafetyNoted",OB4[24]);                                                                 
		     request.addProperty("SafetyComments",OB4[25]);                                                                  
		     request.addProperty("OverallComments",OB4[26]);                                                                   
		     request.addProperty("CreatedDate",cf.datewithtime);                                                                   
		     request.addProperty("ModifiedDate",cf.datewithtime);                                                                                                                          
	       envelope.setOutputSoapObject(request);
	  
		   HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		   androidHttpTransport.call(cf.NAMESPACE+"ExportSinkhole_Question_Part4",envelope);
		   String result =  envelope.getResponse().toString();
		   
		   return cf.check_Status(result);
	    }else
		{
			
			return true;
		}
	
	}
	protected boolean Uploading_Observation3() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {
		// TODO Auto-generated method stub
	SoapObject request = new SoapObject(cf.NAMESPACE,"ExportSinkhole_Question_Part3");
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.dotNet = true;
	/** set the value get from the policy holder table starts **/
	Cursor C_OB3= cf.SelectTablefunction(cf.Observation3, " where SH_OBS3_SRID='" + cf.encode(cf.selectedhomeid) + "'");
	
	if(C_OB3.getCount()==1)
	{
		  OB3=cf.setvalueToArray(C_OB3);/** set the value to the array for the easy usage **/
		  request.addProperty("InspectorID",cf.Insp_id);
		  
	      request.addProperty("SRID",OB3[1]);                                                 
	      request.addProperty("InteriorWallCrackNoted",OB3[2]);                                               
	      request.addProperty("InteriorWallCrackOption",OB3[3]);                                                
	      request.addProperty("InteriorWallCrackComments",OB3[4]);                                             
	      request.addProperty("WallSlabCrackNoted",OB3[5]);                                                  
	      request.addProperty("WallSlabCrackOption",OB3[6]);                                                   
	      request.addProperty("WallSlabCrackComments",OB3[7]);                                                 
	      request.addProperty("InteriorCeilingCrackNoted",OB3[8]);                                               
	      request.addProperty("InteriorCeilingCrackOption",OB3[9]);                                                  
	      request.addProperty("InteriorCeilingCrackComments",OB3[10]);                                               
	      request.addProperty("InteriorFloorCrackNoted",OB3[11]);                                                 
	      request.addProperty("InteriorFloorCrackOption",OB3[12]);                                                
	      request.addProperty("InteriorFloorCrackComments",OB3[13]);                                               
	      request.addProperty("WindowFrameOutofSquare",OB3[14]);                                                  
	      request.addProperty("WindowFrameOutofSquareOption",OB3[15]);                                                   
	      request.addProperty("WindowFrameOutofSquareComments",OB3[16]);                                                
	      request.addProperty("FloorSlopingNoted",OB3[17]);                                                  
	      request.addProperty("FloorSlopingOption",OB3[18]);                                                 
	      request.addProperty("FloorSlopingComments",OB3[19]);                                                
	      request.addProperty("CrackedGlazingNoted",OB3[20]);                                                   
	      request.addProperty("CrackedGlazingOption",OB3[21]);                                              
	      request.addProperty("CrackedGlazingComments",OB3[22]);                                               
	      request.addProperty("PreviousCorrectiveMeasureNoted",OB3[23]);                                                 
	      request.addProperty("PreviousCorrectiveMeasureOption",OB3[24]);                                                  
	      request.addProperty("PreviousCorrectiveMeasureComments",OB3[25]);                                                  
	      request.addProperty("BindingDoorOpeningNoted",OB3[26]);                                                  
	      request.addProperty("BindingDoorOpeningOption",OB3[27]);                                                 
	      request.addProperty("BindingDoorOpeningComments",OB3[28]);                                                   
	      request.addProperty("CreatedDate",cf.datewithtime);                                               
	      request.addProperty("ModifiedDate",cf.datewithtime);                                                             
	       envelope.setOutputSoapObject(request);
	  
		   HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		   androidHttpTransport.call(cf.NAMESPACE+"ExportSinkhole_Question_Part3",envelope);
		   String result =  envelope.getResponse().toString();
		   
		   return cf.check_Status(result);
	    }else
		{
			
			return true;
		}
	
	}	
	
	protected boolean Uploading_Observation2() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException{
		// TODO Auto-generated method stub
	SoapObject request = new SoapObject(cf.NAMESPACE,"ExportSinkhole_Question_Part2");
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.dotNet = true;
	/** set the value get from the policy holder table starts **/
	Cursor C_OB2= cf.SelectTablefunction(cf.Observation2, " where SH_OBS2_SRID='" + cf.encode(cf.selectedhomeid) + "'");
	
	if(C_OB2.getCount()==1)
	{
		  OB2=cf.setvalueToArray(C_OB2);/** set the value to the array for the easy usage **/
		  request.addProperty("InspectorID",cf.Insp_id);
		  
	      request.addProperty("SRID",OB2[1]);                                                   
	       request.addProperty("PoolDeckSlabNoted",OB2[2]);                             
	       request.addProperty("PoolDeckSlabComments",OB2[3]);                 
	       request.addProperty("PoolShellPlumbLevel",OB2[4]);                      
	       request.addProperty("PoolShellPlumbLevelComments",OB2[5]);                                        
	       request.addProperty("ExcessiveSettlement",OB2[6]);                            
	       request.addProperty("ExcessiveSettlementTypeofCrack",OB2[7]);                        
	       request.addProperty("ExcessiveSettlementLocation",OB2[8]);                     
	       request.addProperty("ExcessiveSettlementWidthofCrack",OB2[9]);                     
	       request.addProperty("ExcessiveSettlementLengthofCrack",OB2[10]);                  
	       request.addProperty("ExcessiveSettlementCleanliness",OB2[11]);                    
	       request.addProperty("ExcessiveSettlementProbableCause",OB2[12]);                     
	       request.addProperty("ExcessiveSettlementComments",OB2[13]);                    
	       request.addProperty("WallLeaningNoted",OB2[14]);                     
	       request.addProperty("WallLeaningComments",OB2[15]);                    
	       request.addProperty("WallsVisiblyNotLevel",OB2[16]);                   
	       request.addProperty("WallsVisiblyNotLevelComments",OB2[17]);                    
	       request.addProperty("WallsVisiblyBulgingNoted",OB2[18]);                  
	       request.addProperty("WallsVisiblyBulgingComments",OB2[19]);                   
	       request.addProperty("OpeningsOutofSquare",OB2[20]);                    
	       request.addProperty("OpeningsOutofSquareComments",OB2[21]);                   
	       request.addProperty("DamagedFinishesNoted",OB2[22]);                    
	       request.addProperty("DamagedFinishesComments",OB2[23]);                   
	       request.addProperty("SeperationCrackNoted",OB2[24]);                   
	       request.addProperty("SeperationCrackTypeofCrack","");                   
	       request.addProperty("SeperationCrackCracksLocation","");                   
	       request.addProperty("SeperationCrackWidthofCrack","");                   
	       request.addProperty("SeperationCrackLengthofCrack","");                  
	       request.addProperty("SeperationCrackCleanliness","");                    
	       request.addProperty("SeperationCrackProbableCause","");                    
	       request.addProperty("SeperationCrackOption",OB2[25]);                  
	       request.addProperty("SeperationCrackComments",OB2[26]);                    
	       request.addProperty("ExteriorOpeningCracksNoted",OB2[27]);                    
	       request.addProperty("ExteriorOpeningCracksTypeofCrack",OB2[28]);                   
	       request.addProperty("ExteriorOpeningCracksLocation",OB2[29]);                    
	       request.addProperty("ExteriorOpeningCracksWidthofCrack",OB2[30]);                   
	       request.addProperty("ExteriorOpeningCracksLengthofCrack",OB2[31]);                   
	       request.addProperty("ExteriorOpeningCracksCleanliness",OB2[32]);                   
	       request.addProperty("ExteriorOpeningCracksProbableCause",OB2[33]);                  
	       request.addProperty("ExteriorOpeningCracksComments",OB2[34]);                   
	       request.addProperty("ObservationSettlementNoted",OB2[35]);                  
	       request.addProperty("ObservationSettlementTypeofCrack",OB2[36]);                    
	       request.addProperty("ObservationSettlementLocation",OB2[37]);                   
	       request.addProperty("ObservationSettlementWidthofCrack",OB2[38]);                   
	       request.addProperty("ObservationSettlementLengthofCrack",OB2[39]);                    
	       request.addProperty("ObservationSettlementCleanliness",OB2[40]);                  
	       request.addProperty("ObservationSettlementProbableCause",OB2[41]);                  
	       request.addProperty("ObservationSettlementComments",OB2[42]);                   
	       request.addProperty("CreatedDate",cf.datewithtime);                
	       request.addProperty("ModifiedDate",cf.datewithtime);                          
           envelope.setOutputSoapObject(request);
           System.out.println("OB2[30]= "+OB2[30]);
	  System.out.println("request="+request);
		   HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		   androidHttpTransport.call(cf.NAMESPACE+"ExportSinkhole_Question_Part2",envelope);
		   String result =  envelope.getResponse().toString();
		   
		   return cf.check_Status(result);
	    }else
		{
		
			return true;
		}

}
	protected boolean Uploading_LargeTree() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException {
		// TODO Auto-generated method stub
	
	SoapObject request = new SoapObject(cf.NAMESPACE,"ExportSinkhole_LargeTree");
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.dotNet = true;
	/** set the value get from the policy holder table starts **/
	Cursor C_OB1T= cf.SelectTablefunction(cf.Observation1T, " where SH_OBS1T_SRID='" + cf.encode(cf.selectedhomeid) + "'");
	
	if(C_OB1T.getCount()==1)
	{
		  OB1T=cf.setvalueToArray(C_OB1T);/** set the value to the array for the easy usage **/
		  request.addProperty("InspectorID",cf.Insp_id);
		  
       request.addProperty("SRID",OB1T[1]);                                                   
       request.addProperty("Width",OB1T[2]);                             
       request.addProperty("Height",OB1T[3]);                 
       request.addProperty("Location",OB1T[4]);                      
       request.addProperty("CreatedDate",cf.datewithtime);                                        
                                
     envelope.setOutputSoapObject(request);
	  
	HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
	androidHttpTransport.call(cf.NAMESPACE+"ExportSinkhole_LargeTree",envelope);
	String result =  envelope.getResponse().toString();
	
	return cf.check_Status(result);
}else
{
	
	return true;
}

	}
	protected boolean Uploading_Observation1() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException {
		// TODO Auto-generated method stub
	
	SoapObject request = new SoapObject(cf.NAMESPACE,"ExportSinkhole_Question_Part1");
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.dotNet = true;
	/** set the value get from the policy holder table starts **/
	Cursor C_OB1= cf.SelectTablefunction(cf.Observation1, " where SH_OBS1_SRID='" + cf.encode(cf.selectedhomeid) + "'");
	
	if(C_OB1.getCount()==1)
	{
		  OB1=cf.setvalueToArray(C_OB1);/** set the value to the array for the easy usage **/
		  request.addProperty("InspectorID",cf.Insp_id);
		  
       request.addProperty("SRID",OB1[1]);                                                   
       request.addProperty("IrregularLandSurface",OB1[2]);                             
       request.addProperty("IrregularLandSurfaceLocation",OB1[3]);                 
       request.addProperty("IrregularLandSurfaceComments",OB1[4]);                      
       request.addProperty("VisibleBuriedDebris",OB1[5]);                                        
       request.addProperty("VisibleBuriedDebrisLocation",OB1[6]);                            
       request.addProperty("VisibleBuriedDebrisComments",OB1[7]);                        
       request.addProperty("IrregularLandSurfaceAdjacent",OB1[8]);                          
       request.addProperty("IrregularLandSurfaceAdjacentLocation",OB1[9]);              
       request.addProperty("IrregularLandSurfaceAdjacentComments",OB1[10]);          
       request.addProperty("SoilCollapse",OB1[11]);                                                  
       request.addProperty("SoilCollapseLocation",OB1[12]);                                      
       request.addProperty("SoilCollapseComments",OB1[13]);                                  
       request.addProperty("SoilErosionAroundFoundation",OB1[14]);                         
       request.addProperty("SoilErosionAroundFoundationOption",OB1[15]);               
       request.addProperty("SoilErosionAroundFoundationComments",OB1[16]);         
       request.addProperty("DrivewaysCracksNoted",OB1[17]);                                  
       request.addProperty("DrivewaysCracksOption",OB1[18]);                                 
       request.addProperty("DrivewaysCracksComments",OB1[19]);                           
       request.addProperty("UpliftDrivewaysSurface",OB1[20]);                                   
       request.addProperty("UpliftDrivewaysSurfaceOption",OB1[21]);                         
       request.addProperty("UpliftDrivewaysSurfaceComments",OB1[22]);                   
       request.addProperty("LargeTree",OB1[23]);                                                  
       request.addProperty("LargeTreeComments",OB1[24]);                                  
       request.addProperty("CypressTree",OB1[25]);                                              
       request.addProperty("CypressTreeLocation",OB1[26]);                                  
       request.addProperty("CypressTreeComments",OB1[27]);                            
       request.addProperty("LargeTreesRemoved",OB1[28]);                                 
       request.addProperty("LargeTreesRemovedLocation",OB1[29]);                     
       request.addProperty("LargeTreesRemovedComments",OB1[30]);                 
       request.addProperty("FoundationCrackNoted",OB1[31]);                             
       request.addProperty("FoundationTypeofCrack",OB1[32]);                            
       request.addProperty("FoundationLocation",OB1[33]);                                  
       request.addProperty("FoundationWidthofCrack",OB1[34]);                                      
       request.addProperty("FoundationLengthofCrack",OB1[35]);                                     
       request.addProperty("FoundationCleanliness",OB1[36]);                                               
       request.addProperty("FoundationProbableCause",OB1[37]);
       request.addProperty("FoundationComments",OB1[38]);           
       request.addProperty("RetainingWallsServiceable",OB1[39]);    
       request.addProperty("RetainingWallsServiceOption",OB1[40]);
       request.addProperty("RetainingWallsServiceComments",OB1[41]);
       request.addProperty("CreatedDate",cf.datewithtime);                              
       request.addProperty("ModifiedDate",cf.datewithtime);                             
      System.out.println("proprer ty"+request);
      envelope.setOutputSoapObject(request);
	  
	HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
	androidHttpTransport.call(cf.NAMESPACE+"ExportSinkhole_Question_Part1",envelope);
	String result =  envelope.getResponse().toString();
	System.out.println("result"+result);
	
	return cf.check_Status(result);
}else
{
	
	return true;
}

	}
    /** UPSERVATIN ENDS**/
	protected boolean Uploading_Photos() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {
		// TODO Auto-generated method stub
	/** set the value get from the policy holder table starts **/
		photores=true;
	Cursor C_PH= cf.SelectTablefunction(cf.ImageTable, " where SH_IM_SRID='" + cf.encode(cf.selectedhomeid) + "'");
	if(C_PH.getCount()>=1)
	{
		
		String chkimg = "true";
	
		int countchk = C_PH.getCount();
		C_PH.moveToFirst();
		double Totaltmp = 0.0;
		double temp_inc=0.0;
		double Temp_total=total;
		Totaltmp = ((double) increment_unit / (double) countchk);
		System.out.println("The incremental unit "+Temp_total+" errro "+Totaltmp+" Increamental unit "+increment_unit);
	//		double temptot = Totaltmp;
		for (int j = 0; j < C_PH.getCount(); j++) {
			Exportbartext="Uploading Photos   "+(j+1)+" / "+countchk;
			String mypath = cf.decode(C_PH.getString(C_PH.getColumnIndex("SH_IM_ImageName"))); // get the full path of the image 
			String temppath[] = mypath.split("/");
			int ss = temppath[temppath.length - 1].lastIndexOf(".");
			String tests = temppath[temppath.length - 1].substring(ss);
			String elevationttype;
			File f = new File(mypath);
			/** check the file is exist in the device **/
			if (f.exists()) {
				/** check the file extanstion is availabel  **/
				if (tests.equals("") || tests.equals("null")
						|| tests == null) {
					if (C_PH.getString(3).toString().equals("1")) {
						erromsg = "External Photos Tab in the image order"
								+ C_PH.getString(9).toString() ;
					} else if (C_PH.getString(3).toString().equals("2")) {
						erromsg = "Roof and attic Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("3")) {
						erromsg = "Ground and adjoining photos Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("4")) {
						erromsg = "Internal photos Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("5")) {
						erromsg = "Additional phots  Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("6")) {
						erromsg = "Homeownersignature  "
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("7")) {
						erromsg = "Paperworksignature"
								+ C_PH.getString(9).toString();
					} else {

						

					}
					Error_traker("plroblem uploading image in "+erromsg);// calling error msg track
					//return false;
				} else {

					
					if (C_PH.getString(3).toString().equals("1")) {
						elevationttype = "Ep"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("2")) {
						elevationttype = "RA"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("3")) {
						elevationttype = "GA"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("4")) {
						elevationttype = "IP"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("5")) {
						elevationttype = "AP"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("6")) {
						elevationttype = "Homeownersignature"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("7")) {
						elevationttype = "Paperworksignature"
								+ C_PH.getString(9).toString();
					} else {

						elevationttype = "EXTRA"
								+ C_PH.getString(9).toString();

					}
					cf.getInspecionTypeid(cf.selectedhomeid);
					elevationttype += tests;
					Bitmap bitmap = cf.ShrinkBitmap(cf.decode(C_PH.getString(C_PH
							.getColumnIndex("SH_IM_ImageName"))), 400, 400);

					MarshalBase64 marshal = new MarshalBase64();
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					bitmap.compress(CompressFormat.PNG, 100, out);
					byte[] raw = out.toByteArray();
					String imagetype="1";
					/**Change the image type for the server side code chage the EP to type 2 and GA to type 1 **/
                    if(C_PH.getString(C_PH.getColumnIndex("SH_IM_Elevation")).equals("1"))
                    {
                    	imagetype="2";
                    }
                    else if(C_PH.getString(C_PH.getColumnIndex("SH_IM_Elevation")).equals("2"))
                    {
                    	imagetype="1";
                    }
                    else
                    {
                    	imagetype=C_PH.getString(C_PH.getColumnIndex("SH_IM_Elevation"));
                    }
					SoapObject request = new SoapObject(cf.NAMESPACE,"ExportInsertInspectionPicture");
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
					envelope.dotNet = true;
					request.addProperty("InspectorID",cf.Insp_id);
					request.addProperty("SRID",cf.selectedhomeid);
					request.addProperty("inspectionTypeId", cf.Inspectiontypeid);
					request.addProperty("description", cf.decode(C_PH.getString(6)));
					request.addProperty("imageType", imagetype);
					request.addProperty("CreatedDate", cf.datewithtime);
					request.addProperty("ModifiedDate", cf.datewithtime);
					request.addProperty("ImageOrder", C_PH.getString(9));
					request.addProperty("ImageNameWithExtension",elevationttype);
					request.addProperty("imgByte", raw);
					envelope.setOutputSoapObject(request);
					marshal.register(envelope);
					System.out.println("proprer ty"+request);
					envelope.setOutputSoapObject(request);
					HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
					androidHttpTransport.call(cf.NAMESPACE+"ExportInsertInspectionPicture",envelope);
					String result =  envelope.getResponse().toString();
					if(result.equals("false"))
					{
						if(photores==true){
						Error_traker("Images have not exported completely." );}
						photores=false;
						
					}
					
					System.out.println("result"+result);
				//	total=100;
					//return cf.check_Status(result);
					C_PH.moveToNext();
					try {
						out.close();
					} catch (IOException e) {

					}
				
				}
				
			}
			else
			{
				if (tests.equals("") || tests.equals("null")
						|| tests == null) {
					if (C_PH.getString(3).toString().equals("1")) {
						erromsg = "External Photos Tab in the image order"
								+ C_PH.getString(9).toString() ;
					} else if (C_PH.getString(3).toString().equals("2")) {
						erromsg = "Roof and attic Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("3")) {
						erromsg = "Ground and adjoining photos Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("4")) {
						erromsg = "Internal photos Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("5")) {
						erromsg = "Additional phots  Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("6")) {
						erromsg = "Homeownersignature  "
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("7")) {
						erromsg = "Paperworksignature"
								+ C_PH.getString(9).toString();
					} 
					Error_traker("please check the image in "+erromsg +" available " );
			}
			/** check the file is exist in the device Ends **/
		}
			
				if (total <= Temp_total+increment_unit) {
					temp_inc +=Totaltmp;
					total = Temp_total + (int) (temp_inc);
					System.out.println("total"+total+"current "+temp_inc);         
				} else {
					total = Temp_total+increment_unit;
				}
			
			//Totaltmp += temptot;
	}	

		return true;

	}else
	{
		
		return true;
	}
	
		
	}
	protected boolean Uploading_feedback() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {
		// TODO Auto-generated method stub

	if(feedback_Info()) /** call the feed back info to upload **/
	{
		
		Bitmap bitmap;
	 /** Uploading Feed back document Starts here   **/
		
		
		Cursor C_feed_Doc= cf.SelectTablefunction(cf.FeedBackDocumentTable, " where SH_FI_SRID='" + cf.selectedhomeid + "' ");
		//int countchk=C_feed_Doc.getCount();
		
	//	System.out.println("issuesin the table is FBD "+C_feed_Doc.getCount());
		if(C_feed_Doc.getCount()>=1)
		{
	
			MarshalBase64 marshal = new MarshalBase64();
			ByteArrayOutputStream out = null;
		

			C_feed_Doc.moveToFirst();
			
			int countchk = C_feed_Doc.getCount();
			double Totaltmp = 0.0;
			double Temp_total=total;double temp_inc=0.0;
			Totaltmp = ((double) increment_unit / (double) countchk);
			System.out.println("The incremental unit "+Temp_total+" errro "+Totaltmp+" Increamental unit "+increment_unit);
			for (int j = 0; j < C_feed_Doc.getCount(); j++) 
			{
				cf.getInspecionTypeid(cf.selectedhomeid);
				boolean offuse=(C_feed_Doc.getString(6).equals("1")) ? true:false; 
						Exportbartext="Uploading feedback documents   "+(j+1)+" / "+countchk;
				SoapObject request = new SoapObject(cf.NAMESPACE,"ExportFeedbackDocument");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				String substring = cf.decode(C_feed_Doc.getString(C_feed_Doc.getColumnIndex("SH_FI_FileName")));
				request.addProperty("InspectorID", cf.Insp_id);
				request.addProperty("SRID", C_feed_Doc.getString(1));
				request.addProperty("InspectionTypeId", cf.Inspectiontypeid);
				request.addProperty("DocumentTitle", cf.decode(C_feed_Doc.getString(2)));
				request.addProperty("IsOfficeUse", offuse);
				request.addProperty("CreatedDate", cf.decode(C_feed_Doc.getString(7)));
				request.addProperty("ModifiedDate", cf.decode(C_feed_Doc.getString(8)));
				request.addProperty("ImageOrder", C_feed_Doc.getInt(5));
				System.out.println("comes after the proper added"+substring);
				if (substring.endsWith(".pdf")) 
				{
					File dir = Environment.getExternalStorageDirectory();
					if (substring.startsWith("file:///")) 
					{
						substring = substring.substring(11);
					}
					else
					{
						substring = substring;
					}
					File assist = new File(substring);
					if (assist.exists()) 
					{
						String mypath = substring;
						String temppath[] = mypath.split("/");
						int ss = temppath[temppath.length - 1].lastIndexOf(".");// substring(".");
						String tests = temppath[temppath.length - 1].substring(ss);
						String namedocument;
						if (tests.equals(".pdf")) 
						{
							namedocument = "pdf_document" + j + ".pdf";
							request.addProperty("ImageNameWithExtension",
							cf.decode(namedocument));
							try
							{
								InputStream fis = new FileInputStream(assist);
								long length = assist.length();
								byte[] bytes = new byte[(int) length];
								int offset = 0;
								int numRead = 0;
								while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0)
								{
									offset += numRead;
								}
								strBase64 = Base64.encode(bytes);
								System.out.println("request"+request.toString());
								request.addProperty("imgByte", strBase64);
								
								envelope.setOutputSoapObject(request);
								HttpTransportSE httpTransport = new HttpTransportSE(
										cf.URL);
								HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
								androidHttpTransport.call(cf.NAMESPACE+"ExportFeedbackDocument",envelope);
								String result =  envelope.getResponse().toString();
								
								System.out.println("Result is "+j+result);
								cf.check_Status(result);
							}
							catch (Exception e) 
							{ System.out.println("issues in error"+e);
								Error_traker("You have problem in uploading documentin the feed back document tab Document Title"+cf.decode(C_feed_Doc.getString(2)) );
								/*cf.errorlogmanage(" Problem while opening the file ="+ e.getMessage()+ " path of the file is ="+ substring,selectedItem.toString());
								return "false";*/
							}
						}
						else
						{
							System.out.println("issues in extention missing");
							Error_traker("You have problem in uploading documentin the feed back document tab Document Title"+cf.decode(C_feed_Doc.getString(2)) );
							
						}

						
					
					}
					else
					{
						System.out.println("issues in error file not available");
						Error_traker("You have problem in uploading documentin the feed back document tab Document Title"+cf.decode(C_feed_Doc.getString(2)) );
					}
				} 
				else
				{
					String mypath = substring;
					String temppath[] = mypath.split("/");
					int ss = temppath[temppath.length - 1].lastIndexOf(".");// substring(".");
					String tests = temppath[temppath.length - 1].substring(ss);
					String elevationttype;
					File f = new File(substring);
					if (f.exists()) 
					{
						/**check weather the extention is not null**/
						if (tests.equals("") || tests.equals("null")|| tests == null) 
						{
							Error_traker("You have problem in uploading documentin the feed back document tab Document Title"+cf.decode(C_feed_Doc.getString(2)) );
							System.out.println("error file not extention not found");
							
						}
						else
						{
							elevationttype = "feedbackimage" + j + tests;
							request.addProperty("ImageNameWithExtension",
									elevationttype);
							bitmap = cf.ShrinkBitmap(substring, 800, 800);
							
							out = new ByteArrayOutputStream();
							bitmap.compress(CompressFormat.PNG, 100, out);
							byte[] raw = out.toByteArray();
							System.out.println("request"+request.toString());
							request.addProperty("imgByte", raw);
							marshal.register(envelope);
						    // System.out.println("request"+request.toString());
							envelope.setOutputSoapObject(request);
							HttpTransportSE httpTransport = new HttpTransportSE(
									cf.URL);
							HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
							androidHttpTransport.call(cf.NAMESPACE+"ExportFeedbackDocument",envelope);
							String result =  envelope.getResponse().toString();
							System.out.println("Result is "+j+result);
							cf.check_Status(result);
							try 
							{
								out.close();
							}
							catch (IOException e) 
							{
							}
						}

						
					}
					else
					{
						System.out.println("error file not exist");
						Error_traker("You have problem in uploading documentin the feed back document tab Document Title"+cf.decode(C_feed_Doc.getString(2)) );
					}
					
				}
					
				
				
			//	System.out.println("result"+ envelope.getResponse());
				
				
				C_feed_Doc.moveToNext();

				if (total <= Temp_total+increment_unit) {
					temp_inc +=Totaltmp;
					total = Temp_total + (int) (temp_inc);
					System.out.println("total"+total+"current "+temp_inc);         
				} else {
					total = Temp_total+increment_unit;
				}
			}
				
			
	}
	else
	{
	   		
	}
	/** Uploading Feed back document Ends here   **/
	}
	/** feed back info table error traking **/
	else
	{
		Error_traker("You have problem in the Feed back info tables data Exporting" );
	}
	return true;
			
}
	private boolean feedback_Info() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(cf.NAMESPACE,"ExportRevisedWSIFeedBack");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		
		Cursor C_feed_inf= cf.SelectTablefunction(cf.FeedBackInfoTable, " where SH_FI_Srid='" + cf.selectedhomeid + "' ");
		if(C_feed_inf.getCount()==1)
		{
			feed_info=cf.setvalueToArray(C_feed_inf);/** set the value to the array for the easy usage **/
			request.addProperty("InspectorID",cf.Insp_id);                   
			request.addProperty("Srid",cf.selectedhomeid);                             
			request.addProperty("Weather","");                      
			request.addProperty("Temperature","");                
			request.addProperty("IsHOSatisfied",0);              
			request.addProperty("IsCusServiceCompltd",feed_info[2]);   
			request.addProperty("IsInspectionPaperAvbl",feed_info[4]);  
			request.addProperty("IsManufacturerInfo",feed_info[5]);       
			request.addProperty("PresentatInspection",feed_info[3]);    
			request.addProperty("TxtAckFile","");                 
			request.addProperty("AcknowledgeFile","");        
			request.addProperty("TxtCseFile","");                 
			request.addProperty("CSEFile","");                    
			request.addProperty("TxtOirFile","");                  
			request.addProperty("OIRFile","");                     
			request.addProperty("TxtSurveyFile","");            
			request.addProperty("SurveyFile","");                
			request.addProperty("TxtSignUpFile","");                 
			request.addProperty("SignUpFile","");                     
			request.addProperty("TxtOtherFile",feed_info[8]);                   
			request.addProperty("OtherFile",feed_info[8]);                       
			request.addProperty("Feedback",feed_info[6]);                      
			request.addProperty("Addendum","");                     
			request.addProperty("CreatedOn",feed_info[7]);                     
			request.addProperty("OfficeComments","");            
			System.out.println("result"+request);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			androidHttpTransport.call(cf.NAMESPACE+"ExportRevisedWSIFeedBack",envelope);
			String result =  envelope.getResponse().toString();
			System.out.println("result"+ envelope.getResponse());
			
			return cf.check_Status(result);
		}
		return true;
	}
	private void Error_traker(String ErrorMsg) {
		// TODO Auto-generated method stub
		System.out.println("error msg"+ErrorMsg);
		if(erro_trce==null)
		{
			erro_trce=new String[1];
			
			erro_trce[0]=ErrorMsg;
		}
		else
		{
			
			String tmp[]=new String[this.erro_trce.length+1];
			int i;
			for(i =0;i<erro_trce.length;i++)
			{
				
				tmp[i]=erro_trce[i];
			}
		
			tmp[tmp.length-1]=ErrorMsg;
			erro_trce=null;
			erro_trce=tmp.clone();
		}
	}

	private void dbquery() {

			int k = 1;
			cf.data = null;
			cf.inspdata = "";
			cf.sql = "select * from " + cf.policyholder;
			if (!cf.res.equals("")) {

				cf.sql += " where (SH_PH_FirstName like '%" + cf.encode(cf.res)
						+ "%' or SH_PH_LastName like '%" + cf.encode(cf.res)
						+ "%' or SH_PH_Policyno like '%" + cf.encode(cf.res)
						+ "%') and SH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'";
				if(cf.onlstatus.equals("export"))
				{
					cf.sql += " and SH_PH_IsInspected='1' ";
				}
				else if(cf.onlstatus.equals("Reexport"))
				{
					cf.sql += " and SH_PH_IsInspected='2' and SH_PH_IsUploaded='1' ";
				}

			} else {
				if(cf.onlstatus.equals("export"))
				{
					cf.sql += " where SH_PH_IsInspected='1'  and SH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "' ";
				}
				else if(cf.onlstatus.equals("Reexport"))
				{
					cf.sql += " where SH_PH_IsInspected='2' and SH_PH_IsUploaded='1' and SH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "' ";
				}

			}
			System.out.println("Type = "+cf.onlstatus+"Quesry : "+cf.sql);
			Cursor cur = cf.sh_db.rawQuery(cf.sql, null);
			cf.rws = cur.getCount();
			title.setText("Total Record : " + cf.rws);
			cf.data = new String[cf.rws];
			cf.countarr = new String[cf.rws];
			System.out.println("count "+cf.sql);
			int i=0,j = 0;
			cur.moveToFirst();
			this.cf.data = new String[cf.rws];
			if (cur.getCount() >= 1) {
				do {
					cf.inspdata += " "
							+ cf.decode(cur.getString(cur
									.getColumnIndex("SH_PH_FirstName"))) + " ";
					this.cf.data[i] += cf.decode(cur.getString(cur
							.getColumnIndex("SH_PH_LastName"))) + " | ";
					this.cf.data[i] += cf.decode(cur.getString(cur
							.getColumnIndex("SH_PH_Policyno")))
							+ " \n ";
					this.cf.data[i]+= cf.decode(cur.getString(cur
							.getColumnIndex("SH_PH_Address1"))) + " | ";
					this.cf.data[i] += cf.decode(cur.getString(cur
							.getColumnIndex("SH_PH_City"))) + " | ";
					this.cf.data[i] += cf.decode(cur.getString(cur
							.getColumnIndex("SH_PH_State"))) + " | ";
					this.cf.data[i] += cf.decode(cur.getString(cur
							.getColumnIndex("SH_PH_County"))) + " | ";
					this.cf.data[i] += cf.decode(cur.getString(cur.getColumnIndex("SH_PH_Zip")))
							+ " \n ";
					this.cf.data[i] += cf.decode(cur.getString(cur
							.getColumnIndex("SH_Schedule_ScheduledDate"))) + " | ";
					this.cf.data[i] += cf.decode(cur.getString(cur
							.getColumnIndex("SH_Schedule_InspectionStartTime"))) + " - ";
					this.cf.data[i] += cf.decode(cur.getString(cur
							.getColumnIndex("SH_Schedule_InspectionEndTime"))) ;


					cf.countarr[j] = cf.decode(cur.getString(cur.getColumnIndex("SH_PH_SRID")));
					j++;

					if (this.cf.data[i].contains("null")) {
						this.cf.data[i] = this.cf.data[i].replace("null", "");
					}
					if (this.cf.data[i].contains("N/A |")) {
						this.cf.data[i] = this.cf.data[i].replace("N/A |", "");
					}
					if (this.cf.data[i].contains("N/A - N/A")) {
						this.cf.data[i] = this.cf.data[i].replace("N/A - N/A", "");
					}
					i++;
				} while (cur.moveToNext());
				cf.search_text.setText("");
				display();
			} else {
			//	cf.ShowToast("Sorry, No records found ", 1);
				cf.onlinspectionlist.removeAllViews();
			}

		}
    private void display() {
		    cf.onlinspectionlist.removeAllViews();
			cf.sv = new ScrollView(this);
			cf.onlinspectionlist.addView(cf.sv);

			final LinearLayout l1 = new LinearLayout(this);
			l1.setOrientation(LinearLayout.VERTICAL);
			cf.sv.addView(l1);

			
			cf.tvstatus = new TextView[cf.rws];
			cf.deletebtn = new Button[cf.rws];
				for (int i = 0; i < cf.data.length; i++) {
					LinearLayout l2 = new LinearLayout(this);
					l2.setOrientation(LinearLayout.HORIZONTAL);
					l1.addView(l2);

					LinearLayout lchkbox = new LinearLayout(this);
					LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
							 ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					paramschk.topMargin = 8;
					paramschk.leftMargin = 20;
					paramschk.bottomMargin = 10;
					l2.addView(lchkbox);

					cf.tvstatus[i] = new TextView(this);
					cf.tvstatus[i].setMinimumWidth(580);
					cf.tvstatus[i].setMaxWidth(580);
					cf.tvstatus[i].setTag("textbtn" + i);
					cf.tvstatus[i].setText(cf.data[i]);
					cf.tvstatus[i].setTextColor(Color.WHITE);
					cf.tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
					lchkbox.addView(cf.tvstatus[i], paramschk);
					
					LinearLayout ldelbtn = new LinearLayout(this);
					LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
							93, 37);
					paramsdelbtn.rightMargin = 10;
					paramsdelbtn.bottomMargin = 10;
					l2.addView(ldelbtn);
					
					
                 /** DELETE BUTTON NO NEED FOR THE EXPORT AND ONCLICK EVENT SETTIN INSPECTION SO I JUST HIDE IT NOW   **/
					cf.tvstatus[i].setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									String getidofselbtn = v.getTag().toString();
									final String repidofselbtn = getidofselbtn.replace(
											"textbtn", "");
									final int cvrtstr = Integer.parseInt(repidofselbtn);
									dt = cf.countarr[cvrtstr];
									/** Set the alert listener for the export option **/
									System.out.println("dt= "+dt);
									if(cf.onlstatus.equals("Reexport"))
									{
										cf.selectedhomeid=dt;
										String source = "<b><font color=#00FF33>Retrieving data. Please wait..."
												+ "</font></b>";
										cf.pd = ProgressDialog.show(ExportInspection.this, "", Html.fromHtml(source), true);
										Thread thread = new Thread(ExportInspection.this);
										thread.start();
										/*try {
											cf.exprtcnt=cf.fn_CheckExportCount(cf.Insp_id,dt);
										} catch (SocketException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
											
										} catch (NetworkErrorException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (IOException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (TimeoutException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (XmlPullParserException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (Exception e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
										
										System.out.println("reexport"+cf.exprtcnt);
											    cf.alerttitle="Re-export";
											    System.out.println(Html.fromHtml("This inspection has been exported already.Details :"+cf.exprtcnt+
													    "Are you sure want to Re-export again?"));
											    cf.alertcontent="This inspection has been exported already."+"<br /><br />"+"<b>Details :</b>"+cf.exprtcnt+
													    "<b>Are you sure want to Re-export again?</b>";
											    final Dialog dialog1 = new Dialog(ExportInspection.this,android.R.style.Theme_Translucent_NoTitleBar);
												dialog1.getWindow().setContentView(R.layout.alertsync);
												TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
												txttitle.setText( cf.alerttitle);
												TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
												txt.setText(Html.fromHtml( cf.alertcontent));
												Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
												Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
												btn_yes.setOnClickListener(new OnClickListener()
												{
								                	@Override
													public void onClick(View arg0) {
														// TODO Auto-generated method stub
														dialog1.dismiss();
														
														fn_export();
													}
													
												});
												btn_cancel.setOnClickListener(new OnClickListener()
												{

													@Override
													public void onClick(View arg0) {
														// TODO Auto-generated method stub
														dialog1.dismiss();
														
													}
													
												});
												dialog1.setCancelable(false);
												dialog1.show();*/
										//}
										

									}
									else
									{
										fn_export();
									}
																		
								}

								
							});
							
				/** DELETE BUTTON NO NEED FOR THE EXPORT INSPECTION SO I JUST HIDE IT NOW ENDS   **/
				        }
				
				
			}	
    protected void fn_export() {
		// TODO Auto-generated method stub
    	final Dialog alert=cf.showalert();
		Button btn_helpclose = (Button) alert.findViewById(R.id.helpclose_ex);
		Button btn_helpclose_t = (Button) alert.findViewById(R.id.helpclose_ex_t);
		btn_helpclose.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				for(int i=1;i<Export_chk.length;i++)
				{
					Export_chk[i]="0";
				}
				System.out.println("closed");
				alert.setCancelable(true);
				alert.dismiss();
			}
			
		});
		btn_helpclose_t.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				for(int i=1;i<Export_chk.length;i++)
				{
					Export_chk[i]="0";
				}
				System.out.println("closed");
				alert.setCancelable(true);
				alert.dismiss();
			}
			
		});
		alert_chk[0]=(CheckBox) alert.findViewById(R.id.Export_chk_all);
		alert_chk[1]=(CheckBox) alert.findViewById(R.id.Export_chk_GI);
		alert_chk[2]=(CheckBox) alert.findViewById(R.id.Export_chk_SQ);
		alert_chk[3]=(CheckBox) alert.findViewById(R.id.Export_chk_BI);
		alert_chk[4]=(CheckBox) alert.findViewById(R.id.Export_chk_OB1);
		alert_chk[5]=(CheckBox) alert.findViewById(R.id.Export_chk_OB2);
		alert_chk[6]=(CheckBox) alert.findViewById(R.id.Export_chk_OB3);
		alert_chk[7]=(CheckBox) alert.findViewById(R.id.Export_chk_OB4);
		alert_chk[8]=(CheckBox) alert.findViewById(R.id.Export_chk_PH);
		alert_chk[9]=(CheckBox) alert.findViewById(R.id.Export_chk_FB);
		alert_chk[0].setOnClickListener(new clicker());
		alert_chk[1].setOnClickListener(new clicker());
		alert_chk[2].setOnClickListener(new clicker());
		alert_chk[3].setOnClickListener(new clicker());
		alert_chk[4].setOnClickListener(new clicker());
		alert_chk[5].setOnClickListener(new clicker());
		alert_chk[6].setOnClickListener(new clicker());
		alert_chk[7].setOnClickListener(new clicker());
		alert_chk[8].setOnClickListener(new clicker());
		alert_chk[9].setOnClickListener(new clicker());
		Button export=(Button) alert.findViewById(R.id.export_now);
		LinearLayout lin = (LinearLayout)alert.findViewById(R.id.ExportOption);
		lin.setVisibility(View.VISIBLE);
		LinearLayout lin1= (LinearLayout)alert.findViewById(R.id.maintable);
		lin1.setVisibility(View.GONE);
		export.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alert.setCancelable(true);
				alert.dismiss();
				int j=0;
				double incre_unit;
				for(int i=1;i<Export_chk.length;i++)
				{
					if(Export_chk[i].equals("1"))
						j++;
				}
				try
				{
					incre_unit=80.00/Double.parseDouble(String.valueOf(j));
					System.out.println("increament unit is "+incre_unit+" j = "+j);
				}
				catch(Exception e)
				{
					incre_unit=10.00;
				}
				if(j>=1)
				{
				/** export will start here **/
				StartExport(dt,incre_unit);
				/** export will Ends here **/
				}
				else
				{
					cf.ShowToast("Please select atleast one option ", 1);
				}
			}
		});
		alert.setCancelable(false);
		alert.show();

	}
	private class clicker implements OnClickListener 
			{
			
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					switch (v.getId()) {
					case R.id.Export_chk_all:
						if(alert_chk[0].isChecked())
						{
							Export_chk[0]="1";
							Export_chk[1]="1";
							Export_chk[2]="1";
							Export_chk[3]="1";
							Export_chk[4]="1";
							Export_chk[5]="1";
							Export_chk[6]="1";
							Export_chk[7]="1";
							Export_chk[8]="1";
							Export_chk[9]="1";
							alert_chk[1].setChecked(true);
							alert_chk[2].setChecked(true);
							alert_chk[3].setChecked(true);
							alert_chk[4].setChecked(true);
							alert_chk[5].setChecked(true);
							alert_chk[6].setChecked(true);
							alert_chk[7].setChecked(true);
							alert_chk[8].setChecked(true);
							alert_chk[9].setChecked(true);
						}
						else
						{
							Export_chk[0]="0";
						}
						
					break;
					case R.id.Export_chk_GI:
						if(alert_chk[1].isChecked())
						{
							Export_chk[1]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[1]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.Export_chk_SQ:
						if(alert_chk[2].isChecked())
						{
							Export_chk[2]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[2]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.Export_chk_BI:
						if(alert_chk[3].isChecked())
						{
							Export_chk[3]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[3]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.Export_chk_OB1:
						if(alert_chk[4].isChecked())
						{
							Export_chk[4]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[4]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.Export_chk_OB2:
						if(alert_chk[5].isChecked())
						{
							Export_chk[5]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[5]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.Export_chk_OB3:
						if(alert_chk[6].isChecked())
						{
							Export_chk[6]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[6]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.Export_chk_OB4:
						if(alert_chk[7].isChecked())
						{
							Export_chk[7]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[7]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.Export_chk_PH:
						
						if(alert_chk[8].isChecked())
						{
							
							Export_chk[8]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[8]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.Export_chk_FB:
						if(alert_chk[9].isChecked())
						{
							Export_chk[9]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[9]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.completed_insp:
							Intent inte = new Intent(getApplicationContext(),ExportInspection.class);
							System.out.println("issues not in hewrw the re export ");
							inte.putExtra("type", "Reexport");
							startActivity(inte);
					break;
					case R.id.export_insp:
						Intent inte1 = new Intent(getApplicationContext(),ExportInspection.class);
						inte1.putExtra("type", "export");
						startActivity(inte1);
						break;
					default:
						break;
					}

					
				}
			} 
/** PROGRESS DILOG WITH LODING DATA INFORMATION **/		
@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 1:
			System.out.println("there is iisues");
			
			progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progDialog.setMax(maxBarValue);
			progDialog.setMessage(Html.fromHtml("Please be patient as we export your file. When complete you must log into www.paperlessinspectors.com and submit this file for auditing.  \n"+Exportbartext));
			progDialog.setCancelable(false);
			progThread = new ProgressThread(handler1);
			progThread.start();
			return progDialog;
		default:
			return null;
		}
	}
 
	class ProgressThread extends Thread {


	// Class constants defining state of the thread
	final static int DONE = 0;

	Handler mHandler;

	ProgressThread(Handler h) {
		mHandler = h;
	}

	@Override
	public void run() {
		mState = RUNNING;
		
		while (mState == RUNNING) {
			try {
				
				// Control speed of update (but precision of delay not
				// guaranteed)
				Thread.sleep(delay);
			} catch (InterruptedException e) {
				//Log.e("ERROR", "Thread was Interrupted");
			}

			Message msg = mHandler.obtainMessage();
			Bundle b = new Bundle();
			b.putInt("total", (int)total);
			msg.setData(b);
			mHandler.sendMessage(msg);

		}
	}

	public void setState(int state) {
		mState = state;
	}

}
	/** Upload policy holder information Starts **/
	final Handler handler1 = new Handler() {

		public void handleMessage(Message msg) {
			// Get the current value of the variable total from the message data
			// and update the progress bar.
			
			int total = msg.getData().getInt("total");
			
			progDialog.setProgress(total);
			progDialog.setMessage(Html.fromHtml("Please be patient as we export your file.<br> When complete you must log into www.paperlessinspectors.com and submit this file for auditing. <br> "+Exportbartext));
			//Exportbartext="";
			if (total == 100) {
				progDialog.setCancelable(true);
				dismissDialog(typeBar);
				
				//handler2.sendMessage(msg2);
				handler2.sendEmptyMessage(0);
				progThread.setState(ProgressThread.DONE);


			}

		}
	};
	final Handler handler2 = new Handler() {

		private AlertDialog alertDialog;

		@Override
		public void handleMessage(Message msg) {
		//	progThread.destroy();
//System.out.println("handler2"+cf.Chk_Inspector);
			if(cf.Chk_Inspector.equals("true")){
			
		
			ImageView sta_im[]=new ImageView[9];
			TableRow sta_row[]=new TableRow[9];
			// TODO Auto-generated method stub
			/** Set the  dilog to cancel if any thing shown **/
			if(dialog1!=null)
			{ 
				if(dialog1.isShowing())
				{
					dialog1.dismiss();
				}
			}
			/** Set the  dilog to cancel if any thing  shown ends **/
				dialog1 = new Dialog(ExportInspection.this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog1.getWindow().setContentView(R.layout.alertexportcompleted);
				
				sta_chk[0]=(CheckBox) dialog1.findViewById(R.id.sta_chk1);
				sta_chk[1]=(CheckBox) dialog1.findViewById(R.id.sta_chk2);
				sta_chk[2]=(CheckBox) dialog1.findViewById(R.id.sta_chk3);
				sta_chk[3]=(CheckBox) dialog1.findViewById(R.id.sta_chk4);
				sta_chk[4]=(CheckBox) dialog1.findViewById(R.id.sta_chk5);
				sta_chk[5]=(CheckBox) dialog1.findViewById(R.id.sta_chk6);
				sta_chk[6]=(CheckBox) dialog1.findViewById(R.id.sta_chk7);
				sta_chk[7]=(CheckBox) dialog1.findViewById(R.id.sta_chk8);
				sta_chk[8]=(CheckBox) dialog1.findViewById(R.id.sta_chk9);
				
				sta_row[0]=(TableRow) dialog1.findViewById(R.id.sta_row1);
				sta_row[1]=(TableRow) dialog1.findViewById(R.id.sta_row2);
				sta_row[2]=(TableRow) dialog1.findViewById(R.id.sta_row3);
				sta_row[3]=(TableRow) dialog1.findViewById(R.id.sta_row4);
				sta_row[4]=(TableRow) dialog1.findViewById(R.id.sta_row5);
				sta_row[5]=(TableRow) dialog1.findViewById(R.id.sta_row6);
				sta_row[6]=(TableRow) dialog1.findViewById(R.id.sta_row7);
				sta_row[7]=(TableRow) dialog1.findViewById(R.id.sta_row8);
				sta_row[8]=(TableRow) dialog1.findViewById(R.id.sta_row9);
				
				sta_im[0]=(ImageView) dialog1.findViewById(R.id.sta_im1);
				sta_im[1]=(ImageView) dialog1.findViewById(R.id.sta_im2);
				sta_im[2]=(ImageView) dialog1.findViewById(R.id.sta_im3);
				sta_im[3]=(ImageView) dialog1.findViewById(R.id.sta_im4);
				sta_im[4]=(ImageView) dialog1.findViewById(R.id.sta_im5);
				sta_im[5]=(ImageView) dialog1.findViewById(R.id.sta_im6);
				sta_im[6]=(ImageView) dialog1.findViewById(R.id.sta_im7);
				sta_im[7]=(ImageView) dialog1.findViewById(R.id.sta_im8);
				sta_im[8]=(ImageView) dialog1.findViewById(R.id.sta_im9);
				
				final Button detailbut =(Button) dialog1.findViewById(R.id.error_detail); 
				
				final LinearLayout tlist=(LinearLayout) dialog1.findViewById(R.id.error_reports);
				if(erro_trce!=null)
				{
					if(erro_trce.length>=1)
					{
						/*ArrayAdapter arrayAdapter = new ArrayAdapter(ExportInspection.this, android.R.layout.simple_list_item_1,erro_trce);
						tlist.setAdapter(arrayAdapter);
*						
*/						//LinearLayout litop= new LinearLayout(ExportInspection.this);
						LinearLayout.LayoutParams lm= new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,2);
						/*litop.setLayoutParams(lm);
						litop.setBackgroundColor(Color.GREEN);*/
						
					for(int i=0;i<erro_trce.length;i++)
					{
						
						
						TextView tv1=new TextView(ExportInspection.this);
						tv1.setText(erro_trce[i]);
						tv1.setTextSize(14);
						tv1.setTextColor(Color.parseColor("#000000"));
						LinearLayout litop= new LinearLayout(ExportInspection.this);
						litop.setLayoutParams(lm);
						litop.setBackgroundColor(Color.GREEN);
						tlist.addView(litop);
						tlist.addView(tv1);
						
						
					}
					
					}
				}
				
				
				/** Set the tick icon , cross icon and visibility for the alert**/
				
				if(Export_chk[1].equals("1"))
				{
					sta_row[0].setVisibility(View.VISIBLE);
					if(Export_status[0])
					{
						sta_im[0].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[0].setBackgroundResource(R.drawable.cross_icon);
					}
				}
				if(Export_chk[2].equals("1"))
				{
					sta_row[1].setVisibility(View.VISIBLE);
					if(Export_status[1])
					{
						sta_im[1].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[1].setBackgroundResource(R.drawable.cross_icon);
					}
				}
				if(Export_chk[3].equals("1"))
				{
					sta_row[2].setVisibility(View.VISIBLE);
					if(Export_status[2])
					{
						sta_im[2].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[2].setBackgroundResource(R.drawable.cross_icon);
					}
				}
				if(Export_chk[4].equals("1"))
				{
					sta_row[3].setVisibility(View.VISIBLE);
					if(Export_status[3])
					{
						sta_im[3].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[3].setBackgroundResource(R.drawable.cross_icon);
					}
				}
				if(Export_chk[5].equals("1"))
				{
					sta_row[4].setVisibility(View.VISIBLE);
					if(Export_status[4])
					{
						sta_im[4].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[4].setBackgroundResource(R.drawable.cross_icon);
					}
				}
				if(Export_chk[6].equals("1"))
				{
					sta_row[5].setVisibility(View.VISIBLE);
					if(Export_status[5])
					{
						sta_im[5].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[5].setBackgroundResource(R.drawable.cross_icon);
					}
				}
				if(Export_chk[7].equals("1"))
				{
					sta_row[6].setVisibility(View.VISIBLE);
					if(Export_status[6])
					{
						sta_im[6].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[6].setBackgroundResource(R.drawable.cross_icon);
					}
				}
				if(Export_chk[8].equals("1"))
				{
					sta_row[7].setVisibility(View.VISIBLE);
					if(Export_status[7])
					{
						sta_im[7].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[7].setBackgroundResource(R.drawable.cross_icon);
					}
				}if(Export_chk[9].equals("1"))
				{
					sta_row[8].setVisibility(View.VISIBLE);
					if(Export_status[8])
					{
						sta_im[8].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[8].setBackgroundResource(R.drawable.cross_icon);
					}
				}
		
				Button btn_helpclose = (Button) dialog1.findViewById(R.id.helpclose_ex_t);
				Button btn_helpclose_x = (Button) dialog1.findViewById(R.id.helpclose_ex);
				
				
				Button select = (Button) dialog1.findViewById(R.id.re_export_now);
				btn_helpclose.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						for(int i=1;i<Export_chk.length;i++)
						{
							Export_chk[i]="0";
						}
						dialog1.setCancelable(true);
						dialog1.dismiss();
						dialog1.setCancelable(true);
						dialog1.dismiss();
						alertDialog = new AlertDialog.Builder(ExportInspection.this)
						.create();
						alertDialog.setMessage(status);
						alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								  Intent inte = new Intent(getApplicationContext(),ExportInspection.class);
								 
								  inte.putExtra("type", "Reexport");
								  startActivity(inte);
                                   
							}
						});
				alertDialog.show();
					
					}
					
				});
				btn_helpclose_x.setOnClickListener(new OnClickListener()
				{

					@Override 
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						for(int i=1;i<Export_chk.length;i++)
						{
							Export_chk[i]="0";
						}
						System.out.println("closed");
						dialog1.setCancelable(true);
						dialog1.dismiss();
						alertDialog = new AlertDialog.Builder(ExportInspection.this)
						.create();
						alertDialog.setMessage(status);
						alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								 Intent inte = new Intent(getApplicationContext(),ExportInspection.class);
								  inte.putExtra("type", "Reexport");
								  startActivity(inte);

							}
						});
				alertDialog.show();
						
					}

					
					
				});
				
				detailbut.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(tlist.getVisibility()==View.VISIBLE)
						{
							detailbut.setText("Hide failure reports");
							
							tlist.setVisibility(View.GONE);
						}
						else
						{
							detailbut.setText("Show failure reports");
							tlist.setVisibility(View.VISIBLE);
						}
					
				
					}
					
				});
               select.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						
						re_export();
					
							
					}

					
					
				});
               
				dialog1.setCancelable(false);
				dialog1.show();
				
			}
			else
			{
				cf.ShowToast("Sorry. This record has been reallocated to another inspector.", 1);
				
			}
		}
		
		

		
			// TODO Auto-generated method stub
			
		
};
	

	

	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		
	}
	/** PROGRESS DILOG WITH LODING DATA INFORMATION  ENDS
	 * @throws XmlPullParserException 
	 * @throws IOException **/

	public void show_staus() throws IOException, XmlPullParserException ,SocketTimeoutException,Exception{
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(cf.NAMESPACE,"GetInspectionStatus");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectorID",cf.Insp_id);
		request.addProperty("SRID",cf.selectedhomeid);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		androidHttpTransport.call(cf.NAMESPACE+"GetInspectionStatus",envelope);
		System.out.println("property "+request);
		String result =  envelope.getResponse().toString();
		System.out.println("statusresult "+result);
		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			cf.exprtcnt=cf.fn_CheckExportCount(cf.Insp_id,cf.selectedhomeid);
			show_handler=1;
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ ExportInspection.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			 
			
		} catch (NetworkErrorException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ ExportInspection.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ ExportInspection.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		
		} catch (TimeoutException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ ExportInspection.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		
		} catch (XmlPullParserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ ExportInspection.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ ExportInspection.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		
		}
		myhandler.sendEmptyMessage(0);
	}
	private Handler myhandler = new Handler() {


		public void handleMessage(Message msg) {
			System.out.println("show_handler"+show_handler);
			if(show_handler==1)
			{
				show_handler=0;cf.pd.dismiss();System.out.println("dismiss");
				/*if(cf.exprtcnt==0)
				{*/
					
				/*}
				else if(cf.exprtcnt>0)
				{*/
				if(cf.onlstatus.equals("Reexport"))
				{}else{
					    dialog1.dismiss();}
					    System.out.println("tetsed");
					    cf.alerttitle="Re-export";
					    cf.alertcontent="This inspection has been exported already."+"<br /><br />"+"<b>Details :</b>"+cf.exprtcnt+
							    "<b>Are you sure want to Re-export again?</b>";
					    final Dialog dialog1 = new Dialog(ExportInspection.this,android.R.style.Theme_Translucent_NoTitleBar);
						dialog1.getWindow().setContentView(R.layout.alertsync);
						TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
						txttitle.setText( cf.alerttitle);
						TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
						txt.setText(Html.fromHtml( cf.alertcontent));
						Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
						Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
						btn_yes.setOnClickListener(new OnClickListener()
						{
		                	@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								dialog1.dismiss();
								
								//re_export();
								if(cf.onlstatus.equals("Reexport"))
								{
									fn_export();
									
								}
								else{
								 StartExport(cf.selectedhomeid,incre_unit);}
							}

							
						
						});
						btn_cancel.setOnClickListener(new OnClickListener()
						{

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								dialog1.dismiss();
								
							}
							
						});
						dialog1.setCancelable(false);
						dialog1.show();
				//}
				
			}
			else if(show_handler==2)
			{
				show_handler=0;cf.pd.dismiss();
				cf.ShowToast("Please check your network connection and try again later.", 0);
			}
		}
	};
	protected void re_export() {
		// TODO Auto-generated method stub
		System.out.println("reexport");
		Export_chk[1]=Export_chk[2]=Export_chk[3]=Export_chk[4]=Export_chk[5]=Export_chk[6]=Export_chk[7]=Export_chk[8]=Export_chk[9]=Export_chk[0]="0";
			if(sta_chk[0].isChecked())
				Export_chk[1]="1";
			else
				Export_chk[1]="0";
			if(sta_chk[1].isChecked())
				Export_chk[2]="1";
			else
				Export_chk[2]="0";
			if(sta_chk[2].isChecked())
				Export_chk[3]="1";
			else
				Export_chk[3]="0";
			if(sta_chk[3].isChecked())
				Export_chk[4]="1";
			else
				Export_chk[4]="0";
			if(sta_chk[4].isChecked())
				Export_chk[5]="1";
			else
				Export_chk[5]="0";
			if(sta_chk[5].isChecked())
				Export_chk[6]="1";
			else
				Export_chk[6]="0";
			if(sta_chk[6].isChecked())
				Export_chk[7]="1";
			else
				Export_chk[7]="0";
			if(sta_chk[7].isChecked())
				Export_chk[8]="1";
			else
				Export_chk[8]="0";
			if(sta_chk[8].isChecked())
				Export_chk[9]="1";
			else
				Export_chk[9]="0";
			
			
			dialog1.setCancelable(true);
			dialog1.dismiss();

		
			
			
			/** get the units for increament for every uploading **/
			int j=0;
		
			for(int i=1;i<Export_chk.length;i++)
			{
				if(Export_chk[i].equals("1"))
					j++;
			}
			try
			{
				incre_unit=80.00/Double.parseDouble(String.valueOf(j));
				System.out.println("increament unit is "+incre_unit+" j = "+j);
			}
			catch(Exception e)
			{
				incre_unit=10.00;
			}
			//int incre_unit=100/j;
			/** get the units for increament for every uploading Ends **/
			System.out.println("increament unit is "+incre_unit);
			/** export will start here **/
			if(j>=1)
			{
				/*String source = "<b><font color=#00FF33>Retrieving data. Please wait..."
						+ "</font></b>";
				cf.pd = ProgressDialog.show(ExportInspection.this, "", Html.fromHtml(source), true);
				Thread thread = new Thread(ExportInspection.this);
				thread.start();*/
			   
			StartExport(cf.selectedhomeid,incre_unit);
			}
			else
			{
				cf.ShowToast("Please select atleast one option ", 1);
			}
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(ExportInspection.this, HomeScreen.class));
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

};


